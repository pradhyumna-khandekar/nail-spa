<?php

require_once("setting.fya");
$DB = Connect();
$response = array();
$mobile_no = isset($_POST['phone']) ? trim($_POST['phone']) : '';
$offer_id = isset($_POST['offer_id']) ? trim($_POST['offer_id']) : '';
if ($mobile_no != '' && $offer_id != '') {
    /*
     * Check Customer Exist Or Not
     */
    $custexist_q = "Select * from tblCustomers Where SUBSTRING(CustomerMobileNo , -10) LIKE '%" . substr(trim($mobile_no), -10) . "%'";
    $custexist_exe = $DB->query($custexist_q);
    if ($custexist_exe->num_rows > 0) {
        $cust_data = $custexist_exe->fetch_assoc();
        $strCustomerID = $cust_data['CustomerID'];


        /*
         * Customer Exist then check if offerid present with unused status
         */
        $offerused_q = "Select * from tblAppointments "
                . " Where CustomerID ='" . $strCustomerID . "' AND website_offer_id ='" . $offer_id . "'";
        $offerused_exe = $DB->query($offerused_q);
        if ($offerused_exe->num_rows > 0) {
            $offer_data = $offerused_exe->fetch_assoc();
        }

        if (isset($offer_data) && is_array($offer_data) && count($offer_data) > 0) {
            $response['offer_apt_id'] = $offer_data['AppointmentID'];
            $response['website_offer_redempt'] = $offer_data['website_offer_redempt'];
        }
    }
}
echo json_encode($response);
?>