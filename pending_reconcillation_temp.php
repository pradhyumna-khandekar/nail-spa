<?php require_once("setting.fya"); ?>
<?php

$DB = Connect();
$approve_data = select("*", "emp_reconcillation", "status = 1 AND created_by_type=1 AND created_date LIKE '%" . date('Y-m-d') . "%'");

if (isset($approve_data) && is_array($approve_data) && count($approve_data) > 0) {
    $email_body = '<html id="printarea">
    <head>
        <style>
            .main-table{background-color:#fafafa;font-family:Arial, Helvetica, sans-serif; font-size:14px; margin:0; padding:0;}
            tr, td{font-size: 14px;}
            th{color:#795548;padding:5px;}
            h1{margin: 0;padding: 10px;background-color: #f5f5f5;text-align:left;font-size:25px;color:#607D8B;}
            .report-table{border-collapse: collapse;display:block;text-align:center;}
            .report-table tr{border-bottom: 2px solid #795548;}
            .report-table tr td{padding:5px;}
        </style>
    </head>


    <body>

        <table  class="main-table" cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">

            <td align="center" valign="top" style="padding:20px 0 20px 0">

                <table bgcolor="#FFFFFF" cellspacing="0" cellpadding="10" border="0" width="600" style="border:1px solid #dddddd;">


                    <tr>
                        <td colspan="2" style="padding:10px;">
                            <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td colspan="2" style="background-color: #f5f5f5;padding:15px 0 0;text-align:center;">
                                        <h1 class="text-center" style="padding:0 0 15px;background:none;text-align:center;font-size:25px;color:#607D8B;">Pending Reconcillation Approve On ' . date('d/m/Y') . '</h1>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>';


    $email_body.= '<tr>
                                                    <td colspan="2" valign="top" style="border-bottom:1px solid #dddddd; padding:10px; ">
                                                        <table class="report-table">
                                                            <tr style="background-color: #000;">
                                                                <th style="color: #fff;">Sr. No.</th>
                                                                <th style="color: #fff;">Invoice. No.</th>
                                                                <th style="color: #fff;">Client Name</th>
                                                                <th style="color: #fff;">Employee</th> 
                                                                <th style="color: #fff;">Branch</th> 
                                                                <th style="color: #fff;">Approved By</th>
                                                            </tr>';


    /*
     * Get All Admin
     */
    $all_admin = select("*", "tblAdmin", "AdminID > 0");
    if (isset($all_admin) && is_array($all_admin) && count($all_admin) > 0) {
        foreach ($all_admin as $akey => $avalue) {
            $admin_data[$avalue['AdminID']] = $avalue;
        }
    }

    /*
     * Get All Employee
     */
    $all_emp = select("*", "tblEmployees", "EID > 0");
    if (isset($all_emp) && is_array($all_emp) && count($all_emp) > 0) {
        foreach ($all_emp as $ekey => $evalue) {
            $emp_data[$evalue['EID']] = $evalue;
        }
    }

    /*
     * Get All Stores
     */
    $all_store = select("*", "tblStores", "StoreID > 0");
    if (isset($all_store) && is_array($all_store) && count($all_store) > 0) {
        foreach ($all_store as $skey => $svalue) {
            $store_data[$svalue['StoreID']] = $svalue;
        }
    }

    foreach ($approve_data as $akey => $avalue) {
        $appoint_id[$avalue['appointment_id']] = $avalue['appointment_id'];
    }

    /*
     * Get Invoice Number
     */
    if (isset($appoint_id) && is_array($appoint_id) && count($appoint_id) > 0) {
        $in_appoint_ids = implode(",", $appoint_id);
        if ($in_appoint_ids != '') {
            $invoice_data = select("*", "tblInvoiceDetails", "AppointmentId IN (" . $in_appoint_ids . ")");
            if (isset($invoice_data) && is_array($invoice_data) && count($invoice_data) > 0) {
                foreach ($invoice_data as $ikey => $ivalue) {
                    $all_invoice[$ivalue['AppointmentId']] = $ivalue;
                    $all_cust_ids[$ivalue['CustomerID']] = $ivalue['CustomerID'];
                }
            }
        }
    }

    /*
     * Get All Customer
     */
    if (isset($all_cust_ids) && is_array($all_cust_ids) && count($all_cust_ids) > 0) {
        $in_cust_ids = implode(",", $all_cust_ids);
        if ($in_cust_ids != '') {
            $customer_data = select("CustomerID,CustomerFullName", "tblCustomers", "CustomerID IN (" . $in_cust_ids . ")");
            if (isset($customer_data) && is_array($customer_data) && count($customer_data) > 0) {
                foreach ($customer_data as $ckey => $cvalue) {
                    $all_customer[$cvalue['CustomerID']] = $cvalue;
                }
            }
        }
    }

    if (isset($approve_data) && is_array($approve_data) && count($approve_data) > 0) {
        $counter = 1;
        foreach ($approve_data as $key => $value) {
            $customer_id = isset($all_invoice[$value['appointment_id']]['CustomerID']) ? $all_invoice[$value['appointment_id']]['CustomerID'] : 0;
            $CustomerFullName = isset($all_customer[$customer_id]) ? $all_customer[$customer_id]['CustomerFullName'] : '';

            $invoice_id = isset($all_invoice[$value['appointment_id']]['InvoiceId']) ? $all_invoice[$value['appointment_id']]['InvoiceId'] : 0;

            $employee_name = isset($emp_data[$value['emp_id']]) ? $emp_data[$value['emp_id']]['EmployeeName'] : '';

            $emp_store_id = isset($emp_data[$value['emp_id']]) ? $emp_data[$value['emp_id']]['StoreID'] : '0';

            $emp_store_name = isset($store_data[$emp_store_id]) ? ucwords($store_data[$emp_store_id]['StoreName']) : '';

            $approve_name = isset($admin_data[$value['created_by']]) ? $admin_data[$value['created_by']]['AdminFullName'] : '';

            $email_body .= '<tr id="my_data_tr_<?= $counter ?>"> 
                                                                    <td>' . $counter . '</td>
                                                                    <td>' . $invoice_id . '</td>
                                                                    <td>' . $CustomerFullName . '</td>
                                                                    <td>' . $employee_name . '</td>
                                                                        <td>' . $emp_store_name . '</td>
                                                                    <td>' . $approve_name . '</td>
                                                                    
     
</tr>';
            $counter++;
        }
    }
    $email_body .= '</tbody>

                                                        </table>
                                                    </td>
                                                </tr>';

    $DB->close();

    $email_body .= '</table>
            </td>
        </tr>
    </table>
</body>
</html>';
}

return $email_body;
