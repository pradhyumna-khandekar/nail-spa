<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
	$strPageTitle = "Manage Customers | Nailspa";
	$strDisplayTitle = "Manage Customers Location for Nailspa";
	$strMenuID = "10";
	$strMyTable = "tblCustomerLocation";
	$strMyTableID = "CustomerID";
	$strMyField = "CustomerMobileNo";
	$strMyActionPage = "customerlocationadd.php";
	$strMessage = "";
	$sqlColumn = "";
	$sqlColumnValues = "";
	
// code for not allowing the normal admin to access the super admin rights	
	if($strAdminType!="0")
	{
		die("Sorry you are trying to enter Unauthorized access");
	}
// code for not allowing the normal admin to access the super admin rights	


	
	

?>


<!DOCTYPE html>
<html lang="en">

<head>
	<?php require_once("incMetaScript.fya"); ?>
	


<script language=JavaScript>
    var message = "Please dont try this as we are keeping a track of who is trying this!";

    function clickIE4() {
        if (event.button == 2) {
            //alert(message);
            //return false;
        }
    }

    function clickNS4(e) {
        if (document.layers || document.getElementById && !document.all) {
            if (e.which == 2 || e.which == 3) {
                alert(message);
                return false;
            }
        }
    }
    if (document.layers) {
        document.captureEvents(Event.MOUSEDOWN);
        document.onmousedown = clickNS4;
    } else if (document.all && !document.getElementById) {
        document.onmousedown = clickIE4;
    }
    document.oncontextmenu = new Function("alert(message);return false")
</script>

<script>
jQuery(document).ready(function($){
    $(document).keydown(function(event) { 
        var pressedKey = String.fromCharCode(event.keyCode).toLowerCase();
        
        if (event.ctrlKey && (pressedKey == "c" || pressedKey == "u" || pressedKey == "v" || pressedKey == "a" || pressedKey == "s")) {
            alert('Sorry, This Functionality Has Been Disabled For Security Reasons!'); 
            //disable key press porcessing
            return false; 
        }
    });
});

$(document).keyup(function(e){
  if(e.keyCode == 44) return false;
});

</script>
<!-----------css & js files added for tabs by gandhali 3/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>
</head>

<body  style="-webkit-touch-callout: none;
-webkit-user-select: none;
-khtml-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;" 
 unselectable="on"
 onselectstart="return false;" ondragstart="return false;">
 
    <div id="sb-site">
        
	<?php // require_once("incOpenLayout.fya"); ?>
            <!----------commented by gandhali 3/9/18---------------->
		
		
        <?php require_once("incLoader.fya"); ?>
		
        <div id="page-wrapper">
            <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>
            
				<?php require_once("incLeftMenu.fya"); ?>
			
            <div id="page-content-wrapper">
                <div id="page-content">
                    
					<?php require_once("incHeader.fya"); ?>
					

                    <div id="page-title">
                        <h2><?=$strDisplayTitle?></h2>
                        <p>Add, Edit, Delete Customers</p>
                    </div>
<?php
if(!isset($_GET["uid"]))
{
?>					
					
                    <div class="panel">
						<div class="panel">
							<div class="panel-body">
								
								<div class="example-box-wrapper">
									<div class="tabs">
										<ul>
											
											<li><a href="#normal-tabs-2" title="Tab 2">Add Customer Location</a></li>
											<!--<li><a href="#normal-tabs-3" title="Tab 3">Edit Customer Location</a></li>-->
											<li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab" data-toggle="tab"  aria-expanded="false">Add Bulk Customer Location</a></li>
										</ul>
										
			
										
										<div id="normal-tabs-2">
											<div class="panel-body">
													<div class="form-group"><label class="col-sm-3 control-label">Add Location <span>*</span></label>
														<div class="col-sm-4"><input type="text" name="location" id="cutlocation" class="form-control required" placeholder="Customer Location"></div>
														<input type="submit" class="btn ra-100 btn-primary locationbtn" value="Submit">
													</div>
													
															
															
															
												<div class="csulocationadd">	</div>	
											</div>
										</div>
	<!--Tab 3 started-->									
										<!--<div id="normal-tabs-3">
											<div class="panel-body">
													<div class="form-group"><label class="col-sm-3 control-label">FirstName <span>*</span></label>
														<div class="col-sm-4"><input type="text" name="FirstName" id="FirstName" class="form-control required" placeholder="Customer Location"></div>
														<input type="submit" class="btn ra-100 btn-primary" value="Submit">
													</div>
											</div>
										</div>-->
	<!--Tab 3 end-->									
										<div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="data-tab">
									
											<?php require_once "Excelbulcklocation.php"; ?>
									
										</div>
									</div>
								</div>
							</div>
						</div>
                    </div>
<?php
} // End null condition


?>
        
		
        <?php require_once 'incFooter.fya'; ?>
		
    </div>
	<script>
	$(document).ready(function(){
		$(".locationbtn").click(function(){
			var location = $("#cutlocation").val();
			if(location == ''){
				alert('Please Enter Location');
			}
			else{
				$.ajax({
					method:"post",
					url:"ajaxinputCustomerLocation.php",
					data:{location:location},
					success:function(responce){
						$(".csulocationadd").html(responce);
                                                
					}
				});
			}
		});
	});
	</script>
</body>

</html>