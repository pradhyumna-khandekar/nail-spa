<?php

require_once("setting.fya");
$DB = Connect();
$is_data = isset($_GET['type']) && $_GET['type'] != '' ? $_GET['type'] : '';
$sql = "SELECT * FROM tblStores WHERE Status=0";
$RS2 = $DB->query($sql);
if ($RS2->num_rows > 0) {
    while ($row2 = $RS2->fetch_assoc()) {
        if ($is_data == 'data') {
            $store[$row2["StoreID"]] = $row2;
        } else {
            $store[$row2["StoreID"]] = $row2["StoreName"];
        }
    }
}

$DB->close();

$storejson = json_encode($store);
echo $storejson;
?>