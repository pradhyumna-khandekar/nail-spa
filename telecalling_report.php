<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
$strPageTitle = "TeleCalling Report | NailSpa";
$strDisplayTitle = "TeleCalling Report for NailSpa";
$strMenuID = "10";
$strMyTable = "tblAppointments";
$strMyTableID = "AppointmentID";
$strMyField = "AppointmentDate";
$strMyActionPage = "feedback_list.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <?php require_once("incChart-SalonDashboard.fya"); ?>

        <!-----------------included datatable.css & js files by gandhali 1/9/18----------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/datatable/datatable.css">
        <script type="text/javascript" src="assets/widgets/datatable/datatable.js"></script>
        <script type="text/javascript" src="assets/widgets/datatable/datatable-bootstrap.js"></script>
        <script type="text/javascript" src="assets/widgets/datatable/datatable-responsive.js"></script>
        <!-----------------included datatable.css by gandhali 1/9/18----------------->
        <style>
            .btn-danger:hover
            {
                border-color: #fc8213;
                background: #fc8213;
            }

        </style>
        <!-- Styles -->


    </head>

    <body>

        <div id="sb-site">


            <?php // require_once("incOpenLayout.fya");   ?>
            <!----------commented by gandhali 1/9/18---------------->


            <?php require_once("incLoader.fya"); ?>


            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>

                        <script type="text/javascript" src="assets/widgets/skycons/skycons.js"></script>
                        <script type="text/javascript" src="assets/widgets/datatable/datatable.js"></script>
                        <script type="text/javascript" src="assets/widgets/datatable/datatable-bootstrap.js"></script>
                        <script type="text/javascript" src="assets/widgets/datatable/datatable-tabletools.js"></script>

                        <?php
                        $sqlTempfrom = '';
                        $sqlTempto = '';
                        if (isset($_GET["toandfrom"])) {
                            $strtoandfrom = $_GET["toandfrom"];
                            $arraytofrom = explode("-", $strtoandfrom);

                            $from = $arraytofrom[0];
                            $datetime = new DateTime($from);
                            $getfrom = $datetime->format('Y-m-d');


                            $to = $arraytofrom[1];
                            $datetime = new DateTime($to);
                            $getto = $datetime->format('Y-m-d');

                            if (!IsNull($from)) {
                                $sqlTempfrom = " AND Date(non_visiting_update_date)>=Date('" . $getfrom . "')";
                            }

                            if (!IsNull($to)) {
                                $sqlTempto = " AND Date(non_visiting_update_date)<=Date('" . $getto . "')";
                            }

                            $append = '';
                            if (isset($_GET["Store"]) && $_GET["Store"] != '') {
                                $append .= " AND StoreID ='" . $_GET["Store"] . "'";
                            }
                            $DB = Connect();

                            /*
                             * get all status Count
                             */
                            $book_countq = "SELECT COUNT(AppointmentID) as cust_count FROM tblAppointments "
                                    . " WHERE non_visiting_update_value=1 " . $sqlTempfrom . $sqlTempto;
                            $book_countq_exe = $DB->query($book_countq);
                            if ($book_countq_exe->num_rows > 0) {
                                while ($book_count_res = $book_countq_exe->fetch_assoc()) {
                                    $book_now_count = $book_count_res['cust_count'];
                                }
                            }

                            $call_countq = "SELECT COUNT(AppointmentID) as cust_count FROM tblAppointments "
                                    . " WHERE non_visiting_update_value=2 " . $sqlTempfrom . $sqlTempto;
                            $call_countq_exe = $DB->query($call_countq);
                            if ($call_countq_exe->num_rows > 0) {
                                while ($call_count_res = $call_countq_exe->fetch_assoc()) {
                                    $call_count = $call_count_res['cust_count'];
                                }
                            }

                            $nint_countq = "SELECT COUNT(AppointmentID) as cust_count FROM tblAppointments "
                                    . " WHERE non_visiting_update_value=9" . $sqlTempfrom . $sqlTempto;
                            $nint_countq_exe = $DB->query($nint_countq);
                            if ($nint_countq_exe->num_rows > 0) {
                                while ($nint_count_res = $nint_countq_exe->fetch_assoc()) {
                                    $not_interested_count = $nint_count_res['cust_count'];
                                }
                            }

                            $client_call_countq = "SELECT COUNT(AppointmentID) as cust_count FROM tblAppointments "
                                    . " WHERE non_visiting_update_value=10" . $sqlTempfrom . $sqlTempto;
                            $client_call_exe = $DB->query($client_call_countq);
                            if ($client_call_exe->num_rows > 0) {
                                while ($cli_call_count_res = $client_call_exe->fetch_assoc()) {
                                    $client_call_count = $cli_call_count_res['cust_count'];
                                }
                            }
                        }
                        ?>

                        <div class="panel">
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="example-box-wrapper">
                                        <div id="normal-tabs-1">
                                            <span class="form_result">&nbsp; <br>
                                            </span>

                                            <div class="panel-body">
                                                <h4 class="title-hero"><center>Telecalling Report | NailSpa</center></h4>
                                                <br>

                                                <form method="get" class="form-horizontal bordered-row" role="form" action="telecalling_report.php">
                                                    <div class="form-group"><label for="" class="col-sm-4 control-label">Select date</label>
                                                        <div class="col-sm-4">
                                                            <div class="input-prepend input-group">
                                                                <span class="add-on input-group-addon">
                                                                    <i class="glyph-icon icon-calendar"></i>
                                                                </span> 
                                                                <input type="text" name="toandfrom" id="daterangepicker-example" class="form-control" value="<?php echo isset($strtoandfrom) ? $strtoandfrom : ''; ?>">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">Select Store</label>
                                                        <div class="col-sm-4">
                                                            <select name="Store" class="form-control">
                                                                <option value="0">All</option>
                                                                <?php
                                                                $selp = select("*", "tblStores", "Status='0'");
                                                                foreach ($selp as $val) {
                                                                    $strStoreName = $val["StoreName"];
                                                                    $strStoreID = $val["StoreID"];
                                                                    $store = $_GET["Store"];
                                                                    if ($store == $strStoreID) {
                                                                        ?>
                                                                        <option  selected value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                        <?php
                                                                    } else {
                                                                        ?>
                                                                        <option value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>


                                                    <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                        <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                                        &nbsp;&nbsp;&nbsp;
                                                        <a class="btn btn-link" href="telecalling_report.php">Clear All Filter</a>
                                                        &nbsp;&nbsp;&nbsp;

                                                    </div>
                                                </form>

                                                <div class="example-box-wrapper">
                                                    <?php if (isset($_GET["toandfrom"]) && $_GET["toandfrom"] != '') { ?>
                                                        <table class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                                                            <thead>
                                                                <tr>
                                                                    <th><center>Remark Type</center></th>
                                                            <th><center>Count</center></th>
                                                            </tr>
                                                            </thead>


                                                            <tbody>
                                                                <tr>
                                                                    <td>Book Now</td>
                                                                    <td><?php echo isset($book_now_count) ? $book_now_count : 0; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Call Later</td>
                                                                    <td><?php echo isset($call_count) ? $call_count : 0; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Not Interested</td>
                                                                    <td><?php echo isset($not_interested_count) ? $not_interested_count : 0; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Client will call</td>
                                                                    <td><?php echo isset($client_call_count) ? $client_call_count : 0; ?></td>
                                                                </tr>

                                                            </tbody>
                                                            <?php
                                                            $DB->close();
                                                            ?>
                                                        </table>
                                                        <?php
                                                    } else {
                                                        echo "<br><center><h3>Please Select Month And Year!</h3></center>";
                                                    }
                                                    ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php require_once 'incFooter.fya'; ?>

                </div>
                </body>

                </html>		