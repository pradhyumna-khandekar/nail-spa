<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
$strPageTitle = "Customer Details | NailSpa";
$strDisplayTitle = "Customer Details for NailSpa";
$strMenuID = "10";
$strMyTable = "tblAppointments";
$strMyTableID = "AppointmentID";
$strMyField = "AppointmentDate";
$strMyActionPage = "DisplayNonCustomerCount.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <?php require_once("incChart-SalonDashboard.fya"); ?>

        <!-----------------included datatable.css & js files by gandhali 1/9/18----------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/datatable/datatable.css">
        <script type="text/javascript" src="assets/widgets/datatable/datatable.js"></script>
        <script type="text/javascript" src="assets/widgets/datatable/datatable-bootstrap.js"></script>
        <script type="text/javascript" src="assets/widgets/datatable/datatable-responsive.js"></script>
        <!-----------------included datatable.css by gandhali 1/9/18----------------->
        <style>
            .btn-danger:hover
            {
                border-color: #fc8213;
                background: #fc8213;
            }

        </style>
        <!-- Styles -->


    </head>

    <body>

        <div id="sb-site">


            <?php // require_once("incOpenLayout.fya");   ?>
            <!----------commented by gandhali 1/9/18---------------->


            <?php require_once("incLoader.fya"); ?>


            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>

                        <script type="text/javascript" src="assets/widgets/skycons/skycons.js"></script>
                        <script type="text/javascript" src="assets/widgets/datatable/datatable.js"></script>
                        <script type="text/javascript" src="assets/widgets/datatable/datatable-bootstrap.js"></script>
                        <script type="text/javascript" src="assets/widgets/datatable/datatable-tabletools.js"></script>

<?php
$sqlTempfrom ='';
$sqlTempto = '';
if (isset($_GET["toandfrom"])) {
    $strtoandfrom = $_GET["toandfrom"];
    $arraytofrom = explode("-", $strtoandfrom);

    $from = $arraytofrom[0];
    $datetime = new DateTime($from);
    $getfrom = $datetime->format('Y-m-d');


    $to = $arraytofrom[1];
    $datetime = new DateTime($to);
    $getto = $datetime->format('Y-m-d');

    if (!IsNull($from)) {
        $sqlTempfrom = " and Date(approved_date)>=Date('" . $getfrom . "')";
    }

    if (!IsNull($to)) {
        $sqlTempto = " and Date(approved_date)<=Date('" . $getto . "')";
    }
}?>

                        <div class="panel">
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="example-box-wrapper">
                                        <div id="normal-tabs-1">
                                            <span class="form_result">&nbsp; <br>
                                            </span>

                                            <div class="panel-body">
                                                <h4 class="title-hero"><center>List of 	Employee Transfer | NailSpa</center></h4>
                                                <br>


                                                <form method="get" class="form-horizontal bordered-row" role="form" action="emp_transfer_report.php">
                                                            <div class="form-group"><label for="" class="col-sm-4 control-label">Select date</label>
                                                                <div class="col-sm-4">
                                                                    <div class="input-prepend input-group">
                                                                        <span class="add-on input-group-addon">
                                                                            <i class="glyph-icon icon-calendar"></i>
                                                                        </span> 
                                                                        <input type="text" name="toandfrom" id="daterangepicker-example" class="form-control" value="<?= $strtoandfrom ?>">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            
                                                            
                                                            <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                                <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                                                &nbsp;&nbsp;&nbsp;
                                                                <a class="btn btn-link" href="emp_transfer_report.php">Clear All Filter</a>
                                                                &nbsp;&nbsp;&nbsp;
                                                                
                                                            </div>
                                                        </form>

                                                <div class="example-box-wrapper">
                                                    <table class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th><center>Store Name</center></th>
                                                        <th><center>Transfer Count</center></th>
                                                        </tr>
                                                        </thead>


                                                        <tbody>
                                                            <?php
                                                            $total_count = 0;
                                                            $DB = Connect();
                                                            $n5_daysAgot = date('Y-m-d', strtotime('-50 days', time()));
                                                            $selp = select("*", "tblStores", "Status='0'");
                                                            if (isset($selp) && is_array($selp) && count($selp) > 0) {
                                                                foreach ($selp as $skey => $svalue) {
                                                                    /*
                                                                     * Get Customer Count
                                                                     */
                                                                    $store_append = " AND from_store='" . $svalue['StoreID'] . "'";
                                                                    $countq = "SELECT COUNT(id) as trans_count FROM emp_transfer_request WHERE  status=1 AND request_status=2 ". $store_append . $sqlTempfrom . $sqlTempto;
                                                                   // echo $countq;exit;
                                                                    $countq_exe = $DB->query($countq);
                                                                    if ($countq_exe->num_rows > 0) {
                                                                        while ($count_res = $countq_exe->fetch_assoc()) {
                                                                            $total_transfer = $count_res['trans_count'];
                                                                        }
                                                                    }


                                                                    ?>
                                                                    <tr onclick="location.href = 'DisplaySalonNonCustomerDetails.php?Store=<?php echo $svalue['StoreID']; ?>'">
                                                                        <td><?php echo $svalue['StoreName']; ?></td>
                                                                        <td class="numeric"><b><?php
                                                                        $store_count = isset($total_transfer) ? $total_transfer : 0;
                                                                         echo $store_count;
                                                                         $total_count += $store_count; ?></b></td>
                                                                    </tr>
                                                                    <?php
                                                                }?>
                                                                <tr>
                                                                    <td>Total</td>
                                                                    <td><b><?php echo isset($total_count) ? $total_count : 0;?></b></td>

                                                                </tr>
                                                            <?php }
                                                            ?>
                                                        </tbody>
                                                        <?php
                                                        $DB->close();
                                                        ?>
                                                    </table>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php require_once 'incFooter.fya'; ?>

                </div>
                </body>

                </html>		