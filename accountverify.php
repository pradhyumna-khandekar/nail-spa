<?php

require_once('setting.fya');

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $Email = $_POST["email"];
    $Password = $_POST["password"];
    $Remember_me = $_POST["rememberme"];

    $DB = Connect();
    $sql = "SELECT AdminMobile,AdminID, AdminFullName, AdminPassword, AdminType, AdminRoleID, AdminEmailID , Status from tblAdmin where AdminUsername='" . $Email . "'";
    $RS = $DB->query($sql);
    if ($RS->num_rows > 0) {
        while ($row = $RS->fetch_assoc()) {
            $AdminID = $row["AdminID"];
            $AdminUsername = $Email;
            $AdminFullName = $row["AdminFullName"];
            $AdminPassword = $row["AdminPassword"];
            $AdminType = $row["AdminType"];
            $verify = ENCODEQ($AdminType);
            $Status = $row["Status"];
            //$AdminMobile = $row["AdminMobile"];
            $AdminRoleID = $row["AdminRoleID"];

            $selectStore = "Select StoreID from tblAdminStore where AdminID=$AdminID";
            $RS1 = $DB->query($selectStore);
            if ($RS1->num_rows > 0) {
                while ($row1 = $RS1->fetch_assoc()) {
                    $strStoreID = $row1["StoreID"];
                }
            }
            $seldata = select("StoreID", "tblAdminStore", "AdminID='" . $AdminID . "'");
            $storeid = $seldata[0]['StoreID'];

            /*
             * Get Store NUmber
             */
            $store_data = select("*", "tblStores", "StoreID='" . $storeid . "'");
            $AdminMobile = $store_data[0]['StoreOfficialNumber'];
            $store_name = $store_data[0]['StoreName'];
        }
        if ($Status == "0") {

            if ($AdminPassword == $Password) {

                /*
                 * Check Available ip Address
                 */
                if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] != '') {
                    $current_ip = $_SERVER['REMOTE_ADDR'];
                    $emp_ips = select("*", "login_ipaddress", "user_type=1 AND status=1 AND user_id='" . $AdminID . "'");
                    if (isset($emp_ips) && is_array($emp_ips) && count($emp_ips) > 0) {
                        foreach ($emp_ips as $eikey => $eivalue) {
                            $emp_all_ids[$eivalue['id']] = trim($eivalue['ip_address']);
                        }
                    }
                }

                if (in_array($current_ip, $emp_all_ids) || in_array('*', $emp_all_ids) || $AdminRoleID == '43') {

                    if ($AdminRoleID == 6) {
                        /*
                         * If Role is Salon Manager Send Login OTP
                         */
                        $hash_code = rand(100000, 999999);

                        /*
                         * If OTP is send for the first time then use store number else use farheen number
                         */
                        $opt_exist = select("*", "otp_log", "store_id='" . $storeid . "' AND status=1");
                        if (isset($opt_exist) && is_array($opt_exist) && count($opt_exist) > 0) {
                            $EmpContact = '9769014888';
                        } else {
                            $EmpContact = $AdminMobile;
                        }


                        //$EmpContact = '8390810863';
                        if ($EmpContact != '') {
                            $message = "Your Verification Code For " . $store_name . " Login is " . $hash_code;
                            $xmldata = '<?xml version="1.0" encoding="UTF-8"?>
                  <api>
                  <campaign>Service Reminder</campaign>
                  <unicode>0</unicode>
                  <flash>0</flash>
                  <sender>NAILSP</sender>
                  <message><![CDATA[' . $message . ']]></message>
                  <sms>
                  <to>' . $EmpContact . '</to>
                  </sms>
                  </api>';
                            $api_url = 'http://api-alerts.solutionsinfini.com/v3/?method=sms.xml&api_key=Aebe2e79fd5ce8a3ffb0720d4bef17fe9&format=json&xml=' . urlencode($xmldata);
                            //$response = file_get_contents($api_url);
                            $response = json_decode($response, true);
                            $strSendingStatus = $response['status'];
                            $strSendingStatus = 'OK';
                            if ($strSendingStatus == 'OK') {

                                $sms_insert = "INSERT INTO tblsmsmessages(CustomerID,SMSTo,Message,Status,created_date,description,msg_status,send_datetime)"
                                        . " VALUES('" . $AdminID . "','" . $EmpContact . "','" . $message . "','1','" . date('Y-m-d H:i:s') . "','Login OTP','2','" . date('Y-m-d H:i:s') . "') ";
                                $DB->query($sms_insert);
                                $sms_id = $DB->insert_id;

                                $otp_insert = "INSERT INTO otp_log(user_id,contact_no,status,hash_otp,created_date,smsid,username,password,store_id)"
                                        . " VALUES('" . $AdminID . "','" . $EmpContact . "','1','" . $hash_code . "','" . date('Y-m-d H:i:s') . "','" . $sms_id . "','" . $Email . "','" . $AdminPassword . "','" . $storeid . "')";
                                $DB->query($otp_insert);
                                $otp_id = $DB->insert_id;

                                $otp_url = "AdminOtpForm.php?id=" . $otp_id . "&remember_me=" . $Remember_me;
                                echo("<script>location.href='" . $otp_url . "';</script>");
                            } else {
                                echo('<font color="red">Failed to send OTP.Please Try Again.</font>');
                            }
                        } else {
                            echo('<font color="red">Failed to send OTP.Mobile Number is Empty.</font>');
                        }
                    } else {
                        $CookieRemember_me = Encode("Remember_me," . $Remember_me . ",Remember_me");
                        setcookie("CookieRemember_me", $CookieRemember_me, time() + (10 * 365 * 24 * 60 * 60));

                        if ($Remember_me == "Y") {
                            $CookieAdminID = Encode("AdminID," . $AdminID . ",AdminID");
                            $CookieAdminUsername = Encode("AdminUsername," . $AdminUsername . ",AdminUsername");
                            $CookieAdminFullName = Encode("AdminFullName," . $AdminFullName . ",AdminFullName");
                            $CookieAdminRoleID = Encode("AdminRoleID," . $AdminRoleID . ",AdminRoleID");
                            $CookieAdminType = Encode("AdminType," . $AdminType . ",AdminType");
                            $CookieStore = Encode("StoreID," . $storeid . ",StoreID");


                            setcookie("CookieAdminID", $CookieAdminID, time() + (10 * 365 * 24 * 60 * 60));
                            setcookie("CookieAdminUsername", $CookieAdminUsername, time() + (10 * 365 * 24 * 60 * 60));
                            setcookie("CookieAdminFullName", $CookieAdminFullName, time() + (10 * 365 * 24 * 60 * 60));
                            setcookie("CookieAdminType", $CookieAdminType, time() + (10 * 365 * 24 * 60 * 60));
                            setcookie("CookieStore", $CookieStore, time() + (10 * 365 * 24 * 60 * 60));
                            setcookie("CookieAdminRoleID", $CookieAdminRoleID, time() + (10 * 365 * 24 * 60 * 60));
                        } else {
                            session_start();
                            $_SESSION["AdminID"] = $AdminID;
                            $_SESSION["AdminFullName"] = $AdminFullName;
                            $_SESSION["AdminUsername"] = $AdminUsername;
                            $_SESSION["AdminType"] = $AdminType;
                            $_SESSION["Store"] = $storeid;
                            $_SESSION["AdminRoleID"] = $AdminRoleID;
                        }


                        ExecuteNQ("update tblAdmin set LastLogin=NOW() where AdminID='$AdminID'");
                        echo('<font color="green">Processing Login Process.. Please wait !!</font>');


                        //added by asmita --Start************************************
                        if ($AdminRoleID == '36') {
                            echo("<script>location.href='Dashboard.php';</script>");
                        } elseif ($AdminRoleID == '2') {
                            echo("<script>location.href='Marketing-Dashboard.php';</script>");
                        } elseif ($AdminRoleID == '38') {
                            echo("<script>location.href='Audit-Dashboard.php';</script>");
                        } elseif ($AdminRoleID == '6') {
                            echo("<script>location.href='Salon-Dashboard.php';</script>");
                        } elseif ($AdminRoleID == '4') {
                            echo("<script>location.href='Operation-Dashboard.php';</script>");
                        } elseif ($AdminRoleID == '39') {
                            echo("<script>location.href='Admin-Dashboard.php';</script>");
                        } elseif ($AdminRoleID == '42') {
                            echo("<script>location.href='telecalling_dashboard.php';</script>");
                        } elseif ($AdminRoleID == '43') {
                            echo("<script>location.href='show_cash_invoice.php';</script>");
                        } else {
                            echo("<script>location.href='Dashboard.php';</script>");
                        }
                    }
                } else {
                    echo('<font color="red">IP Address Not Match!</font>');
                }
                //added by asmita--End***************************************
                // echo("<script>location.href='Dashboard.php';</script>"); 
            } else {
                echo('<font color="red">Invalid Password. Please try again</font>');
            }
        } else {
            echo('<font color="red">Account Locked !!</font>');
        }
    } else {
        echo('<font color="red">Invalid Username. Please try again</font>');
    }

    $DB->close();
} else {
    echo "Chori Naa munnaa !! ";
}
?> 