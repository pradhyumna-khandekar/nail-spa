<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Manage Offers | Nailspa";
$strDisplayTitle = "Offers Logs for Nailspa";
$strMenuID = "3";
$strMyTable = "tblOffers";
$strMyTableID = "OfferID";
$strMyField = "OfferName";
$strMyActionPage = "OffersHistoryList.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <!-----------css & js files added for tabs by gandhali 3/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");     ?>
            <!----------commented by gandhali 3/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>



                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-3 box">
                                    <a href="report_config_form.php"><h2>Sales Report</h2></a>
                                </div>
                                <div class="col-md-3 box">
                                    <a href="reconcillation_approve_config.php"><h2>Pending Reconcillation Approve Report</h2></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php require_once 'incFooter.fya'; ?>
        </div>
    </body>
</html>
