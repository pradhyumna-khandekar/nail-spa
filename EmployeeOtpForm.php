<?php
error_reporting(0);
require_once("setting.fya");
?>
<?php
$strRemember_me = ValidateCookie("CookieRemember_me");
if ($strRemember_me == "Y") {
    $strEmpUsername = ValidateCookie("CookieEmpUsername");
} else {
    session_start();
    $strEmpUsername = $_SESSION["EmpUsername"];
}


if (!IsNull($strEmpUsername)) {
    header('Location: DisplayReconcillationDetail.php');
}

$strPageTitle = "Login | NailSpa";
$strMenuID = "1";
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once 'incMetaScript.fya'; ?>

        <script>
            function proceed_otp_check()
            {
                var isValid = true;
                var counter = 0;
                $('.required').each(function () {
                    if ($.trim($(this).val()) == '') {
                        counter++;
                        if (counter == 1)
                        {
                            $(this).focus();
                        }
                        isValid = false;
                        $(this).css({
                            "border": "1px solid #BF0404",
                            "color": "#000000",
                            "background-color": "rgb(234, 221, 221)"
                        });
                    } else {
                        $(this).css({
                            "border": "",
                            "background-color": "",
                        });
                    }
                });
                if (isValid == false)
                {
                    alert('Fields marked red are compulsory');
                    return false;
                }

                StartLoading();
                var otp = $("#inputotp").val();
                var rememberme = $("#remember_me").val();
                var otp_id = $("#otp_id").val();

                $.ajax({
                    type: 'POST',
                    url: '<?= FindHost() ?>/emp_otp_check.php',
                    data: {
                        otp: otp,
                        otp_id: otp_id,
                        rememberme: rememberme
                    },
                    success: function (response)
                    {

                        EndLoading();
                        $('.result_message').html(response);

                    }
                });
            }

        </script>

    </head>

    <body>
        <?php require_once("incLoader.fya"); ?>

        <style type="text/css">
            html,
            body {
                height: 100%;
                background: #fff;
            }
        </style>
        <div class="center-vertical">
            <div class="center-content">

                <?php
                if ($_SERVER['REMOTE_ADDR'] == "111.119.219.70") {
                    $strMPStitle = "Mypetshop";
                    $strimage = "app-mps.png";
                } else {
                    $strMPStitle = "NailSpa";
                    $strimage = "app-icon (2).png";
                }
                ?>

                <form class="col-md-4 col-sm-5 col-xs-11 col-lg-3 center-margin My_LoginotpForm" method="post" onsubmit="proceed_otp_check();
                        return false;">
                    <h3 class="text-center pad25B font-gray text-transform-upr font-size-23"><?= $strMPStitle ?><span class="opacity-80">&nbsp;v1.0</span></h3>

                    <div id="login-form" class="content-box bg-default">


                        <div class="content-box-wrapper pad20A"><img class="mrg25B center-margin radius-all-100 display-block" src="assets/image-resources/<?= $strimage ?>" alt="">

                            <div class="result_message" style="font-size:15px;">&nbsp;</div>
                            <h3>Enter OTP to Continue Login</h3><br>
                            <input type="hidden" id="otp_id" value="<?php echo isset($_GET['id']) ? $_GET['id'] : 0; ?>"/>
                            <input type="hidden" id="remember_me" value="<?php echo isset($_GET['remember_me']) ? $_GET['remember_me'] : ''; ?>"/>
                            <div class="form-group">
                                <div class="input-group"><span class="input-group-addon addon-inside bg-gray"><i class="glyph-icon icon-unlock-alt"></i></span> 
                                    <input autocomplete="off" type="text" class="form-control required" id="inputotp" placeholder="Enter OTP" required="required">
                                </div>
                            </div>

                            <div class="form-group"><button type="submit" class="btn btn-block btn-primary">Login</button></div>

                        </div>
                </form>	
            </div>
        </div>

        <?php require_once 'incFooter.fya'; ?>

    </body>

</html>