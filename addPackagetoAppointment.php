<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php

$strPageTitle = "Manage Customers | Nailspa";
$strDisplayTitle = "Manage Customers for Nailspa";
$strMenuID = "10";
$strMyTable = "tblServices";
$strMyTableID = "ServiceID";
//$strMyField = "CustomerMobileNo";
$strMyActionPage = "appointment_invoice.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights	

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $DB = Connect();
    //$PackageID = $_POST["packageid"];
    $custpackid = $_POST["custpackid"];
    $app_id = $_POST["app_id"];

    if ($app_id != 0 && $custpackid != 0) {
        $package_data = select("*", "tblCustomerPackage", "id='" . $custpackid . "'");

        $PackageID = isset($package_data[0]['package_id']) ? $package_data[0]['package_id'] : 0;


        $apt_data = select("*", "tblAppointments", "AppointmentID='" . $app_id . "'");
        $customerid = $apt_data[0]['CustomerID'];
        /*
         * Add package id to appointment
         */
        $sqlUpdate = "UPDATE tblAppointments SET PackageID='" . $PackageID . "',new_package=2 WHERE AppointmentID='" . $app_id . "'";
        ExecuteNQ($sqlUpdate);

        /*
         * add package amount and validity date
         */
        $seld = select("*", "tblPackages", "PackageID='" . $PackageID . "'");
        $ServiceID = $seld[0]['ServiceID'];
        $PackageNewPrice = $seld[0]['PackageNewPrice'];
        $Validityp = $seld[0]['Validity'];
        $valid = "+" . $Validityp . "Months";
        $validpack = date('Y-m-d', strtotime($valid));

        $ServiceIDd = explode(",", $ServiceID);
        $ServiceQty = explode(",", $seld[0]['Qty']);

        if (isset($ServiceIDd) && is_array($ServiceIDd) && count($ServiceIDd) > 0) {
            foreach ($ServiceIDd as $key => $value) {
                $service_in_pac_qty[$value] = $ServiceQty[$key];
            }
        }
        /*
         * Get pakage Service Code
         */
        $service_in_package_data = select("*", "tblServices", "ServiceID IN(" . $ServiceID . ")");
        if (isset($service_in_package_data) && is_array($service_in_package_data) && count($service_in_package_data) > 0) {
            foreach ($service_in_package_data as $pskey => $psvalue) {
                $service_in_package[$psvalue['ServiceCode']] = isset($service_in_pac_qty[$psvalue['ServiceID']]) ? $service_in_pac_qty[$psvalue['ServiceID']] : 0;
            }
        }

        $sqlcustpackage = "INSERT INTO tblCustomerPackageAmt(appointment_id, customer_id, package_id,package_expiry,package_status,created_date) VALUES 
          ('" . $app_id . "', '" . $customerid . "', '" . $PackageID . "','" . $validpack . "','1','" . date('Y-m-d H:i:s') . "')";

        $DB->query($sqlcustpackage);

        $package_service_res = select("*", "tblCustomerAppointmentPackage", "CustomerPackageId='" . $custpackid . "' AND status=1");
        if (isset($package_service_res) && is_array($package_service_res) && count($package_service_res) > 0) {
            foreach ($package_service_res as $serkey => $servalue) {
                $package_service_data[$servalue['service_code']] = $servalue;
            }
        }

        $customer_package = select("*", "tblCustomerPackage", "customer_id='" . $customerid . "' AND package_id='" . $PackageID . "' AND status=1");


        $current_package_service = select("*", "tblCustomerAppointmentPackage", "CustomerPackageId='" . $custpackid . "' AND appointment_id='" . $app_id . "' AND status=1");
        if (isset($current_package_service) && is_array($current_package_service) && count($current_package_service) > 0) {
            foreach ($current_package_service as $cursekey => $cursevalue) {
                $current_service_data[$cursevalue['service_id']] = $cursevalue['service_id'];
            }
        }

        if (isset($customer_package) && is_array($customer_package) && count($customer_package) > 0) {
            $all_service_id = select("*", "tblAppointmentsDetailsInvoice", "AppointmentID='" . $app_id . "'");

            if (isset($all_service_id) && is_array($all_service_id) && count($all_service_id) > 0) {
                foreach ($all_service_id as $skey => $svalue) {
                    $service_data = select("*", "tblServices", "ServiceID='" . $svalue['ServiceID'] . "'");
                    $service_code = $service_data[0]['ServiceCode'];
                    $package_qty_remain = isset($package_service_data[$service_code]['qty_remaining']) ? $package_service_data[$service_code]['qty_remaining'] : $service_in_package[$service_code];

                    // echo "<br>package_qty_remain=".$package_qty_remain;
                    if (!isset($current_service_data[$svalue['ServiceID']]) && $package_qty_remain > 0 && isset($service_in_package[$service_code])) {
                        $sqlcustpackageservice = "INSERT INTO tblCustomerAppointmentPackage(CustomerPackageId, appointment_id, customer_id,package_id,service_id,service_code,service_amount,package_expiry,service_qty,created_date,service_qty_done,service_apt_id,qty_remaining,service_status,package_qty,package_remain) VALUES
          ('" . $customer_package[0]['id'] . "', '" . $app_id . "', '" . $customerid . "', '" . $PackageID . "', '" . $service_data[0]['ServiceID'] . "','" . $service_data[0]['ServiceCode'] . "','" . $service_data[0]['ServiceCost'] . "','"
                                . $validpack . "','" . $service_in_package[$service_code] . "','" . date('Y-m-d H:i:s') . "','0','0','" . $package_qty_remain . "','1','" . $service_in_package[$service_code] . "','" . $package_qty_remain . "')";
                        //echo "<br>Qry=" . $sqlcustpackageservice;
                        $DB->query($sqlcustpackageservice);
                    }
                }
            }
        }
    }
}
//http_response_code(400);
$DB->close();
?>