<?php require_once("setting.fya"); ?>

<?php
$counter = 1;
require_once 'incFirewall.fya';
$strPageTitle = "Verify Order| Nailspa";
$strDisplayTitle = "Verify Order| Nailspa";
$strMenuID = "6";
$strMyTable = "tblOrder";
$strMyTableID = "OrderID";
$strMyActionPage = "verify_order.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $DB = Connect();
    $post = $_POST;

    $barcode = isset($post['barcode']) ? $post['barcode'] : array();
    $product_id = isset($post['product_id']) ? $post['product_id'] : array();
    $barcode_id = isset($post['barcode_id']) ? $post['barcode_id'] : array();
    $order_id = isset($post['order_id']) ? $post['order_id'] : 0;
    $store_id = isset($post['store_id']) ? $post['store_id'] : 0;
    $barcode_generated = isset($post['barcode_generated']) ? $post['barcode_generated'] : array();
    $product_detail = isset($post['product_detail']) ? $post['product_detail'] : array();
    $confirm_order = isset($post['confirm_order']) ? $post['confirm_order'] : 2;

    /*
     * Case 1: Check if Barcode is Duplicated
     */
    $dups = array();
    foreach (array_count_values($barcode) as $val => $c) {
        if ($c > 1 && trim($val) != '' && $val != 0) {
            $dups[] = $val;
        }
    }

    if (isset($dups) && is_array($dups) && count($dups) > 0) {
        $dup_barcode = implode(",", $dups);
        $error = "Duplicate Barcode " . $dup_barcode . " Found.";
        die('<div class="alert alert-warning alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
								</button>
								<strong>' . $error . '</strong>
							</div>');
        die("");
    } else {
        /*
         * Get System Generated Barcode
         */
        $sys_barcode_data = select("*", "product_barcode", "status=1 AND order_request_id=" . $order_id);
        if (isset($sys_barcode_data) && is_array($sys_barcode_data) && count($sys_barcode_data) > 0) {
            foreach ($sys_barcode_data as $sbkey => $sbvalue) {
                $sys_barcode[] = $sbvalue['full_barcode'];
            }
        }

        if (isset($barcode) && is_array($barcode) && count($barcode) > 0) {
            foreach ($barcode as $bkey => $bvalue) {
                if (isset($barcode_generated[$bkey]) && $barcode_generated[$bkey] == 2) {
                    $autog_barcode[] = $bvalue;
                } else {
                    $manual_barcode[] = $bvalue;
                }
            }
        }

        /*
         * Case 2: Check if Barcode is same as order generated barcode
         */
        $unmacth_bar = array_diff($autog_barcode, $sys_barcode);
        if (isset($unmacth_bar) && is_array($unmacth_bar) && count($unmacth_bar) > 0) {
            $unmatch_barcode = implode(",", $unmacth_bar);
            $error = "Barcode " . $unmatch_barcode . " Not Matched.";
            die('<div class="alert alert-warning alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
								</button>
								<strong>' . $error . '</strong>
							</div>');
            die("");
        } else {
            /*
             * Check manual enter barcode already exist or not
             */
            if (isset($manual_barcode) && is_array($manual_barcode) && count($manual_barcode) > 0) {
                $manual_bar = "'" . implode("','", $manual_barcode) . "'";
                if ($manual_bar != '') {
                    $barcode_exist = select("id,full_barcode", "product_barcode", "status=1 AND product_id!=0 AND full_barcode IN(" . $manual_bar . ") AND order_request_id!='" . $order_id . "' AND full_barcode!=0");
                }
            }

            if (isset($barcode_exist) && is_array($barcode_exist) && count($barcode_exist) > 0) {
                foreach ($barcode_exist as $mbkey => $mbvalue) {
                    $manual_name[] = $mbvalue['full_barcode'];
                    die('<div class="alert alert-warning alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
								</button>
								<strong>Barcode ' . implode(",", $manual_name) . ' Already Exist.</strong>
							</div>');
                }
            } else {

                /*
                 * map barcode
                 */
                if (isset($barcode) && is_array($barcode) && count($barcode) > 0) {
                    foreach ($barcode as $bkey => $bvalue) {
                        $barcode_pdt_id = isset($product_id[$bkey]) ? $product_id[$bkey] : 0;
                        if (isset($barcode_generated[$bkey]) && $barcode_generated[$bkey] == 2) {
                            $updateq = "UPDATE product_barcode SET product_id='" . $barcode_pdt_id . "',is_verified=1,product_detail='" . $product_detail[$bkey] . "' WHERE order_request_id='" . $order_id . "'"
                                    . " AND full_barcode='" . $bvalue . "'";
                            //echo "<br>" . $updateq;
                            $DB->query($updateq);
                        } else {
                            if (isset($barcode_id[$bkey]) && $barcode_id[$bkey] > 0) {
                                $updateq = "UPDATE product_barcode SET full_barcode='" . $bvalue . "',modified_date='" . date('Y-m-d H:i:s') . "',"
                                        . "modified_by='," . $strAdminID . "',is_verified=1,product_detail='" . $product_detail[$bkey] . "' WHERE id='" . $barcode_id[$bkey] . "'";
                                $DB->query($updateq);
                            } else {
                                $insertq = "INSERT INTO product_barcode(is_verified,order_request_id,product_id,full_barcode,prefix,store_id,is_barcode_generated,"
                                        . "created_date,created_by,product_detail) "
                                        . " VALUES('1','" . $order_id . "','" . $barcode_pdt_id . "','" . $bvalue . "','NS','" . $store_id . "',"
                                        . "'2','" . date('Y-m-d H:i:s') . "','" . $strAdminID . "','" . $product_detail[$bkey] . "')";
                                $DB->query($insertq);
                            }
                        }

                        if ($product_detail[$bkey] == '') {
                            if (isset($product_stock_data[$barcode_pdt_id])) {
                                $product_stock_data[$barcode_pdt_id] += 1;
                            } else {
                                $product_stock_data[$barcode_pdt_id] = 1;
                            }
                        }
                    }
                }

                $update_verify = "UPDATE order_request SET is_verified='" . $confirm_order . "',verified_by='" . $strAdminID . "',verified_date='" . date('Y-m-d H:i:s') . "'"
                        . " WHERE id='" . $order_id . "'";
                $DB->query($update_verify);

                /*
                 * add inevntory
                 */
                if ($confirm_order == 1) {
                    $order_data = select("*", "order_request", "id='" . $order_id . "'");
                    if (isset($order_data) && is_array($order_data) && count($order_data) > 0) {
                        $product_qty_data = select("*", "order_request_product", "status=1 AND order_request_id='" . $order_id . "'");
                        if (isset($product_qty_data) && is_array($product_qty_data) && count($product_qty_data) > 0) {
                            foreach ($product_qty_data as $pkey => $pvalue) {

                                $product_qty_add = isset($product_stock_data[$pvalue['product_id']]) ? $product_stock_data[$pvalue['product_id']] : 0;
                                /*
                                 * Check if product inventory already exist or not
                                 */
                                $product_inv_exist = select("*", "product_inventory", "status=1 AND ProductID='" . $pvalue['product_id'] . "' AND StoreID='" . $order_data[0]['store_id'] . "'");
                                if (isset($product_inv_exist) && is_array($product_inv_exist) && count($product_inv_exist) > 0) {
                                    $new_total = $product_inv_exist[0]['total_qty'] + $product_qty_add;
                                    $available_total = $product_inv_exist[0]['Stock'] + $product_qty_add;
                                    $updateinv = "UPDATE product_inventory SET total_qty='" . $new_total . "',Stock='" . $available_total . "',"
                                            . "modified_date='" . date('Y-m-d H:i:s') . "',modified_by='" . $strAdminID . "' "
                                            . " WHERE id='" . $product_inv_exist[0]['id'] . "'";
                                    $DB->query($updateinv);
                                } else {
                                    /*
                                     * Insert New Inventory
                                     */
                                    $insertinv = "INSERT INTO product_inventory(StoreID,ProductID,total_qty,Stock,created_date,created_by)"
                                            . " VALUES('" . $order_data[0]['store_id'] . "','" . $pvalue['product_id'] . "','" . $product_qty_add . "',"
                                            . "'" . $product_qty_add . "','" . date('Y-m-d H:i:s') . "','" . $strAdminID . "')";
                                    $DB->query($insertinv);
                                }
                            }
                        }
                    }

                    /*
                     * Send Order Confirmation mail 
                     */
                    require_once("order_helper.php");
                    if (isset($order_id) && is_numeric($order_id)) {
                        $body_data = get_detail_data($order_id, 2);
                        if (isset($body_data) && is_array($body_data) && count($body_data) > 0) {
                            $message = file_get_contents('verify_order_template.php');
                            $order_date = (isset($body_data['order_data'][0]['created_date']) && $body_data['order_data'][0]['created_date'] != '0000-00-00 00:00:00' ? date('d M, Y h:i a', strtotime($body_data['order_data'][0]['created_date'])) : '');
                            $order_created = (isset($body_data['created_data'][0]['AdminFullName']) ? $body_data['created_data'][0]['AdminFullName'] : '');

                            /*
                             * Get Product Data
                             */
                            $product_append = '';
                            $counter = 1;
                            if (isset($body_data['order_product']) && is_array($body_data['order_product']) && count($body_data['order_product']) > 0) {

                                $pdt_barcode_data = select("*", "product_barcode", "status=1 AND order_request_id='" . $order_id . "'");

                                if (isset($pdt_barcode_data) && is_array($pdt_barcode_data) && count($pdt_barcode_data) > 0) {
                                    foreach ($pdt_barcode_data as $pkey => $pvalue) {
                                        if (isset($pdt_detail[$pvalue['product_id']][$pvalue['product_detail']])) {
                                            $pdt_detail[$pvalue['product_id']][$pvalue['product_detail']] += 1;
                                        } else {
                                            $pdt_detail[$pvalue['product_id']][$pvalue['product_detail']] = 1;
                                        }
                                    }
                                }


                                foreach ($body_data['order_product'] as $okey => $ovalue) {

                                    $product_append .= '<tr style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;" bgcolor="#ffffff" width="500" align="center">';
                                    $product_append .= '<td>' . $counter . '</td>';
                                    $product_append .= '<td>' . $body_data['pdt_name_data'][$ovalue['product_id']]['name'] . '</td>';
                                    $product_append .= '<td>' . $body_data['pdt_name_data'][$ovalue['product_id']]['code'] . '</td>';
                                    $product_append .= '<td>' . $ovalue['quantity'] . '</td>';
                                    $product_append .= '<td>' . (isset($pdt_detail[$ovalue['product_id']][1]) ? $pdt_detail[$ovalue['product_id']][1] : '0') . '</td>';
                                    $product_append .= '<td>' . (isset($pdt_detail[$ovalue['product_id']][2]) ? $pdt_detail[$ovalue['product_id']][2] : '0') . '</td>';
                                    $product_append .= '<td>' . (isset($pdt_detail[$ovalue['product_id']][3]) ? $pdt_detail[$ovalue['product_id']][3] : '0') . '</td>';
                                    $product_append .= '<td>' . (isset($pdt_detail[$ovalue['product_id']][0]) ? $pdt_detail[$ovalue['product_id']][0] : '0') . '</td>';
                                    $product_append .= '</tr>';
                                    $counter++;
                                }
                            }
                            $vars = array('{STORE_NAME}', '{STORE_ADDRESS}', '{ORDER_DATE}', '{ORDER_CREATED}', '{PRODUCT_SUMMARY}'); //Replace varaibles
                            $values = array($body_data['store_data'][0]['StoreName'], $body_data['store_data'][0]['StoreOfficialAddress'], $order_date, $order_created, $product_append);

                            $message = str_replace($vars, $values, $message);
                            $report_data = select("*", 'report_config', 'status =1 AND report_name="order_confirmation_mail"');
                            if (isset($report_data) && is_array($report_data) && count($report_data) > 0) {
                                foreach ($report_data as $rkey => $evalue) {
                                    $strTo = $evalue['email_id'];
                                    $strFrom = "invoice@nailspaexperience.com";
                                    $strSubject = "Order Verified and Confirmation";
                                    $strbody1 = $message;
                                    $headers = "From: $strFrom\r\n";
                                    $headers .= "Content-type: text/html\r\n";
                                    $strBodysa = AntiFilter1($strbody1);

                                    // Mail sending 
                                    $retval = mail($strTo, $strSubject, $strBodysa, $headers);
                                }
                            }
                        }
                    }
                }
                $_SESSION['message'] = "Order Verified Successfully.";
                die('<div class="alert alert-warning alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
								</button>
								<strong>Order Verification Added Successfully.</strong>
							</div>');
            }
        }
    }
    $DB->close();
    header('Location:manage_order_list.php');
}

require_once("order_helper.php");


if (isset($_GET['id']) && is_numeric($_GET['id'])) {
    $order_data = get_detail_data($_GET['id'], '2');
    $order_product = isset($order_data['order_product']) ? $order_data['order_product'] : array();
    $pdt_name_data = isset($order_data['pdt_name_data']) ? $order_data['pdt_name_data'] : array();

    $order_id = isset($order_data['order_data'][0]['id']) ? $order_data['order_data'][0]['id'] : 0;
    if ($order_id > 0) {
        $product_barcode_data = select("*", "product_barcode", "status=1 AND order_request_id='" . $order_id . "'");
    }
}
if (isset($order_data['order_data'][0]['is_verified']) && $order_data['order_data'][0]['is_verified'] == 1 && !isset($_GET['verify'])) {
    header('Location:manage_order_list.php');
}
?>


<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>

    <body>
        <div id="sb-site">   
            <?php require_once("incOpenLayout.fya"); ?>	

            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>
                <?php require_once("incLeftMenu.fya"); ?>


                <div id="page-content-wrapper">
                    <div id="page-content">
                        <?php
                        require_once("incHeader.fya");
                        ?>
                        <script type="text/javascript" src="assets/scanner/jquery-code-scanner.js"></script>
                        <script>
                            $(function () {
                                //$('.barcode').codeScanner();
                                $('.order_verify_form').codeScanner({
                                    onScan: function ($element, code) {
                                        $element.find('input.barcode[value=""]').first().val(code);
                                    }
                                });

                                $('.barcode').keyup(function (event) {
                                    var keycode = (event.keyCode ? event.keyCode : event.which);
                                    if (keycode == '13') {
                                        textboxes = $("input.barcode");
                                        currentBoxNumber = textboxes.index(this);
                                        if (textboxes[currentBoxNumber + 1] != null) {
                                            nextBox = textboxes[currentBoxNumber + 1];
                                            nextBox.focus();
                                            nextBox.select();
                                        }
                                        event.preventDefault();
                                        return false;
                                    }
                                });

                                $('.submit').click(function () {
                                    $(".confirm_order").val('2');
                                    $(".barcode").removeAttr("required");
                                    $(".order_verify_form").submit();
                                });


                            });
                        </script>
                        <div class="panel">
                            <div class="panel-body">

                                <h3 class="title-hero">Verify Order</h3>

                                <?php
                                if (isset($order_data) && is_array($order_data) && count($order_data) > 0) {
                                    ?>
                                    <form role="form" enctype="multipart/form-data" class="form-horizontal bordered-row order_verify_form" onSubmit="proceed_formsubmit('.order_verify_form', 'verify_order.php', '.result_message', '', '', '', '');
                                            return false;">
                                        <span class="result_message">&nbsp; <br>
                                        </span>
                                        <input type="hidden" name="confirm_order" value="1" class="confirm_order"/>
                                        <input type="hidden" name="order_id" value="<?php echo $_GET['id']; ?>"/>
                                        <input type="hidden" name="store_id" value="<?php echo $order_data['order_data'][0]['store_id'] ? $order_data['order_data'][0]['store_id'] : 0; ?>"/>

                                        <?php if (!isset($_GET['verify'])) { ?>
                                            <div class="form-group">
                                                <label class="col-sm-9 control-label"></label>
                                                <input type="submit" class="btn ra-100 btn-info confirm_submit" value="Confirm & Submit">
                                                <input type="button" class="btn ra-100 btn-primary submit" value="Submit">
                                            </div>
                                        <?php } ?>
                                        <table class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Sr. No.</th>
                                                    <th>Product Name</th>
                                                    <th>Product detail</th>
                                                    <th>Barcode</th>
                                                </tr>
                                            </thead>
                                            <?php
                                            $counter = 1;
                                            if (isset($product_barcode_data) && is_array($product_barcode_data) && count($product_barcode_data) > 0) {

                                                foreach ($product_barcode_data as $okey => $ovalue) {
                                                    if (isset($pdt_name_data[$ovalue['product_id']])) {
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $counter; ?></td>
                                                            <td><?php echo $pdt_name_data[$ovalue['product_id']]['name']; ?>
                                                                <input type="hidden" name="barcode_id[<?php echo $counter; ?>]" value="<?php echo $ovalue['id']; ?>"/>
                                                                <input type="hidden" name="product_id[<?php echo $counter; ?>]" value="<?php echo $ovalue['product_id']; ?>"/>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                if (isset($_GET['verify'])) {
                                                                    $select_style = "disabled='disabled'";
                                                                    if ($ovalue['product_detail'] == 1) {
                                                                        echo "Product Defected";
                                                                    } else if ($ovalue['product_detail'] == 2) {
                                                                        echo "Not Available";
                                                                    } else if ($ovalue['product_detail'] == 3) {
                                                                        echo "Goods Return";
                                                                    }
                                                                } else {
                                                                    ?>
                                                                    <select name="product_detail[<?php echo $counter; ?>]">
                                                                        <option value="">Select</option>
                                                                        <option value="1" <?php echo $ovalue['product_detail'] == 1 ? 'selected' : ''; ?>>Product Defected</option>
                                                                        <option value="2" <?php echo $ovalue['product_detail'] == 2 ? 'selected' : ''; ?>>Not Available</option>
                                                                        <option value="3" <?php echo $ovalue['product_detail'] == 3 ? 'selected' : ''; ?>>Goods Return</option>
                                                                    </select>
                                                                <?php } ?>
                                                            </td>
                                                            <td>
                                                                <?php if (isset($_GET['verify'])) { ?>
                                                                    <p><?php echo $ovalue['full_barcode']; ?></p>
                                                                <?php } else { ?> 
                                                                    <input type="text" class="form-control barcode" name="barcode[<?php echo $counter; ?>]" required="required" id="barcode_<?php echo $counter ?>" value="<?php echo $ovalue['full_barcode']; ?>"/>
                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $counter++;
                                                    }
                                                }
                                            } else if (isset($order_product) && is_array($order_product) && count($order_product) > 0) {

                                                foreach ($order_product as $okey => $ovalue) {

                                                    if (isset($pdt_name_data[$ovalue['product_id']])) {


                                                        $qty = $ovalue['quantity'];
                                                        for ($i = 0; $i < $qty; $i++) {
                                                            ?>
                                                            <tr>
                                                                <td><?php echo $counter; ?></td>

                                                                <td><?php echo $pdt_name_data[$ovalue['product_id']]['name']; ?>
                                                                    <input type="hidden" name="product_id[<?php echo $counter; ?>]" value="<?php echo $ovalue['product_id']; ?>"/>
                                                                    <input type="hidden" name="barcode_generated[<?php echo $counter; ?>]" value="<?php echo $ovalue['is_barcode_generated']; ?>"/>
                                                                </td>
                                                                <td>

                                                                    <?php
                                                                    if (isset($_GET['verify'])) {
                                                                        $select_style = "disabled='disabled'";
                                                                        echo "";
                                                                    } else {
                                                                        ?>
                                                                        <select name="product_detail[<?php echo $counter; ?>]">
                                                                            <option value="">Select</option>
                                                                            <option value="1">Product Defected</option>
                                                                            <option value="2">Not Available</option>
                                                                            <option value="3">Goods Return</option>
                                                                        </select>
                                                                    <?php } ?>
                                                                </td>
                                                                <td>
                                                                    <?php if (isset($_GET['verify'])) { ?>
                                                                        <p><?php echo $ovalue['full_barcode']; ?></p>
                                                                    <?php } else { ?>
                                                                        <input type="text" class="form-control barcode" name="barcode[<?php echo $counter; ?>]" required="required" id="barcode_<?php echo $counter ?>" value="<?php echo $ovalue['full_barcode']; ?>"/>
                                                                    <?php } ?>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                            $counter++;
                                                        }
                                                    }
                                                }
                                            }
                                            ?>
                                        </table>
                                        <?php if (!isset($_GET['verify'])) { ?>
                                            <div class="form-group">
                                                <label class="col-sm-9 control-label"></label>
                                                <input type="submit" class="btn ra-100 btn-info confirm_submit" value="Confirm & Submit">
                                                <input type="button" class="btn ra-100 btn-primary submit" value="Submit">
                                            </div>
                                        <?php } ?>
                                    </form>
                                    <?php
                                } else {
                                    ?>
                                    <h3>No Product Found...</h3>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php require_once 'incFooter.fya'; ?>
            </div>
        </div>


    </body>
</html>