<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php

$DB = Connect();
date_default_timezone_set('Asia/Kolkata');
$date = date('Y-m-d');

/*
 * get Todays Expiry Date membership
 */
$today = date('Y-m-d');

if (isset($_GET['type']) && $_GET['type'] == 1) {
    /*
     * One Month Before Expiry
     */
    $membership_rem_date = date("Y-m-d", strtotime("+1 months"));
} else if (isset($_GET['type']) && $_GET['type'] == 2) {
    /*
     * Todays Expiry
     */
    $membership_rem_date = $today;
}

if (isset($membership_rem_date) && $membership_rem_date != '') {
    $sqlservice = "SELECT * from tblCustomerMemberShip where MembershipStatus='1' AND EndDay='" . $membership_rem_date . "'";
    $RSservice = $DB->query($sqlservice);
    if ($RSservice->num_rows > 0) {
        while ($rowservice = $RSservice->fetch_assoc()) {
            $today_expiry[$rowservice['CustomerID']] = $rowservice;
            $expiry_cust[$rowservice['CustomerID']] = $rowservice['CustomerID'];
        }
    }
}


/*
 * Get Customer Name
 */
if (isset($expiry_cust) && is_array($expiry_cust) && count($expiry_cust) > 0) {
    $expiry_cust_ids = implode(",", $expiry_cust);
    if ($expiry_cust_ids != '') {
        $custqry = "SELECT * from tblCustomers where CustomerID IN(" . $expiry_cust_ids . ")";
        $custqry_exe = $DB->query($custqry);
        if ($custqry_exe->num_rows > 0) {
            while ($custqry_data = $custqry_exe->fetch_assoc()) {
                $expiry_cust[$custqry_data['CustomerID']] = $custqry_data;
            }
        }
    }
}


/*
 * Get membership Name
 */
$membership_data = select("*", "tblMembership", "MembershipID > 0");
if (isset($membership_data) && is_array($membership_data) && count($membership_data) > 0) {
    foreach ($membership_data as $mkey => $mvalue) {
        $membership_res[$mvalue['MembershipID']] = $mvalue;
    }
}

if (isset($today_expiry) && is_array($today_expiry) && count($today_expiry) > 0) {
    /*
     * Get Email Template 
     */
    if (isset($_GET['type']) && $_GET['type'] == 1) {
        $expiry_template = select("*", "tblnotification_msg", "status = 1 AND message_type='one_mon_membership_renew_reminder'");
    } else if (isset($_GET['type']) && $_GET['type'] == 2) {
        $expiry_template = select("*", "tblnotification_msg", "status = 1 AND message_type='membership_expired_mail'");
    }
    if (isset($expiry_template) && is_array($expiry_template) && count($expiry_template) > 0) {
        if (isset($expiry_template[0]['message']) && $expiry_template[0]['message'] != '') {
            foreach ($today_expiry as $tokey => $tovalue) {
                $last_email_id = 0;
                if (isset($expiry_cust[$tovalue['CustomerID']]) && filter_var($expiry_cust[$tovalue['CustomerID']]['CustomerEmailID'], FILTER_VALIDATE_EMAIL)) {
                    $strTo = $expiry_cust[$tovalue['CustomerID']]['CustomerEmailID'];
                    //$strTo = 'kanchan@orggen.com';

                    $email_content = $expiry_template[0]['message'];
                    $strSubject = isset($expiry_template[0]['subject']) && $expiry_template[0]['subject'] != '' ? $expiry_template[0]['subject'] : 'Membership Expiry Mail';

                    $customer_name = $expiry_cust[$tovalue['CustomerID']]['CustomerFullName'];
                    $membership_name = isset($membership_res[$tovalue['MembershipID']]) ? $membership_res[$tovalue['MembershipID']]['MembershipName'] : '';

                    $member_discount = 0;
                    $member_amount = 0;
                    if (isset($membership_res[$tovalue['MembershipID']]) && $membership_res[$tovalue['MembershipID']]['DiscountType'] == 0) {
                        $member_discount = $membership_res[$tovalue['MembershipID']]['Discount'] . '%';
                        $member_amount = $membership_res[$tovalue['MembershipID']]['Cost'];
                    } else if (isset($membership_res[$tovalue['MembershipID']]) && $membership_res[$tovalue['MembershipID']]['DiscountType'] == 1) {
                        $member_discount = $membership_res[$tovalue['MembershipID']]['Discount'] . 'Rs.';
                        $member_amount = $membership_res[$tovalue['MembershipID']]['Cost'];
                    }
                    $email_content = isset($customer_name) ? str_replace('{member_name}', ucwords($customer_name), $email_content) : $email_content;
                    $email_content = isset($membership_name) ? str_replace('{membership_name}', ucwords($membership_name), $email_content) : $email_content;

                    $email_content = isset($member_amount) ? str_replace('{membership_amount}', $member_amount, $email_content) : $email_content;
                    $email_content = isset($member_discount) ? str_replace('{membership_discount}', $member_discount, $email_content) : $email_content;

                    $email_content = isset($tovalue['StartDay']) && $tovalue['StartDay'] != '0000-00-00' ? str_replace('{start_date}', date('d/m/Y', strtotime($tovalue['StartDay'])), $email_content) : $email_content;
                    $email_content = isset($tovalue['EndDay']) && $tovalue['EndDay'] != '0000-00-00' ? str_replace('{end_date}', date('d/m/Y', strtotime($tovalue['EndDay'])), $email_content) : $email_content;

                    $strFrom = "Membership@nailspaexperience.com";
                    $strbody1 = $email_content;
                    $headers = "From: $strFrom\r\n";
                    //$headers .= 'Cc: kanchan@orggen.com,pradhyumna@orggen.com' . "\r\n";
                    //$headers .= 'Cc: operations@nailspaexperience.com,noor@nailspaexperience.com' . "\r\n";
                    $headers .= "Content-type: text/html\r\n";
                    $strBodysa = $strbody1;

                    /*
                     * Insert Into message table
                     */
                    $col_val = array('ToEmail', 'FromEmail', 'Subject', 'Body', 'DateTime', 'Status', 'created_date', 'created_by', 'description', 'CustomerID');
                    $user['ToEmail'] = "'" . $strTo . "'";
                    $user['FromEmail'] = "'" . $strFrom . "'";
                    $user['Subject'] = "'" . $strSubject . "'";
                    $user['Body'] = "'" . $strBodysa . "'";
                    $user['DateTime'] = "'" . date('Y-m-d H:i:s') . "'";
                    $user['Status'] = "'0'";
                    $user['created_date'] = "'" . date('Y-m-d H:i:s') . "'";
                    $user['created_by'] = 0;
                    $user['description'] = "'Membership Today Expired Email Customer ID =" . $tovalue['CustomerID'] . "'";
                    $user['CustomerID'] = "'" . $tovalue['CustomerID'] . "'";
                    $field_values = implode(',', $col_val);
                    $data_values = implode(',', $user);
                    $insert_sql = "INSERT into tblEmailMessages (" . $field_values . ") VALUES(" . $data_values . ")";
                    $DB->query($insert_sql);
                    $last_email_id = $DB->insert_id;
                    unset($user);

                    $retval = mail($strTo, $strSubject, $strBodysa, $headers);
                    if ($retval == true) {
                        $update_qry = "UPDATE tblEmailMessages SET Status='1',DateOfSending='" . date('Y-m-d H:i:s') . "'"
                                . " WHERE ID='" . $last_email_id . "'";
                        $DB->query($update_qry);
                    }
                }
            }
        }
    }
}

$DB->close();
?>