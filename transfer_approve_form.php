<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
$strPageTitle = "Customer Details | NailSpa";
$strDisplayTitle = "Customer Details for NailSpa";
$strMenuID = "10";
$strMyTable = "tblAppointments";
$strMyTableID = "AppointmentID";
$strMyField = "AppointmentDate";
$strMyActionPage = "DisplayNonCustomerCount.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <?php require_once("incChart-SalonDashboard.fya"); ?>
        <?php /* <script type="text/javascript" src="assets/widgets/modal/modal.js"></script> */ ?>
        <link rel="stylesheet" type="text/css" href="assets/widgets/chosen/chosen.css">
        <script type="text/javascript" src="assets/widgets/chosen/chosen.js"></script>
        <script type="text/javascript" src="assets/widgets/chosen/chosen-demo.js"></script>
        <script>
            function proceed_transfer(transfer_id) {
                if (confirm('Are you sure you want to Aprove Request?')) {
                    $.ajax({
                        url: "insert_transfer_req.php",
                        type: 'post',
                        data: {"transfer_id": transfer_id, "type": '2'},
                        success: function (response) {
                            if (response == 1) {
                                $msg = "Request Approved Successfully!";
                                alert($msg);
                                window.location.reload(true);
                            } else {
                                $msg = "Failed To Approve!";
                                alert($msg);
                                window.location.reload(true);
                            }
                        }
                    });
                }
            }

            function proceed_transfer_reject(transfer_id) {
                if (confirm('Are you sure you want to Reject Request?')) {
                    $.ajax({
                        url: "insert_transfer_req.php",
                        type: 'post',
                        data: {"transfer_id": transfer_id, "type": '3'},
                        success: function (response) {
                            if (response == 1) {
                                $msg = "Request Rejected Successfully!";
                                alert($msg);
                                window.location.reload(true);
                            } else {
                                $msg = "Failed To Reject!";
                                alert($msg);
                                window.location.reload(true);
                            }
                        }
                    });
                }
            }
        </script>
    </head>

    <body>

        <div id="sb-site">


            <?php // require_once("incOpenLayout.fya");   ?>
            <!----------commented by gandhali 1/9/18---------------->


            <?php require_once("incLoader.fya"); ?>


            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <script type="text/javascript" src="assets/widgets/datatable/date-euro.js"></script>
                <script type="text/javascript">
            /* Datatables responsive */
            $(document).ready(function () {
                $('#datatable-responsive-date').DataTable({
                    "order": [[0, "desc"]],
                    columnDefs: [{type: 'date-euro', targets: 0}]
                });
            });
            $(document).ready(function () {
                $('.dataTables_filter input').attr("placeholder", "Search...");
            });
                </script>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>



                        <div class="panel">

                            <div class="panel-body">
                                <div class="example-box-wrapper">

                                    <div id="normal-tabs-1">
                                        <span class="form_result">&nbsp; <br>
                                        </span>

                                        <h4 class="title-hero"><center><?php echo 'Employee Transfer : Approval Request | NailSpa' ?></center></h4>

                                        <?php
                                        $request_data = select("*", "emp_transfer_request", "status=1 AND from_store='" . $strStore . "'");
                                        $store = select("*", "tblStores", "StoreID > 0");
                                        if (isset($store) && is_array($store) && count($store) > 0) {
                                            foreach ($store as $skey => $svalue) {
                                                $store_data[$svalue['StoreID']] = $svalue;
                                            }
                                        }

                                        $emp = select("*", "tblEmployees", "EID > 0");
                                        if (isset($emp) && is_array($emp) && count($emp) > 0) {
                                            foreach ($emp as $ekey => $evalue) {
                                                $emp_data[$evalue['EID']] = $evalue;
                                            }
                                        }

                                        $admin = select("*", "tblAdmin", "AdminID > 0");
                                        if (isset($admin) && is_array($admin) && count($admin) > 0) {
                                            foreach ($admin as $akey => $avalue) {
                                                $admin_data[$avalue['AdminID']] = $avalue;
                                            }
                                        }
                                        ?>

                                        <table id="datatable-responsive-date" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Request date</th>
                                                    <th>Request By</th>
                                                    <th>Transfer Store</th>
                                                    <th>Employee</th>
                                                    <th>Status</th>
                                                    <th>Approved date</th>
                                                    <th>Approved By</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Request date</th>
                                                    <th>Request By</th>
                                                    <th>Transfer Store</th>
                                                    <th>Employee</th>
                                                    <th>Status</th>
                                                    <th>Approved date</th>
                                                    <th>Approved By</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                <?php
                                                if (isset($request_data) && is_array($request_data) && count($request_data) > 0) {
                                                    foreach ($request_data as $rkey => $rvalue) {
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                <?php echo isset($rvalue['request_date']) && $rvalue['request_date'] != '0000-00-00 00:00:00' ? date('d/m/Y h:i a', strtotime($rvalue['request_date'])) : ''; ?> 

                                                            </td>
                                                            <td>
                                                                <?php echo isset($admin_data[$rvalue['request_by']]) ? $admin_data[$rvalue['request_by']]['AdminFullName'] : ''; ?>
                                                            </td>
                                                            <td><?php echo isset($store_data[$rvalue['to_store']]) ? $store_data[$rvalue['to_store']]['StoreName'] : ''; ?></td>
                                                            <td><?php echo isset($emp_data[$rvalue['emp_id']]) ? $emp_data[$rvalue['emp_id']]['EmployeeName'] : ''; ?></td>
                                                            <td>
                                                                <?php
                                                                if ($rvalue['request_status'] == '1') {
                                                                    echo "<span style='color:blue;'>Pending</span>";
                                                                } else if ($rvalue['request_status'] == '2') {
                                                                    echo "<span class='text-success'>Approved</span>";
                                                                } else if ($rvalue['request_status'] == '3') {
                                                                    echo "<span class='text-danger'>Rejected</span>";
                                                                }
                                                                ?>
                                                            </td>

                                                            <td>
                                                                <?php if ($rvalue['request_status'] != '1') { ?>
                                                                    <?php echo isset($rvalue['approved_date']) && $rvalue['approved_date'] != '0000-00-00 00:00:00' ? date('d/m/Y h:i a', strtotime($rvalue['approved_date'])) : ''; ?>
                                                                <?php } ?>
                                                            </td>
                                                            <td>
                                                                <?php if ($rvalue['request_status'] != '1') { ?>
                                                                    <?php echo isset($admin_data[$rvalue['approved_by']]) ? $admin_data[$rvalue['approved_by']]['AdminFullName'] : ''; ?>
                                                                <?php } ?>
                                                            </td>
                                                            <td>
                                                                <?php if ($rvalue['request_status'] == '1') { ?>
                                                                    <a class="btn btn-link" href="#" id="transfer_confirm" onclick="proceed_transfer('<?php echo $rvalue['id']; ?>');">Approve</a>
                                                                    <a class="btn btn-link font-red" href="#" id="transfer_reject" onclick="proceed_transfer_reject('<?php echo $rvalue['id']; ?>');">Reject</a> &nbsp;&nbsp;&nbsp;
                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php require_once 'incFooter.fya'; ?>
        </div>


    </body>
</html>
