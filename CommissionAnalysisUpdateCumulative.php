<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Employee Commission Report | Nailspa";
$strDisplayTitle = "Employee Commission of Nailspa Experience";
$strMenuID = "2";
$strMyTable = "";
$strMyTableID = "";
$strMyField = "";
$strMyActionPage = "CommissionAnalysisUpdateCumulative.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights



if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $strStep = Filter($_POST["step"]);

    if ($strStep == "add") {

    }

    if ($strStep == "edit") {

    }
}
?>


<?php
if (isset($_GET["toandfrom"])) {
    $strtoandfrom = $_GET["toandfrom"];
    $arraytofrom = explode("-", $strtoandfrom);

    $from = $arraytofrom[0];
    $datetime = new DateTime($from);
    $getfrom = $datetime->format('Y-m-d');


    $to = $arraytofrom[1];
    $datetime = new DateTime($to);
    $getto = $datetime->format('Y-m-d');

    if (!IsNull($from)) {
        $sqlTempfrom = " and Date(tblInvoiceDetails.OfferDiscountDateTime)>=Date('" . $getfrom . "')";
    }

    if (!IsNull($to)) {
        $sqlTempto = " and Date(tblInvoiceDetails.OfferDiscountDateTime)<=Date('" . $getto . "')";
    }
}
?>


<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>

        <script type="text/javascript" src="assets/widgets/datepicker/datepicker.js"></script>
        <script type="text/javascript">
            /* Datepicker bootstrap */

            $(function () {
                "use strict";
                $('.bootstrap-datepicker').bsdatepicker({
                    format: 'mm-dd-yyyy'
                });
            });
        </script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker-demo.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/moment.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
    </head>
    <script>
        function printDiv(divName)
        {

            $("#heading").show();
            var divToPrint = document.getElementById("printdata");
            var htmlToPrint = '' +
                '<style type="text/css">' +
                'table th, table td {' +
                'border:1px solid #000;' +
                'padding;0.5em;' +
                '}' +
                '</style>';
            htmlToPrint += divToPrint.outerHTML;
            newWin = window.open("");
            newWin.document.write(htmlToPrint);
            newWin.print();
            newWin.close();
            // var printContents = document.getElementById(divName);
            // var originalContents = document.body.innerHTML;

            // document.body.innerHTML = printContents;

            // window.print();

            // document.body.innerHTML = originalContents;
        }

    </script>
    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");      ?>
            <!----------commented by gandhali 5/9/18---------------->


<?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>


                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                        </div>
                        <style type="text/css">
                            @media print {
                                body * {
                                    visibility: hidden;
                                }
                                #printarea * {
                                    visibility: visible;
                                }
                                #printarea{
                                    position: absolute;
                                    left: 0;
                                    top: 0;
                                }
                            }
                        </style>
                        <?php
                        if (!isset($_GET["uid"])) {
                            ?>

                        <div class="panel">
                            <div class="panel">
                                <div class="panel-body">


                                    <div class="example-box-wrapper">
                                        <div class="tabs">

                                            <div id="normal-tabs-1">

                                                <span class="form_result">&nbsp; <br>
                                                </span>

                                                <div class="panel-body">
                                                    <h3 class="title-hero">Employee Commission Cumulative</h3>

                                                    <form method="get" class="form-horizontal bordered-row" role="form">

                                                        <div class="form-group"><label for="" class="col-sm-4 control-label">Select Date Range</label>
                                                            <div class="col-sm-4">
                                                                <div class="input-prepend input-group">
                                                                    <span class="add-on input-group-addon">
                                                                        <i class="glyph-icon icon-calendar"></i>
                                                                    </span>
                                                                    <input type="text" name="toandfrom" id="daterangepicker-example" class="form-control" value="<?= $strtoandfrom ?>">
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Select Store</label>
                                                            <div class="col-sm-4">
                                                                <select name="Store" class="form-control" >

                                                                        <?php
                                                                        if ($strStore == '0') {
                                                                            $strStatement = "";
                                                                        } else {
                                                                            $strStatement = " and StoreID='$strStore'";
                                                                        }
                                                                        $selp = select("*", "tblStores", "Status='0' $strStatement");
                                                                        foreach ($selp as $val) {
                                                                            $strStoreName = $val["StoreName"];
                                                                            $strStoreID = $val["StoreID"];
                                                                            $store = $_GET["Store"];
                                                                            if ($store == $strStoreID) {
                                                                                ?>
                                                                    <option  selected value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <option value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Select Percentage</label>
                                                            <div class="col-sm-4">
                                                                <?php
                                                                $per = $_GET["per"];
                                                                ?>
                                                                <select name="per" class="form-control">
                                                                    <option value="0" <?php if ($per == '0') { ?> selected <?php } ?>>Without Percentage</option>
                                                                    <option value="1" <?php if ($per == '1') { ?> selected <?php } ?>>Percentage</option>
                                                                </select>
                                                            </div>
                                                        </div>


                                                        <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                            <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                                            &nbsp;&nbsp;&nbsp;
                                                            <a class="btn btn-link" href="CommissionAnalysisUpdateCumulative.php">Clear All Filter</a> &nbsp;&nbsp;&nbsp;
                                                            <?php
                                                            $datedrom = $_GET["toandfrom"];
                                                            if ($datedrom != "") {
                                                                ?>
                                                            <button onclick="printDiv('printarea')" class="btn btn-blue-alt">Print<div class="ripple-wrapper"></div></button>
                                                            <?php
                                                        }
                                                        ?>
                                                        </div>

                                                    </form>

                                                    <div id="printdata">
                                                        <h2 class="title-hero" id="heading" style="display:none"><center>Report Employee Commission Cumulative</center></h2>
                                                        <div class="example-box-wrapper">
                                                            <?php
                                                            $EID = $_GET["Store"];
                                                            $datedrom = $_GET["toandfrom"];
                                                            if ($datedrom != "") {
                                                                $EID = $_GET["Store"];
                                                                if ($EID == '0') {
                                                                    $emp_id = 'All';
                                                                } else {
                                                                    $selpT = select("*", "tblStores", "StoreID='" . $EID . "'");
                                                                    $StoreName = $selpT[0]['StoreName'];
                                                                    $emp_id = $StoreName;
                                                                }
                                                                ?>
                                                            <br>

                                                            <span>
                                                                <h3 >Date Range selected : FROM - <?= $getfrom ?> / TO - <?= $getto ?> / Store - <?= $emp_id ?></h3>

                                                            </span>

                                                            <table id="printdata" class="table table-striped table-bordered responsive no-wrap printdata" cellspacing="0" width="100%">
                                                                <?php
                                                                $per = $_GET["per"];
                                                                ?>
                                                                <thead>
                                                                    <tr>
                                                                        <th>Name</th>
                                                                        <th>Email</th>
                                                                        <th>Mobile</th>
                                                                        <th>Commission Percentage</th>
                                                                        <th>Service Amount</th>
                                                                        <th>Final Amount</th>
                                                                        <th>Super GV Discount</th>
                                                                        <th>Package Discount</th>
                                                                        <th>Discount</th>
                                                                        <th>Total Sales</th>

                                                                                <?php
                                                                                if ($per != '0') {
                                                                                    ?>
                                                                        <th>Sales %</th>
                                                                        <?php
                                                                    }
                                                                    ?>

                                                                        <th>Commission</th>
                                                                    </tr>
                                                                </thead>

                                                                <tbody>

                                                                            <?php
                                                                            $FandFSales = '';
                                                                            $FandFComm = 0;
                                                                            $FServicetotalamount = 0;
                                                                            $FDistotalamount = 0;
                                                                            $Fsaletotalamount = 0;
                                                                            $Grand_AfterDivideSale = 0;
                                                                            $Grand_super_gv_deduct_amount = 0;
                                                                            $Grand_package_discount = 0;
                                                                            $per = $_GET["per"];
                                                                            $DB = Connect();

                                                                            $all_offers = select("OfferID,OfferName,OfferCode,Type,TypeAmount", "tblOffers", "OfferID >0");

                                                                            if (isset($all_offers) && is_array($all_offers) && count($all_offers) > 0) {
                                                                                foreach ($all_offers as $offkey => $offvalue) {
                                                                                    $all_offers_name[$offvalue['OfferID']] = $offvalue;
                                                                                }
                                                                            }

                                                                            $EmployeeID = $_GET["Store"];
                                                                            $sql = "select EID, EmployeeName, EmployeeEmailID, EmpPercentage, EmployeeMobileNo from tblEmployees where Status=0 AND StoreID='" . $EmployeeID . "'";
                                                                            $RS = $DB->query($sql);
                                                                            if ($RS->num_rows > 0) {
                                                                                while ($row = $RS->fetch_assoc()) {
                                                                                    $all_emp_data[$row["EID"]] = $row;
                                                                                    $all_emp_ids[$row["EID"]] = $row["EID"];
                                                                                }
                                                                            }
                                                                            ///$all_emp_ids = array();
                                                                            //$all_emp_ids[8] = 8;
                                                                            if (isset($all_emp_ids) && is_array($all_emp_ids) && count($all_emp_ids) > 0) {
                                                                                $emp_ids = implode(",", $all_emp_ids);
                                                                                if ($emp_ids != '') {
                                                                                    $sqldetails = "SELECT tblEmployees.EID,tblInvoiceDetails.CustomerID,tblAppointmentAssignEmployee.AppointmentID,
                                                                                                                                                                                                                                                                                                                                                                            tblAppointmentAssignEmployee.Qty,
                                                                                                                                                                                                                                                                                                                                                                            tblAppointmentAssignEmployee.ServiceID,
                                                                                                                                                                                                                                                                                                                                                                            tblAppointmentAssignEmployee.Commission,
                                                                                                                                                                                                                                                                                                                                                                            tblAppointmentAssignEmployee.QtyParam,
                                                                                                                                                                                                                                                                                                                                                                            tblInvoiceDetails.OfferDiscountDateTime,tblEmployees.StoreID
                                                                                                                                                                                                                                                                                                                                                                            FROM tblEmployees
                                                                                                                                                                                                                                                                                                                                                                                                    join tblAppointmentAssignEmployee on tblEmployees.EID = tblAppointmentAssignEmployee.MECID
                                                                                                                                                                                                                                                                                                                                                                            join tblInvoiceDetails on tblAppointmentAssignEmployee.AppointmentID=tblInvoiceDetails.AppointmentId
                                                                                                                                                                                                                                                                                                                                                                                                    join tblAppointments on tblAppointments.AppointmentID=tblInvoiceDetails.AppointmentId
                                                                                                                                                                                                                                                                                                                                                                            where tblAppointmentAssignEmployee.AppointmentID!='NULL'
                                                                                                                                                                                                                                                                                                                                                                            and tblInvoiceDetails.OfferDiscountDateTime!='NULL'
                                                                                    $sqlTempfrom
                                                                                    $sqlTempto
                                                                                    $empq and tblEmployees.EID IN (" . $emp_ids . ") AND tblAppointments.Status=2 AND tblAppointments.IsDeleted=0
                                                                                                                                                                                                                                                                                                                                                                                                    group by tblAppointmentAssignEmployee.AppointmentID,tblAppointmentAssignEmployee.MECID,ServiceID,QtyParam";

                                                                                    $RSdetails = $DB->query($sqldetails);

                                                                                    while ($rowdetails = $RSdetails->fetch_assoc()) {
                                                                                        $invoice_data[$rowdetails['EID']][] = $rowdetails;
                                                                                    }
                                                                                }
                                                                            }

                                                                            if (isset($invoice_data) && is_array($invoice_data) && count($invoice_data) > 0) {
                                                                                $service_ids = array();
                                                                                $store_ids = array();
                                                                                $appointment_ids = array();
                                                                                $customer_ids = array();

                                                                                foreach ($invoice_data as $detailkey => $detailvalue) {
                                                                                    foreach ($detailvalue as $invkey => $invvalue) {
                                                                                        $service_ids[$invvalue["ServiceID"]] = $invvalue["ServiceID"];
                                                                                        $store_ids[$invvalue["StoreID"]] = $invvalue["StoreID"];
                                                                                        $appointment_ids[$invvalue["AppointmentID"]] = $invvalue["AppointmentID"];
                                                                                        $customer_ids[$invvalue["CustomerID"]] = $invvalue["CustomerID"];
                                                                                    }
                                                                                }


                                                                                                                                                                                                                                                                                                                                                                                                                                    /*
                                                                                                                                                                                                                                                                                                                                                                                                                                     * get service amt
                                                                                                                                                                                                                                                                                                                                                                                                                                     */

                                                                                if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                                                                                    $apt_in_ids = implode(",", $appointment_ids);
                                                                                    $aptdate_q = "SELECT * FROM tblAppointmentsDetailsInvoice WHERE AppointmentID IN(" . $apt_in_ids . ") AND PackageService=0";
                                                                                    $aptdate_exe = $DB->query($aptdate_q);
                                                                                    while ($aptdatedetails = $aptdate_exe->fetch_assoc()) {
                                                                                        $all_apt_service_amt[] = $aptdatedetails;
                                                                                    }
                                                                                    if (isset($all_apt_service_amt) && is_array($all_apt_service_amt) && count($all_apt_service_amt) > 0) {
                                                                                        foreach ($all_apt_service_amt as $servdkey => $servvalue) {
                                                                                            $all_ser_amt[$servvalue['AppointmentID']][$servvalue['ServiceID']] = $servvalue['ServiceAmount'];
                                                                                            $super_gv_deduct[$servvalue['AppointmentID']][$servvalue['ServiceID']] = $servvalue['super_gv_discount'] / $servvalue['qty'];
                                                                                            $package_discount_deduct[$servvalue['AppointmentID']][$servvalue['ServiceID']] = round(($servvalue['package_discount'] / $servvalue['qty']),2);
                                                                                            $tall_ser_amt[$servvalue['AppointmentID']][$servvalue['ServiceID']] = round(($servvalue['service_amount_deduct_dis'] / $servvalue['qty']),2);
                                                                                        }
                                                                                    }
                                                                                }

                                                                                                                                                                                                                                                                                                                                                                                                                                    /*
                                                                                                                                                                                                                                                                                                                                                                                                                                     * get discount
                                                                                                                                                                                                                                                                                                                                                                                                                                     */
                                                                                if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                                                                                    $apt_in_ids = implode(",", $appointment_ids);
                                                                                    $aptdate_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ")";
                                                                                    $aptdate_exe = $DB->query($aptdate_q);
                                                                                    while ($disdetails = $aptdate_exe->fetch_assoc()) {
                                                                                        $all_apt_dis_amt[] = $disdetails;
                                                                                    }

                                                                                                                                                                                                                                                                                                                                                                                                                                        /*
                                                                                                                                                                                                                                                                                                                                                                                                                                         * Get Appointment Offer AMount
                                                                                                                                                                                                                                                                                                                                                                                                                                         */
                                                                                    $apt_in_ids = implode(",", $appointment_ids);
                                                                                    $aptoffer_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID,OfferID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ") AND OfferAmount > 0";
                                                                                    $aptoffer_exe = $DB->query($aptoffer_q);
                                                                                    while ($offeretails = $aptoffer_exe->fetch_assoc()) {
                                                                                        $apt_service_count[$offeretails['AppointmentID']][$offeretails['ServiceID']] = $offeretails['OfferAmount'];
                                                                                    }

                                                                                    $apt_in_ids = implode(",", $appointment_ids);
                                                                                    $aptoffer_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID,OfferID,AppointmentMembershipDiscountID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ") AND OfferAmount > 0";
                                                                                    $aptoffer_exe = $DB->query($aptoffer_q);

                                                                                    while ($offeretails = $aptoffer_exe->fetch_assoc()) {
                                                                                        if (isset($all_offers_name[$offeretails['OfferID']]) && $all_offers_name[$offeretails['OfferID']]['Type'] == 1 && $offeretails['OfferID'] > 0) {
                                                                                            $offer_service_cnt = isset($apt_service_count[$offeretails['AppointmentID']]) ? count($apt_service_count[$offeretails['AppointmentID']]) : 1;
                                                                                            //$all_apt_off_dis[$offeretails['AppointmentID']][$offeretails['ServiceID']] = $offeretails['OfferAmount'] / $offer_service_cnt;
                                                                                            $all_apt_off_dis[$offeretails['AppointmentID']][$offeretails['ServiceID']] = round($offeretails['OfferAmount'], 2);
                                                                                        } else {
                                                                                            $all_apt_off_dis[$offeretails['AppointmentID']][$offeretails['ServiceID']] = round($offeretails['OfferAmount'], 2);
                                                                                        }
                                                                                    }

                                                                                    if (isset($all_apt_dis_amt) && is_array($all_apt_dis_amt) && count($all_apt_dis_amt) > 0) {
                                                                                        foreach ($all_apt_dis_amt as $diskey => $disvalue) {
                                                                                                                                                                                                                                                                                                                                                                                                                                                /*
                                                                                                                                                                                                                                                                                                                                                                                                                                                 * Get Appointment service count
                                                                                                                                                                                                                                                                                                                                                                                                                                                 */
                                                                                            $extra_offer_amount = 0;

                                                                                            if (isset($all_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']]) && $all_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']] > 0) {
                                                                                                $extra_offer_amount = $all_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']];
                                                                                            }
                                                                                            //echo '<br>extra_offer_amount=' . $extra_offer_amount;
                                                                                            //echo '<br>member_amount=' . $disvalue['MemberShipAmount'];
                                                                                            $all_dis_amt[$disvalue['AppointmentID']][$disvalue['ServiceID']]['MemberShipAmount'] = $disvalue['MemberShipAmount'] + round($extra_offer_amount, 2);
                                                                                            $all_apt_mem_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']] = $disvalue['MemberShipAmount'];
                                                                                            //$all_dis_amt[$disvalue['AppointmentID']][$disvalue['ServiceID']]['OfferAmount'] = $disvalue['OfferAmount'];
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }

                                                                            $total_amount = 0;
                                                                            if (isset($all_emp_data) && is_array($all_emp_data) && count($all_emp_data) > 0) {
                                                                                foreach ($all_emp_data as $key => $row) {
                                                                                    $strEID = $row["EID"];
                                                                                    if (isset($invoice_data[$strEID]) && is_array($invoice_data[$strEID]) && count($invoice_data[$strEID]) > 0) {
                                                                                        foreach ($invoice_data[$strEID] as $rowkey => $rowdetails) {
                                                                                            $strAID = $rowdetails["AppointmentID"];
                                                                                            $strSID = $rowdetails["ServiceID"];
                                                                                            $strCommission = $rowdetails["Commission"];
                                                                                            $strAmount = isset($all_ser_amt[$strAID][$strSID]) ? $all_ser_amt[$strAID][$strSID] : '';
                                                                                            if ($strCommission == 2) {
                                                                                                $strAmount = $strAmount / 2;
                                                                                                $all_discount = $all_discount / 2;
                                                                                            }
                                                                                            $total_amount += $strAmount;
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }

                                                                            if (isset($all_emp_data) && is_array($all_emp_data) && count($all_emp_data) > 0) {
                                                                                foreach ($all_emp_data as $key => $row) {

                                                                                    $strEID = $row["EID"];

                                                                                    if ($strEID == "0") {
                                                                                        // List of managers, HO and Audit whose details need not to be shown
                                                                                    } else {

                                                                                        $strEmployeeName = $row["EmployeeName"];
                                                                                        $strEmployeeEmailID = $row["EmployeeEmailID"];
                                                                                        $strEmpPercentage = $row["EmpPercentage"];
                                                                                        $strEmployeeMobileNo = $row["EmployeeMobileNo"];

                                                                                        $TotalAfterDivideSale = 0;
                                                                                        $strTotalAmount = 0;
                                                                                        $TotalFinalDiscount = 0;
                                                                                        $TotalUltimateSale = 0;
                                                                                        $ComFinal = 0;
                                                                                        $Total_super_gv_deduct_amount = 0;
                                                                                        $Total_AfterDivideSale = 0;
                                                                                        $Total_package_dicsount = 0;

                                                                                        if (isset($invoice_data[$strEID]) && is_array($invoice_data[$strEID]) && count($invoice_data[$strEID]) > 0) {

                                                                                            foreach ($invoice_data[$strEID] as $rowkey => $rowdetails) {
                                                                                                if (isset($emp_service_qty[$rowdetails['AppointmentID']][$rowdetails["ServiceID"]][$strEID])) {
                                                                                                    $emp_service_qty[$rowdetails['AppointmentID']][$rowdetails["ServiceID"]][$strEID] += 1;
                                                                                                } else {
                                                                                                    $emp_service_qty[$rowdetails['AppointmentID']][$rowdetails["ServiceID"]][$strEID] = 1;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        if (isset($invoice_data[$strEID]) && is_array($invoice_data[$strEID]) && count($invoice_data[$strEID]) > 0) {
                                                                                            $counter = 0;
                                                                                            $strSID = "";
                                                                                            $qty = "";
                                                                                            $strSAmount = "";
                                                                                            $strAmount = "";
                                                                                            $strCommission = "";
                                                                                            $FinalDAmount = '';
                                                                                            $FinalDiscount = '';
                                                                                            $UltimateSale = '';
                                                                                            $AfterDivideSale = '';
                                                                                            $CommssionFinal = "";


                                                                                            foreach ($invoice_data[$strEID] as $rowkey => $rowdetails) {
                                                                                                $counter ++;
                                                                                                $strEIDa = $rowdetails["EID"];
                                                                                                $strAID = $rowdetails["AppointmentID"];
                                                                                                $strSID = $rowdetails["ServiceID"];
                                                                                                $CustomerID = $rowdetails['CustomerID'];
                                                                                                $qty = $rowdetails["Qty"];
                                                                                                $strAmount = isset($all_ser_amt[$strAID][$strSID]) ? $all_ser_amt[$strAID][$strSID] : '';
                                                                                                //$super_gv_deduct_amount = isset($super_gv_deduct[$strAID][$strSID]) ? $super_gv_deduct[$strAID][$strSID] : '0';

                                                                                                $strSAmount = $strAmount;
                                                                                                $strCommission = $rowdetails["Commission"];
                                                                                                $StoreIDd = $rowdetails["StoreID"];


                                                                                                $AfterDivideSale = $strSAmount;

                                                                                                $rowdiscount = isset($all_dis_amt[$strAID][$strSID]) ? $all_dis_amt[$strAID][$strSID] : array();
                                                                                                if (isset($rowdiscount) && is_array($rowdiscount) && count($rowdiscount) > 0) {
                                                                                                    $strOfferAmount = $rowdiscount["OfferAmount"];
                                                                                                    $strDiscountAmount = $rowdiscount["MemberShipAmount"];

                                                                                                    if ($strOfferAmount > 0) {
                                                                                                        $FinalDAmount = $strOfferAmount;
                                                                                                    } elseif ($strDiscountAmount > 0) {
                                                                                                        $FinalDAmount = $strDiscountAmount;
                                                                                                    }
                                                                                                } else {
                                                                                                    $FinalDAmount = "0";
                                                                                                }

                                                                                                // $FinalDiscount = $FinalDAmount / $qty;
                                                                                                $FinalDiscount = isset($all_apt_mem_dis[$strAID][$strSID]) ? ($all_apt_mem_dis[$strAID][$strSID]) / $qty : 0;
                                                                                                $FinalOtherDiscount = isset($all_apt_off_dis[$strAID][$strSID]) ? ($all_apt_off_dis[$strAID][$strSID]) / $qty : 0;

                                                                                                $emp_ser_count = isset($emp_service_qty[$strAID][$strSID][$strEID]) ? $emp_service_qty[$strAID][$strSID][$strEID] : 1;
                                                                                                $UltimateSale = isset($tall_ser_amt[$strAID][$strSID]) ? $tall_ser_amt[$strAID][$strSID] : '';

                                                                                                $package_discount_deduct_amount = isset($package_discount_deduct[$strAID][$strSID]) ? $package_discount_deduct[$strAID][$strSID]  : '0';
                                                                                                $super_gv_deduct_amount = isset($super_gv_deduct[$strAID][$strSID]) ? $super_gv_deduct[$strAID][$strSID] : '0';


                                                                                                //$UltimateSale = $AfterDivideSale - $FinalDiscount - $FinalOtherDiscount - $super_gv_deduct_amount;
                                                                                                $UltimateSale = round($UltimateSale, 2);



                                                                                                $all_discount = $FinalDiscount + $FinalOtherDiscount;
                                                                                                if ($strCommission == 2) {
                                                                                                    $UltimateSale = $UltimateSale / 2;
                                                                                                    $AfterDivideSale = $AfterDivideSale / 2;
                                                                                                    $super_gv_deduct_amount = $super_gv_deduct_amount / 2;
                                                                                                    $all_discount = $all_discount / 2;
                                                                                                    $package_discount_deduct_amount = $package_discount_deduct_amount /2;
                                                                                                }
                                                                                                $TotalUltimateSale += $UltimateSale; //Total of discounted amount
                                                                                                $Total_AfterDivideSale += $AfterDivideSale;
                                                                                                $Total_super_gv_deduct_amount += $super_gv_deduct_amount;
                                                                                                $Total_package_dicsount += $package_discount_deduct_amount;
                                                                                                $TotalFinalDiscount += $all_discount;
                                                                                                $CommssionFinal = ($UltimateSale / 100) * $strEmpPercentage;

                                                                                                $ComFinal += $CommssionFinal; //Total of Commission
                                                                                            }
                                                                                        }
                                                                                        ?>


                                                                    <tr id="my_data_tr_<?= $counter ?>">

                                                                        <td><?= $strEmployeeName ?></td>
                                                                        <td><?= $strEmployeeEmailID ?></td>
                                                                        <td><?= $strEmployeeMobileNo ?></td>
                                                                        <td><?= $strEmpPercentage ?></td>
                                                                        <td>Rs. <?= $TotalUltimateSale ?> /-</td>
                                                                        <td>Rs.  <?php echo $Total_AfterDivideSale; ?>/-</td>
                                                                        <td>Rs. <?= $Total_super_gv_deduct_amount ?> /-</td>
                                                                        <td>Rs. <?= $Total_package_dicsount ?> /-</td>
                                                                        <td>Rs. <?= $TotalFinalDiscount ?> /-</td>
                                                                        <td>Rs. <?= $TotalUltimateSale ?> /-</td>
                                                                        <?php
                                                                        if ($per != '0') {
                                                                            $saleper = ($Total_AfterDivideSale / $total_amount) * 100;
                                                                            $saleper = round($saleper, 2);
                                                                            $totalsa += $saleper;
                                                                            ?>
                                                                        <td><?= round($saleper, 2) ?></td>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                        <td>Rs. <?= $ComFinal ?> /-</td>
                                                                        <?php
                                                                        $FServicetotalamount += $TotalUltimateSale;
                                                                        $Grand_AfterDivideSale += $Total_AfterDivideSale;
                                                                        $Grand_super_gv_deduct_amount += $Total_super_gv_deduct_amount;
                                                                        $Grand_package_discount += $Total_package_dicsount;
                                                                        $FDistotalamount += $TotalFinalDiscount;

                                                                        $FandFComm += $ComFinal;
                                                                        ?>
                                                                    </tr>



                                                                                        <?php
                                                                                    }
                                                                                }
                                                                                $FandFSalest = "";
                                                                                ?>
                                                                    <tr>
                                                                        <td colspan='4'><b><center>Total commission for selected period : </center></b></td>
                                                                        <td><b>Rs. <?= $FServicetotalamount ?> /-</b></td>
                                                                        <td><b>Rs. <?= $Grand_AfterDivideSale ?> /-</b></td>
                                                                        <td><b>Rs. <?= $Grand_super_gv_deduct_amount ?> /-</b></td>
                                                                        <td><b>Rs. <?= $Grand_package_discount ?> /-</b></td>
                                                                        <td><b>Rs. <?= $FDistotalamount ?> /-</b></td>
                                                                        <td><b>Rs. <?= $FServicetotalamount ?> /-</b></td>
                                                                        <?php
                                                                        if ($per != '0') {
                                                                            ?>
                                                                        <td><b><?= $totalsa ?></b></td>
                                                                        <?php
                                                                    }
                                                                    ?>

                                                                        <td><b>Rs. <?= $FandFComm ?> /-</b></td>
                                                                    </tr>
                                                                    <?php
                                                                } else {
                                                                    echo "No Employees found for this store";
                                                                }
                                                                $DB->close();
                                                                ?>
                                                                </tbody>
                                                            </table>


                                                                    <?php
                                                                } else {

                                                                }
                                                                ?>


                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                } // End null condition
                else {

                }
                ?>
                </div>
            </div>

            <?php require_once 'incFooter.fya'; ?>

        </div>
    </body>

</html>