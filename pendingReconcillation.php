<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
$strPageTitle = "Reconcillation Details | NailSpa";
$strDisplayTitle = "Reconcillation Details for NailSpa";
$strMenuID = "10";
$strMyTable = "tblAppointments";
$strMyTableID = "AppointmentID";
$strMyField = "AppointmentDate";
$strMyActionPage = "DisplayReconcillationDetail.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>

        <script>

            //$("#reconcillation_confirm").click(function () {
            function proceed_reconcillation() {
                var reconcille_val = [];
                $.each($("input[class='reconciile_check']:checked"), function () {
                    reconcille_val.push($(this).val());
                });

                //var emp_id = '<?php echo $strEmpID; ?>';
                if (confirm('Are you sure you want to Confirm?')) {
                    $.ajax({
                        url: "reconcillation_confirm.php",
                        type: 'post',
                        //data: "emp_apt=" + reconcille_val, "type=1", "created_id=" + <?php echo $strAdminID ?>,
                        data: {"emp_apt": reconcille_val, "type": '1', "created_id": '<?php echo $strAdminID ?>'},
                        success: function (response) {
                            if (response == 1) {
                                $msg = "Reconcillation Confirmed Successfully!";
                                alert($msg);
                                window.location.reload(true);
                            } else {
                                $msg = "Failed To Confirm!";
                                alert($msg);
                                window.location.reload(true);
                            }
                        }
                    });
                }
            }


        </script>

    </head>

    <body>

        <div id="sb-site">


        <?php require_once("incOpenLayout.fya"); ?>


        <?php require_once("incLoader.fya"); ?>


        <div id="page-wrapper">
            <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

            <?php require_once("incLeftMenu.fya"); ?>

            <div id="page-content-wrapper">
                <div id="page-content">

                    <?php require_once("incHeader.fya"); ?>


                    <div class="panel">
                        <div class="panel">
                            <div class="panel-body">

                                <div class="example-box-wrapper">

                                    <div id="normal-tabs-1">
                                        <span class="form_result">&nbsp; <br>
                                        </span>


                                        <h4 class="title-hero"><center><?php echo 'Pending Reconcillation | NailSpa' ?></center></h4>

                                        <form method = "get" class = "form-horizontal bordered-row" role = "form">
                                            <div class = "form-group"><label for = "" class = "col-sm-4 control-label">Select Bill date</label>
                                                <div class = "col-sm-4">
                                                    <div class = "input-prepend input-group">
                                                        <span class = "add-on input-group-addon">
                                                            <i class = "glyph-icon icon-calendar"></i>
                                                        </span>
                                                        <?php
                                                        if (isset($_GET["toandfrom"])) {
                                                            $strtoandfrom = $_GET["toandfrom"];
                                                        }
                                                        ?>
                                                        <input type = "text" name = "toandfrom" id = "daterangepicker-example" class = "form-control" value = "<?= $strtoandfrom ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class = "form-group"><label for = "" class = "col-sm-4 control-label">Select Store</label>
                                                <div class = "col-sm-4">

                                                    <select name = "store" class = "form-control">
                                                        <option value = "0">All</option>
                                                        <?php
                                                        $selp = select("*", "tblStores", "Status='0'");
                                                        foreach ($selp as $val) {
                                                            $strStoreName = $val["StoreName"];
                                                            $strStoreID = $val["StoreID"];
                                                            $store = $_GET["store"];
                                                            if ($store == $strStoreID) {
                                                                ?>
                                                        <option  selected value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                                    </select>

                                                </div>

                                            </div>



                                            <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                                &nbsp;&nbsp;&nbsp;
                                                <a class="btn btn-link" href="pendingReconcillation.php">Clear All Filter</a>
                                                &nbsp;&nbsp;&nbsp;

                                            </div>
                                        </form>
                                        <a class="btn btn-alt btn-hover btn-warning" href="#" id="reconcillation_confirm" onclick="proceed_reconcillation();">Reconcille</a> &nbsp;&nbsp;&nbsp;


                                        <?php
                                        if (isset($_GET['toandfrom']) && $_GET['toandfrom'] != '') {

                                            $all_offers = select("OfferID,OfferName,OfferCode,Type,TypeAmount", "tblOffers", "OfferID >0");

                                            if (isset($all_offers) && is_array($all_offers) && count($all_offers) > 0) {
                                                foreach ($all_offers as $offkey => $offvalue) {
                                                    $all_offers_name[$offvalue['OfferID']] = $offvalue;
                                                }
                                            }

                                            $arraytofrom = explode("-", $_GET['toandfrom']);

                                            $from = $arraytofrom[0];
                                            $datetime = new DateTime($from);
                                            $getfrom = $datetime->format('Y-m-d');


                                            $to = $arraytofrom[1];
                                            $datetime = new DateTime($to);
                                            $getto = $datetime->format('Y-m-d');
                                            if (isset($_GET['store']) && $_GET['store'] != '' && !empty($_GET['store'])) {
                                                $emp_child = select("*", "tblEmployees", "StoreID = '" . $_GET['store'] . "'");
                                            } else {
                                                $emp_child = select("*", "tblEmployees", "EID > 0");
                                            }


                                            if (isset($emp_child) && is_array($emp_child) && count($emp_child) > 0) {
                                                foreach ($emp_child as $ckey => $cvalue) {
                                                    $all_emp_ids[$cvalue['EID']] = $cvalue['EID'];
                                                }
                                            }

                                            if (isset($all_emp_ids) && is_array($all_emp_ids) && count($all_emp_ids) > 0) {
                                                $in_emp_ids = implode(",", $all_emp_ids);
                                                if ($in_emp_ids != '') {
                                                    $sqlTempfrom = " and Date(tblInvoiceDetails.OfferDiscountDateTime)>=Date('" . $getfrom . "')";
                                                    $sqlTempto = " and Date(tblInvoiceDetails.OfferDiscountDateTime)<=Date('" . $getto . "')";

                                                    $sqldetails = "SELECT tblEmployees.EID,tblInvoiceDetails.CustomerID,tblAppointmentAssignEmployee.AppointmentID,
                                                                                                                                                                    tblAppointmentAssignEmployee.Qty,
                                                                                                                                                                    tblAppointmentAssignEmployee.ServiceID,
                                                                                                                                                                    tblAppointmentAssignEmployee.Commission,
                                                                                                                                                                    tblAppointmentAssignEmployee.QtyParam,
                                                                                                                                                                    tblInvoiceDetails.OfferDiscountDateTime,tblEmployees.StoreID
                                                                                                                                                                    FROM tblEmployees
                                                                                                                                                                    join tblAppointmentAssignEmployee on tblEmployees.EID = tblAppointmentAssignEmployee.MECID
                                                                                                                                                                    join tblInvoiceDetails on tblAppointmentAssignEmployee.AppointmentID=tblInvoiceDetails.AppointmentId
                                                                                                                                                                    join tblAppointments on tblAppointments.AppointmentID=tblInvoiceDetails.AppointmentId
                                                                                                                                                                    where tblAppointmentAssignEmployee.AppointmentID!='NULL'
                                                                                                                                                                    and tblInvoiceDetails.OfferDiscountDateTime!='NULL'
                                                    $sqlTempfrom
                                                    $sqlTempto
                                                    $empq and tblEmployees.EID IN(" . $in_emp_ids . ") AND tblAppointments.Status=2 AND tblAppointments.IsDeleted=0
                                                                                                                                                                    group by tblAppointmentAssignEmployee.AppointmentID,ServiceID,QtyParam,EID";
                                                    $RSdetails = $DB->query($sqldetails);


                                                    while ($rowdetails = $RSdetails->fetch_assoc()) {
                                                        $invoice_data[$rowdetails['EID']][] = $rowdetails;
                                                    }


                                                    if (isset($invoice_data) && is_array($invoice_data) && count($invoice_data) > 0) {
                                                        $service_ids = array();
                                                        $store_ids = array();
                                                        $appointment_ids = array();
                                                        $customer_ids = array();

                                                        foreach ($invoice_data as $detailkey => $detailvalue) {
                                                            foreach ($detailvalue as $invkey => $invvalue) {
                                                                $service_ids[$invvalue["ServiceID"]] = $invvalue["ServiceID"];
                                                                $store_ids[$invvalue["StoreID"]] = $invvalue["StoreID"];
                                                                $appointment_ids[$invvalue["AppointmentID"]] = $invvalue["AppointmentID"];
                                                                $customer_ids[$invvalue["CustomerID"]] = $invvalue["CustomerID"];
                                                            }
                                                        }


                                                                                                                                                                                    /*
                                                                                                                                                                                     * get service name
                                                                                                                                                                                     */
                                                        if (isset($service_ids) && is_array($service_ids) && count($service_ids) > 0) {
                                                            $service_in_ids = implode(",", $service_ids);
                                                            $service_q = "SELECT ServiceID,ServiceName FROM tblServices WHERE ServiceID IN(" . $service_in_ids . ")";
                                                            $service_exe = $DB->query($service_q);
                                                            while ($servdetails = $service_exe->fetch_assoc()) {
                                                                $all_services[] = $servdetails;
                                                            }

                                                            if (isset($all_services) && is_array($all_services) && count($all_services) > 0) {
                                                                foreach ($all_services as $serkey => $servalue) {
                                                                    $sep[$servalue['ServiceID']] = $servalue['ServiceName'];
                                                                }
                                                            }
                                                        }


                                                                                                                                                                                    /*
                                                                                                                                                                                     * get store name
                                                                                                                                                                                     */
                                                        if (isset($store_ids) && is_array($store_ids) && count($store_ids) > 0) {
                                                            $store_in_ids = implode(",", $store_ids);
                                                            $store_q = "SELECT StoreID,StoreName FROM tblStores WHERE StoreID IN(" . $store_in_ids . ")";
                                                            $store_exe = $DB->query($store_q);
                                                            while ($storedetails = $store_exe->fetch_assoc()) {
                                                                $all_store[] = $storedetails;
                                                            }
                                                            if (isset($all_store) && is_array($all_store) && count($all_store) > 0) {
                                                                foreach ($all_store as $stokey => $stovalue) {
                                                                    $stpp[$stovalue['StoreID']] = $stovalue['StoreName'];
                                                                }
                                                            }
                                                        }

                                                                                                                                                                                    /*
                                                                                                                                                                                     * get invoice number
                                                                                                                                                                                     */
                                                        if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                                                            $apt_in_ids = implode(",", $appointment_ids);
                                                            $apt_q = "SELECT InvoiceID,AppointmentID FROM tblInvoice WHERE AppointmentID IN(" . $apt_in_ids . ")";
                                                            $apt_exe = $DB->query($apt_q);
                                                            while ($aptdetails = $apt_exe->fetch_assoc()) {
                                                                $all_apt[] = $aptdetails;
                                                            }

                                                            if (isset($all_apt) && is_array($all_apt) && count($all_apt) > 0) {
                                                                foreach ($all_apt as $aptkey => $aptvalue) {
                                                                    $sql_invoice_number[$aptvalue['AppointmentID']] = $aptvalue['InvoiceID'];
                                                                }
                                                            }
                                                        }

                                                                                                                                                                                    /*
                                                                                                                                                                                     * get AppointmentDate
                                                                                                                                                                                     */
                                                        if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                                                            $apt_in_ids = implode(",", $appointment_ids);
                                                            $aptdate_q = "SELECT AppointmentDate,AppointmentID FROM tblAppointments WHERE AppointmentID IN(" . $apt_in_ids . ")";
                                                            $aptdate_exe = $DB->query($aptdate_q);
                                                            while ($aptdatedetails = $aptdate_exe->fetch_assoc()) {
                                                                $alldate_apt[] = $aptdatedetails;
                                                            }
                                                            if (isset($alldate_apt) && is_array($alldate_apt) && count($alldate_apt) > 0) {
                                                                foreach ($alldate_apt as $aptdkey => $aptdvalue) {
                                                                    $sql_apt_date[$aptdvalue['AppointmentID']] = $aptdvalue['AppointmentDate'];
                                                                }
                                                            }
                                                        }

                                                                                                                                                                                    /*
                                                                                                                                                                                     * get customer
                                                                                                                                                                                     */
                                                        if (isset($customer_ids) && is_array($customer_ids) && count($customer_ids) > 0) {
                                                            $cust_in_ids = implode(",", $customer_ids);

                                                            $cust_q = "SELECT CustomerID,CustomerFullName FROM tblCustomers WHERE CustomerID IN(" . $cust_in_ids . ")";
                                                            $cust_exe = $DB->query($cust_q);
                                                            while ($custdetails = $cust_exe->fetch_assoc()) {
                                                                $all_cust[] = $custdetails;
                                                            }
                                                            if (isset($all_cust) && is_array($all_cust) && count($all_cust) > 0) {
                                                                foreach ($all_cust as $custkey => $custvalue) {
                                                                    $sql_customer[$custvalue['CustomerID']] = $custvalue['CustomerFullName'];
                                                                }
                                                            }
                                                        }

                                                                                                                                                                                    /*
                                                                                                                                                                                     * get service amt
                                                                                                                                                                                     */

                                                        if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                                                            $apt_in_ids = implode(",", $appointment_ids);
                                                            $aptdate_q = "SELECT ServiceAmount,AppointmentID,ServiceID FROM tblAppointmentsDetailsInvoice WHERE AppointmentID IN(" . $apt_in_ids . ") AND PackageService=0";
                                                            $aptdate_exe = $DB->query($aptdate_q);
                                                            while ($aptdatedetails = $aptdate_exe->fetch_assoc()) {
                                                                $all_apt_service_amt[] = $aptdatedetails;
                                                            }
                                                            if (isset($all_apt_service_amt) && is_array($all_apt_service_amt) && count($all_apt_service_amt) > 0) {
                                                                foreach ($all_apt_service_amt as $servdkey => $servvalue) {
                                                                    $all_ser_amt[$servvalue['AppointmentID']][$servvalue['ServiceID']] = $servvalue['ServiceAmount'];
                                                                }
                                                            }
                                                        }

                                                                                                                                                                                    /*
                                                                                                                                                                                     * get discount
                                                                                                                                                                                     */
                                                        if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                                                            $apt_in_ids = implode(",", $appointment_ids);
                                                            $aptdate_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ")";
                                                            $aptdate_exe = $DB->query($aptdate_q);
                                                            while ($disdetails = $aptdate_exe->fetch_assoc()) {
                                                                $all_apt_dis_amt[] = $disdetails;
                                                            }

                                                                                                                                                                                        /*
                                                                                                                                                                                         * Get Appointment Offer AMount
                                                                                                                                                                                         */
                                                            $apt_in_ids = implode(",", $appointment_ids);
                                                            $aptoffer_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID,OfferID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ") AND OfferAmount > 0";
                                                            $aptoffer_exe = $DB->query($aptoffer_q);
                                                            while ($offeretails = $aptoffer_exe->fetch_assoc()) {
                                                                $apt_service_count[$offeretails['AppointmentID']][$offeretails['ServiceID']] = $offeretails['OfferAmount'];
                                                            }

                                                            $apt_in_ids = implode(",", $appointment_ids);
                                                            $aptoffer_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID,OfferID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ") AND OfferAmount > 0";
                                                            $aptoffer_exe = $DB->query($aptoffer_q);
                                                            while ($offeretails = $aptoffer_exe->fetch_assoc()) {
                                                                                                                                                                                            /*
                                                                                                                                                                                             * if offer is amount divide offer by service count
                                                                                                                                                                                             */
                                                                if (isset($all_offers_name[$offeretails['OfferID']]) && $all_offers_name[$offeretails['OfferID']]['Type'] == 1 && $offeretails['OfferID'] > 0) {
                                                                    $offer_service_cnt = isset($apt_service_count[$offeretails['AppointmentID']]) ? count($apt_service_count[$offeretails['AppointmentID']]) : 1;
                                                                    $all_apt_off_dis[$offeretails['AppointmentID']][$offeretails['ServiceID']] = $offeretails['OfferAmount'] / $offer_service_cnt;
                                                                } else {
                                                                    $all_apt_off_dis[$offeretails['AppointmentID']][$offeretails['ServiceID']] = $offeretails['OfferAmount'];
                                                                }
                                                            }


                                                            if (isset($all_apt_dis_amt) && is_array($all_apt_dis_amt) && count($all_apt_dis_amt) > 0) {
                                                                foreach ($all_apt_dis_amt as $diskey => $disvalue) {
                                                                                                                                                                                                /*
                                                                                                                                                                                                 * Get Appointment service count
                                                                                                                                                                                                 */
                                                                    $extra_offer_amount = 0;

                                                                    if (isset($all_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']]) && $all_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']] > 0) {
                                                                        $extra_offer_amount = $all_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']];
                                                                    }
                                                                    //echo '<br>extra_offer_amount=' . $extra_offer_amount;
                                                                    //echo '<br>member_amount=' . $disvalue['MemberShipAmount'];
                                                                    $all_dis_amt[$disvalue['AppointmentID']][$disvalue['ServiceID']]['MemberShipAmount'] = $disvalue['MemberShipAmount'] + round($extra_offer_amount, 2);
                                                                    $all_apt_mem_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']] = $disvalue['MemberShipAmount'];
                                                                    //$all_dis_amt[$disvalue['AppointmentID']][$disvalue['ServiceID']]['OfferAmount'] = $disvalue['OfferAmount'];
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            if (isset($all_emp_ids) && is_array($all_emp_ids) && count($all_emp_ids) > 0) {
                                                $final_res = array();
                                                foreach ($all_emp_ids as $ekey => $evalue) {
                                                    $search_emp[0]['EID'] = $evalue;

                                                    if (isset($search_emp) && is_array($search_emp) && count($search_emp) > 0) {
                                                        $emp_data = select("*", "tblEmployees", "EID='" . $search_emp[0]['EID'] . "'");
                                                        foreach ($search_emp as $key => $row) {
                                                            $strEID = $emp_data[0]["EID"];
                                                            $strEmployeeName = $emp_data[0]["EmployeeName"];
                                                            $strEmployeeEmailID = $emp_data[0]["EmployeeEmailID"];
                                                            $strEmpPercentage = $emp_data[0]["EmpPercentage"];
                                                            $strEmployeeMobileNo = $emp_data[0]["EmployeeMobileNo"];
                                                            $strEmployeeCode = $emp_data[0]["EmployeeCode"];
                                                            $strTarget = $emp_data[0]["Target"];
                                                            $TotalAfterDivideSale = '';
                                                            $strTotalAmount = '';
                                                            $TotalFinalDiscount = '';
                                                            $TotalUltimateSale = '';
                                                            $ComFinal = '';
                                                            ?>


                                                            <?php /* <div class="col-md-6 col-xs-12 margin-bot-zero no-pad"><b>Name :</b><?php echo $strEmployeeName; ?></div>
                                                                                                                                                                                          <div class="col-md-6 col-xs-12 margin-bot-zero no-pad"><b>Email :</b><?php echo $strEmployeeEmailID; ?></div>
                                                                                                                                                                                          <div class="col-md-6 col-xs-12 margin-bot-zero no-pad"><b>Commission Percentage :</b><?php echo $strEmpPercentage; ?></div>
                                                                                                                                                                                          <div class="col-md-6 col-xs-12 margin-bot-zero no-pad"><b>Mobile no. :</b><?php echo $strEmployeeMobileNo; ?></div>
                                                                                                                                                                                         */ ?>


                                                            <?php
                                                            if (isset($invoice_data[$strEID]) && is_array($invoice_data[$strEID]) && count($invoice_data[$strEID]) > 0) {

                                                                                                                                                                                            /*
                                                                                                                                                                                             * Get Employee Target
                                                                                                                                                                                             */
                                                                $total_target = 0;
                                                                $Month = date('m');
                                                                $MonthSpell = getMonthSpelling($Month);
                                                                $target_data = select("*", "tblEmployeeTarget", "EmployeeCode='" . $strEmployeeCode . "'"
                                                                    . " AND Year='" . date('Y') . "' AND TargetForMonth='" . $MonthSpell . "'");
                                                                if (isset($target_data) && is_array($target_data) && count($target_data) > 0) {
                                                                    $total_target = $target_data[0]['BaseTarget'];
                                                                } else {
                                                                    $total_target = $strTarget;
                                                                }

                                                                                                                                                                                            /*
                                                                                                                                                                                             * Get Completed Target
                                                                                                                                                                                             */
                                                                $getfrom = date('Y-m-01');
                                                                $getto = date('Y-m-t');
                                                                $sqlTempfrom = " and Date(tblInvoiceDetails.OfferDiscountDateTime)>='" . $getfrom . "'";
                                                                $sqlTempto = " and Date(tblInvoiceDetails.OfferDiscountDateTime)<='" . $getto . "'";
                                                                $search_emp[0]['EID'] = $strEID;

                                                                $sqldetails = "SELECT tblEmployees.EID,tblInvoiceDetails.CustomerID,tblAppointmentAssignEmployee.AppointmentID,
                                                                                                                                                tblAppointmentAssignEmployee.Qty,
                                                                                                                                                tblAppointmentAssignEmployee.ServiceID,
                                                                                                                                                tblAppointmentAssignEmployee.Commission,
                                                                                                                                                tblAppointmentAssignEmployee.QtyParam,
                                                                                                                                                tblInvoiceDetails.OfferDiscountDateTime,tblEmployees.StoreID
                                                                                                                                                FROM tblEmployees
                                                                                                                                                                        join tblAppointmentAssignEmployee on tblEmployees.EID = tblAppointmentAssignEmployee.MECID
                                                                                                                                                join tblInvoiceDetails on tblAppointmentAssignEmployee.AppointmentID=tblInvoiceDetails.AppointmentId
                                                                                                                                                                        join tblAppointments on tblAppointments.AppointmentID=tblInvoiceDetails.AppointmentId
                                                                                                                                                where tblAppointmentAssignEmployee.AppointmentID!='NULL'
                                                                                                                                                and tblInvoiceDetails.OfferDiscountDateTime!='NULL'
                                                                $sqlTempfrom
                                                                $sqlTempto
                                                                $empq and tblEmployees.EID = '" . $search_emp[0]['EID'] . "'  AND tblAppointments.Status=2 AND tblAppointments.IsDeleted=0
                                                                                                                                                                        group by tblAppointmentAssignEmployee.AppointmentID,ServiceID,QtyParam";

                                                                $RSdetails = $DB->query($sqldetails);

                                                                while ($rowdetails = $RSdetails->fetch_assoc()) {
                                                                    $target_invoice_data[$rowdetails['EID']][] = $rowdetails;
                                                                }

                                                                if (isset($target_invoice_data) && is_array($target_invoice_data) && count($target_invoice_data) > 0) {
                                                                    $appointment_ids = array();

                                                                    foreach ($target_invoice_data as $detailkey => $detailvalue) {
                                                                        foreach ($detailvalue as $invkey => $invvalue) {
                                                                            $tappointment_ids[$invvalue["AppointmentID"]] = $invvalue["AppointmentID"];
                                                                        }
                                                                    }
                                                                                                                                                                                                /*
                                                                                                                                                                                                 * get service amt
                                                                                                                                                                                                 */

                                                                    if (isset($tappointment_ids) && is_array($tappointment_ids) && count($tappointment_ids) > 0) {
                                                                        $apt_in_ids = implode(",", $tappointment_ids);
                                                                        $aptdate_q = "SELECT ServiceAmount,AppointmentID,ServiceID,service_amount_deduct_dis,super_gv_discount,qty,package_discount FROM tblAppointmentsDetailsInvoice WHERE AppointmentID IN(" . $apt_in_ids . ") AND PackageService=0";
                                                                        $aptdate_exe = $DB->query($aptdate_q);
                                                                        while ($aptdatedetails = $aptdate_exe->fetch_assoc()) {
                                                                            $tall_apt_service_amt[] = $aptdatedetails;
                                                                        }
                                                                        if (isset($tall_apt_service_amt) && is_array($tall_apt_service_amt) && count($tall_apt_service_amt) > 0) {
                                                                            foreach ($tall_apt_service_amt as $servdkey => $servvalue) {
                                                                                $tall_ser_amt[$servvalue['AppointmentID']][$servvalue['ServiceID']] = round(($servvalue['service_amount_deduct_dis'] / $servvalue['qty']),2);
                                                                                $super_gv_deduct[$servvalue['AppointmentID']][$servvalue['ServiceID']] = round(($servvalue['super_gv_discount'] / $servvalue['qty']),2);
                                                                                $package_discount_deduct[$servvalue['AppointmentID']][$servvalue['ServiceID']] = round(($servvalue['package_discount'] / $servvalue['qty']),2);
                                                                            }
                                                                        }
                                                                    }


                                                                                                                                                                                                /*
                                                                                                                                                                                                 * get discount
                                                                                                                                                                                                 */
                                                                    if (isset($tappointment_ids) && is_array($tappointment_ids) && count($tappointment_ids) > 0) {
                                                                        $apt_in_ids = implode(",", $tappointment_ids);
                                                                        $aptdate_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ")";
                                                                        $aptdate_exe = $DB->query($aptdate_q);
                                                                        while ($disdetails = $aptdate_exe->fetch_assoc()) {
                                                                            $tall_apt_dis_amt[] = $disdetails;
                                                                        }

                                                                                                                                                                                                    /*
                                                                                                                                                                                                     * Get Appointment Offer AMount
                                                                                                                                                                                                     */

                                                                        $apt_in_ids = implode(",", $tappointment_ids);
                                                                        $aptoffer_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID,OfferID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ") AND OfferAmount > 0";
                                                                        $aptoffer_exe = $DB->query($aptoffer_q);
                                                                        while ($offeretails = $aptoffer_exe->fetch_assoc()) {
                                                                            $tapt_service_count[$offeretails['AppointmentID']][$offeretails['ServiceID']] = $offeretails['OfferAmount'];
                                                                        }

                                                                        $aptoffer_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID,OfferID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ") AND OfferAmount > 0";
                                                                        $aptoffer_exe = $DB->query($aptoffer_q);
                                                                        while ($offeretails = $aptoffer_exe->fetch_assoc()) {
                                                                            //$tall_apt_off_dis[$offeretails['AppointmentID']][$offeretails['ServiceID']] = $offeretails['OfferAmount'];
                                                                            if (isset($all_offers_name[$offeretails['OfferID']]) && $all_offers_name[$offeretails['OfferID']]['Type'] == 1 && $offeretails['OfferID'] > 0) {
                                                                                $toffer_service_cnt = isset($tapt_service_count[$offeretails['AppointmentID']]) ? count($tapt_service_count[$offeretails['AppointmentID']]) : 1;
                                                                                $tall_apt_off_dis[$offeretails['AppointmentID']][$offeretails['ServiceID']] = $offeretails['OfferAmount'] / $toffer_service_cnt;
                                                                            } else {
                                                                                $tall_apt_off_dis[$offeretails['AppointmentID']][$offeretails['ServiceID']] = $offeretails['OfferAmount'];
                                                                            }
                                                                        }


                                                                        if (isset($tall_apt_dis_amt) && is_array($tall_apt_dis_amt) && count($tall_apt_dis_amt) > 0) {
                                                                            foreach ($tall_apt_dis_amt as $diskey => $disvalue) {
                                                                                                                                                                                                            /*
                                                                                                                                                                                                             * Get Appointment service count
                                                                                                                                                                                                             */
                                                                                $extra_offer_amount = 0;

                                                                                if (isset($tall_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']]) && $tall_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']] > 0) {
                                                                                    $extra_offer_amount = $tall_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']];
                                                                                }
                                                                                $tall_dis_amt[$disvalue['AppointmentID']][$disvalue['ServiceID']]['MemberShipAmount'] = $disvalue['MemberShipAmount'] + round($extra_offer_amount, 2);
                                                                            }
                                                                        }
                                                                    }
                                                                }


                                                                $strEID = $search_emp[0]['EID'];
                                                                $emp_data[0] = isset($emp_id_data[$strEID]) ? $emp_id_data[$strEID] : array();

                                                                $tTotalAfterDivideSale = '';
                                                                $tTotalUltimateSale = 0;


                                                                if (isset($target_invoice_data[$strEID]) && is_array($target_invoice_data[$strEID]) && count($target_invoice_data[$strEID]) > 0) {

                                                                    $rowstrTotalAmount = 0;
                                                                    $row_invoice = 0;
                                                                    $rowComFinal = 0;
                                                                    foreach ($target_invoice_data[$strEID] as $rowkey => $rowdetails) {

                                                                        $strEID = $search_emp[0]['EID'];
                                                                        $strAID = $rowdetails["AppointmentID"];
                                                                        $strSID = $rowdetails["ServiceID"];
                                                                        $rowstrAmount = isset($tall_ser_amt[$strAID][$strSID]) ? $tall_ser_amt[$strAID][$strSID] : '';
                                                                        $rowstrTotalAmount += $rowstrAmount;
                                                                        $qty = $rowdetails["Qty"];

                                                                        $tstrCommission = $rowdetails["Commission"];
                                                                        if ($tstrCommission == "1") {
                                                                            $tAfterDivideSale = $rowstrAmount;
                                                                        } elseif ($tstrCommission == "2") {
                                                                            $tAfterDivideSale = ($rowstrAmount / 2);
                                                                        }

                                                                        $trowdiscount = isset($tall_dis_amt[$strAID][$strSID]) ? $tall_dis_amt[$strAID][$strSID] : array();
                                                                        if (isset($trowdiscount) && is_array($trowdiscount) && count($trowdiscount) > 0) {
                                                                            $strOfferAmount = $trowdiscount["OfferAmount"];
                                                                            $strDiscountAmount = $trowdiscount["MemberShipAmount"];
                                                                            if ($strOfferAmount > 0) {
                                                                                $tFinalDAmount = $tstrOfferAmount;
                                                                            } elseif ($strDiscountAmount > 0) {
                                                                                $tFinalDAmount = $strDiscountAmount;
                                                                            }
                                                                        } else {
                                                                            $tFinalDAmount = "0";
                                                                        }

                                                                        $FinalDiscount = $tFinalDAmount / $qty;

                                                                        //$trowUltimateSale = $tAfterDivideSale - $FinalDiscount;
                                                                        $trowUltimateSale = $rowstrAmount;
                                                                        $tTotalUltimateSale += $trowUltimateSale; //Total of discounted amount
                                                                    }
                                                                }

                                                                $com_perc = 0;
                                                                if (isset($total_target) && $total_target != '' && $total_target != 0) {
                                                                    if ($tTotalUltimateSale > $total_target) {
                                                                        $com_perc = 100;
                                                                    } else {
                                                                        $com_perc = ($tTotalUltimateSale * 100) / $total_target;
                                                                    }
                                                                }
                                                                ?>
                                        <div id="printdata">
                                            <div style="overflow-x: auto;width:100%;">
                                                <table id="printdata" class="table table-striped table-bordered table-responsive no-wrap printdata" cellspacing="0" width="100%">

                                                    <thead>
                                                        <tr>
                                                            <td colspan="3"><b>Name :</b><?php echo $strEmployeeName; ?></td>
                                                            <td colspan="4"><b>Email :</b><?php echo $strEmployeeEmailID; ?></td>
                                                            <td colspan="5"><b>Mobile no. :</b><?php echo $strEmployeeMobileNo; ?></td>
                                                            <td><b>Commission Percentage :</b><?php echo $strEmpPercentage; ?></td>
                                                        </tr>

                                                        <tr>
                                                            <?php
                                                            $da_target = isset($total_target) ? $total_target : 0;
                                                            $da_completed = isset($tTotalUltimateSale) ? $tTotalUltimateSale : 0;
                                                            $remaining = $da_target - $da_completed;
                                                            if ($remaining > 0) {
                                                                                                                                                                                                        /*
                                                                                                                                                                                                         * Get Number Of Remaining Days
                                                                                                                                                                                                         */
                                                                $timestamp = strtotime(date('Y-m-d'));
                                                                $daysRemaining = (int) date('t', $timestamp) - (int) date('j', $timestamp);
                                                                $daysRemaining = $daysRemaining + 1;
                                                                $daily_target = $remaining / $daysRemaining;
                                                            }
                                                            ?>
                                                            <td colspan="3"><b>Target Amount : </b><?php echo isset($total_target) ? $total_target : 0; ?></td>
                                                            <td colspan="5"><b>Completed Target : </b><?php echo $tTotalUltimateSale . ' (' . round($com_perc, 2) . '%)'; ?></td>
                                                            <td colspan="5"><b>Daily Target : </b><?php echo isset($daily_target) ? round($daily_target, 2) : 0; ?></td>

                                                        </tr>
                                                        <tr>
                                                            <th>Sr. No.</th>
                                                            <?php /* <th># Invoice No</th>
                                                                                                                                                                                                      <th>Store</th> */ ?>
                                                            <th>Client Name</th>
                                                            <th>Bill Date</th>
                                                            <?php /* <th>Commission Date</th> */ ?>
                                                            <th>Service(s) Done</th>
                                                            <th>Service Amount</th>
                                                            <?php /* <th>Final Amount</th> */ ?>
                                                            <th>MemberShip Discount</th>
                                                            <th>Other Discount</th>
                                                            <th>Super GV Deduct</th>
                                                            <th>Package Discount</th>
                                                            <th>Net Amount</th>
                                                            <th>Tax Amount</th>
                                                            <th>Total Sales</th>
                                                            <th>Service Type</th>
                                                            <?php /* <th>Commission Amount</th>
                                                                                                                                                                                                      <th>Commission Type</th> */ ?>

                                                        </tr>
                                                    </thead>

                                                    <tbody>

                                                                                <?php
                                                                                if ($_SERVER['REMOTE_ADDR'] == "111.119.219.70") {
                                                                                    //echo $sqldetails;
                                                                                } else {

                                                                                }

                                                                                $counter = 0;
                                                                                $strSID = "";
                                                                                $qty = "";
                                                                                $strSAmount = "";
                                                                                $strAmount = "";
                                                                                $strCommission = "";
                                                                                $FinalDAmount = '';
                                                                                $FinalDiscount = '';
                                                                                $FinalOtherDiscount = 0;
                                                                                $TotalOtherDiscount = 0;
                                                                                $UltimateSale = '';
                                                                                $AfterDivideSale = '';
                                                                                $CommssionFinal = "";
                                                                                //echo("Error description: " . mysqli_error($DB));


                                                                                $final_res = array();
                                                                                $rowstrTotalAmount = 0;
                                                                                $row_invoice = 0;
                                                                                $apt_id = 0;
                                                                                $rowComFinal = 0;
                                                                                $total_gv_deduct = 0;
                                                                                $total_package_dis_deduct = 0;
                                                                                $grand_total_tax = 0;
                                                                                $grand_total_sale_amount = 0;


                                                                                foreach ($invoice_data[$strEID] as $rowkey => $rowdetails) {
                                                                                    $strAID = $rowdetails["AppointmentID"];
                                                                                    $strSID = $rowdetails["ServiceID"];
                                                                                    $rowstrAmount = isset($all_ser_amt[$strAID][$strSID]) ? $all_ser_amt[$strAID][$strSID] : '';
                                                                                    $rowstrTotalAmount += $rowstrAmount;
                                                                                    $qty = $rowdetails["Qty"];

                                                                                    if ($apt_id != $rowdetails['AppointmentID']) {
                                                                                        $row_invoice = $row_invoice + 1;
                                                                                    }
                                                                                    $apt_id = $rowdetails['AppointmentID'];

                                                                                    $strEmpPercentage = $emp_data[0]["EmpPercentage"];

                                                                                    $strCommission = $rowdetails["Commission"];
                                                                                    if ($strCommission == "1") {
                                                                                        $AfterDivideSale = $rowstrAmount;
                                                                                    } elseif ($strCommission == "2") {
                                                                                        $AfterDivideSale = ($rowstrAmount / 2);
                                                                                    }

                                                                                    $rowdiscount = isset($all_dis_amt[$strAID][$strSID]) ? $all_dis_amt[$strAID][$strSID] : array();
                                                                                    if (isset($rowdiscount) && is_array($rowdiscount) && count($rowdiscount) > 0) {
                                                                                        $strOfferAmount = $rowdiscount["OfferAmount"];
                                                                                        $strDiscountAmount = $rowdiscount["MemberShipAmount"];

                                                                                        if ($strOfferAmount > 0) {
                                                                                            $FinalDAmount = $strOfferAmount;
                                                                                        } elseif ($strDiscountAmount > 0) {
                                                                                            $FinalDAmount = $strDiscountAmount;
                                                                                        }
                                                                                    } else {
                                                                                        $FinalDAmount = "0";
                                                                                    }

                                                                                    $FinalDiscount = $FinalDAmount / $qty;

                                                                                    $rowUltimateSale = $AfterDivideSale - $FinalDiscount;
                                                                                    if ($strCommission == "1") {
                                                                                        $rowCommssionFinal = ($rowUltimateSale / 100) * $strEmpPercentage;
                                                                                    } elseif ($strCommission == "2") {
                                                                                        $rowCommssionFinal = ($rowUltimateSale / 200) * $strEmpPercentage;
                                                                                    }
                                                                                    $rowComFinal += $rowCommssionFinal;

                                                                                    $final_res[$strEID][$rowdetails['AppointmentID'] . '-' . $rowdetails["ServiceID"]] = $rowdetails;

                                                                                    if (isset($emp_service_qty[$rowdetails['AppointmentID']][$rowdetails["ServiceID"]][$strEID])) {
                                                                                        $emp_service_qty[$rowdetails['AppointmentID']][$rowdetails["ServiceID"]][$strEID] +=1;
                                                                                    } else {
                                                                                        $emp_service_qty[$rowdetails['AppointmentID']][$rowdetails["ServiceID"]][$strEID] = 1;
                                                                                    }

                                                                                    $apt_service_qty[$rowdetails['AppointmentID']][$rowdetails["ServiceID"]] = $rowdetails['Qty'];
                                                                                }
                                                                                ?>
                                                                                <?php /* <div class="col-md-12 emp-com">
                                                                                                                                                                                                                                                              <div class="col-md-4 no-pad">
                                                                                                                                                                                                                                                              <h3>Total Invoice : </h3> <b><span class="text-danger"><?php echo $row_invoice; ?></span></b>
                                                                                                                                                                                                                                                              </div>
                                                                                                                                                                                                                                                              <div class="col-md-4 no-pad">
                                                                                                                                                                                                                                                              <h3>Total Sales Amount : </h3> <b><span class="text-danger"><?php echo $rowstrTotalAmount; ?></span></b>
                                                                                                                                                                                                                                                              </div>
                                                                                                                                                                                                                                                              <div class="col-md-4 no-pad">
                                                                                                                                                                                                                                                              <h3>Total Commission : </h3> <b><span class="text-danger"><?php echo $rowComFinal; ?></span></b>
                                                                                                                                                                                                                                                              </div>
                                                                                                                                                                                                                                                              </div> */ ?>
                                                                                <?php
                                                                                // foreach ($invoice_data[$strEID] as $rowkey => $rowdetails) {


                                                                                foreach ($final_res[$strEID] as $rowkey => $rowdetails) {
                                                                                    $strEIDa = $rowdetails["EID"];
                                                                                    $strAID = $rowdetails["AppointmentID"];
                                                                                    $strSID = $rowdetails["ServiceID"];
                                                                                    $CustomerID = $rowdetails['CustomerID'];
                                                                                    $qty = $rowdetails["Qty"];
                                                                                    $service_type = '';
                                                                                    $row_existq = "SELECT * FROM emp_reconcillation WHERE emp_id='" . $strEID . "'"
                                                                                    . " AND appointment_id = '" . $strAID . "' AND status=1 AND marked_status=2 AND DATE_FORMAT(created_date,'%Y-%m-%d') = '" . date('Y-m-d') . "'";
                                                                                    $row_existq_exe = $DB->query($row_existq);
                                                                                    if ($row_existq_exe->num_rows == 0) {
                                                                                        $counter ++;

                                                                                        $emp_ser_count = isset($emp_service_qty[$strAID][$strSID][$strEID]) ? $emp_service_qty[$strAID][$strSID][$strEID] : 1;
                                                                                        $apt_ser_count = isset($apt_service_qty[$strAID][$strSID]) ? $apt_service_qty[$strAID][$strSID] : 1;

                                                                                        // $strAmount = isset($all_ser_amt[$strAID][$strSID]) ? $all_ser_amt[$strAID][$strSID] * $qty : '';
                                                                                        $strAmount = isset($all_ser_amt[$strAID][$strSID]) ? ($all_ser_amt[$strAID][$strSID] * $qty * $emp_ser_count) / $apt_ser_count : '';
                                                                                        $rowstrAmount = isset($tall_ser_amt[$strAID][$strSID]) ? $tall_ser_amt[$strAID][$strSID] * $emp_ser_count : '';
                                                                                        $super_gv_deduct_amount = isset($super_gv_deduct[$strAID][$strSID]) ? $super_gv_deduct[$strAID][$strSID] * $emp_ser_count : '';
                                                                                        $package_discount_deduct_amount = isset($package_discount_deduct[$strAID][$strSID]) ? $package_discount_deduct[$strAID][$strSID] * $emp_ser_count : '0';

                                                                                        $total_gv_deduct += $super_gv_deduct_amount;
                                                                                        $total_package_dis_deduct += $package_discount_deduct_amount;
                                                                                        $strSAmount = $rowstrAmount;
                                                                                        $strCommission = $rowdetails["Commission"];
                                                                                        $StoreIDd = $rowdetails["StoreID"];

                                                                                        $strTotalAmount += $strAmount;  //Total of Service sale amount
                                                                                        $servicename = isset($sep[$strSID]) ? $sep[$strSID] : '';

                                                                                        $StoreName = isset($stpp[$StoreIDd]) ? $stpp[$StoreIDd] : '';

                                                                                        $Invoice_Number = isset($sql_invoice_number[$strAID]) ? $sql_invoice_number[$strAID] : '';

                                                                                        $AppointmentDate = isset($sql_apt_date[$strAID]) ? $sql_apt_date[$strAID] : '';


                                                                                        $CustomerFullName = isset($sql_customer[$CustomerID]) ? $sql_customer[$CustomerID] : '';


                                                                                        if ($strCommission == "1") {
                                                                                            $AfterDivideSale = $strSAmount;
                                                                                            $strCommissionType = '<span class="bs-label label-success">Alone</span>';
                                                                                        } elseif ($strCommission == "2") {
                                                                                            //$AfterDivideSale = ($strSAmount / 2);
                                                                                            $AfterDivideSale = ($strSAmount);
                                                                                            $strCommissionType = '<span class="bs-label label-blue-alt">Split</span>';
                                                                                        }
                                                                                        $TotalAfterDivideSale += $AfterDivideSale;  //Total of Final payment
                                                                                        $FinalDiscount = isset($all_apt_mem_dis[$strAID][$strSID]) ? ($all_apt_mem_dis[$strAID][$strSID] * $emp_ser_count) / $apt_ser_count : 0;
                                                                                        $FinalOtherDiscount = isset($all_apt_off_dis[$strAID][$strSID]) ? ($all_apt_off_dis[$strAID][$strSID] * $emp_ser_count) / $apt_ser_count : 0;

                                                                                        //echo "<br>discount=" . $FinalOtherDiscount . "&appointment_id=" . $strAID;
                                                                                        $TotalFinalDiscount += $FinalDiscount; //Total of discounted amount
                                                                                        $TotalOtherDiscount += $FinalOtherDiscount; //Total of discounted amount
                                                                                        //$UltimateSale = $AfterDivideSale - $FinalDiscount - $FinalOtherDiscount;
                                                                                        $UltimateSale = $AfterDivideSale;

                                                                                        $TotalUltimateSale += $UltimateSale; //Total of discounted amount
                                                                                        //Calculation for commission
                                                                                        if ($strCommission == "1") {
                                                                                            $CommssionFinal = ($UltimateSale / 100) * $strEmpPercentage;
                                                                                        } elseif ($strCommission == "2") {
                                                                                            $CommssionFinal = ($UltimateSale / 200) * $strEmpPercentage;
                                                                                        }

                                                                                        if ($strCommission == "1") {
                                                                                            $CommssionFinal = ($UltimateSale / 100) * $strEmpPercentage;
                                                                                            $service_type = 'Alone';
                                                                                        } elseif ($strCommission == "2") {
                                                                                            $CommssionFinal = ($UltimateSale / 200) * $strEmpPercentage;
                                                                                            $UltimateSale = $UltimateSale / 2;
                                                                                            $FinalDiscount = $FinalDiscount / 2;
                                                                                            $FinalOtherDiscount = $FinalOtherDiscount / 2;
                                                                                            $super_gv_deduct_amount = $super_gv_deduct_amount / 2;
                                                                                            $strAmount = $strAmount / 2;
                                                                                            $service_type = 'Split';
                                                                                        }

                                                                                        $ser_tax_amount = ($UltimateSale * 0.18 );
                                                                                        $total_sale_tax = round($ser_tax_amount) + $UltimateSale;

                                                                                        if($package_discount_deduct_amount > 0){
                                                                                            $ser_tax_amount = 0;
                                                                                            $total_sale_tax = 0;
                                                                                        }
                                                                                        $grand_total_tax += $ser_tax_amount;
                                                                                        $grand_total_sale_amount += $total_sale_tax;

                                                                                        $ComFinal += $CommssionFinal; //Total of Commission
                                                                                        ?>
                                                        <tr id="my_data_tr_<?= $counter ?>">
                                                            <td><?= $counter; ?>

                                                                <input type="checkbox" class="reconciile_check" value="<?php echo $strEIDa . '/' . $strAID ?>"/>



                                                            </td>
                                                            <?php /* <td>#<?= $Invoice_Number ?></td>
                                                                                                                      <td><?= $StoreName ?></td> */ ?>
                                                            <td><?= $CustomerFullName ?></td>
                                                            <td><?php echo isset($rowdetails['OfferDiscountDateTime']) ? date('d/m/Y', strtotime($rowdetails['OfferDiscountDateTime'])) : '' ?></td>
                                                            <?php /* <td><?= date("d/m/Y", strtotime($AppointmentDate)); ?></td> */ ?>
                                                            <td><?= $servicename ?></td>
                                                            <td>Rs.<?= $UltimateSale ?>/-</td>
                                                            <?php /* <td>Rs.<?= $AfterDivideSale ?>/-</td> */ ?>
                                                            <td>Rs.<?= $FinalDiscount ?>/-</td>
                                                            <td>Rs.<?= $FinalOtherDiscount ?>/-</td>
                                                            <td>Rs.<?= $super_gv_deduct_amount ?>/-</td>
                                                            <td>Rs.<?= $package_discount_deduct_amount ?>/-</td>
                                                            <td>Rs.<?= $strAmount ?>/-</td>
                                                            <td>Rs.<?= $ser_tax_amount ?>/-</td>
                                                            <td>Rs.<?= $total_sale_tax ?>/-</td>
                                                            <td><?php echo $service_type; ?></td>
                                                            <?php /* <td>Rs.<?= $CommssionFinal ?>/-</td>
                                                                                                                      <td><font color="red"><?= $strCommissionType ?></font></td> */ ?>

                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>

                                                    <?php }
                                                ?>

                                                        <tr>
                                                            <td colspan="4"><b><center>Total commission for selected period : </center></b></td>

                                                            <td><b>Rs.<?= $TotalUltimateSale ?>/-</b></td>
                                                            <?php /* <td><b>Rs.<?= $TotalAfterDivideSale ?>/-</b></td> */ ?>
                                                            <td><b>Rs.<?= $TotalFinalDiscount ?></b></td>
                                                            <td><b>Rs.<?= $TotalOtherDiscount ?>/-</b></td>
                                                            <td><b>Rs.<?= $total_gv_deduct ?>/-</b></td>
                                                            <td><b>Rs.<?= $total_package_dis_deduct ?>/-</b></td>
                                                            <td><b>Rs.<?= $strTotalAmount ?>/-</b></td>
                                                            <td><b>Rs.<?= $grand_total_tax ?>/-</b></td>
                                                            <td><b>Rs.<?= $grand_total_sale_amount ?>/-</b></td>
                                                            <td></td>
                                                            <?php /* <td><b>Rs.<?= $ComFinal ?>/-</b></td> */ ?>

                                                        </tr>
                                                    </tbody>
                                                </table>
                                        </div></div>
                                        <br><br>
                                        <?php
                                    }
                                }
                            }
                        }
                    } else {
                        echo "No Employees found...";
                    }

                    $DB->close();
                } else {
                    echo "<br><center><h3>Please Select Month And Year!</h3></center>";
                }
                ?>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php require_once 'incFooter.fya'; ?>

        </div>


        <script>
            var data = [
<?php
if (isset($csvdata) && is_array($csvdata) && count($csvdata)) {
    foreach ($csvdata as $key => $value) {
        echo "['" . $key . "','" . $value['name'] . "','" . $value['mobile'] . "','" . $value['remark_type'] . "','" . $value['comment'] . "'],";
    }
}
?>
    ];
    function download_csv() {
        var csv = 'Sr,Customer Name,Customer Mobile,Remark Type,Comment\n';
        data.forEach(function (row) {
            csv += row.join(',');
            csv += "\n";
        });
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        hiddenElement.target = '_blank';
        hiddenElement.download = 'non_visiting_customer.csv';
        hiddenElement.click();
    }
        </script>

        <script>
            $(document).ready(function () {
                var totalCheckboxes = $('input:checkbox').length;
                if (totalCheckboxes == 0) {
                    $("#reconcillation_confirm").hide();
                }
            });
        </script>

    </body>

</html>