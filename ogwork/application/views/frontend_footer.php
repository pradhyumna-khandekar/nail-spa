
<!-- footer starts here -->
<footer id="footer">
    <div class="footer-widgets-wrapper">
        <div class="container">
            <div class="column dt-sc-one-fourth first">
                <aside class="widget widget_text">
                    <div class="textwidget">
                        <h3 class="widgettitle"><span class="fa fa-user"></span>About Us</h3>
                        <p>Sixpackabsindia.com, a fitness website providing fitness services in india . SixPackAbsIndia.com is the website which is providing unique and expert Blog articles on Yoga, Rehab, sports conditioning, Physiotherapy, Cross fit exercises, Diet plan, Videos by Indian trainers , dietitians for Indian people Also, it is the only one of its kind Search Engine for Gyms and trainers from all over country .</p>

                    </div>
                </aside>
            </div>
            <div class="column dt-sc-one-fourth">
                <aside class="widget widget_text">
                    <h3 class="widgettitle"><span class="fa fa-link"></span>Useful Links</h3>
                    <div class="textwidget">
                        <ul>
                            <li><a href="<?php echo site_url('videos'); ?>">Workout Videos</a></li>
                            <li><a href="<?php echo site_url('blog'); ?>">Blogs</a></li>
                            <li><a href="<?php echo site_url('terms-conditions'); ?>">Terms & Conditions</a></li>
                            <li><a href="<?php echo site_url('privacy'); ?>">Privacy Policy</a></li>
                            <li><a href="<?php echo site_url('about-us'); ?>">About Us</a></li>
                        </ul>
                    </div>
                </aside>
            </div>
            <div class="column dt-sc-one-fourth">
                <aside class="widget widget_contact">
                    <h3 class="widgettitle"><span class="fa fa-map-marker"></span>Contact Us</h3>
                    <p><i class="fa fa-rocket"></i><span class="org">Six Pack Abs India</span></p>
                    <p class="address1"><span> Royal Palms, Aarey Colony,</span><br><span>Goregoan,Mumbai</span><br><span>Maharashtra 400065</span><br><span>India</span></p>
                    <p class="tel"><i class="fa fa-phone"></i><a href="tel:+919988661099">+91 9988661099</a></p>

                    <p><i class="fa fa-envelope"></i><a  class="email"  href="mailto:info@sixpackabsindia.com"> info@sixpackabsindia.com</a></p>
                    <p><i class="fa fa-globe"></i><a  class="url fn n"  href="http://www.sixpackabsindia.com" target="_blank"> sixpackabsindia.com</a></p>
                </aside>
            </div>
            <div class="column dt-sc-one-fourth">
                <aside class="widget widget_recent_entries">
                    <h3 class="widgettitle"><span class="fa fa-calendar"></span>Upcoming Events</h3>
                    <div class="recent-posts-widget" id="event_wrapper">

                    </div>
                    <a href="<?php echo site_url('events'); ?>">View All Events</a>
                </aside>
            </div>
        </div>
        <div class="social-media-container">
            <div class="social-media">
                <div class="container">
                    <div class="dt-sc-contact-info dt-phone dt-sc-one-third column first">
                        <p><i class="fa fa-phone"></i> <a href="tel:+919988661099" title="Call Us"><span>+919988661099</span></a> </p>
                    </div>
                    <div class="text-center dt-sc-one-third column poweredby">
                        <p>&copy; 2017 - SixPackAbsIndia. <br>Powered By&nbsp;<a href="http://www.orggen.com/" target="_blank">&nbsp;OrgGen</a></p>
                    </div>
                    <div class=" dt-sc-one-third  column">
                        <ul class="dt-sc-social-icons">
                            <li class="instagram"><a  target="_blank" href="https://www.instagram.com/sixpackabsindia/" class="fa fa-instagram"></a></li>
                            <li class="facebook"><a target="_blank" href="https://www.facebook.com/sixpackabsindia/" class="fa fa-facebook"></a></li>
                            <li class="youtube"><a  target="_blank" href="https://www.youtube.com/channel/UC-oSJYTdaqRDQhpMp_rC3Cw" class="fa fa-youtube"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer ends here -->
</div>
<!-- **Inner Wrapper - End** -->
</div>
<!-- **Wrapper - End** -->

<script type="text/javascript" src="<?php echo JS_PATH_FRONTEND; ?>jquery.ui.totop.min.js"></script>

<script type="text/javascript" src="<?php echo JS_PATH_FRONTEND; ?>custom.js"></script>
<script type="text/javascript">
    var x = document.getElementById("demo");

    function getLocation() {

        if (navigator.geolocation) {
            //alert('footer_163');
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            x.innerHTML = "Geolocation is not supported by this browser.";
        }
    }

    function showPosition(position) {
        //alert(position.coords.latitude);
        //alert(position.coords.latitude);
        //console.log(position);
        document.getElementById('lat').value = position.coords.latitude;
        document.getElementById('long').value = position.coords.longitude;
        var url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + position.coords.latitude + ',' + position.coords.longitude + '&sensor=false';
        var response = httpGet(url);
        var startIndex = response.indexOf('formatted_address') + 'formatted_address" : "'.length;
        var endIndex = response.indexOf('",', startIndex);
        var location = response.substring(startIndex, endIndex);
        //alert(location);
        /*jQuery.ajax({
         type: "POST",
         url: "<?php echo site_url('login/set_geo_data'); ?>",
         data: {lat: position.coords.latitude, long: position.coords.longitude, location: location},
         success: function (data) {
         
         },
         error: function (data) {
         
         }
         });*/

    }
    function httpGet(theUrl)
    {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("GET", theUrl, false); // false for synchronous request
        xmlHttp.send(null);
        return xmlHttp.responseText;
    }

    getLocation();
    /*
     * get blog content for menu
     
     * @param {type} param     */
    jQuery(document).ready(function () {
        jQuery.ajax({
            type: "POST",
            url: "<?php echo site_url('blog/get_blog'); ?>",
            success: function (data) {
                jQuery("ul .sub-menu").html(data);
            },
            error: function (data) {

            }
        });
        jQuery.ajax({
            type: "POST",
            url: "<?php echo site_url('event/event_admin/get_event'); ?>",
            success: function (data) {
                jQuery("#event_wrapper").html();
                jQuery("#event_wrapper").html(data);
            },
            error: function (data) {

            }
        });
        jQuery('img').each(function () {
            var val = jQuery(this).attr('data-src');
            jQuery(this).removeAttr('data-src').attr('src', val);
        });
        setTimeout(function () {
            if (jQuery('p.error').length > 0) {
                jQuery('p.error').remove();
            }
        }, 5000);
    });
</script>
<script type="text/javascript">

    function initAutocomplete() {
        //alert('sfdsfd');
        // restrict cities of india only
        var locality_options = {
            componentRestrictions: {country: "in"}
        }


        // Create the input box and link it to the UI element.
        var input = document.getElementById('locality');
        var locality = new google.maps.places.Autocomplete(document.getElementById('locality'), locality_options);

        google.maps.event.addListener(locality, 'place_changed', function (e) {

            var place = locality.getPlace();
            if (!place.geometry) {
                document.getElementById('locality').value = '';
                document.getElementById('lat').value = '';
                document.getElementById('long').value = '';
                return false;
            } else
            {
                document.getElementById('location').value = place.formatted_address;
                document.getElementById('latitude').value = place.geometry.location.lat();
                document.getElementById('longitude').value = place.geometry.location.lng();
                document.getElementById('locality').value = place.formatted_address;
                return true;
            }
        });
    }

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1HmR4E0rIzBpPi0nF0NZgRJYF42_Kuig&libraries=places">
</script>
<style>
    .pac-logo:after{background-image:none;}
</style>

<?php if (ENVIRONMENT == 'production') { ?>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-78364523-17', 'auto');
        ga('send', 'pageview');

    </script>
<?php } ?>
<?php $this->output->enable_profiler(FALSE); ?>
</body>
</html>