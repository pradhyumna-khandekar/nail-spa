<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <div class="user-panel">
        <div class="pull-left image">
            <img title="Sixpackabsindia" alt="Sixpackabsindia" src="<?php echo IMAGE_PATH_FRONTEND; ?>frontend/logo.jpg">
        </div>
        <div class="pull-left info">
            <p><?php echo ucwords($this->session->userdata['admin_user']['name']); ?></p>

        </div>
    </div>

    <section class="sidebar" style="height: auto;">
        <!-- Sidebar user panel -->

        <!-- search form -->

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>

            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Trainer Management</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('trainer/trainer_admin'); ?>"><i class="fa fa-circle-o"></i> View All</a></li>
                </ul>

            </li>

            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Gym Management</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('gym/gym_admin'); ?>"><i class="fa fa-circle-o"></i> View All</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Enquiry Management</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('contact_us/contact_us_admin'); ?>"><i class="fa fa-circle-o"></i> View All</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Newsletter Management</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('newsletter/newsletter_admin'); ?>"><i class="fa fa-circle-o"></i> View All</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Blog Management</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('blog/blog_admin'); ?>"><i class="fa fa-circle-o"></i> View All</a></li>
                </ul>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('blog/blog_admin/add'); ?>"><i class="fa fa-circle-o"></i> Add</a></li>
                </ul>
            </li>
            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Blog Category Management</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('blog/blog_category_admin'); ?>"><i class="fa fa-circle-o"></i> View All</a></li>
                </ul>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('blog/blog_category_admin/add'); ?>"><i class="fa fa-circle-o"></i> Add</a></li>
                </ul>
            </li>
            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Event Management</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('event/event_admin'); ?>"><i class="fa fa-circle-o"></i> View All</a></li>
                </ul>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('event/event_admin/add'); ?>"><i class="fa fa-circle-o"></i> Add</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>User Management</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('user/user_admin'); ?>"><i class="fa fa-circle-o"></i> View All</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="<?php echo site_url(); ?>login/admin_login/logout">Log out</a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
<div class="content-wrapper" style="min-height: 946px;">