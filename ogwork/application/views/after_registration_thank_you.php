<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
        <style>
            p{margin:3px;color:#0D1F35;}
            tr, td{font-size: 14px;}
            th{color:#795548;padding:5px;}
            h1{margin: 0;text-align:center;font-size:25px;color:#607D8B;}
            h2{font-size:16px;}
            button{}
            .main-table{background-color:#fafafa;font-family: 'Dosis', sans-serif; font-size:14px; margin:0; padding:0;}
            .report-table{border-collapse: collapse;display:block;text-align:center;}
            .report-table tr{border-bottom: 2px solid #795548;}
            .report-table tr td{padding:5px;}
        </style>
    </head>
    <body>
        <table  class="main-table" style="padding:0;border-spacing: 0;width: 100%;height: 100%;border: 0;">
            <tr>
                <td align="center" valign="top" style="padding:20px 0 20px 0">
                    <table bgcolor="#FFFFFF" style="border:3px dashed #0D1F35;padding:10px;border-spacing: 0;width: 600px;">
                        <!--<tr>
                            <td style="padding: 5px 15px 0;background-color:#f5f5f5;">
                                <img src="http://test-bed.orggen.com/cdn/phone.png" alt="+917678020068" style="-ms-interpolation-mode: bicubic; border: 0; display: inline; outline: 0; text-decoration: none; width: 20px;" width="20" border="0">
                                <a href="tel:+91 (022) 28395599 / 28393344" style="position:relative;top:-5px;color: rgb(123, 123, 123);border: none; display: inline; outline: none 0px; text-decoration: none; cursor: pointer; box-sizing: content-box;font-size: 14px;">
                                    +91 (022) 28395599 / 28393344
                                </a></td>


                            <td style="padding: 5px 15px 0;background-color:#f5f5f5;">
                                <img src="http://test-bed.orggen.com/cdn/mail.png" alt="info@vastunirmaninfra.com" style="-ms-interpolation-mode: bicubic; border: 0; display: inline; outline: 0; padding-left: 20px; text-decoration: none; width: 20px;" width="20" border="0">
                                <a href="mailto:info@vastunirmaninfra.com" style="position:relative;top:-5px;color: rgb(123, 123, 123);border: none; display: inline; outline: none 0px; text-decoration: none; cursor: pointer; box-sizing: content-box;font-size: 14px;">
                                    info@vastunirmaninfra.com
                                </a></td>
                        </tr>-->
                        <tr>
                            <td colspan="2" style="padding:0;">
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td colspan="2" style="padding:15px;text-align:center;">
                                            <img src="<?php echo IMAGE_PATH_FRONTEND . 'frontend/logo.jpg'; ?>" style="width:215px;height:55px;">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="">
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td colspan="2" style="text-align:center;background: #fafafa;">
                                            <h1 style="font-size: 5em;text-transform: uppercase;color:#20ADE8;">Thank You</h1>
                                            <h2 style="margin-top:0;border-top: 1px solid #0D1F35;border-bottom: 1px solid #0D1F35;color:#0D1F35;letter-spacing: 4px;display: inline-block;text-transform: uppercase;padding: 0 30px;">For registering with us</h2>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding:10px 30px 0;">
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td colspan="2">
                                            <img src="<?php echo $image; ?>" style="width:150px;height:130px;margin:0 auto;display:block;" alt="<?php echo ucwords($name); ?>">
                                            <h2>Hi <?php echo ucwords($name); ?>,</h2>
                                            <p>Thank you for trusting us to be your source for top quality service.</p>
                                            <p>We appreciate your kind expressions of appreciation, and are especially grateful for the recent registration on sixpackabsindia.</p>
                                            <p>You recently registered for your Sixpackabsindia account :  </p>
                                            <p><span style="font-weight:700;">Email :</span><?php echo $email; ?></p>
                                            <p><span style="font-weight:700;">Contact No. :</span><?php echo $contact_no; ?></p>
                                            <p><span style="font-weight:700;">Address :</span><?php echo $locality; ?></p>
                                            <p>Click the button below to view your profile.</p>
                                            <button type="button" class="text-center" style="font-size: 16px;display:block;margin:15px auto;background-color: #F44336;color: #fff;padding: 10px;border: none;border-radius: 8px;font-family: 'Dosis', sans-serif;"><a href="<?php echo site_url($sef); ?>" style="text-decoration:none;color:#fff;">View your Profile</a></button>
                                            <p>Thanks,</p>
                                            <p>Sixpackabsindia</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding:8px 30px;">
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td colspan="2" style="padding-bottom:15px; border-bottom: 1px solid #ddd;">
                                            <p><strong>P.S. </strong> We would also love hearing from you with any issues you have. Please reply to this email if you have any question.</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding:0 30px;">
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td colspan="2" style="padding-bottom:15px;">
                                            <p>If you are having trouble clicking the view profile button, copy and paste the url below into your web browser.</p>
                                            <p><a href="<?php echo site_url($sef); ?>" target="_blank"><?php echo site_url($sef); ?></a></p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding:10px;">
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td colspan="2" style="background-color:#f5f5f5;padding: 15px;text-align: center;">
                                            <p class="text-center">©2017 Sixpackabsindia.com | </p>
                                            <p style="font-size:16px;">Powered by <a href="http://www.orggen.com" target="_blank" style="color:#607D8B;text-decoration:none;font-weight:700;"> OrgGen Technologies</a></p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
