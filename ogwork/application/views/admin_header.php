
<!DOCTYPE html>
<html lang="en">
    <head>

        <title><?php echo WEBSITE_NAME; ?> : Admin Panel</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="noindex,nofollow" />
        <meta name="robots" content="noarchive" />

        <link rel="stylesheet" href="<?php echo CSS_PATH_BACKEND; ?>bootstrap.min.css">
        
        <link rel="stylesheet" href="<?php echo CSS_PATH_BACKEND; ?>AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo CSS_PATH_BACKEND; ?>_all-skins.min.css">
        <link rel="stylesheet" href="<?php echo CSS_PATH_BACKEND; ?>style.css">

        <link href="https://fonts.googleapis.com/css?family=Play" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->




        <!-- jQuery 2.2.0 -->
        <script src="<?php echo JS_PATH_BACKEND; ?>jquery.min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo JS_PATH_BACKEND;  ?>bootstrap.min.js"></script>


    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">

                <!-- Logo -->
                <a href="<?php echo site_url('adminx'); ?>" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini">
                    
                    </span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Sixpackabsindia</b></span>
                </a>

                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->

                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <!-- Navbar Right Menu -->


                </nav>
            </header>


