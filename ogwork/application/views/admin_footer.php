</div><!-- end of content-wrapper wrapper-->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <a href="http://www.orggen.com/" title="" target="_blank">Powered By: OrgGen</a>
    </div>
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 

<script src="<?php echo JS_PATH_BACKEND; ?>dist/js/app.min.js"></script>


<!-- AdminLTE App -->

</div><!-- end of main wrapper-->
<script type="text/javascript">

    $(document).ready(function () {
        setTimeout(function () {
            $('.feedback_msg').hide();
        }, 5000);
    });
</script>
</body>
</html>