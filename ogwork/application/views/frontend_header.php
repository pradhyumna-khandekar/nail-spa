<!doctype html>
<!--[if IE 7 ]>    
<html lang="en-gb" class="isie ie7 oldie no-js">
  <![endif]-->
<!--[if IE 8 ]>    
<html lang="en-gb" class="isie ie8 oldie no-js">
  <![endif]-->
<!--[if IE 9 ]>    
<html lang="en-gb" class="isie ie9 no-js">
  <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> 
<html lang="en-gb" class="no-js">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> Six Pack Abs India</title>
        <meta name="description" content="">
        <meta name="author" content="Six Pack Abs India - OrgGen">
        <meta http-equiv="cache-control" content="no-cache" />
        <noscript>
        <meta http-equiv="refresh" content="0; url=<?php echo site_url() . 'page_management/no_js'; ?>" />
        </noscript>

        <link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon" />

        <link href="<?php echo CSS_PATH_FRONTEND; ?>style.css" rel="stylesheet" media="all"  id="default-css"  />
        <link href="<?php echo CSS_PATH_FRONTEND; ?>shortcodes.css" rel="stylesheet" media="all" id="shortcodes-css" />
        <link href="<?php echo CSS_PATH_FRONTEND; ?>skins/blue/style.css" rel="stylesheet" media="all" id="skin-css" />
        
        <link href="<?php echo CSS_PATH_FRONTEND; ?>responsive.css" rel="stylesheet" media="all" />
        <link href="<?php echo CSS_PATH_FRONTEND; ?>custom.css" rel="stylesheet" media="all" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Exo+2" rel="stylesheet">

        <script type="text/javascript" src="<?php echo JS_PATH_FRONTEND; ?>jquery.js"></script>          
        <script type="text/javascript" src="<?php echo JS_PATH_FRONTEND; ?>jquery-migrate.min.js"></script>
        <script type="text/javascript" src="<?php echo JS_PATH_FRONTEND; ?>modernizr.custom.js"></script>

        <script>
            function myFunction() {
<?php
$mobile = $this->agent->is_mobile();
if ($mobile) {
    ?>
                    jQuery("#menu-main-menu").hide();
<?php } ?>
                if (jQuery("#main-header-search").val() === '') {
                    jQuery("#myDropdown").hide();
                } else {

                    var q = jQuery("#main-header-search").val();

                    if ((q.length) > 1) {

                        jQuery.ajax({
                            url: "<?php echo site_url('register/user/autocomplete'); ?>",
                            type: "POST",
                            data: {q: q},
                            beforeSend: function () {

                            },
                            success: function (result) {
                                jQuery("#myDropdown").html();
                                jQuery("#myDropdown").html(result);
                            },
                            error: function (result) {

                                return false;
                            }
                        });
                    }
                    jQuery("#myDropdown").show();
                }
            }

        </script>
        <script type="text/javascript">
            var mytheme_urls = {
                stickynav: 'enable'
            };
        </script>
    </head>
    <body>

        <!-- **Wrapper** -->
        <div class="wrapper">
            <div class="inner-wrapper">
                <!-- header-wrapper starts here -->
                <div id="header-wrapper">
                    <header id="header">
                        <!-- Top bar starts here -->
                        <div class="top-bar">
                            <div class="container">
                                <div class="dt-sc-contact-info">
                                    <p><i class="fa fa-phone"></i>Call Us: <a href="tel:+919988661099" title="Call Us"><span>+919988661099</span></a></p>
                                </div>
                                <div class="top-right">
                                    <ul>
                                        <?php
                                        $user = $this->session->userdata('user');
                                        if (isset($user) && $user['id'] > 0) {
                                            ?><li><a title="<?php echo ucwords($user['name']); ?>" href="<?php echo site_url('dashboard'); ?>"><span class="fa fa-sign-in"></span>&nbsp;<?php echo (isset($user['entity_type']) && ($user['entity_type'] == 1 OR $user['entity_type'] == 3)) ? 'Hi' : 'Welcome' ?>,&nbsp;<?php echo ucwords($user['name']); ?></a></li>
                                            <li><a title="Logout" href="<?php echo site_url('login/logout'); ?>"><span class="fa fa-sign-out"></span>&nbsp;&nbsp;Logout</a></li><?php
                                        } else {
                                            ?><li><a title="Login" href="<?php echo site_url('login'); ?>"><span class="fa fa-sign-in"></span>&nbsp;Login</a></li><?php
                                            }
                                            ?>
                                        <li><a title="Register Now" href="<?php echo site_url('register/user/gym#page-title'); ?>"><span class="fa fa-user"></span> Register Gym</a></li>
                                        <li><a title="Register Now" href="<?php echo site_url('register-trainer#page-title'); ?>"><span class="fa fa-user"></span> Register Trainer</a></li>
                                        <?php
                                        if (!isset($user) && $user['id'] > 0) {
                                            ?>
                                            <li><a title="Sign Up Now" href="<?php echo site_url('signup'); ?>"><span class="fa fa-sign-in"></span> Sign-Up</a></li><?php
                                        }
                                        ?>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- Top bar ends here -->
                        <?php
                        $mobile = $this->agent->is_mobile();
                        if (!$mobile) {
                            /* desktop code */
                            ?>
                            <div class="main-menu-container desktop">
                                <div class="main-menu">
                                    <div id="logo">
                                        <a title="Sixpackabsindia" href="<?php echo site_url(); ?>"><img title="Sixpackabsindia" alt="Sixpackabsindia" src="<?php echo IMAGE_PATH_FRONTEND; ?>frontend/logo.jpg"></a>
                                    </div>
                                    <div id="search-bar">
                                        <form>
                                            <div class="dt-sc-three-fourth column first">
                                                <input autocomplete="off" type="text" id="main-header-search" name="q" class="  search-query form-control dropbtn" placeholder="Find Gym / Find Trainer" onkeypress="myFunction()"/>
                                                <input type="hidden" name="latitude" id="lat">
                                                <input type="hidden" name="longitude" id="long">
                                                <input type="hidden" name="location" id="location">

                                                <div id="myDropdown" class="dropdown-content">

                                                </div>
                                            </div>
                                            <div class="dt-sc-one-fourth column first">  
                                                <button class="main-search-button" type="button">
                                                    <i class="fa fa-search search-icon" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                    <div id="primary-menu">
                                        <div class="dt-menu-toggle" id="dt-menu-toggle"><span class="dt-menu-toggle-icon"></span></div>
                                        <nav id="main-menu">
                                            <ul id="menu-main-menu" class="menu">
                                                <li class="<?php echo active('', 1, 'current_page_item'); ?> menu-item-simple-parent menu-item-depth-0"><a href="<?php echo site_url(); ?>">Home</a>
                                                </li>
                                                <li class="<?php echo active('search_trainer', 1, 'current_page_item'); ?> menu-item-simple-parent menu-item-depth-0">
                                                    <a href="<?php echo site_url('search_trainer?sort=rank'); ?>">Hire Trainer </a>               
                                                </li>
                                                <li class="<?php echo active('search_gym', 1, 'current_page_item'); ?> menu-item-megamenu-parent  megamenu-4-columns-group menu-item-depth-0">
                                                    <a href="<?php echo site_url('search_gym?sort=rank'); ?>" title="">Book Gym </a>
                                                </li>
                                                <li class="menu-item-megamenu-parent  megamenu-4-columns-group ">
                                                    <a href="<?php echo site_url('blog'); ?>" title="">Blog</a>
                                                    <div class="megamenu-child-container">
                                                        <ul class="sub-menu">

                                                        </ul>
                                                    </div>
                                                    <a class="dt-menu-expand">+</a>
                                                </li>
                                                <li class="menu-item-megamenu-parent  megamenu-4-columns-group menu-item-depth-0">
                                                    <a href="<?php echo site_url('videos'); ?>" title="">Workout Videos</a>
                                                </li>
                                                <li class="menu-item-simple-parent menu-item-depth-0">
                                                    <a href="<?php echo site_url('contact_us'); ?>">Contact</a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                            <?php
                        } else {
                            /* mobile code */
                            ?>

                            <div class="main-menu-container mobile">
                                <div class="main-menu">
                                    <div id="logo" class="col-sm-2 col-md-2 col-xs-9">
                                        <a title="Sixpackabsindia" href="<?php echo site_url(); ?>"><img title="Sixpackabsindia" alt="Sixpackabsindia" src="<?php echo IMAGE_PATH_FRONTEND; ?>frontend/logo.jpg"></a>
                                    </div>

                                    <div id="primary-menu" class="col-sm-6 col-md-6 col-xs-3">
                                        <div class="dt-menu-toggle" id="dt-menu-toggle"><span class="dt-menu-toggle-icon"></span></div>
                                        <nav id="main-menu">
                                            <ul id="menu-main-menu" class="menu">
                                                <li class="<?php echo active('', 1, 'current_page_item'); ?> menu-item-simple-parent menu-item-depth-0"><a href="<?php echo site_url(); ?>">Home</a>
                                                </li>
                                                <li class="<?php echo active('search_trainer', 1, 'current_page_item'); ?> menu-item-simple-parent menu-item-depth-0">
                                                    <a href="<?php echo site_url('search_trainer'); ?>">Hire Trainer </a>
                                                </li>
                                                <li class="<?php echo active('search_gym', 1, 'current_page_item'); ?> menu-item-megamenu-parent  megamenu-4-columns-group menu-item-depth-0">
                                                    <a href="<?php echo site_url('search_gym'); ?>" title="">Book Gym </a>
                                                </li>
                                                <li class="menu-item-megamenu-parent">
                                                    <a href="<?php echo site_url('blog'); ?>" title="">Blog</a>
                                                    <div class="megamenu-child-container">
                                                        <ul class="sub-menu">
                                                            <li class="menu-item-fullwidth fill-four-columns">
                                                                <div class="menu-item-widget-area-container">
                                                                    <ul>
                                                                        <li class="widget">
                                                                            <div class="dt-sc-programs">
                                                                                <div class="dt-sc-pro-thumb">
                                                                                    <a href="javascript:void(0)"><img src="<?php echo IMAGE_PATH_FRONTEND; ?>frontend/img/blog1.jpg" alt="" title=""></a>
                                                                                    <div class="programs-overlay">
                                                                                        <div class="dt-sc-pro-title">
                                                                                            <h3>Muscle Build Pro</h3>
                                                                                            <span>1 yr training program</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="dt-sc-pro-detail">
                                                                                    <div class="dt-sc-pro-price">
                                                                                        <p class="pro-price-content">
                                                                                            <span>What is cross-fit training?&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                        </p>
                                                                                        <a class="dt-sc-button small" href="javascript:void(0)" data-hover="Enroll Now">Enroll Now</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li class="widget">
                                                                            <div class="dt-sc-programs">
                                                                                <div class="dt-sc-pro-thumb">
                                                                                    <a href="javascript:void(0)"><img src="<?php echo IMAGE_PATH_FRONTEND; ?>frontend/img/blog2.jpg" alt="" title=""></a>
                                                                                    <div class="programs-overlay">
                                                                                        <div class="dt-sc-pro-title">
                                                                                            <h3>Muscle Build Pro</h3>
                                                                                            <span>1 yr training program</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="dt-sc-pro-detail">
                                                                                    <div class="dt-sc-pro-price">
                                                                                        <p class="pro-price-content">
                                                                                            <span>Find Out Diet plan of bollywood stars</span>
                                                                                        </p>
                                                                                        <a class="dt-sc-button small" href="javascript:void(0)" data-hover="Enroll Now">Enroll Now</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li class="widget">
                                                                            <div class="dt-sc-programs">
                                                                                <div class="dt-sc-pro-thumb">
                                                                                    <a href="javascript:void(0)"><img src="<?php echo IMAGE_PATH_FRONTEND; ?>frontend/img/blog3.jpg" alt="" title=""></a>
                                                                                    <div class="programs-overlay">
                                                                                        <div class="dt-sc-pro-title">
                                                                                            <h3>Muscle Build Pro</h3>
                                                                                            <span>1 yr training program</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="dt-sc-pro-detail">
                                                                                    <div class="dt-sc-pro-price">
                                                                                        <p class="pro-price-content">
                                                                                            <span>Why personal trainer is important in gym</span>
                                                                                        </p>
                                                                                        <a class="dt-sc-button small" href="javascript:void(0)" data-hover="Enroll Now">Enroll Now</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li class="widget">
                                                                            <div class="dt-sc-programs">
                                                                                <div class="dt-sc-pro-thumb">
                                                                                    <a href="javascript:void(0)"><img src="<?php echo IMAGE_PATH_FRONTEND; ?>frontend/img/blog4.jpg" alt="" title=""></a>
                                                                                    <div class="programs-overlay">
                                                                                        <div class="dt-sc-pro-title">
                                                                                            <h3>Muscle Build Pro</h3>
                                                                                            <span>1 yr training program</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="dt-sc-pro-detail">
                                                                                    <div class="dt-sc-pro-price">
                                                                                        <p class="pro-price-content">
                                                                                            <span>The ultimate guide to cardio workouts</span>
                                                                                        </p>
                                                                                        <a class="dt-sc-button small" href="javascript:void(0)" data-hover="Enroll Now">Enroll Now</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <a class="dt-menu-expand">+</a>
                                                </li>
                                                <li class="menu-item-megamenu-parent  megamenu-4-columns-group menu-item-depth-0">
                                                    <a href="<?php echo site_url('videos'); ?>" title="">Workout Videos</a>  
                                                </li>
                                                <li class="menu-item-simple-parent menu-item-depth-0">
                                                    <a href="<?php echo site_url('contact_us'); ?>">Contact</a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                    <div id="custom-search-input" class="col-sm-4 col-md-4 col-xs-12">
                                        <div class="input-group">
                                            <div class="col-sm-11 col-md-11 col-xs-10">
                                                <form>
                                                    <input autocomplete="off" type="text" id="main-header-search" name="q" class="  search-query form-control dropbtn" placeholder="Find Gym / Find Trainer" onkeypress="myFunction()"/>
                                                    <div id="myDropdown" class="dropdown-content">

                                                    </div>
                                                </form>
                                            </div>
                                            <div class="col-sm-1 col-md-1 col-xs-2">
                                                <button class="btn btn-default" type="button">
                                                    <i class="fa fa-search search-icon" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }
                        ?>
                    </header>
                </div>
                <!-- header-wrapper ends here -->
