<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | Display Debug backtrace
  |--------------------------------------------------------------------------
  |
  | If set to TRUE, a backtrace will be displayed along with php errors. If
  | error_reporting is disabled, the backtrace will not display, regardless
  | of this setting
  |
 */
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
defined('FILE_READ_MODE') OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') OR define('DIR_WRITE_MODE', 0755);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */
defined('FOPEN_READ') OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESCTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
  |--------------------------------------------------------------------------
  | Exit Status Codes
  |--------------------------------------------------------------------------
  |
  | Used to indicate the conditions under which the script is exit()ing.
  | While there is no universal standard for error codes, there are some
  | broad conventions.  Three such conventions are mentioned below, for
  | those who wish to make use of them.  The CodeIgniter defaults were
  | chosen for the least overlap with these conventions, while still
  | leaving room for others to be defined in future versions and user
  | applications.
  |
  | The three main conventions used for determining exit status codes
  | are as follows:
  |
  |    Standard C/C++ Library (stdlibc):
  |       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
  |       (This link also contains other GNU-specific conventions)
  |    BSD sysexits.h:
  |       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
  |    Bash scripting:
  |       http://tldp.org/LDP/abs/html/exitcodes.html
  |
 */
defined('EXIT_SUCCESS') OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


/*
  |--------------------------------------------------------------------------
  | Define css, js image and media path
  |--------------------------------------------------------------------------
  | for frontend , backend
  |
  |
 */
define('BASE_URL', 'http://localhost/sixpackabsindia/');
define('CSS_PATH_FRONTEND', BASE_URL . 'css/frontend/'); // css path
define('JS_PATH_FRONTEND', BASE_URL . 'js/frontend/'); // js path
define('IMAGE_PATH_FRONTEND', BASE_URL . 'images/'); // image path
define('MEDIA_PATH_FRONTEND', BASE_URL . 'media/'); // media path


define('CSS_PATH_BACKEND', BASE_URL . 'css/backend/'); // css path
define('JS_PATH_BACKEND', BASE_URL . 'js/backend/'); // js path



define('ERROREMAIL_NAME', 'Sixpackabsindia.com');
define('ERROR_MAIL', 'yuvraj@orggen.com');
define('ERROR_SUBJECT', 'ERROR');
define('EXCEPTION_HANDLING', TRUE); //MAKE IT FALSE ONLY WHEN SITE GOES LIVE

define('WEBSITE_PWD', '');  //salt
define('WEBSITE_NAME', 'Sixpackabsindia');
define('WEBSITE_NAME_ISSUU', '');
define('MARKER_COMPANY_NAME', '');  //:COMMENT= site name used in email templates
define('WEBSITE_DATE_FORMAT', 'jS M, Y');
define('CALL_DEBUG', TRUE);
// Only make CALL_BACK_TREE as TRUE if you need heavy debugging
define('CALL_BACK_TREE', FALSE);


define('DB_ERR_SUBJECT', 'DB Error : Six Pack Abs india - ' . date('Y-m-d H:i:s'));
define('DB_ERROR_EMAIL', 'yuvraj@orggen.com');

define('SMTP_SEND_MAIL', TRUE);
define('SMTP_AUTH', TRUE);
define('SMTP_NAME', '');
define('SMTP_USERNAME', 'noreply@gajanan-jewellers.com');
define('SMTP_PASSWORD', 'noreply123');
define('SMTP_HOST', 'www.gajanan-jewellers.com');
define('SMTP_PORT', '587');
define('SMTP_REPLY_TO', TRUE);
define('NOREPLY_EMAIL', 'noreply@orggen.com');
define('FROM_EMAIL', 'noreply@orggen.com');
define('FROM_NAME', 'Noreply OrgGen');
define('SMTP_FROM_EMAIL', 'noreply@gajanan-jewellers.com');


        const CLIENT_ID = '811071266642-iq51fe125k7m5fvt4d43smtm812csa4k.apps.googleusercontent.com';

/**
 * Replace this with the client secret you got from the Google APIs console.
 */
        const CLIENT_SECRET = 'hubtHU856QyPDGYX2fGiOaaU';

/**
 * Optionally replace this with your application's name.
 */
        const APPLICATION_NAME = "absindiasixpack1234";
define('PER_PAGE', 12);