
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <?php if ($this->session->flashdata('feedback')) { ?>
                <div class="alert alert-success"> <?php echo $this->session->flashdata('feedback') ?>
                </div>
            <?php } ?>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">List of customers</h3>
                </div>
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="example1_filter" class="dataTables_filter">
                                    <label>Search:
                                        <input type="search" class="form-control input-sm" placeholder="" aria-controls="example1"></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Sr. No</th>
                                            <th>Name</th>
                                            <th >Email</th>
                                            <th >Contact no</th>
                                            <th >City</th>
                                            <!--<th >Payment</th>-->
                                            <th >Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $cnt = $offset + 1;
                                        foreach ($result as $key => $value) {
                                            ?>
                                            <tr>
                                                <td><?php echo $cnt; ?></td>
                                                <td><?php echo $value['fname'] . ' ' . $value['lname']; ?></td>
                                                <td><?php echo $value['email']; ?></td>
                                                <td><?php echo $value['contact_no']; ?></td>
                                                <td><?php echo $value['city']; ?></td>
                                                <?php /*<td><?php $phpdate1 = strtotime( $value['pay_start_date'] );

$phpdate2 = strtotime( $value['pay_end_date'] );


echo date( 'd-m-Y', $phpdate1 ) . ' - ' .date( 'd-m-Y', $phpdate2 ); ?></td> */ ?>
                                                <td><a href="<?php echo site_url('register/user_admin/edit' . '/' . $value['user_id']); ?>"><i class="fa fa-edit"></i></a> |
                                                    <a href="<?php echo site_url('register/user_admin/delete' . '/' . $value['user_id']); ?>"><i class="fa fa-trash"></i></a></td>
                                            </tr>
                                            <?php
                                            $cnt++;
                                        }
                                        ?>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Showing 1 to 10 of <?php echo $count; ?> entries</div>
                            </div>
                            <div class="col-sm-7">
                                <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                                    <?php if (isset($result) && is_array($result) && count($result) > 0) { ?>
                                        <?php echo $this->pagination->create_links(); ?>

                                    <?php } else { ?>
                                        <h3>No Record Found</h3>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
