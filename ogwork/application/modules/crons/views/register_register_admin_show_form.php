<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Add service</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>

        <div class="box-body">
            <form class="form-horizontal" id="serviceform" method="post" action="<?php echo site_url('register/user_admin') . '/' . $mode; ?>">


                <?php if (isset($result) && is_array($result) && $result != "") { ?>
                    <input type="hidden" value="<?php echo $result['id']; ?>" name="service_id">
                <?php } ?>
                <div class="form-group">
                    <div class="col-sm-2 col-md-2">
                        <label  for="fname">First Name:<em>*</em></label></div>
                    <div class="col-sm-10 col-md-10">
                        <input value="<?php echo isset($result['fname']) ? $result['fname'] : ""; ?>" type="text" class="form-control" name="user_information[fname]" id="fname">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2 col-md-2">
                        <label for="lname">Last Name:<em>*</em></label></div>
                    <div class="col-sm-10 col-md-10">
                        <input value="<?php echo isset($result['lname']) ? $result['lname'] : ""; ?>" type="text" class="form-control"  name="user_information[lname]" id="lname">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2 col-md-2">
                        <label for="email">Email:<em>*</em></label></div>
                    <div class="col-sm-10 col-md-10">
                        <input disabled="disabled" value="<?php echo isset($result['email']) ? $result['email'] : ""; ?>" type="email" class="form-control"  name="user_information[email]" id="email">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2 col-md-2">
                        <label for="contact_no">Contact Number:<em>*</em></label></div>
                    <div class="col-sm-10 col-md-10">
                        <input value="<?php echo isset($result['contact_no']) ? $result['contact_no'] : ""; ?>" type="number" class="form-control"  name="user_information[contact_no]" id="contact_no">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2 col-md-2">
                        <label for="city">City:<em>*</em></label></div>
                    <div class="col-sm-10 col-md-10">
                        <input value="<?php echo isset($result['city']) ? $result['city'] : ""; ?>" type="text" class="form-control"  name="user_information[city]" id="city">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2 col-md-2">
                        <label>Intraday calls Payment:</label>
                    </div>
                    <div class="col-sm-10 col-md-10">
                        <input value="<?php echo isset($payment['0']['pay_start_date']) ? $payment['0']['pay_start_date'] . '  ' . $payment['0']['pay_end_date'] : ""; ?>" type="text" name="payment[0]" class="form-control pull-right " id="reservation">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2 col-md-2">
                        <label>Delivery base Payment:</label>
                    </div>
                    <div class="col-sm-10 col-md-10">
                        <input value="<?php echo isset($payment['1']['pay_start_date']) ? $payment['1']['pay_start_date'] . '  ' . $payment['1']['pay_end_date'] : ""; ?>" type="text" name="payment[1]" class="form-control pull-right " id="reservation1">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2 col-md-2">
                        <label>Long term Payment:</label>
                    </div>
                    <div class="col-sm-10 col-md-10">
                        <input value="<?php echo isset($payment['2']['pay_start_date']) ? $payment['2']['pay_start_date'] . '  ' . $result['pay_end_date'] : ""; ?>" type="text" name="payment[2]" class="form-control pull-right " id="reservation2">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2 col-md-2">
                        <label>Live intraday Payment:</label>
                    </div>
                    <div class="col-sm-10 col-md-10">
                        <input value="<?php echo isset($payment['3']['pay_start_date']) ? $payment['3']['pay_start_date'] . '  ' . $result['pay_end_date'] : ""; ?>" type="text" name="payment[3]" class="form-control pull-right " id="reservation3">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2 col-md-2">
                        <label>Commodity Payment:</label>
                    </div>
                    <div class="col-sm-10 col-md-10">
                        <input value="<?php echo isset($payment['4']['pay_start_date']) ? $payment['4']['pay_start_date'] . '  ' . $result['pay_end_date'] : ""; ?>" type="text" name="payment[4]" class="form-control pull-right " id="reservation4">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2 col-md-2">
                        <label>Daily currency Payment:</label>
                    </div>
                    <div class="col-sm-10 col-md-10">
                        <input value="<?php echo isset($payment['5']['pay_start_date']) ? $payment['5']['pay_start_date'] . '  ' . $result['pay_end_date'] : ""; ?>" type="text" name="payment[5]" class="form-control pull-right " id="reservation5">
                    </div>
                </div>


                <div class="box-footer">
                    <button type="submit" name="submitButton" class="btn btn-primary"><?php echo ucwords($mode); ?></button>
                </div>

            </form>
        </div>
    </div>
</div>
</section>
<link rel="stylesheet" href="<?php echo CSS_PATH_BACKEND; ?>daterangepicker-bs3.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="<?php echo JS_PATH_BACKEND; ?>plugins/daterangepicker.js"></script>
<script>
    $(function () {
        $('#reservation').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD'
            }
        });
        $('#reservation1').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD'
            }
        });
        $('#reservation2').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD'
            }
        });
        $('#reservation3').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD'
            }
        });
        $('#reservation4').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD'
            }
        });
        $('#reservation5').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD'
            }
        });
        $('#reservation6').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD'
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#serviceform').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'service[stocks]': {
                    validators: {
                        notEmpty: {
                            message: 'Stocks Required'
                        },
                        stringLength: {
                            min: 3,
                            max: 40,
                            message: 'minimum 3 and maximum 40 characters Required'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9\s\()]+$/,
                            message: 'Invalid Stocks'
                        }
                    }
                },
                'service[cmp]': {
                    validators: {
                        notEmpty: {
                            message: 'CMP Required'
                        },
                        regexp: {
                            message: 'Invalid',
                            regexp: /^[0-9\s\.\()]+$/
                        }

                    }
                },
                'service[target]': {
                    validators: {
                        notEmpty: {
                            message: 'Target Required'
                        },
                        regexp: {
                            message: 'Invalid',
                            regexp: /^[0-9\s\.\/\()]+$/
                        }

                    }

                },
                'service[stop]': {
                    validators: {
                        notEmpty: {
                            message: 'Stop Required'
                        },
                        regexp: {
                            message: 'Invalid',
                            regexp: /^[0-9\s\.\()]+$/
                        }

                    }

                },
                'service[trade]': {
                    validators: {
                        notEmpty: {
                            message: 'Please select one option'
                        }

                    }

                },
                'service[remarks]': {
                    validators: {
                        stringLength: {
                            min: 10,
                            message: 'Minimum 10 charaters Required'
                        }

                    }

                },
                'service[share_info]': {
                    validators: {
                        notEmpty: {
                            message: 'Please select one option'
                        }

                    }

                }

            }
        });
    });
</script>