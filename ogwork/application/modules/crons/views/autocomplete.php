<ul>
    <?php
    $this->load->helper('url');
    if (isset($list) && is_array($list) && count($list) > 0) {
        foreach ($list as $key => $value) {
            ?>
            <li onclick="window.location = '<?php echo site_url() . $value['sef']; ?>'" tabindex="1" ><?php echo $value['name']; ?> In <span><?php echo ($value['entity_type'] == '1') ? 'Trainer' : 'Gym'; ?></span></li>                                                        
            <?php
        }
    } else {
        ?><li >No Records found.</li><?php
        }
        ?>                                                       
</ul>