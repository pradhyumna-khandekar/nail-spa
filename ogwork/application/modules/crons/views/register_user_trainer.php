<div id="main" class="full-pattern3">
    <div class="main-content">
        <div class="content-full-width">
            <div class="dt-sc-hr-invisible"></div>
            <div class="fullwidth-section">
                <div class="container">
                    <form method="POST" class="form-horizontal registration-form" id="registration_form" action="<?php echo site_url('trainer/insert'); ?>">
                        <input type="hidden" name="trainer[profile]" id="profile_pic" value="">
                        <input type="hidden" name="trainer[latitude]" id="latitude" value="">
                        <input type="hidden" name="trainer[longitude]" id="longitude" value="">
                        <h3 class="border-title aligncenter" id="page-title"> <span> <i class="fa fa-user"></i> Register as a Trainer </span></h3>
                        <div id="example-vertical" class="register">
                            <h3>Personal Information</h3>
                            <section>
                                <?php
                                $user = $this->session->userdata('user');
                                ?>
                                <div class="dt-sc-one-half column first">
                                    <label class="control-label col-sm-2" for="name"><span class="required">*</span>Name :</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="trainer[name]" id="name" placeholder="Enter Name" value="<?php echo isset($user['id']) ? $user['name'] : '' ?>">
                                    </div>
                                </div>
                                <div class="dt-sc-one-half column">
                                    <label class="control-label col-sm-2" for="description">Description :</label>
                                    <div class="col-sm-4">
                                        <textarea cols="40" rows="2" name="trainer[description]" id="description" maxlength="300"></textarea>
                                    </div>
                                </div>
                                <div class="dt-sc-one-half column first">

                                    <label class="control-label col-sm-2" for="gender"><span class="required">*</span>Gender :</label>
                                    <div class="col-sm-4 radio-labels">

                                        <input name="trainer[gender]" type="radio" value="1" id="male">
                                        <label for="male">Male</label>
                                        <input name="trainer[gender]" type="radio" value="2" id="female">
                                        <label for="female">Female</label>
                                        <input name="trainer[gender]" type="radio" value="3" id="others">
                                        <label for="others">Others</label>
                                    </div>

                                </div>
                                <div class="dt-sc-one-half column bottom-input">
                                    <div class="dt-sc-one-half column  first">
                                        <label class="control-label col-sm-2" for="birthday"><span class="required">*</span>Birthdate :</label>
                                        <div class="col-sm-4">
                                            <input autocomplete="off"  readonly="" id="birthday" class="date-picker form-control" name="trainer[birth_date]" type="text" value="" placeholder="select birthdate">
                                        </div>
                                    </div>    
                                    <div class="dt-sc-one-half column ">
                                        <label class="control-label col-sm-2" for="birthday">&nbsp;</label>
                                        <input type="text" id="alternate" size="30" readonly="" style="display:none;">
                                    </div>    
                                </div>


                                <?php if (!isset($user)) { ?>
                                    <div class="dt-sc-one-half column first">
                                        <label class="control-label col-sm-2" for="email"><span class="required">*</span>Email :</label>
                                        <div class="col-sm-4">
                                            <input <?php echo isset($user) ? 'readonly' : ''; ?>  value="<?php echo isset($user) ? $user['email'] : ''; ?>"   type="email" class="form-control" id="email" name="trainer[email]" placeholder="Enter email">
                                        </div>  
                                    </div>
                                    <div class="dt-sc-one-half column">
                                        <label class="control-label col-sm-2" for="password"><span class="required">*</span>Password :</label>
                                        <div class="col-sm-4">
                                            <input type="password" class="form-control" id="password" placeholder="Enter Password" name="trainer[password]">
                                        </div>
                                    </div>
                                <?php } ?>



                            </section>
                            <h3>Images</h3>
                            <section>
                                <div class="dropzone" id="dropzoneFileUpload">
                                    <div class="fallback">
                                        <input id="files" multiple="true" name="files" type="file">
                                    </div>
                                </div>
                            </section>
                            <h3>Contact Information</h3>
                            <section>

                                <div class="dt-sc-one-half column first">
                                    <label class="control-label col-sm-2" for="address_1">Address Line 1 :</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="address_1" placeholder="Enter Address Line 1" name="trainer[street_address_1]">
                                    </div>
                                </div>
                                <div class="dt-sc-one-half column">
                                    <label class="control-label col-sm-2" for="address_2">Address Line 2 :</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="address_2" placeholder="Enter Address Line 2" name="trainer[street_address_2]">
                                    </div>
                                </div>
                                <div class="dt-sc-one-half column first select-box">
                                    <label class="control-label col-sm-2" for="state">State & City :</label>
                                    <select  class="form-control select2" name="trainer[state_city]">
                                        <option>Select State & City</option>
                                        <?php
                                        foreach ($state_city as $key => $value) {
                                            ?>    
                                            <optgroup label="<?php echo $value['name']; ?>">
                                                <?php
                                                if (isset($value['city'])) {
                                                    foreach ($value['city'] as $k => $v) {
                                                        ?><option value="<?php echo $k; ?>"><?php echo $v . '-' . $value['name']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </optgroup>
                                        <?php }
                                        ?>
                                    </select>
                                </div>
                                <div class="dt-sc-one-half column">
                                    <label class="control-label col-sm-2" for="pincode">Pin Code :</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="pincode" placeholder="Enter Pincode" name="trainer[pincode]">
                                    </div>
                                </div>
                                <div class="dt-sc-one-half column first">
                                    <label class="control-label col-sm-2" for="locality"><span class="required">*</span>Locality (Area) :</label>
                                    <div class="col-sm-4"> 
                                        <input autocomplete="off" type="text" class="form-control" id="locality" name="trainer[locality]">
                                    </div>
                                </div>
                                <div class="dt-sc-one-half column">
                                    <label class="control-label col-sm-2" for="contact_no"><span class="required">*</span>Contact No. :</label>
                                    <div class="col-sm-4">
                                        <input  <?php echo (isset($user) && $user['contact_no'] != '') ? 'readonly' : ''; ?>  value="<?php echo isset($user) ? $user['contact_no'] : ''; ?>"  type="text" class="form-control" id="contact_no" name="trainer[contact_no]" placeholder="Enter Contact No.">
                                    </div>
                                </div>

                            </section>
                            <h3>Services</h3>
                            <section>

                                <div class="dt-sc-one-half column first">
                                    <label class="control-label col-md-2 col-sm-2" for="skills">Skills :</label>
                                    <div class="col-md-4 col-sm-4">
                                        <select id="skills" class="form-control multiselect" name="skills[]" multiple="multiple" id="skills">
                                            <?php
                                            $trainer_skills = $this->config->item('trainer_skills');
                                            foreach ($trainer_skills as $key => $value) {
                                                ?>
                                                <option <?php
                                                if ($key == 1) {
                                                    echo 'selected';
                                                }
                                                ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                    <?php
                                                }
                                                ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="dt-sc-one-half column experience">
                                    <label class="control-label col-md-2 col-sm-2"><span class="required">*</span>Experience :</label>
                                    <div class="col-md-4 col-sm-4">
                                        <input type="text" class="form-control" id="text" name="trainer[experience]">
                                    </div>
                                </div>
                                <div class="dt-sc-one-half column first">
                                    <label class="control-label split-label col-md-2 col-sm-2" for="min_fees"><span class="required">*</span>Fees per month :</label>
                                    <div class="dt-sc-one-half split-input">
                                        <input type="text" class="form-control" id="min_fees" placeholder="From" name="trainer[min_fees]">
                                    </div>
                                    <div class="dt-sc-one-half split-input">
                                        <input type="text" class="form-control" id="max_fees" placeholder="To" name="trainer[max_fees]">
                                    </div>
                                </div>				


                            </section>
                            <h3>Social Links</h3>
                            <section class="social-select">

                                <div id="clone" class="last" style="display:none;">
                                    <div class="dt-sc-one-third column first">
                                        <input type="url" class="form-control" name="url[]">
                                    </div>
                                    <div class="dt-sc-one-third column">
                                        <select name="social[]" class="social-select  dt-sc-one-half column">
                                            <?php
                                            $social = $this->config->item('social');
                                            foreach ($social as $key => $value) {
                                                ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="dt-sc-one-third column">
                                        <button class="social-button remove">Remove</button>

                                    </div>
                                </div>

                                <div class="last">
                                    <div class="dt-sc-one-third column first">
                                        <input type="url" class="form-control" name="url[]">
                                    </div>
                                    <div class="dt-sc-one-third column">
                                        <select name="social[]" class="social-select  dt-sc-one-half column">
                                            <?php
                                            $social = $this->config->item('social');
                                            foreach ($social as $key => $value) {
                                                ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <button id="add" class="social-button">Add</button>



                            </section>

                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<link href="<?php echo CSS_PATH_FRONTEND; ?>jslider.css" rel="stylesheet" media="all" />
<link href="<?php echo CSS_PATH_FRONTEND; ?>jslider.blue.css" rel="stylesheet" media="all" />
<link href="<?php echo CSS_PATH_FRONTEND; ?>jslider.plastic.css" rel="stylesheet" media="all" />
<link href="<?php echo CSS_PATH_FRONTEND; ?>jslider.round.css" rel="stylesheet" media="all" />-->
<link href="<?php echo CSS_PATH_FRONTEND; ?>jslider.round.plastic.css" rel="stylesheet" media="all" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/basic.min.css" rel="stylesheet" media="all" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="<?php echo JS_PATH_FRONTEND; ?>modernizr-2.6.2.min.js"></script>
<script type="text/javascript" src="<?php echo JS_PATH_FRONTEND; ?>jquery.steps.min.js"></script>
<link href="<?php echo CSS_PATH_FRONTEND; ?>jquery.steps.css" rel="stylesheet">

<script type="text/javascript" src="<?php echo JS_PATH_FRONTEND; ?>dropzone.js"></script>
<script type="text/javascript" src="<?php echo JS_PATH_FRONTEND; ?>jquery.validate.js"></script>
<link href="<?php echo CSS_PATH_FRONTEND; ?>select2.min.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="<?php echo JS_PATH_FRONTEND; ?>select2.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH_FRONTEND; ?>jquery.multiselect.css">
<script src="<?php echo JS_PATH_FRONTEND; ?>jquery.multiselect.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo JS_PATH_FRONTEND; ?>jquery.validate.min.js"></script>
<script>

    jQuery(document).ready(function () {

        /*
         * 
         * validate form
         */
        jQuery.validator.addMethod("lettersonly", function (value, element) {
            return this.optional(element) || /^[a-zA-Z," "\s]+$/i.test(value);
        });
        jQuery.validator.addMethod("check_contact_no", function (value, element) {
            var response = false;
            jQuery.ajax({
                url: "<?php echo site_url('register/user/check_contact_no'); ?>",
                type: "POST",
                async: false,
                data: {contact_no: value},
                beforeSend: function () {

                },
                success: function (result) {
                    response = true;
                },
                error: function (result) {
                    response = false;
                }
            });
            return response;
        }, "Contact No already registered.Please login.");
        jQuery.validator.addMethod("check_email", function (value, element) {
            var response = false;
            jQuery.ajax({
                url: "<?php echo site_url('register/user/check_email_register'); ?>",
                type: "POST",
                async: false,
                data: {email_id: value},
                beforeSend: function () {

                },
                success: function (result) {
                    response = true;
                },
                error: function (result) {
                    response = false;
                }
            });
            return response;
        }, "Email id already registered.Please login.");
        jQuery.validator.addMethod("greaterThan",
                function (value, element, param) {
                    var $otherElement = jQuery(param);
                    return parseInt(value, 10) > parseInt($otherElement.val(), 10);
                });
        jQuery("#registration_form").validate({
            rules: {
                'trainer[name]':
                        {
                            required: true,
                            lettersonly: true,
                            rangelength: [3, 40]
                        },
                'trainer[password]': {
                    required: true,
                    rangelength: [7, 10]
                },
                'trainer[description]':
                        {
                            rangelength: [3, 1000]
                        },
                'trainer[gender]':
                        {
                            required: true
                        },
                'trainer[contact_no]': {
                    required: true,
                    rangelength: [10, 10],
                    digits: true,
                    check_contact_no: <?php echo isset($_SESSION['user']['id']) ? 'false' : 'true'; ?>
                },
                'trainer[birth_date]':
                        {
                            required: true
                        },
                'trainer[email]': {
                    required: true,
                    email: true,
                    check_email: <?php echo isset($_SESSION['user']['id']) ? 'false' : 'true'; ?>
                },
                'trainer[street_address_1]': {
                    rangelength: [3, 100]
                },
                'trainer[street_address_2]': {
                    rangelength: [3, 100]
                },
                'trainer[pincode]': {
                    digits: true,
                    rangelength: [6, 6]
                },
                'trainer[locality]':
                        {
                            required: true
                        },
                'trainer[skills]':
                        {
                            required: true
                        },
                'trainer[experience]':
                        {
                            required: true,
                            range: [0, 50]
                        },
                'trainer[min_fees]':
                        {
                            required: true,
                            number: true,
                            rangelength: [3, 5]
                        },
                'trainer[max_fees]':
                        {
                            required: true,
                            number: true,
                            rangelength: [3, 5],
                            greaterThan: "#min_fees"
                        },
                'profile':
                        {
                            required: true
                        },
                'url': {
                    url: true
                }
            },
            messages: {
                'trainer[name]': {
                    required: "Name Required",
                    lettersonly: "Enter Only Letters",
                    rangelength: "Minimum 3 & Maximum 40 Character Required"
                },
                'trainer[description]':
                        {
                            rangelength: "Minimum 3 & Maximum 1000 Character Allowed"
                        },
                'trainer[gender]':
                        {
                            required: 'Gender Required'
                        },
                'trainer[contact_no]': {
                    required: "Contact Number Required",
                    rangelength: "10 Digits Required",
                    digits: "Enter Only Digits",
                    check_contact_no: "Contact Number already exist.Please login."
                },
                'trainer[birth_date]':
                        {
                            required: 'Birth Date Required'
                        },
                'trainer[email]': {
                    required: "Email Required",
                    email: "Enter Valid Email ID"
                },
                'trainer[street_address_1]': {
                    rangelength: "Minimum 3 & Maximum 1000 Character Allowed"
                },
                'trainer[street_address_2]': {
                    rangelength: "Minimum 3 & Maximum 1000 Character Allowed"
                },
                'trainer[pincode]': {
                    digits: "Pincode Required",
                    rangelength: "6 Digits Required"
                },
                'trainer[locality]':
                        {
                            required: "Locality Required"
                        },
                'trainer[skills]':
                        {
                            required: "Skills Required"
                        },
                'trainer[experience]':
                        {
                            required: "Experience Required"
                        },
                'trainer[min_fees]':
                        {
                            required: "Fees Required",
                            number: "Enter Valid Fees",
                            rangelength: "Enter valid fees. Fees should be greater than INR 99."
                        },
                'trainer[max_fees]':
                        {
                            required: "Fees Required",
                            number: "Enter Valid Fees",
                            rangelength: "Enter valid fees. Fees should be greater than INR 99.",
                            greaterThan: "Please enter valid range of fees. Exmaple: 1000-5000"
                        },
                'trainer[profile]':
                        {
                            required: "Select your profile picture."
                        }
            },
            errorElement: 'p',
            errorPlacement: function (error, element) {
                jQuery(element).parent().append(error);
            }
        });
        jQuery("#example-vertical").steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            stepsOrientation: "vertical",
            enableAllSteps: true,
            enablePagination: true,
            enableFinishButton: true,
            onStepChanging: function (e, currentIndex, newIndex) {

                var isValid = jQuery("#registration_form").valid();
                var response = false;
                if (isValid == true) {
                    response = true;
                }
                if (currentIndex == 1) {
                    if (!jQuery('.dropzone-image').length) {
                        alert('kindly upload at least 1 image');
                        response = false;
                    }
                }
                if (currentIndex == 2) {
                    var lat = jQuery('#latitude').val();
                    var long = jQuery('#longitude').val();
                    
                    if (lat == "" || long == "") {
                        alert('Enter locality(Area) from given list.');
                        jQuery("#locality").focus();
                        response = false;
                    }
                }
                return response;
            },
            onFinishing: function (event, currentIndex) {
                var isValid = jQuery("#registration_form").valid();
                if (isValid == true) {
                    if (jQuery('.dropzone-image').length)         // use this if you are using id to check
                    {
                        jQuery("#registration_form").submit();
                    } else {
                        alert('kindly upload at least 1 image');
                        jQuery("#example-vertical-t-1").get(0).click();
                    }
                }
            },
            onFinished: function (event, currentIndex) {
                console.log(event);
                console.log(currentIndex);
            }
        });
        jQuery(".select2").select2({
            allowClear: true
        });



        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone("#dropzoneFileUpload",
                {
                    url: "<?php echo site_url('image_upload/do_upload'); ?>",
                    paramName: "file", // The name that will be used to transfer the file
                    maxFilesize: 4, // MB,
                    maxFiles: 6,
                    acceptedFiles: "image/*",
                    uploadMultiple: false,
                    parallelUploads: 1,
                    dictInvalidFileType: "Only Image allowed",
                    dictDefaultMessage:
                            '<span class="bigger-150 bolder"><i class="ace-icon fa fa-caret-right red"></i> Drop files</span> to upload \
            <span class="smaller-80 grey">(or click)</span> <br /> \
            <i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>',
                    addRemoveLinks: true,
                    dictRemoveFile: "Remove Image",
                    dictResponseError: 'Error while uploading file!',
                    removedfile: function (file) {
                        jQuery('input[type="hidden"][data-original="' + file.name + '"]').remove();
                        var _ref;
                        return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                    },
                    dictMaxFilesExceeded: "Maximum upload limit reached",
                    init: function () {

                        this.on("maxfilesexceeded", function (file) {
                            alert("No more files please!");
                        });

                        this.on("addedfile", function (file) {
                            if (this.files.length) {
                                var _i, _len;
                                for (_i = 0, _len = this.files.length; _i < _len - 1; _i++) // -1 to exclude current file
                                {
                                    if (this.files[_i].name === file.name && this.files[_i].size === file.size && this.files[_i].lastModifiedDate.toString() === file.lastModifiedDate.toString())
                                    {
                                        alert('Duplicate Images not allowed!');
                                        this.removeFile(file);
                                    }
                                }
                            }

                            // Capture the Dropzone instance as closure.
                            var _this = this;
                            // Create the remove button
                            var removeButton = Dropzone.createElement("<span><input class=profile_pic type=\"radio\" name=\"profile\" value=\"" + file.name + "\" id=" + file.name + "> <label for=" + file.name + "> Set Profile</label></span>");
                            // Listen to the click event
                            removeButton.addEventListener("click", function (e) {

                                // Make sure the button click doesn't submit the form:
                                //e.preventDefault();
                                //e.stopPropagation();

                                // Remove the file preview.
                                //_this.removeFile(file);
                                // If you want to the delete the file on the server as well,
                                // you can do the AJAX request here.
                            });
                            // Add the button to the file preview element.
                            file.previewElement.appendChild(removeButton);
                            // Create the remove button
                            //var previewButton = Dropzone.createElement("<a id=" + file.name + " href=" + file.name + " target='_blank'>Preview</a>");
                            // Add the button to the file preview element.
                            //file.previewElement.appendChild(previewButton);
                        });
                        this.on("success", function (file, responseText) {

                            result = JSON.parse(responseText);

                            var new_img_name = result['file_name'];
                            var ori_img_name = result['orig_name'];
                            var absolute_path = result['path'];
                            var size = result['file_size'];
                            jQuery('#registration_form').append('<input class="dropzone-image" type="hidden" name="image[]" data-original="' + ori_img_name + '" value="' + new_img_name + '" />');

                            //var thisDropzone = this;
                            //var mockFile = {name: ori_img_name, size: size};
                            //thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                            //thisDropzone.options.thumbnail.call(thisDropzone, mockFile, "media/temp_img" + name);

                            //jQuery('#' + file.name).attr('href', absolute_path);
                            //
                            //jQuery('#registration_form').append('<a href="' + absolute_path + '" target="_blank">Preview</a>');
                            /*
                             * add value to radio button of 
                             */
                            //console.log(ori_img_name);
                            //console.log(absolute_path);
                            //jQuery('#' + ori_img_name).attr('href',absolute_path);
                        });
                    }
                });
        /*
         * 
         */

        jQuery('.multiselect').multiselect({
            //columns: 1,
            placeholder: 'Select Amenities',
            search: true,
            selectAll: true
        });
        initAutocomplete();
        jQuery("#add").click(function (event) {
            event.preventDefault();
            /*jQuery("#gym_registration_form").submit(function (e) {
             return false;
             }); */
            //jQuery("#clone").clone().insertAfter("div.last:last");
            $clone = jQuery("#clone").clone().removeAttr('id'),
                    $clone.show();
            $clone.insertAfter("div.last:last");

        });
        jQuery(document).on('click', ".remove", function () {
            jQuery(this).closest('div').parent().remove();
        });
        jQuery(function () {
            jQuery("#birthday").datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                altField: "#alternate",
                altFormat: "DD, d MM, yy",
                yearRange: '1930:2014',
                onSelect: function (dateText, inst) {
                    jQuery("#alternate").show();
                }
            });
        });

    });

</script>

<style>
    .ui-datepicker select.ui-datepicker-month, .ui-datepicker select.ui-datepicker-year{display:inline;}
</style>