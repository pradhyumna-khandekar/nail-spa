<div id="main" class="full-pattern3">
    <div class="main-content">
        <div class="content-full-width">
            <div class="dt-sc-hr-invisible"></div>
            <div class="fullwidth-section">
                <div class="container">
                    <form class="form-horizontal registration-form" method="POST" id="gym_registration_form"  action="<?php echo site_url('gym/insert'); ?>">
                        <input type="hidden" name="gym[profile]" id="profile_pic" value="">
                        <input type="hidden" name="gym[latitude]" id="latitude" value="">
                        <input type="hidden" name="gym[longitude]" id="longitude" value="">
                        <h3 id="page-title" class="border-title aligncenter"> <span> <i class="fa fa-user"></i> Register Gym </span></h3>
                        <div id="example-vertical">
                            <h3>Information</h3>
                            <section class="gym-form-information">
                                <div class="dt-sc-one-half column first">
                                    <label class="control-label col-sm-2" for="name"><span class="required">*</span>Name :</label>
                                    <div class="col-sm-4">
                                        <input maxlength="50" type="text" class="form-control" id="name" placeholder="Enter Name" name="gym[name]" autofocus="on">
                                    </div>
                                </div>
                                <div class="dt-sc-one-half column">
                                    <label class="control-label col-sm-2" for="about">About :</label>
                                    <div class="col-sm-4">
                                        <textarea cols="40" rows="2" name="gym[description]" id="about"></textarea>
                                    </div>
                                </div>
                                <div class="dt-sc-one-half column first">
                                    <label class="control-label col-sm-2" for="center_type"><span class="required">*</span>Center Type :</label>

                                    <div class="col-sm-4 radio-labels">
                                        <input name="gym[center_type]" type="radio" value="1" id="male">
                                        <label for="male">Male</label>
                                        <input name="gym[center_type]" type="radio" value="2" id="female">
                                        <label for="female">Female</label>
                                        <input name="gym[center_type]" type="radio" value="3" id="unisex" checked="checked">
                                        <label for="unisex">Unisex</label>
                                    </div>

                                </div>
                                <?php
                                $user = $this->session->userdata('user');
                                if (!isset($user)) {
                                    ?><div class="dt-sc-one-half column password">
                                        <label class="control-label col-sm-2" for="password"><span class="required">*</span>Password :</label>
                                        <div class="col-sm-4">
                                            <input type="password" class="form-control" id="password" placeholder="Enter Password" name="gym[password]">
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div class="dt-sc-one-half column first">
                                    <label class="control-label col-sm-2" for="contact_no"><span class="required">*</span>Contact No. :</label>
                                    <div class="col-sm-4">
                                        <input <?php echo (isset($user) && $user['contact_no'] != '') ? 'readonly' : ''; ?>  value="<?php echo isset($user) ? trim($user['contact_no']) : ''; ?>" type="text" class="form-control" id="contact_no" placeholder="Enter Contact Number" name="gym[contact_no]">
                                    </div>
                                </div>
                                <div class="dt-sc-one-half column">
                                    <label class="control-label col-sm-2" for="email"><span class="required">*</span>Email :</label>
                                    <div class="col-sm-4">
                                        <input <?php echo isset($user) ? 'readonly' : ''; ?>  value="<?php echo isset($user) ? $user['email'] : ''; ?>"  type="email" class="form-control" id="email" placeholder="Enter email" name="gym[email]">
                                    </div>
                                </div>

                            </section>
                            <h3>Images</h3>
                            <section>
                                <?php /* <form class="form-horizontal dropzone"  id="my-awesome-dropzone" action="<?php echo site_url('/image_upload/do_upload'); ?>">
                                  <div class="form-group" id="dropzoneFileUpload">
                                  <label class="control-label col-sm-2" for="email">Upload Images:</label>
                                  <div class="col-sm-4">
                                  <div class="fallback">
                                  <input type="file" name="file" accept="image/*">
                                  <button type="submit" value="Upload" />
                                  </div>
                                  <div class="dropzone-previews"></div>
                                  </div>
                                  </form> */ ?>
                                <div class="dropzone" id="dropzoneFileUpload">
                                    <div class="fallback">
                                        <input id="files" multiple="true" name="files" type="file">
                                    </div>
                                </div>
                            </section>
                            <h3>Contact Information</h3>
                            <section>

                                <div class="dt-sc-one-half column first">
                                    <label class="control-label col-sm-2" for="add_1">Address Line 1 :</label>
                                    <div class="col-sm-4">
                                        <input maxlength="40" type="text" class="form-control" id="add_1" placeholder="Enter Address Line 1" name="gym[street_address1]">
                                    </div>
                                </div>
                                <div class="dt-sc-one-half column">
                                    <label class="control-label col-sm-2" for="add_2">Address Line 2 :</label>
                                    <div class="col-sm-4">
                                        <input maxlength="40" type="text" class="form-control" id="add_2" placeholder="Enter Address Line 2" name="gym[street_address2]">
                                    </div>
                                </div>
                                <div class="dt-sc-one-half column first state_select">
                                    <label class="control-label col-sm-2" for="state">State & City :</label>
                                    <div class="col-sm-4">
                                        <select  class="form-control select2" name="gym[state_city]">
                                            <option>Select State & City</option>
                                            <?php
                                            foreach ($state_city as $key => $value) {
                                                ?>    
                                                <optgroup label="<?php echo $value['name']; ?>">
                                                    <?php
                                                    if (isset($value['city'])) {
                                                        foreach ($value['city'] as $k => $v) {
                                                            ?><option value="<?php echo $k; ?>"><?php echo $v . ' - ' . $value['name']; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </optgroup>
                                            <?php }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="dt-sc-one-half column">
                                    <label class="control-label col-sm-2" for="locality"><span class="required">*</span>Locality (Area) :</label>
                                    <div class="col-sm-4">
                                        <input type="text"  id="locality"  class="form-control" name="gym[locality]">
                                    </div>
                                </div>
                                <div class="dt-sc-one-half column first">
                                    <label class="control-label col-sm-2" for="pincode">Pin Code :</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="pincode" placeholder="Enter Pincode" name="gym[pincode]">
                                    </div>
                                </div>
                                <div class="dt-sc-one-half column">
                                    <label class="control-label col-sm-2" for="website">Website :</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="website" value="" name="gym[website]">
                                    </div>
                                </div>
                            </section>
                            <h3>Services</h3>
                            <section>

                                <div class="dt-sc-one-half column first">
                                    <label class="control-label col-md-2 col-sm-2" for="services">Services :</label>
                                    <div class="col-md-4 col-sm-4">
                                        <select multiple class="form-control multiselect" name="services[]" id="services">
                                            <?php
                                            $services = $this->config->item('services');
                                            foreach ($services as $key => $value) {
                                                ?>
                                                <option <?php echo ($key == 4) ? 'selected' : ''; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="dt-sc-one-half column">
                                    <label class="control-label col-md-2 col-sm-2" for="facilities">Facilities :</label>
                                    <div class="col-md-4 col-sm-4">
                                        <select multiple class="form-control multiselect" name="facilities[]" id="facilities">
                                            <?php
                                            $facilities = $this->config->item('facilities');
                                            foreach ($facilities as $key => $value) {
                                                ?>
                                                <option <?php echo ($key == 1) ? 'selected' : ''; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="dt-sc-one-half column first">
                                    <label class="control-label col-sm-2" for="category">Category :</label>
                                    <div class="col-sm-4 category">
                                        <select class="form-control" name="gym[category]" id="category">
                                            <option value="1">Gym</option>
                                            <option value="2">Fitness Studio</option>
                                            <option value="3">Fitness Center</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="dt-sc-one-half column ">
                                    <label class="control-label split-label col-md-2 col-sm-2" for="price"><span class="required">*</span>Price Range (&#8377;) (Per Month):</label>
                                    <div class="dt-sc-one-half split-input">
                                        <input type="text" class="form-control" id="min_price" placeholder="Lowest Price" name="gym[min_price]">
                                    </div>
                                    <div class="dt-sc-one-half split-input">
                                        <input type="text" class="form-control" id="max_price" placeholder="Highest Price" name="gym[max_price]">
                                    </div>
                                </div>				

                            </section>
                            <h3>Timings</h3>
                            <section class="gym-form-timing">
                                <div class="table-responsive">
                                    <table class="table register-table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Days</th>
                                                <th>From</th>
                                                <th>To</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <input class="timing-checkbox" data-id="1" type="checkbox" name="weekday[]">
                                                </td>
                                                <td>Monday
                                                <td>
                                                    <select id='time_from_1' class="form-control" name="time_from[]">
                                                        <option></option> <option value="00:00">00:00</option><option value="01:00">01:00</option><option value="02:00">02:00</option><option value="03:00">03:00</option><option value="04:00">04:00</option><option value="05:00">05:00</option><option value="06:00">06:00</option><option value="07:00">07:00</option><option value="08:00">08:00</option><option value="09:00">09:00</option><option value="10:00">10:00</option><option value="11:00">11:00</option><option value="12:00">12:00</option><option value="13:00">13:00</option><option value="14:00">14:00</option><option value="15:00">15:00</option><option value="16:00">16:00</option><option value="17:00">17:00</option><option value="18:00">18:00</option><option value="19:00">19:00</option><option value="20:00">20:00</option><option value="21:00">21:00</option><option value="22:00">22:00</option><option value="23:00">23:00</option>                                              
                                                    </select>   
                                                </td>
                                                <td>
                                                    <select id='time_to_1' class="form-control" name="time_to[]">
                                                        <option></option><option value="00:00">00:00</option><option value="01:00">01:00</option><option value="02:00">02:00</option><option value="03:00">03:00</option><option value="04:00">04:00</option><option value="05:00">05:00</option><option value="06:00">06:00</option><option value="07:00">07:00</option><option value="08:00">08:00</option><option value="09:00">09:00</option><option value="10:00">10:00</option><option value="11:00">11:00</option><option value="12:00">12:00</option><option value="13:00">13:00</option><option value="14:00">14:00</option><option value="15:00">15:00</option><option value="16:00">16:00</option><option value="17:00">17:00</option><option value="18:00">18:00</option><option value="19:00">19:00</option><option value="20:00">20:00</option><option value="21:00">21:00</option><option value="22:00">22:00</option><option value="23:00">23:00</option>                                             
                                                    </select>
                                                    <a style="display: none;" id="replica-checkbox">Click here to mark same timing for other days</a></td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input class="timing-checkbox"  data-id="2" type="checkbox" name="weekday[]">
                                                </td>
                                                <td>Tuesday</td>
                                                <td>
                                                    <select id='time_from_2' class="form-control" name="time_from[]">
                                                        <option></option> <option value="00:00">00:00</option><option value="01:00">01:00</option><option value="02:00">02:00</option><option value="03:00">03:00</option><option value="04:00">04:00</option><option value="05:00">05:00</option><option value="06:00">06:00</option><option value="07:00">07:00</option><option value="08:00">08:00</option><option value="09:00">09:00</option><option value="10:00">10:00</option><option value="11:00">11:00</option><option value="12:00">12:00</option><option value="13:00">13:00</option><option value="14:00">14:00</option><option value="15:00">15:00</option><option value="16:00">16:00</option><option value="17:00">17:00</option><option value="18:00">18:00</option><option value="19:00">19:00</option><option value="20:00">20:00</option><option value="21:00">21:00</option><option value="22:00">22:00</option><option value="23:00">23:00</option>                                              
                                                    </select>   
                                                </td>
                                                <td>
                                                    <select id='time_to_2' class="form-control" name="time_to[]">
                                                        <option></option>  <option value="00:00">00:00</option><option value="01:00">01:00</option><option value="02:00">02:00</option><option value="03:00">03:00</option><option value="04:00">04:00</option><option value="05:00">05:00</option><option value="06:00">06:00</option><option value="07:00">07:00</option><option value="08:00">08:00</option><option value="09:00">09:00</option><option value="10:00">10:00</option><option value="11:00">11:00</option><option value="12:00">12:00</option><option value="13:00">13:00</option><option value="14:00">14:00</option><option value="15:00">15:00</option><option value="16:00">16:00</option><option value="17:00">17:00</option><option value="18:00">18:00</option><option value="19:00">19:00</option><option value="20:00">20:00</option><option value="21:00">21:00</option><option value="22:00">22:00</option><option value="23:00">23:00</option>                                             
                                                    </select>  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input class="timing-checkbox" data-id="3" type="checkbox" name="weekday[]">
                                                </td>
                                                <td>Wednesday</td>
                                                <td>
                                                    <select id='time_from_3' class="form-control" name="time_from[]">
                                                        <option></option><option value="00:00">00:00</option><option value="01:00">01:00</option><option value="02:00">02:00</option><option value="03:00">03:00</option><option value="04:00">04:00</option><option value="05:00">05:00</option><option value="06:00">06:00</option><option value="07:00">07:00</option><option value="08:00">08:00</option><option value="09:00">09:00</option><option value="10:00">10:00</option><option value="11:00">11:00</option><option value="12:00">12:00</option><option value="13:00">13:00</option><option value="14:00">14:00</option><option value="15:00">15:00</option><option value="16:00">16:00</option><option value="17:00">17:00</option><option value="18:00">18:00</option><option value="19:00">19:00</option><option value="20:00">20:00</option><option value="21:00">21:00</option><option value="22:00">22:00</option><option value="23:00">23:00</option>                                              
                                                    </select>   
                                                </td>
                                                <td>
                                                    <select id='time_to_3' class="form-control" name="time_to[]">
                                                        <option></option> <option value="00:00">00:00</option><option value="01:00">01:00</option><option value="02:00">02:00</option><option value="03:00">03:00</option><option value="04:00">04:00</option><option value="05:00">05:00</option><option value="06:00">06:00</option><option value="07:00">07:00</option><option value="08:00">08:00</option><option value="09:00">09:00</option><option value="10:00">10:00</option><option value="11:00">11:00</option><option value="12:00">12:00</option><option value="13:00">13:00</option><option value="14:00">14:00</option><option value="15:00">15:00</option><option value="16:00">16:00</option><option value="17:00">17:00</option><option value="18:00">18:00</option><option value="19:00">19:00</option><option value="20:00">20:00</option><option value="21:00">21:00</option><option value="22:00">22:00</option><option value="23:00">23:00</option>                                             
                                                    </select>  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input  class="timing-checkbox" data-id="4" type="checkbox" name="weekday[]">
                                                </td>
                                                <td>Thursday</td>
                                                <td>
                                                    <select id='time_from_4' class="form-control" name="time_from[]">
                                                        <option></option>  <option value="00:00">00:00</option><option value="01:00">01:00</option><option value="02:00">02:00</option><option value="03:00">03:00</option><option value="04:00">04:00</option><option value="05:00">05:00</option><option value="06:00">06:00</option><option value="07:00">07:00</option><option value="08:00">08:00</option><option value="09:00">09:00</option><option value="10:00">10:00</option><option value="11:00">11:00</option><option value="12:00">12:00</option><option value="13:00">13:00</option><option value="14:00">14:00</option><option value="15:00">15:00</option><option value="16:00">16:00</option><option value="17:00">17:00</option><option value="18:00">18:00</option><option value="19:00">19:00</option><option value="20:00">20:00</option><option value="21:00">21:00</option><option value="22:00">22:00</option><option value="23:00">23:00</option>                                              
                                                    </select>   
                                                </td>
                                                <td>
                                                    <select id='time_to_4' class="form-control" name="time_to[]">
                                                        <option></option> <option value="00:00">00:00</option><option value="01:00">01:00</option><option value="02:00">02:00</option><option value="03:00">03:00</option><option value="04:00">04:00</option><option value="05:00">05:00</option><option value="06:00">06:00</option><option value="07:00">07:00</option><option value="08:00">08:00</option><option value="09:00">09:00</option><option value="10:00">10:00</option><option value="11:00">11:00</option><option value="12:00">12:00</option><option value="13:00">13:00</option><option value="14:00">14:00</option><option value="15:00">15:00</option><option value="16:00">16:00</option><option value="17:00">17:00</option><option value="18:00">18:00</option><option value="19:00">19:00</option><option value="20:00">20:00</option><option value="21:00">21:00</option><option value="22:00">22:00</option><option value="23:00">23:00</option>                                             
                                                    </select>  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input class="timing-checkbox" data-id="5"  type="checkbox" name="weekday[]">
                                                </td>
                                                <td>Friday</td>
                                                <td>
                                                    <select id='time_from_5' class="form-control" name="time_from[]">
                                                        <option></option> <option value="00:00">00:00</option><option value="01:00">01:00</option><option value="02:00">02:00</option><option value="03:00">03:00</option><option value="04:00">04:00</option><option value="05:00">05:00</option><option value="06:00">06:00</option><option value="07:00">07:00</option><option value="08:00">08:00</option><option value="09:00">09:00</option><option value="10:00">10:00</option><option value="11:00">11:00</option><option value="12:00">12:00</option><option value="13:00">13:00</option><option value="14:00">14:00</option><option value="15:00">15:00</option><option value="16:00">16:00</option><option value="17:00">17:00</option><option value="18:00">18:00</option><option value="19:00">19:00</option><option value="20:00">20:00</option><option value="21:00">21:00</option><option value="22:00">22:00</option><option value="23:00">23:00</option>                                              
                                                    </select>   
                                                </td>
                                                <td>
                                                    <select id='time_to_5' class="form-control" name="time_to[]">
                                                        <option></option> <option value="00:00">00:00</option><option value="01:00">01:00</option><option value="02:00">02:00</option><option value="03:00">03:00</option><option value="04:00">04:00</option><option value="05:00">05:00</option><option value="06:00">06:00</option><option value="07:00">07:00</option><option value="08:00">08:00</option><option value="09:00">09:00</option><option value="10:00">10:00</option><option value="11:00">11:00</option><option value="12:00">12:00</option><option value="13:00">13:00</option><option value="14:00">14:00</option><option value="15:00">15:00</option><option value="16:00">16:00</option><option value="17:00">17:00</option><option value="18:00">18:00</option><option value="19:00">19:00</option><option value="20:00">20:00</option><option value="21:00">21:00</option><option value="22:00">22:00</option><option value="23:00">23:00</option>                                             
                                                    </select>  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input class="timing-checkbox" data-id="6"  type="checkbox" name="weekday[]">
                                                </td>
                                                <td>Saturday</td>
                                                <td>
                                                    <select id='time_from_6' class="form-control" name="time_from[]">
                                                        <option></option> <option value="00:00">00:00</option><option value="01:00">01:00</option><option value="02:00">02:00</option><option value="03:00">03:00</option><option value="04:00">04:00</option><option value="05:00">05:00</option><option value="06:00">06:00</option><option value="07:00">07:00</option><option value="08:00">08:00</option><option value="09:00">09:00</option><option value="10:00">10:00</option><option value="11:00">11:00</option><option value="12:00">12:00</option><option value="13:00">13:00</option><option value="14:00">14:00</option><option value="15:00">15:00</option><option value="16:00">16:00</option><option value="17:00">17:00</option><option value="18:00">18:00</option><option value="19:00">19:00</option><option value="20:00">20:00</option><option value="21:00">21:00</option><option value="22:00">22:00</option><option value="23:00">23:00</option>                                              
                                                    </select>   
                                                </td>
                                                <td>
                                                    <select id='time_to_6' class="form-control" name="time_to[]">
                                                        <option></option> <option value="00:00">00:00</option><option value="01:00">01:00</option><option value="02:00">02:00</option><option value="03:00">03:00</option><option value="04:00">04:00</option><option value="05:00">05:00</option><option value="06:00">06:00</option><option value="07:00">07:00</option><option value="08:00">08:00</option><option value="09:00">09:00</option><option value="10:00">10:00</option><option value="11:00">11:00</option><option value="12:00">12:00</option><option value="13:00">13:00</option><option value="14:00">14:00</option><option value="15:00">15:00</option><option value="16:00">16:00</option><option value="17:00">17:00</option><option value="18:00">18:00</option><option value="19:00">19:00</option><option value="20:00">20:00</option><option value="21:00">21:00</option><option value="22:00">22:00</option><option value="23:00">23:00</option>                                             
                                                    </select>  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input  class="timing-checkbox" data-id="7" type="checkbox" name="weekday[]">
                                                </td>
                                                <td>Sunday</td>
                                                <td>
                                                    <select id='time_from_7' class="form-control" name="time_from[]">
                                                        <option></option> <option value="00:00">00:00</option><option value="01:00">01:00</option><option value="02:00">02:00</option><option value="03:00">03:00</option><option value="04:00">04:00</option><option value="05:00">05:00</option><option value="06:00">06:00</option><option value="07:00">07:00</option><option value="08:00">08:00</option><option value="09:00">09:00</option><option value="10:00">10:00</option><option value="11:00">11:00</option><option value="12:00">12:00</option><option value="13:00">13:00</option><option value="14:00">14:00</option><option value="15:00">15:00</option><option value="16:00">16:00</option><option value="17:00">17:00</option><option value="18:00">18:00</option><option value="19:00">19:00</option><option value="20:00">20:00</option><option value="21:00">21:00</option><option value="22:00">22:00</option><option value="23:00">23:00</option>                                              
                                                    </select>   
                                                </td>
                                                <td>
                                                    <select id='time_to_7' class="form-control" name="time_to[]">
                                                        <option></option> <option value="00:00">00:00</option><option value="01:00">01:00</option><option value="02:00">02:00</option><option value="03:00">03:00</option><option value="04:00">04:00</option><option value="05:00">05:00</option><option value="06:00">06:00</option><option value="07:00">07:00</option><option value="08:00">08:00</option><option value="09:00">09:00</option><option value="10:00">10:00</option><option value="11:00">11:00</option><option value="12:00">12:00</option><option value="13:00">13:00</option><option value="14:00">14:00</option><option value="15:00">15:00</option><option value="16:00">16:00</option><option value="17:00">17:00</option><option value="18:00">18:00</option><option value="19:00">19:00</option><option value="20:00">20:00</option><option value="21:00">21:00</option><option value="22:00">22:00</option><option value="23:00">23:00</option>                                             
                                                    </select>  
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                            <h3>Social Links</h3>
                            <section>

                                <div id="clone" class="last" style="display:none;">
                                    <div class="dt-sc-one-third column first">
                                        <input type="url" class="form-control" name="url[]">
                                    </div>
                                    <div class="dt-sc-one-third column">
                                        <select name="social[]" class="social-select  dt-sc-one-half column">
                                            <?php
                                            $social = $this->config->item('social');
                                            foreach ($social as $key => $value) {
                                                ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="dt-sc-one-third column">
                                        <button class="social-button remove">Remove</button>

                                    </div>
                                </div>

                                <div class="last">
                                    <div class="dt-sc-one-third column first">
                                        <input type="url" class="form-control" name="url[]">
                                    </div>
                                    <div class="dt-sc-one-third column">
                                        <select name="social[]" class="social-select  dt-sc-one-half column">
                                            <?php
                                            $social = $this->config->item('social');
                                            foreach ($social as $key => $value) {
                                                ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <button id="add" class="social-button">Add</button>

                            </section>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<link href="<?php echo CSS_PATH_FRONTEND; ?>jslider.css" rel="stylesheet" media="all" />
<link href="<?php echo CSS_PATH_FRONTEND; ?>jslider.blue.css" rel="stylesheet" media="all" />
<link href="<?php echo CSS_PATH_FRONTEND; ?>jslider.plastic.css" rel="stylesheet" media="all" />
<link href="<?php echo CSS_PATH_FRONTEND; ?>jslider.round.css" rel="stylesheet" media="all" />
<link href="<?php echo CSS_PATH_FRONTEND; ?>jslider.round.plastic.css" rel="stylesheet" media="all" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/basic.min.css" rel="stylesheet" media="all" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css" rel="stylesheet" media="all" />
<link href="<?php echo CSS_PATH_FRONTEND; ?>jquery.steps.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo JS_PATH_FRONTEND; ?>jquery.steps.min.js"></script>
<script type="text/javascript" src="<?php echo JS_PATH_FRONTEND; ?>dropzone.js"></script>
<link href="<?php echo CSS_PATH_FRONTEND; ?>select2.min.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="<?php echo JS_PATH_FRONTEND; ?>select2.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH_FRONTEND; ?>jquery.multiselect.css">
<script src="<?php echo JS_PATH_FRONTEND; ?>jquery.multiselect.js"></script>
<script src="<?php echo JS_PATH_FRONTEND; ?>jquery.validate.min.js"></script>

<script>
    jQuery(document).ready(function () {

        /*
         * 
         * validate form
         */
        jQuery.validator.addMethod("lettersonly", function (value, element) {
            return this.optional(element) || /^[a-zA-Z," "\s]+$/i.test(value);
        });
        jQuery.validator.addMethod("check_contact_no", function (value, element) {
            var response = false;
            jQuery.ajax({
                url: "<?php echo site_url('register/user/check_contact_no'); ?>",
                type: "POST",
                async: false,
                data: {contact_no: value},
                beforeSend: function () {

                },
                success: function (result) {
                    response = true;
                },
                error: function (result) {
                    response = false;
                }
            });
            return response;
        }, "Contact No already registered.Please login.");
        jQuery.validator.addMethod("check_email", function (value, element) {
            var response = false;
            jQuery.ajax({
                url: "<?php echo site_url('register/user/check_email_register'); ?>",
                type: "POST",
                async: false,
                data: {email_id: value},
                beforeSend: function () {

                },
                success: function (result) {
                    response = true;
                },
                error: function (result) {
                    response = false;
                }
            });
            return response;
        }, "Email id already registered.Please login.");
        jQuery.validator.addMethod("greaterThan",
                function (value, element, param) {
                    var $otherElement = jQuery(param);
                    return parseInt(value, 10) > parseInt($otherElement.val(), 10);
                });
        jQuery("#gym_registration_form").validate({
            rules: {
                'gym[name]':
                        {
                            required: true,
                            rangelength: [3, 40]
                        },
                'gym[password]': {
                    required: true,
                    rangelength: [7, 10]
                },
                'gym[description]':
                        {
                            rangelength: [3, 1000]
                        },
                'gym[center_type]':
                        {
                            required: true
                        },
                'gym[contact_no]': {
                    required: true,
                    rangelength: [10, 10],
                    digits: true,
                    check_contact_no: <?php echo isset($_SESSION['user']['id']) ? 'false' : 'true'; ?>
                },
                'gym[category]':
                        {
                            required: true
                        },
                'gym[email]': {
                    required: true,
                    email: true,
                    check_email: <?php echo isset($_SESSION['user']['id']) ? 'false' : 'true'; ?>
                },
                'gym[street_address_1]': {
                    rangelength: [3, 100]
                },
                'gym[street_address_2]': {
                    rangelength: [3, 100]
                },
                'gym[pincode]': {
                    digits: true,
                    rangelength: [6, 6]
                },
                'gym[locality]':
                        {
                            required: true
                        },
                'gym[website]': {
                    url: true,
                    rangelength: [10, 255]
                },
                'gym[services]':
                        {
                            required: true
                        },
                'gym[facilities]':
                        {
                            required: true
                        },
                'gym[min_price]':
                        {
                            required: true,
                            number: true,
                            min: 0,
                            rangelength: [1, 10]
                        },
                'gym[max_price]':
                        {
                            required: true,
                            number: true,
                            min: 0,
                            rangelength: [1, 10],
                            greaterThan: "#min_price"
                        },
                /* 'gym[facebook_url]':
                 {
                 required: true,
                 facebookURL: false,
                 rangelength: [3, 1000]
                 }, */
                'weekday[]': {
                    required: true,
                    minlength: 1
                },
                'profile':
                        {
                            required: true
                        },
                'services[]':
                        {
                            required: true
                        },
                'facilities[]':
                        {
                            required: true
                        }
            },
            highlight: function (element, errorClass, validClass) {

                var elem = jQuery(element);
                if (elem.hasClass('s-select2')) {
                    var isMulti = (!!elem.attr('multiple')) ? 's' : '';
                    elem.siblings('.sl').find('.select2-choice' + isMulti + '').addClass(errorClass);
                } else {
                    elem.addClass(errorClass);
                }

            },
            unhighlight: function (element, errorClass, validClass) {
                var elem = jQuery(element);
                if (elem.hasClass('sl')) {
                    elem.siblings('.sl').find('.select2-choice').removeClass(errorClass);
                } else {
                    elem.removeClass(errorClass);
                }
            },
            messages: {
                'gym[name]': {
                    required: "Name Required",
                    rangelength: "Minimum 3 & Maximum 40 Character Required"
                },
                'gym[description]':
                        {
                            rangelength: "Minimum 3 & Maximum 1000 Character Allowed"
                        },
                'gym[center_type]':
                        {
                            required: 'Center Type Required'
                        },
                'gym[contact_no]': {
                    required: "Contact Number Required",
                    rangelength: "10 Digits Required",
                    digits: "Enter Only Digits",
                    check_contact_no: "Contact No already registered.Please login."
                },
                'gym[category]':
                        {
                            required: 'Birth Date Required'
                        },
                'gym[email]': {
                    required: "Email Required",
                    email: "Enter Valid Email ID",
                    check_email: "Email already registered.Please login."
                },
                'gym[street_address_1]': {
                    rangelength: "Minimum 3 & Maximum 1000 Character Allowed"
                },
                'gym[street_address_2]': {
                    rangelength: "Minimum 3 & Maximum 1000 Character Allowed"
                },
                'gym[pincode]': {
                    digits: "Pincode Required",
                    rangelength: "6 Digits Required"
                },
                'gym[locality]':
                        {
                            required: "Locality Required"
                        },
                'gym[website]': {
                    url: 'Enter Valid Website Name',
                    rangelength: "Minimum 10 & Maximum 255 Character Allowed"
                },
                'gym[services]':
                        {
                            required: "Services Required"
                        },
                'gym[facilities]':
                        {
                            required: "Facilities Required"
                        },
                'gym[min_price]':
                        {
                            required: "Fees Required",
                            number: "Enter Valid Fees",
                            rangelength: "Too Large Fees"
                        },
                'gym[max_price]':
                        {
                            required: "Fees Required",
                            number: "Enter Valid Fees",
                            rangelength: "Too Large Fees",
                            greaterThan: "lowest price should  be less than highest price."
                        },
                /*'gym[facebook_url]':
                 {
                 required: "Facebook Link Required",
                 facebookURL: 'Enter Valid URL',
                 rangelength: "Minimum 3 & Maximum 1000 Character Allowed"
                 },*/
                'weekday[]': {
                    required: "Select atleast one day."
                },
                'time_from': {
                    required: "select timing"
                },
                'time_to': {
                    required: "select timing"
                },
                'profile': {
                    required: "select profile pic"
                }
            },
            errorElement: 'p',
            errorPlacement: function (error, element) {
                jQuery(element).parent().append(error);
            }
        });
        jQuery("#example-vertical").steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            stepsOrientation: "vertical",
            onStepChanging: function (e, currentIndex, newIndex) {
                var isValid = jQuery("#gym_registration_form").valid();
                var response = false;
                if (isValid == true) {
                    response = true;
                }
                if (currentIndex == 1) {
                    if (!jQuery('.dropzone-image').length) {
                        alert('kindly upload at least 1 image');
                        response = false;
                    }
                }
                if (currentIndex == 2) {
                    var lat = jQuery('#latitude').val();
                    var long = jQuery('#longitude').val();
                    //alert(lat);
                    //alert(long);
                    if (lat == "" || long == "") {
                        alert('Please select option from given list.');
                        response = false;
                    }
                }
                if (currentIndex == 4) {
                    jQuery('.timing-checkbox:checkbox:checked').each(function () {
                        if (jQuery("#time_from_" + jQuery(this).data("id")).val() === "") {
                            alert('kindly select from timing.');
                            response = false;
                        }
                        if (jQuery("#time_to_" + jQuery(this).data("id")).val() === "") {
                            alert('kindly select to timing.');
                            response = false;
                        }
                    });
                }
                return response;
            },
            onFinishing: function (event, currentIndex) {
                var isValid = jQuery("#gym_registration_form").valid();
                if (isValid == true) {
                    if (jQuery('.dropzone-image').length) {
                        jQuery("#gym_registration_form").submit();
                    } else {
                        alert('kindly upload at least 1 image');
                        jQuery("#example-vertical-t-1").get(0).click();
                    }
                }
            }
        });
        jQuery('.multiselect').multiselect({
            //columns: 1,
            placeholder: 'Select Amenities',
            search: true,
            selectAll: true
        });
        jQuery('select').on('change', function () {
            jQuery(this).valid();
        });
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone("#dropzoneFileUpload",
                {
                    url: "<?php echo site_url('image_upload/do_upload'); ?>",
                    paramName: "file", // The name that will be used to transfer the file
                    maxFilesize: 4, // MB,
                    dictDefaultMessage: "Drop File(s) Here or Click to Upload",
                    maxFiles: 6,
                    acceptedFiles: "image/*",
                    addRemoveLinks: true,
                    dictRemoveFile: "Remove Image",
                    dictInvalidFileType: "Only Image allowed",
                    removedfile: function (file) {
                        jQuery('input[type="hidden"][data-original="' + file.name + '"]').remove();
                        var _ref;
                        return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                    },
                    dictMaxFilesExceeded: "Maximum upload limit reached",
                    init: function () {
                        this.on("addedfile", function (file) {
                            var _this = this;
                            var removeButton = Dropzone.createElement("<span><input class=profile_pic type=\"radio\" name=\"profile\" value=\"" + file.name + "\" id=" + file.name + "> <label for=" + file.name + ">Set Profile</label></span>");
                            removeButton.addEventListener("click", function (e) {
                            });
                            file.previewElement.appendChild(removeButton);
                        });
                        this.on("success", function (file, responseText) {
                            result = JSON.parse(responseText);
                            var new_img_name = result['file_name'];
                            var ori_img_name = result['orig_name'];
                            var absolute_path = result['path'];
                            jQuery('#gym_registration_form').append('<input class="dropzone-image"  type="hidden" name="image[]" data-original="' + ori_img_name + '" value="' + new_img_name + '" />');
                        });
                    }
                });
        jQuery("#replica-checkbox").click(function () {
            jQuery(".timing-checkbox").prop("checked", true);
            jQuery("#time_from_2").val(jQuery("#time_from_1").val());
            jQuery("#time_to_2").val(jQuery("#time_to_1").val());
            jQuery("#time_from_3").val(jQuery("#time_from_1").val());
            jQuery("#time_to_3").val(jQuery("#time_to_1").val());
            jQuery("#time_from_4").val(jQuery("#time_from_1").val());
            jQuery("#time_to_4").val(jQuery("#time_to_1").val());
            jQuery("#time_from_5").val(jQuery("#time_from_1").val());
            jQuery("#time_to_5").val(jQuery("#time_to_1").val());
            jQuery("#time_from_6").val(jQuery("#time_from_1").val());
            jQuery("#time_to_6").val(jQuery("#time_to_1").val());
            jQuery("#time_from_7").val(jQuery("#time_from_1").val());
            jQuery("#time_to_7").val(jQuery("#time_to_1").val());
        });

        jQuery("#time_to_1").change(function () {
            jQuery("#replica-checkbox").show();
        });
        jQuery(".timing-checkbox").change(function () {
            if (this.checked) {

            } else {
                var id = jQuery(this).data("id");
                jQuery('#time_from_' + id).find('option:first').attr('selected', 'selected');
                jQuery('#time_to_' + id).find('option:first').attr('selected', 'selected');
            }
        });
        initAutocomplete();
        jQuery("#add").click(function (event) {
            event.preventDefault();
            /*jQuery("#gym_registration_form").submit(function (e) {
             return false;
             }); */
            //jQuery("#clone").clone().insertAfter("div.last:last");
            $clone = jQuery("#clone").clone().removeAttr('id'),
                    $clone.show();
            $clone.insertAfter("div.last:last");

        });

        jQuery(document).on('click', ".remove", function () {
            jQuery(this).closest('div').parent().remove();
        });

        jQuery(".select2").select2({
            allowClear: true
        });
    });
</script>