<?php

class Db_backup extends MX_Controller {

    function save_data() {
        $this->load->model('Global_model', 'gm');
        $this->load->helper('url');
        $this->load->helper('cron');
        $cron_name = 'Daily Mirror Backup';
        $cron_url = "http://pos.nailspaexperience.com/og/ogwork/crons/db_backup/save_data/";
        $start_time = date('Y-m-d H:i:s');
        $cron_id = log_cron_start($cron_name, $cron_url, $start_time);
        // $this->load->helper('url'); 
// old server mysql id
        $DBIUser = $this->db->username;
        $DBIPass = $this->db->password;
        $oldServer = $this->db->hostname;
        $originalDB = $this->db->database;

        $backup_db = $this->load->database('backup_db', TRUE);

// new server mysql id
        $NewUser = $new_db->username;
        $NewPass = $new_db->password;
        $newServer = $new_db->hostname;
        $newDB = $new_db->database;


        /*  $sql = "SHOW TABLES";
          $sql_exe = $this->db->query($sql);
          $originalDBs = $sql_exe->result_array(); */



        /*
         * Create Table if not exist
         */
        /* if (isset($all_log_table) && is_array($all_log_table) && count($all_log_table) > 0) {
          foreach ($all_log_table as $key => $tablevalue) {
          $create_sql = "CREATE TABLE " . $tablevalue['table_name'] . " LIKE " . $originalDB . "." . $tab;
          $backup_db->query($create_sql);
          }
          } */
        $last_idq = "SELECT * FROM `config` WHERE status=1";
        $last_idq_exe = $backup_db->query($last_idq);
        $last_id_data = $last_idq_exe->result_array();


        if (isset($last_id_data) && is_array($last_id_data) && count($last_id_data) > 0) {
            foreach ($last_id_data as $ckey => $cvalue) {
                $config_data[$cvalue['config_key']] = $cvalue['config_value'];
            }
            $last_backup_id = isset($config_data['last_backup_table_id']) ? $config_data['last_backup_table_id'] : '';
            $last_stop_id = isset($config_data['last_stop_id']) ? $config_data['last_stop_id'] : '';
        }

        /*
         * Get All Table name
         */
        $table_limit = 2;
        if ($last_backup_id != '') {
            $all_tableq = "SELECT * FROM backup_table WHERE status =1 AND id > " . $last_backup_id . " LIMIT " . $table_limit;
        } else {
            $all_tableq = "SELECT * FROM backup_table WHERE status =1 LIMIT " . $table_limit;
        }
        $all_tableq_exe = $backup_db->query($all_tableq);
        $all_log_table = $all_tableq_exe->result_array();


        /*
         * Insert/Update Table Record
         */
        $today_date = date('Y-m-d');

        // $query_date = date('Y-m-d', strtotime('-2 day', strtotime($today_date)));

        $invoice_rel_table = array(
            0 => 'tblAppointmentAssignEmployee',
            1 => 'tblAppointmentMembershipDiscount',
            2 => 'tblAppointmentsCharges',
            3 => 'tblAppointmentsChargesInvoice',
            4 => 'tblAppointmentsDetails',
            5 => 'tblAppointmentsDetailsInvoice',
            6 => 'tblInvoice',
            7 => 'tblInvoiceDetails'
        );


        echo '<pre>';
        print_r($all_log_table);

        if (isset($config_data[$today_date]) && $config_data[$today_date] == 1) {
            /*
             * Todays backup Done
             */
        } else {
            if (isset($all_log_table) && is_array($all_log_table) && count($all_log_table) > 0) {
                foreach ($all_log_table as $key => $tablevalue) {

                    /*
                     * Check if table is sync or not
                     */
                    /* $backdoneq = "SELECT * FROM backup_table_log "
                      . " WHERE table_name='" . $tablevalue['table_name'] . "' AND created_date LIKE '%" . date('Y-m-d') . "%' AND backup_done=1";
                      //echo "<br>Query=" . $backdoneq;
                      $backdoneq_exe = $backup_db->query($backdoneq);
                      $backup_exist = $backdoneq_exe->result_array(); */

                    $append_q = '';
                    if ($tablevalue['get_last_id'] == '1' && $tablevalue['last_row_id'] > 0) {
                        $append_q = " WHERE " . $tablevalue['primary_column_name'] . " > " . $tablevalue['last_row_id'];
                    }

                    /*
                     * Get Table Total Row Count
                     */
                    $row_limit = 20000;


                    if ($tablevalue['table_name'] == 'tblAppointments') {
                        if ($append_q == '') {
                            $append_q .= " WHERE AppointmentDate >='" . $query_date . "'";
                        } else {
                            $append_q .= " AND AppointmentDate >='" . $query_date . "'";
                        }
                    } else if (in_array($tablevalue['table_name'], $invoice_rel_table)) {
                        /*  $apt_in_ids[0] = 0;
                          $appointmentq = "SELECT * FROM tblAppointments WHERE AppointmentDate >='" . $query_date . "'";
                          $appointmentq_exe = $this->db->query($appointmentq);
                          $appointment_data = $appointmentq_exe->result_array();
                          if (isset($appointment_data) && is_array($appointment_data) && count($appointment_data) > 0) {
                          foreach ($appointment_data as $akey => $avalue) {
                          $apt_in_ids[$avalue['AppointmentID']] = $avalue['AppointmentID'];
                          }
                          }

                          if (isset($apt_in_ids) && is_array($apt_in_ids) && count($apt_in_ids) > 0) {
                          $apt_ids = implode(",", $apt_in_ids);
                          if ($apt_ids != '') {
                          if ($append_q == '') {
                          $concat = ' WHERE';
                          } else {
                          $concat = ' AND';
                          }
                          if ($tablevalue['table_name'] == 'tblInvoiceDetails') {
                          $append_q .= $concat . " AppointmentId IN(" . $apt_ids . ")";
                          } else {
                          $append_q .= $concat . " AppointmentID IN(" . $apt_ids . ")";
                          }
                          }
                          } */
                    }
                    $append_q = '';
                    $get_table_dataq = "SELECT * FROM " . $tablevalue['table_name'] . $append_q;
                    $get_table_data_exe = $this->db->query($get_table_dataq);
                    $table_total_rows = $get_table_data_exe->num_rows();

                    echo "<br>row=" . $table_total_rows;
                    echo "<br>qry=" . $get_table_dataq;
                    if ($table_total_rows > 0) {

                        $total_row_iteration = $table_total_rows / $row_limit;
                        $row_iter = ceil($total_row_iteration);
                        for ($ir = 0; $ir < $row_iter; $ir++) {
                            $row_offset = $ir * $row_limit;
                            $get_table_dataq = "SELECT * FROM " . $tablevalue['table_name'] . $append_q . " ORDER BY " . $tablevalue['primary_column_name'] . ""
                                    . " LIMIT " . $row_limit . " OFFSET " . $row_offset;
                            $get_table_data_exe = $this->db->query($get_table_dataq);
                            $table_res = $get_table_data_exe->result_array();

                            $row_count = isset($table_res) ? count($table_res) : 0;
                            //echo "<br>TableRow=" . count($table_res);
                            $limit = 100;
                            if ($row_count > 0) {
                                $total_iteration = $row_count / $limit;
                                $iter = ceil($total_iteration);
                                for ($i = 0; $i < $iter; $i++) {
                                    $offset = $i * $limit;
                                    $slice_data = array_slice($table_res, $offset, $limit);
                                    $col = array();
                                    $row_value = array();
                                    if (isset($slice_data) && is_array($slice_data) && count($slice_data) > 0) {
                                        foreach ($slice_data as $ikey => $ivalue) {
                                            foreach ($ivalue as $ickey => $icvalue) {
                                                $col[$ickey] = $ickey;
                                                $row_value[$ikey][$ickey] = "'" . $icvalue . "'";
                                            }
                                        }
                                    }


                                    if (isset($row_value) && is_array($row_value) && count($row_value) > 0) {
                                        $replace_query = "REPLACE INTO " . $tablevalue['table_name'] . " (" . implode(",", $col) . ") VALUES";

                                        foreach ($row_value as $rkey => $rvalue) {
                                            if ((count($row_value) - 1) == $rkey) {
                                                $replace_query .= " (" . implode(",", $rvalue) . ");";
                                            } else {
                                                $replace_query .= " (" . implode(",", $rvalue) . "),";
                                            }
                                        }


                                        //echo "<br>replaceq=" . $replace_query;
                                        $backup_db->query($replace_query);

                                        if ($tablevalue['get_last_id'] == '1') {
                                            $last_index = count($row_value) - 1;
                                            $last_id = $row_value[$last_index][$tablevalue['primary_column_name']];
                                            $last_id = trim($last_id, "'");

                                            $update_last = "UPDATE backup_table SET last_row_id ='" . $last_id . "' WHERE table_name='" . $tablevalue['table_name'] . "'";
                                            $backup_db->query($update_last);
                                            //echo "<br>Updateq=" . $update_last;
                                        }
                                    }
                                }
                            }

                            /*
                             * Mark Todays Days Backup Done for table
                             */
                            $insert_log = "INSERT INTO backup_table_log(table_name,created_date,backup_done ) "
                                    . " VALUES ('" . $tablevalue['table_name'] . "','" . date('Y-m-d H:i:s') . "','1')";
                            //echo "<br>Insertq=" . $insert_log;
                            $backup_db->query($insert_log);
                        }
                    }

                    /*
                     * Update Config if last row
                     */
                    echo "<br>last_id=" . $last_stop_id;
                    echo "<br>cur_id=" . $tablevalue['id'];
                    if (isset($last_stop_id) && $last_stop_id == $tablevalue['id']) {

                        $updateq = "UPDATE config SET config_value =2 WHERE config_key ='last_backup_table_id'";
                        echo 'if=' . $updateq;
                        $backup_db->query($updateq);


                        $insertq = "INSERT INTO config(config_key,config_value,created_date)"
                                . " VALUES('" . date('Y-m-d') . "','1','" . date('Y-m-d H:i:s') . "')";
                        echo 'if=' . $insertq;
                        $backup_db->query($insertq);
                        exit;
                    } else {
                        $updateq = "UPDATE config SET config_value =" . $tablevalue['id'] . " WHERE config_key ='last_backup_table_id'";
                        $backup_db->query($updateq);

                        //redirect('crons/db_backup/save_data?id='.$tablevalue['id']);
                    }
                }
            }
        }
        $end_time = date('Y-m-d H:i:s');
        log_cron_end($cron_id, $end_time);
    }

}

?>