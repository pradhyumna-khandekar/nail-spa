<?php

class Script extends MX_Controller {

    function __construct() {
        parent::__construct();
    }

    function update_upsell_product() {
        $this->load->model('Global_model', 'gm');
        $qry = "SELECT * FROM `tbl_online_payment_info` WHERE `service_provider` LIKE '%pay%' AND created_date > '2019-12-31'";
        $qry_exe = $this->db->query($qry);
        $qry_data = $qry_exe->result_array();

        if (isset($qry_data) && is_array($qry_data) && count($qry_data) > 0) {
            foreach ($qry_data as $key => $value) {
                /*
                 * Get Customer ID
                 */
                $mobile_no = $value['contact'];
                $custexist_q = "Select * from tblCustomers Where SUBSTRING(CustomerMobileNo , -10) LIKE '" . substr(trim($mobile_no), -10) . "'";
                $custexist_exe = $this->db->query($custexist_q);
                $cust_data = $custexist_exe->row_array();
                if (count($cust_data) == 0) {
                    $custexist_q = "Select * from tblCustomers Where CustomerMobileNo LIKE '%" . trim($mobile_no) . "%'";
                    $custexist_exe = $this->db->query($custexist_q);
                    unset($cust_data);
                    $cust_data = $custexist_exe->row_array();
                }

                if (isset($cust_data) && is_array($cust_data) && count($cust_data) > 0) {
                    $strCustomerID = $cust_data['CustomerID'];
                    $post_data = unserialize($value['data']);

                    /*
                     * Get Appointment ID
                     */
                    $strAppointmentDate1 = isset($post_data['appointment_date']) ? date('Y-m-d', strtotime($post_data['appointment_date'])) : 0;
                    $strStoreID = isset($post_data['store_id']) ? $post_data['store_id'] : 0;

                    $aptq = "SELECT AppointmentID FROM tblAppointments WHERE CustomerID ='" . $strCustomerID . "'"
                            . " AND AppointmentDate ='" . $strAppointmentDate1 . "' AND StoreID='" . $strStoreID . "'";
                    $aptq_exe = $this->db->query($aptq);
                    $apt_data = $aptq_exe->result_array();

                    /*
                     * Get Service id
                     */
                    $service_data = array();
                    $all_services_str = isset($post_data['services']) ? $post_data['services'] : '';
                    $all_services = explode(",", $all_services_str);
                    if (isset($all_services) && is_array($all_services) && count($all_services) > 0) {
                        foreach ($all_services as $sname_key => $sname_value) {
                            $serviceq = "SELECT ServiceID FROM tblServices WHERE ServiceCode ='" . $sname_value . "' AND StoreID ='" . $strStoreID . "'";
                            $serviceq_exe = $this->db->query($serviceq);
                            $service_data[] = $serviceq_exe->row_array();
                        }
                    }

                    if (isset($apt_data) && is_array($apt_data) && count($apt_data) > 0) {
                        foreach ($apt_data as $akey => $avalue) {
                            $final_data[$strCustomerID][$value['id']]['apt_id'] = $avalue['AppointmentID'];
                            $final_data[$strCustomerID][$value['id']]['store_id'] = $strStoreID;
                            if (isset($service_data) && is_array($service_data) && count($service_data) > 0) {
                                foreach ($service_data as $sikey => $sivalue) {
                                    $final_data[$strCustomerID][$value['id']]['service_id'][$sivalue['ServiceID']] = $sivalue['ServiceID'];
                                }
                            }
                        }
                    } else {
                        echo "<br>APT Not=" . $aptq;
                    }
                } else {
                    echo "<br>Not=" . $mobile_no;
                }
            }
        }

        echo '<pre>';
        print_r($final_data);
        
        if (isset($final_data) && is_array($final_data) && count($final_data) > 0) {
            foreach ($final_data as $ckey => $cvalue) {
                foreach ($cvalue as $data_key => $data_value) {
                    if (count($data_value['service_id']) > 1) {
                        $updaq = "UPDATE tblAppointmentsDetailsInvoice SET is_upsell = 1 WHERE AppointmentID='" . $data_value['apt_id'] . "'";
                        $this->db->query($updaq);

                        foreach ($data_value['service_id'] as $dskey => $dsvalue) {
                            if ($dsvalue > 0) {
                                $updaq = "UPDATE tblAppointmentsDetailsInvoice SET is_upsell = 2 WHERE AppointmentID='" . $data_value['apt_id'] . "' AND ServiceID ='" . $dsvalue . "'";
                                $this->db->query($updaq);
                            }
                        }
                    } else {
                        $updaq = "UPDATE tblAppointmentsDetailsInvoice SET is_upsell = 2 WHERE AppointmentID='" . $data_value['apt_id'] . "'";
                        $this->db->query($updaq);
                    }
                }
            }
        }
    }

}
