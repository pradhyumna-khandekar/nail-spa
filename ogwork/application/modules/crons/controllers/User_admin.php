<?php

class User_admin extends CI_Controller {

    function index() {
        $this->show_list();
    }

    function show_form() {
        $this->load->helper('url');
        $data = array();
        $this->_display('register_user_show_form', $data);
    }

    function update() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $id = $this->input->post('service_id');


        $user_information = $this->input->post('user_information');
        //echo '<pre>';
        //print_r($user_information);exit;

        unset($user_information['payment']);

        $this->gm->update('user_information', $user_information, $id, array('status' => 1));


        $payment = $this->input->post('payment');

        //echo '<pre>';
        //print_r($payment);
        foreach ($payment as $key => $value) {

            if (strpos($value, '/') !== false) {
                //                echo 'true';

                $pay_date = explode('-', $value);
                $pay_date_start = explode('/', $pay_date[0]);
                $pay_date_end = explode('/', $pay_date[1]);
                $payment_information['pay_start_date'] = trim($pay_date_start[2]) . '-' . trim($pay_date_start[0]) . '-' . trim($pay_date_start[1]);
                $payment_information['pay_end_date'] = trim($pay_date_end[2]) . '-' . trim($pay_date_end[0]) . '-' . trim($pay_date_end[1]);

                $this->gm->update('payment', $payment_information, 0, array('user_id' => $id,'call_type' => $key));
                //echo '<pre>';
                //print_r($this->db->last_query());
                
            }
        }

        //exit;
        $this->session->set_flashdata('feedback', "Your data has been Updated Successfully!");

        redirect('register/user_admin');
    }

    function edit($id) {

        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        //$row = $this->gm->get_selected_record('service', 'id,stocks,cmp,target,stop,remarks,trade,share_info', array('id' => $id, 'status' => '1'));

        $query = "SELECT * FROM `user` left outer join `user_information` on user.id = user_information.user_id where user.id = " . $id . "";
        //echo $query;exit;

        $query_exec = $this->db->query($query);

        $result = $query_exec->row_array();
        
        $query = "SELECT * FROM `payment` where user_id = " . $id . "";
        //echo $query;exit;

        $query_exec = $this->db->query($query);

        $pay = $query_exec->result_array();
        
        foreach ($pay as $key => $value) {
            $payment[$value['call_type']] = $value;
        }
        
        $data['mode'] = 'update';
        $data['result'] = $result;
        $data['payment'] = $payment;
        //echo '<pre>';
        //print_r($data);exit;

        $this->_display('register_register_admin_show_form', $data);
    }

    function delete($id) {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');

        $this->gm->update('user', array('status' => 0), $id, array('status' => 1));
        $this->session->set_flashdata('feedback', "Your data has been Deleted Successfully!");

        redirect('register/user_admin');
        // $this->show_list();
    }

    function insert() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        /*
         * get user sign up data
         */
        $user_information = $this->input->post('user_information');
        if (isset($user_information) && is_array($user_information) && count($user_information) > 0) {

            unset($user_information['repassword']);
            /*
             * insert email and password in user table
             */
            $user['email'] = $user_information['email'];
            $user['password'] = md5(trim(strip_tags($user_information['password'])));
            $user['created_date'] = date('Y-m-d h:i:s');

            $user_id = $this->gm->insert('user', $user);
            /*
             * insert other information in user_information table
             */
            unset($user_information['email']);
            unset($user_information['password']);
            unset($user_information['repassword']);
            $user_information['created_date'] = date('Y-m-d h:i:s');
            $user_information['ip_address'] = $this->input->ip_address();
            $user_information['http_ref'] = $_SERVER['HTTP_REFERER'];
            $user_information['user_agent'] = $this->input->user_agent();
            $user_information['user_id'] = $user_id;

            $id = $this->gm->insert('user_information', $user_information);


            /*
             * add user data to session
             */
            $data = array(
                'user' => array(
                    'id' => $user_id,
                    'email' => $user['email'],
                    'logged_in' => TRUE,
                )
            );
            $this->session->set_userdata($data);

            redirect(site_url());
        } else {
            redirect(site_url());
        }
    }

    function _display($view, $data) {
        $this->load->view('admin_header', $data);
        $this->load->view('sidebar', $data);
        $this->load->view($view, $data);
        $this->load->view('admin_footer', $data);
    }

    function show_list() {

        $this->load->helper('url');
        $page = $this->uri->segment(4);

        if ($page == 0) {
            $offset = 0;
        } else {
            $offset = ($page - 1) * 10;
        }
        if ($offset < 0) {
            $offset = 0;
        }
        $query = "SELECT * FROM `user` left outer join `user_information` on user.id = user_information.user_id where user.status=1 ORDER BY user.id  DESC limit 10 offset " . $offset . "";
        //echo $query;exit;

        $query_exec = $this->db->query($query);

        $result = $query_exec->result_array();



        $cquery = "SELECT count(user.id) as id FROM `user` left outer join `user_information` on user.id = user_information.user_id where user.status=1";

        $cquery_exec = $this->db->query($cquery);
        $count = $cquery_exec->result_array();


        $data['result'] = $result;
        $data['count'] = $count['0']['id'];
        $data['offset'] = $offset;
        //echo '<pre>';
        //print_r($data);exit;

        $this->load->library('pagination');

        $config['base_url'] = site_url('register/user_admin/show_list');
        $config['total_rows'] = $count['0']['id'];

        $config['per_page'] = 10;
        if (count($_GET) > 0) {
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        }
        $config['num_links'] = 2;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = '<div class="pagination-custom"><ul class="pagination">';
        $config['full_tag_close'] = '</ul></div>';
        $config['first_link'] = 'FIRST';
        $config['first_tag_open'] = '&nbsp;<li class="paginate_button " title="Go to first page">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'LAST';
        $config['last_tag_open'] = '&nbsp;<li class="paginate_button " title="Go to last page">';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = 'NEXT';
        $config['next_tag_open'] = '&nbsp;<li class="paginate_button "  title="Go to next page">';
        $config['next_tag_close'] = '</li>&nbsp;';
        $config['prev_link'] = 'PREVIOUS';
        $config['prev_tag_open'] = '&nbsp;<li class="paginate_button " title="Go to previous page">';
        $config['prev_tag_close'] = '</li>&nbsp;';
        $config['cur_tag_open'] = '&nbsp;<li class="paginate_button active" title="Current page">';
        $config['cur_tag_close'] = '</li>&nbsp;';
        $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
        $this->pagination->initialize($config);

        $this->_display('register_register_admin_show_list', $data);
    }

    function check_email() {
        if ($this->input->is_ajax_request()) {
            $this->load->model('Global_model', 'gm');
            $data = array();
            $user = $this->input->post('user_information');

            $where = array(
                'email' => $user['email']
            );
            $data = $this->gm->get_selected_record('user', 'id', $where);
            $temp = array();



            if (isset($data['id']) && $data['id'] > 0) {
                $temp['id'] = 0;
            } else {
                $temp['id'] = 1;
            }
            echo json_encode($temp);
        } else {
            $this->load->helper('url');
            redirect(site_url());
        }
    }

}

?>