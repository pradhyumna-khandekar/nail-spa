<?php

class User extends MX_Controller {

    function __construct() {


        parent::__construct();

        //if (!$this->login->_is_logged_in()) {
        //  redirect('login?view=backend_login');
        //}
    }

    function index() {
        $this->show_form();
    }

    function autocomplete() {
        $this->load->model('Global_model', 'gm');
        $q = $this->input->post('q');
        $sql = "SELECT * FROM `routes` WHERE `status` = 1 AND `name` LIKE '%" . $q . "%' AND `entity_type` IN (1,2) LIMIT 5 ";
        $sql_exec = $this->db->query($sql);
        $data['list'] = $sql_exec->result_array();
        $this->load->view('autocomplete', $data);
        //echo '<pre>';
        //print_r($sql_exec->result_array());
    }

    function check_contact_no() {

        //if ($this->input->is_ajax_request()) {
        if (TRUE) {
            $this->load->model('Global_model', 'gm');
            $data = $this->gm->get_selected_record('user', $select = "*", $where = array('contact_no' => $this->input->post('contact_no')), $order_by = '');
            //print_r($this->db->last_query());
            //print_r($data);
            if (isset($data['id']) && $data['id'] > 0) {
                http_response_code(401);
            } else {
                http_response_code(200);
            }
        } else {
            $this->load->helper('url');
            redirect(site_url());
        }
    }

    function trainer() {

        $this->load->helper('url');
        $this->load->library('user_agent');
        $this->load->helper('frontend_common_helper');
        $this->load->model('Global_model', 'gm');
        $data = array();
        $state_list = $this->gm->get_data_list('states', $where = array('country_id' => 101), $like = array(), $order_by = array(), $select = '*', $limit = 0, '');
        foreach ($state_list as $key => $value) {
            $state[$value['id']] = $value['name'];
        }
        $sql = "SELECT * FROM `cities` WHERE `state_id` IN (" . (implode(',', array_flip($state))) . ")";
        $sql_exec = $this->db->query($sql);
        $city_list = $sql_exec->result_array();
        foreach ($city_list as $key => $value) {
            $city[$value['state_id']]['city'][$value['id']] = $value['name'];
            $city[$value['state_id']]['name'] = $state[$value['state_id']];
        }
        $data['state_city'] = $city;
        $this->_display('register_user_trainer', $data);
    }

    function gym() {
        $this->load->helper('url');
        $this->load->library('user_agent');
        $this->load->helper('frontend_common_helper');
        $this->load->model('Global_model', 'gm');
        $data = array();

        $state_list = $this->gm->get_data_list('states', $where = array('country_id' => 101), $like = array(), $order_by = array(), $select = '*', $limit = 0, '');
        foreach ($state_list as $key => $value) {
            $state[$value['id']] = $value['name'];
        }
        $sql = "SELECT * FROM `cities` WHERE `state_id` IN (" . (implode(',', array_flip($state))) . ")";
        $sql_exec = $this->db->query($sql);
        $city_list = $sql_exec->result_array();
        foreach ($city_list as $key => $value) {
            $city[$value['state_id']]['city'][$value['id']] = $value['name'];
            $city[$value['state_id']]['name'] = $state[$value['state_id']];
        }
        $data['state_city'] = $city;
        //print_r($data);
        //exit;
        $this->_display('register_user_gym', $data);
    }

    function insert() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        /*
         * get user sign up data
         */
        $user_information = $this->input->post('user_information');
        if (isset($user_information) && is_array($user_information) && count($user_information) > 0) {

            unset($user_information['repassword']);
            /*
             * insert email and password in user table
             */
            $user['email'] = $user_information['email'];
            $user['password'] = md5(trim(strip_tags($user_information['password'])));
            $user['created_date'] = date('Y-m-d h:i:s');

            $user_id = $this->gm->insert('user', $user);
            /*
             * insert other information in user_information table
             */
            unset($user_information['email']);
            unset($user_information['password']);
            unset($user_information['repassword']);
            $user_information['created_date'] = date('Y-m-d h:i:s');
            $user_information['ip_address'] = $this->input->ip_address();
            $user_information['http_ref'] = $_SERVER['HTTP_REFERER'];
            $user_information['user_agent'] = $this->input->user_agent();
            $user_information['user_id'] = $user_id;

            $id = $this->gm->insert('user_information', $user_information);
            $i = 0;
            for ($i == 0; $i < 6; $i++) {
                $payment = array(
                    'call_type' => $i,
                    'user_id' => $id,
                    'pay_start_date' => date('Y-m-d'),
                    'pay_end_date' => date('Y-m-d', strtotime("+2 days"))
                );
                $this->gm->insert('payment', $payment);
            }

            /*
             * add user data to session
             */
            $data = array(
                'user' => array(
                    'id' => $user_id,
                    'email' => $user['email'],
                    'logged_in' => TRUE,
                )
            );
            $this->session->set_userdata($data);

            redirect(site_url());
        } else {
            redirect(site_url());
        }
    }

    function _display($view, $data) {
        $this->load->view('frontend_header', $data);
        $this->load->view($view, $data);
        $this->load->view('frontend_footer', $data);
    }

    function check_email_register() {
        if ($this->input->is_ajax_request()) {
            $this->load->model('Global_model', 'gm');
            $data = $this->gm->get_selected_record('user', $select = "*", $where = array('email_id' => $this->input->post('email_id')), $order_by = '');
            //print_r($this->db->last_query());
            //print_r($data);
            if (isset($data['id']) && $data['id'] > 0) {
                http_response_code(401);
            } else {
                http_response_code(200);
            }
        }
    }

    function check_email() {
        if ($this->input->is_ajax_request()) {

            $this->load->model('Global_model', 'gm');

            $data = array();
            $temp = array();

            $where = array(
                'email' => $this->input->post('login_email')
            );
            $data = $this->gm->get_selected_record('user', 'id', $where);
            // print_r($this->db->last_query());
            //exit;
            $userid = $data['id'];
            if ($data['id'] < '0') {
                echo json_encode(0);
                exit;
            } else {

                $email = $this->input->post('login_email');

                unset($data);
                $data = array();
                $data['email'] = $email;
                $otp = rand(100000, 999999);
                $body = "Your verification code=" . $otp;
                $this->load->library('email');

                $config['protocol'] = "smtp";
                $config['mailtype'] = "html";
                $config['charset'] = "utf-8";
                $config['priority'] = "1";
                //$config['smtp_host'] = "24mehta.com";
                //$config['smtp_user'] = "noreply@24mehta.com";
                //$config['smtp_pass'] = "fn$=+S#PA!RM";
                $config['smtp_host'] = "weqcorporation.in";
                $config['smtp_user'] = "yuvraj@weqcorporation.in";
                $config['smtp_pass'] = "YuvRaj080203010";
                $config['smtp_port'] = "587";
                //$config['smtp_port'] = "587";

                $this->email->initialize($config);

                $this->email->from('noreply@weqcorporation.in', '24mehta.com');
                $this->email->to($email, $email);
                $this->email->subject('Verification code ' . date('D-M-Y h:i:sa'));

                $this->email->message($body);
                if (!$this->email->send()) {
                    echo 'error';
                }
                //$this->email->send();
                //print_r($body);
                //echo $this->email->print_debugger();
                //echo $data;
                //echo $email;

                $forgot_password = array(
                    'user_id' => $userid,
                    'email' => $email,
                    'otp' => $otp,
                    'created_date' => date('y-m-d H:i:s')
                );

                $this->gm->insert('forgot_password', $forgot_password);
                //print_r($this->db->last_query());
                //exit;
                echo json_encode(intval($userid));
                exit;
                //$this->load->helper('url');
                //redirect(site_url('register/user/show_forgot_form').'?id='.$userid);
            }
        } else {
            $this->load->helper('url');
            redirect(site_url());
        }
    }

    function show_forgot_form() {
        $data = array();
        $this->load->helper('url');
        $this->load->library('user_agent');
        $this->load->helper('frontend_common_helper');
        $this->_display('register_user_show_forgot_form', $data);
    }

    function forgot() {
        echo 'sfd';

        $this->load->model('Global_model', 'gm');
        $email = $this->input->get('login_email');

        $row = $this->gm->get_selected_record('user', 'id', array('email' => addslashes($email), 'status' => '1'));
        print_r($row);
        if (isset($row) && is_array($row) && count($row) > 0) {
            $data['email'] = $email;
            $otp = rand(100000, 999999);
            $body = "Your verification code=" . $otp;
            $this->load->library('email');

            $config['protocol'] = "smtp";
            $config['mailtype'] = "html";
            $config['charset'] = "utf-8";
            $config['priority'] = "1";
            $config['smtp_host'] = "24mehta.com";
            $config['smtp_user'] = "noreply@24mehta.com";
            $config['smtp_pass'] = "fn$=+S#PA!RM";
            $config['smtp_port'] = "587";

            $this->email->initialize($config);

            $this->email->from('noreply@24mehta.com', '24mehta.com');
            $this->email->to($email, $email);
            $this->email->subject('Verification code ' . date('D-M-Y h:i:sa'));
            print_r($body);
            $this->email->message($body);
            $this->email->send();

            $forgot_password = array(
                'user_id' => $row['id'],
                'email' => $email,
                'otp' => $otp,
                'created_date' => date('y-m-d H:i:s')
            );

            $data = $this->gm->insert('forgot_password', $forgot_password);

            $result = array(
                'email' => $email
            );
            //echo json_encode($result);
        } else {
            //echo 0;
        }
    }

    function check_otp() {

        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $user_information = $this->input->post('user_information');
        $where = array(
            'otp' => $user_information['otp'],
            'user_id' => $user_information['id']
        );
        $data = $this->gm->get_selected_record('forgot_password', 'id', $where);
        //echo '<pre>';
        //print_r($this->db->last_query());
        //print_r($data);
        echo json_encode(intval($data['id']));
        exit;
    }

    function reset_password() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $user_information = $this->input->post('user_information');

        $user_information['modified_date'] = date('y-m-d h:i:s');
        $user_information['password'] = md5(trim(strip_tags($user_information['password'])));
        $row = $this->gm->update('user', array('password' => $user_information['password'], 'modified_date' => $user_information['modified_date']), $id = '', array('id' => $user_information['id']));
        //print_r($this->db->last_query());
        //exit;
        redirect(site_url());
    }

}

?>