<?php

class Sms extends MX_Controller {

    function __construct() {


        parent::__construct();

        //if (!$this->login->_is_logged_in()) {
        //  redirect('login?view=backend_login');
        //}
    }

    function insert_sms_log_non_visiting_customers() {
        $this->load->model('Global_model', 'gm');

        $this->load->helper('cron');
        $cron_name = 'Insert Non-visiting Customer';
        $cron_url = "http://pos.nailspaexperience.com/og/ogwork/crons/sms/insert_sms_log_non_visiting_customers";
        $start_time = date('Y-m-d H:i:s');
        $cron_id = log_cron_start($cron_name, $cron_url, $start_time);


        $n5_daysAgoyu = date('Y-m-d', strtotime('-50 days', time()));
        $sql = "SELECT DISTINCT apt.StoreID,apt.AppointmentDate,c.`CustomerID`,c.`CustomerMobileNo`,c.CustomerFullName FROM `tblCustomers` as c "
                . " JOIN `tblAppointments` as apt on c.`CustomerID` = apt.`CustomerID` "
                . " WHERE apt.`AppointmentDate` = '" . $n5_daysAgoyu . "' AND apt.status=2";
        $sql_exec = $this->db->query($sql);
        $appoint_data = $sql_exec->result_array();
     
        $storeq = "SELECT * FROM tblStores";
        $storeq_exe = $this->db->query($storeq);
        $store_data = $storeq_exe->result_array();
        if (isset($store_data) && is_array($store_data) && count($store_data) > 0) {
            foreach ($store_data as $skey => $svalue) {
                $store_result[$svalue['StoreID']] = $svalue;
            }
        }


        if (isset($appoint_data) && is_array($appoint_data) && count($appoint_data) > 0) {
            foreach ($appoint_data as $aptkey => $aptvalue) {
                $data[$aptvalue['CustomerID']] = $aptvalue;
            }
        }



        /*
         * add data main campaign table
         */

        $user['SMSEvent'] = 'Non visiting customers from last 50 days';
        $user['SMSDate'] = date('Y-m-d');
        $user['SMSContent'] = '';
        $user['Status'] = 2;

        $this->gm->insert('tblSMSApproval', $user);
        unset($user);

        /*
         * add customer id and contact no into sms table
         */
        if (isset($data) && is_array($data) && count($data)) {
            /*
             * GET non-visiting customer sms
             */
            $msg_data = $this->gm->get_selected_record('tblnotification_msg', '*', array('message_type' => 'non_visiting_customer', 'status' => 1), '');
            if (isset($msg_data) && is_array($msg_data) && count($msg_data) > 0) {
                if (isset($msg_data['message']) && $msg_data['message'] != '') {
                    foreach ($data as $key => $value) {
                       
                        /*
                         * Check if Customer has appointment done after that date or not
                         */
                        $apt_sql = "SELECT * FROM tblAppointments apt WHERE apt.`AppointmentDate` > '" . $n5_daysAgoyu . "' "
                                . " AND apt.status=2 AND CustomerID='" . $value['CustomerID'] . "'";
                        $apt_sql_exe = $this->db->query($apt_sql);
                        $apt_exist = $apt_sql_exe->result_array();
                        
                        if (count($apt_exist) == 0) {
                            $store_name = isset($store_result[$value['StoreID']]) ? $store_result[$value['StoreID']]['StoreName'] : '';
                            $StoreNumber = isset($store_result[$value['StoreID']]) ? $store_result[$value['StoreID']]['StoreOfficialNumber'] : '';
                            $user['CustomerID'] = $value['CustomerID'];
                            $user['SMSTo'] = $value['CustomerMobileNo'];

                            $sms_content = $msg_data['message'];
                            $sms_content = isset($value['CustomerFullName']) ? str_replace('{customer_name}', ucwords($value['CustomerFullName']), $sms_content) : $sms_content;
                            $sms_content = isset($store_name) ? str_replace('{store_name}', ucwords($store_name), $sms_content) : $sms_content;
                            $sms_content = isset($StoreNumber) ? str_replace('{store_no}', $StoreNumber, $sms_content) : $sms_content;
                            $user['Message'] = $sms_content;
                            echo $sms_content;exit;
                            //$user['Message'] = "Dear " . $value['CustomerFullName'] . ", you have earned Cash Voucher worth Rs.250/-. "
                            //      . " Pls visit our salon or cal " . $store_name . " to redeem the voucher. Looking forward to see you soon!";
                            $user['Status'] = 1;
                            $user['created_date'] = date('Y-m-d H:i:s');
                            $user['description'] = '50Days Reminder SMS';
                            $user['msg_status'] = 1;
                            $this->gm->insert('tblsmsmessages', $user);
                            unset($user);
                        }
                    }
                }
            }
        }

        $end_time = date('Y-m-d H:i:s');
        log_cron_end($cron_id, $end_time);
    }

    function send_non_visiting_sms() {
        $this->load->model('Global_model', 'gm');
        $this->load->helper('sms_helper');

        $this->load->helper('cron');
        $cron_name = 'Send SMS Non-visiting Customer';
        $cron_url = "http://pos.nailspaexperience.com/og/ogwork/crons/sms/send_non_visiting_sms";
        $start_time = date('Y-m-d H:i:s');
        $cron_id = log_cron_start($cron_name, $cron_url, $start_time);

        $data = $this->gm->get_data_list('tblsmsmessages', $where = array('msg_status' => 1, 'Status' => 1, 'description' => '50Days Reminder SMS'), $like = array(), $order_by = array(), $select = '*', $limit = 100, $like_or);

        if (isset($data) && is_array($data) && count($data)) {
            foreach ($data as $key => $value) {
                $mobile_no = $value['SMSTo'];
                $message = $value['Message'];
                $sms_response = CreateSMSURL("", "0", "0", $mobile_no, $message);
                if ($sms_response == true) {
                    $sms_update_data = array(
                        'msg_status' => 2,
                        'send_datetime' => date('Y-m-d H:i:s')
                    );
                } else {
                    $sms_update_data = array(
                        'msg_status' => 3,
                        'send_datetime' => date('Y-m-d H:i:s')
                    );
                }
                $this->gm->update('tblsmsmessages', $sms_update_data, '', array('SMSID' => $value['SMSID']));
            }
        }

        $end_time = date('Y-m-d H:i:s');
        log_cron_end($cron_id, $end_time);
    }

    function send_sms() {
        $this->load->model('Global_model', 'gm');
        $this->load->helper('sms_helper');

        $this->load->helper('cron');
        $cron_name = 'Send SMS Customer';
        $cron_url = "http://pos.nailspaexperience.com/og/ogwork/crons/sms/send_sms";
        $start_time = date('Y-m-d H:i:s');
        $cron_id = log_cron_start($cron_name, $cron_url, $start_time);

        $data = $this->gm->get_data_list('tblsmsmessages', $where = array('msg_status' => 1, 'Status' => 1), $like = array(), $order_by = array(), $select = '*', $limit = 100, $like_or);
        if (isset($data) && is_array($data) && count($data)) {
            foreach ($data as $key => $value) {
                $mobile_no = $value['SMSTo'];
                $message = $value['Message'];
                $sms_response = CreateSMSURL("Service Reminder", "0", "0", $message, $mobile_no);
                if ($sms_response == true) {
                    $sms_update_data = array(
                        'msg_status' => 2,
                        'send_datetime' => date('Y-m-d H:i:s')
                    );
                } else {
                    $sms_update_data = array(
                        'msg_status' => 3,
                        'send_datetime' => date('Y-m-d H:i:s')
                    );
                }
                $this->gm->update('tblsmsmessages', $sms_update_data, '', array('SMSID' => $value['SMSID']));
            }
        }

        $end_time = date('Y-m-d H:i:s');
        log_cron_end($cron_id, $end_time);
    }

    function send_email() {
        $this->load->model('Global_model', 'gm');
        $this->load->helper('email_helper');

        $this->load->helper('cron');
        $cron_name = 'Send Email Customer';
        $cron_url = "http://pos.nailspaexperience.com/og/ogwork/crons/sms/send_email";
        $start_time = date('Y-m-d H:i:s');
        $cron_id = log_cron_start($cron_name, $cron_url, $start_time);

        $data = $this->gm->get_data_list('tblEmailMessages', $where = array('Status' => 0, 'description' => 'customer list bulk Email'), $like = array(), $order_by = array(), $select = '*', $limit = 30, $like_or);

        if (isset($data) && is_array($data) && count($data)) {
            foreach ($data as $key => $value) {
                $recipient_email = $value['ToEmail'];
                $subject = $value['Subject'];
                $body = $value['Body'];
                $from_email = $value['FromEmail'];
                send_email($recipient_email, $subject, $body, $from_email, "", "", "", array(), array('email_id' => $value['ID']));
            }
        }

        $end_time = date('Y-m-d H:i:s');
        log_cron_end($cron_id, $end_time);
    }

}

?>