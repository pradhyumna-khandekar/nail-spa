<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function CreateSMSURL($campaign, $unicode, $flash, $message, $sms) {
    $sms_response = false;
    $xmldata = '<?xml version="1.0" encoding="UTF-8"?>
                <api>
                    <campaign>Service Reminder</campaign>
					
                    <unicode>0</unicode>
                    <flash>0</flash>
                    <sender>NAILSP</sender>
                    <message><![CDATA[' . $message . ']]></message>
                        <sms>
                            <to>' . $sms . '</to>
                        </sms>
                </api>';

    $api_url = 'http://api-alerts.solutionsinfini.com/v3/?method=sms.xml&api_key=Aebe2e79fd5ce8a3ffb0720d4bef17fe9&format=json&xml=' . urlencode($xmldata);


    $response = file_get_contents($api_url);
    $response = json_decode($response, true);

    $strSendingStatus = $response['status'];

    if ($strSendingStatus == 'OK') {
        $sms_response = true;
    } else {
        $sms_response = false;
    }

    return $sms_response;
}
?>