<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!function_exists('active')) {

    function active($uri, $level = 1, $class_suffix = 'active') {
        $ci = & get_instance();

        if ($ci->uri->segment($level) == $uri) {
            return $class_suffix;
        } else
            return;
    }

}