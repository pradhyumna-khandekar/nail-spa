<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function get_google_login_link() {
    require_once FCPATH . 'social_login/google/src/Google/autoload.php';
    $client = new Google_Client();
    $client->setApplicationName(APPLICATION_NAME);
    $client->setClientId(CLIENT_ID);
    $client->setClientSecret(CLIENT_SECRET);
    $client->setRedirectUri(site_url() . 'login/g_success');
    $client->setScopes(array('https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/userinfo.profile'));
    return $authUrl = $client->createAuthUrl();
}

function get_data() {
    require_once FCPATH . 'social_login/google/src/Google/autoload.php';
    //exit;
    $client = new Google_Client();
    $client->setApplicationName(APPLICATION_NAME);
    $client->setClientId(CLIENT_ID);
    $client->setClientSecret(CLIENT_SECRET);
    $client->setRedirectUri(site_url() . 'login/g_success');
    $client->setScopes(array('https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/userinfo.profile'));
    $google_oauthV2 = new Google_Service_Plus($client);
    if (isset($_GET['code'])) {
        $client->authenticate($_GET['code']);
        $token = $client->getAccessToken();
        if (isset($token)) {
            $access_token = json_decode($token, true);
            $q = 'https://www.googleapis.com/oauth2/v1/userinfo?access_token=' . $access_token['access_token'];
            $json = file_get_contents($q);
            $userInfoArray = json_decode($json, true);
            return $userInfoArray;
        }
    } else {
        return FALSE;
    }
}

?>