<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function log_cron_start($cron_name, $cron_url, $start_time) {
    $cron_id = '';
    $CI = &get_instance();
    $data = array(
        'cron_name' => $cron_name,
        'cron_url' => $cron_url,
        'start_time' => $start_time,
    );
    $cron_id = $CI->gm->insert('cron_log', $data);
    return $cron_id;
}

function log_cron_end($cron_id, $end_time) {
    $CI = &get_instance();
    $CI->gm->update('cron_log', array('end_time' => $end_time), $cron_id);
}
