<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function get_facebook_login_link() {
    require_once FCPATH . 'social_login/facebook/src/Facebook/autoload.php';
    $fb = new Facebook\Facebook([
        'app_id' => '768325916653539',
        'app_secret' => '112e52d120d3538b165482586b499b20',
        'default_graph_version' => 'v2.5',
    ]);
    $helper = $fb->getRedirectLoginHelper();
    $permissions = ['email', 'public_profile', 'user_birthday']; // optional
    return $helper->getLoginUrl(site_url() . 'login/f_success?close', $permissions);
}

function get_facebook_data() {

    require_once FCPATH . 'social_login/facebook/src/Facebook/autoload.php';
    $fb = new Facebook\Facebook([
        'app_id' => '768325916653539',
        'app_secret' => '112e52d120d3538b165482586b499b20',
        'default_graph_version' => 'v2.5',
    ]);
    $helper = $fb->getRedirectLoginHelper();

    $accessToken = $helper->getAccessToken();

    $response = $fb->get('/me?fields=id,name,first_name,last_name,middle_name,email,birthday', $accessToken);
    return $response;
}
