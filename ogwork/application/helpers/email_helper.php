<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function send_email($recipient_email, $subject, $body, $from_email, $reply_email = "", $cc_email = "", $bcc_email = "", $attachment = array(), $entity_data = array()) {
    $CI = &get_instance();
    $return_data = FALSE;

    $strbody1 = $body;
    $headers = "From: $from_email\r\n";
    $headers .= "Content-type: text/html\r\n";
    $strBodysa = $strbody1;

    // Mail sending 
    $retval = mail($recipient_email, $subject, $strBodysa, $headers);
    $email_id = 0;
    if (isset($entity_data['email_id']) && $entity_data['email_id'] != '') {
        $email_id = $entity_data['email_id'];
    }
    if ($retval == true) {
        $update_qry = "UPDATE tblEmailMessages SET Status='1',DateOfSending='" . date('Y-m-d H:i:s') . "'"
                . " WHERE ID='" . $email_id . "'";
        $CI->db->query($update_qry);
        $return_data = TRUE;
    } else {
        $update_qry = "UPDATE tblEmailMessages SET Status='2'"
                . " WHERE ID='" . $email_id . "'";
        $CI->db->query($update_qry);
        $return_data = FALSE;
    }

    return $return_data;
}
