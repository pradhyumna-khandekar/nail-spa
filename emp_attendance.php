<?php require_once("setting.fya"); ?>
<?php require_once 'incEmpLoginData.php'; ?>
<?php
$strPageTitle = "Employee Attendance | NailSpa";
$strDisplayTitle = "Employee Attendance for NailSpa";
$strMenuID = "10";
$strMyTable = "tblAppointments";
$strMyTableID = "AppointmentID";
$strMyField = "AppointmentDate";
$strMyActionPage = "emp_attendance.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>

    </head>

    <body>

        <div id="sb-site">


            <?php require_once("incOpenLayout.fya"); ?>


            <?php require_once("incLoader.fya"); ?>


            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("EmpLeftMenu.php"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>


                        <div class="panel">
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="example-box-wrapper">

                                        <div id="normal-tabs-1">
                                            <span class="form_result">&nbsp; <br>
                                            </span>
                                            <h4 class="title-hero"><center><?php echo 'Employee Attendance For ' . date('d/m/Y'); ?> | NailSpa</center></h4>
                                            <div class="col-md-4 col-xs-12 margin-bot-zero no-pad btn" style="border-color: #FC8213;"><b id="cur_distance"></b></div>

                                            <table id="printdata" class="table table-striped table-bordered table-responsive no-wrap printdata" cellspacing="0" width="100%">

                                                <thead>
                                                    <tr>
                                                        <th>Employee Name</th>
                                                        <th>Store Name</th>
                                                        <th>Date</th>
                                                        <th>CheckIn & CheckOut</th>

                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <?php
                                                    $emp_data = select("*", "tblEmployees", "EID='" . $strEmpID . "'");

                                                    if (isset($emp_data) && is_array($emp_data) && count($emp_data) > 0) {
                                                        $store_id = isset($emp_data[0]['StoreID']) ? $emp_data[0]['StoreID'] : 0;
                                                        $store_data = select("*", "tblStores", "StoreID='" . $store_id . "'");
                                                        $latitude = isset($store_data[0]['latitude']) ? ucwords($store_data[0]['latitude']) : '';
                                                        $longitude = isset($store_data[0]['longitude']) ? ucwords($store_data[0]['longitude']) : '';


                                                        $emp_code = isset($emp_data[0]['EmployeeCode']) ? $emp_data[0]['EmployeeCode'] : '';
                                                        $TodaysDate = date('Y-m-d');
                                                        if ($emp_code != '') {
                                                            $attendance_data = select("*", "tblEmployeesRecords", "EmployeeCode='" . $emp_code . "' AND DateOfAttendance='" . $TodaysDate . "'");
                                                        }
                                                        // var_dump($attendance_data);
                                                        // exit();
                                                        ?>
                                                        <tr>
                                                            <td><?php echo isset($emp_data[0]['EmployeeName']) ? ucwords($emp_data[0]['EmployeeName']) : '' ?></td>
                                                            <td><?php echo isset($store_data[0]['StoreName']) ? ucwords($store_data[0]['StoreName']) : '' ?></td>
                                                            <td><?php echo date('d M,Y'); ?></td>
                                                            <td>

                                                                <!-- <div class="in_out_div" style="display:none;"> -->
                                                                <div class="in_out_div">
                                                                    <?php
                                                                    if (isset($attendance_data) && is_array($attendance_data) && count($attendance_data) > 0) 
                                                                    {
                                                                        $LoginTime = $attendance_data[0]['LoginTime'];
                                                                        $LogoutTime = $attendance_data[0]['LogoutTime'];
                                                                        $strERID = $attendance_data[0]['ERID'];

                                                                        $currtime = date('H:i:s');
                                                                        $checktimewith = date('H:i:s', strtotime($LoginTime) + 3600);


                                                                        if ($LoginTime == "00:00:00") {
                                                                            ?>
                                                                            <div align="center" id="Checkin">
                                                                                <button class="btn btn-xs btn-primary result_message<?= $strERID ?>"  value="Checkin" id="Checkin" name="Checkin" onclick="Checkin(<?= $strERID ?>);">Checkin</button>
                                                                            </div>
                                                                            <?php
                                                                        } else if ($LogoutTime == "00:00:00" && $LoginTime != "00:00:00" && $currtime > $checktimewith) {
                                                                            ?>
                                                                            <div align="center"  id="CheckOut<?= $strERID ?>"> <a class=" btn btn-xs btn-primary  result_message1<?= $strERID ?>" value="CheckOut" id="CheckOut" name="CheckOut" onclick="CheckOut(<?= $strERID ?>);" >CheckOut</a>
                                                                            </div>

                                                                            <?php
                                                                        } else {
                                                                            $timesql = "Select TIMEDIFF(LogoutTime,LoginTime) as Hour1 from tblEmployeesRecords Where ERID = $strERID";
                                                                            $RS = $DB->query($timesql);
                                                                            if ($RS->num_rows > 0) {
                                                                                $row = $RS->fetch_assoc();
                                                                                $x = $row['Hour1'];
                                                                                echo ($currtime > $checktimewith) ? "Completed " . $x . " Hours." : '';
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php } else { ?>
                                                        <tr>
                                                            <td colspan="3">No Employee Found...</td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php require_once 'incFooter.fya'; ?>
        </div>
        <script>

            function Checkin(a1)
            {
                // alert (a1);
                var ERID = a1;
                $.ajax({
                    type: 'POST',
                    url: 'AttendanceCheckin.php',
                    data: {getERID: ERID, type: 1},
                    success: function (response) {
                        $('.result_message').html(response);
                        location.reload();
                    }
                });
            }
            function CheckOut(a2)
            {
                var ERID1 = a2;
                $.ajax({
                    type: 'POST',
                    url: 'AttendanceCheckout.php',
                    data: {getERID: ERID1, type: 1},
                    success: function (response) {
                        document.getElementById("CheckOut" + ERID1).innerHTML = response;
                    }

                });
            }

            function geocords() {
                var startPos;

                var geoSuccess = function (position) {

                    startPos = position;
                    // document.getElementById('startLat').innerHTML = startPos.coords.latitude;
                    // document.getElementById('startLon').innerHTML = startPos.coords.longitude;

                    var latcurr = startPos.coords.latitude;
                    var longcurr = startPos.coords.longitude;

                    var store_lat = '<?php echo isset($latitude) ? $latitude : ''; ?>';
                    var store_long = '<?php echo isset($longitude) ? $longitude : ''; ?>';

                    getDistanceFromLatLonInM(latcurr, longcurr, store_lat, store_long);
                };

                navigator.geolocation.getCurrentPosition(geoSuccess);
            }



            function getDistanceFromLatLonInM(lat1, lon1, lat2, lon2) {
                
                var R = 6371000; // Radius of the earth in km
                var dLat = deg2rad(lat2 - lat1);  // deg2rad below
                var dLon = deg2rad(lon2 - lon1);
                var a =
                        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                        Math.sin(dLon / 2) * Math.sin(dLon / 2)
                        ;
                var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                var d = R * c; // Distance in km
                document.getElementById('cur_distance').innerHTML = 'Distance in Meter : ' + d.toFixed(2);
                var distance = d.toFixed(2);

                var emp_store_id = '<?php echo isset($store_id) ? $store_id : ''; ?>';
                
                /*
                 * If kochi make distance 300 meter
                 */
                if (emp_store_id == '109') {
                    var store_dis = 300;
                } else {
                    var store_dis = 50;
                }
                console.log(distance);
                console.log(store_dis);

                // if (distance <= store_dis) {
                //     $(".in_out_div").show();
                // } else {
                //     $(".in_out_div").hide();
                // }
                if (true) {
                    $(".in_out_div").show();
                } else {
                    $(".in_out_div").hide();
                }

            }

            function deg2rad(deg) {
                return deg * (Math.PI / 180)
            }

            $(document).ready(function () {
                geocords();
            }
            );
        </script>
    </body>
</html>
