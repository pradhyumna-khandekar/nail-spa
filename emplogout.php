<?php

$pasttime = time() - 3600;
setcookie("CookieEmpID", "", $pasttime + (10 * 365 * 24 * 60 * 60));
setcookie("CookieEmpFullName", "", $pasttime + (10 * 365 * 24 * 60 * 60));
setcookie("CookieEmpUsername", "", $pasttime + (10 * 365 * 24 * 60 * 60));

session_start();
session_unset();
session_destroy();
header('Location: ../admin/emplogin.php');
?>