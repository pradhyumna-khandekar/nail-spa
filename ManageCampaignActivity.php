<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Manage Campaign Activity | Nailspa";
$strDisplayTitle = "Manage Campaign Activity for Nailspa";
$strMenuID = "10";
$strMyTable = "tblMembership";
$strMyTableID = "MembershipID";
$strMyField = "MembershipName";
$strMyActionPage = "ManageCampaignActivity.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if($strAdminType!="0")
{
die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights	



if ($_SERVER["REQUEST_METHOD"] == "POST")
{
$strStep = Filter($_POST["step"]);
if($strStep=="add")
{
foreach($_POST as $key => $val)
{
if($key!="step")
{
if(IsNull($sqlColumn))
{
$sqlColumn = $key;
$sqlColumnValues = "'".$_POST[$key]."'";
}
else
{
$sqlColumn = $sqlColumn.",".$key;
$sqlColumnValues = $sqlColumnValues.", '".$_POST[$key]."'";
}
}

}

// foreach($_POST as $key => $val)
// {
// echo $key ." = ".$_POST[$key];
// }
// die();
$CampaignName = Filter($_POST["CampaignName"]);
$ActivityID = Filter($_POST["ActivityID"]);
$Status = Filter($_POST["Status"]);
$OfferID = $_POST["OfferID"];
$StoreID = $_POST["StoreID"];

$Qty = Filter($_POST["Qty"]);
$TotalCost = Filter($_POST["TotalCost"]);
$StartDate = Filter($_POST["StartDate"]);
$EndDate = Filter($_POST["EndDate"]);

$stores = implode(",", $StoreID);
$offers = implode(",", $OfferID);
$stadate = date('Y-m-d', strtotime($StartDate));
$endate = date('Y-m-d', strtotime($EndDate));

//print_r($_POST);

$DB = Connect();
$sql = "SELECT CampaignActivityID FROM tblCampaignActivity WHERE CampaignName='$CampaignName' and StoreID='$StoreID' and StartDate>='$stadate' and EndDate<='$endate' and OfferID='$OfferID'";
$RS = $DB->query($sql);
if ($RS->num_rows > 0)
{
$DB->close();
die('<div class="alert alert-close alert-danger">
					<div class="bg-red alert-icon"><i class="glyph-icon icon-times"></i></div>
					<div class="alert-content">
						<h4 class="alert-title">Record Add Failed</h4>
						<p>This Activity with the same Name already exists. Please try again with a different Name.</p>
					</div>
				</div>');
}
else
{
$sqlInsert = "INSERT INTO tblCampaignActivity (CampaignName,ActivityID, StoreID,OfferID,Qty,TotalCost,StartDate,EndDate,Status) VALUES 
				('".$CampaignName."','".$ActivityID."', '".$StoreID."', '".$OfferID."', '".$Qty."', '".$TotalCost."', '".$stadate."', '".$endate."', '".$Status."')";
ExecuteNQ($sqlInsert);
//echo $sqlInsert;
// die();

$DB->close();
die('<div class="alert alert-close alert-success">
					<div class="bg-green alert-icon"><i class="glyph-icon icon-check"></i></div>
					<div class="alert-content">
						<h4 class="alert-title">Record Added Successfully.</h4>
						
					</div>
				</div>');
}
}

if($strStep=="edit")
{
$DB = Connect();
$CampaignName = Filter($_POST["CampaignName"]);
$ActivityID = Filter($_POST["ActivityID"]);
$OfferID = $_POST["OfferID"];
$StoreID = $_POST["StoreID"];
$Status = Filter($_POST["Status"]);

$Qty = Filter($_POST["Qty"]);
$TotalCost = Filter($_POST["TotalCost"]);
$StartDate = Filter($_POST["StartDate"]);
$EndDate = Filter($_POST["EndDate"]);

$stores = implode(",", $StoreID);
$offers = Filter($_POST["OfferID"]);
$stadate = date('Y-m-d', strtotime($StartDate));
$endate = date('Y-m-d', strtotime($EndDate));

$strMyTableID = Filter($_POST["CampaignActivityID"]);
$sql = "SELECT CampaignActivityID FROM tblCampaignActivity WHERE CampaignName='$CampaignName' and StoreID='$StoreID' and StartDate>='$stadate' and EndDate<='$endate' and OfferID='$OfferID'";
$RS = $DB->query($sql);
// echo $sql."<br>";
if ($RS->num_rows > 1)
{
$DB->close();
die('<div class="alert alert-close alert-danger">
					<div class="bg-red alert-icon"><i class="glyph-icon icon-times"></i></div>
					<div class="alert-content">
						<h4 class="alert-title">Record Add Failed</h4>
						<p>This Activity with the same Name already exists. Please try again with a different Name.</p>
					</div>
				</div>');
}
else
{

////////////////////////////////////////////////////////////////////////////////

$sqlUpdate = "UPDATE tblCampaignActivity SET CampaignName='$CampaignName',ActivityID='$ActivityID',StoreID='$StoreID',Qty='$Qty',OfferID='$OfferID',TotalCost='$TotalCost',EndDate='$endate',StartDate='$stadate',Status='$Status' WHERE CampaignActivityID='".Decode($strMyTableID)."'";
ExecuteNQ($sqlUpdate);
// echo $sqlUpdate;
}
$DB->close();
die('<div class="alert alert-close alert-success">
					<div class="bg-green alert-icon"><i class="glyph-icon icon-check"></i></div>
					<div class="alert-content">
						<h4 class="alert-title">Record Updated Successfully</h4>
					</div>
				</div>');
}
die();
}
?>


<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <script>
            $(function ()
            {
                $("#StartDay").datepicker({minDate: 0});
                $("#EndDay").datepicker({minDate: 0});
                $("#StartDay1").datepicker({minDate: 0});
                $("#EndtDay1").datepicker({minDate: 0});

            });
            function calcost()
            {
                //alert(11)
                var Qty = $("#Qty").val();
                var ActivityID = $("#ActivityID").val();

                if (Qty != "0" && Qty != "")
                {
                    $.ajax({
                        type: "post",
                        data: "Qty=" + Qty + "&ActivityID=" + ActivityID,
                        url: "UpdateCampaignCost.php",
                        success: function (res)
                        {
                            //alert(res)
                            var cost = $.trim(res);
                            $("#TotalCost").val(cost);



                        }

                    })

                }
            }
            function selectoffer(evt)
            {

                var typef = $(evt).val();

                $.ajax({
                    type: "post",
                    data: "StoreID=" + typef,
                    url: "SelectStoreOffers.php",
                    success: function (res)
                    {
                        //alert(res)
                        $("#showoffer").html(res)

                    }

                })


            }

        </script>
        <script>
            $(function ()
            {
                $("#StartDate").datepicker({minDate: 0});
                $("#EndDate").datepicker({minDate: 0});
            });
        </script>
        <!-----------css & js files added for tabs by gandhali 5/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>
        
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");   ?>
            <!----------commented by gandhali 5/9/18---------------->
            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>


                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                            <p>Add, Edit, Delete Campaign Activity</p>
                        </div>
                        <?php
                        if(!isset($_GET["uid"]))
                        {
                        ?>					

                        <div class="panel">
                            <div class="panel">
                                <div class="panel-body">

                                    <div class="example-box-wrapper">
                                        <div class="tabs">
                                            <ul>
                                                <li><a href="#normal-tabs-1" title="Tab 1">Manage</a></li>
                                                <li><a href="#normal-tabs-2" title="Tab 2">Add</a></li>
                                            </ul>
                                            <div id="normal-tabs-1">
                                                <span class="form_result">&nbsp; <br>
                                                </span>

                                                <div class="panel-body">
                                                    <h3 class="title-hero">List of Campaign | Nailspa</h3>
                                                    <div class="example-box-wrapper">
                                                        <table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                                                            <thead>
                                                                <tr>
                                                                    <th>Sr.No</th>
                                                                    <th>Campaign</th>
                                                                    <th>Activity</th>
                                                                    <th>Store</th>
                                                                    <th>Offer</th>
                                                                    <th>Qty</th>
                                                                    <th>TotalCost</th>
                                                                    <th>StartDate</th>
                                                                    <th>EndDate</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tfoot>
                                                                <tr>
                                                                    <th>Sr.No</th>
                                                                    <th>Campaign</th>
                                                                    <th>Activity</th>
                                                                    <th>Store</th>
                                                                    <th>Offer</th>
                                                                    <th>Qty</th>
                                                                    <th>TotalCost</th>
                                                                    <th>StartDate</th>
                                                                    <th>EndDate</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                            </tfoot>
                                                            <tbody>

<?php
// Create connection And Write Values
$DB = Connect();
$sql = "SELECT * FROM tblCampaignActivity";
// echo $sql."<br>";
$RS = $DB->query($sql);
if ($RS->num_rows > 0)
{
$counter = 0;

while($row = $RS->fetch_assoc())
{
$counter ++;
$CampaignActivityID = $row["CampaignActivityID"];
$getUID = EncodeQ($CampaignActivityID);
$getUIDDelete = Encode($CampaignActivityID);
$CampaignName = $row["CampaignName"];
$ActivityID = $row["ActivityID"];
$seldata1 = select("Name", "tblActivityCostCenter", "ActivityCostCenterID='$ActivityID'");
$Name = $seldata1[0]['Name'];
$StoreID = $row["StoreID"];
$stt = explode(",", $StoreID);

$OfferID = $row["OfferID"];
$off = explode(",", $OfferID);

$Qty = $row["Qty"];
$TotalCost = $row["TotalCost"];
$StartDate = $row["StartDate"];
$EndDate = $row["EndDate"];
?>	
                                                                <tr id="my_data_tr_<?= $counter ?>">
                                                                    <td><?= $counter ?></td>
                                                                    <td><?= $CampaignName ?></td>
                                                                    <td><?= $Name ?></td>

                                                                    <td>
<?php
if($StoreID=='0')
{

$StoreNames = "All";
}
else
{
$seldata1S = select("StoreName", "tblStores", "StoreID='$StoreID'");
$StoreNames = $seldata1S[0]['StoreName'];
}

echo $tstioe = $StoreNames."<br/>";
?>
                                                                    </td>
                                                                    <td>
                                                                        <?php
                                                                        $seldata1SZXC = select("OfferName", "tblOffers", "OfferID='$OfferID'");
                                                                        $OfferName = $seldata1SZXC[0]['OfferName'];
                                                                        echo $OfferNamezcvsn = $OfferName."<br/>";
                                                                        ?>
                                                                    </td>
                                                                    <td>
                                                                        <?= $Qty ?>
                                                                    </td>
                                                                    <td>
                                                                        <?= $TotalCost ?>
                                                                    </td>
                                                                    <td>
                                                                        <?= date('d/m/Y', strtotime($StartDate)) ?>
                                                                    </td>
                                                                    <td>
                                                                        <?= date('d/m/Y', strtotime($EndDate)) ?>
                                                                    </td>

                                                                    <td style="text-align: center">
                                                                        <a class="btn btn-link" href="<?= $strMyActionPage ?>?uid=<?= $getUID ?>">Edit</a>
<?php
if($strAdminRoleID=="36")
{
?>
                                                                        <a class="btn btn-link font-red" font-redhref="javascript:;" onclick="DeleteData('Step38', '<?= $getUIDDelete ?>', 'Are you sure you want to delete - <?= $AdminFullName ?>?', 'my_data_tr_<?= $counter ?>');">Delete</a>
                                                                        <?php
                                                                        }
                                                                        ?>
                                             <!--   <a href="DisplayCampaignDetails.php?uid=<?= $getUID ?>" class="btn btn-xs btn-primary" >Details</a>-->

                                                                        <br>

                                                                    </td>
                                                                </tr>
                                                                        <?php
                                                                        }
                                                                        }
                                                                        else
                                                                        {
                                                                        ?>															
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td>No Records Found</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>



                                                                </tr>

<?php
}
$DB->close();
?>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="normal-tabs-2">
                                                <div class="panel-body">
                                                    <form role="form" class="form-horizontal bordered-row enquiry_form" onSubmit="proceed_formsubmit('.enquiry_form', '<?= $strMyActionPage ?>', '.result_message', '', '', '');
                                                                                                        return false;">

                                                        <span class="result_message">&nbsp; <br>
                                                        </span>
                                                        <input type="hidden" name="step" value="add">


                                                        <h3 class="title-hero">Add Campaign Activity</h3>
                                                        <div class="example-box-wrapper">

                                                        <?php
// Create connection And Write Values
                                                        $DB = Connect();
                                                        $sql = "SHOW COLUMNS FROM tblCampaignActivity";
                                                        $RS = $DB->query($sql);
                                                        if ($RS->num_rows > 0)
                                                        {

                                                        while($row = $RS->fetch_assoc())
                                                        {
                                                        if($row["Field"]=="CampaignActivityID")
                                                        {
                                                        }
                                                        else if ($row["Field"]=="CampaignName")
                                                        {
                                                        ?>	
                                                            <div class="form-group"><label class="col-sm-3 control-label"><?= str_replace("CampaignName", "Campaign Name", $row["Field"]) ?> <span>*</span></label>
                                                                <div class="col-sm-4"><input type="text"  name="<?= $row["Field"] ?>" id="CampaignName" class="form-control required" placeholder="<?= str_replace("CampaignName", "CampaignName", $row["Field"]) ?>"></div>
                                                            </div>

                                                            <?php
                                                            }
                                                            else if ($row["Field"]=="ActivityID")
                                                            {
                                                            $sql1 = "SELECT * from tblActivityCostCenter where PerQtyCost!=0";

                                                            $RS2 = $DB->query($sql1);
                                                            if ($RS2->num_rows > 0)
                                                            {
                                                            ?>	

                                                            <div class="form-group"><label class="col-sm-3 control-label">Activity<span>*</span></label>
                                                                <div class="col-sm-4">
                                                                    <select class="form-control"  name="ActivityID" id="ActivityID"  >
                                                                        <?
                                                                        while($row2 = $RS2->fetch_assoc())
                                                                        {
                                                                        $ActivityCostCenterID=$row2['ActivityCostCenterID'];
                                                                        $Name=$row2['Name'];
                                                                        ?>
                                                                        <option value="<?= $ActivityCostCenterID ?>" ><?= $Name ?></option>
<?php
}
?>
                                                                    </select>
                                                                </div>
                                                            </div>


<?php
}
}
else if ($row["Field"]=="StoreID")
{
$sql1 = "SELECT StoreID, StoreName from tblStores where Status=0";

$RS2 = $DB->query($sql1);
if ($RS2->num_rows > 0)
{
?>	

                                                            <div class="form-group"><label class="col-sm-3 control-label">Store<span>*</span></label>
                                                                <div class="col-sm-4">
                                                                    <select class="form-control "  name="StoreID" id="StoreID" onChange="selectoffer(this);" >
                                                                        <option value="" selected>--SELECT NAME--</option>
                                                                        <option value="0">All</option>
                                                                        <?
                                                                        while($row2 = $RS2->fetch_assoc())
                                                                        {
                                                                        $StoreName=$row2['StoreName'];
                                                                        $StoreID=$row2['StoreID'];

                                                                        ?>
                                                                        <option value="<?= $StoreID ?>" ><?= $StoreName ?></option>
<?php
}
?>
                                                                    </select>
                                                                </div>
                                                            </div>


<?php
}
}
else if ($row["Field"]=="OfferID")
{
?>
                                                            <span id="showoffer"></span>

<?php
}
else if ($row["Field"]=="Qty")
{
?>	
                                                            <div class="form-group"><label class="col-sm-3 control-label"><?= str_replace("Qty", "Qty", $row["Field"]) ?> <span>*</span></label>
                                                                <div class="col-sm-4"><input type="text" onkeyup="calcost()" name="<?= $row["Field"] ?>" id="<?= str_replace("Qty", "Qty", $row["Field"]) ?>" class="form-control required" placeholder="<?= str_replace("Qty", "Qty", $row["Field"]) ?>"></div>
                                                            </div>
                                                            <?php
                                                            }
                                                            else if ($row["Field"]=="TotalCost")
                                                            {
                                                            ?>	
                                                            <div class="form-group"><label class="col-sm-3 control-label"><?= str_replace("TotalCost", "TotalCost", $row["Field"]) ?> <span>*</span></label>
                                                                <div class="col-sm-4"><input type="text" readonly name="<?= $row["Field"] ?>" id="TotalCost" class="form-control required" placeholder="<?= str_replace("TotalCost", "TotalCost", $row["Field"]) ?>"></div>
                                                            </div>

<?php
}
else if ($row["Field"]=="StartDate")
{
?>	
                                                            <div class="form-group"><label class="col-sm-3 control-label">Start Date <span>*</span></label>
                                                                <div class="col-sm-3">
                                                                        <!--<span class="add-on input-group-addon"><i class="glyph-icon icon-calendar"></i></span><input type="text" name="AppointmentDate" id="AppointmentDate" class="bootstrap-datepicker form-control required" value="02/16/12" data-date-format="yyyy/dd/mm">-->
                                                                    <div class="input-prepend input-group"><span class="add-on input-group-addon"><i class="glyph-icon icon-calendar"></i></span> <input type="text" name="StartDate" id="StartDate"  class="form-control" data-date-format="YY-MM-DD" value="<?php echo date('Y-m-d'); ?>"></div>
                                                                </div>
                                                            </div>	
                                                            <?php
                                                            }
                                                            else if ($row["Field"]=="EndDate")
                                                            {
                                                            ?>	
                                                            <div class="form-group"><label class="col-sm-3 control-label">End Date <span>*</span></label>
                                                                <div class="col-sm-3">
                                                                        <!--<span class="add-on input-group-addon"><i class="glyph-icon icon-calendar"></i></span><input type="text" name="AppointmentDate" id="AppointmentDate" class="bootstrap-datepicker form-control required" value="02/16/12" data-date-format="yyyy/dd/mm">-->
                                                                    <div class="input-prepend input-group"><span class="add-on input-group-addon"><i class="glyph-icon icon-calendar"></i></span> <input type="text" name="EndDate" id="EndDate"  class="form-control" data-date-format="YY-MM-DD" value="<?php echo date('Y-m-d'); ?>"></div>
                                                                </div>
                                                            </div>	
                                                            <?php
                                                            }
                                                            else if ($row["Field"]=="Status")
                                                            {
                                                            ?>
                                                            <div class="form-group"><label class="col-sm-3 control-label"><?= str_replace("Admin", " ", $row["Field"]) ?> <span>*</span></label>
                                                                <div class="col-sm-3">
                                                                    <select name="<?= $row["Field"] ?>" class="form-control required">
                                                                        <option value="0" Selected>Live</option>
                                                                        <option value="1">Offline</option>	
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <?php
                                                            }
                                                            else
                                                            {
                                                            ?>
                                                            <div class="form-group"><label class="col-sm-3 control-label"><?= str_replace("Admin", " ", $row["Field"]) ?> <span>*</span></label>
                                                                <div class="col-sm-4"><input type="text" name="<?= $row["Field"] ?>" id="<?= str_replace("Admin", " ", $row["Field"]) ?>" class="form-control required" placeholder="<?= str_replace("Admin", " ", $row["Field"]) ?>"></div>
                                                            </div>
<?php
}
}
?>
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label"></label>
                                                                <input type="submit" class="btn ra-100 btn-primary" value="Submit">
                                                                <div class="col-sm-2">
                                                                    <a class="btn ra-100 btn-black-opacity" href="javascript:;" onclick="ClearInfo('enquiry_form');" title="Clear"><span>Clear</span></a>
                                                                </div>
                                                            </div>
                                                            <?php
                                                            }
                                                            $DB->close();
                                                            ?>													
                                                        </div>
                                                    </form>
                                                </div>



                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
<?php
} // End null condition

else
{
?>						

                        <div class="panel">
                            <div class="panel-body">
                                <div class="fa-hover">	
                                    <a class="btn btn-primary btn-lg btn-block" href="<?= $strMyActionPage ?>"><i class="fa fa-backward"></i> &nbsp; Go back to <?= $strPageTitle ?></a>
                                </div>

                                <div class="panel-body">
                                    <form role="form" class="form-horizontal bordered-row enquiry_form" onSubmit="proceed_formsubmit('.enquiry_form', '<?= $strMyActionPage ?>', '.result_message', '', '.admin_email', '.admin_password');
                                                                return false;">

                                        <span class="result_message">&nbsp; <br>
                                        </span>
                                        <br>
                                        <input type="hidden" name="step" value="edit">


                                        <h3 class="title-hero">Edit Campaign Activity</h3>
                                        <div class="example-box-wrapper">

                                        <?php
                                        $strID = DecodeQ(Filter($_GET["uid"]));
                                        $DB = Connect();
                                        $sql = "SELECT * FROM tblCampaignActivity WHERE  CampaignActivityID= '$strID'";
                                        $RS = $DB->query($sql);
                                        if ($RS->num_rows > 0)
                                        {
                                        while($row = $RS->fetch_assoc())
                                        {
                                        foreach($row as $key => $val)
                                        {
                                        if($key=="CampaignActivityID")
                                        {
                                        ?>
                                            <input type="hidden" name="<?= $key ?>" value="<?= Encode($strID) ?>">	

                                            <?php
                                            }
                                            else if ($key=="CampaignName")
                                            {
                                            ?>
                                            <div class="form-group"><label class="col-sm-3 control-label"><?= str_replace("CampaignName", "CampaignName", $key) ?> <span>*</span></label>
                                                <div class="col-sm-4"><input type="text" value="<?= $row[$key] ?>"  name="<?= $key ?>" id="CampaignName" class="form-control required" readonly  placeholder="<?= str_replace("CampaignName", "CampaignName", $key) ?>"></div>
                                            </div>
                                            <?php
                                            }
                                            else if ($key=="ActivityID")
                                            {
                                            $acitivity = $row[$key];
                                            $sql1 = "SELECT * from tblActivityCostCenter where PerQtyCost!=0";

                                            $RS2 = $DB->query($sql1);
                                            if ($RS2->num_rows > 0)
                                            {
                                            ?>	

                                            <div class="form-group"><label class="col-sm-3 control-label">Activity<span>*</span></label>
                                                <div class="col-sm-4">
                                                    <select class="form-control" readonly name="ActivityID" id="ActivityID">
                                                        <?
                                                        while($row2 = $RS2->fetch_assoc())
                                                        {
                                                        $ActivityCostCenterID=$row2['ActivityCostCenterID'];
                                                        $Name=$row2['Name'];
                                                        ?>
                                                        <option value="<?= $ActivityCostCenterID ?>"  <?php if($acitivity==$ActivityCostCenterID) { ?> selected="selected" <?php } ?>><?= $Name ?></option>
                                            <?php
                                            }
                                            ?>
                                                    </select>
                                                </div>
                                            </div>


<?php
}
}
else if ($key=="StoreID")
{
$sty = $row[$key];
$totalst = explode(",", $sty);
$sql1 = "SELECT StoreID, StoreName from tblStores where Status=0";

$RS2 = $DB->query($sql1);
if ($RS2->num_rows > 0)
{
?>	

                                            <div class="form-group"><label class="col-sm-3 control-label">Store<span>*</span></label>
                                                <div class="col-sm-4">
                                                    <select class="form-control " readonly name="StoreID" id="StoreID" onChange="selectoffer(this);" >
                                                        <option value="" selected>--SELECT NAME--</option>
                                                        <option value="0" <?php if($sty=='0') { ?> selected="selected" <?php } ?>>All</option>
                                                        <?
                                                        while($row2 = $RS2->fetch_assoc())
                                                        {
                                                        $StoreName=$row2['StoreName'];
                                                        $StoreID=$row2['StoreID'];


                                                        ?>
                                                        <option value="<?= $StoreID ?>" <?php if($sty==$StoreID) { ?> selected="selected" <?php } ?>><?= $StoreName ?></option>
<?php
}
?>
                                                    </select>
                                                </div>
                                            </div>


<?php
}
}
else if ($key=="OfferID")
{
$off = $row[$key];

$sql1 = "SELECT * from tblOffers where Status=0";

$RS2 = $DB->query($sql1);
if ($RS2->num_rows > 0)
{
?>	
                                            <span id="showoffer">	
                                                <div class="form-group"><label class="col-sm-3 control-label">Offers<span>*</span></label>
                                                    <div class="col-sm-4">
                                                        <select class="form-control " readonly name="OfferID" id="OfferID" >
                                                            <option value="" selected>--SELECT NAME--</option>
                                                            <?
                                                            while($row2 = $RS2->fetch_assoc())
                                                            {
                                                            $OfferName=$row2['OfferName'];
                                                            $OfferID=$row2['OfferID'];
                                                            ?>
                                                            <option value="<?= $OfferID ?>" <?php if($OfferID==$off) { ?> selected="selected" <?php } ?>><?= $OfferName ?></option>
<?php
}
?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </span>


<?php
}
}
else if ($key=="Qty")
{
?>	
                                            <div class="form-group"><label class="col-sm-3 control-label"><?= str_replace("Qty", "Qty", $key) ?> <span>*</span></label>
                                                <div class="col-sm-4"><input readonly type="text" value="<?= $row[$key] ?>" onkeyup="calcost()" name="<?= $key ?>" id="<?= str_replace("Qty", "Qty", $key) ?>" class="form-control required" placeholder="<?= str_replace("Qty", "Qty", $key) ?>"></div>
                                            </div>
<?php
}
else if ($key=="TotalCost")
{
?>	
                                            <div class="form-group"><label class="col-sm-3 control-label"><?= str_replace("TotalCost", "TotalCost", $key) ?> <span>*</span></label>
                                                <div class="col-sm-4"><input readonly type="text" value="<?= $row[$key] ?>" readonly name="<?= $key ?>" id="TotalCost" class="form-control required" placeholder="<?= str_replace("TotalCost", "TotalCost", $key) ?>"></div>
                                            </div>

<?php
}
else if ($key=="StartDate")
{
?>	
                                            <div class="form-group"><label class="col-sm-3 control-label">Start Date <span>*</span></label>
                                                <div class="col-sm-3">
                                                        <!--<span class="add-on input-group-addon"><i class="glyph-icon icon-calendar"></i></span><input type="text" name="AppointmentDate" id="AppointmentDate" class="bootstrap-datepicker form-control required" value="02/16/12" data-date-format="yyyy/dd/mm">-->
                                                    <div class="input-prepend input-group"><span class="add-on input-group-addon"><i class="glyph-icon icon-calendar"></i></span> <input type="text" name="StartDate" id="StartDate" value="<?= $row[$key] ?>"  class="form-control" data-date-format="YY-MM-DD" ></div>
                                                </div>
                                            </div>	
                                            <?php
                                            }
                                            else if ($key=="EndDate")
                                            {
                                            ?>	
                                            <div class="form-group"><label class="col-sm-3 control-label">End Date <span>*</span></label>
                                                <div class="col-sm-3">
                                                        <!--<span class="add-on input-group-addon"><i class="glyph-icon icon-calendar"></i></span><input type="text" name="AppointmentDate" id="AppointmentDate" class="bootstrap-datepicker form-control required" value="02/16/12" data-date-format="yyyy/dd/mm">-->
                                                    <div class="input-prepend input-group"><span class="add-on input-group-addon"><i class="glyph-icon icon-calendar"></i></span> <input type="text" name="EndDate" id="EndDate"  class="form-control" data-date-format="YY-MM-DD" value="<?= $row[$key] ?>"></div>
                                                </div>
                                            </div>	
                                            <?php
                                            }
                                            elseif($key=="Status")
                                            {
                                            ?>
                                            <div class="form-group"><label class="col-sm-3 control-label"><?= str_replace("Admin", " ", $key) ?> <span>*</span></label>
                                                <div class="col-sm-2">
                                                    <select name="<?= $key ?>" class="form-control required">
<?php
if ($row[$key]=="0")
{
?>
                                                        <option value="0" selected>Live</option>
                                                        <option value="1">Offline</option>
                                            <?php
                                            }
                                            elseif ($row[$key]=="1")
                                            {
                                            ?>
                                                        <option value="0">Live</option>
                                                        <option value="1" selected>Offline</option>
                                                        <?php
                                                        }
                                                        else
                                                        {
                                                        ?>
                                                        <option value="" selected>--Choose option--</option>
                                                        <option value="0">Live</option>
                                                        <option value="1">Offline</option>
                                                        <?php
                                                        }
                                                        ?>	
                                                    </select>
                                                </div>
                                            </div>
                                                        <?php
                                                        }
                                                        else
                                                        {
                                                        ?>
                                            <div class="form-group"><label class="col-sm-3 control-label"><?= str_replace("Admin", " ", $row["Field"]) ?> <span>*</span></label>
                                                <div class="col-sm-4"><input type="text" name="<?= $row["Field"] ?>" id="<?= str_replace("Admin", " ", $row["Field"]) ?>" class="form-control required" placeholder="<?= str_replace("Admin", " ", $row["Field"]) ?>"></div>
                                            </div>
                                                        <?php
                                                        }


                                                        }
                                                        }
                                                        ?>
                                            <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                <input type="submit" class="btn ra-100 btn-primary" value="Update">

                                                <div class="col-sm-2"><a class="btn ra-100 btn-black-opacity" href="javascript:;" onclick="ClearInfo('enquiry_form');" title="Clear"><span>Clear</span></a></div>
                                            </div>
                                            <?php
                                            }
                                            $DB->close();
                                            ?>													

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>			
<?php
}
?>	

                    </div>
                </div>
            </div>

<?php require_once 'incFooter.fya'; ?>

        </div>
    </body>

</html>