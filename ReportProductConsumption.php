<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
//$start = microtime(true);
$strPageTitle = "Product Consumption| Nailspa";
$strDisplayTitle = "Product Consumption Nailspa";
$strMenuID = "2";
$strMyTable = "tblStoreStock";
$strMyTableID = "StoreStockID";
$strMyField = "";
$strMyActionPage = "ReportProductConsumption.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights	



if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $strStep = Filter($_POST["step"]);

    if ($strStep == "add") {
        
    }

    if ($strStep == "edit") {
        
    }
}
?>


<?php
if (isset($_GET["toandfrom"])) {
    $strtoandfrom = $_GET["toandfrom"];
    $arraytofrom = explode("-", $strtoandfrom);

    $from = $arraytofrom[0];
    $datetime = new DateTime($from);
    $getfrom = $datetime->format('Y-m-d');


    $to = $arraytofrom[1];
    $datetime = new DateTime($to);
    $getto = $datetime->format('Y-m-d');

    if (!IsNull($from)) {
        $sqlTempfrom = " and Date(tblAppointments.AppointmentDate)>=Date('" . $getfrom . "')";
    }

    if (!IsNull($to)) {
        $sqlTempto = " and Date(tblAppointments.AppointmentDate)<=Date('" . $getto . "')";
    }
}

if (!IsNull($_GET["Store"])) {
    $strStoreID = $_GET["Store"];

    $sqlTempStore = " StoreID='$strStoreID'";
}
?>	


<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>

        <script type="text/javascript" src="assets/widgets/datepicker/datepicker.js"></script>
        <script type="text/javascript">
            /* Datepicker bootstrap */

            $(function () {
                "use strict";
                $('.bootstrap-datepicker').bsdatepicker({
                    format: 'mm-dd-yyyy'
                });
            });

            function printDiv(divName)
            {
                $("#heading").show();
                var divToPrint = document.getElementById("printdata");
                var htmlToPrint = '' +
                        '<style type="text/css">' +
                        'table th, table td {' +
                        'border:1px solid #000;' +
                        'padding;0.5em;' +
                        '}' +
                        '</style>';
                htmlToPrint += divToPrint.outerHTML;
                newWin = window.open("");
                newWin.document.write(htmlToPrint);
                newWin.print();
                newWin.close();
                // var printContents = document.getElementById(divName);
                // var originalContents = document.body.innerHTML;

                // document.body.innerHTML = printContents;

                // window.print();

                // document.body.innerHTML = originalContents; 
            }

        </script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker-demo.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/moment.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");      ?>
            <!----------commented by gandhali 5/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>


                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                        </div>
                        <?php
                        if (!isset($_GET["uid"])) {
                            ?>					
                            <div class="panel">
                                <div class="panel">
                                    <div class="panel-body">


                                        <div class="example-box-wrapper">
                                            <div class="tabs">

                                                <div id="normal-tabs-1">

                                                    <span class="form_result">&nbsp; <br>
                                                    </span>

                                                    <div class="panel-body">


                                                        <form method="get" class="form-horizontal bordered-row" role="form">
                                                            <div class="form-group"><label for="" class="col-sm-4 control-label">Select Date Range</label>
                                                                <div class="col-sm-4">
                                                                    <div class="input-prepend input-group">
                                                                        <span class="add-on input-group-addon">
                                                                            <i class="glyph-icon icon-calendar"></i>
                                                                        </span> 
                                                                        <input type="text" name="toandfrom" id="daterangepicker-example" class="form-control" value="<?= $strtoandfrom ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label">Select Store</label>
                                                                <div class="col-sm-4">
                                                                    <select name="Store" class="form-control">
                                                                        <option value="0" >All</option>	
                                                                        <?php
                                                                        $selp = select("*", "tblStores", "Status='0'");
                                                                        foreach ($selp as $val) {
                                                                            $active_store[$val['StoreID']] = $val['StoreID'];
                                                                            $strStoreName = $val["StoreName"];
                                                                            $strStoreID = $val["StoreID"];
                                                                            $store = $_GET["Store"];
                                                                            if ($store == $strStoreID) {
                                                                                ?>
                                                                                <option  selected value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                                <?php
                                                                            } else {
                                                                                ?>
                                                                                <option value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                                <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                                                &nbsp;&nbsp;&nbsp;
                                                                <a class="btn btn-link" href="ReportProductConsumption.php">Clear All Filter</a>
                                                                <?php
                                                                $datedrom = $_GET["toandfrom"];
                                                                if ($datedrom != "") {
                                                                    ?>
                                                                    <button onclick="printDiv('printarea')" class="btn btn-blue-alt">Print<div class="ripple-wrapper"></div></button>
                                                                    <?php
                                                                }
                                                                ?>

                                                                &nbsp;&nbsp;&nbsp;


                                                                                                                                                                                                                                                                <!--<a class="btn btn-border btn-alt border-primary font-primary" href="ExcelExportData.php?from=<?= $getfrom ?>&to=<?= $getto ?>" title="Excel format 2016"><span>Export To Excel</span><div class="ripple-wrapper"></div></a>
                                                                -->

                                                            </div>
                                                        </form>
                                                        <div id="printdata">	
                                                            <h2 class="title-hero" id="heading" style="display:none"><center>Report Product Consumption</center></h2>
                                                            <br>
                                                            <?php
                                                            $datedrom = $_GET["toandfrom"];
                                                            if ($datedrom != "" && !IsNull($_GET["Store"])) {
                                                                $storrr = $_GET["Store"];
                                                                if ($storrr == '0') {
                                                                    $storrrp = 'All';
                                                                } else {
                                                                    $stpp = select("StoreName", "tblStores", "StoreID='" . $storrr . "'");
                                                                    $StoreName = $stpp[0]['StoreName'];
                                                                    $storrrp = $StoreName;
                                                                }
                                                                ?>
                                                                <h3>Date Range selected : FROM - <?= $getfrom ?> / TO - <?= $getto ?> / Store Filter selected : <?= $storrrp ?> </h3>

                                                                <br>


                                                                <div class="example-box-wrapper">
                                                                    <table class="table table-bordered table-striped table-condensed cf">
                                                                        <thead>
                                                                            <tr>
                                                                                <th style="text-align:center">Sr. No.</th>
                                                                                <th style="text-align:center">Product Name</th>
                                                                                <th style="text-align:center">Product Qty Used<br/>(Consumption Target * Service Done)</th>
                                                                                <th style="text-align:center">Services Done<br/>(Total No Of Service Count)</th>
                                                                                <th style="text-align:center">Consumption Target<br/>(Per Qty Serve)</th>
                                                                                <?php /* <th style="text-align:center">Consumption Performance<br/>(Per Qty Serve minus Total No Of Service Count)</th>
                                                                                  <th style="text-align:center">Revenue Generated<br/>(Total Service Sale Amount)</th> */ ?>

                                                                                <th style="text-align:center">Cost of Product<br/>(Product MRP Multiply Total No Of Service Count)</th>
                                                                                <th style="text-align:center">Available Stock</th>
                                                                                <?php /*  <th style="text-align:center">Profitability<br/>(Cost of Product minus Revenue Generated)</th> */ ?>

                                                                            </tr>
                                                                        </thead>

                                                                        <tbody>

                                                                            <?php
                                                                            $DB = Connect();
                                                                            $storrr = $_GET["Store"];
                                                                            /*
                                                                             * query used to get store ids
                                                                             */
                                                                            if (!empty($storrr)) {
                                                                                //$sql=selectproduct($storrr,$getfrom,$getto);
                                                                                $sql = "select group_concat(distinct tblAppointments.StoreID) as g_store from  tblAppointments join tblInvoiceDetails on tblInvoiceDetails.AppointmentId=tblAppointments.AppointmentID WHERE tblAppointments.StoreID='" . $storrr . "'  AND tblAppointments.IsDeleted !=  '1' AND tblAppointments.FreeService !=  '1' and tblAppointments.Status='2' $sqlTempfrom $sqlTempto ";
                                                                            } else {
                                                                                $sql = "select group_concat(distinct tblAppointments.StoreID) as g_store from  tblAppointments join tblInvoiceDetails on tblInvoiceDetails.AppointmentId=tblAppointments.AppointmentID WHERE tblAppointments.IsDeleted !=  '1' AND tblAppointments.FreeService !=  '1' and tblAppointments.Status='2' $sqlTempfrom $sqlTempto ";
                                                                            }

                                                                            $get_Store = $DB->query($sql);
                                                                            if ($get_Store->num_rows > 0) {
                                                                                while ($row_data = $get_Store->fetch_assoc()) {
                                                                                    $group_store['store_ids'] = $row_data['g_store'];
                                                                                }
                                                                            }



                                                                            $RS = $DB->query($sql);
                                                                            if (isset($group_store['store_ids']) && $group_store['store_ids'] != '') {
                                                                                /*
                                                                                 * get store product
                                                                                 */
                                                                                // $store_pdtq = "SELECT DISTINCT ProductID FROM `tblProductsServices` WHERE `StoreID` IN (1,3,4,5,108)";
                                                                                $store_pdtq = "SELECT DISTINCT ProductID FROM `tblProductsServices`";
                                                                                $store_pdtq_exe = $DB->query($store_pdtq);
                                                                                if ($store_pdtq_exe->num_rows > 0) {
                                                                                    while ($row_data = $store_pdtq_exe->fetch_assoc()) {
                                                                                        $result[] = $row_data;
                                                                                        $product_ids[$row_data['ProductID']] = $row_data['ProductID'];
                                                                                    }
                                                                                }

                                                                                /*
                                                                                 * get product data
                                                                                 */

                                                                                if (isset($product_ids) && is_array($product_ids) && count($product_ids) > 0) {
                                                                                    $product_in_ids = implode(",", $product_ids);

                                                                                    /*
                                                                                     * get product service ids
                                                                                     */
                                                                                    if (!empty($storrr)) {
                                                                                        $store_append = " AND tblProductsServices.StoreID ='" . $storrr . "'";
                                                                                    } else {
                                                                                        $store_append = '';
                                                                                    }
                                                                                    $pdt_services = "SELECT tblProductsServices.ServiceID,tblProductsServices.ProductID FROM `tblProductsServices` WHERE ProductID IN(" . $product_in_ids . ") " . $store_append;
                                                                                    $pdt_services_exe = $DB->query($pdt_services);
                                                                                    while ($pdtservices_data = $pdt_services_exe->fetch_assoc()) {
                                                                                        $all_pdt_services[] = $pdtservices_data;
                                                                                    }

                                                                                    if (isset($all_pdt_services) && is_array($all_pdt_services) && count($all_pdt_services) > 0) {
                                                                                        foreach ($all_pdt_services as $pdtskey => $pdtsvalue) {
                                                                                            $pdt_services_res[$pdtsvalue['ProductID']][] = $pdtsvalue['ServiceID'];
                                                                                            $service_ids_arr[$pdtsvalue['ServiceID']] = $pdtsvalue['ServiceID'];
                                                                                        }
                                                                                    }


                                                                                    $pdt_q = "SELECT * FROM tblNewProducts WHERE ProductID IN(" . $product_in_ids . ")";
                                                                                    $pdt_exe = $DB->query($pdt_q);
                                                                                    while ($pdtdetails = $pdt_exe->fetch_assoc()) {
                                                                                        $all_pdt[] = $pdtdetails;
                                                                                    }

                                                                                    if (isset($all_pdt) && is_array($all_pdt) && count($all_pdt) > 0) {
                                                                                        foreach ($all_pdt as $pdtkey => $pdtvalue) {
                                                                                            $pdt_data[$pdtvalue['ProductID']] = $pdtvalue;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                /*
                                                                                 * Get Product Stock
                                                                                 */
                                                                                $pdt_stock = "SELECT DISTINCT ProductID,ProductStockID,StoreID,Stock FROM tblStoreProduct WHERE ProductID IN(" . $product_in_ids . ")";
                                                                                $pdt_stock_exe = $DB->query($pdt_stock);
                                                                                while ($stock_res = $pdt_stock_exe->fetch_assoc()) {
                                                                                    $stock_detail[$stock_res['ProductID']][$stock_res['StoreID']] = $stock_res['Stock'];
                                                                                }

                                                                                /*
                                                                                 * get service qty and amount
                                                                                 */
                                                                                if (isset($service_ids_arr) && is_array($service_ids_arr) && count($service_ids_arr) > 0) {
                                                                                    $service_in_ids = implode(",", $service_ids_arr);
                                                                                    if (!empty($storrr)) {
                                                                                        $qty_store_append = " AND tblAppointments.StoreID ='" . $storrr . "'";
                                                                                    } else {
                                                                                        $qty_store_append = '';
                                                                                    }
                                                                                    $qty_amtq = "SELECT tblInvoiceDetails.DisAmt,tblInvoiceDetails.OfferAmt,tblAppointmentsDetailsInvoice.ServiceID,tblAppointmentsDetailsInvoice.qty,tblAppointmentsDetailsInvoice.ServiceAmount FROM tblAppointmentsDetailsInvoice"
                                                                                            . " JOIN tblAppointments on tblAppointmentsDetailsInvoice.AppointmentID=tblAppointments.AppointmentID"
                                                                                            . " JOIN tblInvoiceDetails on tblInvoiceDetails.AppointmentId=tblAppointments.AppointmentID"
                                                                                            . " WHERE tblAppointmentsDetailsInvoice.ServiceID!='NULL' AND tblAppointmentsDetailsInvoice.ServiceID!=''"
                                                                                            . " AND tblAppointments.IsDeleted !=  '1' and tblAppointments.Status='2' AND tblAppointments.FreeService !=  '1' "
                                                                                            . " AND tblAppointmentsDetailsInvoice.ServiceID IN (" . $service_in_ids . ")" . $qty_store_append . $sqlTempfrom . $sqlTempto;
                                                                                    $qty_amtq_exe = $DB->query($qty_amtq);

                                                                                    while ($qtyservices_data = $qty_amtq_exe->fetch_assoc()) {
                                                                                        $all_pdt_services_qty_amt[] = $qtyservices_data;
                                                                                    }




                                                                                    if (isset($all_pdt_services_qty_amt) && is_array($all_pdt_services_qty_amt) && count($all_pdt_services_qty_amt) > 0) {
                                                                                        foreach ($all_pdt_services_qty_amt as $qtykey => $qtyvalue) {
                                                                                            $ser_offer_amount = 0;
                                                                                            $OfferAmt = trim($qtyvalue['OfferAmt'], ' - ');
                                                                                            if ($OfferAmt > 0) {
                                                                                                $ser_offer_amount = $OfferAmt;
                                                                                            }

                                                                                            $dis_amt = 0;
                                                                                            $DisAmt = explode(",", $qtyvalue['DisAmt']);
                                                                                            if (isset($DisAmt) && is_array($DisAmt) && count($DisAmt) > 0) {
                                                                                                foreach ($DisAmt as $key => $value) {
                                                                                                    if (trim($value) > 0) {
                                                                                                        $dis_amt += $value;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            $pdtservice_qty_res[$qtyvalue['ServiceID']][] = array(
                                                                                                'qty' => $qtyvalue['qty'],
                                                                                                'ServiceAmount' => $qtyvalue['ServiceAmount'],
                                                                                                'membership_dis' => $dis_amt,
                                                                                                'offer_dis' => $ser_offer_amount
                                                                                            );
                                                                                        }
                                                                                    }
                                                                                }


                                                                                $counter = 1;
                                                                                $total_avail_stock = 0;
                                                                                foreach ($result as $key => $row) {


                                                                                    $ProductID = $row["ProductID"];
                                                                                    //$sep = select("count(*)", "tblNewProducts", "ProductID='" . $ProductID . "'");
                                                                                    //$cntserr = $sep[0]['count(*)'];
                                                                                    $cntserr = isset($pdt_data[$ProductID]) ? $pdt_data[$ProductID] : array();
                                                                                    if (count($cntserr) > 0) {
                                                                                        //$sept = select("*", "tblNewProducts", "ProductID='" . $ProductID . "'");
                                                                                        $ProductIDT = $cntserr['ProductID'];
                                                                                        $productname = $cntserr['ProductName'];
                                                                                        $PerQtyServe = $cntserr['PerQtyServe'];
                                                                                        $ProductMRP = $cntserr['ProductMRP'];

                                                                                        //$sepa = select("*", "tblStores", "StoreID='" . $storrr . "'");
                                                                                        //$storename = $sepa[0]['StoreName'];
                                                                                        // $sqlt=selectproductservice($storrr,$getfrom,$getto,$ProductIDT);
                                                                                        //$sqlt = "select distinct(tblProductsServices.ServiceID) from tblProductsServices left join tblAppointments on tblProductsServices.StoreID=tblAppointments.StoreID right join tblInvoiceDetails on tblInvoiceDetails.AppointmentId=tblAppointments.AppointmentID left join tblNewProducts on tblNewProducts.ProductID=tblProductsServices.ProductID WHERE tblAppointments.StoreID='" . $storrr . "' AND tblProductsServices.StoreID='" . $storrr . "'  AND tblAppointments.IsDeleted !=  '1' AND tblAppointments.FreeService !=  '1' and tblAppointments.Status='2' $sqlTempfrom $sqlTempto  AND tblProductsServices.StoreID='" . $storrr . "' and tblProductsServices.ProductID='" . $ProductIDT . "'";


                                                                                        $product_mul_service = isset($pdt_services_res[$ProductIDT]) ? $pdt_services_res[$ProductIDT] : array();
                                                                                        //$RSt = $DB->query($sqlt);
                                                                                        if (isset($product_mul_service) && is_array($product_mul_service) && count($product_mul_service) > 0) {


                                                                                            foreach ($product_mul_service as $mulserkey => $row1) {


                                                                                                $servicedt = $row1;
                                                                                                // $stppsertyptup=selectproductservicedetail($storrr,$getfrom,$getto,$servicedt);
                                                                                                //$stppsertyptup = select("tblAppointmentsDetailsInvoice.qty,tblAppointmentsDetailsInvoice.ServiceAmount", "tblAppointmentsDetailsInvoice left join tblAppointments on tblAppointmentsDetailsInvoice.AppointmentID=tblAppointments.AppointmentID right join tblInvoiceDetails on tblInvoiceDetails.AppointmentId=tblAppointments.AppointmentID", "tblAppointmentsDetailsInvoice.ServiceID!='NULL' AND tblAppointmentsDetailsInvoice.ServiceID!='' and tblAppointments.StoreID='" . $storrr . "' and tblAppointmentsDetailsInvoice.ServiceID='" . $servicedt . "' AND tblAppointments.IsDeleted !=  '1' and tblAppointments.Status='2' AND tblAppointments.FreeService !=  '1' $sqlTempfrom $sqlTempto ");
                                                                                                //exit;
                                                                                                $stppsertyptup = isset($pdtservice_qty_res[$servicedt]) ? $pdtservice_qty_res[$servicedt] : array();

                                                                                                foreach ($stppsertyptup as $tr) {
                                                                                                    $ser_mem_amount = 0;
                                                                                                    $ser_offer_amount = 0;
                                                                                                    $qty = $tr['qty'];
                                                                                                    $ServiceAmount = $tr['ServiceAmount'];
                                                                                                    if (isset($tr['membership_dis']) && $tr['membership_dis'] > 0) {
                                                                                                        $ser_mem_amount = $tr['membership_dis'];
                                                                                                    }

                                                                                                    if (isset($tr['offer_dis']) && $tr['offer_dis'] > 0) {
                                                                                                        $ser_offer_amount = $tr['offer_dis'];
                                                                                                    }
                                                                                                    $qttyt +=$qty;
                                                                                                    $strServiceAmount = ($ServiceAmount * $qty) - $ser_mem_amount - $ser_offer_amount;
                                                                                                    $totalstrServiceAmount = $totalstrServiceAmount + $strServiceAmount;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        //$productcost = $ProductMRP * $qttyt;
                                                                                        $pcost = $ProductMRP / $PerQtyServe;
                                                                                        $productcost = round($pcost, 2) * $qttyt;

                                                                                        $ProductQtyUsed = $PerQtyServe * $qttyt;
                                                                                        if ($qttyt == "") {
                                                                                            $qttyt = 0;
                                                                                        }
                                                                                        $consumperformance = $PerQtyServe - $qttyt;
                                                                                        $profit = $productcost - $totalstrServiceAmount;


                                                                                        if ($ProductQtyUsed == "") {
                                                                                            $ProductQtyUsed = "0.00";
                                                                                        } else {

                                                                                            $ProductQtyUsed = $ProductQtyUsed;
                                                                                        }
                                                                                        $TotalProductQtyUsed += $ProductQtyUsed;
                                                                                        if ($qttyt == "") {
                                                                                            $qttyt = "0.00";
                                                                                        } else {

                                                                                            $qttyt = $qttyt;
                                                                                        }
                                                                                        $Totalqttyt += $qttyt;
                                                                                        if ($PerQtyServe == "") {
                                                                                            $PerQtyServe = "0.00";
                                                                                        } else {

                                                                                            $PerQtyServe = $PerQtyServe;
                                                                                        }
                                                                                        if (isset($qttyt) && $qttyt > 0) {
                                                                                            $TotalPerQtyServe += $PerQtyServe;
                                                                                        }
                                                                                        if ($consumperformance == "") {
                                                                                            $consumperformance = "0.00";
                                                                                        } else {

                                                                                            $consumperformance = $consumperformance;
                                                                                        }
                                                                                        $Totalconsumperformance += $consumperformance;
                                                                                        if ($totalstrServiceAmount == "") {
                                                                                            $totalstrServiceAmount = "0.00";
                                                                                        } else {

                                                                                            $totalstrServiceAmount = $totalstrServiceAmount;
                                                                                        }
                                                                                        $TotaltotalstrServiceAmount += $totalstrServiceAmount;
                                                                                        if ($productcost == "") {
                                                                                            $productcost = "0.00";
                                                                                        } else {

                                                                                            $productcost = $productcost;
                                                                                        }
                                                                                        $Totalproductcost += $productcost;
                                                                                        if ($profit == "") {
                                                                                            $profit = "0.00";
                                                                                        } else {

                                                                                            $profit = $profit;
                                                                                        }
                                                                                        $Totalprofit += $profit;
                                                                                        ?>														

                                                                                        <?php if (isset($qttyt) && $qttyt > 0) { ?>
                                                                                            <tr id="my_data_tr_<?= $counter ?>">
                                                                                                <td><?= $counter; ?></td>
                                                                                                <td><center><?= $productname ?></center></td>
                                                                                        <td><center><?= ceil($ProductQtyUsed) ?></center></td>
                                                                                        <td><center><?= $qttyt ?></center></td>

                                                                                        <td><center><?php echo isset($qttyt) && $qttyt > 0 ? $PerQtyServe : 0; ?></center></td>
                                                                                        <?php /*  <td><center><?= $consumperformance ?></center></td>
                                                                                          <td><center><?= $totalstrServiceAmount ?></center></td> */ ?>
                                                                                        <td><center><?= $productcost ?></center></td>
                                                                                        <?php /* <td><center><?= $profit ?></center></td> */ ?>
                                                                                        <td><center>
                                                                                            <?php
                                                                                            $avail_stock = 0;
                                                                                            if (empty($_GET['Store'])) {
                                                                                                if (isset($active_store) && is_array($active_store) && count($active_store) > 0) {
                                                                                                    foreach ($active_store as $pskey => $psvalue) {
                                                                                                        if (isset($stock_detail[$ProductIDT][$psvalue])) {
                                                                                                            $avail_stock += $stock_detail[$ProductIDT][$psvalue];
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            } else {
                                                                                                $avail_stock = isset($stock_detail[$ProductIDT][$_GET['Store']]) ? $stock_detail[$ProductIDT][$_GET['Store']] : 0;
                                                                                            }
                                                                                            echo $avail_stock;
                                                                                            $total_avail_stock += $avail_stock;
                                                                                            ?>
                                                                                        </center></td>
                                                                                        </tr>
                                                                                        <?php
                                                                                        $counter++;
                                                                                    }
                                                                                    ?>
                                                                                    <?php
                                                                                }
                                                                                $qttyt = "";
                                                                                $PerQtyServe = "";
                                                                                $ProductQtyUsed = "";
                                                                                $consumperformance = "";
                                                                                $totalstrServiceAmount = "";
                                                                                $profit = "";
                                                                                $productcost = "";
                                                                            }
                                                                            $stocksm = "";
                                                                        } else {
                                                                            ?>															
                                                                            <tr>
                                                                                <td></td>
                                                                                <td></td>
                                                                                <td></td>
                                                                                <td>No Records Found</td>
                                                                                <?php /* <td></td>
                                                                                  <td></td> */ ?>
                                                                                <td></td>
                                                                                <?php /* <td></td> */ ?>

                                                                            </tr>


                                                                            <?php
                                                                        }


                                                                        $DB->close();
                                                                        // $time_elapsed_secs = microtime(true) - $start;
                                                                        //echo 'Time elapsed=' . $time_elapsed_secs / 1000;
                                                                        ?>

                                                                        </tbody>

                                                                        <tbody>
                                                                            <tr>
                                                                                <td colspan="2"><center><b>Total</b></center></td>
                                                                        <td class="numeric"><center><?= round($TotalProductQtyUsed, 2) ?></center></td>
                                                                        <td class="numeric"><center><?= $Totalqttyt ?></center></td>
                                                                        <td class="numeric"><center><?= $TotalPerQtyServe ?></center></td>
                                                                        <?php /*  <td class="numeric"><center><?= $Totalconsumperformance ?></center></td>
                                                                          <td class="numeric"><center><?= $TotaltotalstrServiceAmount ?></center></td> */ ?>
                                                                        <td class="numeric"><center><?= $Totalproductcost ?></center></td>
                                                                        <td class="numeric"><center><?= $total_avail_stock ?></center></td>
                                                                            <?php /*  <td class="numeric"><center><?= $Totalprofit ?></center></td> */ ?>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                } else {
                                                    echo "<br><center><h3>Please Select Month And Year!</h3></center>";
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        } // End null condition
                        else {
                            
                        }
                        ?>
                    </div>
                </div>

                <?php require_once 'incFooter.fya'; ?>

            </div>
    </body>

</html>