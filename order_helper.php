<?php

function get_detail_data($id = '', $is_pdf = '') {
    $response = '';
    $pdt_name_data = array();
    if (isset($id) && is_numeric($id)) {
        $order_data = select("*", "order_request", "id='" . $id . "' AND status=1");

        $store_id = isset($order_data[0]['store_id']) ? $order_data[0]['store_id'] : 0;
        $store_data = select("*", "tblStores", "StoreID='" . $store_id . "'");

        $created_by = isset($order_data[0]['created_by']) ? $order_data[0]['created_by'] : 0;
        $created_data = select("*", "tblAdmin", "AdminID='" . $created_by . "'");

        $order_product = select("*", "order_request_product", "order_request_id='" . $id . "' AND status=1");

        if (isset($order_product) && is_array($order_product) && count($order_product) > 0) {
            foreach ($order_product as $pkey => $pvalue) {
                $pdt_id_arr[$pvalue['product_id']] = $pvalue['product_id'];
            }
        }

        $DB = Connect();
        if (isset($pdt_id_arr) && is_array($pdt_id_arr) && count($pdt_id_arr) > 0) {
            $pdt_in_ids = implode(",", $pdt_id_arr);
            $pdt_nameq = "SELECT * FROM tblNewProducts WHERE ProductID IN(" . $pdt_in_ids . ")";
            $pdt_nameq_exe = $DB->query($pdt_nameq);
            if ($pdt_nameq_exe->num_rows > 0) {
                while ($pdt_row = $pdt_nameq_exe->fetch_assoc()) {
                    $pdt_name_data[$pdt_row["ProductID"]]['name'] = $pdt_row['ProductName'];
                    $pdt_name_data[$pdt_row["ProductID"]]['code'] = $pdt_row['ProductUniqueCode'];
                }
            }
        }
    }



    if ($is_pdf == 2) {
        $result_data['store_data'] = $store_data;
        $result_data['order_data'] = $order_data;
        $result_data['created_data'] = $created_data;
        $result_data['order_product'] = $order_product;
        $result_data['pdt_name_data'] = $pdt_name_data;
        return $result_data;
    }
    $counter = 1;
    if ($is_pdf == 1) {
        $response .= '<html lang="en"><head><link rel="stylesheet" type="text/css" href="assets/helpers/custom-style.css?12112018">';
        $response .= '<meta name="viewport" content="width=device-width, initial-scale=1">
                  </head>
<body style="background:#FFFFFF;">';
    }
    $response .= '<div class="panel" style="min-height: 100%;">
                            <div class="panel-body">

                                <h3 class="title-hero">Order Details</h3>
                                <div class="form-group">
                                    
                                    <p><strong>Name : </strong>' . (isset($order_data[0]['order_name']) ? $order_data[0]['order_name'] : '') . '</p>
                                    <p><strong>Store : </strong>' . (isset($store_data[0]['StoreName']) ? $store_data[0]['StoreName'] : '') . '</p>
                                        <p><strong>Store Address : </strong>' . (isset($store_data[0]['StoreOfficialAddress']) ? $store_data[0]['StoreOfficialAddress'] : '') . '</p>
                                    <p><strong>Created By : </strong>' . (isset($created_data[0]['AdminFullName']) ? $created_data[0]['AdminFullName'] : '') . '</p>
                                    <p><strong>Created Date : </strong>' . (isset($order_data[0]['created_date']) && $order_data[0]['created_date'] != '0000-00-00 00:00:00' ? date('d M, Y h:i a', strtotime($order_data[0]['created_date'])) : '') . '</p>

                                </div>
                                <div class="row">
                                    <table class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sr. No.</th>
                                                <th>Product</th>
                                                <th>Code</th>
                                                <th>Quantity</th>
                                            </tr>
                                        </thead>
                                        <tbody class="product_body">';
    if (isset($order_product) && is_array($order_product) && count($order_product) > 0) {
        foreach ($order_product as $okey => $ovalue) {
            if (isset($pdt_name_data[$ovalue['product_id']])) {
                $response .= '<tr id="product_div_' . $okey . '">
                                                            <td>' . $counter . '</td>
                                                            <td>' . $pdt_name_data[$ovalue['product_id']]['name'] . '</td>
                                                                <td>' . $pdt_name_data[$ovalue['product_id']]['code'] . '</td>
                                                            <td>' . $ovalue['quantity'] . '</td>
                                                        </tr>';
                $counter++;
            }
        }
    } else {
        $response .= '<tr id="product_div_0">
            <td colspan="3">No Product Found...</td>
        </tr>';
    }
    $response .= '</tbody>
</table>
</div>
</div>
</div>';

    if ($is_pdf == 1) {
        $response .= '</body></html>';
    }

    return $response;
}

?>