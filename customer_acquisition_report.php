<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Manage Customers | Nailspa";
$strDisplayTitle = "Customer Acquisition Report of Nailspa Experience";
$strMenuID = "10";
$strMyTable = "tblCustomers";
$strMyTableID = "CustomerID";
$strMyField = "CustomerMobileNo";
$strMyActionPage = "customer_acquisition_report.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
?>
<!DOCTYPE html>
<html lang="en">
    <?php require_once("incMetaScript.fya"); ?>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");   ?>
            <!----------commented by gandhali 3/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>
                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                        </div>
                        <script type="text/javascript">
                            /* Datatables responsive */
                            $(document).ready(function () {
                                $('#datatable-responsive-count').DataTable({
                                    "order": [[3, "desc"]]
                                });
                            });

                        </script>

                        <div class="panel">
                            <div class="panel-body" style="overflow-x: scroll;">
                                <h3 class="title-hero">Customer Acquisition</h3>
                                <form method="get" class="form-horizontal bordered-row" role="form">

                                    <?php
                                    if (isset($_GET["toandfrom"])) {
                                        $strtoandfrom = $_GET["toandfrom"];
                                    }
                                    ?>
                                    <div class="form-group"><label for="" class="col-sm-2 control-label">Select Date Range</label>
                                        <div class="col-sm-4">
                                            <div class="input-prepend input-group">
                                                <span class="add-on input-group-addon">
                                                    <i class="glyph-icon icon-calendar"></i>
                                                </span> 
                                                <input type="text" name="toandfrom" id="daterangepicker-example" class="form-control" value="<?= $strtoandfrom ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                    $sql1 = "SELECT StoreID, StoreName FROM tblStores WHERE Status = 0";
                                    $RS2 = $DB->query($sql1);
                                    if ($RS2->num_rows > 0) {
                                        ?>
                                        <div class="form-group"><label class="col-sm-2 control-label">Store</label>
                                            <div class="col-sm-4">
                                                <select class="form-control required"  id="StoreID" name="StoreID" >
                                                    <option value="" selected>All</option>
                                                    <?php
                                                    while ($row2 = $RS2->fetch_assoc()) {
                                                        $StoreID = $row2["StoreID"];
                                                        $StoreName = $row2["StoreName"];
                                                        ?>	
                                                        <option value="<?= $StoreID ?>" <?php echo isset($_GET['StoreID']) && $_GET['StoreID'] == $StoreID ? 'selected' : '' ?>><?= $StoreName ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>	

                                        <div class="form-group"><label class="col-sm-3 control-label"></label>
                                            <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                            &nbsp;&nbsp;&nbsp;
                                            <a class="btn btn-link" href="customer_acquisition_report.php">Clear All Filter</a> &nbsp;&nbsp;&nbsp;
                                        </div>
                                        <?php
                                    }
                                    ?>


                                </form>

                                <?php
                                if (isset($_GET["toandfrom"]) && $_GET["toandfrom"] != '') {
                                    if (isset($_GET['StoreID']) && !empty($_GET['StoreID'])) {
                                        $store_data = select("*", "tblStores", "StoreID='" . $_GET['StoreID'] . "'");
                                        $store_name = $store_data[0]['StoreName'];
                                    } else {
                                        $store_name = "All";
                                    }
                                    ?>
                                    <h3>Store : <?php echo $store_name; ?></h3>
                                    <table id = "datatable-responsive-count" class = "table table-striped table-bordered responsive no-wrap" cellspacing = "0" width = "100%">
                                        <thead>
                                            <tr>
                                                <th>Acquisition</th>
                                                <th>Existing Customer</th>
                                                <th>New Customer</th>
                                                <th>Total</th>
                                                <th>Revenue Generated</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Acquisition</th>
                                                <th>Existing Customer</th>
                                                <th>New Customer</th>
                                                <th>Total</th>
                                                <th>Revenue Generated</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <?php
                                            $acquisition_data = array(
                                                'Leaflet' => 'Leaflet',
                                                'WordOfMouth' => 'Word Of Mouth',
                                                'Reference' => 'Reference',
                                                'Facebook' => 'Facebook',
                                                'Instagram' => 'Instagram',
                                                'Website' => 'Website',
                                                'PR' => 'PR',
                                                'Voucher' => 'Voucher',
'Walk-In' => 'Walk-In',
'Google' => 'Google'
                                            );
                                            /*
                                             * Get Customer Count 
                                             */
                                            $old_total = 0;
                                            $new_total = 0;
                                            $customerq = "SELECT CustomerID,Acquisition FROM `tblCustomers`";
                                            $customerq_exe = $DB->query($customerq);
                                            if ($customerq_exe->num_rows > 0) {
                                                while ($cust_data = $customerq_exe->fetch_assoc()) {
                                                    //$customer_ids[$cust_data['CustomerID']] = $cust_data['CustomerID'];
                                                    if (array_key_exists($cust_data['Acquisition'], $acquisition_data)) {
                                                        $customer_loc[$cust_data['CustomerID']] = $cust_data['Acquisition'];
                                                    } else {
                                                        $customer_loc[$cust_data['CustomerID']] = "Empty";
                                                    }
                                                }
                                            }


                                            if (isset($_GET['StoreID']) && !empty($_GET['StoreID'])) {
                                                $append .= " AND tblAppointments.StoreID='" . $_GET['StoreID'] . "'";
                                            }

                                            if (isset($_GET["toandfrom"]) && $_GET["toandfrom"] != '') {
                                                $strtoandfrom = $_GET["toandfrom"];
                                                $arraytofrom = explode("-", $strtoandfrom);

                                                $from = $arraytofrom[0];
                                                $datetime = new DateTime($from);
                                                $getfrom = $datetime->format('Y-m-d');

                                                $to = $arraytofrom[1];
                                                $datetime = new DateTime($to);
                                                $getto = $datetime->format('Y-m-d');
                                                $append .= " AND Date(tblAppointments.AppointmentDate)>='" . $getfrom . "'";
                                                $append .= " AND Date(tblAppointments.AppointmentDate)<='" . $getto . "'";
                                            }
                                            $cust_countQ = "SELECT AppointmentId,CustomerID FROM tblAppointments "
                                                    . " WHERE AppointmentCheckInTime!='00:00:00' AND status=2 " . $append;
                                            $cust_count_exe = $DB->query($cust_countQ);
                                            //echo "<br><br>Query=" . $apt_countQ;
                                            //echo "<br>Count=" . $apt_count_exe->num_rows;

                                            while ($cust_res = $cust_count_exe->fetch_assoc()) {
                                                $all_apt[$cust_res['AppointmentId']] = $cust_res['AppointmentId'];
                                                $customer_apt_id[$cust_res['CustomerID']][$cust_res['AppointmentId']] = $cust_res['AppointmentId'];
                                                $customer_ids[$cust_res['CustomerID']] = $cust_res['CustomerID'];
                                            }


                                            /*
                                             * Get Appointment Amount i.e. Revenue Generated by customer
                                             */
                                            $cust_apt_count = isset($customer_apt_id) ? count($customer_apt_id) : 0;
                                            $apt_limit = 1000;
                                            if ($cust_apt_count > 0) {
                                                $total_iteration = $cust_apt_count / $apt_limit;
                                                $iter = ceil($total_iteration);
                                                for ($i = 0; $i < $iter; $i++) {
                                                    $offset = $i * $apt_limit;
                                                    //echo "<br>Offset=" . $offset;
                                                    $data = array_slice($all_apt, $offset, $limit);
                                                    if (isset($data) && is_array($data) && count($data) > 0) {
                                                        $cust_apt_in_ids = implode(",", $data);
                                                        if ($cust_apt_in_ids != '') {
                                                            $append = '';

                                                            $sqlservice = "SELECT inv.AppointmentId,inv.CustomerID,inv.ServiceName,inv.Qty,inv.ServiceAmt,inv.DisAmt,inv.OfferAmt,inv.SubTotal,"
                                                                    . " inv.RoundTotal FROM `tblInvoiceDetails` inv "
                                                                    . " WHERE AppointmentID IN(" . $cust_apt_in_ids . ")";
                                                            $RSservice = $DB->query($sqlservice);

                                                            if ($RSservice->num_rows > 0) {
                                                                while ($rowservice = $RSservice->fetch_assoc()) {
                                                                    $offer_amount = 0;
                                                                    $amount_exculde_dis = 0;
                                                                    if (!empty($rowservice['ServiceName'])) {
                                                                        $ServiceName = explode(',', $rowservice['ServiceName']);
                                                                        $Qty = explode(',', $rowservice['Qty']);
                                                                        $ServiceAmt = explode(',', $rowservice['ServiceAmt']);
                                                                        $DisAmt = explode(',', $rowservice['DisAmt']);
                                                                        $count = count($ServiceName);
                                                                        for ($i = 0; $i < $count; $i++) {
                                                                            $offer_amount = str_replace(array('-', ' '), '', $rowservice['OfferAmt']);
                                                                            if (isset($DisAmt[$i])) {
                                                                                $amount_exculde_dis += ($ServiceAmt[$i] - $DisAmt[$i]);
                                                                            } else {
                                                                                $amount_exculde_dis += $ServiceAmt[$i];
                                                                            }
                                                                        }
                                                                        if ($offer_amount > 0) {
                                                                            $amount_exculde_dis = $amount_exculde_dis - $offer_amount;
                                                                        }
                                                                        if ($amount_exculde_dis < 0) {
                                                                            $amount_exculde_dis = 0;
                                                                        }
                                                                        if (isset($appointment_service_amt[$rowservice['CustomerID']])) {
                                                                            $appointment_service_amt[$rowservice['CustomerID']] += $amount_exculde_dis;
                                                                        } else {
                                                                            $appointment_service_amt[$rowservice['CustomerID']] = $amount_exculde_dis;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            /*
                                             * Get Customer Appointment Count to check new or exisitng client
                                             */

                                            $cust_count = isset($customer_ids) ? count($customer_ids) : 0;
                                            $limit = 1000;
                                            $total_revenue = 0;
                                            if ($cust_count > 0) {
                                                $total_iteration = $cust_count / $limit;
                                                $iter = ceil($total_iteration);
                                                for ($i = 0; $i < $iter; $i++) {
                                                    $offset = $i * $limit;
                                                    //echo "<br>Offset=" . $offset;
                                                    $data = array_slice($customer_ids, $offset, $limit);
                                                    if (isset($data) && is_array($data) && count($data) > 0) {
                                                        $cust_in_ids = implode(",", $data);
                                                        if ($cust_in_ids != '') {
                                                            $append = '';

                                                            $apt_countQ = "SELECT COUNT(AppointmentId) as apt_count,CustomerID FROM tblAppointments "
                                                                    . " WHERE AppointmentCheckInTime!='00:00:00' AND status=2 AND CustomerID IN(" . $cust_in_ids . ")"
                                                                    . " GROUP BY CustomerID";
                                                            $apt_count_exe = $DB->query($apt_countQ);
                                                            //echo "<br><br>Query=" . $apt_countQ;
                                                            //echo "<br>Count=" . $apt_count_exe->num_rows;

                                                            while ($count_data = $apt_count_exe->fetch_assoc()) {

                                                                $customer_apt_count[$count_data['CustomerID']] = $count_data['apt_count'];


                                                                /*
                                                                 * Get Customer Location
                                                                 */
                                                                $apt_cust_location = isset($customer_loc[$count_data['CustomerID']]) ? $customer_loc[$count_data['CustomerID']] : 'Empty';
                                                                if ($count_data['apt_count'] > 1) {
                                                                    /*
                                                                     * Old Customer
                                                                     */
                                                                    if (isset($final_res[$apt_cust_location]['old'])) {
                                                                        $final_res[$apt_cust_location]['old'] +=1;
                                                                    } else {
                                                                        $final_res[$apt_cust_location]['old'] = 1;
                                                                    }
                                                                    $old_total += 1;
                                                                    $old_cut_ids[$count_data['CustomerID']] = $count_data['CustomerID'];
                                                                } else {
                                                                    /*
                                                                     * New Customer
                                                                     */
                                                                    if (isset($final_res[$apt_cust_location]['new'])) {
                                                                        $final_res[$apt_cust_location]['new'] +=1;
                                                                    } else {
                                                                        $final_res[$apt_cust_location]['new'] = 1;
                                                                    }
                                                                    $new_total += 1;
                                                                    $new_cut_ids[$count_data['CustomerID']] = $count_data['CustomerID'];
                                                                }

                                                                $revenue_amount = isset($appointment_service_amt[$count_data['CustomerID']]) ? $appointment_service_amt[$count_data['CustomerID']] : 0;
                                                                if (isset($revenue_res[$apt_cust_location])) {
                                                                    $revenue_res[$apt_cust_location] += $revenue_amount;
                                                                } else {
                                                                    $revenue_res[$apt_cust_location] = $revenue_amount;
                                                                }

                                                                $total_revenue += $revenue_amount;
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            if (isset($acquisition_data) && is_array($acquisition_data) && count($acquisition_data) > 0) {
                                                ?>
                                                <tr>
                                                    <td>All</td>
                                                    <td><?php echo $old_total; ?></td>
                                                    <td><?php echo $new_total; ?></td>
                                                    <td><?php echo $old_total + $new_total; ?></td>
                                                    <td><?php echo round($total_revenue, 2); ?></td>
                                                </tr>
                                                <?php
                                                foreach ($acquisition_data as $lkey => $lvalue) {
                                                    $old_client = isset($final_res[$lkey]['old']) ? $final_res[$lkey]['old'] : 0;
                                                    $new_client = isset($final_res[$lkey]['new']) ? $final_res[$lkey]['new'] : 0;
                                                    $total_client = $old_client + $new_client;
                                                    ?>
                                                    <tr>
                                                        <td><?php echo ucwords($lvalue); ?></td>
                                                        <td><?php echo $old_client; ?></td>
                                                        <td><?php echo $new_client; ?></td>
                                                        <td><?php echo $total_client; ?></td>
                                                        <td><?php echo isset($revenue_res[$lkey]) ? round($revenue_res[$lkey], 2) : 0; ?></td>
                                                    </tr>
                                                    <?php
                                                }
                                                /*
                                                 * Show Customer Count whose location is empty
                                                 */
                                                if (isset($final_res['Empty']) && is_array($final_res['Empty']) && count($final_res['Empty']) > 0) {
                                                    $old_client = isset($final_res['Empty']['old']) ? $final_res['Empty']['old'] : 0;
                                                    $new_client = isset($final_res['Empty']['new']) ? $final_res['Empty']['new'] : 0;
                                                    $total_client = $old_client + $new_client;
                                                    ?>
                                                    <tr>
                                                        <td><?php echo "Empty"; ?></td>
                                                        <td><?php echo $old_client; ?></td>
                                                        <td><?php echo $new_client; ?></td>
                                                        <td><?php echo $total_client; ?></td>
                                                        <td><?php echo isset($revenue_res['Empty']) ? round($revenue_res['Empty'], 2) : 0; ?></td>
                                                    </tr>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <tr>
                                                    <td></td>
                                                    <td colspan="3">No Acquisition Found...</td>
                                                    <td></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                    <?php
                                } else {
                                    echo "<br><center><h3>Please Select Month And Year!</h3></center>";
                                }
                                ?>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
            <?php require_once 'incFooter.fya'; ?>
        </div>
    </body>
</html>