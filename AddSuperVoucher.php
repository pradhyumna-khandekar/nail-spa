<?php require_once 'setting.fya'; ?>
<?php require_once 'incFirewall.fya'; ?>
<?php

$DB = Connect();
if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
    $app_id = $_POST['app_id'];
    $gv_id = $_POST['id'];
    $apply_status = isset($_POST['apply_status']) ? $_POST['apply_status'] : 1;
    $cust_gv = isset($_POST['cust_gv']) ? $_POST['cust_gv'] : 0;

    $appointment_data = select("*", "tblAppointments", "AppointmentID='" . $app_id . "'");
    $appointment_service_data = select("*", "tblAppointmentsDetailsInvoice", "AppointmentID='" . $app_id . "'");
    $gv_data = select("*", "customer_super_gv", "id='" . $_POST['apply_super_gv'] . "'");
    $gv_result = select("*", "super_gift_voucher", "id='" . $gv_data[0]['super_gv_id'] . "'");


    if ($cust_gv == 1) {
        /*
         * Apply GV to Appointment
         */

        $gv_id = $gv_result[0]['id'];
        $gv_percentage = $gv_result[0]['percentage'];
        $total_used = 0;
        if ($apply_status == 1) {
            $updatecutst_super = "UPDATE customer_super_gv SET used_amount = '0'"
                    . " WHERE id ='" . $_POST['apply_super_gv'] . "'";
            $DB->query($updatecutst_super);

            if (isset($appointment_service_data) && is_array($appointment_service_data) && count($appointment_service_data) > 0) {
                foreach ($appointment_service_data as $ser_k => $ser_v) {
                    $gv_data = select("*", "customer_super_gv", "id='" . $_POST['apply_super_gv'] . "'");

                    $serv_amount = ($ser_v['ServiceAmount'] * $ser_v['qty']) - $ser_v['offer_discount'] - $ser_v['member_discount'];
                    if ($serv_amount < 0) {
                        $serv_amount = 0;
                    }

                    $balance_gv_amount = $gv_data[0]['gv_amount'] - $gv_data[0]['used_amount'] - $gv_data[0]['pre_used_amount'];
                    if ($serv_amount <= $balance_gv_amount) {
                        $ser_gv_used = $serv_amount;
                    } else {
                        $ser_gv_used = $balance_gv_amount;
                    }


                    $record_exist = select("*", "appointment_redempt_super_gv", "AppointmentID='$app_id' AND ServiceID='" . $ser_v['ServiceID'] . "' AND CustomerID='" . $appointment_data[0]['CustomerID'] . "' AND super_gv_id='" . $gv_data[0]['super_gv_id'] . "'");

                    if (isset($record_exist) && is_array($record_exist) && count($record_exist) > 0) {
                        $update_red_qry = "UPDATE appointment_redempt_super_gv SET amount_used='" . $ser_gv_used . "' WHERE AppointmentID='$app_id' AND ServiceID='" . $ser_v['ServiceID'] . "' AND CustomerID='" . $appointment_data[0]['CustomerID'] . "' AND super_gv_id='" . $gv_data[0]['super_gv_id'] . "'";
                        $DB->query($update_red_qry);
                    } else {
                        $appo_red_vou = "INSERT INTO appointment_redempt_super_gv(AppointmentID,CustomerID,ServiceID,super_gv_id,amount_used,created_date,created_by)"
                                . " VALUES('" . $app_id . "','" . $appointment_data[0]['CustomerID'] . "','" . $ser_v['ServiceID'] . "','" . $gv_data[0]['super_gv_id'] . "',"
                                . "'" . $ser_gv_used . "','" . date('Y-m-d H:i:s') . "','" . $strAdminID . "')";
                        $DB->query($appo_red_vou);
                    }
                    $total_used += $ser_gv_used;

                    if ($ser_gv_used > 0) {
                        $percentage_amount = ($serv_amount * $gv_percentage) / 100;
                        $comission_amount = $serv_amount - $percentage_amount;
                        $updateS = "UPDATE tblAppointmentsDetailsInvoice SET service_amount_deduct_dis='" . $comission_amount . "'
 ,super_gv_discount='" . $percentage_amount . "' WHERE AppointmentDetailsID='" . $ser_v['AppointmentDetailsID'] . "'";
                        $DB->query($updateS);

                        $apppoint_serv_used = $ser_gv_used + $gv_data[0]['used_amount'];
                        $updatecutst_super = "UPDATE customer_super_gv SET used_amount='" . $apppoint_serv_used . "'"
                                . " WHERE id ='" . $_POST['apply_super_gv'] . "'";
                        $DB->query($updatecutst_super);
                    }
                }
            }

            $cust_gv_status = 2;
            $gv_data = select("*", "customer_super_gv", "id='" . $_POST['apply_super_gv'] . "'");
            $balance_gv_amount = $gv_data[0]['gv_amount'] - $gv_data[0]['used_amount'] - $gv_data[0]['pre_used_amount'];
            if ($balance_gv_amount > 0) {
                $cust_gv_status = 1;
            }
            $updatecutst_super = "UPDATE customer_super_gv SET gv_status='" . $cust_gv_status . "'"
                    . " WHERE id ='" . $_POST['apply_super_gv'] . "'";
            $DB->query($updatecutst_super);


            $updateq = "UPDATE tblAppointments SET super_gv_id = '" . $gv_id . "', super_gv_status=2,super_gv_amount='" . $total_used . "'"
                    . " WHERE AppointmentID ='" . $app_id . "'";
            $DB->query($updateq);
        } else {

            if (isset($appointment_service_data) && is_array($appointment_service_data) && count($appointment_service_data) > 0) {
                foreach ($appointment_service_data as $ser_k => $ser_v) {
                    $serv_amount = $ser_v['ServiceAmount'] - $ser_v['offer_discount'] - $ser_v['member_discount'];
                    if ($serv_amount < 0) {
                        $serv_amount = 0;
                    }
                    $comission_amount = $serv_amount;
                    $updateS = "UPDATE tblAppointmentsDetailsInvoice SET service_amount_deduct_dis='" . $comission_amount . "'
 ,super_gv_discount=0 WHERE AppointmentDetailsID='" . $ser_v['AppointmentDetailsID'] . "'";
                    $DB->query($updateS);
                }
            }

            $apt_gv_amount = $appointment_data[0]['super_gv_amount'];
            $updateq = "UPDATE tblAppointments SET super_gv_id = '0', super_gv_status=0,super_gv_amount=0"
                    . " WHERE AppointmentID ='" . $app_id . "'";
            $DB->query($updateq);

            $appo_red_vou = "DELETE FROM appointment_redempt_super_gv WHERE AppointmentID='" . $app_id . "'";
            $DB->query($appo_red_vou);

            $gv_data = select("*", "customer_super_gv", "id='" . $_POST['apply_super_gv'] . "'");

            $cust_gv_status = 1;
            $balance_gv_amount = $gv_data[0]['gv_amount'] - $gv_data[0]['pre_used_amount'];
            if ($balance_gv_amount <= 0) {
                $cust_gv_status = 2;
            }
            $updatecutst_super = "UPDATE customer_super_gv SET used_amount = '0', gv_status='" . $cust_gv_status . "'"
                    . " WHERE id ='" . $_POST['apply_super_gv'] . "'";
            $DB->query($updatecutst_super);
        }
    } else {
        /*
         * Add Super Gift Voucher to Appointment i.e. Purchase GV
         */
        if ($apply_status == 1) {
            $updateq = "UPDATE tblAppointments SET super_gv_id = '" . $gv_id . "', super_gv_status=1"
                    . " WHERE AppointmentID ='" . $app_id . "'";
            $DB->query($updateq);
        } else {
            $updateq = "UPDATE tblAppointments SET super_gv_id = '0', super_gv_status=1"
                    . " WHERE AppointmentID ='" . $app_id . "'";
            $DB->query($updateq);
        }
    }
}
?>