<?php

function get_date($str, $day) {
    $result = array();
    $last_day = date($str . '-t');

    $last_day_arr = explode("-", $last_day);
    if (isset($last_day_arr[2]) && $last_day_arr[2] != '') {
        $max_day = $last_day_arr[2];
        for ($i2 = 1; $i2 <= $max_day; $i2++) {

            $ddd = $str . '-' . $i2;
            $date_res[] = date('Y-m-d', strtotime($ddd));
        }
    }

    if (isset($date_res) && is_array($date_res) && count($date_res) > 0) {
        foreach ($date_res as $dkey => $dvalue) {
            $day_num = date('N', strtotime($dvalue));
            if ($day_num == $day) {
                $result[] = $dvalue;
            }
        }
    }

    return $result;
}

function get_month_year($start_date, $end_date) {
    $all_month_year = array();
    if ($start_date != '' && $end_date != '') {
        $start = (new DateTime($start_date))->modify('first day of this month');
        $end = (new DateTime($end_date))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);

        foreach ($period as $dt) {
            $all_month_year[] = $dt->format("Y-m");
        }
    }

    return $all_month_year;
}

?>