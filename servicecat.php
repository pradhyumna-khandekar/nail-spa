<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<div class="form-group"><label class="col-sm-3 control-label">Select Service<span>*</span></label>

    <?php
    /*
     * Get All Services
     */
    $ser_sto_code = array();
    $all_services = select("*", "tblServices", "Status = 0 AND active_status=1");
    if (isset($all_services) && is_array($all_services) && count($all_services) > 0) {
        foreach ($all_services as $askey => $asvalue) {
            $ser_sto_code[$asvalue['StoreID']][$asvalue['ServiceID']] = array(
                'ServiceName' => $asvalue['ServiceName'],
                'ServiceCode' => $asvalue['ServiceCode'],
                'ServiceCost' => $asvalue['ServiceCost']
            );
        }
    }
    ?>
    <div class="col-sm-6 ">	
        <?php /* <select class="form-control required" name="ServiceID[]" multiple  id="ServiceID" onChange="checkproduct();" > */ ?>
        <input type="checkbox" name="all_services" id="sel_all_ser" onChange="selectService();" value="1"/><label for="sel_all_ser" style="font-weight: 200px;">Select All Services</label>
        <select name="ServiceID[]" class="form-control col-sm-4 load_charges jqr_service_div" multiple style="height:150pt" id="serviceid">

            <option value="" selected>--SELECT Service--</option>

            <?php
            $type = $_POST["type"];
            $storeid = $_POST['storeid'];
            $DB = Connect();
            foreach ($type as $vap) {
                foreach ($storeid as $vapp) {
                    $sql_display = select("distinct(ServiceID)", "tblProductsServices", "CategoryID='$vap' and StoreID='$vapp'");
                    foreach ($sql_display as $val) {
                        //$servicessr[] = $val['ServiceID'];
                        $service_code = isset($ser_sto_code[$vapp][$val['ServiceID']]['ServiceCode']) ? $ser_sto_code[$vapp][$val['ServiceID']]['ServiceCode'] : '';
                        $service_name = isset($ser_sto_code[$vapp][$val['ServiceID']]['ServiceName']) ? $ser_sto_code[$vapp][$val['ServiceID']]['ServiceName'] : '';
                        $service_cost = isset($ser_sto_code[$vapp][$val['ServiceID']]['ServiceCost']) ? $ser_sto_code[$vapp][$val['ServiceID']]['ServiceCost'] : '';

                        if ($service_code != '') {
                            $final_service[$service_code] = array(
                                'ServiceName' => $service_name,
                                'ServiceCode' => $service_code,
                                'ServiceCost' => $service_cost,
                                'ID' => $val['ServiceID']
                            );
                        }
                    }
                }
            }

            if (isset($final_service) && is_array($final_service) && count($final_service) > 0) {
                foreach ($final_service as $fkey => $fvalue) {
                    $servicename = $fvalue['ServiceName'];
                    $ServiceCost = $fvalue["ServiceCost"];
                    $ServiceCode = $fvalue["ServiceCode"];
                    ?>
                    <option value="<?= $fvalue['ID'] . "#" . $ServiceCode ?>"><?php echo $servicename ?>, Rs. <?= $ServiceCost ?></option>
                    <?php
                }
            }


            /*  $servicess = array_unique($servicessr);

              for ($t = 0; $t < count($servicess); $t++) {

              if ($servicess[$t] == "") {
              ?>

              <?php
              } else {
              $selpt = select("DISTINCT(ServiceID)", "tblServices", "ServiceID='" . $servicess[$t] . "'");
              $serviceidd = $selpt[0]['ServiceID'];
              if ($serviceidd == $servicess[$t]) {
              $seti = select("*", "tblServices", "ServiceID='" . $serviceidd . "'");
              $servicename = $seti[0]['ServiceName'];
              $ServiceCost = $seti[0]["ServiceCost"];
              $ServiceCode = $seti[0]["ServiceCode"];
              ?>

              <option value="<?= $serviceidd . "#" . $ServiceCode ?>"><?php echo $servicename ?>, Rs. <?= $ServiceCost ?></option>
              <?php } else {
              ?>

              <?php
              }
              }
              } */
            ?>

        </select>
    </div>
</div>