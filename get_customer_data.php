<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<div class="panel">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Customer Data</h4>
    </div>
    <div class="panel-body">
        <div id="printdata">
            <?php
            $DB = Connect();
            if (isset($_POST['customer_ids']) && $_POST['customer_ids'] != '') {
                $customer_ids = json_decode($_POST['customer_ids'], TRUE);

                if (isset($customer_ids) && is_array($customer_ids) && count($customer_ids) > 0) {
                    $cust_in_ids = implode(",", $customer_ids);
                    if ($cust_in_ids != '') {
                        $custQ = "SELECT * FROM tblCustomers WHERE CustomerID IN(" . $cust_in_ids . ")";
                        $RSdata = $DB->query($custQ);
                        if ($RSdata->num_rows > 0) {
                            while ($result = $RSdata->fetch_assoc()) {
                                $customer_data[$result['CustomerID']] = $result;
                            }
                        }
                    }
                }
            }
            $DB->close();

            if (isset($customer_data) && is_array($customer_data) && count($customer_data) > 0) {
                ?>
                <table class="table table-bordered table-striped table-condensed cf" width="100%">
                    <thead class="cf">
                        <td>Sr. No.</td>
                        <td>Name</td>
                        <td>Mobile No.</td>
                        <td>Email ID</td>
                    </thead>
                    <tbody>
                        <?php
                        $counter = 1;
                        foreach ($customer_data as $key => $value) {
                            ?>
                            <tr>
                                <td><?php echo $counter; ?></td>
                                <td><?php echo $value['CustomerFullName']; ?></td>
                                <td><?php echo $value['CustomerMobileNo']; ?></td>
                                <td><?php echo $value['CustomerEmailID']; ?></td>
                            </tr>
                            <?php
                            $counter++;
                        }
                        ?>
                    </tbody>
                </table>

            <?php } else { ?>
                <h4>No Record Found</h4>
            <?php }
            ?>
        </div>
    </div>
</div>