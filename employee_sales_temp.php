<?php require_once("setting.fya"); ?>
<?php

$email_body = '<html id="printarea">
    <head>
        <style>
            .main-table{background-color:#fafafa;font-family:Arial, Helvetica, sans-serif; font-size:14px; margin:0; padding:0;}
            tr, td{font-size: 14px;}
            th{color:#795548;padding:5px;}
            h1{margin: 0;padding: 10px;background-color: #f5f5f5;text-align:left;font-size:25px;color:#607D8B;}
            .report-table{border-collapse: collapse;display:block;text-align:center;}
            .report-table tr{border-bottom: 2px solid #795548;}
            .report-table tr td{padding:5px;}
        </style>
    </head>


    <body>

        <table  class="main-table" cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">

            <td align="center" valign="top" style="padding:20px 0 20px 0">

                <table bgcolor="#FFFFFF" cellspacing="0" cellpadding="10" border="0" width="600" style="border:1px solid #dddddd;">


                    <tr>
                        <td colspan="2" style="padding:10px;">
                            <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td colspan="2" style="background-color: #f5f5f5;padding:15px 0 0;text-align:center;">
                                        <h1 class="text-center" style="padding:0 0 15px;background:none;text-align:center;font-size:25px;color:#607D8B;">Sales Report For ' . date('d/m/Y') . ' | Nailspa </h1>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>';

$DB = Connect();

/*
 * Get Time For sales report
 */
$config_data = select("*", "report_config", "status = 1 AND report_name ='emp_sales"
        . "'");
if (isset($config_data[0]['report_time']) && $config_data[0]['report_time'] != '') {
    $time_in_12_hour_format = $config_data[0]['report_time'];


    $append_qry = " AND DATE_FORMAT(OfferDiscountDateTime, '%H:%i:%s') >='" . $time_in_12_hour_format . "'";

    /*
     * Get All Employee
     */
    $getfrom = date('Y-m-d');
    $getto = date('Y-m-d');

    $emp_child = select("*", "tblEmployees", "EID > 0");
    if (isset($emp_child) && is_array($emp_child) && count($emp_child) > 0) {
        foreach ($emp_child as $ckey => $cvalue) {
            $all_emp_ids[$cvalue['EID']] = $cvalue['EID'];
        }
    }

    if (isset($all_emp_ids) && is_array($all_emp_ids) && count($all_emp_ids) > 0) {
        $in_emp_ids = implode(",", $all_emp_ids);
        if ($in_emp_ids != '') {
            $sqlTempfrom = " and Date(tblInvoiceDetails.OfferDiscountDateTime)>=Date('" . $getfrom . "')";
            $sqlTempto = " and Date(tblInvoiceDetails.OfferDiscountDateTime)<=Date('" . $getto . "')";

            $sqldetails = "SELECT tblEmployees.EID,tblInvoiceDetails.CustomerID,tblAppointmentAssignEmployee.AppointmentID,
                                            tblAppointmentAssignEmployee.Qty,
                                            tblAppointmentAssignEmployee.ServiceID,
                                            tblAppointmentAssignEmployee.Commission, 
                                            tblAppointmentAssignEmployee.QtyParam,
                                            tblInvoiceDetails.OfferDiscountDateTime,tblEmployees.StoreID 
                                            FROM tblEmployees 
                                            join tblAppointmentAssignEmployee on tblEmployees.EID = tblAppointmentAssignEmployee.MECID 
                                            join tblInvoiceDetails on tblAppointmentAssignEmployee.AppointmentID=tblInvoiceDetails.AppointmentId 
                                            where tblAppointmentAssignEmployee.AppointmentID!='NULL' 
                                            and tblInvoiceDetails.OfferDiscountDateTime!='NULL' 
                                            $sqlTempfrom 
                                            $sqlTempto 
                                            $empq and tblEmployees.EID IN(" . $in_emp_ids . ") $append_qry
                                            group by tblAppointmentAssignEmployee.AppointmentID,ServiceID,QtyParam";
            $RSdetails = $DB->query($sqldetails);

            while ($rowdetails = $RSdetails->fetch_assoc()) {
                $invoice_data[$rowdetails['EID']][] = $rowdetails;
            }

            if (isset($invoice_data) && is_array($invoice_data) && count($invoice_data) > 0) {
                $service_ids = array();
                $store_ids = array();
                $appointment_ids = array();
                $customer_ids = array();

                foreach ($invoice_data as $detailkey => $detailvalue) {
                    foreach ($detailvalue as $invkey => $invvalue) {
                        $service_ids[$invvalue["ServiceID"]] = $invvalue["ServiceID"];
                        $store_ids[$invvalue["StoreID"]] = $invvalue["StoreID"];
                        $appointment_ids[$invvalue["AppointmentID"]] = $invvalue["AppointmentID"];
                        $customer_ids[$invvalue["CustomerID"]] = $invvalue["CustomerID"];
                    }
                }


                /*
                 * get service name
                 */
                if (isset($service_ids) && is_array($service_ids) && count($service_ids) > 0) {
                    $service_in_ids = implode(",", $service_ids);
                    $service_q = "SELECT ServiceID,ServiceName FROM tblServices WHERE ServiceID IN(" . $service_in_ids . ")";
                    $service_exe = $DB->query($service_q);
                    while ($servdetails = $service_exe->fetch_assoc()) {
                        $all_services[] = $servdetails;
                    }

                    if (isset($all_services) && is_array($all_services) && count($all_services) > 0) {
                        foreach ($all_services as $serkey => $servalue) {
                            $sep[$servalue['ServiceID']] = $servalue['ServiceName'];
                        }
                    }
                }


                /*
                 * get store name
                 */
                if (isset($store_ids) && is_array($store_ids) && count($store_ids) > 0) {
                    $store_in_ids = implode(",", $store_ids);
                    $store_q = "SELECT StoreID,StoreName FROM tblStores WHERE StoreID IN(" . $store_in_ids . ")";
                    $store_exe = $DB->query($store_q);
                    while ($storedetails = $store_exe->fetch_assoc()) {
                        $all_store[] = $storedetails;
                    }
                    if (isset($all_store) && is_array($all_store) && count($all_store) > 0) {
                        foreach ($all_store as $stokey => $stovalue) {
                            $stpp[$stovalue['StoreID']] = $stovalue['StoreName'];
                        }
                    }
                }

                /*
                 * get invoice number
                 */
                if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                    $apt_in_ids = implode(",", $appointment_ids);
                    $apt_q = "SELECT InvoiceID,AppointmentID FROM tblInvoice WHERE AppointmentID IN(" . $apt_in_ids . ")";
                    $apt_exe = $DB->query($apt_q);
                    while ($aptdetails = $apt_exe->fetch_assoc()) {
                        $all_apt[] = $aptdetails;
                    }

                    if (isset($all_apt) && is_array($all_apt) && count($all_apt) > 0) {
                        foreach ($all_apt as $aptkey => $aptvalue) {
                            $sql_invoice_number[$aptvalue['AppointmentID']] = $aptvalue['InvoiceID'];
                        }
                    }
                }

                /*
                 * get AppointmentDate
                 */
                if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                    $apt_in_ids = implode(",", $appointment_ids);
                    $aptdate_q = "SELECT AppointmentDate,AppointmentID FROM tblAppointments WHERE AppointmentID IN(" . $apt_in_ids . ")";
                    $aptdate_exe = $DB->query($aptdate_q);
                    while ($aptdatedetails = $aptdate_exe->fetch_assoc()) {
                        $alldate_apt[] = $aptdatedetails;
                    }
                    if (isset($alldate_apt) && is_array($alldate_apt) && count($alldate_apt) > 0) {
                        foreach ($alldate_apt as $aptdkey => $aptdvalue) {
                            $sql_apt_date[$aptdvalue['AppointmentID']] = $aptdvalue['AppointmentDate'];
                        }
                    }
                }

                /*
                 * get customer
                 */
                if (isset($customer_ids) && is_array($customer_ids) && count($customer_ids) > 0) {
                    $cust_in_ids = implode(",", $customer_ids);

                    $cust_q = "SELECT CustomerID,CustomerFullName FROM tblCustomers WHERE CustomerID IN(" . $cust_in_ids . ")";
                    $cust_exe = $DB->query($cust_q);
                    while ($custdetails = $cust_exe->fetch_assoc()) {
                        $all_cust[] = $custdetails;
                    }
                    if (isset($all_cust) && is_array($all_cust) && count($all_cust) > 0) {
                        foreach ($all_cust as $custkey => $custvalue) {
                            $sql_customer[$custvalue['CustomerID']] = $custvalue['CustomerFullName'];
                        }
                    }
                }

                /*
                 * get service amt
                 */

                if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                    $apt_in_ids = implode(",", $appointment_ids);
                    $aptdate_q = "SELECT ServiceAmount,AppointmentID,ServiceID FROM tblAppointmentsDetailsInvoice WHERE AppointmentID IN(" . $apt_in_ids . ") AND PackageService=0";
                    $aptdate_exe = $DB->query($aptdate_q);
                    while ($aptdatedetails = $aptdate_exe->fetch_assoc()) {
                        $all_apt_service_amt[] = $aptdatedetails;
                    }
                    if (isset($all_apt_service_amt) && is_array($all_apt_service_amt) && count($all_apt_service_amt) > 0) {
                        foreach ($all_apt_service_amt as $servdkey => $servvalue) {
                            $all_ser_amt[$servvalue['AppointmentID']][$servvalue['ServiceID']] = $servvalue['ServiceAmount'];
                        }
                    }
                }


                /*
                 * get discount 
                 */
                if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                    $apt_in_ids = implode(",", $appointment_ids);
                    $aptdate_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ")";
                    $aptdate_exe = $DB->query($aptdate_q);
                    while ($disdetails = $aptdate_exe->fetch_assoc()) {
                        $all_apt_dis_amt[] = $disdetails;
                    }

                    /*
                     * Get Appointment Offer AMount
                     */
                    $apt_in_ids = implode(",", $appointment_ids);
                    $aptoffer_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ") AND OfferAmount > 0";
                    $aptoffer_exe = $DB->query($aptoffer_q);
                    while ($offeretails = $aptoffer_exe->fetch_assoc()) {
                        $all_apt_off_dis[$offeretails['AppointmentID']][$offeretails['ServiceID']] = $offeretails['OfferAmount'];
                    }


                    if (isset($all_apt_dis_amt) && is_array($all_apt_dis_amt) && count($all_apt_dis_amt) > 0) {
                        foreach ($all_apt_dis_amt as $diskey => $disvalue) {
                            /*
                             * Get Appointment service count
                             */
                            $extra_offer_amount = 0;

                            if (isset($all_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']]) && $all_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']] > 0) {
                                $extra_offer_amount = $all_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']];
                            }
                            //echo '<br>extra_offer_amount = ' . $extra_offer_amount;
                            //echo '<br>member_amount = ' . $disvalue['MemberShipAmount'];
                            $all_dis_amt[$disvalue['AppointmentID']][$disvalue['ServiceID']]['MemberShipAmount'] = $disvalue['MemberShipAmount'] + round($extra_offer_amount, 2);
                            $all_apt_mem_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']] = $disvalue['MemberShipAmount'];
                            //$all_dis_amt[$disvalue['AppointmentID']][$disvalue['ServiceID']]['OfferAmount'] = $disvalue['OfferAmount'];
                        }
                    }
                }
            }
        }
    }


    if (isset($invoice_data) && is_array($invoice_data) && count($invoice_data) > 0) {
        if (isset($all_emp_ids) && is_array($all_emp_ids) && count($all_emp_ids) > 0) {
            $final_res = array();
            foreach ($all_emp_ids as $ekey => $evalue) {
                $search_emp[0]['EID'] = $evalue;

                if (isset($search_emp) && is_array($search_emp) && count($search_emp) > 0) {
                    $emp_data = select("*", "tblEmployees", "EID='" . $search_emp[0]['EID'] . "'");
                    foreach ($search_emp as $key => $row) {
                        $strEID = $emp_data[0]["EID"];
                        $strEmployeeName = $emp_data[0]["EmployeeName"];
                        $strEmployeeEmailID = $emp_data[0]["EmployeeEmailID"];
                        $strEmpPercentage = $emp_data[0]["EmpPercentage"];
                        $strEmployeeMobileNo = $emp_data[0]["EmployeeMobileNo"];

                        $TotalAfterDivideSale = '';
                        $strTotalAmount = '';
                        $TotalFinalDiscount = '';
                        $TotalUltimateSale = '';
                        $ComFinal = '';
                        if (isset($invoice_data[$strEID]) && is_array($invoice_data[$strEID]) && count($invoice_data[$strEID]) > 0) {
                            ?>

                            <?php

                            $email_body.= '<tr>
                                                    <td colspan="2" valign="top" style="border-bottom:1px solid #dddddd; padding:10px; ">
                                                        <table class="report-table">
                                                            <tr style="background-color: #000;">
                                                                <th colspan="2" style="color: #fff;">Name : ' . $strEmployeeName . '</th>
                                                                <th colspan="2" style="color: #fff;">Email : ' . $strEmployeeEmailID . '</th>
                                                                <th colspan="2" style="color: #fff;">Mobile no. : ' . $strEmployeeMobileNo . ' </th>
                                                                <th colspan="1" style="color: #fff;">Commission Percentage : ' . $strEmpPercentage . '</th>
                                                            </tr>
                                                            <tr style="background-color: #f5f5f5;">
                                                                <th>Sr. No.</th>
                                                                <th>Client Name</th>
                                                                <th>Service(s) Done</th>
                                                                <th>Service Amount</th>
                                                                <th>MemberShip Discount</th>
                                                                <th>Other Discount</th>
                                                                <th>Total Sales</th>
                                                            </tr>';


                            if ($_SERVER['REMOTE_ADDR'] == "111.119.219.70") {
                                //echo $sqldetails;
                            } else {
                                
                            }

                            $counter = 0;
                            $strSID = "";
                            $qty = "";
                            $strSAmount = "";
                            $strAmount = "";
                            $strCommission = "";
                            $FinalDAmount = '';
                            $FinalDiscount = '';
                            $FinalOtherDiscount = 0;
                            $TotalOtherDiscount = 0;
                            $UltimateSale = '';
                            $AfterDivideSale = '';
                            $CommssionFinal = "";
                            //echo("Error description: " . mysqli_error($DB));


                            $final_res = array();
                            $rowstrTotalAmount = 0;
                            $row_invoice = 0;
                            $apt_id = 0;
                            $rowComFinal = 0;
                            foreach ($invoice_data[$strEID] as $rowkey => $rowdetails) {
                                $strAID = $rowdetails["AppointmentID"];
                                $strSID = $rowdetails["ServiceID"];
                                $rowstrAmount = isset($all_ser_amt[$strAID][$strSID]) ? $all_ser_amt[$strAID][$strSID] : '';
                                $rowstrTotalAmount += $rowstrAmount;
                                $qty = $rowdetails["Qty"];

                                if ($apt_id != $rowdetails['AppointmentID']) {
                                    $row_invoice = $row_invoice + 1;
                                }
                                $apt_id = $rowdetails['AppointmentID'];

                                $strEmpPercentage = $emp_data[0]["EmpPercentage"];

                                $strCommission = $rowdetails["Commission"];
                                if ($strCommission == "1") {
                                    $AfterDivideSale = $rowstrAmount;
                                } elseif ($strCommission == "2") {
                                    $AfterDivideSale = ($rowstrAmount / 2);
                                }

                                $rowdiscount = isset($all_dis_amt[$strAID][$strSID]) ? $all_dis_amt[$strAID][$strSID] : array();
                                if (isset($rowdiscount) && is_array($rowdiscount) && count($rowdiscount) > 0) {
                                    $strOfferAmount = $rowdiscount["OfferAmount"];
                                    $strDiscountAmount = $rowdiscount["MemberShipAmount"];

                                    if ($strOfferAmount > 0) {
                                        $FinalDAmount = $strOfferAmount;
                                    } elseif ($strDiscountAmount > 0) {
                                        $FinalDAmount = $strDiscountAmount;
                                    }
                                } else {
                                    $FinalDAmount = "0";
                                }

                                $FinalDiscount = $FinalDAmount / $qty;

                                $rowUltimateSale = $AfterDivideSale - $FinalDiscount;
                                if ($strCommission == "1") {
                                    $rowCommssionFinal = ($rowUltimateSale / 100) * $strEmpPercentage;
                                } elseif ($strCommission == "2") {
                                    $rowCommssionFinal = ($rowUltimateSale / 200) * $strEmpPercentage;
                                }
                                $rowComFinal += $rowCommssionFinal;

                                $final_res[$strEID][$rowdetails['AppointmentID'] . '-' . $rowdetails["ServiceID"]] = $rowdetails;

                                if (isset($emp_service_qty[$rowdetails['AppointmentID']][$rowdetails["ServiceID"]][$strEID])) {
                                    $emp_service_qty[$rowdetails['AppointmentID']][$rowdetails["ServiceID"]][$strEID] +=1;
                                } else {
                                    $emp_service_qty[$rowdetails['AppointmentID']][$rowdetails["ServiceID"]][$strEID] = 1;
                                }

                                $apt_service_qty[$rowdetails['AppointmentID']][$rowdetails["ServiceID"]] = $rowdetails['Qty'];
                            }
                            foreach ($final_res[$strEID] as $rowkey => $rowdetails) {
                                $strEIDa = $rowdetails["EID"];
                                $strAID = $rowdetails["AppointmentID"];
                                $strSID = $rowdetails["ServiceID"];
                                $CustomerID = $rowdetails['CustomerID'];
                                $qty = $rowdetails["Qty"];

                                $counter ++;

                                $emp_ser_count = isset($emp_service_qty[$strAID][$strSID][$strEID]) ? $emp_service_qty[$strAID][$strSID][$strEID] : 1;
                                $apt_ser_count = isset($apt_service_qty[$strAID][$strSID]) ? $apt_service_qty[$strAID][$strSID] : 1;

                                $strAmount = isset($all_ser_amt[$strAID][$strSID]) ? ($all_ser_amt[$strAID][$strSID] * $qty * $emp_ser_count) / $apt_ser_count : '';
                                $strSAmount = $strAmount;
                                $strCommission = $rowdetails["Commission"];
                                $StoreIDd = $rowdetails["StoreID"];

                                $strTotalAmount += $strSAmount;  //Total of Service sale amount
                                $servicename = isset($sep[$strSID]) ? $sep[$strSID] : '';

                                $StoreName = isset($stpp[$StoreIDd]) ? $stpp[$StoreIDd] : '';

                                $Invoice_Number = isset($sql_invoice_number[$strAID]) ? $sql_invoice_number[$strAID] : '';

                                $AppointmentDate = isset($sql_apt_date[$strAID]) ? $sql_apt_date[$strAID] : '';


                                $CustomerFullName = isset($sql_customer[$CustomerID]) ? $sql_customer[$CustomerID] : '';


                                if ($strCommission == "1") {
                                    $AfterDivideSale = $strSAmount;
                                    $strCommissionType = '<span class = "bs-label label-success">Alone</span>';
                                } elseif ($strCommission == "2") {
                                    $AfterDivideSale = ($strSAmount / 2);
                                    $strCommissionType = '<span class = "bs-label label-blue-alt">Split</span > ';
                                }
                                $TotalAfterDivideSale += $AfterDivideSale;  //Total of Final payment

                                $FinalDiscount = isset($all_apt_mem_dis[$strAID][$strSID]) ? ($all_apt_mem_dis[$strAID][$strSID] * $emp_ser_count) / $apt_ser_count : 0;
                                $FinalOtherDiscount = isset($all_apt_off_dis[$strAID][$strSID]) ? ($all_apt_off_dis[$strAID][$strSID] * $emp_ser_count) / $apt_ser_count : 0;

                                $TotalFinalDiscount += $FinalDiscount; //Total of discounted amount
                                $TotalOtherDiscount += $FinalOtherDiscount; //Total of discounted amount


                                $UltimateSale = $AfterDivideSale - $FinalDiscount - $FinalOtherDiscount;
                                $TotalUltimateSale += $UltimateSale; //Total of discounted amount
                                //Calculation for commission
                                if ($strCommission == "1") {
                                    $CommssionFinal = ($UltimateSale / 100) * $strEmpPercentage;
                                } elseif ($strCommission == "2") {
                                    $CommssionFinal = ($UltimateSale / 200) * $strEmpPercentage;
                                }
                                $ComFinal += $CommssionFinal; //Total of Commission

                                $email_body .= '<tr id="my_data_tr_<?= $counter ?>">
                                                                    <td>' . $counter . '</td>
                                                                    <td>' . $CustomerFullName . '</td>
                                                                    <td>' . $servicename . '</td>
                                                                    <td>Rs.' . $UltimateSale . '/-</td>
                                                                    <td>Rs.' . $FinalDiscount . '/-</td>
                                                                    <td>Rs.' . $FinalOtherDiscount . '/-</td>
                                                                    <td>Rs.' . $strAmount . '/-</td>
                                                                </tr>';
                            }


                            $email_body .= '<tr>
                                                                <td colspan="3"><b><center>Total Sales : </center></b></td>
                                                                <td><b>Rs.' . $TotalUltimateSale . '/-</b></td>
                                                                <td><b>Rs.' . $TotalFinalDiscount . '</b></td> 
                                                                <td><b>Rs.' . $TotalOtherDiscount . '/-</b></td>
                                                                <td><b>Rs.' . $strTotalAmount . '/-</b></td>

                                                            </tr>
                                                            </tbody>

                                                        </table>
                                                    </td>
                                                </tr>';
                        }
                    }
                }
            }
        }
    } else {

        $email_body.= '<tr>
                                <td> <h3>No Sales Found For ' . date('d/m/Y') . ' !</h3></td>
                            <tr>';
    }
} else {

    $email_body .='<tr>
                            <td> <h3>No Time Set For Report!</h3></td>
                        <tr>';
}
$DB->close();

$email_body .= '</table>
            </td>
        </tr>
    </table>
</body>
</html>';

return $email_body;
