<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
$strPageTitle = "Invoice Print | Nailspa";
$strDisplayTitle = "Invoice Print";

if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
?>
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


        <!--@media print
        {
            #printarea { display: block; }
        }
        #printarea1 {
                background:url('http://nailspaexperience.com/images/test3.png') no-repeat; 
                background-position:50% -5px;
                visibility: visible;
        }-->
        <style type="text/css">
            @media print {
                body * {
                    visibility: hidden;
                }
                #printarea * {
                    visibility: visible;
                }
                #printarea{
                    position: absolute;
                    left: 0;
                    top: 0;
                }
            }

            #printareasaifu
            {
                visibility: hidden;
            }
        </style>
        <style>
            body{color:#3a020a;;}
            p{margin:3px;}
            tr, td{font-size: 14px;}
            th{color:#795548;padding:5px;}
            h1{margin: 0;text-align:center;font-size:25px;color:#607D8B;}
            h2{font-size:16px;}
            button{font-size: 16px;
                   display: block;
                   margin: 20px 0;
                   background-color: #F44336;
                   color: #fff;
                   padding: 10px;
                   border: none;
                   border-radius: 5px;}
            .main-table{background-color:#fafafa;font-family: 'Helvetica' , sans-serif; font-size:14px; margin:0; padding:0;}
            .report-table{border-collapse: collapse;display:block;text-align:center;}
            .report-table tr{border-bottom: 2px solid #795548;}
            .report-table tr td{padding:5px;}

        </style>
    </head>
    <?php $img = 'http://nailspaexperience.com/images/test3.png' ?>
    <body id="displays">                


        <div class="alert-content">
            <h4 class="alert-title"><center><b><?php echo $_GET['msg'] ?></b></center></h4>
        </div><br/>
        <div class="alert-content" id="displaymsg" style="display:none">

        </div><br/>       
        <div id="printarea">
            <table class="main-table" cellspacing="0" cellpadding="0" border="0" width="100%">
                <tbody style="border:1px" >
                    <tr style="background-repeat: no-repeat; background-position:50% 20%; media:print; -webkit-print-color-adjust: exact;">
                        <td>
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center" >
                                <tbody>

                                    <tr >
                                        <td>
                                            <table id="printarea" style="BORDER-BOTTOM:#3a020a 1px solid;BORDER-LEFT:#3a020a 1px solid;BORDER-TOP:#3a020a 1px solid;BORDER-RIGHT:#3a020a 1px solid;" cellspacing="0" cellpadding="0" width="98%" align="center" media="print">
                                                <tbody>

                                                    <?php
                                                    $seldp = select("*", "tblAppointments", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                    $seldpd = select("StoreName", "tblStores", "StoreID='" . $seldp[0]['StoreID'] . "'");
                                                    $seldpde = select("InvoiceID", "tblInvoice", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                    $seldpdep = select("*", "tblCustomers", "CustomerID='" . $seldp[0]['CustomerID'] . "'");
                                                    date_default_timezone_set('Asia/Kolkata');
                                                    $timestamp = date("H:i:s", time());
                                                    $sqlUpdate1 = "UPDATE tblAppointments SET AppointmentCheckOutTime = '" . $timestamp . "', Status = '2' WHERE AppointmentID='" . DecodeQ($_GET['uid']) . "'";
//$passingID = EncodeQ(DecodeQ($passingID1));
                                                    ExecuteNQ($sqlUpdate1);

                                                    $seldpdeptp = select("*", " tblAppointmentsDetailsInvoice", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                    foreach ($seldpdeptp as $ty) {
                                                        $totalservices = $ty['ServiceID'];
                                                        $seldpdepp = select("MECID", "tblAppointmentAssignEmployee", "ServiceID='" . $totalservices . "' and AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                        //print_r($seldpdepp);
                                                    }
                                                    $appp_id = DecodeQ($_GET['uid']);
                                                    ?>
                                                    <?php
                                                    $selSettingsData = select("*", "tblSettings", "SettingID='1'");

                                                    $selStoreGST = select("GSTNo, AccountingCode, CompanyName, URL", "tblStores", "StoreID='" . $seldp[0]['StoreID'] . "'");

                                                    if ($selStoreGST[0]['GSTNo'] == "" || $selStoreGST[0]['GSTNo'] == " " || $selStoreGST[0]['GSTNo'] == "0") {
                                                        $strGSTno = $selSettingsData[0]['MasterGSTNo'];
                                                    } else {
                                                        $strGSTno = $selStoreGST[0]['GSTNo'];
                                                    }

                                                    if ($selStoreGST[0]['CompanyName'] == "" || $selStoreGST[0]['CompanyName'] == " " || $selStoreGST[0]['CompanyName'] == "0") {
                                                        $strCompanyName = $selSettingsData[0]['CompanyName'];
                                                    } else {
                                                        $strCompanyName = $selStoreGST[0]['CompanyName'];
                                                    }
                                                    //$strCompanyName = $selSettingsData[0]['CompanyName'];
                                                    ?>
                                                    <tr>
                                                        <td align="middle">
                                                            <input type="hidden" name="appointment_id" id="appointment_id" value="<?php echo DecodeQ($_GET['uid']) ?>" />
                                                            <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td width="20%" align="left" style="padding:1%;">
                                                                            <?php 
                                                                            if ($selStoreGST[0]['URL'] != "" || $selStoreGST[0]['URL'] != " " )
                                                                            {
                                                                                $strCompanyLogo = $selStoreGST[0]['URL'];
                                                                                // var_dump($strCompanyLogo);exit();
                                                                            } else {
                                                                                $strCompanyLogo = '';
                                                                            } ?>
                                                                            <?php if(isset($strCompanyLogo)){?>
                                                                            <img border="0" src="<?php echo $strCompanyLogo; ?>" width="100" height="100">
                                                                            <?php }?>
                                                                        </td>
                                                                        <td width="30%">
                                                                            <span style="LINE-HEIGHT:0;FONT-SIZE:20px;text-align:center;"><?= $strCompanyName ?></span>
                                                                            <br><br><span class="fs">GSTIN - <?php echo $strGSTno; ?></span></td>
                                                                        <td width="50%" align="right" style="LINE-HEIGHT:15px; padding:1%; FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal;"><b>
                                                                                <?php echo $seldpd[0]['StoreName']; ?></b>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="LINE-HEIGHT:0;BACKGROUND:#3a020a;FONT-SIZE:0px;" height="5"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="middle">
                                                            <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                                                <tbody>
                                                                    <tr style="padding-left:10%;">
                                                                        <td width="76.5%">To ,</td>
                                                                        <td width="10%">Invoice No :</td>
                                                                        <td width="10%" style="float:center; padding-right:05%"><?php echo $seldpde[0]['InvoiceID']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="50%"><b><?php echo $seldpdep[0]['CustomerFullName']; ?></b></td>
                                                                        <td width="25%">Membership No :</td>
                                                                        <td width="25%" style="float:center; padding-right:05%">

                                                                            <?php
                                                                            if ($seldp[0]['memberid'] != '0') {
                                                                                echo $seldp[0]['memberid'];
                                                                            } else {
                                                                                echo "-";
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>

                                                                        <td width="50%"><?php echo $seldpdep[0]['CustomerEmailID'] ?>
                                                                            <input type="hidden" id="email" value="<?php echo $seldpdep[0]['CustomerEmailID'] ?>" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="50%"><?php echo $seldpdep[0]['CustomerMobileNo'] ?></td>
                                                                        <td width="25%">stylist(s) :</td>
                                                                        <td width="25%" style="float:center; padding-right:05%">
                                                                            <?php
                                                                            $seldpdeppt = select("MECID", " tblAppointmentAssignEmployee", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                                            foreach ($seldpdeppt as $vap) {
                                                                                $empname = $vap['MECID'];
                                                                                if ($empname != "0") {
                                                                                    $emppp = $emppp . ',' . $empname;
                                                                                }
                                                                            }
//$empnamep=implode(",",$empname);

                                                                            if ($emppp == "") {
                                                                                echo "-";
                                                                            } elseif ($emppp == "0") {
                                                                                
                                                                            } else {
                                                                                ?>
                                                                                <?php echo trim($emppp, ","); ?>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td height="8"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="LINE-HEIGHT:0;BACKGROUND:#3a020a;FONT-SIZE:20px;text-align:center;color: #dcc36f;" height="30"><b>Service</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="8">
                                                            <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td bgcolor="#e4e4e4" height="4"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <th width="5%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">Sr</th>
                                                                        <th width="60%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:left; padding-left:2%;">Item Description</th>
                                                                        <th width="15%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">Quantity</th>
                                                                        <th width="15%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">Amount</th>
                                                                    </tr>
                                                                    <?php
                                                                    $seldpdept = select("*", " tblAppointmentsDetailsInvoice", "AppointmentID='" . DecodeQ($_GET['uid']) . "' and PackageService='0'");
                                                                    $sub_total = 0;
                                                                    $countsf = "0";
                                                                    $countersaif = "";
                                                                    $counterusmani = "1";
                                                                    foreach ($seldpdept as $val) {
                                                                        $countersaif ++;
                                                                        $countsf++;
                                                                        $counterusmani = $counterusmani + 1;
                                                                        $totalammt = 0;
                                                                        $AppointmentDetailsID = $val['AppointmentDetailsID'];
                                                                        $servicee = select("*", "tblServices", "ServiceID='" . $val['ServiceID'] . "'");
                                                                        $qtyyy = $val['qty'];
                                                                        $amtt = $val['ServiceAmount'];
                                                                        $quantity = $val['qty'];
                                                                        $totalammt = $qtyyy * $amtt;
                                                                        $total = 0;
                                                                        ?>

                                                                        <tr>
                                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;"><?= $countsf ?></td>
                                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; padding-left:2%;"><?php echo $servicee[0]['ServiceName']; ?></td>
                                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:right; padding-right:05%;"><?php echo $quantity; ?>

                                                                            </td>
                                                                            <td id="cost" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:right; padding-right:05%;"><?php echo $totalammt . ".00"; ?>
                                                                                <?php
                                                                                $sub_total = $sub_total + $totalammt;
                                                                                $total = $total + $sub_total;
                                                                                ?>
                                                                            </td>

                                                                        </tr>
                                                                        <tr>
                                                                            <?php
                                                                            $seldember = select("*", "tblAppointments", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                                            $memid = $seldember[0]['memberid'];

                                                                            $PackageID = $seldoffert[0]['PackageID'];
                                                                            $DB = Connect();


                                                                            if ($memid != "0" && count($temp_package_data) == 0) {
                                                                                $seldatap = select("DiscountType", "tblMembership", "MembershipID='" . $memid . "'");
                                                                                $type = $seldatap[0]['DiscountType'];
                                                                                if ($type == '0') {
                                                                                    $seldata = select("distinct(NotValidOnServices),MembershipName,Discount", "tblMembership", "MembershipID='" . $memid . "'");
                                                                                    //print_r($seldata);
                                                                                    $services = $seldata[0]['NotValidOnServices'];
                                                                                    $membershipname = $seldata[0]['MembershipName'];
                                                                                    $Discount = $seldata[0]['Discount'];
                                                                                    $sericesd = explode(",", $services);
                                                                                    $temp_package_data = array();

                                                                                    $package_done_qry = "SELECT * FROM tblCustomerAppointmentPackage WHERE appointment_id='" . DecodeQ($_GET['uid']) . "' AND status=1 AND service_id ='" . $val['ServiceID'] . "'";
                                                                                    $package_done_res = $DB->query($package_done_qry);
                                                                                    if ($package_done_res->num_rows > 0) {
                                                                                        while ($rowdata = $package_done_res->fetch_assoc()) {
                                                                                            $total_qty_done_qty = $rowdata['service_qty_done'];
                                                                                            $qty_to_apply_dis = $val['qty'] - $total_qty_done_qty;
                                                                                            if ($qty_to_apply_dis <= 0) {
                                                                                                $qty_to_apply_dis = 0;
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    if (in_array($val['ServiceID'], $sericesd)) {
                                                                                        
                                                                                    } else {
                                                                                        $serviceid = $val['ServiceID'];
                                                                                        $serviceamount = $val['ServiceAmount'];
                                                                                        if (isset($qty_to_apply_dis)) {
                                                                                            $qty = $qty_to_apply_dis;
                                                                                        } else {
                                                                                            $qty = $val['qty'];
                                                                                        }
                                                                                        $amount = $qty * $serviceamount;
                                                                                        $totalamount = $amount * $Discount / 100;
                                                                                        ?>

                                                                                        <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;"><?php echo $membershipname; ?>
                                                                                            <input type="hidden" name="serviceidm[]" id="serviceidm" value="<?= $serviceid ?>" />
                                                                                            <input type="hidden" name="discountm[]" id="discountm" value="<?= $totalamount ?>" />
                                                                                            <input type="hidden" name="memid[]" id="memid" value="<?= $memid ?>" />

                                                                                        </td><td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; padding-left:2%;"><?php echo $Discount; ?>% Membership Discount </td><td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; padding-left:2%;"></td><td id="cost" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:right; padding-right:05%;">
                                                                                            <?= $totalamount . ".00" ?>

                                                                                            <?php
                                                                                            $offdisp = $offdisp + $Discount;
                                                                                            $memberdis = $memberdis + $totalamount;
                                                                                            ?></td><td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;"></td>
                                                                                            <?php
                                                                                        }
                                                                                    } else {
                                                                                        $seldata = select("distinct(NotValidOnServices),MembershipName,Discount", "tblMembership", "MembershipID='" . $memid . "'");
                                                                                        //print_r($seldata);
                                                                                        $services = $seldata[0]['NotValidOnServices'];
                                                                                        $membershipname = $seldata[0]['MembershipName'];
                                                                                        $Discount = $seldata[0]['Discount'];
                                                                                        $sericesd = explode(",", $services);
                                                                                        //print_r($sericesd);

                                                                                        if (in_array($val['ServiceID'], $sericesd)) {
                                                                                            
                                                                                        } else {
                                                                                            ?>

                                                                                        <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;"><?php echo $membershipname; ?>
                                                                                            <input type="hidden" name="serviceidm[]" id="serviceidm" value="<?= $serviceid ?>" />
                                                                                            <input type="hidden" name="discountm[]" id="discountm" value="<?= $Discount ?>" />
                                                                                            <input type="hidden" name="memid[]" id="memid" value="<?= $memid ?>" />
                                                                                        </td>

                                                                                        <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; padding-left:2%;"><?php echo $Discount; ?>Amount Membership Discount </td>
                                                                                        <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; padding-left:2%;"></td>
                                                                                        <td id="cost" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:right; padding-right:05%;">
                                                                                            <?= $Discount . ".00" ?>
                                                                                            <?php
                                                                                            $offdisp = $offdisp + $Discount;
                                                                                            $memberdis = $memberdis + $Discount;




                                                                                            //  $sub_total=$sub_total-$Discount;
                                                                                            // $total=$total+$sub_total;
                                                                                            ?></td> 
                                                                                        <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;"></td>

                                                                                        <?php
                                                                                    }



                                                                                    //echo "<br/>";
                                                                                }
                                                                                $DB->close();
                                                                            } else {
                                                                                
                                                                            }
                                                                            ?>
                                                                        </tr>

                                                                        <?php
                                                                    }

                                                                    $seldoffertpp = select("*", "tblAppointments", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                                    $Package = $seldoffertpp[0]['PackageID'];
                                                                    $packagess = explode(",", $Package);
                                                                    $AppointmentDate = $seldoffertpp[0]['AppointmentDate'];
                                                                    for ($i = 0; $i < count($packagess); $i++) {
                                                                        if ($packagess[$i] != 0) {

                                                                            $seldpack = select("*", "tblPackages", "PackageID='" . $packagess[$i] . "'");
                                                                            $packname = $seldpack[0]['Name'];
                                                                            $packcode = $seldpack[0]['Code'];
                                                                            if ($seldoffertpp[0]['new_package'] == 1) {
                                                                                $PackageNewPrice = $seldpack[0]['PackageNewPrice'];
                                                                            } else {
                                                                                $PackageNewPrice = 0;
                                                                            }
                                                                            $Validityp = $seldpack[0]['Validity'];
                                                                            $valid = "+" . $Validityp . "Months";
                                                                            $validpack = date('Y-m-d', strtotime($valid));
                                                                            $sub_total = $sub_total + $PackageNewPrice;

                                                                            $seldpackq = select("count(*)", "tblBillingPackage", "PackageID='" . $packagess[$i] . "' and AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                                            $countpack = $seldpackq[0]['count(*)'];
                                                                            ?>
                                                                            <tr>
                                                                                <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;"></td>
                                                                                <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:right; padding-right:05%;">&nbsp;&nbsp;<?= $packname . "(" . $packcode . ")" ?></td>
                                                                                <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:right; padding-right:05%;"><b>Valid Till</b><br/><input type="hidden" id="app_date" value="<?= $AppointmentDate ?>" /><?= $validpack ?></td>
                                                                                <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:right; padding-right:05%;"><?= $PackageNewPrice ?>	</td>
                                                                                <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;" class="no-print"> <input type="hidden" id="PackageID" name="PackageID[]" value="<?= $packagess[$i] ?>" />
                                                                                    <?php
                                                                                    if ($countpack > 0) {
                                                                                        
                                                                                    } else {
                                                                                        
                                                                                    }
                                                                                    ?>


                                                                            </tr>
                                                                            <?php
                                                                            if ($packagess[$i] != "0" || $packagess[$i] != "") {

                                                                                $seldpdeptwp = select("*", "tblCustomerAppointmentPackage", "appointment_id = '" . DecodeQ($_GET['uid']) . "' AND package_id='" . $packagess[$i] . "' AND status=1 AND service_id > 0");

                                                                                foreach ($seldpdeptwp as $vatq) {
                                                                                    $servicee = select("*", "tblServices", "ServiceID='" . $vatq['service_id'] . "'");
                                                                                    $ServiceName = $servicee[0]['ServiceName'];
                                                                                    $qtyyy = $val['qty'];
                                                                                    $amtt = $val['ServiceAmount'];
                                                                                    $sstatus = $vatq['service_status'];
                                                                                    $service_qty = isset($vatq['service_qty']) && $vatq['service_qty'] != '' ? $vatq['service_qty'] : 0;
                                                                                    $service_qty_done = isset($vatq['service_qty_done']) && $vatq['service_qty_done'] != '' ? $vatq['service_qty_done'] : 0;
                                                                                    $remaining = isset($vatq['qty_remaining']) && $vatq['qty_remaining'] != '' ? $vatq['qty_remaining'] : 0;

                                                                                    //$seldpdepte = select("*", "tblBillingPackage", "AppointmentID='" . DecodeQ($_GET['uid']) . "' and PackageID='" . $packagess[$i] . "' and ServiceID='" . $vatq['ServiceID'] . "'");
                                                                                    //$sstatus = $seldpdepte[0]['Status'];
                                                                                    ?>
                                                                                    <tr>

                                                                                        <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;"></td>

                                                                                        <?php
                                                                                        if ($sstatus == '2' || $remaining == 0) {
                                                                                            ?>
                                                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:right; padding-right:05%;"><?= $ServiceName . "(" . $service_qty . ")" ?></td>
                                                                                            <?php
                                                                                        } else {
                                                                                            ?>
                                                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:right; padding-right:05%;"><?= $ServiceName . "(" . $service_qty . ")"; ?></td>
                                                                                            <?php
                                                                                        }
                                                                                        ?>

                                                                                        <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:right; padding-right:05%;">
                                                                                            <?php
                                                                                            if ($sstatus == '2' || $remaining == 0 || ($vatq['service_qty_done']!=0 && $vatq['service_qty_done']>=$apvalue['qty'])) {
                                                                                                echo "Done";
                                                                                            } else {
                                                                                                $ValidTill = $vatq['package_expiry'];
                                                                                                if ($date > $ValidTill) {
                                                                                                    echo "Service Expired";
                                                                                                } else {
                                                                                                    echo "Remaining";
                                                                                                }
                                                                                            }
                                                                                            ?>
                                                                                        </td>
                                                                                        <td style="display:none"><input type="hidden" name="packid" id="packid" value="<?= $packagess[$i] ?>" /></td>



                                                                                    </tr>
                                                                                    <?php
                                                                                }
                                                                            }
                                                                        }
                                                                    }



                                                                    $seldoffert = select("*", "tblAppointments", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                                    $VoucherID = $seldoffert[0]['VoucherID'];
                                                                    if ($VoucherID != '0') {
                                                                        $selp = select("*", "tblGiftVouchers", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                                        $GiftVoucherID = $selp[0]['GiftVoucherID'];
                                                                        $RedemptionCode = $selp[0]['RedemptionCode'];
                                                                        $Amount = $selp[0]['Amount'];
                                                                        $Status = $selp[0]['Status'];
                                                                        if ($RedemptionCode != "0") {
                                                                            if ($Status != '0') {
                                                                                $sub_total = $sub_total + $Amount;
                                                                            }
                                                                            //echo $RedemptionCode;
                                                                            ?>
                                                                            <tr>
                                                                                <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;"></td>
                                                                                <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; padding-left:2%;"><?= $RedemptionCode ?>&nbsp;&nbsp;Gift Voucher Code</td>
                                                                                <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; padding-left:2%;"></td>
                                                                                <td id="cost" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;"><?= $Amount ?>


                                                                            </tr>
                                                                            <?php
                                                                            // }
                                                                        }
                                                                    }
                                                                    ?>






<?php
                                                                                                                                                                                                                                                                        /* Show Super Gift Voucher */
                                                            $appoint_super_gv = select("*", "tblAppointments", "AppointmentID='" . DecodeQ($_GET['uid']) . "' AND super_gv_id!=0 AND super_gv_status=1");
                                                            $super_gv_data = select("*", "super_gift_voucher", "id='" . $appoint_super_gv[0]['super_gv_id'] . "'AND status=1");
                                                            if (isset($super_gv_data) && is_array($super_gv_data) && count($super_gv_data) > 0) {
                                                                $super_gv_amount = $super_gv_data[0]['GVAmount'];
                                                                $sub_total = $sub_total + $super_gv_amount;
                                                                ?>
                                                                        <tr>
                                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;">*</td>
                                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; padding-left:2%;"> Super Gift Voucher (<?php echo $super_gv_data[0]['GVcode']; ?>)</td>
                                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; padding-left:2%;">1</td>
                                                                            <td id="cost" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:left;">
                                                                                <input type="hidden" name="super_gv_id" value="<?= $super_gv_data[0]['id'] ?>" />
                                                                                <input type="hidden" name="super_gv_status" value="1" />
                                                                                <span><?php echo number_format($super_gv_amount, 2);
                                                                                ?></span>
                                                                            </td>
                                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;" class="no-print"></td>
                                                                        </tr>
                                                                        <?php }
                                                                    ?>

                                                                </tbody>

                                                            </table>
                                                        </td>


                                                    </tr>




                                                    <tr>
                                                        <td height="8">
                                                            <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td bgcolor="#e4e4e4" height="4"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">

                                                                <tbody>


<?php
$selecttotal = select("*", " tblInvoiceDetails", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
?>
                                                                    <tr>
                                                                        <td width="50%">&nbsp;</td>
                                                                        <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Sub Total</td>
                                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%;"><?php echo $selecttotal[0]['SubTotal']; ?>
                                                                        </td>
                                                                    </tr>

                                                                    <?php
                                                                    $package_dis = 0;
                                                                    if (isset($seldember[0]['PackageID']) && $seldember[0]['PackageID'] > 0) {
                                                                        $package_dis_data = select("*", "tblCustomerPackageAmt", "appointment_id='" . DecodeQ($_GET['uid']) . "' AND package_id=" . $seldember[0]['PackageID'] . " AND status=1");
                                                                        if (isset($package_dis_data[0]['package_amt_used']) && $package_dis_data[0]['package_amt_used'] > 0) {
                                                                            $package_dis = $package_dis_data[0]['package_amt_used'];
                                                                            ?>
                                                                            <tr>
                                                                                <td width="50%">&nbsp;</td>
                                                                                <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Package Discount</td>
                                                                                <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%;"><?php echo "-" . number_format($package_dis, 2); ?>
                                                                                </td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>

                                                                    <?php
                                                                    $seldember = select("*", "tblAppointments", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                                    $VoucherID = $seldember[0]['VoucherID'];


                                                                    $seldembert = select("Status", "tblGiftVouchers", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                                    //print_r($seldembert);
                                                                    $Status = $seldembert[0]['Status'];

                                                                    if ($Status == '0') {




                                                                        $selpt = select("*", "tblGiftVouchers", "AppointmentID='" . DecodeQ($_GET['uid']) . "' and Status='0'");
                                                                        //print_r($selpt);

                                                                        $amtt = $selpt[0]['Amount'];
                                                                        $id = $selpt[0]['GiftVoucherID'];


                                                                        $selptp = select("*", "tblGiftVouchers", "RedempedBy='" . DecodeQ($_GET['uid']) . "' and Status='1'");
                                                                        if ($selptp != '0') {

                                                                            $amttp = $selptp[0]['Amount'];
                                                                            $id = $selptp[0]['GiftVoucherID'];

                                                                            if ($amttp != "0") {
                                                                                if ($amtt > $sub_total) {
                                                                                    $sub_total = 0;
                                                                                } else {
                                                                                    $totalamt = $amtt - $amttp;
                                                                                    $sub_total = $sub_total + $totalamt;
                                                                                }
                                                                                ?>
                                                                                <tr>
                                                                                    <td width="50%">&nbsp;</td>
                                                                                    <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Redemption Gift Voucher Discount</td>
                                                                                    <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%;"><?php echo "-" . number_format($amttp, 2); ?>
                                                                                    </td>
                                                                                </tr>
                                                                                <?php
                                                                            } else {
                                                                                
                                                                            }
                                                                        }
                                                                        $sepcont = select("count(*)", "tblAppointmentsDetailsInvoice", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                                        $cntserp = $sepcont[0]['count(*)'];
                                                                        if ($cntserp > 0) {

                                                                            $sub_total = $sub_total + $amtt;
                                                                            ?>
                                                                            <tr>
                                                                                <td width="50%">&nbsp;</td>
                                                                                <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Gift Voucher Cost</td>
                                                                                <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%;"><?php echo "+" . number_format($amtt, 2); ?>
                                                                                </td>
                                                                            </tr>
                                                                            <?php
                                                                        } else {
                                                                            
                                                                        }
                                                                    } else {


                                                                        $selpt = select("*", "tblGiftVouchers", "RedempedBy='" . DecodeQ($_GET['uid']) . "' and Status='1'");

                                                                        $amtt = $selpt[0]['Amount'];
                                                                        $RedempedBy = $selpt[0]['RedempedBy'];
                                                                        $id = $selpt[0]['GiftVoucherID'];

                                                                        if ($amtt != '0') {
                                                                            if ($RedempedBy == DecodeQ($_GET['uid'])) {

                                                                                if ($amtt > $sub_total) {
                                                                                    $sub_total = 0;
                                                                                } else {
                                                                                    $sub_total = $sub_total - $amtt;
                                                                                }
                                                                                ?>
                                                                                <tr>
                                                                                    <td width="50%">&nbsp;</td>
                                                                                    <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Redemption Gift Voucher Discount</td>
                                                                                    <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="hidden" name="Redemptid"  Value="<?= $id ?>"/><input type="text" name="vouchercost" readonly value="<?php echo "-" . number_format($amtt, 2); ?>" />
                                                                                    </td>
                                                                                </tr>
                                                                                <?php
                                                                            }
                                                                        }

                                                                        $selpt = select("*", "tblGiftVouchers", "AppointmentID='" . DecodeQ($_GET['uid']) . "' and Status='0'");
                                                                        if ($selpt != '0') {
                                                                            $sepcont = select("count(*)", "tblAppointmentsDetailsInvoice", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                                            $cntserp = $sepcont[0]['count(*)'];
                                                                            if ($cntserp > 0) {
                                                                                $amttp = $selpt[0]['Amount'];
                                                                                $id = $selpt[0]['GiftVoucherID'];
                                                                                $sub_total = $sub_total + $amttp;
                                                                                ?>
                                                                                <tr>
                                                                                    <td width="50%">&nbsp;</td>
                                                                                    <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Gift Voucher Cost</td>
                                                                                    <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%;"><?php echo "+" . number_format($amttp, 2); ?>
                                                                                    </td>
                                                                                </tr>
                                                                                <?php
                                                                            } else {
                                                                                
                                                                            }
                                                                        } else {
                                                                            
                                                                        }
                                                                    }




                                                                    $seldember = select("*", "tblAppointments", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                                    $memid = $seldember[0]['memberid'];
                                                                    $custid = $seldember[0]['CustomerID'];
                                                                    $seldemberg = select("*", "tblMembership", "MembershipID='" . $memid . "'");
                                                                    $Cost = $seldemberg[0]['Cost'];

                                                                    $selcust = select("*", "tblCustomers", "CustomerID='" . $custid . "'");
                                                                    $memberflag = $selcust[0]['memberflag'];
                                                                    $cust_name = $selcust[0]['CustomerFullName'];
                                                                    $selcustd = select("Membership_Amount", "tblInvoiceDetails", "CustomerFullName='" . $cust_name . "' and AppointmentId='" . DecodeQ($_GET['uid']) . "'");
                                                                    $Membership_Amount = $selcustd[0]['Membership_Amount'];
                                                                    if ($Membership_Amount != "") {
                                                                        $sub_total = $sub_total + $Cost;
                                                                        ?>
                                                                        <tr>
                                                                            <td width="50%">&nbsp;</td>
                                                                            <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;"><?= $membershipname ?> Amount</td>
                                                                            <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%;"><?php echo "+" . number_format($Cost, 2); ?>
                                                                            </td>
                                                                        </tr>
                                                                        <?php
                                                                    }




/////////////////////////////////////////////////////
                                                                    $seldoffer = select("*", "tblAppointments", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                                    $offerid = $seldoffer[0]['offerid'];
                                                                    if ($offerid != "0") {
                                                                        ?>

                                                                        <tr>
                                                                            <td width="50%">&nbsp;</td>


                                                                            <?php
                                                                            $seldoffer = select("*", "tblAppointments", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                                            $offerid = $seldoffer[0]['offerid'];
                                                                            $StoreIDd = $seldoffer[0]['StoreID'];
                                                                            $memid = $seldoffer[0]['memberid'];

                                                                            $seldofferp = select("*", "tblOffers", "OfferID='" . $offerid . "'");
                                                                            $services = $seldofferp[0]['ServiceID'];
                                                                            $offernamee = $seldofferp[0]['OfferName'];
                                                                            $baseamt = $seldofferp[0]['BaseAmount'];
                                                                            $Type = $seldofferp[0]['Type'];
                                                                            $TypeAmount = $seldofferp[0]['TypeAmount'];
                                                                            $StoreID = $seldofferp[0]['StoreID'];
                                                                            $stores = explode(",", $StoreID);

                                                                            $servicessf = explode(",", $services);
                                                                            // if (in_array("$StoreIDd", $stores)) {
                                                                            $statuscheck = "No";
                                                                            foreach ($seldpdept as $val) {

                                                                                $totalammt = 0;
                                                                                $serviceid = $val['ServiceID'];
                                                                                $AppointmentDetailsID = $val['AppointmentDetailsID'];
                                                                                $AppointmentID = $val['AppointmentID'];

                                                                                //if (in_array("$serviceid", $servicessf)) {


                                                                                $sqp = select("*", "tblAppointmentsDetailsInvoice", "ServiceID='" . $serviceid . "' and AppointmentID='" . $AppointmentID . "'");

                                                                                $amtt = $sqp[0]['ServiceAmount'];


                                                                                $qtyyy = $sqp[0]['qty'];
                                                                                $totals = $qtyyy * $amtt;
                                                                                $totalpp = $totalpp + $totals;
                                                                                if ($baseamt != "") {

                                                                                    if ($totalpp >= $baseamt) {
                                                                                        //echo 1;
                                                                                        if ($Type == '1') {
                                                                                            if ($statuscheck == "No") {
                                                                                                $servicefinal = $serviceid;
                                                                                                //$offeramtt=$totalpp-$TypeAmount;
                                                                                                $offeramtt = $TypeAmount;
                                                                                                $statuscheck = "Yes";
                                                                                            } else {
                                                                                                
                                                                                            }
                                                                                        } else {
                                                                                            $amt = $totals * $TypeAmount / 100;

                                                                                            $offeramtt = $amt;

                                                                                            $offeramount = $offeramount + $offeramtt;
                                                                                        }
                                                                                    } else {
                                                                                        $data = "Offer Not Applicable Offer Amount is Less Than Billing Amount";
                                                                                    }
                                                                                } else {
                                                                                    if ($Type == '1') {
                                                                                        if ($statuscheck == "No") {
                                                                                            $servicefinal = $serviceid;
                                                                                            //$offeramtt=$totalpp-$TypeAmount;
                                                                                            $offeramtt = $TypeAmount;
                                                                                            $statuscheck = "Yes";
                                                                                        }
                                                                                    } else {
                                                                                        $amt = $totals * $TypeAmount / 100;

                                                                                        $offeramtt = $amt;

                                                                                        $offeramount = $offeramount + $offeramtt;
                                                                                    }
                                                                                }
                                                                                ?>
                                                                        <input type="hidden" name="serviceido[]" id="serviceido" value="<?= $serviceid ?>" />
                                                                        <input type="hidden" name="offeramttt[]" id="offeramttt" value="<?= $offeramtt ?>" />
                                                                        <input type="hidden" name="offerid[]" id="offerid" value="<?= $offerid ?>" />
                                                                        <?php
                                                                        /*  } else {
                                                                          $data = "Offer Not Applicable For Some Of These Services";
                                                                          } */
                                                                    }
                                                                    /* } else {
                                                                      $data = "This Offer Is Not Valid For This Store";
                                                                      } */





                                                                    //print_r($servicessf);

                                                                    if ($offeramtt != "") {
                                                                        if ($Type == "1") {
                                                                            $offeramtt = $offeramtt;
                                                                        } else {
                                                                            $offeramtt = $offeramount;
                                                                        }
                                                                        ?>
                                                                        <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">
                                                                            <?= $offernamee ?></td>
                                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%;">
                                                                            <?= "-" . $offeramtt ?></td>
                                                                        <?php
                                                                    }
                                                                    ?>


                                                        </tr>
                                                        <?php
                                                    }


///////////////////////////////////////////////////////////////
////////////////Member Discount///////////////////////

                                                    $seldember = select("*", "tblAppointments", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                    $memid = $seldember[0]['memberid'];
                                                    $offerid = $seldember[0]['offerid'];
                                                    $custid = $seldember[0]['CustomerID'];
                                                    $seldemberg = select("*", "tblMembership", "MembershipID='" . $memid . "'");
                                                    $Cost = $seldemberg[0]['Cost'];

                                                    $selcust = select("*", "tblCustomers", "CustomerID='" . $custid . "'");
                                                    $memberflag = $selcust[0]['memberflag'];

                                                    $seldofferp = select("*", "tblOffers", "OfferID='" . $offerid . "'");
                                                    $services = $seldofferp[0]['ServiceID'];
                                                    $baseamt = $seldofferp[0]['BaseAmount'];
                                                    $Type = $seldofferp[0]['Type'];
                                                    $TypeAmount = $seldofferp[0]['TypeAmount'];
                                                    $StoreID = $seldofferp[0]['StoreID'];
                                                    $stores = explode(",", $StoreID);


                                                    if ($memid != "0" && number_format($memberdis, 2) > 0) {
                                                        ?>
                                                        <tr>
                                                            <td width="50%">&nbsp;</td>
                                                            <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;"><?= $membershipname ?>&nbsp;Discount</td>
                                                            <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%;"><?php echo "-" . number_format($memberdis, 2); ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }


                                                        $appointment_data = select("*", "tblAppointments", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                        
                                                        if (isset($appointment_data[0]['super_gv_status']) && $appointment_data[0]['super_gv_status'] == 2 && $appointment_data[0]['super_gv_id'] > 0) {
                                                            $super_gv_data = select("*", "super_gift_voucher", "id='" . $appointment_data[0]['super_gv_id'] . "'AND status=1");
                                                            if (isset($super_gv_data) && is_array($super_gv_data) && count($super_gv_data) > 0) {
                                                                $sub_total = $sub_total - $appointment_data[0]['super_gv_amount'];
                                                                ?>
                                                            <tr>
															<td width="50%">&nbsp;</td>
                                                            <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Super Gift Voucher(<?php echo $super_gv_data[0]['GVcode']; ?>) Discount</td>
                                                            <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold;text-align:right; padding-right:05%;"><?php echo " - " . number_format($appointment_data[0]['super_gv_amount'], 2); ?></td>
                                                        </tr>
														
                                                                
                                                            <?php
                                                        }
                                                    }


///////////////////////////////////////////////////////////////// 
                                                    $seldoffert = select("*", "tblAppointments", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");

                                                    $FreeService = $seldoffert[0]['FreeService'];
                                                    if ($FreeService != "0") {
                                                        
                                                    } else {
                                                        $strChargeAmountDetails = $seldoffert[0]['total_tax'];
                                                        $amountdetail = $amountdetail + $strChargeAmountDetails;
                                                        ?>

                                                        <tr>
                                                            <td width="50%">&nbsp;</td>
                                                            <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">CGST @ 9%</td>
                                                            <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold;text-align:right; padding-right:05%;"><?= "+" . $strChargeAmountDetails / 2 ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="50%">&nbsp;</td>
                                                            <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">SGST @ 9%</td>
                                                            <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold;text-align:right; padding-right:05%;"><?= "+" . $strChargeAmountDetails / 2 ?></td>
                                                        </tr>
                                                        <?php
                                                    }



                                                    //print_r($value);
                                                    //echo $value['AppointmentDetailsID'];

                                                    $total = 0;
                                                    //echo 
                                                    if ($amtt > $sub_total) {
                                                        $total = $total + $sub_total;
                                                    } else {

                                                        $total = $total + $sub_total + $amountdetail - $offeramtt - $memberdis - $package_dis;
                                                    }
                                                    if ($total < 0) {
                                                        $total = 0;
                                                    }
                                                    ?>
                                                    </tr>
                                                    <?php
                                                    $sept = select("PendingAmount,pending_amt_apt_id", "tblInvoiceDetails", "AppointmentId='" . DecodeQ($_GET['uid']) . "'");
                                                    $pendamt = $sept[0]['PendingAmount'];
                                                    if ($pendamt != "0" && $pendamt != "") {
                                                        $total = $total + $pendamt;
                                                        $pending_apt_id = $sept[0]['pending_amt_apt_id'];
                                                        /*
                                                         * get pending amount invoice details
                                                         */
                                                        if ($pending_apt_id != '' && $pending_apt_id != NULL) {
                                                            $pending_date_data = select("AppointmentID,AppointmentDate", "tblAppointments", "AppointmentId='" . $pending_apt_id . "'");
                                                            if (isset($pending_date_data) && is_array($pending_date_data) && count($pending_date_data) > 0) {
                                                                $pending_amt_date = $pending_date_data[0]['AppointmentDate'];
                                                            }
                                                        }
                                                        ?>
                                                        <tr id="pendingpayment"> 
                                                            <td>&nbsp;</td>
                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Pending Amount
                                                                <b><?php echo isset($pending_amt_date) ? '(Invoice Date: ' . $pending_amt_date . ')' : ''; ?> </b>
                                                            </td>
                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%" ><?= $pendamt ?></td>

                                                        </tr>
                                                        <?php
                                                    }
                                                    $seldp = select("*", "tblInvoiceDetails", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");

                                                    ?>

                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Total</td>
                                                        <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%" id="totalvalue"><?php echo isset($seldp[0]['Total']) ? $seldp[0]['Total'] :'0'  ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Round Off</td>
                                                        <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%;"><?php echo isset($seldp[0]['RoundTotal']) ? $seldp[0]['RoundTotal'] :'0'  ?></td>
                                                    </tr>

                                                </tbody>

                                            </table>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td height="8">
                                            <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                                <tbody>
                                                    <tr>
                                                        <td bgcolor="#e4e4e4" height="4"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>


                                    <?php
                                    $apt_paid_data = select("*", "tblAppointments", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                    $prepaid_amount = isset($apt_paid_data[0]['prepaid_amount']) ? $apt_paid_data[0]['prepaid_amount'] : 0;

                                    if ($prepaid_amount > 0) {
                                        $total = $total - $prepaid_amount;
                                        ?>

                                        <tr>
                                            <td id="payment">
                                                <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">


                                                    <tbody>
                                                        <tr>
                                                            <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                            <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Online Paid Amount</td>
                                                            <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%">-<?= number_format($prepaid_amount, 2) ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    <?php }
                                    ?>

                                    <?php
                                    $seldppay = select("*", "tblInvoiceDetails", "AppointmentId='" . DecodeQ($_GET['uid']) . "'");
                                    $flag = $seldppay[0]['Flag'];

                                    $seldppayP = select("*", "tblInvoiceDetails", "AppointmentId='" . DecodeQ($_GET['uid']) . "' AND Flag='" . $flag . "'");
                                    $amount = $seldppayP[0]['CashAmount'];



                                    $seldppayPt = select("*", "tblPendingPayments", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                    $id = $seldppayPt[0]['PendingPaymentID'];
                                    $pendingamt = $seldppayPt[0]['PendingAmount'];
                                    $paidamt = $seldppayPt[0]['PaidAmount'];
                                    if ($flag == 'CS') {
                                        $seldppayP = select("*", "tblInvoiceDetails", "AppointmentId='" . DecodeQ($_GET['uid']) . "' AND Flag='" . $flag . "'");
                                        $amount = $seldppayP[0]['CashAmount'];
                                        if ($prepaid_amount > 0) {
                                            $amount = $amount - $prepaid_amount;
                                        }
                                        //print_r($seldppayP);



                                        if ($id != "") {
                                            ?>
                                            <tr>
                                                <td id="payment">
                                                    <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">


                                                        <tbody>


                                                            <?php
                                                            if ($paidamt != "" && $pendingamt != "" && $paidamt != "0" && $pendingamt != "0") {
                                                                ?>
                                                                <tr>
                                                                    <td colspan="3" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:16px;FONT-WEIGHT:bold; text-align:center;">Payments</td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                    <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Paid Amount</td>
                                                                    <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%"><?php echo number_format($paidamt, 2) ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                    <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Pending Amount</td>
                                                                    <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%"><?php echo number_format($pendingamt, 2) ?></td>
                                                                </tr>

                                                                <?php
                                                            } else {

                                                                if ($pendingamt != "" && $pendingamt != "0") {
                                                                    ?>
                                                                    <tr>
                                                                        <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                        <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Pending Amount</td>
                                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%"><?php echo number_format($pendingamt, 2) ?></td>
                                                                    </tr>
                                                                    <?php
                                                                } elseif ($paidamt != "" && $paidamt != "0") {
                                                                    ?>
                                                                    <tr>
                                                                        <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                        <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Paid Amount</td>
                                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%"><?php echo number_format($paidamt, 2) ?></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </tbody>

                                                    </table>
                                                </td>
                                            </tr>
                                            <?php
                                        } else {
                                            ?>
                                            <tr>
                                                <td id="payment">
                                                    <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">


                                                        <tbody>
                                                            <tr>
                                                                <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Cash</td>
                                                                <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%"><?php echo number_format($amount, 2) ?></td>
                                                            </tr>
                                                        </tbody>

                                                    </table>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>







                                        <?php
                                    } elseif ($flag == 'H') {
                                        $seldppayP = select("*", "tblInvoiceDetails", "AppointmentId='" . DecodeQ($_GET['uid']) . "' AND Flag='" . $flag . "'");
                                        $amount = $seldppayP[0]['CashAmount'];
                                        if ($prepaid_amount > 0) {
                                            $amount = $amount - $prepaid_amount;
                                        }
                                        ?>
                                        <tr>
                                            <td id="payment">
                                                <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">


                                                    <tbody>


                                                        <?php
                                                        if ($id != "") {
                                                            ?>
                                                            <tr>
                                                                <td id="payment">
                                                                    <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">


                                                                        <tbody>


                                                                            <?php
                                                                            if ($paidamt != "" && $pendingamt != "" && $paidamt != "0" && $pendingamt != "0") {
                                                                                ?>
                                                                                <tr>
                                                                                    <td colspan="3" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:16px;FONT-WEIGHT:bold; text-align:center;">Payments</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                                    <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Paid Amount</td>
                                                                                    <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%"><?php echo number_format($paidamt, 2) ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                                    <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Pending Amount</td>
                                                                                    <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%"><?php echo number_format($pendingamt, 2) ?></td>
                                                                                </tr>

                                                                                <?php
                                                                            } else {

                                                                                if ($pendingamt != "" && $pendingamt != "0") {
                                                                                    ?>
                                                                                    <tr>
                                                                                        <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                                        <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Pending Amount</td>
                                                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%"><?php echo number_format($pendingamt, 2) ?></td>
                                                                                    </tr>
                                                                                    <?php
                                                                                } elseif ($paidamt != "" && $paidamt != "0") {
                                                                                    ?>
                                                                                    <tr>
                                                                                        <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                                        <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Paid Amount</td>
                                                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%"><?php echo number_format($paidamt, 2) ?></td>
                                                                                    </tr>
                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </tbody>

                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <tr>
                                                                <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Pending Amount</td>
                                                                <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><?php echo number_format($amount, 2) ?></td>
                                                            </tr>

                                                            <?php
                                                        }
                                                        ?>



                                                    </tbody>

                                                </table>
                                            </td>
                                        </tr>
                                        <?php
                                    } elseif ($flag == 'C') {
                                        $seldppayP = select("*", "tblInvoiceDetails", "AppointmentId='" . DecodeQ($_GET['uid']) . "' and Flag='" . $flag . "'");
                                        $amount = $seldppayP[0]['CardAmount'];
                                        if ($prepaid_amount > 0) {
                                            $amount = $amount - $prepaid_amount;
                                        }
                                        ?>
                                        <tr>
                                            <td id="payment">
                                                <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">


                                                    <tbody>


                                                        <?php
                                                        if ($id != "") {

                                                            if ($paidamt != "" && $pendingamt != "" && $paidamt != "0" && $pendingamt != "0") {
                                                                ?>
                                                                <tr>
                                                                    <td colspan="3" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:16px;FONT-WEIGHT:bold; text-align:center;">Payments</td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                    <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Paid Amount</td>
                                                                    <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%"><?php echo number_format($paidamt, 2) ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                    <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Pending Amount</td>
                                                                    <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%"><?php echo number_format($pendingamt, 2) ?></td>
                                                                </tr>

                                                                <?php
                                                            } else {

                                                                if ($pendingamt != "" && $pendingamt != "0") {
                                                                    ?>
                                                                    <tr>
                                                                        <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                        <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Pending Amount</td>
                                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%"><?php echo number_format($pendingamt, 2) ?></td>
                                                                    </tr>
                                                                    <?php
                                                                } elseif ($paidamt != "" && $paidamt != "0") {
                                                                    ?>
                                                                    <tr>
                                                                        <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                        <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Paid Amount</td>
                                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%"><?php echo number_format($paidamt, 2) ?></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>

                                                            <?php
                                                        } else {
                                                            ?>
                                                            <tr>
                                                                <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Card</td>
                                                                <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><?php echo number_format($amount, 2) ?></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>


                                                    </tbody>

                                                </table>
                                            </td>
                                        </tr>
                                        <?php
                                    } elseif ($flag == 'BOTH') {

                                        $seldppayP = select("*", "tblInvoiceDetails", "AppointmentId='" . DecodeQ($_GET['uid']) . "' and Flag='" . $flag . "'");
                                        //print_r($seldppayP);
                                        $cashamount = $seldppayP[0]['CashAmount'];
                                        $cardamount = $seldppayP[0]['CardAmount'];
                                        ?>
                                        <tr>
                                            <td id="payment">
                                                <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">


                                                    <tbody>

                                                        <tr>
                                                            <td colspan="3" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:16px;FONT-WEIGHT:bold; text-align:center;">Payments</td>
                                                        </tr>
                                                        <?php
                                                        if ($id != "") {

                                                            if ($paidamt != "" && $pendingamt != "" && $paidamt != "0" && $pendingamt != "0") {
                                                                ?>
                                                                <tr>
                                                                    <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                    <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Paid Amount</td>
                                                                    <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%"><?php echo number_format($paidamt, 2) ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                    <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Pending Amount</td>
                                                                    <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%"><?php echo number_format($pendingamt, 2) ?></td>
                                                                </tr>


                                                                <tr>
                                                                    <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                    <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Cash Amount</td>
                                                                    <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%"><?php echo number_format($cashamount, 2) ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                    <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Card Amount</td>
                                                                    <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%"><?php echo number_format($cardamount, 2) ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>&nbsp;</td>
                                                                    <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Total Payment</td>
                                                                    <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right;padding-right:05%" ><?php echo number_format($total, 2); ?></td>
                                                                </tr>

                                                                <?php
                                                            } else {

                                                                if ($pendingamt != "" && $pendingamt != "0") {
                                                                    ?>
                                                                    <tr>
                                                                        <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                        <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Pending Amount</td>
                                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%"><?php echo number_format($pendingamt, 2) ?></td>
                                                                    </tr>
                                                                    <?php
                                                                } elseif ($paidamt != "" && $paidamt != "0") {
                                                                    ?>
                                                                    <tr>
                                                                        <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                        <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Paid Amount</td>
                                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%"><?php echo number_format($paidamt, 2) ?></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                                ?>

                                                                <tr>
                                                                    <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                    <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Cash</td>
                                                                    <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%"><?php echo number_format($cashamount, 2) ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                    <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Card</td>
                                                                    <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%"><?php echo number_format($cardamount, 2) ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>&nbsp;</td>
                                                                    <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Total Payment</td>
                                                                    <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right;padding-right:05%" ><?php echo number_format($total, 2); ?></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        } else {
                                                            ?>

                                                            <tr>
                                                                <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Cash</td>
                                                                <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%"><?php echo number_format($cashamount, 2) ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Card</td>
                                                                <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%"><?php echo number_format($cardamount, 2) ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Total Payment</td>
                                                                <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right;padding-right:05%" ><?php echo number_format($total, 2); ?></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>

                                                    </tbody>

                                                </table>
                                            </td>
                                        </tr>

                                        <?php
                                    } else {
                                        if ($prepaid_amount > 0) {
                                            $total = $total - $prepaid_amount;
                                        }
                                        ?>
                                        <tr>
                                            <td style="display:none" id="payment">
                                                <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">

                                                    <tbody>

                                                        <tr>
                                                            <td colspan="3" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:16px;FONT-WEIGHT:bold; text-align:center;">Payments</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                            <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Cash</td>
                                                            <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%;"><input type="number" id="paymentid" name="cashamt" value="<?php echo $total; ?>" onkeyup="test()"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Total Payment</td>
                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;" ><input id="totalpayment" name="totalpayment"/></td>
                                                        </tr>


                                                    </tbody>

                                                </table>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>

                                    <tr>
                                        <td colspan="2" style="padding: 0px;background: #3a020a;">
                                            <table width="100%" cellspacing="0" cellpadding="0">
                                                <tbody><tr>
                                                        <td colspan="2" style="background-color: #3a020a;padding: 15px 0;text-align: center;color: #fff;">
                                                            <p class="text-center" style="font-size:14px;"></p>
                                                            <p style="color:#dcc36f;">11+ LOCATIONS | T1 & T2 MUMBAI AIRPORT &nbsp;| MUMBAI | KOCHI </p>
                                                            <p style="color: #dcc36f; text-align:center;"><strong>GO GREEN GO PAPERLESS</strong></p>

                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</tbody>
</table>

</div>
</body>

</html>


<table align="right">
    <tr align="right">

        <!--<a href="appointment_invoice.php" class="btn btn-primary btn-sm " style="float: right;">Print</a>-->
        <?php
        $seldp = select("*", "tblAppointments", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
        $STORED = $seldp[0]['StoreID'];
        ?>
        <?php
        if ($_SERVER['REMOTE_ADDR'] == "111.119.219.70") {
            ?>
            <td align="right"><button onclick="sendmail()" class="btn btn-success">Send Email to Client<div class="ripple-wrapper"></div></button></td>
            <?php
        }
        ?>
        <td align="right">
            <!--<a href="appointment_invoice.php" class="btn btn-primary btn-sm " style="float: right;">Print</a>-->
            <button onclick="printDiv('printarea')" class="btn btn-blue-alt">Print<div class="ripple-wrapper"></div></button>
        </td>

        <td align="right">
            <a href="appointment_invoice.php" class="btn btn-primary btn-sm" style="float: right;">Go Back</a>
        </td>
        <td align="right">
            <?php //require_once("incBillingSMS.php");     ?>
        </td>
    </tr>
</table>




<!--####################Invoice to be sent #############################-->


<span id="printareasaifu">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" width="100%">
        <tbody>
                <!--<tr>
                        <td height="10"></td>
                </tr>-->
            <?php
            $seldp = select("*", "tblAppointments", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
            $seldpd = select("StoreName", "tblStores", "StoreID='" . $seldp[0]['StoreID'] . "'");
            $seldpde = select("InvoiceID", "tblInvoice", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
            $seldpdep = select("*", "tblCustomers", "CustomerID='" . $seldp[0]['CustomerID'] . "'");

            $timestamp = date("H:i:s", time());
//sqlUpdate1 = "UPDATE tblAppointments SET AppointmentCheckOutTime = '".$timestamp."', Status = '2' WHERE AppointmentID='".DecodeQ($_GET['uid'])."'";
//$passingID = EncodeQ(DecodeQ($passingID1));
//ExecuteNQ($sqlUpdate1);

            $seldpdeptp = select("*", " tblAppointmentsDetailsInvoice", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
            foreach ($seldpdeptp as $ty) {
                $totalservices = $ty['ServiceID'];
                $seldpdepp = select("MECID", "tblAppointmentAssignEmployee", "ServiceID='" . $totalservices . "' and AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                //print_r($seldpdepp);
            }

            $appp_id = DecodeQ($_GET['uid']);
            ?>
            <tr>
                <td>					
                    <table class="one" style="font-family: 'Actor', sans-serif; margin: 0 auto; background: url('http://pos.nailspaexperience.com/admin/images/invoice/header1.png'); height: 110px; background-size: cover; color: #807e7f;"width="900">
                        <tr>
                            <td width="20%">&nbsp;</td>
                            <?php
                            $selSettingsData = select("*", "tblSettings", "SettingID='1'");

                            $selStoreGST = select("GSTNo, AccountingCode, CompanyName", "tblStores", "StoreID='" . $seldp[0]['StoreID'] . "'");
                            if ($selStoreGST[0]['GSTNo'] == "" || $selStoreGST[0]['GSTNo'] == " " || $selStoreGST[0]['GSTNo'] == "0") {
                                $strGSTno = $selSettingsData[0]['MasterGSTNo'];
                            } else {
                                $strGSTno = $selStoreGST[0]['GSTNo'];
                            }

                            if ($selStoreGST[0]['CompanyName'] == "" || $selStoreGST[0]['CompanyName'] == " " || $selStoreGST[0]['CompanyName'] == "0") {
                                $strCompanyName = $selSettingsData[0]['CompanyName'];
                            } else {
                                $strCompanyName = $selStoreGST[0]['CompanyName'];
                            }
                            ?>
                            <td width="50%"><?= $strCompanyName ?><br><br><span class="fs">Maharashtra &bull; GSTIN - <?php echo $strGSTno; ?></span></td>
                            <td width="40%" align="center">
                                <table style="border-top:1px solid #7d7b7c;border-bottom:1px solid #7d7b7c;"><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Customer Copy &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr></table>
                                <?php echo $seldpd[0]['StoreName']; ?><br>
                                <?php echo $seldp[0]['AppointmentDate']; ?> - <?php echo $seldp[0]['AppointmentCheckOutTime']; ?><br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="two" width="900" style="font-family: 'Actor', sans-serif; padding: 15px; margin: 0 auto; color: #807e7f;">
                        <tr>
                            <td width="30%"><?php echo $seldpdep[0]['CustomerFullName']; ?><br><?php echo $seldpdep[0]['CustomerMobileNo'] ?><br> <?php echo $seldpdep[0]['CustomerEmailID'] ?><br><span class="es" style="font-style: italic;">Membership no: <?php
                                if ($seldp[0]['memberid'] != '0') {
                                    echo $seldp[0]['memberid'];
                                } else {
                                    echo "-";
                                };
                                ?></span><br><br><span class="fs">Maharashtra &bull; GSTIN:"Unregistered"</span></td>


                        <input type="hidden" id="email" value="<?php echo $seldpdep[0]['CustomerEmailID'] ?>" />
                        <!-- <input type="hidden" id="email" value="dhrumilupadhyay28@gmail.com" /> -->
                        <input type="hidden" name="app_id" id="app_id" value="<?= DecodeQ($_GET['uid']) ?>" />


                        <td width="45%"></td>

                        <?php
                        $seldpdeppt = select("MECID", " tblAppointmentAssignEmployee", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                        foreach ($seldpdeppt as $vap) {
                            $empname = $vap['MECID'];
                            if ($empname != "0") {
                                $emppp = $emppp . ',' . $empname;
                            }
                        }
                        ?>
                        <td width="25%"><br>Invoice No:<?php echo $seldpde[0]['InvoiceID']; ?><br><br>Stylist: 
                            <?php
                            if ($emppp == "") {
                                echo "-";
                            } elseif ($emppp == "0") {
                                
                            } else {
                                echo trim($emppp, ",");
                            }
                            ?>
                        </td>
            </tr>
    </table>
</td>
</tr>
<tr>
    <td>
        <table class="two" width="900" style="font-family: 'Actor', sans-serif; padding: 15px; margin: 0 auto; color: #807e7f; border-top:1px  solid #ccc;  border-bottom:1px solid #ccc;">
            <?php
            $seldpdept = select("*", " tblAppointmentsDetailsInvoice", "AppointmentID='" . DecodeQ($_GET['uid']) . "' and PackageService='0'");
            $sub_total = 0;
            $countsf = "0";
            $countersaif = "";
            $counterusmani = "1";
            foreach ($seldpdept as $val) {
                $countersaif ++;
                $countsf++;
                $counterusmani = $counterusmani + 1;
                $totalammt = 0;
                $AppointmentDetailsID = $val['AppointmentDetailsID'];
                $servicee = select("*", "tblServices", "ServiceID='" . $val['ServiceID'] . "'");
                $qtyyy = $val['qty'];
                $amtt = $val['ServiceAmount'];
                $quantity = $val['qty'];
                $totalammt = $qtyyy * $amtt;
                $total = 0;
                ?>

                <tr>
                    <td width="35%"><?php echo $servicee[0]['ServiceName']; ?></td>
                    <td width="20%" align="right" style="padding-right: 30px;">
                        <?php
                        //Stylist
                        $stylistID = select("*", "tblAppointmentAssignEmployee", "AppointmentID='" . DecodeQ($_GET['uid']) . "' and ServiceID='" . $val['ServiceID'] . "' group by MECID");
                        foreach ($stylistID as $strstylist) {
                            $stylistName = select("*", "tblEmployees", "EID='" . $strstylist['MECID'] . "'");
                            echo $stylistName[0]['EmployeeName'] . '<br>';
                        }
                        ?>
                    </td>
                    <td width="5%">&nbsp;</td>
                    <td width="20%"><?php echo $quantity; ?></td>
                    <td width="20%" align="center"><?php echo $totalammt . ".00"; ?></td>
                </tr>



                <?php
            }
            ?>	
        </table>
    </td>
</tr>		


<tr>
    <td>
        <?php
        $selecttotal = select("*", " tblInvoiceDetails", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
        ?>


        <table class="three" width="900" style="font-family: 'Actor', sans-serif; background: url('http://pos.nailspaexperience.com/admin/images/invoice/middle.png'); background-size: contain; padding: 15px; margin: 0 auto; background-repeat: no-repeat; color: #807e7f;">
            <tr>
                <td width="25%">&nbsp;</td>
                <td width="25%">&nbsp;</td>
                <td width="10%">&nbsp;</td>
                <td width="20%" align="right">Sub Total</td>
                <td width="20%" align="center"><?php echo $selecttotal[0]['SubTotal']; ?></td>
            </tr>

            <?php
// Purchase memberhship amount		
            $seldember = select("*", "tblAppointments", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
            $memid = $seldember[0]['memberid'];
            $custid = $seldember[0]['CustomerID'];
            $getOfferID = $seldember[0]['offerid'];
            $seldemberg = select("*", "tblMembership", "MembershipID='" . $memid . "'");
            $Cost = $seldemberg[0]['Cost'];

            $selcust = select("*", "tblCustomers", "CustomerID='" . $custid . "'");
            $memberflag = $selcust[0]['memberflag'];
            $cust_name = $selcust[0]['CustomerFullName'];
            $selcustd = select("Membership_Amount", "tblInvoiceDetails", "CustomerFullName='" . $cust_name . "' and AppointmentId='" . DecodeQ($_GET['uid']) . "'");
            $Membership_Amount = $selcustd[0]['Membership_Amount'];
            if ($Membership_Amount != "") {
                $sub_total = $sub_total + $Cost;
                ?>
                <tr>
                    <td width="25%">&nbsp;</td>
                    <td width="25%">&nbsp;</td>
                    <td width="10%">&nbsp;</td>
                    <td width="20" align="right"><?= $seldemberg[0]['MembershipName']; ?> Membership Cost</td>
                    <td width="20%" align="center"><?php echo "" . number_format($Cost, 2); ?></td>
                </tr>
                <?php
            }
            ?>


            <?php
////////////////Member Discount///////////////////////
            if ($memid != "0") {
                $MembershipDiscountAmount = explode(",", $selecttotal[0]['DisAmt']);
                foreach ($MembershipDiscountAmount as $MemAmt) {
                    $strMembershipAmount += $MemAmt;
                }
                ?>
                <tr>
                    <td width="25%">&nbsp;</td>
                    <td width="25%">&nbsp;</td>
                    <td width="10%">&nbsp;</td>
                    <td width="20" align="right">Membership Discount</td>
                    <td width="20%" align="center"><?php echo "" . number_format($strMembershipAmount, 2); ?></td>
                </tr>
                <?php
                // Savings if Membership
                $strSavings = $strMembershipAmount;
            }
            ?>



            <?php
////////////////Offer Discount///////////////////////
            if ($getOfferID != "0") {
                $OfferDiscountdisplay = str_replace("-", "", $selecttotal[0]['OfferAmt']);
                ?>
                <tr>
                    <td width="25%">&nbsp;</td>
                    <td width="25%">&nbsp;</td>
                    <td width="10%">&nbsp;</td>
                    <td width="20" align="right">Offer Discount</td>
                    <td width="20%" align="center"><?php echo number_format($OfferDiscountdisplay, 2); ?></td>
                </tr>
                <?php
                // Savings if Offer
                $strSavings = $OfferDiscountdisplay;
            }
            ?>

<!--<tr>
        <td width="25%">&nbsp;</td>
        <td width="25%">&nbsp;</td>
        <td width="10%">&nbsp;</td>
        <td width="20%" align="right">Discount</td>
        <td width="20%" align="center">578001.00</td>
</tr>-->

            <?php /*
              if ($selecttotal[0]['ChargeName'] == "GST") {
              $gst = $selecttotal[0]['ChargeAmount'];
              $sgst = str_replace("+", "", $gst) / 2;
              $cgst = str_replace("+", "", $gst) / 2;
              ?>
              <tr>
              <td width="25%">&nbsp;</td>
              <td width="25%">&nbsp;</td>
              <td width="10%">&nbsp;</td>
              <td width="20%" align="right">CGST @ 9%</td>
              <td width="20%" align="center"><?php echo $cgst ?></td>
              </tr>
              <tr>
              <td width="25%">&nbsp;</td>
              <td width="25%">&nbsp;</td>
              <td width="10%">&nbsp;</td>
              <td width="20%" align="right">SGST @ 9%</td>
              <td width="20%" align="center"><?php echo $sgst ?></td>
              </tr>
              <?php } */ ?>

            <?php
            $sqlExtraCharges = select("DISTINCT (ChargeName), SUM( ChargeAmount ) AS Sumarize", "tblAppointmentsChargesInvoice", "AppointmentID ='" . DecodeQ($_GET['uid']) . "' AND AppointmentDetailsID!=0 GROUP BY ChargeName");
            //print_r($sqlExtraCharges);

            foreach ($sqlExtraCharges as $vaqq) {
                $strChargeNameDetails = $vaqq["ChargeName"];
                $strChargeAmountDetails = $vaqq["Sumarize"];
                ?>
                <?php if ($strChargeNameDetails == 'GST') { ?>
                    <tr>
                        <td width="25%">&nbsp;</td>
                        <td width="25%">&nbsp;</td>
                        <td width="10%">&nbsp;</td>
                        <td width="20%" align="right">CGST @ 9%</td>
                        <td width="20%" align="center"><?= "+" . $strChargeAmountDetails / 2 ?></td>
                    </tr>
                    <tr>
                        <td width="25%">&nbsp;</td>
                        <td width="25%">&nbsp;</td>
                        <td width="10%">&nbsp;</td>
                        <td width="20%" align="right">SGST @ 9%</td>
                        <td width="20%" align="center"><?= "+" . $strChargeAmountDetails / 2 ?></td>
                    </tr>
                <?php } else { ?>
                    <tr>
                        <td width="25%">&nbsp;</td>
                        <td width="25%">&nbsp;</td>
                        <td width="10%">&nbsp;</td>
                        <td width="20%" align="right"><?= $strChargeNameDetails ?></td>
                        <td width="20%" align="center"><?= "+" . $strChargeAmountDetails; ?></td>
                    </tr>
                    <?php
                }
            }
            ?>

            <tr>
                <td width="25%">&nbsp;</td>
                <td width="25%">&nbsp;</td>
                <td width="10%">&nbsp;</td>
                <td width="20%" align="right">Total Payment</td>
                <td width="20%" align="center"><?php echo number_format($selecttotal[0]['TotalPayment'], 2); ?></td>
            </tr>
            <tr>
                <td width="25%">&nbsp;</td>
                <td width="25%">&nbsp;</td>
                <td width="10%">&nbsp;</td>
                <td width="20%" align="right">Round-off</td>
                <td width="20%" align="center"><?php echo $selecttotal[0]['RoundTotal']; ?></td>
            </tr>
            <tr>
                <td width="25%">&nbsp;</td>
                <td width="25%">&nbsp;</td>
                <td width="10%">&nbsp;</td>
                <td width="20%" align="right">
                    Paid by
                </td>
                <td width="20%" align="center">
                    <?php
                    if ($selecttotal[0]['Flag'] == "C") {
                        echo 'Card';
                    } elseif ($selecttotal[0]['Flag'] == "CS") {
                        echo 'Cash';
                    } else {
                        echo 'Cash & Card';
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td height="5"></td>
            </tr>
            <?php ?>

            <tr>
                <td width="25%">&nbsp;</td>
                <td width="25%">&nbsp;</td>
                <td width="10%">&nbsp;</td>
                <td width="20%" align="right">Your Savings</td>
                <td width="20%" style="border:1px solid #ccc;" align="center"><?php echo number_format($strSavings, 2); ?></td>
            </tr>
            <tr>
                <?php
                if ($selStoreGST[0]['AccountingCode'] == "" || $selStoreGST[0]['AccountingCode'] == " " || $selStoreGST[0]['AccountingCode'] == "0") {
                    $strAccoutingCode = $selSettingsData[0]['MasterAccountingCode'];
                } else {
                    $strAccoutingCode = $selStoreGST[0]['AccountingCode'];
                }
                ?>
                <td><span class="fs" style="font-size:12.5px;">Accounting Code:<?= $strAccoutingCode ?></span></td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td>
        <table width="900" class="four" style="margin: 0 auto; color: #807e7f;">
            <tr>
                <td>
                    <img src="http://pos.nailspaexperience.com/admin/images/invoice/<?php echo $selSettingsData[0]['InvoiceImgURL']; ?>">
                </td>
            </tr>
        </table>
    </td>
</tr>
</tbody>
</table>
</span>


<script>
    //$("#displays").one( "onload", function() { alert("You'll only see this once!"); } );
    // function saifusmaniisgreat() {
    //alert(111);
    // var divContents = encodeURIComponent($("#printarea").html());
    // var email =$("#email").val();
    // var app=$("#app_id").val();
    //alert(divContents)
    //document.write(divContents);
    // if(email!='')
    // {
    // $.ajax({
    // type:"post",
    // data:"divContents="+divContents+"&email="+email+"&app="+app,
    // url:"sendinvoice.php",
    // success:function(result1)
    // {
    // alert(result1)


    // }
    // });
    // }
    // else
    // {
    // alert('Email Id is blank')
    // }

    // };

<?php if (isset($_GET['automsg']) && $_GET['automsg'] == 1) { ?>
        sendSMS();
        sendmail();
<?php } else { ?>
        var retVal = confirm("Are You Sure you want to send SMS and Email ?");
        if (retVal == true) {
            sendSMS();
            sendmail();
        }
<?php } ?>
    function printDiv(divName)
    {
        var divContents = $("#printarea").html();
        var abc = $("#printarea1").html();
        // alert(abc);
        // alert(divContents)
        var printWindow = window.open('', '', 'height=600,width=800');
        printWindow.document.write('<html><head><title>Invoice</title>');
        printWindow.document.write('</head><body >');
        printWindow.document.write(divContents);
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        printWindow.print();

        // var printContents = document.getElementById(divName).innerHTML;
        // var originalContents = document.body.innerHTML;

        // document.body.innerHTML = printContents;

        // window.print();

        // document.body.innerHTML = originalContents; 
    }
    function sendmail()
    {
        //var divContents = encodeURIComponent($("#printareasaifu").html());
        var divContents = encodeURIComponent($("#printarea").html());
        console.log(divContents);
        var email = $("#email").val();
        var app = $("#app_id").val();
        //alert(divContents);return;

        //document.write(divContents);
        if (email != '')
        {
            $.ajax({
                type: "post",
                data: "divContents=" + divContents + "&email=" + email + "&app=" + app,
                url: "sendinvoice.php",
                success: function (result1)
                {
                    alert(result1);
                }
            });
        } else
        {
            alert('Email Id is blank');
        }

    }

    function sendSMS()
    {
        $.ajax({
            type: "GET",
            data: "uid=" + '<?php echo $_GET['uid'] ?>',
            url: "incBillingSMS.php",
            success: function (result1)
            {

            }
        });
    }
</script>