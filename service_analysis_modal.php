<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>

<div class="panel">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Service Analysis Report</h4>
    </div>
    <div class="panel-body">
        <div id="printdata">
            <?php
            $datedrom = $_GET["toandfrom"];
            if ($datedrom != "" && !IsNull($_GET["Store"])) {

                if (isset($_GET["toandfrom"])) {
                    $strtoandfrom = $_GET["toandfrom"];
                    $arraytofrom = explode("-", $strtoandfrom);

                    $from = $arraytofrom[0];
                    $datetime = new DateTime($from);
                    $getfrom = $datetime->format('Y-m-d');


                    $to = $arraytofrom[1];
                    $datetime = new DateTime($to);
                    $getto = $datetime->format('Y-m-d');

                    if (!IsNull($from)) {
                        $sqlTempfrom = " and Date(tblInvoiceDetails.OfferDiscountDateTime)>=Date('" . $getfrom . "')";
                    }

                    if (!IsNull($to)) {
                        $sqlTempto = " and Date(tblInvoiceDetails.OfferDiscountDateTime)<=Date('" . $getto . "')";
                    }
                }

                if (!IsNull($_GET["Store"])) {
                    $strStoreID = $_GET["Store"];

                    $sqlTempStore = " StoreID='$strStoreID'";
                }

                $store = $_GET["Store"];
                $sep = select("StoreName", "tblStores", "StoreID='" . $store . "'");
                $storename = $sep[0]['StoreName'];

                $get_source = $_GET["source"];
                $append = '';
                if (!empty($get_source)) {
                    $append = " AND tblAppointments.source='" . $get_source . "'";
                }
                ?>
                <h3 class="title-hero">Date Range selected : FROM - <?= $getfrom ?> / TO - <?= $getto ?> / Store Filter selected : <?= $storename ?> </h3>

                <?php
                $DB = Connect();
                $per = $_GET["per"];
                ?>

                <div class="example-box-wrapper">
                    <div class="scroll-columns">
                        <table class="table table-bordered table-striped table-condensed cf" width="100%">
                            <thead class="cf">
                                <tr>

                                    <th>Code</th>
                                    <th>Service Name</th>

                                    <th class="numeric">Cost</th>
                                    <?php
                                    if ($per != '0') {
                                        ?>
                                        <th >Amt %</th>
                                        <?php
                                    }
                                    ?>
                                    <th class="numeric"># Count</th>
                                    <?php
                                    if ($per != '0') {
                                        ?>
                                        <th >Service Count %</th>
                                        <?php
                                    }
                                    ?>
                                    <th class="numeric">Product Cost</th>
                                    <th class="numeric">Profitibility</th>
                                    <th class="numeric">ARPU</th>




                                </tr>
                            </thead>

                            <?php
                            $storr = $_GET["Store"];



                            $sqldata = "SELECT Distinct(AppointmentID) FROM tblAppointments WHERE AppointmentDate >= '" . $getfrom . "' AND AppointmentDate <= '" . $getto . "'" . $append;

                            $RSdata = $DB->query($sqldata);
                            if ($RSdata->num_rows > 0) {
                                while ($rowdata = $RSdata->fetch_assoc()) {
                                    $invAppointmentID[] = $rowdata["AppointmentID"];
                                }
                            }
//                                                                                                echo '<pre>';
//                                                                                                print_r($invAppointmentID);
//                                                                                                exit;
                            if (!empty($storr)) {
                                $sqldata = " SELECT Distinct(AppointmentID) FROM tblAppointments WHERE StoreID = '" . $storr . "'  AND IsDeleted != '1'  AND FreeService !=  '1'  AND Status = '2' AND AppointmentID IN (" . implode(',', $invAppointmentID) . ")";
                            } else {
                                $sqldata = " SELECT Distinct(AppointmentID) FROM tblAppointments WHERE StoreID != '0'  AND IsDeleted != '1'  AND FreeService !=  '1'  AND Status = '2' AND AppointmentID IN (" . implode(',', $invAppointmentID) . ")";
                            }


                            $RSdata = $DB->query($sqldata);
                            if ($RSdata->num_rows > 0) {
                                while ($rowdata = $RSdata->fetch_assoc()) {
                                    $appAppointmentID[] = $rowdata["AppointmentID"];
                                }
                            }

                            $sqldata = " SELECT AppointmentID,ServiceID,service_amount_deduct_dis FROM tblAppointmentsDetailsInvoice WHERE ServiceID != 'NULL' AND ServiceID != '' AND AppointmentID IN (" . implode(',', $appAppointmentID) . ")";
                            $RSdata = $DB->query($sqldata);
                            if ($RSdata->num_rows > 0) {
                                while ($rowdata = $RSdata->fetch_assoc()) {
                                    $serviceid[$rowdata["ServiceID"]] = $rowdata["ServiceID"];
                                    $service_deduct_disc[$rowdata["AppointmentID"]][$rowdata["ServiceID"]] = $rowdata["service_amount_deduct_dis"];
                                }
                            }
          

                            if (isset($_GET['Category']) && !empty($_GET['Category'])) {
                                $sqldata = " SELECT DISTINCT(ser.ServiceCode),ser.ServiceName,ser.ServiceID FROM tblServices ser"
                                        . " JOIN tblProductServiceCategory sercat ON(ser.ServiceID = sercat.ServiceID)"
                                        . " WHERE ser.ServiceID IN  (" . implode(',', $serviceid) . ") AND sercat.CategoryID='" . $_GET['Category'] . "'";
                            } else {
                                $sqldata = " SELECT DISTINCT(ServiceCode),ServiceName,ServiceID FROM tblServices WHERE ServiceID IN  (" . implode(',', $serviceid) . ") ";
                            }
                            $RSdata = $DB->query($sqldata);
                            if ($RSdata->num_rows > 0) {
                                while ($rowdata = $RSdata->fetch_assoc()) {
                                    $present_service[$rowdata['ServiceID']] = $rowdata['ServiceID'];
                                    $temp[] = $rowdata["ServiceCode"];
                                    $service_id_code[$rowdata['ServiceID']] = $rowdata['ServiceCode'];
                                    //$services[$rowdata['ServiceID']]['code'] = $rowdata['ServiceCode'];
                                    //$services[$rowdata['ServiceID']]['name'] = $rowdata['ServiceName'];
                                    $services[$rowdata['ServiceCode']]['id'] = $rowdata['ServiceID'];
                                    $services[$rowdata['ServiceCode']]['code'] = $rowdata['ServiceCode'];
                                    $services[$rowdata['ServiceCode']]['name'] = $rowdata['ServiceName'];
                                }
                            }

                            /* $deleted_service = array_diff($serviceid, $present_service);
                              if (isset($deleted_service) && is_array($deleted_service) && count($deleted_service) > 0) {
                              foreach ($deleted_service as $delskey => $delsvalue) {
                              $service_code = 'Deleted Services -' . $delsvalue;
                              $service_id_code[$delsvalue] = $service_code;
                              $services[$service_code]['id'] = $delsvalue;
                              $services[$service_code]['code'] = 'Deleted Services -' . $delsvalue;
                              $services[$service_code]['name'] = 'Deleted Services -' . $delsvalue;
                              }
                              } */




                            $ServiceIDdd = array_unique($temp);

                            /*
                             * get qty and service amount
                             */
                            //$sqldata = " SELECT qty,ServiceAmount,ServiceID FROM tblAppointmentsDetailsInvoice WHERE ServiceID != 'NULL' AND ServiceID != '' AND AppointmentID IN (" . implode(',', $appAppointmentID) . ")";
                            $sqldata = " SELECT DISTINCT inv.AppointmentId,inv.ServiceName,inv.Qty,inv.ServiceAmt,inv.DisAmt,inv.OfferAmt FROM tblInvoiceDetails inv WHERE AppointmentId IN (" . implode(',', $appAppointmentID) . ")";
                            $RSdata = $DB->query($sqldata);
                            if ($RSdata->num_rows > 0) {
                                while ($value = $RSdata->fetch_assoc()) {
                                    if (!empty($value['ServiceName'])) {
                                        $ServiceName = explode(',', $value['ServiceName']);
                                        $Qty = explode(',', $value['Qty']);
                                        $ServiceAmt = explode(',', $value['ServiceAmt']);
                                        $DisAmt = explode(',', $value['DisAmt']);
                                        $count = count($ServiceName);
                                        for ($i = 0; $i < $count; $i++) {
                                            $rowdata['ServiceID'] = $ServiceName[$i];
                                            $rowdata['qty'] = $Qty[$i];
                                            $rowdata['ServiceAmount'] = $ServiceAmt[$i];

                                            $offer_deduct = 0;
                                            $offer_amount = str_replace(array('-', ' '), '', $value['OfferAmt']);
                                            if ($offer_amount > 0) {
                                                $offer_deduct = $offer_amount / $count;
                                            }

                                            $service_code = isset($service_id_code[$rowdata['ServiceID']]) ? $service_id_code[$rowdata['ServiceID']] : '';
                                            
                                            if ($service_code != '') {
                                                if (isset($data[$service_code]['qty'])) {
                                                    $data[$service_code]['qty'] += $rowdata['qty'];
                                                } else {
                                                    $data[$service_code]['qty'] = $rowdata['qty'];
                                                }

                                                if (isset($data[$service_code]['ServiceAmount'])) {
                                                    $data[$service_code]['ServiceAmount'] += $rowdata['ServiceAmount'];
                                                } else {
                                                    $data[$service_code]['ServiceAmount'] = $rowdata['ServiceAmount'];
                                                }

                                                if (isset($DisAmt[$i]) && $DisAmt[$i] != '') {
                                                    $amount_exculde_dis = $ServiceAmt[$i] - $DisAmt[$i];
                                                } else {
                                                    $amount_exculde_dis = $ServiceAmt[$i];
                                                }

                                                if ($amount_exculde_dis < $offer_deduct) {
                                                    $amount_exculde_dis = 0;
                                                } else {
                                                    $amount_exculde_dis = $amount_exculde_dis - $offer_deduct;
                                                }

                                                $amount_after_deduct = isset($service_deduct_disc[$value['AppointmentId']][$rowdata['ServiceID']]) ? $service_deduct_disc[$value['AppointmentId']][$rowdata['ServiceID']] : 0;
                                                if (isset($data[$service_code]['totalamount'])) {
                                                    $data[$service_code]['totalamount'] += $amount_after_deduct;
                                                } else {
                                                    $data[$service_code]['totalamount'] = $amount_after_deduct;
                                                }
                                            }
                                            $all_service_id[] = $rowdata['ServiceID'];
                                        }
                                    }
                                }
                            }


                            /*
                             * get service product cost from tblservices
                             */
                            $final_service_price = array();
                            if (isset($all_service_id) && is_array($all_service_id) && count($all_service_id) > 0) {

                                $in_service_id = implode(",", $all_service_id);
                                if (isset($in_service_id) && $in_service_id != '') {

                                    $ser_pdtq = "SELECT psc.ServiceID,psc.StoredID,np.* FROM tblProductServiceCategory psc JOIN tblNewProducts np ON (np.ProductID = psc.ProductID)"
                                            . " WHERE ServiceID IN(" . $in_service_id . ")";
                                    $serpdt_exe = $DB->query($ser_pdtq);

                                    while ($ser_esult = $serpdt_exe->fetch_assoc()) {
                                        $all_service_pdt[] = $ser_esult;
                                    }
                                    if (isset($all_service_pdt) && is_array($all_service_pdt) && count($all_service_pdt) > 0) {
                                        foreach ($all_service_pdt as $pdt_k => $pdt_val) {
                                            $service_code = isset($service_id_code[$pdt_val['ServiceID']]) ? $service_id_code[$pdt_val['ServiceID']] : '';
                                            $pdt_cost[$service_code][$pdt_val['ProductID']] = array(
                                                'StoredID' => $pdt_val['StoredID'],
                                                'ProductMRP' => $pdt_val['ProductMRP'],
                                                'PerQtyServe' => $pdt_val['PerQtyServe'],
                                            );
                                        }
                                    }


                                    if (isset($pdt_cost) && is_array($pdt_cost) && count($pdt_cost) > 0) {
                                        foreach ($pdt_cost as $servipro_k => $servipro_val) {
                                            foreach ($servipro_val as $pdtpri_k => $pdtpri_val) {
                                                $amounts = $pdtpri_val['ProductMRP'];
                                                $qty = $pdtpri_val['PerQtyServe'];
                                                $amount = $amounts / $qty;

                                                if (isset($final_service_price[$servipro_k])) {
                                                    $final_service_price[$servipro_k] += $amount;
                                                } else {
                                                    $final_service_price[$servipro_k] = $amount;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            ?>
                            <tbody>
                                <?php
                                $sort_res = array();
                                if (isset($services) && is_array($services) && count($services)) {
                                    foreach ($services as $skey => $svalue) {
                                        $sort_res[$skey] = $svalue;
                                        $sort_res[$skey]['totalamount'] = isset($data[$skey]['totalamount']) ? $data[$skey]['totalamount'] : 0;
                                    }
                                }

                                uasort($sort_res, 'sortByName');
                                $final_res = array_reverse($sort_res);

                                if (isset($final_res) && is_array($final_res) && count($final_res)) {
                                    $totaltpcost = 0;
                                    $totalstrServiceAmount = 0;
                                    $totalstrqty = 0;
                                    foreach ($final_res as $key => $value) {

                                        $sumaatt = round($data[$key]['totalamount'], 2);
                                        $sumqty = $data[$key]['qty'];

                                        if ($sumaatt == "") {
                                            $sumaatt = 0;
                                        } else {
                                            $sumaatt = $sumaatt;
                                        }
                                        $totalstrServiceAmount = $totalstrServiceAmount + $sumaatt;

                                        if ($sumqty == "") {
                                            $sumqty = 0;
                                        } else {
                                            $sumqty = $sumqty;
                                        }

                                        $totalstrqty = $totalstrqty + $sumqty;
                                    }
                                }
                                $total_amount_per = 0;
                                $total_qty_per = 0;
                                if (isset($final_res) && is_array($final_res) && count($final_res)) {
                                    foreach ($final_res as $key => $value) {
                                        ?>
                                        <tr>

                                            <td><?php echo $value['code']; ?></td>
                                            <td><?php echo $value['name']; ?></td>
                                            <td><?php
                                                echo round($data[$key]['totalamount'], 2);
                                                ?></td>
                                            <?php
                                            if ($per != '0') {
                                                ?>
                                                <td class="numeric" ><?php
                                                    $amtper = (round($data[$key]['totalamount'], 2) / $totalstrServiceAmount) * 100;
                                                    echo round($amtper, 2);
                                                    $total_amount_per = $total_amount_per + round($amtper, 2);
                                                    ?></td>
                                                <?php
                                            }
                                            ?>
                                            <td><?php
                                                echo $data[$key]['qty'];
                                                ?></td>
                                            <?php
                                            if ($per != '0') {
                                                ?>
                                                <td class="numeric" ><?php
                                                    $qtyper = ($data[$key]['qty'] / $totalstrqty) * 100;
                                                    echo round($qtyper, 2);
                                                    $total_qty_per = $total_qty_per + round($qtyper, 2);
                                                    ?></td>
                                                <?php
                                            }
                                            ?>
                                            <td><?php
                                                $ProductCost = (number_format((float) $final_service_price[$key], 2, '.', ''));
                                                $round_cost = round($ProductCost) * $data[$key]['qty'];
                                                echo $round_cost;
                                                $totaltpcost = $totaltpcost + $round_cost;
                                                ?></td>

                                            <td><?php
                                                $t = $data[$key]['totalamount'] - round($ProductCost);
                                                $Profitibility = (number_format((float) $t, 2, '.', ''));
                                                echo $Profitibility;
                                                $totalstrprofit = $totalstrprofit + $Profitibility;
                                                ?></td>

                                            <td><?php
                                                $x = $Profitibility / round($ProductCost);
                                                $arpu = (number_format((float) $x, 2, '.', ''));
                                                echo $arpu;
                                                $totalARPU = $totalARPU + $arpu;
                                                ?></td>


                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>


                            <tbody>

                                <tr>
                                    <td colspan="2"><center><b>Total Amount and Count in selected periods(s) : <?= $counter ?></b><center></td>

                                    <td class="numeric"><b>Rs. <?= round($totalstrServiceAmount, 2) ?>/-</b></td>
                                    <?php
                                    if ($per != '0') {
                                        ?>
                                        <td class="numeric"><?php echo round($total_amount_per); ?></td>
                                        <?php
                                    }
                                    ?>
                                    <td class="numeric"><b><?= $totalstrqty ?></b></td>
                                    <?php
                                    if ($per != '0') {
                                        ?>
                                        <td class="numeric"><?php echo round($total_qty_per); ?></td>
                                        <?php
                                    }
                                    ?>
                                    <td class="numeric"><b>Rs. <?= $totaltpcost ?>/-</b></td>
                                    <td class="numeric"><b><?= $totalstrprofit ?></b></td>
                                    <td class="numeric"><b>Rs. <?= round($totalARPU) ?></b></td>


                                    </tr>

                                    </tbody>
                                    </table>

                                    </div>
                                    </div>

                                <?php } ?>
                                </div>
                                </div>
                                </div>
