<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
$strPageTitle = "Customer Details | NailSpa";
$strDisplayTitle = "Customer Details for NailSpa";
$strMenuID = "10";
$strMyTable = "tblAppointments";
$strMyTableID = "AppointmentID";
$strMyField = "AppointmentDate";
$strMyActionPage = "DisplayNonCustomerCount.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <?php require_once("incChart-SalonDashboard.fya"); ?>

        <!-----------------included datatable.css & js files by gandhali 1/9/18----------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/datatable/datatable.css">
        <script type="text/javascript" src="assets/widgets/datatable/datatable.js"></script>
        <script type="text/javascript" src="assets/widgets/datatable/datatable-bootstrap.js"></script>
        <script type="text/javascript" src="assets/widgets/datatable/datatable-responsive.js"></script>
        <!-----------------included datatable.css by gandhali 1/9/18----------------->
        <style>
            .btn-danger:hover
            {
                border-color: #fc8213;
                background: #fc8213;
            }

        </style>
        <!-- Styles -->


    </head>

    <body>

        <div id="sb-site">


            <?php // require_once("incOpenLayout.fya");   ?>
            <!----------commented by gandhali 1/9/18---------------->


            <?php require_once("incLoader.fya"); ?>


            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>

                        <script type="text/javascript" src="assets/widgets/skycons/skycons.js"></script>
                        <script type="text/javascript" src="assets/widgets/datatable/datatable.js"></script>
                        <script type="text/javascript" src="assets/widgets/datatable/datatable-bootstrap.js"></script>
                        <script type="text/javascript" src="assets/widgets/datatable/datatable-tabletools.js"></script>


                        <div class="panel">
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="example-box-wrapper">
                                        <div id="normal-tabs-1">
                                            <span class="form_result">&nbsp; <br>
                                            </span>

                                            <div class="panel-body">
                                                <h4 class="title-hero"><center>List of Website Customers | NailSpa</center></h4>
                                                <br>
                                                <div class="example-box-wrapper">
                                                    <table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap table-hover" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th><center>Name</center></th>
                                                        <th><center>Mobile</center></th>
                                                        <th><center>Email ID</center></th>
                                                        <th><center>Action</center></th>
                                                        </tr>
                                                        </thead>


                                                        <tbody>
                                                            <?php
                                                            $DB = Connect();
                                                            $append = '';
                                                            if ($strAdminRoleID == 6) {
                                                                $append = " AND PreferredStoreID='" . $strStore . "'";
                                                            }
                                                            $customerq = "SELECT * FROM tblCustomers WHERE Acquisition='Website'"
                                                                    . " AND website_customer =1 " . $append;
                                                            $customer_exe = $DB->query($customerq);
                                                            if ($customer_exe->num_rows > 0) {
                                                                while ($cust_res = $customer_exe->fetch_assoc()) {
                                                                    $customer_data[$cust_res['CustomerID']] = $cust_res;
                                                                }
                                                            }

                                                            if (isset($customer_data) && is_array($customer_data) && count($customer_data) > 0) {
                                                                foreach ($customer_data as $ckey => $cvalue) {
                                                                    $EncodedID = EncodeQ($cvalue['CustomerID']);
                                                                    ?>
                                                                    <tr>
                                                                        <td><?php echo $cvalue['CustomerFullName']; ?></td>
                                                                        <td><?php echo $cvalue['CustomerMobileNo']; ?></td>
                                                                        <td><?php echo $cvalue['CustomerEmailID']; ?></td>
                                                                        <td>
                                                                <center><a target="_blank" class="btn btn-link font-blue" href="ManageAppointments.php?source=4&bid=<?= $EncodedID ?>">Book Appointment</a></center>
                                                                </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                        </tbody>
                                                        <?php
                                                        $DB->close();
                                                        ?>
                                                    </table>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php require_once 'incFooter.fya'; ?>

                </div>
                </body>

                </html>		