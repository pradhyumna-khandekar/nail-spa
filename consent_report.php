<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php
$strPageTitle = "Manage Customers | Nailspa";
$strDisplayTitle = "Consent Report of Nailspa Experience";
$strMenuID = "10";
$strMyTable = "tblCustomers";
$strMyTableID = "CustomerID";
$strMyField = "CustomerMobileNo";
$strMyActionPage = "consent_report.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
?>
<!DOCTYPE html>
<html lang="en">
    <?php require_once("incMetaScript.fya"); ?>

    <body>
        <div id="sb-site">

            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>
                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                        </div>
                        <script type="text/javascript">
                            /* Datatables responsive */
                            $(document).ready(function () {
                                $('#datatable-responsive-count').DataTable({
                                    "order": [[3, "desc"]]
                                });
                            });

                        </script>

                        <div class="panel">
                            <div class="panel-body" style="overflow-x: scroll;">
                                <h3 class="title-hero">Consent Report</h3>
                                <form method="get" class="form-horizontal bordered-row" role="form">

                                    <?php
                                    if (isset($_GET["toandfrom"])) {
                                        $strtoandfrom = $_GET["toandfrom"];
                                    }
                                    ?>
                                    <div class="form-group"><label for="" class="col-sm-2 control-label">Select Date Range</label>
                                        <div class="col-sm-4">
                                            <div class="input-prepend input-group">
                                                <span class="add-on input-group-addon">
                                                    <i class="glyph-icon icon-calendar"></i>
                                                </span> 
                                                <input type="text" name="toandfrom" id="daterangepicker-example" class="form-control" value="<?= $strtoandfrom ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                    $sql1 = "SELECT StoreID, StoreName FROM tblStores WHERE Status = 0";
                                    $RS2 = $DB->query($sql1);
                                    if ($RS2->num_rows > 0) {
                                        ?>
                                        <div class="form-group"><label class="col-sm-2 control-label">Store</label>
                                            <div class="col-sm-4">
                                                <select class="form-control required"  id="StoreID" name="StoreID" >
                                                    <option value="" selected>All</option>
                                                    <?php
                                                    while ($row2 = $RS2->fetch_assoc()) {
                                                        $StoreID = $row2["StoreID"];
                                                        $StoreName = $row2["StoreName"];
                                                        $all_stores[$row2["StoreID"]] = $row2;
                                                        ?>	
                                                        <option value="<?= $StoreID ?>" <?php echo isset($_GET['StoreID']) && $_GET['StoreID'] == $StoreID ? 'selected' : '' ?>><?= $StoreName ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>	

                                        <div class="form-group"><label class="col-sm-3 control-label"></label>
                                            <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                            &nbsp;&nbsp;&nbsp;
                                            <a class="btn btn-link" href="consent_report.php">Clear All Filter</a> &nbsp;&nbsp;&nbsp;
                                        </div>
                                        <?php
                                    }
                                    ?>


                                </form>

                                <?php
                                if (isset($_GET["toandfrom"]) && $_GET["toandfrom"] != '') {
                                    if (isset($_GET['StoreID']) && !empty($_GET['StoreID'])) {
                                        $store_data = select("*", "tblStores", "StoreID='" . $_GET['StoreID'] . "'");
                                        $store_name = $store_data[0]['StoreName'];
                                    } else {
                                        $store_name = "All";
                                    }
                                    ?>
                                    <h3>Store : <?php echo $store_name; ?></h3>
                                    <table id = "datatable-responsive-count" class = "table table-striped table-bordered responsive no-wrap" cellspacing = "0" width = "100%">
                                        <thead>
                                            <tr>
                                                <th>Invoice No.</th>
                                                <th>Customer Name</th>
                                                <th>Store</th>
                                                <th>Appointment Datetime</th>
                                                <th>Invoice Amount</th>
                                                <th>Consent Form</th>
                                                <th>Invoice</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Invoice No.</th>
                                                <th>Customer Name</th>
                                                <th>Store</th>
                                                <th>Appointment Datetime</th>
                                                <th>Invoice Amount</th>
                                                <th>Consent Form</th>
                                                <th>Invoice</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <?php
                                            if (isset($_GET['StoreID']) && !empty($_GET['StoreID'])) {
                                                $append .= " AND tblAppointments.StoreID='" . $_GET['StoreID'] . "'";
                                            }

                                            if (isset($_GET["toandfrom"]) && $_GET["toandfrom"] != '') {
                                                $strtoandfrom = $_GET["toandfrom"];
                                                $arraytofrom = explode("-", $strtoandfrom);

                                                $from = $arraytofrom[0];
                                                $datetime = new DateTime($from);
                                                $getfrom = $datetime->format('Y-m-d');

                                                $to = $arraytofrom[1];
                                                $datetime = new DateTime($to);
                                                $getto = $datetime->format('Y-m-d');
                                                $append .= " AND Date(tblAppointments.AppointmentDate)>='" . $getfrom . "'";
                                                $append .= " AND Date(tblAppointments.AppointmentDate)<='" . $getto . "'";
                                            }
                                            $aptq = "SELECT * FROM tblAppointments WHERE StoreID != '0' AND IsDeleted != '1' 
                                                AND FreeService !=  '1' AND Status = '2' AND is_concent_approve=1 " . $append;
                                            $aptq_exe = $DB->query($aptq);

                                            while ($appointment_res = $aptq_exe->fetch_assoc()) {
                                                $all_appointment[$appointment_res['AppointmentID']] = $appointment_res;
                                                $all_apt[$appointment_res['AppointmentID']] = $appointment_res['AppointmentID'];
                                                $customer_ids[$appointment_res['CustomerID']] = $appointment_res['CustomerID'];
                                            }

                                            /*
                                             * Get Customer Data
                                             */
                                            if (isset($customer_ids) && is_array($customer_ids) && count($customer_ids) > 0) {
                                                $cust_in_ids = implode(",", $customer_ids);
                                                if ($cust_in_ids != '') {
                                                    $custq = "SELECT * FROM tblCustomers WHERE CustomerID IN (" . $cust_in_ids . ")";
                                                    $custq_exe = $DB->query($custq);
                                                    while ($customer_res = $custq_exe->fetch_assoc()) {
                                                        $customer_all[$customer_res['CustomerID']] = $customer_res;
                                                    }
                                                }
                                            }

                                            /*
                                             * Get Invoice Number
                                             */
                                            if (isset($all_apt) && is_array($all_apt) && count($all_apt) > 0) {
                                                $all_apt_ids = implode(",", $all_apt);
                                                if ($all_apt_ids != '') {
                                                    $invoiceq = "SELECT * FROM tblInvoice WHERE AppointmentID IN (" . $all_apt_ids . ")";
                                                    $invoice_exe = $DB->query($invoiceq);
                                                    while ($invoice_res = $invoice_exe->fetch_assoc()) {
                                                        $invoice_all[$invoice_res['AppointmentID']] = $invoice_res;
                                                    }
                                                }
                                            }

                                            if (isset($all_appointment) && is_array($all_appointment) && count($all_appointment) > 0) {
                                                foreach ($all_appointment as $akey => $avalue) {
                                                    $getUID = EncodeQ($avalue['AppointmentID']);
                                                    ?>
                                                    <tr>
                                                        <td><?php echo isset($invoice_all[$avalue['AppointmentID']]) ? '#' . $invoice_all[$avalue['AppointmentID']]['InvoiceID'] : ''; ?></td>
                                                        <td><?php echo isset($customer_all[$avalue['CustomerID']]) ? $customer_all[$avalue['CustomerID']]['CustomerFullName'] : ''; ?></td>
                                                        <td><?php echo isset($all_stores[$avalue['StoreID']]) ? $all_stores[$avalue['StoreID']]['StoreName'] : ''; ?></td>
                                                        <td><?php echo date('d/m/Y', strtotime($avalue['AppointmentDate'])) . ' ' . date('h:i a', strtotime($avalue['SuitableAppointmentTime'])) ?></td>
                                                        <td><?php echo number_format($avalue['total_after_tax'],2); ?></td>
                                                        <td>
                                                            <a target="_blank" class="btn btn-link" href="show-consent-form.php?app=<?= $avalue['AppointmentID'] ?>&show=1">Consent Form</a>
                                                        </td>
                                                        <td>
                                                            <a target="_blank" class="btn btn-link" href="CheckInvoices.php?uid=<?= $getUID; ?>">View Invoice</a>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <?php
                                } else {
                                    echo "<br><center><h3>Please Select Month And Year!</h3></center>";
                                }
                                ?>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
            <?php require_once 'incFooter.fya'; ?>
        </div>
    </body>
</html>