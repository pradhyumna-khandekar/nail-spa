<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php

$strPageTitle = "Manage Customers | Nailspa";
$strDisplayTitle = "Manage Customers for Nailspa";
$strMenuID = "10";
$strMyTable = "tblServices";
$strMyTableID = "ServiceID";
//$strMyField = "CustomerMobileNo";
$strMyActionPage = "appointment_invoice.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights	

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $DB = Connect();
    $PackageID = $_POST["packageid"];
    $app_id = $_POST["app_id"];
    $seld = select("*", "tblPackages", "PackageID='" . $PackageID . "'");
    $ServiceID = $seld[0]['ServiceID'];
    $PackageNewPrice = $seld[0]['PackageNewPrice'];
    $Validityp = $seld[0]['Validity'];
    $valid = "+" . $Validityp . "Months";
    $validpack = date('Y-m-d', strtotime($valid));

    $ServiceIDd = explode(",", $ServiceID);
    $ServiceQty = explode(",", $seld[0]['Qty']);
    if (isset($ServiceIDd) && is_array($ServiceIDd) && count($ServiceIDd) > 0) {
        foreach ($ServiceIDd as $key => $value) {
            $service_in_pac_qty[$value] = $ServiceQty[$key];
        }
    }

    /*
     * Get pakage Service Code
     */
    $service_in_package_data = select("*", "tblServices", "ServiceID IN(" . $ServiceID . ")");
    if (isset($service_in_package_data) && is_array($service_in_package_data) && count($service_in_package_data) > 0) {
        foreach ($service_in_package_data as $pskey => $psvalue) {
            $service_in_package[$psvalue['ServiceCode']] = isset($service_in_pac_qty[$psvalue['ServiceID']]) ? $service_in_pac_qty[$psvalue['ServiceID']] : 0;
        }
    }

    $selp = select("*", "tblAppointments", "AppointmentID='" . $app_id . "'");
    $memberid = $selp[0]['memberid'];
    $customerid = $selp[0]['CustomerID'];
    $offerid = $selp[0]['offerid'];
    $VoucherID = $selp[0]['VoucherID'];
    $sep = select("*", "tblAppointments", "AppointmentID='" . $app_id . "'");
    $storee = $sep[0]['StoreID'];
    $oldpackage = $sep[0]['PackageID'];
    $packages = $oldpackage . "," . $PackageID;


    /*
     * Insert Into Customer Package
     */
    $sqlcustpackage = "INSERT INTO tblCustomerPackageAmt(appointment_id, customer_id, package_id,package_expiry,package_status,created_date) VALUES 
												('" . $app_id . "', '" . $customerid . "', '" . $PackageID . "','" . $validpack . "','1','" . date('Y-m-d H:i:s') . "')";

    $DB->query($sqlcustpackage);


    $customer_package = "INSERT INTO tblCustomerPackage(customer_id,package_id,package_expiry,created_date) VALUES 
                ('" . $customerid . "', '" . $PackageID . "', '" . $validpack . "','" . date('Y-m-d H:i:s') . "')";
    $DB->query($customer_package);
    $customer_package_id = $DB->insert_id;

    $package_service_res = select("*", "tblCustomerAppointmentPackage", "CustomerPackageId='" . $customer_package_id . "' AND status=1");
    if (isset($package_service_res) && is_array($package_service_res) && count($package_service_res) > 0) {
        foreach ($package_service_res as $serkey => $servalue) {
            $package_service_data[$servalue['service_code']] = $servalue;
        }
    }

    for ($u = 0; $u < count($ServiceIDd); $u++) {
        $sept = select("ServiceCost", "tblServices", "ServiceID='" . $ServiceIDd[$u] . "'");
        $ServiceCost = $sept[0]['ServiceCost'];

        $sqlInsert1 = "Insert into tblAppointmentsDetailsInvoice(AppointmentID, ServiceID, ServiceAmount,qty,Status,PackageService) values('" . $app_id . "','" . $ServiceIDd[$u] . "', '" . $ServiceCost . "','1','1','" . $PackageID . "')";
        if ($DB->query($sqlInsert1) === TRUE) {
            $last_id7 = $DB->insert_id;
        } else {
            echo "Error: " . $sqlInsert1 . "<br>" . $conn->error;
        }

        /*
         * Check if record already present 
         */
        /* $service_exist = select("AppointmentDetailsID", "tblAppointmentsDetailsInvoice", "AppointmentID='" . $app_id . "' AND ServiceID='" . $ServiceIDd[$u] . "'");
          if (isset($service_exist) && is_array($service_exist) && count($service_exist) > 0) {
          $update_service_qry = "UPDATE tblAppointmentsDetailsInvoice SET PackageService='" . $PackageID . "' WHERE AppointmentDetailsID='" . $service_exist[0]['AppointmentDetailsID'] . "'";
          $DB->query($update_service_qry);
          $last_id7 = $service_exist[0]['AppointmentDetailsID'];
          } else {
          $sqlInsert1 = "Insert into tblAppointmentsDetailsInvoice(AppointmentID, ServiceID, ServiceAmount,qty,Status,PackageService) values('" . $app_id . "','" . $ServiceIDd[$u] . "', '" . $ServiceCost . "','1','1','" . $PackageID . "')";
          if ($DB->query($sqlInsert1) === TRUE) {
          $last_id7 = $DB->insert_id;
          } else {
          echo "Error: " . $sqlInsert1 . "<br>" . $conn->error;
          }
          } */

        $all_service_id = select("*", "tblAppointmentsDetailsInvoice", "AppointmentID='" . $app_id . "'");
        if (isset($service_in_package_data) && is_array($service_in_package_data) && count($service_in_package_data) > 0) {
            foreach ($service_in_package_data as $pskey => $psvalue) {
                $service_in_package[$psvalue['ServiceCode']] = isset($service_in_pac_qty[$psvalue['ServiceID']]) ? $service_in_pac_qty[$psvalue['ServiceID']] : 0;
            }
        }
        if (isset($PackageID) && !empty($PackageID)) {
            /*
             * Get Service Code
             */
            $service_row = select("*", "tblServices", "ServiceID='" . $ServiceIDd[$u] . "'");

            $service_code = $service_row[0]['ServiceCode'];

            $package_qty_remain = isset($package_service_data[$service_code]['qty_remaining']) ? $package_service_data[$service_code]['qty_remaining'] : $service_in_package[$service_code];

            if ($package_qty_remain > 0 && isset($service_in_package[$service_code])) {
                /*
                 * get all service of package 
                 */
                $sqlcustpackageservice = "INSERT INTO tblCustomerAppointmentPackage(CustomerPackageId,appointment_id, customer_id,package_id,service_id,service_code,service_amount,package_expiry,service_status,service_qty,qty_remaining,created_date,service_qty_done) VALUES 
												('" . $customer_package_id . "', '" . $app_id . "', '" . $customerid . "', '" . $PackageID . "', '" . $ServiceIDd[$u] . "','" . $service_row[0]['ServiceCode'] . "','" . $ServiceCost . "','" . $validpack . "','1','" . $ServiceQty[$u] . "','" . $ServiceQty[$u] . "','" . date('Y-m-d H:i:s') . "','0')";

                $DB->query($sqlcustpackageservice);
            }
        }
    }
    $sqlInsertpq = "Insert into tblAppointmentPackageValidity(AppointmentID,PackageID,ValidTill) values('" . $app_id . "','" . $PackageID . "', '" . $validpack . "')";
    if ($DB->query($sqlInsertpq) === TRUE) {
        $last_id90 = $DB->insert_id;
    } else {
        echo "Error: " . $sqlInsertpq . "<br>" . $conn->error;
    }


    $sqlcharges = "Select ChargeNameId , (select GROUP_CONCAT(distinct ChargeSetID) from tblCharges where 
								ChargeNameID=tblServicesCharges.ChargeNameID) as ArrayChargeSet from tblServicesCharges";
//echo $sqlcharges."<br>";
    $charges = $DB->query($sqlcharges);
    if ($charges->num_rows > 0) {
        while ($row = $charges->fetch_assoc()) {
            $ChargeNameId = $row["ChargeNameId"];
            $ArrayChargeSet = $row["ArrayChargeSet"];
            $strChargeSet = explode(",", $ArrayChargeSet);
        }
    }



    for ($j = 0; $j < count($strChargeSet); $j++) {
        $strChargeSetforwork = $strChargeSet[$j];
        $sqlchargeset = "select SetName, ChargeAmt, ChargeFPType from tblChargeSets where ChargeSetID=$strChargeSetforwork";

        $RS2 = $DB->query($sqlchargeset);
        if ($RS2->num_rows > 0) {
            while ($row2 = $RS2->fetch_assoc()) {
                $strChargeAmt = $row2["ChargeAmt"];
                $strSetName = $row2["SetName"];
                $strChargeFPType = $row2["ChargeFPType"];
// Calculation of charges
                $PackageNewPrice = $PackageNewPrice;
                if ($strChargeFPType == "0") {
                    $strChargeAmt = $strChargeAmt;
                } else {

                    $percentage = $strChargeAmt;
//echo "percentage=".$percentage."<br/>";
                    $outof = $PackageNewPrice;
//echo "ServiceCost=".$ServiceCost."<br/>";
                    $strChargeAmt = ($percentage / 100) * $outof;
//echo "strChargeAmt=".$strChargeAmt."<br/>";
                }

                $totalamt = $strChargeAmt * 1;

                $sqlInsertchargesd = "INSERT INTO tblAppointmentsChargesInvoice(AppointmentDetailsID, ChargeName, ChargeAmount,AppointmentID,TaxGVANDM,PackageID) VALUES 
												('" . $last_id7 . "', '" . $strSetName . "', '" . $totalamt . "','" . $app_id . "','3','" . $PackageID . "')";

                ExecuteNQ($sqlInsertchargesd);
            }
        }
    }
    unset($strChargeSet);




    $sqlUpdate = "UPDATE tblAppointments SET PackageID='" . $PackageID . "' WHERE AppointmentID='" . $app_id . "'";
    ExecuteNQ($sqlUpdate);
//	echo $sqlInsert2;



    echo 2;
    unset($ServiceIDd);

    $DB->close();
}
?>