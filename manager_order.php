<?php require_once("setting.fya"); ?>
<?php
$counter = 1;
require_once 'incFirewall.fya';
$strPageTitle = "Place Order Request| Nailspa";
$strDisplayTitle = "Place Order Request| Nailspa";
$strMenuID = "6";
$strMyTable = "tblOrder";
$strMyTableID = "OrderID";
$strMyActionPage = "manager_order.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>

    <body>
        <div id="sb-site">   
            <?php require_once("incOpenLayout.fya"); ?>	

            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>
                <?php require_once("incLeftMenu.fya"); ?>


                <div id="page-content-wrapper">
                    <div id="page-content">
                        <?php require_once("incHeader.fya"); ?>									

                        <?php
                        if (isset($_GET['id']) && is_numeric($_GET['id'])) {
                            $order_data = select("*", "order_request", "id='" . $_GET['id'] . "' AND status=1");
                            $order_product = select("*", "order_request_product", "order_request_id='" . $_GET['id'] . "' AND status=1");

                            if (isset($order_product) && is_array($order_product) && count($order_product) > 0) {
                                foreach ($order_product as $pkey => $pvalue) {
                                    $pdt_id_arr[$pvalue['product_id']] = $pvalue['product_id'];
                                }
                            }

                            $order_store = isset($order_data[0]['store_id']) ? $order_data[0]['store_id'] : 0;
                            $DB = Connect();
                            if (isset($pdt_id_arr) && is_array($pdt_id_arr) && count($pdt_id_arr) > 0) {
                                $pdt_in_ids = implode(",", $pdt_id_arr);
                                $pdt_nameq = "SELECT * FROM tblNewProducts WHERE ProductID IN(" . $pdt_in_ids . ")";
                                $pdt_nameq_exe = $DB->query($pdt_nameq);
                                if ($pdt_nameq_exe->num_rows > 0) {
                                    while ($pdt_row = $pdt_nameq_exe->fetch_assoc()) {
                                        $pdt_name_data[$pdt_row["ProductID"]] = $pdt_row['ProductName'];
                                    }
                                }

                                /*
                                 * Get Available Quantity data
                                 */
                                if ($pdt_in_ids != '') {
                                    $pdt_stockq = "SELECT * FROM product_inventory WHERE status=1 AND ProductID IN(" . $pdt_in_ids . ") AND StoreID='" . $order_store . "'";
                                    $pdt_stock_exe = $DB->query($pdt_stockq);
                                    if ($pdt_stock_exe->num_rows > 0) {
                                        while ($pdtstock_row = $pdt_stock_exe->fetch_assoc()) {
                                            $pdt_stock[$pdtstock_row["ProductID"]] = $pdtstock_row['Stock'];
                                        }
                                    }
                                }
                            }
                        }
                        ?>
                        <div class="panel">
                            <div class="panel-body">

                                <h3 class="title-hero">Order Request</h3>
                                <form role="form" enctype="multipart/form-data" class="form-horizontal bordered-row enquiry_form" action="insert_order_data.php" method="post">
                                    <input type="hidden" name="order_id" value="<?php echo isset($order_data[0]['id']) ? $order_data[0]['id'] : ''; ?>"/>
                                    <div class="form-group" style="display: none;">
                                        <div class="col-md-1">
                                            <label class="control-label" for="order_name">Name :</label>
                                        </div>
                                        <div class="col-md-11">
                                            <input type="text" name="order_name" class="form-control" placeholder="Order Name" value="<?php echo isset($order_data[0]['order_name']) ? $order_data[0]['order_name'] : ''; ?>">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">Store</label>
                                        <div class="col-sm-4">
                                            <select name="order_store_id" id="order_store_id" class="form-control" required="required">
                                                <option value="">Select Store</option>
                                                <?php
                                                if ($strAdminRoleID == 6) {
                                                    $selp = select("*", "tblStores", "StoreID='$strStore'");
                                                    $order_data[0]['store_id'] = $strStore;
                                                } else {
                                                    $selp = select("*", "tblStores", "Status='0'");
                                                }

                                                foreach ($selp as $val) {
                                                    $strStoreName = $val["StoreName"];
                                                    $strStoreID = $val["StoreID"];
                                                    $store = $_GET["Store"];
                                                    ?>
                                                    <option value="<?= $strStoreID ?>" <?php echo isset($order_data[0]['store_id']) && $order_data[0]['store_id'] == $strStoreID ? 'selected' : ''; ?>><?= $strStoreName ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <table class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Product</th>
                                                    <th>Available Quantity</th>
                                                    <th>Quantity</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody class="product_body">
                                                <?php
                                                if (isset($order_product) && is_array($order_product) && count($order_product) > 0) {
                                                    foreach ($order_product as $okey => $ovalue) {
                                                        if (isset($pdt_name_data[$ovalue['product_id']])) {
                                                            ?>
                                                            <tr id="product_div_<?php echo $okey; ?>">
                                                                <td>
                                                                    <input type="hidden" name="map_id[<?php echo $okey ?>]" value="<?php echo $ovalue['id']; ?>"/>
                                                                    <select required="required" class="search_product_jry" name="pdt_id[<?php echo $okey ?>]" lang="<?php echo $okey ?>" id="product_sel_<?php echo $okey ?>">
                                                                        <option value="<?php echo $ovalue['product_id']; ?>" selected="selected"><?php echo $pdt_name_data[$ovalue['product_id']]; ?></option>
                                                                    </select>
                                                                </td>
                                                                <td><span class="avail_<?php echo $okey; ?>">
                                                                        <?php echo isset($pdt_stock[$ovalue['product_id']]) ? $pdt_stock[$ovalue['product_id']] : ''; ?>
                                                                    </span></td>
                                                                <td>
                                                                    <input autocomplete="off" type="text" pattern="\d*" oninvalid="this.setCustomValidity('Enter Number Only')" oninput="this.setCustomValidity('')"  name="pdt_qty[<?php echo $okey ?>]" class="form-control" required="required" placeholder="Quantity" value="<?php echo $ovalue['quantity']; ?>">
                                                                </td>
                                                                <td>
                                                                    <?php if ($okey == 0) { ?>
                                                                        <a class="btn btn-xs btn-primary add_more_product">Add</a>
                                                                    <?php } else { ?>
                                                                        <a href="#" class="btn btn-xs btn-danger" onclick="remove_product(<?php echo $okey; ?>);">Remove</a>
                                                                    <?php } ?>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                            $counter++;
                                                        }
                                                    }
                                                } else {
                                                    ?>
                                                    <tr id="product_div_0">
                                                        <td>
                                                            <input type="hidden" name="map_id[0]" value="0"/>
                                                            <select class="search_product_jry" name="pdt_id[0]" required="required" lang="0" id="product_sel_0"></select>
                                                        </td>
                                                        <td><span class="avail_0"></span></td>
                                                        <td>
                                                            <input required="required" autocomplete="off" type="text" pattern="\d*" oninvalid="this.setCustomValidity('Enter Number Only')" oninput="this.setCustomValidity('')"  name="pdt_qty[0]" class="form-control required" placeholder="Quantity" value="">
                                                        </td>
                                                        <td>
                                                            <a class="btn btn-xs btn-primary add_more_product">Add</a>
                                                        </td>
                                                    </tr>

                                                <?php } ?>
                                            </tbody>
                                        </table>

                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-11 control-label"></label>
                                        <input type="submit" class="btn ra-100 btn-primary" value="Submit">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <?php require_once 'incFooter.fya'; ?>
            </div>
        </div>

        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script> 

        <?php /*   <link href="assets/widgets/select2/select2.min.css" rel="stylesheet" />
          <script src="assets/widgets/select2/select2.min.js"></script> */ ?>

        <style>
            .select2-container{width: 300px !important;}

        </style>
        <script>
                                                                            var productDivCount = '<?php echo $counter; ?>';
                                                                            function search_product_name() {
                                                                                $('.search_product_jry').select2({
                                                                                    allowClear: true,
                                                                                    placeholder: "Search Product",
                                                                                    ajax: {
                                                                                        method: 'post',
                                                                                        url: 'autosearch_product.php',
                                                                                        dataType: 'json',
                                                                                        data: function (params) {
                                                                                            var query = {
                                                                                                term: params.term,
                                                                                                page: params.page || 1
                                                                                            }

                                                                                            // Query parameters will be ?search=[term]&page=[page]
                                                                                            return query;
                                                                                        },
                                                                                        processResults: function (data) {
                                                                                            // Transforms the top-level key of the response object from 'items' to 'results'
                                                                                            return {
                                                                                                results: data.result
                                                                                            };
                                                                                        }
                                                                                    }
                                                                                }).change(function () {
                                                                                    var lang = $(this).attr('lang');
                                                                                    try {
                                                                                        var product_id = $(this).val();
                                                                                        var store_id = $("#order_store_id").val();
                                                                                        if (product_id > 0 && store_id == '') {
                                                                                            alert('Select Store First.');
                                                                                            $('#product_sel_' + lang).val(null).trigger('change');
                                                                                            $('.avail_' + lang).html('');
                                                                                        } else if (product_id > 0 && store_id > 0) {
                                                                                            $.ajax({
                                                                                                url: "autosearch_product.php",
                                                                                                type: "POST",
                                                                                                data: {'pdt_id': product_id, 'get_pdt_qty': 1, 'store_id': store_id},
                                                                                                dataType: 'json',
                                                                                                success: function (result) {
                                                                                                    var product_stock = result['stock'];
                                                                                                    var order_quantity = result['order_quantity'];
                                                                                                    if (order_quantity > 0 && product_stock > order_quantity) {
                                                                                                        alert('Current Stock is ' + product_stock + '.You can make request when Stock is ' + order_quantity + ' Or less than ' + order_quantity);
                                                                                                        $('#product_sel_' + lang).val(null).trigger('change');
                                                                                                        $('.avail_' + lang).html('');
                                                                                                    } else {
                                                                                                        $('.avail_' + lang).html(product_stock);
                                                                                                    }
                                                                                                }
                                                                                            });
                                                                                        }

                                                                                    } catch (e) {
                                                                                        $('.avail_' + lang).html('');
                                                                                    }
                                                                                });
                                                                            }

                                                                            function get_all_stock() {
                                                                                $(".search_product_jry").each(function () {
                                                                                    var lang = $(this).attr('lang');
                                                                                    var product_id = this.value;
                                                                                    console.log('lang=' + lang + 'product_id=' + product_id);
                                                                                    var store_id = $("#order_store_id").val();
                                                                                    if (product_id > 0 && store_id > 0) {
                                                                                        $.ajax({
                                                                                            url: "autosearch_product.php",
                                                                                            type: "POST",
                                                                                            data: {'pdt_id': product_id, 'get_pdt_qty': 1, 'store_id': store_id},
                                                                                            dataType: 'json',
                                                                                            success: function (result) {
                                                                                                var product_stock = result['stock'];
                                                                                                var order_quantity = result['order_quantity'];
                                                                                                if (order_quantity > 0 && product_stock > order_quantity) {
                                                                                                    $('.avail_' + lang).html('');
                                                                                                } else {
                                                                                                    $('.avail_' + lang).html(product_stock);
                                                                                                }
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                });
                                                                            }

                                                                            function remove_product(row_id) {
                                                                                $("#product_div_" + row_id).remove();
                                                                            }

                                                                            $('document').ready(function () {
                                                                                search_product_name();
                                                                                $("#order_store_id").change(function () {
                                                                                    get_all_stock();
                                                                                });

                                                                                $(document).on('click', '.add_more_product', function () {
                                                                                    var divToAppend = '<tr id="product_div_' + productDivCount + '">';
                                                                                    divToAppend += '<td><input type="hidden" name="map_id[' + productDivCount + ']" value="0"/><select required="required" class="search_product_jry" name="pdt_id[' + productDivCount + ']"  lang="' + productDivCount + '" id="product_sel_' + productDivCount + '"></select></td>';
                                                                                    divToAppend += '<td><span class="avail_' + productDivCount + '"></span></td><td><input required="required" autocomplete="off"  type="text" pattern="\\d*" oninvalid="this.setCustomValidity(\'Enter Number Only\')" oninput="this.setCustomValidity(\'\')"  name="pdt_qty[' + productDivCount + ']" class="form-control required" placeholder="Quantity" value=""></td>';
                                                                                    divToAppend += '<td><a href="#" class="btn btn-xs btn-danger" onclick="remove_product(' + productDivCount + ');">Remove</a>';
                                                                                    divToAppend += '</td></tr>';
                                                                                    $(".product_body").append(divToAppend);
                                                                                    search_product_name();
                                                                                    productDivCount = parseInt(productDivCount) + 1;
                                                                                });
                                                                            });
        </script>
    </body>
</html>
