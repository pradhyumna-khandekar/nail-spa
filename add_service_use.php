<?php require_once("setting.fya"); ?>


<?php
$counter = 1;
require_once 'incFirewall.fya';
$strPageTitle = "Place Order Request| Nailspa";
$strDisplayTitle = "Place Order Request| Nailspa";
$strMenuID = "6";
$strMyTable = "tblOrder";
$strMyTableID = "OrderID";
$strMyActionPage = "manager_order.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
        $product_id = isset($_POST['product_id']) && $_POST['product_id'] != '' ? $_POST['product_id'] : 0;
        $start_date = isset($_POST['start_date']) && $_POST['start_date'] != '' ? $_POST['start_date'] : '';
        $end_date = isset($_POST['end_date']) && $_POST['end_date'] != '' ? $_POST['end_date'] : '';
        $use_status = isset($_POST['use_status']) && $_POST['use_status'] != '' ? $_POST['use_status'] : 0;
        $barcode = isset($_POST['barcode']) && $_POST['barcode'] != '' ? $_POST['barcode'] : '';

        $DB = Connect();
        $insertq = "INSERT INTO product_service_use(product_id,store_id,start_date,end_date,barcode,use_status,created_date,created_by)"
                . " VALUES('" . $product_id . "','" . $strStore . "','" . $start_date . "','" . $end_date . "','" . $barcode . "',"
                . "'" . $use_status . "','" . date('Y-m-d H:i:s') . "','" . $strAdminID . "')";
        $DB->query($insertq);
        header('Location:product_service_use.php');
    }
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>

    <body>
        <div id="sb-site">   
            <?php require_once("incOpenLayout.fya"); ?>	

            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>
                <?php require_once("incLeftMenu.fya"); ?>


                <div id="page-content-wrapper">
                    <div id="page-content">
                        <?php require_once("incHeader.fya"); ?>									

                        <?php
                        if (isset($_GET['id']) && is_numeric($_GET['id'])) {
                            $order_data = select("*", "order_request", "id='" . $_GET['id'] . "' AND status=1");
                            $order_product = select("*", "order_request_product", "order_request_id='" . $_GET['id'] . "' AND status=1");

                            if (isset($order_product) && is_array($order_product) && count($order_product) > 0) {
                                foreach ($order_product as $pkey => $pvalue) {
                                    $pdt_id_arr[$pvalue['product_id']] = $pvalue['product_id'];
                                }
                            }

                            $DB = Connect();
                            if (isset($pdt_id_arr) && is_array($pdt_id_arr) && count($pdt_id_arr) > 0) {
                                $pdt_in_ids = implode(",", $pdt_id_arr);
                                $pdt_nameq = "SELECT * FROM tblNewProducts WHERE ProductID IN(" . $pdt_in_ids . ")";
                                $pdt_nameq_exe = $DB->query($pdt_nameq);
                                if ($pdt_nameq_exe->num_rows > 0) {
                                    while ($pdt_row = $pdt_nameq_exe->fetch_assoc()) {
                                        $pdt_name_data[$pdt_row["ProductID"]] = $pdt_row['ProductName'];
                                    }
                                }
                            }
                        }
                        ?>
                        <div class="panel">
                            <div class="panel-body">

                                <h3 class="title-hero">Add Product Service Use</h3>
                                <form role="form" enctype="multipart/form-data" class="form-horizontal service_use_form" action="add_service_use.php" method="post">
                                    <input type="hidden" name="product_id" value="" id="product_id"/>
                                    <input type="hidden" name="start_date" value="" id="start_date"/>
                                    <input type="hidden" name="end_date" value="" id="end_date"/>
                                    <input type="hidden" name="use_status" value="" id="use_status"/>
                                    <input type="hidden" name="order_id" value="<?php echo isset($order_data[0]['id']) ? $order_data[0]['id'] : ''; ?>"/>
                                    <div class="form-group">
                                        <div class="col-md-1">
                                            <label class="control-label" for="barcode">Barcode :</label>
                                        </div>
                                        <div class="col-md-11">
                                            <input type="text" required="required" name="barcode" id="barcode" class="form-control" placeholder="Product Barcode" value="<?php echo isset($order_data[0]['order_name']) ? $order_data[0]['order_name'] : ''; ?>" onblur="getBarcodePdt()">
                                        </div>
                                    </div>

                                    <div class="form-group product_name_div" style="display: none;">
                                        <div class="col-md-2">
                                            <label class="control-label">Product Name :</label>
                                        </div>
                                        <div class="col-md-10">
                                            <span id="product_name" class="form-control" style="border:0px;"></span>
                                        </div>

                                    </div>

                                    <div class="form-group opening_date_div" style="display: none;">
                                        <div class="col-md-2">
                                            <label class="control-label">Opening Date :</label>
                                        </div>
                                        <div class="col-md-10">
                                            <span id="opening_date" class="form-control" style="border:0px;"></span>
                                        </div>

                                    </div>

                                    <div class="form-group closing_date_div" style="display: none;">
                                        <div class="col-md-2">
                                            <label class="control-label">Closing Date :</label>
                                        </div>
                                        <div class="col-md-10">
                                            <span id="closing_date" class="form-control" style="border:0px;"></span>
                                        </div>

                                    </div>

                                    <div class="form-group submit_msg_div" style="display: none;">
                                        <b><span class="text-success" id="sub_msg"></span></b>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-11 control-label"></label>
                                        <input type="submit" class="btn ra-100 btn-primary submit_but" value="Submit" disabled="disabled">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <script type="text/javascript" src="assets/scanner/jquery-code-scanner.js"></script>
                <script>

                                                $(function () {
                                                    $('.service_use_form').codeScanner({
                                                        onScan: function ($element, code) {
                                                            $element.find('input.barcode[value=""]').first().val(code);
                                                            getBarcodePdt();
                                                        }
                                                    });
                                                });

                                                function getBarcodePdt() {
                                                    var barcode = $("#barcode").val();
                                                    if (barcode != '') {
                                                        $.ajax({
                                                            type: "post",
                                                            data: "barcode=" + barcode,
                                                            url: "get_barcode_product.php?type=service_use",
                                                            success: function (result)
                                                            {
                                                                var returnedData = JSON.parse(result);
                                                                var response = returnedData['res'];
                                                                if (response == 'success') {
                                                                    var ProductID = returnedData['ProductID'];
                                                                    var pdtName = returnedData['ProductName'];
                                                                    var ProductMRP = returnedData['ProductMRP'];
                                                                    var use_status = returnedData['use_status'];
                                                                    var opening_date = returnedData['opening_date'];
                                                                    $("#product_name").html(pdtName);
                                                                    $(".product_name_div").show();

                                                                    var opening_date = returnedData['product_start'];
                                                                    var closing_date = returnedData['product_end'];
                                                                    $("#product_id").val(ProductID);
                                                                    $("#start_date").val(opening_date);
                                                                    $("#end_date").val(closing_date);
                                                                    $("#use_status").val(use_status);
                                                                    if (use_status == 1) {
                                                                        $("#opening_date").html(opening_date);
                                                                        $(".opening_date_div").show();

                                                                        var sub_msg = 'If You Submit then Product Opening are marked...';
                                                                        $("#sub_msg").html(sub_msg);
                                                                        $(".submit_msg_div").show();

                                                                        $('.submit_but').removeAttr("disabled");
                                                                    } else if (use_status == 2) {
                                                                        $("#opening_date").html(opening_date);
                                                                        $(".opening_date_div").show();

                                                                        $("#closing_date").html(opening_date);
                                                                        $(".closing_date_div").show();

                                                                        var sub_msg = 'If You Submit then Product Closing are marked...';
                                                                        $("#sub_msg").html(sub_msg);
                                                                        $(".submit_msg_div").show();
                                                                        $('.submit_but').removeAttr("disabled");
                                                                    }

                                                                } else {
                                                                    $("#barcode").val('');
                                                                    var error_msg = returnedData['res_msg'];
                                                                    alert(error_msg);

                                                                    $("#product_name").html('');
                                                                    $(".product_name_div").hide();

                                                                    $("#opening_date").html('');
                                                                    $(".opening_date_div").hide();

                                                                    $("#closing_date").html('');
                                                                    $(".closing_date_div").hide();

                                                                    $("#sub_msg").html('');
                                                                    $(".submit_msg_div").hide();

                                                                    $(".submit_but").attr("disabled", "disabled");
                                                                }
                                                            }
                                                        });
                                                    }
                                                }
                </script>
                <?php require_once 'incFooter.fya'; ?>
            </div>
        </div>

    </body>
</html>
