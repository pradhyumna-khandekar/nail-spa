<?php require_once("setting.fya"); ?>
<?php
$counter = 1;
require_once 'incFirewall.fya';
$strPageTitle = "Place Order Request| Nailspa";
$strDisplayTitle = "Place Order Request| Nailspa";
$strMenuID = "6";
$strMyTable = "tblOrder";
$strMyTableID = "OrderID";
$strMyActionPage = "manager_order.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<!DOCTYPE html>
<html lang="en">

    <head>
<?php require_once("incMetaScript.fya"); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>

    <body>
        <div id="sb-site">   
            <?php require_once("incOpenLayout.fya"); ?>	

<?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>
<?php require_once("incLeftMenu.fya"); ?>


                <div id="page-content-wrapper">
                    <div id="page-content">
                        <?php
                        require_once("incHeader.fya");
                        require_once("order_helper.php");
                        ?>									

                        <?php /* <div class="panel">
                          <div class="panel-body">
                          <a class="btn btn-alt btn-hover btn-primary" href="create_order_pdf.php?id=<?php echo $_GET['id']; ?>">Download PDF</a>
                          </div>
                          </div> */ ?>
                        <?php
                        if (isset($_GET['id']) && is_numeric($_GET['id'])) {
                            $body_data = get_detail_data($_GET['id']);
                            echo $body_data;
                        }
                        ?>
                    </div>
                </div>
<?php require_once 'incFooter.fya'; ?>
            </div>
        </div>


    </body>
</html>
