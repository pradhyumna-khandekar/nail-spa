<?php require_once 'setting.fya'; ?>
<?php require_once 'incFirewall.fya'; ?>
<?php

$DB = Connect();
$result_set['result'] = array();
$result_set['count'] = 0;
if (isset($_POST['term'])) {

    $page_limit = 10;
    $page = isset($_POST['page']) ? $_POST['page'] : 1;
    $offset = $page_limit * ($page - 1);

    $appendq = '';

    $name = trim($_POST['term']);
    $nameQ = 'SELECT ci.ProductID as id,ci.ProductName as text,ci.ProductUniqueCode as code FROM tblNewProducts ci WHERE '
            . ' is_approve=1 AND (LOWER(ci.ProductName) LIKE "%' . strtolower($name) . '%" )'
            . ' OR (LOWER(ci.ProductUniqueCode) LIKE "%' . strtolower($name) . '%" )'
            . $appendq . '  LIMIT ' . $page_limit . ' OFFSET ' . $offset;
    $nameQ_exe = $DB->query($nameQ);
    if ($nameQ_exe->num_rows > 0) {
        $result_set['count'] = $nameQ_exe->num_rows;
        while ($name_data = $nameQ_exe->fetch_assoc()) {
            $result_set['result'][] = array(
                'id' => $name_data['id'],
                'text' => $name_data['text'] . ' ( Code:' . $name_data['code'] . ' )'
            );
        }
    }

    echo json_encode($result_set);
} else if (isset($_POST['get_pdt_qty']) && $_POST['get_pdt_qty'] == 1) {
    $result_data['stock'] = 0;
    $result_data['order_quantity'] = 0;
    $product_id = isset($_POST['pdt_id']) && $_POST['pdt_id'] != '' ? $_POST['pdt_id'] : 0;
    $store_id = isset($_POST['store_id']) && $_POST['store_id'] != '' ? $_POST['store_id'] : 0;
    if ($product_id > 0 && $store_id > 0) {
        $stock_data = select("*", "product_inventory", "status=1 AND StoreID='" . $store_id . "' AND ProductID='" . $product_id . "'");
        if (isset($stock_data) && is_array($stock_data) && count($stock_data) > 0) {
            $result_data['stock'] = $stock_data[0]['Stock'];
        }
    }

    /*
     * Get Product minimum qty for order request 
     */
    $order_qty = select("*", "tblNewProducts", "ProductID='" . $product_id . "'");
    if (isset($order_qty) && is_array($order_qty) && count($order_qty) > 0) {
        $result_data['order_quantity'] = $order_qty[0]['order_quantity'];
    }
    echo json_encode($result_data);
}
$DB->close();
?>