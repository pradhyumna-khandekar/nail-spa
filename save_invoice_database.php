<?php

function save_invoice_db($appointment_id = '') {
    $status = FALSE;
    if ($appointment_id != '') {

        // old server mysql id
        $DBIUser = 'root';
        $DBIPass = '';
        $oldServer = 'localhost';
        $originalDB = 'nailspa';

// new server mysql id
        $NewUser = 'root';
        $NewPass = '';
        $newServer = 'localhost';
        $newDB = 'nailspa_mial';
        /* $NewUser = 'nailspae_sapuser';
          $NewPass = 'O#0k3Lind)A;';
          $newServer = '109.199.97.62';
          $newDB = 'nailspae_sap'; */



        $dbold = mysql_connect($oldServer, $DBIUser, $DBIPass);
        $db_check = mysql_select_db($originalDB, $dbold);

        $conn = new mysqli($oldServer, $DBIUser, $DBIPass, $originalDB);

        $final_data = array();
        /*
         * Get Appointment Customer id and details
         */
        $get_cust_id = "SELECT * FROM " . $originalDB . ".`tblAppointments` WHERE AppointmentID='" . $appointment_id . "'";
        $get_cust_id_exe = $conn->query($get_cust_id);
        while ($cust_id_data = $get_cust_id_exe->fetch_assoc()) {
            $customer_id = $cust_id_data['CustomerID'];
            $final_data['invoice']['location_code'] = $cust_id_data['StoreID'];
            $final_data['invoice']['shift_no'] = '';
            $final_data['invoice']['created_date'] = date('Y-m-d H:i:s');
            $final_data['invoice']['rcpt_dt'] = date('Ymd', strtotime($cust_id_data['AppointmentDate']));
            $final_data['invoice']['business_dt'] = date('Ymd', strtotime($cust_id_data['AppointmentDate']));
            $final_data['invoice']['rcpt_tm'] = date('His', strtotime($cust_id_data['AppointmentCheckInTime']));

            if ($cust_id_data['StoreID'] != '') {
                /*
                 * Get Terminal Id of store
                 */
                if (isset($cust_id_data['StoreID']) && $cust_id_data['StoreID'] != '') {
                    $store_qry = "SELECT * FROM " . $originalDB . ".`tblStores` WHERE StoreID='" . $cust_id_data['StoreID'] . "'";
                    $store_qry_exe = $conn->query($store_qry);
                    while ($store_details = $store_qry_exe->fetch_assoc()) {
                        $terminal_id = $store_details['terminal_id'];
                    }
                }
            }
        }

        /*
         * If Terminal id found then only send data 
         */
        if ($terminal_id != '') {

            $final_data['invoice']['terminal_id'] = $terminal_id;

            if (isset($customer_id) && $customer_id != '') {
                $get_cust_detail = "SELECT * FROM " . $originalDB . ".`tblCustomers` WHERE CustomerID='" . $customer_id . "'";
                $get_cust_detail_exe = $conn->query($get_cust_detail);
                while ($cust_details = $get_cust_detail_exe->fetch_assoc()) {
                    $final_data['customer']['cust_name'] = $cust_details['CustomerFullName'];
                    $final_data['customer']['cust_email'] = $cust_details['CustomerEmailID'];
                    $final_data['customer']['cust_cont'] = $cust_details['CustomerMobileNo'];
                    $final_data['customer']['cust_gen'] = ($cust_details['Gender'] == 1) ? 'F' : 'M';
                    $final_data['customer']['created_date'] = date('Y-m-d H:i:s');
                }
            }

            /*
             * Get Invoice details
             */
            $invoice_qry = "SELECT * FROM " . $originalDB . ".`tblInvoiceDetails` WHERE AppointmentId='" . $appointment_id . "'";
            $invoice_qry_exe = $conn->query($invoice_qry);
            while ($invoice_details = $invoice_qry_exe->fetch_assoc()) {
                $invoice_item = $invoice_details;
                $final_data['invoice']['rcpt_num'] = $invoice_details['InvoiceId'];
                $final_data['invoice']['inv_amt'] = $invoice_details['RoundTotal'];
                $balance_rec = $invoice_details['PendingAmount'] > 0 ? $invoice_details['PendingAmount'] : 0;
                $final_data['invoice']['ret_amt'] = ($invoice_details['RoundTotal']);


                $payment_name = '';
                if ($invoice_details['Flag'] == 'CS') {
                    $payment_name = 'CASH';
                } else if ($invoice_details['Flag'] == 'C') {
                    $payment_name = 'CC';
                } else {
                    $payment_name = 'OTHERS';
                }
                $final_data['payment']['payment_name'] = $payment_name;
                $final_data['payment']['currency_code'] = 'INR';
                $final_data['payment']['exchange_rate'] = '';
                $final_data['payment']['op_cur'] = 'INR';
                $final_data['payment']['bc_exch'] = '';
                $final_data['payment']['payment_status'] = 'sales';
                $final_data['payment']['created_date'] = date('Y-m-d H:i:s');
                $paid_amount = $invoice_details['TotalPayment'];
            }

            /*
             * Get Paid Amount
             */
            $paid_qry = "SELECT * FROM " . $originalDB . ".`tblPendingPayments` WHERE AppointmentId='" . $appointment_id . "'";
            $paid_qry_exe = $conn->query($paid_qry);

            while ($paid_details = $paid_qry_exe->fetch_assoc()) {
                $final_data['payment']['tender_amount'] = $paid_details['PaidAmount'];
            }

            if (!isset($final_data['payment']['tender_amount']) || $final_data['payment']['tender_amount'] == '') {
                $final_data['payment']['tender_amount'] = $paid_amount;
            }
            $final_data['invoice_status']['op_cur'] = 'INR';
            $final_data['invoice_status']['bc_exch'] = '';
            $final_data['invoice_status']['item_status'] = 'sales';
            $final_data['invoice_status']['created_date'] = date('Y-m-d H:i:s');

            /*
             * Get Invoice product details
             */
            /* $item_qry = "SELECT * FROM " . $originalDB . ".`tblInvoiceDetails` WHERE AppointmentId='" . $appointment_id . "'";
              $item_qry_exe = $conn->query($item_qry);
              while ($item_data = $item_qry_exe->fetch_assoc()) {

              } */
            $ServiceName = $invoice_item['ServiceName'];
            /*
             * get service name
             */
            if ($ServiceName != '') {
                $service_qry = "SELECT * FROM " . $originalDB . ".`tblServices` WHERE ServiceID IN(" . $ServiceName . ")";
                $service_qry_exe = $conn->query($service_qry);
                while ($service_data = $service_qry_exe->fetch_assoc()) {
                    $service_names[$service_data['ServiceID']] = $service_data['ServiceName'];
                }
            }

            /*
             * Get service category
             */
            if ($ServiceName != '') {
                $service_cat_qry = "SELECT * FROM " . $originalDB . ".`tblProductsServices` WHERE ServiceID IN(" . $ServiceName . ")";
                $service_cat_qry_exe = $conn->query($service_cat_qry);
                while ($service_cat_data = $service_cat_qry_exe->fetch_assoc()) {
                    $service_category[$service_cat_data['ServiceID']] = $service_cat_data['CategoryID'];
                }
            }

            $cat_qry = "SELECT * FROM " . $originalDB . ".`tblCategories`";
            $cat_qry_exe = $conn->query($cat_qry);
            while ($cat_data = $cat_qry_exe->fetch_assoc()) {
                $category_name_arr[$cat_data['CategoryID']] = $cat_data['CategoryName'];
            }

            $ServiceCode = $invoice_item['ServiceCode'];
            $Qty = $invoice_item['Qty'];
            $ServiceAmt = $invoice_item['ServiceAmt'];
            $ChargeAmount = $invoice_item['ChargeAmount'];

            $service_code_arr = explode(",", $ServiceCode);
            $qty_arr = explode(",", $Qty);
            $service_amt_arr = explode(",", $ServiceAmt);
            $charge_arr = explode(",", $ChargeAmount);
            $service_id_arr = explode(",", $ServiceName);

            /*
             * Get Invoice Details and Tax Amount
             */
            $detail_qry = "SELECT * FROM " . $originalDB . ".`tblAppointmentsDetailsInvoice` WHERE AppointmentID ='" . $appointment_id . "'";
            $detail_qry_exe = $conn->query($detail_qry);
            while ($detail_data = $detail_qry_exe->fetch_assoc()) {
                $detail_arr[$detail_data['ServiceID']] = $detail_data['AppointmentDetailsID'];
            }


            $tax_qry = "SELECT * FROM " . $originalDB . ".`tblAppointmentsChargesInvoice` WHERE AppointmentID ='" . $appointment_id . "'";
            $tax_qry_exe = $conn->query($tax_qry);
            while ($tax_data = $tax_qry_exe->fetch_assoc()) {
                $service_tax[$tax_data['AppointmentDetailsID']] = $tax_data['ChargeAmount'];
            }


            /*
             * Get Service Discount
             */
            $dis_qry = "SELECT * FROM " . $originalDB . ".`tblAppointmentMembershipDiscount` WHERE AppointmentID ='" . $appointment_id . "'";
            $dis_qry_exe = $conn->query($dis_qry);
            while ($dis_data = $dis_qry_exe->fetch_assoc()) {
                $total_discount = 0;
                if ($dis_data['OfferAmount'] > 0) {
                    $total_discount += $dis_data['OfferAmount'];
                }
                if ($dis_data['MembershipAmount'] > 0) {
                    $total_discount += $dis_data['MembershipAmount'];
                }

                if (isset($service_dis_total[$dis_data['ServiceID']])) {
                    $service_dis_total[$dis_data['ServiceID']] += $total_discount;
                } else {
                    $service_dis_total[$dis_data['ServiceID']] = $total_discount;
                }
            }

            $grand_tax = 0;
            if (isset($service_code_arr) && is_array($service_code_arr) && count($service_code_arr) > 0) {
                foreach ($service_code_arr as $skey => $svalue) {
                    if ($svalue != '') {

                        $service_id = isset($service_id_arr[$skey]) ? $service_id_arr[$skey] : '';
                        $tax_id = isset($detail_arr[$service_id]) ? $detail_arr[$service_id] : '';
                        $tax_amount = isset($service_tax[$tax_id]) ? $service_tax[$tax_id] : '0';

                        $service_cat_id = isset($service_category[$service_id]) ? $service_category[$service_id] : '';
                        $category_name = isset($category_name_arr[$service_cat_id]) ? $category_name_arr[$service_cat_id] : '';

                        $disocunt = isset($service_dis_total[$service_id]) ? $service_dis_total[$service_id] : 0;
                        $service_price = isset($service_amt_arr[$skey]) ? $service_amt_arr[$skey] : 0;
                        if ($disocunt > 0) {
                            $service_price = $service_price - $disocunt;
                        }

                        $final_data['item_detail'][$skey]['item_code'] = $svalue;
                        $final_data['item_detail'][$skey]['item_name'] = isset($service_names[$service_id]) ? $service_names[$service_id] : '';
                        $final_data['item_detail'][$skey]['item_qty'] = isset($qty_arr[$skey]) ? $qty_arr[$skey] : '';
                        $final_data['item_detail'][$skey]['item_price'] = isset($service_price) && $service_price > 0 ? $service_price : '';
                        $final_data['item_detail'][$skey]['item_cat'] = $category_name;
                        $final_data['item_detail'][$skey]['item_tax'] = isset($tax_amount) ? $tax_amount : 0;
                        $final_data['item_detail'][$skey]['item_tax_type'] = 'E';
                        $final_data['item_detail'][$skey]['item_net_amt'] = $final_data['item_detail'][$skey]['item_price'] + $final_data['item_detail'][$skey]['item_tax'];

                        $grand_tax += $final_data['item_detail'][$skey]['item_tax'];
                        $final_data['item_detail'][$skey]['created_date'] = date('Y-m-d H:i:s');
                    }
                }
            }

            $tax = 0;
            $tax_qry = "SELECT sum(ChargeAmount) as sumcharge FROM " . $originalDB . ".`tblAppointmentsChargesInvoice` WHERE AppointmentId='" . $appointment_id . "'";
            $tax_qry_exe = $conn->query($tax_qry);
            while ($tax_details = $tax_qry_exe->fetch_assoc()) {
                $tax = $tax_details['sumcharge'];
            }


            $final_data['invoice']['tax_amt'] = isset($grand_tax) ? $grand_tax : 0;
            //$final_data['invoice']['ret_amt'] = $final_data['invoice']['inv_amt'] - $tax - $balance_rec;
            if (isset($final_data) && is_array($final_data) && count($final_data) > 0) {
                foreach ($final_data as $ckey => $cvalue) {
                    foreach ($cvalue as $crkey => $crvalue) {
                        if ($ckey == 'item_detail') {
                            foreach ($crvalue as $cr1key => $cr1value) {
                                $table_col[$ckey][$cr1key] = $cr1key;
                            }
                        } else {
                            $table_col[$ckey][$crkey] = $crkey;
                        }
                    }
                }
            }

         
            if (isset($final_data) && is_array($final_data) && count($final_data) > 0) {
                foreach ($final_data as $key => $row_value) {
                    $col = isset($table_col[$key]) ? $table_col[$key] : array();
                    if (isset($col) && is_array($col) && count($col) > 0) {
                        if ($key == 'item_detail') {
                            $tables = 'invoice_item';
                        } else {
                            $tables = $key;
                        }
                        $insert_query = "INSERT INTO " . $tables . " (" . implode(",", $col) . ") VALUES";
                        if (isset($row_value) && is_array($row_value) && count($row_value) > 0) {
                            if ($key == 'item_detail') {
                                foreach ($row_value as $rkey => $rvalue) {
                                    if ((count($row_value) - 1) == $rkey) {
                                        $insert_query .= " ('" . implode("','", $rvalue) . "');";
                                    } else {
                                        $insert_query .= " ('" . implode("','", $rvalue) . "'),";
                                    }
                                }
                            } else {
                                $insert_query .= " ('" . implode("','", $row_value) . "');";
                            }
                        }
                    }

                    // echo "<br>Query=" . $insert_query;
                    $new_conn = new mysqli($newServer, $NewUser, $NewPass, $newDB);
                    $get_sql_exe = $new_conn->query($insert_query);
                }
            }

            $status = TRUE;
        }
    }


    return $status;
}

?>