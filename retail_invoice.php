<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php
$DB = Connect();
$seldp = select("*", "tblAppointments", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");

$AppointmentID = $seldp[0]['AppointmentID'];
$customer_id = $seldp[0]['CustomerID'];
$store_id = $seldp[0]['StoreID'];

$customer_data = select("*", "tblCustomers", "CustomerID='" . $customer_id . "'");
$store_data = select("*", "tblStores", "StoreID='" . $store_id . "'");
$invoice_data = select("*", "tblInvoice", "AppointmentID='" . $AppointmentID . "'");
$product_data = select("*", "appointment_retail_product", "appointment_id='" . $AppointmentID . "'");
$retail_invoice = select("*", "retailinvoicedetails", "AppointmentId='" . $AppointmentID . "'");
if (isset($product_data) && is_array($product_data) && count($product_data) > 0) {
    foreach ($product_data as $pkey => $pvalue) {
        $pdt_id_arr[$pvalue['product_id']] = $pvalue['product_id'];
    }
}

if (isset($pdt_id_arr) && is_array($pdt_id_arr) && count($pdt_id_arr) > 0) {
    $pdt_in_ids = implode(",", $pdt_id_arr);
    $pdt_nameq = "SELECT * FROM tblNewProducts WHERE ProductID IN(" . $pdt_in_ids . ")";
    $pdt_nameq_exe = $DB->query($pdt_nameq);
    if ($pdt_nameq_exe->num_rows > 0) {
        while ($pdt_row = $pdt_nameq_exe->fetch_assoc()) {
            $pdt_name_data[$pdt_row["ProductID"]]['name'] = $pdt_row['ProductName'];
            $pdt_name_data[$pdt_row["ProductID"]]['code'] = $pdt_row['ProductUniqueCode'];
        }
    }
}
?>
<?php require_once("incMetaScript.fya"); ?>
<html>
    <head>
        <style type="text/css">
            @media print {
                body * {
                    visibility: hidden;
                }
                #printarea * {
                    visibility: visible;
                }
                #printarea{
                    position: absolute;
                    left: 0;
                    top: 0;
                }
            }
        </style>
        <style>
            body{color:#3a020a;;}
            p{margin:3px;}
            tr, td{font-size: 14px;}
            th{color:#795548;padding:5px;}
            h1{margin: 0;text-align:center;font-size:25px;color:#607D8B;}
            h2{font-size:16px;}
            button{font-size: 16px;
                   display: block;
                   margin: 20px 0;
                   background-color: #F44336;
                   color: #fff;
                   padding: 10px;
                   border: none;
                   border-radius: 5px;}
            .main-table{background-color:#fafafa;font-family: 'Helvetica' , sans-serif; font-size:14px; margin:0; padding:0;}
            .report-table{border-collapse: collapse;display:block;text-align:center;}
            .report-table tr{border-bottom: 2px solid #795548;}
            .report-table tr td{padding:5px;}

        </style>
    </head>
    <?php $img = 'http://nailspaexperience.com/images/test3.png' ?>
    <body id="displays">                

        <div id="printarea">
            <table class="main-table" cellspacing="0" cellpadding="0" border="0" width="100%">
                <tbody style="border:1px">
                    <tr style="background-repeat: no-repeat; background-position:50% 20%; media:print; -webkit-print-color-adjust: exact;">
                        <td>
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
                                <tbody>

                                    <tr>
                                        <td>
                                            <table id="printarea" style="BORDER-BOTTOM:#3a020a 1px solid;BORDER-LEFT:#3a020a 1px solid;BORDER-TOP:#3a020a 1px solid;BORDER-RIGHT:#3a020a 1px solid;" cellspacing="0" cellpadding="0" width="98%" align="center" media="print">
                                                <tbody>
                                                    <?php
                                                    $selSettingsData = select("*", "tblSettings", "SettingID='1'");

                                                    $selStoreGST = select("GSTNo, AccountingCode, CompanyName", "tblStores", "StoreID='" . $store_data[0]['StoreID'] . "'");
                                                    if ($selStoreGST[0]['GSTNo'] == "" || $selStoreGST[0]['GSTNo'] == " " || $selStoreGST[0]['GSTNo'] == "0") {
                                                        $strGSTno = $selSettingsData[0]['MasterGSTNo'];
                                                    } else {
                                                        $strGSTno = $selStoreGST[0]['GSTNo'];
                                                    }

                                                    if ($selStoreGST[0]['CompanyName'] == "" || $selStoreGST[0]['CompanyName'] == " " || $selStoreGST[0]['CompanyName'] == "0") {
                                                        $strCompanyName = $selSettingsData[0]['CompanyName'];
                                                    } else {
                                                        $strCompanyName = $selStoreGST[0]['CompanyName'];
                                                    }
                                                    //$strCompanyName = $selSettingsData[0]['CompanyName'];
                                                    ?>
                                                    <tr>

                                                        <td align="middle">
                                                            <input type="hidden" name="appointment_id" id="appointment_id" value="49692">
                                                            <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                                                <tbody>
                                                                    <tr>

                                                                        <td width="20%" align="left" style="padding:1%;"><img border="0" src="https://pos.nailspaexperience.com/og/images/nail_logo_new.png" width="100" height="100"><input type="hidden" name="app_id" id="app_id" value="49692"></td>
                                                                        <td width="30%">
                                                    <span style="LINE-HEIGHT:0;FONT-SIZE:20px;text-align:center;"><?= $strCompanyName ?></span>
                                                                            <br><br><span class="fs">GSTIN - <?php echo $strGSTno; ?></span></td>
                                                                        <td width="50%" align="right" style="LINE-HEIGHT:15px; padding:1%; FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal;"><b>
                                                                                <?php echo $store_data[0]['StoreName']; ?></b>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="LINE-HEIGHT:0;BACKGROUND:#3a020a;FONT-SIZE:0px;" height="5"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="middle">
                                                            <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                                                <tbody>
                                                                    <tr style="padding-left:10%;">
                                                                        <td width="76.5%">To ,</td>
                                                                        <td width="10%">Invoice No :</td>
                                                                        <td width="10%" style="float:center; padding-right:05%"><?php echo $invoice_data[0]['InvoiceID']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="50%"><b><?php echo $customer_data[0]['CustomerFullName']; ?></b></td>
                                                                        <td width="25%"></td>
                                                                        <td width="25%" style="float:center; padding-right:05%"></td>
                                                                    </tr>
                                                                    <tr>

                                                                        <td width="50%"><?php echo $customer_data[0]['CustomerEmailID']; ?>                                                                        <input type="hidden" id="email" value="kanchan@orggen.com">
                                                                        </td>

                                                                    </tr>
                                                                    <tr>
                                                                        <td width="50%"><?php echo $customer_data[0]['CustomerMobileNo']; ?></td>
                                                                        <td width="25%"></td>
                                                                        <td width="25%" style="float:center; padding-right:05%"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td height="8"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="LINE-HEIGHT:0;BACKGROUND:#3a020a;FONT-SIZE:20px;text-align:center;color: #dcc36f;" height="30"><b>Product</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="8">
                                                            <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td bgcolor="#e4e4e4" height="4"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">


                                                                <tbody>
                                                                    <tr>
                                                                        <th width="5%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">Sr</th>
                                                                        <th width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:left; padding-left:2%;">Item Description</th>
                                                                        <th width="15%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">Quantity</th>
                                                                        <th width="15%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">Amount</th>
                                                                        <th width="15%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">Tax(In %)</th>
                                                                        <th width="15%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">Tax Amount</th>
                                                                        <th width="15%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">Sub Total</th>
                                                                    </tr>

                                                                    <?php
                                                                    $pcount = 1;
                                                                    if (isset($product_data) && is_array($product_data) && count($product_data) > 0) {
                                                                        foreach ($product_data as $pkey => $pvalue) {
                                                                            ?>
                                                                            <tr>
                                                                                <td width="5%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;text-align: center;"><?php echo $pcount; ?></td>
                                                                                <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px; text-align:left; padding-left:2%;">
                                                                                    <?php echo isset($pdt_name_data[$pvalue['product_id']]) ? $pdt_name_data[$pvalue['product_id']]['name'] : ''; ?>
                                                                                </td>
                                                                                <td width="15%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;text-align:center;"><?php echo $pvalue['quantity']; ?></td>
                                                                                <td width="15%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;text-align:center;"><?php echo $pvalue['amount']; ?></td>
                                                                                <td width="15%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;text-align:center;"><?php echo floatval($pvalue['tax_per']); ?></td>
                                                                                <td width="15%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;text-align:center;"><?php echo floatval($pvalue['tax_amount']); ?></td>
                                                                                <td width="15%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;text-align:center;"><?php echo $pvalue['amount'] + $pvalue['tax_amount']; ?></td>
                                                                            </tr>

                                                                            <?php
                                                                            $pcount++;
                                                                        }
                                                                    }
                                                                    ?>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>




                                                    <tr>
                                                        <td height="8">
                                                            <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td bgcolor="#e4e4e4" height="4"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td width="50%">&nbsp;</td>
                                                                        <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Sub Total</td>
                                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%;"><?php echo $retail_invoice[0]['sub_total']; ?></td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td width="50%">&nbsp;</td>
                                                                        <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Discount</td>
                                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%;">-<?php echo $retail_invoice[0]['offer_discount']; ?></td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td width="50%">&nbsp;</td>
                                                                        <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Tax Amount	</td>
                                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%;"><?php echo floatval($retail_invoice[0]['tax_amount']); ?></td>
                                                                    </tr>

                                                                    <?php
                                                                    $sept = select("PendingAmount,pending_amt_apt_id", "retailinvoicedetails", "AppointmentId='" . DecodeQ($_GET['uid']) . "'");
                                                                    $pendamt = $sept[0]['PendingAmount'];
                                                                    if ($pendamt != "0" && $pendamt != "") {
                                                                        $total = $total + $pendamt;
                                                                        $pending_apt_id = $sept[0]['pending_amt_apt_id'];
                                                                        /*
                                                                         * get pending amount invoice details
                                                                         */
                                                                        if ($pending_apt_id != '' && $pending_apt_id != NULL) {
                                                                            $pending_date_data = select("AppointmentID,AppointmentDate", "tblAppointments", "AppointmentId='" . $pending_apt_id . "'");
                                                                            if (isset($pending_date_data) && is_array($pending_date_data) && count($pending_date_data) > 0) {
                                                                                $pending_amt_date = $pending_date_data[0]['AppointmentDate'];
                                                                            }
                                                                        }
                                                                        ?>
                                                                        <tr id="pendingpayment"> 
                                                                            <td>&nbsp;</td>
                                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Pending Amount
                                                                                <b><?php echo isset($pending_amt_date) ? '(Invoice Date: ' . $pending_amt_date . ')' : ''; ?> </b>
                                                                            </td>
                                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%" ><?= $pendamt ?></td>

                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    ?>

                                                                    <tr>
                                                                        <td>&nbsp;</td>
                                                                        <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Total</td>
                                                                        <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%" id="totalvalue"><?php echo $retail_invoice[0]['RoundTotal']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>&nbsp;</td>
                                                                        <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Round Off</td>
                                                                        <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%;"><?php echo $retail_invoice[0]['RoundTotal']; ?></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td height="8">
                                                            <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td bgcolor="#e4e4e4" height="4"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td style="display:none" id="payment">
                                                            <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">


                                                                <tbody>

                                                                    <tr>
                                                                        <td colspan="3" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:16px;FONT-WEIGHT:bold; text-align:center;">Payments</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                        <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Cash</td>
                                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%;"><input type="number" id="paymentid" name="cashamt" value="0" onkeyup="test()"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>&nbsp;</td>
                                                                        <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Total Payment</td>
                                                                        <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input id="totalpayment" name="totalpayment"></td>
                                                                    </tr>


                                                                </tbody>

                                                            </table>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td id="payment">
                                                            <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                                                <?php
                                                                $paidamt = $retail_invoice[0]['PaidAmount'];
                                                                $RoundTotal = $retail_invoice[0]['RoundTotal'];
                                                                $pendingamt = $RoundTotal - $paidamt;
                                                                ?>
                                                                <tbody>
                                                                    <tr>
                                                                        <td colspan="3" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:16px;FONT-WEIGHT:bold; text-align:center;">Payments</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                        <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Paid Amount</td>
                                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%"><?php echo number_format($paidamt, 2) ?></td>
                                                                    </tr>
                                                                    <?php if ($pendingamt > 0) { ?>
                                                                        <tr>
                                                                            <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                            <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Pending Amount</td>
                                                                            <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:right; padding-right:05%"><?php echo number_format($pendingamt, 2) ?></td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="2" style="padding: 0px;background: #3a020a;">
                                                            <table width="100%" cellspacing="0" cellpadding="0">
                                                                <tbody><tr>
                                                                        <td colspan="2" style="background-color: #3a020a;padding: 15px 0;text-align: center;color: #fff;">
                                                                            <p class="text-center" style="font-size:14px;"></p>
                                                                            <p style="color:#dcc36f;">11+ LOCATIONS | T1 & T2 MUMBAI AIRPORT &nbsp;| MUMBAI | KOCHI </p>
                                                                            <p style="color: #dcc36f; text-align:center;"><strong>GO GREEN GO PAPERLESS</strong></p>

                                                                        </td>
                                                                    </tr>
                                                                </tbody></table>
                                                        </td>
                                                    </tr>


                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>


<table align="right">
    <tr align="right">

        <!--<a href="appointment_invoice.php" class="btn btn-primary btn-sm " style="float: right;">Print</a>-->
        <?php
        $seldp = select("*", "tblAppointments", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
        $STORED = $seldp[0]['StoreID'];
        ?>
        <td align="right"><button onclick="sendmail()" class="btn btn-success">Send Email to Client<div class="ripple-wrapper"></div></button></td>

        <td align="right">
            <button onclick="printDiv('printarea')" class="btn btn-blue-alt">Print<div class="ripple-wrapper"></div></button>
        </td>

        <td align="right">
            <a href="appointment_invoice.php" class="btn btn-primary btn-sm" style="float: right;">Go Back</a>
        </td>
        <td align="right">
            <?php // require_once("incBillingSMS.php");  ?>
        </td>

    </tr>
</table>

<script>
    var retVal = confirm("Are You Sure you want to send SMS and Email ?");
    if (retVal == true) {
        sendmail();
        sendSMS();

    }

    function sendSMS()
    {
        $.ajax({
            type: "GET",
            data: "uid=" + '<?php echo EncodeQ($AppointmentID) ?>',
            url: "incBillingSMS.php",
            success: function (result1)
            {

            }
        });
    }

    function printDiv(divName)
    {
        var divContents = $("#printarea").html();
        var abc = $("#printarea1").html();
        var printWindow = window.open('', '', 'height=600,width=800');
        printWindow.document.write('<html><head><title>Invoice</title>');
        printWindow.document.write('</head><body >');
        printWindow.document.write(divContents);
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        printWindow.print();

    }

    function sendmail()
    {
        var divContents = encodeURIComponent($("#printarea").html());
        var email = $("#email").val();
        var app = $("#app_id").val();
        if (email != '')
        {
            $.ajax({
                type: "post",
                data: "divContents=" + divContents + "&email=" + email + "&app=" + app,
                url: "sendinvoice.php",
                success: function (result1)
                {
                    alert(result1)


                }
            });
        } else
        {
            alert('Email Id is blank')
        }

    }
</script>