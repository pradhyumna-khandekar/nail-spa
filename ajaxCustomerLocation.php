<?php require_once 'setting.fya'; ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');


/** PHPExcel_IOFactory */
require_once dirname(__FILE__) . '/Classes/PHPExcel/IOFactory.php';

$filename = "uploads/".$_POST["excelfile"];
$update_current = $_POST["update_current"];


if (!file_exists($filename)) {
	exit("<font color='red'>File not uploaded properly. Please re-try</font>" . EOL);
}

$DB = Connect();

function Excel_Date_Converter($strExcel_Date)
{
	$UNIX_DATE = ($strExcel_Date - 25569) * 86400;
	return $UNIX_DATE;
}

$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$objPHPExcel = $objReader->load($filename);
foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) 
{
	if($worksheet->getTitle()=="Sheet1")
	{
		foreach ($worksheet->getRowIterator() as $row) 
		{
			if($row->getRowIndex()!=1)
			{
				

				$CustomerLocation = "";
				$Act = "";
				
				
				$CustomerLocation = GetContents($row, $worksheet, "A");
				if(!IsNull($CustomerLocation))
				{
					$CustomerLocation = GetContents($row, $worksheet, "A");
					$Act = GetContents($row, $worksheet, "B");
								
				
					$seldata="SELECT location_name from tblCutomerLocation where location_name='".$CustomerLocation."'";
					$RS = $DB->query($seldata);
			if ($RS->num_rows > 0) 
					{
						$DB->close();
						echo('<div class="alert alert-danger alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
								</button>
								<strong>Record Already Exist for '.$CustomerLocation.'</strong>
							</div>');
					}
					else
					{
						
						
						$sql = "INSERT into tblCutomerLocation (location_name,act) Values ('".addslashes($CustomerLocation)."', '$Act' )";
						
						if ($DB->query($sql) === TRUE) 
						{
							echo('<div class="alert alert-success alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
							</button>
							<strong>Saved successfully</strong>
						</div>');
						}
						else
						{
							echo "Error: " . $sql . "<br>" . $conn->error;
						}
								
						
						
					}
			
				}
				else
				{
					echo('<div class="alert alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<strong>First Name missing </strong>
					</div>');
				}

				
		
						
			}
			else
			{
									
			}	
					
		}
	}
	else
	{
		echo('<div class="alert alert-danger alert-dismissible fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
				</button>
				<strong>Worksheet does not have sheet 1</strong>
			</div>');
	}	
}	

$DB->close();
// unlink($filename);

echo('<a href="democustomerlocationadd.php" class="btn btn-default btn-lg">Go back to Manage Customers<a>');	

function GetContents($row, $worksheet, $colindex)
{
	$cellIterator = $row->getCellIterator();
	$cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
	$cell = $worksheet->getCell($colindex.$row->getRowIndex());
	$value = $cell->getCalculatedValue();
	return $value;
}
