<?php
$InvoiceId = base64_decode($_GET['int']);
$name = base64_decode($_GET['nrt']);
$Mobile = base64_decode($_GET['mob']);
$CustomerID = base64_decode($_GET['cut']);
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Nailspa Feedback</title>
        <!-- for-mobile-apps -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
        <meta name="keywords" content="Feedback Widget Responsive, Login form web template, Sign up Web Templates, Flat Web Templates, Login signup Responsive web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <!-- //for-mobile-apps -->
        <link rel="shortcut icon" href="../assets/images/icons/favicon.png">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    </head>
    <body>
        <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>

        <style type="text/css">
            .advt {z-index: 999999999999;
                   background-color: #ffffffa3;
                   width: 728px;
                   height: 100px;
                   color: black;
                   font-weight: bold;
                   font-size: 20px;
                   position: fixed;
                   left: 10px;
                   margin-top: 20px;
                   bottom: 1px;
            }
            .adsense_btn_close,.adsense_btn_info{font-size:12px;color:#fff;height:20px;width:20px;vertical-align:middle;text-align:center;background:#000;top:4px;left:4px;position:absolute;z-index:99999999;font-family:Georgia;cursor:pointer;line-height:18px}
        </style>



    </div>
    <div class="clear"></div>
<body>
    <div class="content">
        <!---728x90--->
        <div class="main">
            <img src="images/nailspa.png" style="width:100px; display:block; margin:0 auto">
            <form action="feedback.php" method="post">
                <h3>Dear <?php echo $name; ?> </h3>
                <p>Help us to serve you better by giving your valuable suggestions and feedback.</p>
                <!--<h5>Invoice Number</h5>
                        <input type="text" name='invoice_no' value="Your Invoice No." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Type here';}" required="">
                <h5>Name</h5>
                        <input type="text" name='name' value="Your Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Type here';}" required="">
                <h5>Date</h5>
                        <input type="date" name='date' value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Type here';}" required="">
                <h5>Email</h5>
                        <input type="text" name='email' value="johnkeith@mail.com" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'johnkeith@mail.com';}" required="">
                <h5>Serviced By</h5>
                        <input type="text" name='servie_by' value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Type here';}" required="">-->

                <h3>Please rate the following</h3>
                <input type="hidden" name='invoice_no' value="<?php echo $InvoiceId; ?>" >
                <input type="hidden" name='Customer_name' value="<?php echo $name; ?>" >
                <input type="hidden" name='mobile' value="<?php echo $Mobile; ?>" >
                <input type="hidden" name='CustomerID' value="<?php echo $CustomerID; ?>" >
                <h5>Service Quality</h5>
                <div class="radio-btns">
                    <div class="swit">								
                        <div class="check_box_one"> <div class="radio"> <label><input type="radio" class="feedback" name="service_quality" value="Excellent"><i></i>Excellent</label> </div></div>
                        <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="service_quality" value="Good"><i></i>Good</label> </div></div>
                        <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="service_quality" value="Average"><i></i>Average</label> </div></div>
                        <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="service_quality" value="Poor"><i></i>Poor</label> </div></div>
                        <div class="clear"></div>
                    </div>
                </div>
                <h5>Value for money</h5>
                <div class="radio-btns">
                    <div class="swit">								
                        <div class="check_box_one"> <div class="radio"> <label><input type="radio" class="feedback" name="value_for_money" value="Excellent"><i></i>Excellent</label> </div></div>
                        <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="value_for_money" value="Good"><i></i>Good</label> </div></div>
                        <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="value_for_money" value="Average"><i></i>Average</label> </div></div>
                        <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="value_for_money" value="Poor"><i></i>Poor</label> </div></div>
                        <div class="clear"></div>
                    </div>
                </div>
                <h5>Salon Atmosphere</h5>
                <div class="radio-btns">
                    <div class="swit">								
                        <div class="check_box_one"> <div class="radio"> <label><input type="radio" class="feedback" name="salon_atmosphere" value="Excellent"><i></i>Excellent</label> </div></div>
                        <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="salon_atmosphere" value="Good"><i></i>Good</label> </div></div>
                        <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="salon_atmosphere" value="Average"><i></i>Average</label> </div></div>
                        <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="salon_atmosphere" value="Poor"><i></i>Poor</label> </div></div>
                        <div class="clear"></div>
                    </div>
                </div>
                <h5>Staff Presentation & Attitude</h5>
                <div class="radio-btns">
                    <div class="swit">								
                        <div class="check_box_one"> <div class="radio"> <label><input type="radio" class="feedback" name="staff_presentation_attitude" value="Excellent"><i></i>Excellent</label> </div></div>
                        <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="staff_presentation_attitude" value="Good"><i></i>Good</label> </div></div>
                        <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="staff_presentation_attitude" value="Average"><i></i>Average</label> </div></div>
                        <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="staff_presentation_attitude" value="Poor"><i></i>Poor</label> </div></div>
                        <div class="clear"></div>
                    </div>
                </div>
                <h5>Cleanliness</h5>
                <div class="radio-btns">
                    <div class="swit">								
                        <div class="check_box_one"> <div class="radio"> <label><input type="radio" class="feedback" name="cleanliness" value="Excellent"><i></i>Excellent</label> </div></div>
                        <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="cleanliness" value="Good"><i></i>Good</label> </div></div>
                        <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="cleanliness" value="Average"><i></i>Average</label> </div></div>
                        <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="cleanliness" value="Poor"><i></i>Poor</label> </div></div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div id="div" style="display:none">
                    <h5>Whould You Recommend Your Friends and Family</h5>
                    <div class="radio-btns">
                        <div class="swit">								
                            <div class="check_box_one"> <div class="radio"> <label><input type="radio" name="recommend" value="Yes"><i></i>Yes</label> </div></div>
                            <div class="check_box"> <div class="radio"> <label><input type="radio" name="recommend" value="No"><i></i>No</label> </div></div>

                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                <input type="submit" value="Send Feedback">
            </form>
        </div>

    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $('.feedback').click(function () {
            //if($(':radio[name=rdQueen]:checked, :radio[name=rdFruit]:checked, :radio[name=rdFruit1]:checked, :radio[name=rdFruit2]:checked, :radio[name=rdFruit2]:checked ').length == 3)

            var count1 = $((':radio[name=service_quality]:checked, :radio[name=value_for_money]:checked, :radio[name=salon_atmosphere]:checked, :radio, :radio[name=staff_presentation_attitude], :radio[name=cleanliness]').val());
            alert(count1);
            var count = $('input:radio:checked').length;

            if (count == 3) {
                document.getElementById("div").style.display = "block";
            }
        });
    </script>
</body>
</html>
