<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
	$strPageTitle = "Campaign Analysis | Nailspa";
	$strDisplayTitle = "Campaign Analysis Report Nailspa";
	$strMenuID = "2";
	$strMyTable = "tblStoreStock";
	$strMyTableID = "StoreStockID";
	$strMyField = "";
	$strMyActionPage = "ReportCampaignAnalysis.php";
	$strMessage = "";
	$sqlColumn = "";
	$sqlColumnValues = "";
	
// code for not allowing the normal admin to access the super admin rights	
	if($strAdminType!="0")
	{
		die("Sorry you are trying to enter Unauthorized access");
	}
// code for not allowing the normal admin to access the super admin rights	


	
	if ($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$strStep = Filter($_POST["step"]);
		
		if($strStep=="add")
		{
			
		}
		
		if($strStep=="edit")
		{
			
		}
	}	
?>


<?php

	if(isset($_GET["toandfrom"]))
	{
		$strtoandfrom = $_GET["toandfrom"];
		$arraytofrom = explode("-",$strtoandfrom);
		
		$from = $arraytofrom[0];
		$datetime = new DateTime($from);
		$getfrom = $datetime->format('Y-m-d');
		
		
		$to = $arraytofrom[1];
		$datetime = new DateTime($to);
		$getto = $datetime->format('Y-m-d');

		if(!IsNull($from))
		{
			$sqlTempfrom = " and Date(tblInvoiceDetails.OfferDiscountDateTime)>='".$getfrom."'";
			$sqlTempfrom1 = " and Date(tblAppointmentMembershipDiscount.DateTimeStamp)>='".$getfrom."'";
			$sqlTempfrom2 = " and Date(tblGiftVouchers.RedempedDateTime)>='".$getfrom."'";
			$sqlTempfrom3 = " and Date(tblPendingPayments.DateTimeStamp)>='".$getfrom."'";
			$sqlTempfrom4 = " and Date(tblGiftVouchers.Date)>='".$getfrom."'";
			$sqlTempfrom5 = " and Date(tblCustomers.RegDate)>='".$getfrom."'";
			$sqlTempfrom6 = " and Date(tblCustomers.RegDate)<'".$getfrom."'";
			$sqlTempfrom7 = " and Date(tblAppointments.AppointmentDate)>='".$getfrom."'";
		}

		if(!IsNull($to))
		{
			$sqlTempto = " and Date(tblInvoiceDetails.OfferDiscountDateTime)<='".$getto."'";
			$sqlTempto1 = " and Date(tblAppointmentMembershipDiscount.DateTimeStamp)<='".$getto."'";
			$sqlTempto2 = " and Date(tblGiftVouchers.RedempedDateTime)<='".$getto."'";
			$sqlTempto3 = " and Date(tblPendingPayments.DateTimeStamp)<='".$getto."'";
			$sqlTempto4 = " and Date(tblGiftVouchers.Date)<='".$getto."'";
			$sqlTempto5 = " and Date(tblCustomers.RegDate)<='".$getto."'";
			$sqlTempto7 = " and Date(tblAppointments.AppointmentDate)<='".$getto."'";
			
		}
	}
	
	if(!IsNull($_GET["Store"]))
	{
		$strStoreID = $_GET["Store"];
		
			$sqlTempStore = " StoreID='$strStoreID'";
		
	}
	

?>	


<!DOCTYPE html>
<html lang="en">

<head>
	<?php require_once("incMetaScript.fya"); ?>
	
	<script type="text/javascript" src="assets/widgets/datepicker/datepicker.js"></script>
	<script type="text/javascript">
		/* Datepicker bootstrap */

		$(function() {
			"use strict";
			$('.bootstrap-datepicker').bsdatepicker({
				format: 'mm-dd-yyyy'
			});
		});
	</script>
	<script>
		function printDiv(divName) 
		{
	
	    var divToPrint = document.getElementById("printdata");
        var htmlToPrint = '' +
        '<style type="text/css">' +
        'table th, table td {' +
        'border:1px solid #000;' +
        'padding;0.5em;' +
        '}' +
        '</style>';
		htmlToPrint += divToPrint.outerHTML;
		newWin = window.open("");
		newWin.document.write(htmlToPrint);
		newWin.print();
		newWin.close();
			// var printContents = document.getElementById(divName);
			// var originalContents = document.body.innerHTML;

			// document.body.innerHTML = printContents;

			// window.print();

			// document.body.innerHTML = originalContents; 
		}
		
</script>
	<script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker.js"></script>
	<script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker-demo.js"></script>
	<script type="text/javascript" src="assets/widgets/daterangepicker/moment.js"></script>
	<script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
</head>

<body>
    <div id="sb-site">
        
	<?php // require_once("incOpenLayout.fya");     ?>
        <!----------commented by gandhali 5/9/18---------------->
		
		
        <?php require_once("incLoader.fya"); ?>
		
        <div id="page-wrapper">
            <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>
            
				<?php require_once("incLeftMenu.fya"); ?>
			
            <div id="page-content-wrapper">
                <div id="page-content">
                    
					<?php require_once("incHeader.fya"); ?>
					

                    <div id="page-title">
                        <h2><?=$strDisplayTitle?></h2>
                    </div>
<?php

if(!isset($_GET["uid"]))
{

?>					
					
                    <div class="panel">
						<div class="panel">
							<div class="panel-body">
							
								
								<div class="example-box-wrapper">
									<div class="tabs">
									
										<div id="normal-tabs-1">
										
											<span class="form_result">&nbsp; <br>
											</span>
											
											<div class="panel-body">
											
												<form method="get" class="form-horizontal bordered-row" role="form">
													
													<div class="form-group"><label for="" class="col-sm-4 control-label">Select Date Range</label>
														<div class="col-sm-4">
															<div class="input-prepend input-group">
																<span class="add-on input-group-addon">
																	<i class="glyph-icon icon-calendar"></i>
																</span> 
																<input type="text" name="toandfrom" id="daterangepicker-example" class="form-control" value="<?=$strtoandfrom?>">
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-4 control-label">Select Store</label>
														<div class="col-sm-4">
															<select name="Store" class="form-control">
																<option value="0">All</option>
																<?
                                                    $selp=select("*","tblStores","Status='0'");
													foreach($selp as $val)
													{
														$strStoreName = $val["StoreName"];
														$strStoreID = $val["StoreID"];
														$store=$_GET["Store"];
														if($store==$strStoreID)
														{
															?>
														<option  selected value="<?=$strStoreID?>" ><?=$strStoreName?></option>														
<?php                   
														}
														else
														{
															?>
														<option value="<?=$strStoreID?>" ><?=$strStoreName?></option>														
<?php                   
														}

													}
?>
															</select>
														</div>
													</div>
														<div class="form-group">
														<label class="col-sm-4 control-label">Select Percentage</label>
														<div class="col-sm-4">
														<?php 
														$per=$_GET["per"];
														?>
															<select name="per" class="form-control">
																<option value="0" <?php if($per=='0'){ ?> selected <?php } ?>>Without Percentage</option>
															    <option value="1" <?php if($per=='1'){ ?> selected <?php } ?>>Percentage</option>
															</select>
														</div>
													</div>
													
													<div class="form-group"><label class="col-sm-3 control-label"></label>
														<button type="submit" class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
														&nbsp;&nbsp;&nbsp;
														<a class="btn btn-link" href="ReportCampaignAnalysis.php">Clear All Filter</a>
														&nbsp;&nbsp;&nbsp;
															<button onclick="printDiv('printarea')" class="btn btn-blue-alt">Print<div class="ripple-wrapper"></div></button>
															
														<!--<a class="btn btn-border btn-alt border-primary font-primary" href="ExcelExportData.php?from=<?=$getfrom?>&to=<?=$getto?>" title="Excel format 2016"><span>Export To Excel</span><div class="ripple-wrapper"></div></a>
														-->

													</div>
												</form>
												
												<br>
											
												<?php
													if($_GET["toandfrom"]!="")
													{
														$storr=$_GET["Store"];
													if($storr=='0')
													{
														$storrrp='All';
													}
													else{
													$stpp=select("StoreName","tblStores","StoreID='".$storr."'");
				                                   $StoreName=$stpp[0]['StoreName'];
														$storrrp=$StoreName;
													}
														
												?>
													<div id="printdata">
														<h3 class="title-hero">Date Range selected : FROM - <?=$getfrom?> / TO - <?=$getto?> / Store Filter selected : <?=$storrrp?> </h3>
														<?
															// echo $getfrom."<br>";
															// echo $storrrp."<br>"
														?>
														<br>
<?php
$DB = Connect();
$counter = 0;
$per=$_GET["per"];
		
?>
		<div class="panel">
			<div class="panel-body">
				
				<div class="example-box-wrapper">
					<div class="scroll-columns">
					

					
						<table class="table table-bordered table-striped table-condensed cf" width="100%">
						                                   <thead class="cf">
														 
																<tr>
																	
																	<th class="numeric"><center>Campaign Name</center></th>
																	<th class="numeric"><center>Activity Center</center></th>
																	<th class="numeric"><center>Existing Client</center></th>
																	<th><center>New Aquisition</center></th>
																	<th class="numeric"><center>Total Customer Count</center></th>
																	<th class="numeric"><center>Bill Amount</center></th>
																	<th class="numeric"><center>Store</center></th>
																	<?php
																	if($per!='0')
																	{
																		?>
																<th class="numeric" id="percol" ><center>Existing %</center></th>
																		<?php
																	}
																	?>
																	<?php
																	if($per!='0')
																	{
																		?>
																<th class="numeric" id="percol" ><center>New Aquisition %</center></th>
																		<?php
																	}
																	
																	?>
																<th class="numeric"><center>cost per client</center></th>
																<th class="numeric"><center>ARPU</center></th>
																
																	
																</tr>
															 
																
															</thead>
							                              
															<tbody>
							
<?php
$storr=$_GET["Store"];


	if(!empty($storr))
	{
		      $sqldetailsdtyu="select * from tblCampaignActivity where Date(StartDate)>=Date('".$getfrom."') and Date(EndDate)<=Date('".$getto."') and StoreID='$storr'";
			  //echo $sqldetailsdtyu;
              $RSservicetqdtcc = $DB->query($sqldetailsdtyu);
             if ($RSservicetqdtcc->num_rows > 0) 
				{    
					while($rowservicetqtty = $RSservicetqdtcc->fetch_assoc())
					{
						$ActivityID=$rowservicetqtty['ActivityID'];
						$CampaignName=$rowservicetqtty['CampaignName'];
						$OfferID=$rowservicetqtty['OfferID'];
						$Qty=$rowservicetqtty['Qty'];
						$TotalCost=$rowservicetqtty['TotalCost'];
						$sepss=select("Name","tblActivityCostCenter","ActivityCostCenterID='".$ActivityID."'");
		                $Name=$sepss[0]['Name'];
						
						
								   $sqldetailsdt="select COUNT(tblCustomers.CustomerID) from tblCustomers left join tblAppointments on tblAppointments.CustomerID=tblCustomers.CustomerID right join tblInvoiceDetails on tblInvoiceDetails.AppointmentId=tblAppointments.AppointmentID where tblAppointments.StoreID='".$storr."' AND tblAppointments.IsDeleted !=  '1' AND tblAppointments.FreeService !=  '1' and tblAppointments.Status='2' and Date(tblCustomers.RegDate) not between Date('".$getfrom."') and Date('".$getto."') and tblAppointments.offerid!='0' and tblAppointments.offerid='".$OfferID."' $sqlTempfrom $sqlTempto";
								   $RSservicetqdt = $DB->query($sqldetailsdt);
                                   //echo $sqldetailsdt;
												if ($RSservicetqdt->num_rows > 0) 
												{
													$ty=0;
													while($rowservicetqt = $RSservicetqdt->fetch_assoc())
													{
													$oldcustcnt=$rowservicetqt['COUNT(tblCustomers.CustomerID)'];
													
													}
												}
						   
								   if($oldcustcnt=='' || $oldcustcnt=='0')
									{
										$oldcustcnt=0;
									}
								 if($oldcustcnt=="")
									 {
										 $oldcustcnt=0;
									 }
									 else
									 {
										 $oldcustcnt=$oldcustcnt;
									 }	
									$totaloldcustcnt += $oldcustcnt;	
							$SelectrM="SELECT COUNT(tblCustomers.CustomerID) FROM tblCustomers left join tblAppointments on tblAppointments.CustomerID=tblCustomers.CustomerID right join tblInvoiceDetails on tblInvoiceDetails.AppointmentId=tblAppointments.AppointmentID WHERE tblAppointments.StoreID='$storr' AND tblAppointments.IsDeleted !=  '1' AND tblAppointments.FreeService !=  '1' and tblAppointments.Status='2' and tblAppointments.offerid!='0' $sqlTempfrom5 $sqlTempto5 and tblAppointments.offerid='".$OfferID."' $sqlTempfrom $sqlTempto";

		                     //  $SelectrM="SELECT COUNT(tblCustomers.CustomerID) FROM tblCustomers left join tblAppointments on tblAppointments.CustomerID=tblCustomers.CustomerID right join tblInvoiceDetails on tblInvoiceDetails.AppointmentId=tblAppointments.AppointmentID WHERE tblAppointments.StoreID='".$storr."' AND tblAppointments.IsDeleted !=  '1' AND tblAppointments.FreeService !=  '1' and tblAppointments.Status='2' $sqlTempfrom5 $sqlTempto5 $sqlTempfrom $sqlTempto";
					
						       $RSservicetM = $DB->query($SelectrM);
								if ($RSservicetM->num_rows > 0) 
								{
									$ty=0;
									while($rowservicetM = $RSservicetM->fetch_assoc())
									{
										$custcnt=$rowservicetM ['COUNT(tblCustomers.CustomerID)'];
									}
								}
					   
							   if($custcnt=='' || $custcnt=='0')
								{
									$custcnt=0;
								}
						
								if($custcnt=="")
								{
									$custcnt=0;
								}
								else
								{
									$custcnt=$custcnt;
								}
								$totalcustcnt +=$custcnt;
								
								$Totalcustomer=$custcnt+$oldcustcnt; 
								$costperclient=$TotalCost/$Totalcustomer;
								$exp=($oldcustcnt/$Totalcustomer)*100;
								$new=($custcnt/$Totalcustomer)*100;
								$sep=select("*","tblStores","StoreID='".$storr."'");
		                        $storename=$sep[0]['StoreName'];
								
								
								if(!empty($storr))
								{
								$sql="select tblAppointments.AppointmentID,tblInvoiceDetails.CustomerFullName, tblInvoiceDetails.RoundTotal, tblAppointments.StoreID,tblInvoiceDetails.OfferDiscountDateTime
								from tblAppointments 
								left join tblInvoiceDetails on
								tblAppointments.AppointmentID=tblInvoiceDetails.AppointmentId
								where tblAppointments.IsDeleted!='1' and tblAppointments.StoreID='".$storr."' and tblInvoiceDetails.OfferDiscountDateTime!='NULL' and tblInvoiceDetails.AppointmentId!='NULL' and Status='2' $sqlTempfrom $sqlTempto";
								}
								else
								{
								$sql="select tblAppointments.AppointmentID,tblInvoiceDetails.CustomerFullName, tblInvoiceDetails.RoundTotal, tblAppointments.StoreID,tblInvoiceDetails.OfferDiscountDateTime
								from tblAppointments 
								left join tblInvoiceDetails on
								tblAppointments.AppointmentID=tblInvoiceDetails.AppointmentId
								where tblAppointments.IsDeleted!='1' and tblInvoiceDetails.OfferDiscountDateTime!='NULL' and tblInvoiceDetails.AppointmentId!='NULL' and Status='2' $sqlTempfrom $sqlTempto";
								}
								$RS = $DB->query($sql);
								if ($RS->num_rows > 0) 
								{
									$counter = 0;

									while($row = $RS->fetch_assoc())
									{
										$RoundTotal = $row["RoundTotal"];
										if($RoundTotal =="")
										{
											$RoundTotal ="0.00";
										}
										else
										{
										
											$RoundTotal = $RoundTotal;
											
										}
										$TotalRoundTotal += $RoundTotal;
									}
								}
								$arpu=$TotalRoundTotal/$Totalcustomer;
								if($costperclient=="")
								{
									$costperclient=0;
								}
								if($arpu=="")
								{
									$arpu=0;
								}
						?>
								<tbody>
								<tr>
								 <td class="numeric"><center><?=$CampaignName?></center></td>
		                        <td class="numeric"><center><?=$Name?></center></td>
								<td class="numeric"><center><?=$oldcustcnt?></center></td>
								<td class="numeric"><center><?=$custcnt?></center></td>
								<td class="numeric"><center><?php 
								                       
								  echo $Totalcustomer;
								                       ?></center></td>
							  <td><center><?="Rs. ".round($TotalRoundTotal,2)?></center></td>
							  <td class="numeric"><center><?=$storename?></center></td>
							  <?php
																	if($per!='0')
																	{
																		?>
																 <td class="numeric"><center><?=round($exp,2)?></center></td>
																		<?php
																	}
																	?>
																	<?php
																	if($per!='0')
																	{
																		?>
																 <td class="numeric"><center><?=round($new,2)?></center></td>
																		<?php
																	}
																	?>
																	 <td class="numeric"><center><?=round($costperclient,2)?></center></td>
																	  <td class="numeric"><center><?=round($arpu,2)?></center></td>
							  
								</tr></tbody>
								<?php
					                }
					}
					else
					{
						?>
					<tbody>
					<tr>
					<td class="numeric"><center></center></td>
					<td class="numeric"><center>No Record Found</center></td>
					<td class="numeric"><center></center></td>
					<td class="numeric"><center></center></td>
					<td class="numeric"><center></center></td>
					<td class="numeric"><center></center></td>
					<td class="numeric"><center></center></td>
					<td class="numeric"><center></center></td>
					<td class="numeric"><center></center></td>
					<td class="numeric"><center></center></td>
					<td class="numeric"><center></center></td>
					</tr></tbody>
					<?php
					}
					
					
					
							
		
	}
	else
	{
	    $sqldetailsdtyu="select * from tblCampaignActivity where Date(StartDate)>=Date('".$getfrom."') and Date(EndDate)<=Date('".$getto."') and StoreID='0'";
			//  echo $sqldetailsdtyu;
              $RSservicetqdtcc = $DB->query($sqldetailsdtyu);
             if ($RSservicetqdtcc->num_rows > 0) 
				{    
					while($rowservicetqtty = $RSservicetqdtcc->fetch_assoc())
					{
						$ActivityID=$rowservicetqtty['ActivityID'];
						$CampaignName=$rowservicetqtty['CampaignName'];
						$OfferID=$rowservicetqtty['OfferID'];
						$Qty=$rowservicetqtty['Qty'];
						echo $TotalCost=$rowservicetqtty['TotalCost'];
						$sepss=select("Name","tblActivityCostCenter","ActivityCostCenterID='".$ActivityID."'");
		                $Name=$sepss[0]['Name'];
						
						   $sqldetailsdt="select COUNT(tblCustomers.CustomerID) from tblCustomers left join tblAppointments on tblAppointments.CustomerID=tblCustomers.CustomerID right join tblInvoiceDetails on tblInvoiceDetails.AppointmentId=tblAppointments.AppointmentID where tblAppointments.IsDeleted !=  '1' AND tblAppointments.FreeService !=  '1' and tblAppointments.Status='2' and Date(tblCustomers.RegDate) not between Date('".$getfrom."') and Date('".$getto."') and tblAppointments.offerid!='0' and tblAppointments.offerid='".$OfferID."' $sqlTempfrom $sqlTempto";
								   $RSservicetqdt = $DB->query($sqldetailsdt);
                                   //echo $sqldetailsdt;
												if ($RSservicetqdt->num_rows > 0) 
												{
													$ty=0;
													while($rowservicetqt = $RSservicetqdt->fetch_assoc())
													{
													$oldcustcnt=$rowservicetqt['COUNT(tblCustomers.CustomerID)'];
													
													}
												}
						   
								   if($oldcustcnt=='' || $oldcustcnt=='0')
									{
										$oldcustcnt=0;
									}
								 if($oldcustcnt=="")
									 {
										 $oldcustcnt=0;
									 }
									 else
									 {
										 $oldcustcnt=$oldcustcnt;
									 }	
									$totaloldcustcnt += $oldcustcnt;	
							$SelectrM="SELECT COUNT(tblCustomers.CustomerID) FROM tblCustomers left join tblAppointments on tblAppointments.CustomerID=tblCustomers.CustomerID right join tblInvoiceDetails on tblInvoiceDetails.AppointmentId=tblAppointments.AppointmentID WHERE tblAppointments.IsDeleted !=  '1' AND tblAppointments.FreeService !=  '1' and tblAppointments.Status='2' and tblAppointments.offerid!='0' $sqlTempfrom5 $sqlTempto5 and tblAppointments.offerid='".$OfferID."' $sqlTempfrom $sqlTempto";

		                     //  $SelectrM="SELECT COUNT(tblCustomers.CustomerID) FROM tblCustomers left join tblAppointments on tblAppointments.CustomerID=tblCustomers.CustomerID right join tblInvoiceDetails on tblInvoiceDetails.AppointmentId=tblAppointments.AppointmentID WHERE tblAppointments.StoreID='".$storr."' AND tblAppointments.IsDeleted !=  '1' AND tblAppointments.FreeService !=  '1' and tblAppointments.Status='2' $sqlTempfrom5 $sqlTempto5 $sqlTempfrom $sqlTempto";
					
						       $RSservicetM = $DB->query($SelectrM);
								if ($RSservicetM->num_rows > 0) 
								{
									$ty=0;
									while($rowservicetM = $RSservicetM->fetch_assoc())
									{
										$custcnt=$rowservicetM ['COUNT(tblCustomers.CustomerID)'];
									}
								}
					   
							   if($custcnt=='' || $custcnt=='0')
								{
									$custcnt=0;
								}
						
								if($custcnt=="")
								{
									$custcnt=0;
								}
								else
								{
									$custcnt=$custcnt;
								}
								$totalcustcnt +=$custcnt;
								
								$Totalcustomer=$custcnt+$oldcustcnt; 
								$exp=($oldcustcnt/$Totalcustomer)*100;
								$new=($custcnt/$Totalcustomer)*100;
								$sep=select("*","tblStores","StoreID='".$storr."'");
		                        $storename=$sep[0]['StoreName'];
								$costperclient=$TotalCost/$Totalcustomer;
								
								if(!empty($storr))
								{
								$sql="select tblAppointments.AppointmentID,tblInvoiceDetails.CustomerFullName, tblInvoiceDetails.RoundTotal, tblAppointments.StoreID,tblInvoiceDetails.OfferDiscountDateTime
								from tblAppointments 
								left join tblInvoiceDetails on
								tblAppointments.AppointmentID=tblInvoiceDetails.AppointmentId
								where tblAppointments.IsDeleted!='1' and tblInvoiceDetails.OfferDiscountDateTime!='NULL' and tblInvoiceDetails.AppointmentId!='NULL' and Status='2' $sqlTempfrom $sqlTempto";
								}
								else
								{
								$sql="select tblAppointments.AppointmentID,tblInvoiceDetails.CustomerFullName, tblInvoiceDetails.RoundTotal, tblAppointments.StoreID,tblInvoiceDetails.OfferDiscountDateTime
								from tblAppointments 
								left join tblInvoiceDetails on
								tblAppointments.AppointmentID=tblInvoiceDetails.AppointmentId
								where tblAppointments.IsDeleted!='1' and tblInvoiceDetails.OfferDiscountDateTime!='NULL' and tblInvoiceDetails.AppointmentId!='NULL' and Status='2' $sqlTempfrom $sqlTempto";
								}
								$RS = $DB->query($sql);
								if ($RS->num_rows > 0) 
								{
									$counter = 0;

									while($row = $RS->fetch_assoc())
									{
										$RoundTotal = $row["RoundTotal"];
										if($RoundTotal =="")
										{
											$RoundTotal ="0.00";
										}
										else
										{
										
											$RoundTotal = $RoundTotal;
											
										}
										$TotalRoundTotal += $RoundTotal;
									}
								}
								
								$arpu=$TotalRoundTotal/$Totalcustomer;
								if($costperclient=="")
								{
									$costperclient=0;
								}
								if($arpu=="")
								{
									$arpu=0;
								}
						?>
				<tbody>
								<tr>
								 <td class="numeric"><center><?=$CampaignName?></center></td>
		                        <td class="numeric"><center><?=$Name?></center></td>
								<td class="numeric"><center><?=$oldcustcnt?></center></td>
								<td class="numeric"><center><?=$custcnt?></center></td>
								<td class="numeric"><center><?php 
								                       
								  echo $Totalcustomer;
								                       ?></center></td>
							  <td><center><?="Rs. ".round($TotalRoundTotal,2)?></center></td>
							  <td class="numeric"><center><?="All"?></center></td>
							  <?php
																	if($per!='0')
																	{
																		?>
																 <td class="numeric"><center><?=round($exp,2)?></center></td>
																		<?php
																	}
																	?>
																	<?php
																	if($per!='0')
																	{
																		?>
																 <td class="numeric"><center><?=round($new,2)?></center></td>
																		<?php
																	}
																	?>
																	 <td class="numeric"><center><?=round($costperclient,2)?></center></td>
																	  <td class="numeric"><center><?=round($arpu,2)?></center></td>
							  
								</tr></tbody>
								<?php
					                }
									$TotalRoundTotal=0;
					}
					else
					{
						?>
					<tbody>
					<tr>
				   <td class="numeric"><center></center></td>
					<td class="numeric"><center>No Record Found</center></td>
					<td class="numeric"><center></center></td>
					<td class="numeric"><center></center></td>
					<td class="numeric"><center></center></td>
					<td class="numeric"><center></center></td>
					<td class="numeric"><center></center></td>
					<td class="numeric"><center></center></td>
					<td class="numeric"><center></center></td>
					<td class="numeric"><center></center></td>
					<td class="numeric"><center></center></td>
					</tr></tbody>
					<?php
					}
	}
													

?>							
				
											
						</table>
						
					</div>
				</div>
			</div>
		</div>
		</div>
		
<?php	
$TotalRoundTotal="";
			$TotalTotalRoundTotal="";
			$existing1="";
			$totalcust1="";
$DB->close();

?>
												
												
						<?php
						 }
					   else
					   {
						   echo "<br><center><h3>Please Select Month And Year!</h3></center>";
					   }
						?>
												
											</div>
										</div>
										
									</div>
								</div>
							</div>
						</div>
                    </div>
<?php
} // End null condition
else
{
	
}
?>
            </div>
        </div>
		
        <?php require_once 'incFooter.fya'; ?>
		
    </div>
</body>

</html>