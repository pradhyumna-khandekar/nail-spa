<?php

require_once('setting.fya');

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $otp = $_POST["otp"];
    $otp_id = $_POST["otp_id"];
    $Remember_me = $_POST["rememberme"];

    $DB = Connect();
    $otp_sql = "SELECT * from otp_log where id='" . $otp_id . "' AND status=1";
    $otp_exe = $DB->query($otp_sql);
    if ($otp_exe->num_rows > 0) {
        while ($otp_row = $otp_exe->fetch_assoc()) {
            $login_otp = isset($otp_row['hash_otp']) ? trim($otp_row['hash_otp']) : '';
            $admin_id = $otp_row['user_id'];
            $otp_username = $otp_row['username'];
            $otp_password = $otp_row['password'];
        }
    }

    if (isset($login_otp) && $login_otp != '' && $login_otp == $otp) {


        $sql = "SELECT * from tblAdmin where AdminID='" . $admin_id . "'";
        $RS = $DB->query($sql);
        if ($RS->num_rows > 0) {
            while ($row = $RS->fetch_assoc()) {
                $AdminID = $row["AdminID"];
                $AdminFullName = $row["AdminFullName"];
                $AdminUsername = $row["AdminUserName"];
                $AdminPassword = $row["AdminPassword"];
                $AdminType = $row["AdminType"];
                $Status = $row["Status"];
                $AdminRoleID = $row["AdminRoleID"];
                $seldata = select("StoreID", "tblAdminStore", "AdminID='" . $AdminID . "'");
                $storeid = $seldata[0]['StoreID'];
            }

            if (strtolower($otp_username) == strtolower($AdminUsername) && $otp_password == $AdminPassword) {
                if ($Status == "0") {

                    $CookieRemember_me = Encode("Remember_me," . $Remember_me . ",Remember_me");
                    setcookie("CookieRemember_me", $CookieRemember_me, time() + (10 * 365 * 24 * 60 * 60));

                    if ($Remember_me == "Y") {
                        $CookieAdminID = Encode("AdminID," . $AdminID . ",AdminID");
                        $CookieAdminUsername = Encode("AdminUsername," . $AdminUsername . ",AdminUsername");
                        $CookieAdminFullName = Encode("AdminFullName," . $AdminFullName . ",AdminFullName");
                        $CookieAdminRoleID = Encode("AdminRoleID," . $AdminRoleID . ",AdminRoleID");
                        $CookieAdminType = Encode("AdminType," . $AdminType . ",AdminType");
                        $CookieStore = Encode("StoreID," . $storeid . ",StoreID");


                        setcookie("CookieAdminID", $CookieAdminID, time() + (10 * 365 * 24 * 60 * 60));
                        setcookie("CookieAdminUsername", $CookieAdminUsername, time() + (10 * 365 * 24 * 60 * 60));
                        setcookie("CookieAdminFullName", $CookieAdminFullName, time() + (10 * 365 * 24 * 60 * 60));
                        setcookie("CookieAdminType", $CookieAdminType, time() + (10 * 365 * 24 * 60 * 60));
                        setcookie("CookieStore", $CookieStore, time() + (10 * 365 * 24 * 60 * 60));
                        setcookie("CookieAdminRoleID", $CookieAdminRoleID, time() + (10 * 365 * 24 * 60 * 60));
                    } else {
                        session_start();
                        $_SESSION["AdminID"] = $AdminID;
                        $_SESSION["AdminFullName"] = $AdminFullName;
                        $_SESSION["AdminUsername"] = $AdminUsername;
                        $_SESSION["AdminType"] = $AdminType;
                        $_SESSION["Store"] = $storeid;
                        $_SESSION["AdminRoleID"] = $AdminRoleID;
                    }


                    ExecuteNQ("update tblAdmin set LastLogin=NOW() where AdminID='$AdminID'");

                    $opt_update = "UPDATE otp_log SET status=2 WHERE id='" . $otp_id . "'";
                    $DB->query($opt_update);

                    echo('<font color="green">Processing Login Process.. Please wait !!</font>');
                    echo("<script>location.href='Salon-Dashboard.php';</script>");
                } else {
                    echo('<font color="red">Account Locked !!</font>');
                }
            } else {
                echo('<font color="red">Invalid Username. Please try again</font>');
            }
        } else {
            echo('<font color="red">Invalid Username. Please try again</font>');
        }
    } else {
        echo('<font color="red">Invalid OTP.</font>');
    }

    $DB->close();
} else {
    echo('<font color="red">Invalid OTP.</font>');
}
?> 