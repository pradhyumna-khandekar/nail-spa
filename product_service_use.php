<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Product Service Use | Nailspa";
$strDisplayTitle = "List Of Product Service Use";
$strMenuID = "3";
$strMyActionPage = "product_service_use.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <!-----------css & js files added for tabs by gandhali 3/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>
        <?php /* <script type="text/javascript" src="assets/widgets/modal/modal.js"></script> */ ?>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");     ?>
            <!----------commented by gandhali 3/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>

                        <script type="text/javascript">
                            /* Datatables responsive */
                            /*$(document).ready(function () {
                             $('#datatable-responsive-date').DataTable({
                             "order": [[5, "asc"]],
                             columnDefs: [{type: 'date-euro', targets: 5}]
                             });
                             });
                             $(document).ready(function () {
                             $('.dataTables_filter input').attr("placeholder", "Search...");
                             });*/
                        </script>


                        <script type="text/javascript" src="assets/scanner/jquery-code-scanner.js"></script>
                        <script>
                            $(function () {
                                $('input[type="search"]').codeScanner();
                            });
                        </script>
                        <div class="panel">
                            <div class="panel-body">

                                <div class="panel-body">
                                    <h3 class="title-hero"><?php echo $strDisplayTitle; ?></h3>
                                    <?php if (isset($_SESSION['messgae']) && $_SESSION['messgae'] != '') { ?>
                                        <div class="alert alert-success alert-dismissible fade in" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
                                            </button>
                                            <strong><?php echo $_SESSION['messgae']; ?></strong>
                                        </div>
                                        <?php
                                    }
                                    unset($_SESSION['messgae']);
                                    ?>

                                    <a class="btn btn-alt btn-hover btn-primary" href="add_service_use.php">Add</a>
                                    <table id="datatable-responsive" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>ID.</th>
                                                <th>Product Name</th>
                                                <th>Barcode</th>
                                                <th>Opening Date</th>
                                                <th>Closing Date</th>
                                                <th>Created Date</th>
                                                <th>Created By</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>ID.</th>
                                                <th>Product Name</th>
                                                <th>Barcode</th>
                                                <th>Opening Date</th>
                                                <th>Closing Date</th>
                                                <th>Created Date</th>
                                                <th>Created By</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <?php
                                            $DB = Connect();
                                            /*
                                             * get all users
                                             */
                                            $userq = "SELECT * FROM tblAdmin";
                                            $userq_exe = $DB->query($userq);
                                            if ($userq_exe->num_rows > 0) {
                                                while ($user_row = $userq_exe->fetch_assoc()) {
                                                    $user_data[$user_row['AdminID']] = $user_row;
                                                }
                                            }


                                            $service_data = select("*", "product_service_use", "status =1 AND created_by='" . $strAdminID . "' ORDER BY id DESC");
                                            if (isset($service_data) && is_array($service_data) && count($service_data) > 0) {
                                                foreach ($service_data as $skey => $svalue) {
                                                    $product_in_arr[$svalue['product_id']] = $svalue['product_id'];
                                                }
                                            }


                                            if (isset($product_in_arr) && is_array($product_in_arr) && count($product_in_arr) > 0) {
                                                $pdt_in_ids = implode(",", $product_in_arr);
                                                if ($pdt_in_ids != '') {
                                                    $productName = select("*", "tblNewProducts", "ProductID IN(" . $pdt_in_ids . ")");
                                                    if (isset($productName) && is_array($productName) && count($productName) > 0) {
                                                        foreach ($productName as $pkey => $pvalue) {
                                                            $product_name_data[$pvalue['ProductID']]['ProductName'] = $pvalue['ProductName'];
                                                        }
                                                    }
                                                }
                                            }

                                            $DB->close();


                                            if (isset($service_data) && is_array($service_data) && count($service_data) > 0) {
                                                foreach ($service_data as $skey => $value) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $value['id']; ?></td>
                                                        <td><?php echo isset($product_name_data[$value['product_id']]) ? $product_name_data[$value['product_id']]['ProductName'] : ''; ?></td>
                                                        <td><?php echo $value['barcode']; ?></td>
                                                        <td><?php echo $value['start_date'] != '0000-00-00 00:00:00' && $value['start_date'] != '1970-01-01 00:00:00' ? date('d/m/Y h:i a', strtotime($value['start_date'])) : ''; ?></td>
                                                        <td><?php echo $value['end_date'] != '0000-00-00 00:00:00' && $value['end_date'] != '1970-01-01 00:00:00' ? date('d/m/Y h:i a', strtotime($value['end_date'])) : ''; ?></td>
                                                        <td><?php echo date('d/m/Y h:i a', strtotime($value['created_date'])); ?></td>
                                                        <td><?php echo isset($user_data[$value['created_by']]) ? $user_data[$value['created_by']]['AdminFullName'] : ''; ?></td>
                                                        <td>
                                                            <?php if ($value['use_status'] == 1) { ?>
                                                                <a class="btn btn-link" onclick="return confirm('Are you sure you wan to mark Closing Date?')" href="mark_service_use.php?id=<?php echo $value['id']; ?>">Mark Closing</a>
                                                            <?php }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <?php require_once 'incFooter.fya'; ?>
        </div>

    </body>
</html>