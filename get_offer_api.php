<?php

require_once("setting.fya");
$DB = Connect();
$offer_data = array();

$post_data = $_POST;
$total_amount = isset($post_data['total_amount']) && $post_data['total_amount'] != '' ? $post_data['total_amount'] : 0;
$apt_store_id = isset($post_data['store_id']) && $post_data['store_id'] != '' ? $post_data['store_id'] : '';
$mobile_no = isset($post_data['phone']) && $post_data['phone'] != '' ? $post_data['phone'] : '';
$service_code = isset($post_data['services']) && $post_data['services'] != '' ? explode(",", $post_data['services']) : array();
$offer_code = isset($post_data['offer_code']) && $post_data['offer_code'] != '' ? trim($post_data['offer_code']) : '';
$offer_used = 0;
$response_data = array(
    'status' => 'fail',
    'msg' => "Unable to Apply Offer Code."
);
if ($apt_store_id == '') {
    $response_data = array(
        'status' => 'fail',
        'msg' => 'Select Store First to apply Offer Code.'
    );
} else if ($offer_code == '') {
    $response_data = array(
        'status' => 'fail',
        'msg' => 'Offer Code Cannot be blank.'
    );
} else if ($mobile_no == '') {
    $response_data = array(
        'status' => 'fail',
        'msg' => 'Mobile Number cannot Blank.'
    );
}  else {
    if ($mobile_no != '') {
        /*
         * Check Customer Exist Or Not
         */
        $custexist_q = "Select * from tblCustomers Where SUBSTRING(CustomerMobileNo , -10) LIKE '%" . substr(trim($mobile_no), -10) . "%'";
        $custexist_exe = $DB->query($custexist_q);
        if ($custexist_exe->num_rows > 0) {
            $cust_data = $custexist_exe->fetch_assoc();
            $strCustomerID = $cust_data['CustomerID'];
        }
    }

    $seldofferp = select("*", "tblOffers", "TRIM(OfferCode) ='$offer_code'");
    $offer_id = isset($seldofferp[0]['OfferID']) ? $seldofferp[0]['OfferID'] : 0;

    if (isset($seldofferp) && is_array($seldofferp) && count($seldofferp) > 0) {
        $day_limit = isset($seldofferp[0]['offer_day_limit']) ? $seldofferp[0]['offer_day_limit'] : 0;
        if ($day_limit != 0) {
            $customer_id = isset($strCustomerID) ? $strCustomerID : 0;
            if ($customer_id > 0) {
                /*
                 * Get Customer last date when used offer
                 */
                $last_offer_date = select("*", "tblAppointments", "offerid='" . $offer_id . "' AND CustomerID='" . $customer_id . "' AND IsDeleted='0' ORDER BY AppointmentID DESC");
                if (isset($last_offer_date) && is_array($last_offer_date) && count($last_offer_date) > 0) {
                    $last_date = $last_offer_date[0]['AppointmentDate'];
                    $today_date = strtotime(date('Y-m-d')); // or your date as well
                    $offer_date = strtotime($last_date);
                    $datediff = $today_date - $offer_date;

                    $day_count = round($datediff / (60 * 60 * 24));
                    if ($day_count <= $day_limit) {
                        $offer_used = 1;
                        $response_data = array(
                            'status' => 'fail',
                            'msg' => "This Offer Already Used."
                        );
                    }
                }
            }
        }

        if ($offer_used == 0) {
            $offerdateto = $seldofferp[0]['OfferDateTo'];
            $StoreID = $seldofferp[0]['StoreID'];
            $stores = explode(",", $StoreID);

            $services = $seldofferp[0]['ServiceID'];
            $servicessf = explode(",", $services);
          
            if (isset($service_code) && is_array($service_code) && count($service_code) > 0) {
                foreach ($service_code as $skey => $svalue) {
                    $service_data = select("*", "tblServices", "ServiceCode='" . $svalue . "' and StoreID='" . $apt_store_id . "'");
                    
                    if (isset($service_data) && is_array($service_data) && count($service_data) > 0) {
                        $apt_service[]['ServiceID'] = $service_data[0]['ServiceID'];
                    }
                }
            }
         
            if (isset($apt_service) && is_array($apt_service) && count($apt_service) > 0) {

                $date = date('Y-m-d');
                if ($date > $offerdateto) {
                    $response_data = array(
                        'status' => 'fail',
                        'msg' => "Offer expired."
                    );
                } else if (!in_array($apt_store_id, $stores)) {
                    $response_data = array(
                        'status' => 'fail',
                        'msg' => "This offer is not valid for this store."
                    );
                } else {
                    
                    if (isset($apt_service) && is_array($apt_service) && count($apt_service) > 0) {
                        foreach ($apt_service as $val) {
                            if (!in_array($val['ServiceID'], $servicessf)) {
                                $response_data = array(
                                    'status' => 'fail',
                                    'msg' => "Offer applied is not valid for some of the services."
                                );
                                echo json_encode($response_data);
                                exit;
                            }
                        }

                        /*
                         * Check Discount Amount 
                         */
                        if ($seldofferp[0]['Type'] == 1) {
                            $discount_amo = $seldofferp[0]['TypeAmount'];
                        } else {
                            $dis_per = $seldofferp[0]['TypeAmount'];
                            $discount_amo = ($total_amount * $dis_per) / 100;
                        }

                        if ($discount_amo > $total_amount) {
                            $response_data = array(
                                'status' => 'fail',
                                'msg' => "service amount is less then offer amount."
                            );
                        } else {
                            $response_data = array(
                                'status' => 'success',
                                'msg' => "Offer Applied Successfully.",
                                'offer_id' => $offer_id,
                                'offer_amount' => $discount_amo
                            );
                        }
                    }
                }
            } else {
                $response_data = array(
                    'status' => 'fail',
                    'msg' => "Offer applied is not valid for some of the services."
                );
                echo json_encode($response_data);
                exit;
            }
        }
    } else {
        $response_data = array(
            'status' => 'fail',
            'msg' => 'Invalid Offer Code.'
        );
    }
}

$DB->close();

echo json_encode($response_data);
?>