<?php require_once("setting.fya"); ?>
<?php

$DB = Connect();
$allPdtData = select("ProductID,ProductName,ProductUniqueCode,service_age", "tblNewProducts", "ProductID>0");
if (isset($allPdtData) && is_array($allPdtData) && count($allPdtData) > 0) {
    foreach ($allPdtData as $pdkey => $pdvalue) {
        $product_use_data[$pdvalue['ProductID']] = $pdvalue;
    }
}
$use_data = select("*", "product_service_use", "status=1 AND end_date!='0000-00-00 00:00:00' AND use_status=2");

$store_data = select("*", "tblStores", "StoreID > 0");
if (isset($store_data) && is_array($store_data) && count($store_data) > 0) {
    foreach ($store_data as $skey => $svalue) {
        $store_name[$svalue['StoreID']] = $svalue;
    }
}

if (isset($use_data) && is_array($use_data) && count($use_data) > 0) {
    foreach ($use_data as $ukey => $uvalue) {

        $notify_ids[$uvalue['id']] = $uvalue['id'];
        $start_date = date('Y-m-d', strtotime($uvalue['start_date']));
        $end_date = date('Y-m-d', strtotime($uvalue['end_date']));

        $date1 = new DateTime($start_date);
        $date2 = $date1->diff(new DateTime($end_date));
        $day_diff = $date2->d + 1;

        $product_service_age = isset($product_use_data[$uvalue['product_id']]['service_age']) ? $product_use_data[$uvalue['product_id']]['service_age'] : 0;

        //echo "<br>Day used=" . $day_diff . " AND product use=" . $product_service_age;
        if ($product_service_age > 0 && $day_diff < $product_service_age) {
            $used_product[$uvalue['store_id']][$uvalue['product_id']]['name'] = $product_use_data[$uvalue['product_id']]['ProductName'];
            $used_product[$uvalue['store_id']][$uvalue['product_id']]['start_date'] = $uvalue['start_date'];
            $used_product[$uvalue['store_id']][$uvalue['product_id']]['end_date'] = $uvalue['end_date'];
            $used_product[$uvalue['store_id']][$uvalue['product_id']]['days'] = $day_diff;
            $used_product[$uvalue['store_id']][$uvalue['product_id']]['product_days'] = $product_service_age;
            $used_product[$uvalue['store_id']][$uvalue['product_id']]['code'] = $product_use_data[$uvalue['product_id']]['ProductUniqueCode'];
        }
    }
}


$email_body = '';
if (isset($used_product) && is_array($used_product) && count($used_product) > 0) {

    $email_body = '<html>
    <head>
   
</head>
    <body>
        <table border="0" cellspacing="0" cellpadding="0" width="100%">

            <tbody>
        <tr>
            <td>
                <table border="0" cellspacing="0" cellpadding="0" width="800" align="center">
                    <tbody>
                        
                        <tr>
                            <td>
                                <table style="BORDER-BOTTOM:#d0ac52 1px solid;BORDER-LEFT:#d0ac52 1px solid;BORDER-TOP:#d0ac52 1px solid;BORDER-RIGHT:#d0ac52 1px solid;background: url({path}) no-repeat;background-position: 50% -20px;" border="0" cellspacing="0" cellpadding="0" width="800" bgcolor="#ffffff" align="center">
                                    <tbody>
                                        <tr>
                                            <td align="middle">
                                                <table border="0" cellspacing="0" cellpadding="0" width="790" align="center">
                                                    <tbody>
                                                        <tr>
                                                            <td width="290" align="left" style="padding:1%;"><img border="0" src="http://nailspaexperience.com/header/Nailspa-logo.png" width="117" height="60" alt="NailSpa Experience"></td>
                                                           
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
										   <tr>
                                            <td style="LINE-HEIGHT:0;BACKGROUND:#d0ac52;FONT-SIZE:0px;" height="5"></td>
                                        </tr>
                                        <tr>
                                            <td style="PADDING-LEFT:10px;PADDING-RIGHT:10px;PADDING-TOP:10px;" align="left">
                                                <div style="BORDER-BOTTOM:#dedcdc 1px solid; FONT-FAMILY:Verdana,Geneva,sans-serif; BORDER-LEFT:#dedcdc 1px solid;PADDING-BOTTOM:15px;PADDING-LEFT:5px;PADDING-RIGHT:5px;BACKGROUND:#e4e4e4;FONT-SIZE:14px;BORDER-TOP:#dedcdc 1px solid;FONT-WEIGHT:bold;BORDER-RIGHT:#dedcdc 1px solid;PADDING-TOP:15px; text-align:center;">
                                             Product Service Use On ' . date('d M,Y') . '<br>
                                                                                                
                                                </div>
                                            </td>';


    $email_body.= '<tr>
                                                    <td colspan="2" valign="top" style="border-bottom:1px solid #dddddd; padding:10px; ">
                                                        <table class="report-table" border="1" cellspacing="0" style="FONT-FAMILY:Verdana,Geneva,sans-serif;text-align:center;FONT-SIZE:14px;width: 100%;">
                                                            <tr style="background-color: #e4e4e4;">
                                                                <th>Sr. No.</th>
                                                                <th>Product Name</th>
                                                                <th>Product use Day</th>
                                                                <th>Product Code</th>
                                                                <th>Branch</th>                                                                 
                                                                <th>Start Date</th> 
                                                                <th>End Date</th> 
                                                                <th>No. Of Days Used</th> 
                                                                </tr>';


    $counter = 1;
    if (isset($used_product) && is_array($used_product) && count($used_product) > 0) {
        foreach ($used_product as $skey => $svalue) {
            foreach ($svalue as $pkey => $pvalue) {
                $email_body .= '<tr id="my_data_tr_<?= $counter ?>"> 
                <td>' . $counter . '</td>
                <td>' . $pvalue['name'] . '</td>
                <td>' . $pvalue['product_days'] . '</td>
                <td>' . $pvalue['code'] . '</td>
                <td>' . $store_name[$skey]['StoreName'] . '</td>
                <td>' . date('d M,Y h:i a', strtotime($pvalue['start_date'])) . '</td>
                <td>' . date('d M,Y h:i a', strtotime($pvalue['end_date'])) . '</td>
                <td>' . $pvalue['days'] . '</td>
            </tr>';
                $counter++;
            }
        }
    }

    $email_body .='</td></tr></table><tr><td  style="BACKGROUND:#d0ad53;">
                                                <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                              
                                                <tbody>
                                        
                                        
                                        
                                            <td width="33%" style="FONT-FAMILY:Arial,Helvetica,sans-serif;BACKGROUND:#d0ad53;COLOR:#000;FONT-SIZE:12px; padding:1%;" height="32" align="center">
                                            <span style="font-size:14px; font-weight:600;" >KHAR | BREACH CANDY | ANDHERI | COLABA | LOKHANDWALA</span><br>
                                            </td>
											<tr>
                                            	<td style="BACKGROUND:#d0ad53;font-size:24px;font-weight:bold;" align="center"> Go Green, Go Paperless !</td>
                                            </tr>
                                          
                                            </tbody>
                                            </table>
                                            </td>
                                            
                                            
                                        	</tr>
                                    	</tbody>
                                	</table>
                            	</td>
                        	</tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
</body>
</html>';
}

if ($email_body != '') {
    $report_data = select("*", 'report_config', 'status =1 AND report_name="product_service_use"');
   
    if (isset($report_data) && is_array($report_data) && count($report_data) > 0) {
        foreach ($report_data as $key => $value) {
            $strTo = $value["email_id"];
            $strFrom = "invoice@nailspaexperience.com";
            $strSubject = "Product Service Use Report";
            $strbody1 = $email_body;
            $headers = "From: $strFrom\r\n";
            $headers .= "Content-type: text/html\r\n";
            $strBodysa = AntiFilter1($strbody1);

            // Mail sending 
            $retval = mail($strTo, $strSubject, $strBodysa, $headers);

            if ($retval) {
                if (isset($notify_ids) && is_array($notify_ids) && count($notify_ids) > 0) {
                    $noti_in_ids = implode(",", $notify_ids);
                    if ($noti_in_ids != '') {
                        $updaten = "UPDATE product_service_use SET is_notifiy=1 WHERE id IN(" . $noti_in_ids . ")";
                        echo $updaten;exit;
                        $DB->query($updaten);
                    }
                }
            }
        }
    }
}
$DB->close();
?>