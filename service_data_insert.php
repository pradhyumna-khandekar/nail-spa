<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php

$DB = Connect();

$selectrecord = "SELECT COUNT(*),ServiceCode,ServiceID FROM `tblServices` GROUP BY ServiceCode";
$RSf = $DB->query($selectrecord);
if ($RSf->num_rows > 0) {
    while ($rowf = $RSf->fetch_assoc()) {
        $ServiceCode[$rowf['ServiceCode']] = $rowf["ServiceID"];
    }
}



/*
 * get all services
 */

$get_servicesq = "SELECT * FROM `tblServices`";
$get_servicesq_exe = $DB->query($get_servicesq);
if ($get_servicesq_exe->num_rows > 0) {
    while ($service_data = $get_servicesq_exe->fetch_assoc()) {
        $ServiceData[] = $service_data;
    }
}

if (isset($ServiceData) && is_array($ServiceData) && count($ServiceData) > 0) {
    foreach ($ServiceData as $skey => $svalue) {
        /*
         * check serviceID present in tblProductsServices OR not
         */
        $checkSerq = "SELECT *  FROM `tblProductsServices` WHERE ServiceID = '" . $svalue["ServiceID"] . "'";
        $checkSerq_exe = $DB->query($checkSerq);
        if ($checkSerq_exe->num_rows > 0) {

            /*
             * donthing
             */
        } else {
            /*
             * insert service ID Data
             */
            // get parent service id
            $parent_serviceId = isset($ServiceCode[$svalue['ServiceCode']]) ? $ServiceCode[$svalue['ServiceCode']] : 0;
            if ($parent_serviceId != 0) {
                $get_ser_dataq = "SELECT * FROM `tblProductsServices` WHERE ServiceID ='" . $parent_serviceId . "'";
                $get_ser_dataq_exe = $DB->query($get_ser_dataq);
                if ($get_ser_dataq_exe->num_rows > 0) {
                    while ($parent_service_data = $get_ser_dataq_exe->fetch_assoc()) {
                        $insert1_data_column = array(
                            'ProductID', 'ProductStockID', 'ServiceID', 'CategoryID', 'Status', 'StoreID', 'DateTimeStamp'
                        );

                        $insert1_data_row = array(
                            $parent_service_data['ProductID'], $parent_service_data['ProductStockID'],
                            $svalue['ServiceID'], $parent_service_data['CategoryID'], $parent_service_data['Status'],
                            $svalue['StoreID'], date('Y-m-d')
                        );


                        /*
                         * INSERT into tblProductsServices Table
                         */
                        $field_values1 = implode(',', $insert1_data_column);
                        $data_values1 = implode(',', $insert1_data_row);
                        $insert_sql1 = "INSERT into tblProductsServices (" . $field_values1 . ") VALUES(" . $data_values1 . ")";
                        $DB->query($insert_sql1);


                        $insert2_data_column = array(
                            'ProductID', 'ServiceID', 'StoredID', 'CategoryID'
                        );

                        $insert2_data_row = array(
                            $parent_service_data['ProductID'], $svalue['ServiceID'],
                            $svalue['StoreID'], $parent_service_data['CategoryID']
                        );
                        /*
                         *  INSERT into tblProductServiceCategory Table
                         */
                        $field_values2 = implode(',', $insert2_data_column);
                        $data_values2 = implode(',', $insert2_data_row);
                        $insert_sql2 = "INSERT into tblProductServiceCategory (" . $field_values2 . ") VALUES(" . $data_values2 . ")";
                        $DB->query($insert_sql2);
                    }
                }
            }
        }
    }
}

$DB->close();
?>