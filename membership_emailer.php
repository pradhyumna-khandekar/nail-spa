<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Manage Membership Emailer | Nailspa";
$strDisplayTitle = "Logs for Nailspa";
$strMenuID = "3";
$strMyActionPage = "membership_emailer.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <!-----------css & js files added for tabs by gandhali 3/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");     ?>
            <!----------commented by gandhali 3/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>
                        <script src="https://cdn.ckeditor.com/4.10.1/standard/ckeditor.js"></script>


                        <?php
                        $DB = Connect();

                        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
                            
                            if (isset($_POST['email_subject']) && $_POST['email_subject'] != '') {
                                if (isset($_POST['email_message']) && $_POST['email_message'] != '') {
                                    $mail_subject = $_POST['email_subject'];
                                    $mail_message = $_POST['email_message'];

                                    if (isset($_POST['mailer_id']) && $_POST['mailer_id'] != '') {
                                        $sqlUpdate = "UPDATE tblnotification_msg SET subject='" . $mail_subject . "' "
                                                . ",message='" . $mail_message . "',modified_date='" . date('Y-m-d H:i:s') . "',modified_by='" . $strAdminID . "'"
                                                . " WHERE id='" . $_POST['mailer_id'] . "'";
                                        ExecuteNQ($sqlUpdate);
                                    } else {
                                        $sqlInsert = "Insert into tblnotification_msg (message_type,name, subject, message, created_by, created_date) values
						('one_mon_membership_renew_reminder', 'One Month Membership Reminder','" . $mail_subject . "', '" . trim($mail_message) . "', '" . $strAdminID . "', '" . date('Y-m-d H:i:s') . "')";
                                        $DB->query($sqlInsert);
                                    }
                                    echo('<div class="alert alert-success alert-dismissible fade in" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
										</button>
										<strong>Notification Messages Updated Successfully.</strong>
										</div>');
                                } else {
                                    die('<div class="alert alert-warning alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
								</button>
								<strong>Subject Message be blank.</strong>
							</div>');
                                }
                            } else {
                                die('<div class="alert alert-warning alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
								</button>
								<strong>Subject Cannot be blank.</strong>
							</div>');
                            }
                        }
                        ?>

                        <div class="panel">
                            <div class="panel-body">

                                <div class="panel-body">
                                    <?php
                                    if (isset($_GET['msg_id']) && $_GET['msg_id'] != '') {
                                        $mailer_data = select("*", "tblnotification_msg", "status =1 AND id='" . $_GET['msg_id'] . "'");
                                        ?>

                                        <form role="form" enctype="multipart/form-data" class="form-horizontal bordered-row enquiry_form" action="membership_emailer.php?msg_id=<?php echo $mailer_data[0]['id']; ?>" method="post">

                                            <span class="result_message">&nbsp; <br>
                                            </span>
                                            <br>

                                            <h3 class="title-hero">Edit Notification Message</h3>
                                            <div class="example-box-wrapper">
                                                <input type="hidden" name="mailer_id" value="<?php echo isset($mailer_data[0]['id']) ? $mailer_data[0]['id'] : ''; ?>"/>
                                                <div class="form-group"><label class="col-sm-3 control-label">Subject</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" name="email_subject" value="<?php echo isset($mailer_data[0]['subject']) ? $mailer_data[0]['subject'] : ''; ?>" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="form-group"><label class="col-sm-3 control-label">Message</label>
                                                    <div class="col-sm-9">
                                                        <textarea rows="5" class="form-control" name="email_message" required><?php echo isset($mailer_data[0]['message']) ? $mailer_data[0]['message'] : ''; ?></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label"></label>
                                                    <div class="col-sm-6">
                                                        <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Submit</span></button>
                                                    </div>
                                                </div>
                                            </div>


                                        </form>
                                        <?php
                                    } else {
                                        header('Location: notification_msg_list.php');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <script type="text/javascript">
<?php if ($mailer_data[0]['message_mode'] == 1) { ?>
                    CKEDITOR.replace('email_message');
<?php } ?>
            </script>
            <?php require_once 'incFooter.fya'; ?>
        </div>

    </body>
</html>
