<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
	$strPageTitle = "New addition of center with module & service selection | Nailspa";
	$strDisplayTitle = "New addition of center with module & service selection for Nailspa";
	$strMenuID = "3";
	$strMyTable = "tblAdmin";
	$strMyTableID = "AdminID";
	$strMyField = "Title";
	$strMyActionPage = "TransferNewCenter_detail.php";
	$strMessage = "";
	$sqlColumn = "";
	$sqlColumnValues = "";
// code for not allowing the normal admin to access the super admin rights	
if($strAdminType!="0")
{
	die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights	

	if ($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$strStep = Filter($_POST["step"]);
		if($strStep=="edit")
		{	
	        $storename = Filter($_POST["store"]);
			$StoreOfficialAddress = Filter($_POST["StoreOfficialAddress"]);
			$StoreBillingAddress = Filter($_POST["StoreBillingAddress"]);
			$StoreOfficialEmailID = Filter($_POST["StoreOfficialEmailID"]);
			$StoreBillingEmailID = Filter($_POST["StoreBillingEmailID"]);
			$StoreOfficialNumber = Filter($_POST["StoreOfficialNumber"]);
			$StoreBillingNumber = Filter($_POST["StoreBillingNumber"]);
			
			$Categories = $_POST["Categories"];
			$Categories1= $_POST["Categories1"];
			$Categories2= $_POST["Categories2"];
			$Categories3= $_POST["Categories3"];
			$Categories4= $_POST["Categories4"];
			$Categories5= $_POST["Categories5"];
			$Categories6= $_POST["Categories6"];
			$Categories7= $_POST["Categories7"];
			$ServiceIDdd = $_POST["ServiceID"];			
            $sertu=array_unique($ServiceIDdd);	
			
	        $ServiceIDdd1 = $_POST["ServiceID1"];			
            $sertu1=array_unique($ServiceIDdd1);	
			
            $ServiceIDdd2 = $_POST["ServiceID2"];			
            $sertu2=array_unique($ServiceIDdd2);	
			
            $ServiceIDdd3 = $_POST["ServiceID3"];			
            $sertu3=array_unique($ServiceIDdd3);	
			
            $ServiceIDdd4 = $_POST["ServiceID4"];			
            $sertu4=array_unique($ServiceIDdd4);	
			
			$ServiceIDdd5 = $_POST["ServiceID5"];			
			$sertu5=array_unique($ServiceIDdd5);
			
			$ServiceIDdd6 = $_POST["ServiceID6"];			
			$sertu6=array_unique($ServiceIDdd6);
			
			$ServiceIDdd7 = $_POST["ServiceID7"];			
			$sertu7=array_unique($ServiceIDdd7);
			
			//$Products= $_POST["Products"];
			$getfrom=date('Y-m-d');
		    
		
			//print_r($sertu);
			$DB = Connect();
		   
			$sepstreidrecord=select("count(*)","tblStores","StoreName LIKE '".ucfirst($storename)."'");
			$contreacod=$sepstreidrecord[0]['count(*)'];
			
			
			if($contreacod>0)
			{
					$DB->close();
				die('<div class="alert alert-close alert-danger">
					<div class="bg-red alert-icon"><i class="glyph-icon icon-times"></i></div>
					<div class="alert-content">
						<h4 class="alert-title">Record Add Failed</h4>
						<p>This Store already exists in our system. Please try again.</p>
					</div>
				</div>');
	
			
			}
			else
			{
				$sqlInsert = "Insert into tblStores (StoreName, StoreOfficialAddress, StoreBillingAddress, StoreOfficialEmailID, StoreBillingEmailID, StoreOfficialNumber, StoreBillingNumber, Status) values
				('".ucfirst($storename)."','".$StoreOfficialAddress."', '".$StoreBillingAddress."', '".$StoreOfficialEmailID."', '".$StoreBillingEmailID."', '".$StoreOfficialNumber."', '".$StoreBillingNumber."', '0')";
				//ECHO 12345;
				
				
						 if ($DB->query($sqlInsert) === TRUE) 
						{
							//$last_id = $DB->insert_id;
							$newstoreid = $DB->insert_id;
						}
						else
						{
							echo "Error: " . $sql . "<br>" . $conn->error;
						} 
					
						if(!empty($sertu))
						{
							for($j=0;$j<count($sertu);$j++)
							  {
								  if($sertu[0]!='')
								  {
									$sepASDD=select("*","tblServices","ServiceID='".$sertu[$j]."' group by ServiceCode");
								    $ServiceID1=$sepASDD[0]['ServiceID'];
									$ServiceName=$sepASDD[0]['ServiceName'];
									$ServiceCode=$sepASDD[0]['ServiceCode'];
									$Time=$sepASDD[0]['Time'];
									$ServiceCost=$sepASDD[0]['ServiceCost'];
									$ServiceCommission=$sepASDD[0]['ServiceCommission'];
									$GMPercentage=$sepASDD[0]['GMPercentage'];
									$MRPLessTax=$sepASDD[0]['MRPLessTax'];
									$DirectCost=$sepASDD[0]['DirectCost'];
									$GrossMargin=$sepASDD[0]['GrossMargin'];
									$Status=$sepASDD[0]['Status'];
									$ProductPrice=$sepASDD[0]['ProductPrice'];
									
									$sqlInsert888 = "INSERT INTO tblServices (ServiceName, ServiceCode, ServiceCost, ServiceCommission, MRPLessTax, DirectCost, GMPercentage, GrossMargin,Status,StoreID,Time,ProductPrice) VALUES
									('".$ServiceName."', '".$ServiceCode."', '".$ServiceCost."', '".$ServiceCommission."','".$MRPLessTax."', '".$DirectCost."','".$GMPercentage."', '".$GrossMargin."', '".$Status."' , '".$newstoreid."' , '".$Time."', '".$ProductPrice."')";
								  //echo $sqlInsert888;
									
						        	if ($DB->query($sqlInsert888) === TRUE) 
									{
										
										$last_idp1 = $DB->insert_id;
									}
									else
									{
										echo "Error: " . $sql . "<br>" . $conn->error;
									}  
									 
									
									////////////////////Add TAX for service/////////////////////////////////
									
									$pqr="Insert into tblServicesCharges (ServiceID,ChargeNameID,Status) Values ('$last_idp1','1','0')";
			                       //echo $pqr;
				
										  if ($DB->query($pqr) === TRUE) 
										{
												// echo "Record Update successfully.";
										}
										else
										{
											//echo "Error2";
										}  
										
										
										 $sql = "SELECT ProductID FROM tblProductsServices WHERE CategoryID='$Categories' and ServiceID='$ServiceID1'";
									//echo $sql."<br>";
									
									$RS = $DB->query($sql);

										if ($RS->num_rows > 0)

										{
                               	while($row = $RS->fetch_assoc())

														{
															$prod=$row['ProductID'];
															$selp=select("ProductID","tblNewProducts","ProductID='".$prod."'");
												
															$ProductID1234=$selp[0]['ProductID'];
															if($ProductID1234!="")
															{
																		
																$sqlInsert2 = "Insert into tblProductsServices(ProductID,ProductStockID,ServiceID, Status,CategoryID,StoreID,DateTimeStamp) values ('".$prod."','0', '".$last_idp1."','0','".$Categories."','".$newstoreid."','".$getfrom."')";
																//echo $sqlInsert2."<br/>";
																ExecuteNQ($sqlInsert2);
																
																$sql1="INSERT INTO  tblStoreProduct(ProductID,StoreID,Stock,ProductStockID,UpdatePerQtyServe) VALUES ('".$prod."','".$newstoreid."','5','0','1')";	
		                                                      
																 if ($DB->query($sql1) === TRUE) 
																{
																	$data=3;
																} 
																else 
																{
																	$data="Error: " . $sql1 . "<br>" . $DB->error;
																} 
															
															}
															else
															{
																$selpT=select("*","tblNewProductStocks","ProductStockID='".$prod."'");
																foreach($selpT as $val)
																{
																	$sqlInsert4 = "Insert into tblProductsServices(ProductID,ProductStockID, ServiceID, Status,CategoryID,StoreID,DateTimeStamp) values
																	('".$val['ProductID']."','".$val['ProductStockID']."','".$last_idp1."','1','".$Categories."','".$newstoreid."','".$getfrom."')";
																   // echo $sqlInsert4."<br/>";
																	ExecuteNQ($sqlInsert4);
																	
																	$sql1="INSERT INTO  tblStoreProduct(ProductID,StoreID,Stock,ProductStockID,UpdatePerQtyServe) VALUES ('".$val['ProductID']."','".$newstoreid."','5','".$val['ProductStockID']."','1')";	
		                                                       // echo $sql1;
																 if ($DB->query($sql1) === TRUE) 
																{
																	
																} 
																else 
																{
																	$data="Error: " . $sql1 . "<br>" . $DB->error;
																} 
															
																	
																	
																}
															}
												
													$sqlInsert3 = "Insert into tblProductServiceCategory(ProductID,ServiceID,StoredID,CategoryID) values('".$prod."','".$last_idp1."','".$newstoreid."','".$Categories."')";
													//echo $sqlInsert3."<br/>";
												    ExecuteNQ($sqlInsert3);
														}
										}
								  }
								
							  }
							  unset($sertu);
							
						}
						if(!empty($sertu1))
						{
							for($j=0;$j<count($sertu1);$j++)
							  {
								     if($sertu1[0]!='')
								     {
										 	$sepASDD=select("*","tblServices","ServiceID='".$sertu1[$j]."' group by ServiceCode");
								    $ServiceID2=$sepASDD[0]['ServiceID'];
									$ServiceName=$sepASDD[0]['ServiceName'];
									$ServiceCode=$sepASDD[0]['ServiceCode'];
									$Time=$sepASDD[0]['Time'];
									$ServiceCost=$sepASDD[0]['ServiceCost'];
									$ServiceCommission=$sepASDD[0]['ServiceCommission'];
									$GMPercentage=$sepASDD[0]['GMPercentage'];
									$MRPLessTax=$sepASDD[0]['MRPLessTax'];
									$DirectCost=$sepASDD[0]['DirectCost'];
									$GrossMargin=$sepASDD[0]['GrossMargin'];
									$Status=$sepASDD[0]['Status'];
									$ProductPrice=$sepASDD[0]['ProductPrice'];
									
									$sqlInsert888 = "INSERT INTO tblServices (ServiceName, ServiceCode, ServiceCost, ServiceCommission, MRPLessTax, DirectCost, GMPercentage, GrossMargin,Status,StoreID,Time,ProductPrice) VALUES
									('".$ServiceName."', '".$ServiceCode."', '".$ServiceCost."', '".$ServiceCommission."','".$MRPLessTax."', '".$DirectCost."','".$GMPercentage."', '".$GrossMargin."', '".$Status."' , '".$newstoreid."' , '".$Time."', '".$ProductPrice."')";
								  //echo $sqlInsert888;
									
						        	if ($DB->query($sqlInsert888) === TRUE) 
									{
										
										$last_idp2 = $DB->insert_id;
									}
									else
									{
										echo "Error: " . $sql . "<br>" . $conn->error;
									}  
									 
									
									////////////////////Add TAX for service/////////////////////////////////
									
									$pqr="Insert into tblServicesCharges (ServiceID,ChargeNameID,Status) Values ('$last_idp2','1','0')";
			                      // echo $pqr;
				
										  if ($DB->query($pqr) === TRUE) 
										{
												// echo "Record Update successfully.";
										}
										else
										{
											//echo "Error2";
										}  
										
										
										 $sql = "SELECT ProductID FROM tblProductsServices WHERE CategoryID='$Categories1' and ServiceID='$ServiceID2'";
									//echo $sql."<br>";
									
									$RS = $DB->query($sql);

										if ($RS->num_rows > 0)

										{
                               	while($row = $RS->fetch_assoc())

														{
															$prod=$row['ProductID'];
															$selp=select("ProductID","tblNewProducts","ProductID='".$prod."'");
												
															$ProductID1234=$selp[0]['ProductID'];
															if($ProductID1234!="")
															{
																		
																$sqlInsert2 = "Insert into tblProductsServices(ProductID,ProductStockID,ServiceID, Status,CategoryID,StoreID,DateTimeStamp) values ('".$prod."','0', '".$last_idp2."','0','".$Categories1."','".$newstoreid."','".$getfrom."')";
																//echo $sqlInsert2."<br/>";
																ExecuteNQ($sqlInsert2);
																
																$sql1="INSERT INTO  tblStoreProduct(ProductID,StoreID,Stock,ProductStockID,UpdatePerQtyServe) VALUES ('".$prod."','".$newstoreid."','5','0','1')";	
		                                                      
																 if ($DB->query($sql1) === TRUE) 
																{
																	$data=3;
																} 
																else 
																{
																	$data="Error: " . $sql1 . "<br>" . $DB->error;
																} 
															
															}
															else
															{
																$selpT=select("*","tblNewProductStocks","ProductStockID='".$prod."'");
																foreach($selpT as $val)
																{
																	$sqlInsert4 = "Insert into tblProductsServices(ProductID,ProductStockID, ServiceID, Status,CategoryID,StoreID,DateTimeStamp) values
																	('".$val['ProductID']."','".$val['ProductStockID']."','".$last_idp2."','1','".$Categories1."','".$newstoreid."','".$getfrom."')";
																   // echo $sqlInsert4."<br/>";
																	ExecuteNQ($sqlInsert4);
																	
																	$sql1="INSERT INTO  tblStoreProduct(ProductID,StoreID,Stock,ProductStockID,UpdatePerQtyServe) VALUES ('".$val['ProductID']."','".$newstoreid."','5','".$val['ProductStockID']."','1')";	
		                                                       // echo $sql1;
																 if ($DB->query($sql1) === TRUE) 
																{
																	
																} 
																else 
																{
																	$data="Error: " . $sql1 . "<br>" . $DB->error;
																} 
															
																	
																	
																}
															}
												
													$sqlInsert3 = "Insert into tblProductServiceCategory(ProductID,ServiceID,StoredID,CategoryID) values('".$prod."','".$last_idp2."','".$newstoreid."','".$Categories1."')";
													//echo $sqlInsert3."<br/>";
												    ExecuteNQ($sqlInsert3);
														}
										}
									 }
								
							  }
							  unset($sertu1);
							
						}
						if(!empty($sertu2))
						{
							for($j=0;$j<count($sertu2);$j++)
							  {
								    if($sertu2[0]!='')
								     {
										 		$sepASDD=select("*","tblServices","ServiceID='".$sertu2[$j]."' group by ServiceCode");
								    $ServiceID3=$sepASDD[0]['ServiceID'];
									$ServiceName=$sepASDD[0]['ServiceName'];
									$ServiceCode=$sepASDD[0]['ServiceCode'];
									$Time=$sepASDD[0]['Time'];
									$ServiceCost=$sepASDD[0]['ServiceCost'];
									$ServiceCommission=$sepASDD[0]['ServiceCommission'];
									$GMPercentage=$sepASDD[0]['GMPercentage'];
									$MRPLessTax=$sepASDD[0]['MRPLessTax'];
									$DirectCost=$sepASDD[0]['DirectCost'];
									$GrossMargin=$sepASDD[0]['GrossMargin'];
									$Status=$sepASDD[0]['Status'];
									$ProductPrice=$sepASDD[0]['ProductPrice'];
									
									$sqlInsert888 = "INSERT INTO tblServices (ServiceName, ServiceCode, ServiceCost, ServiceCommission, MRPLessTax, DirectCost, GMPercentage, GrossMargin,Status,StoreID,Time,ProductPrice) VALUES
									('".$ServiceName."', '".$ServiceCode."', '".$ServiceCost."', '".$ServiceCommission."','".$MRPLessTax."', '".$DirectCost."','".$GMPercentage."', '".$GrossMargin."', '".$Status."' , '".$newstoreid."' , '".$Time."', '".$ProductPrice."')";
								  //echo $sqlInsert888;
									
						        	if ($DB->query($sqlInsert888) === TRUE) 
									{
										
										$last_idp3 = $DB->insert_id;
									}
									else
									{
										echo "Error: " . $sql . "<br>" . $conn->error;
									}  
									 
									
									////////////////////Add TAX for service/////////////////////////////////
									
									$pqr="Insert into tblServicesCharges (ServiceID,ChargeNameID,Status) Values ('$last_idp3','1','0')";
			                      // echo $pqr;
				
										  if ($DB->query($pqr) === TRUE) 
										{
												// echo "Record Update successfully.";
										}
										else
										{
											//echo "Error2";
										}  
										
										
										 $sql = "SELECT ProductID FROM tblProductsServices WHERE CategoryID='$Categories2' and ServiceID='$ServiceID3'";
								//	echo $sql."<br>";
									
									$RS = $DB->query($sql);

										if ($RS->num_rows > 0)

										{
                               	while($row = $RS->fetch_assoc())

														{
															$prod=$row['ProductID'];
															$selp=select("ProductID","tblNewProducts","ProductID='".$prod."'");
												
															$ProductID1234=$selp[0]['ProductID'];
															if($ProductID1234!="")
															{
																		
																$sqlInsert2 = "Insert into tblProductsServices(ProductID,ProductStockID,ServiceID, Status,CategoryID,StoreID,DateTimeStamp) values ('".$prod."','0', '".$last_idp3."','0','".$Categories2."','".$newstoreid."','".$getfrom."')";
																//echo $sqlInsert2."<br/>";
																ExecuteNQ($sqlInsert2);
																
																$sql1="INSERT INTO  tblStoreProduct(ProductID,StoreID,Stock,ProductStockID,UpdatePerQtyServe) VALUES ('".$prod."','".$newstoreid."','5','0','1')";	
		                                                      
																 if ($DB->query($sql1) === TRUE) 
																{
																	$data=3;
																} 
																else 
																{
																	$data="Error: " . $sql1 . "<br>" . $DB->error;
																} 
															
															}
															else
															{
																$selpT=select("*","tblNewProductStocks","ProductStockID='".$prod."'");
																foreach($selpT as $val)
																{
																	$sqlInsert4 = "Insert into tblProductsServices(ProductID,ProductStockID, ServiceID, Status,CategoryID,StoreID,DateTimeStamp) values
																	('".$val['ProductID']."','".$val['ProductStockID']."','".$last_idp3."','1','".$Categories2."','".$newstoreid."','".$getfrom."')";
																   // echo $sqlInsert4."<br/>";
																	ExecuteNQ($sqlInsert4);
																	
																	$sql1="INSERT INTO  tblStoreProduct(ProductID,StoreID,Stock,ProductStockID,UpdatePerQtyServe) VALUES ('".$val['ProductID']."','".$newstoreid."','5','".$val['ProductStockID']."','1')";	
		                                                       // echo $sql1;
																 if ($DB->query($sql1) === TRUE) 
																{
																	
																} 
																else 
																{
																	$data="Error: " . $sql1 . "<br>" . $DB->error;
																} 
															
																	
																	
																}
															}
												
													$sqlInsert3 = "Insert into tblProductServiceCategory(ProductID,ServiceID,StoredID,CategoryID) values('".$prod."','".$last_idp3."','".$newstoreid."','".$Categories2."')";
													//echo $sqlInsert3."<br/>";
												    ExecuteNQ($sqlInsert3);
														}
										}
									 }
							
							  }
							  unset($sertu2);
						}
						if(!empty($sertu3))
						{
							for($j=0;$j<count($sertu3);$j++)
							  {
								     if($sertu3[0]!='')
								     {
										 	$sepASDD=select("*","tblServices","ServiceID='".$sertu3[$j]."' group by ServiceCode");
								    $ServiceID4=$sepASDD[0]['ServiceID'];
									$ServiceName=$sepASDD[0]['ServiceName'];
									$ServiceCode=$sepASDD[0]['ServiceCode'];
									$Time=$sepASDD[0]['Time'];
									$ServiceCost=$sepASDD[0]['ServiceCost'];
									$ServiceCommission=$sepASDD[0]['ServiceCommission'];
									$GMPercentage=$sepASDD[0]['GMPercentage'];
									$MRPLessTax=$sepASDD[0]['MRPLessTax'];
									$DirectCost=$sepASDD[0]['DirectCost'];
									$GrossMargin=$sepASDD[0]['GrossMargin'];
									$Status=$sepASDD[0]['Status'];
									$ProductPrice=$sepASDD[0]['ProductPrice'];
									
									$sqlInsert888 = "INSERT INTO tblServices (ServiceName, ServiceCode, ServiceCost, ServiceCommission, MRPLessTax, DirectCost, GMPercentage, GrossMargin,Status,StoreID,Time,ProductPrice) VALUES
									('".$ServiceName."', '".$ServiceCode."', '".$ServiceCost."', '".$ServiceCommission."','".$MRPLessTax."', '".$DirectCost."','".$GMPercentage."', '".$GrossMargin."', '".$Status."' , '".$newstoreid."' , '".$Time."', '".$ProductPrice."')";
								  //echo $sqlInsert888;
									
						        	if ($DB->query($sqlInsert888) === TRUE) 
									{
										
										$last_idp4 = $DB->insert_id;
									}
									else
									{
										echo "Error: " . $sql . "<br>" . $conn->error;
									}  
									 
									
									////////////////////Add TAX for service/////////////////////////////////
									
									$pqr="Insert into tblServicesCharges (ServiceID,ChargeNameID,Status) Values ('$last_idp4','1','0')";
			                       //echo $pqr;
				
										  if ($DB->query($pqr) === TRUE) 
										{
												// echo "Record Update successfully.";
										}
										else
										{
											//echo "Error2";
										}  
										
										
										 $sql = "SELECT ProductID FROM tblProductsServices WHERE CategoryID='$Categories3' and ServiceID='$ServiceID4'";
									//echo $sql."<br>";
									
									$RS = $DB->query($sql);

										if ($RS->num_rows > 0)

										{
                               	while($row = $RS->fetch_assoc())

														{
															$prod=$row['ProductID'];
															$selp=select("ProductID","tblNewProducts","ProductID='".$prod."'");
												
															$ProductID1234=$selp[0]['ProductID'];
															if($ProductID1234!="")
															{
																		
																$sqlInsert2 = "Insert into tblProductsServices(ProductID,ProductStockID,ServiceID, Status,CategoryID,StoreID,DateTimeStamp) values ('".$prod."','0', '".$last_idp4."','0','".$Categories3."','".$newstoreid."','".$getfrom."')";
																//echo $sqlInsert2."<br/>";
																ExecuteNQ($sqlInsert2);
																
																$sql1="INSERT INTO  tblStoreProduct(ProductID,StoreID,Stock,ProductStockID,UpdatePerQtyServe) VALUES ('".$prod."','".$newstoreid."','5','0','1')";	
		                                                      
																 if ($DB->query($sql1) === TRUE) 
																{
																	$data=3;
																} 
																else 
																{
																	$data="Error: " . $sql1 . "<br>" . $DB->error;
																} 
															
															}
															else
															{
																$selpT=select("*","tblNewProductStocks","ProductStockID='".$prod."'");
																foreach($selpT as $val)
																{
																	$sqlInsert4 = "Insert into tblProductsServices(ProductID,ProductStockID, ServiceID, Status,CategoryID,StoreID,DateTimeStamp) values
																	('".$val['ProductID']."','".$val['ProductStockID']."','".$last_idp4."','1','".$Categories3."','".$newstoreid."','".$getfrom."')";
																   // echo $sqlInsert4."<br/>";
																	ExecuteNQ($sqlInsert4);
																	
																	$sql1="INSERT INTO  tblStoreProduct(ProductID,StoreID,Stock,ProductStockID,UpdatePerQtyServe) VALUES ('".$val['ProductID']."','".$newstoreid."','5','".$val['ProductStockID']."','1')";	
		                                                       // echo $sql1;
																 if ($DB->query($sql1) === TRUE) 
																{
																	
																} 
																else 
																{
																	$data="Error: " . $sql1 . "<br>" . $DB->error;
																} 
															
																	
																	
																}
															}
												
													$sqlInsert3 = "Insert into tblProductServiceCategory(ProductID,ServiceID,StoredID,CategoryID) values('".$prod."','".$last_idp4."','".$newstoreid."','".$Categories3."')";
													echo $sqlInsert3."<br/>";
												    ExecuteNQ($sqlInsert3);
														}
										}
									 }
								
							  }
							  unset($sertu3);
						}
					if(!empty($sertu4))
						{
							for($j=0;$j<count($sertu4);$j++)
							  {
								
								   if($sertu4[0]!='')
								     {
										 $sepASDD=select("*","tblServices","ServiceID='".$sertu4[$j]."' group by ServiceCode");
								    $ServiceID5=$sepASDD[0]['ServiceID'];
									$ServiceName=$sepASDD[0]['ServiceName'];
									$ServiceCode=$sepASDD[0]['ServiceCode'];
									$Time=$sepASDD[0]['Time'];
									$ServiceCost=$sepASDD[0]['ServiceCost'];
									$ServiceCommission=$sepASDD[0]['ServiceCommission'];
									$GMPercentage=$sepASDD[0]['GMPercentage'];
									$MRPLessTax=$sepASDD[0]['MRPLessTax'];
									$DirectCost=$sepASDD[0]['DirectCost'];
									$GrossMargin=$sepASDD[0]['GrossMargin'];
									$Status=$sepASDD[0]['Status'];
									$ProductPrice=$sepASDD[0]['ProductPrice'];
									
									$sqlInsert888 = "INSERT INTO tblServices (ServiceName, ServiceCode, ServiceCost, ServiceCommission, MRPLessTax, DirectCost, GMPercentage, GrossMargin,Status,StoreID,Time,ProductPrice) VALUES
									('".$ServiceName."', '".$ServiceCode."', '".$ServiceCost."', '".$ServiceCommission."','".$MRPLessTax."', '".$DirectCost."','".$GMPercentage."', '".$GrossMargin."', '".$Status."' , '".$newstoreid."' , '".$Time."', '".$ProductPrice."')";
								  //echo $sqlInsert888;
									
						        	if ($DB->query($sqlInsert888) === TRUE) 
									{
										
										$last_idp5 = $DB->insert_id;
									}
									else
									{
										echo "Error: " . $sql . "<br>" . $conn->error;
									}  
									 
									
									////////////////////Add TAX for service/////////////////////////////////
									
									$pqr="Insert into tblServicesCharges (ServiceID,ChargeNameID,Status) Values ('$last_idp5','1','0')";
			                      // echo $pqr;
				
										  if ($DB->query($pqr) === TRUE) 
										{
												// echo "Record Update successfully.";
										}
										else
										{
											//echo "Error2";
										}  
										
										
										 $sql = "SELECT ProductID FROM tblProductsServices WHERE CategoryID='$Categories2' and ServiceID='$ServiceID5'";
									//echo $sql."<br>";
									
									$RS = $DB->query($sql);

										if ($RS->num_rows > 0)

										{
                               	while($row = $RS->fetch_assoc())

														{
															$prod=$row['ProductID'];
															$selp=select("ProductID","tblNewProducts","ProductID='".$prod."'");
												
															$ProductID1234=$selp[0]['ProductID'];
															if($ProductID1234!="")
															{
																		
																$sqlInsert2 = "Insert into tblProductsServices(ProductID,ProductStockID,ServiceID, Status,CategoryID,StoreID,DateTimeStamp) values ('".$prod."','0', '".$last_idp5."','0','".$Categories4."','".$newstoreid."','".$getfrom."')";
																//echo $sqlInsert2."<br/>";
																ExecuteNQ($sqlInsert2);
																
																$sql1="INSERT INTO  tblStoreProduct(ProductID,StoreID,Stock,ProductStockID,UpdatePerQtyServe) VALUES ('".$prod."','".$newstoreid."','5','0','1')";	
		                                                      
																 if ($DB->query($sql1) === TRUE) 
																{
																	$data=3;
																} 
																else 
																{
																	$data="Error: " . $sql1 . "<br>" . $DB->error;
																} 
															
															}
															else
															{
																$selpT=select("*","tblNewProductStocks","ProductStockID='".$prod."'");
																foreach($selpT as $val)
																{
																	$sqlInsert4 = "Insert into tblProductsServices(ProductID,ProductStockID, ServiceID, Status,CategoryID,StoreID,DateTimeStamp) values
																	('".$val['ProductID']."','".$val['ProductStockID']."','".$last_idp5."','1','".$Categories4."','".$newstoreid."','".$getfrom."')";
																    //echo $sqlInsert4."<br/>";
																	ExecuteNQ($sqlInsert4);
																	
																	$sql1="INSERT INTO  tblStoreProduct(ProductID,StoreID,Stock,ProductStockID,UpdatePerQtyServe) VALUES ('".$val['ProductID']."','".$newstoreid."','5','".$val['ProductStockID']."','1')";	
		                                                       // echo $sql1;
																 if ($DB->query($sql1) === TRUE) 
																{
																	
																} 
																else 
																{
																	$data="Error: " . $sql1 . "<br>" . $DB->error;
																} 
															
																	
																	
																}
															}
												
													$sqlInsert3 = "Insert into tblProductServiceCategory(ProductID,ServiceID,StoredID,CategoryID) values('".$prod."','".$last_idp5."','".$newstoreid."','".$Categories4."')";
													//echo $sqlInsert3."<br/>";
												    ExecuteNQ($sqlInsert3);
														}
										}
									 }
									
							  }
							  unset($sertu4);
						}
						if(!empty($sertu5))
						{
							for($j=0;$j<count($sertu5);$j++)
							  {
								   if($sertu5[0]!='')
								     {
										 	$sepASDD=select("*","tblServices","ServiceID='".$sertu5[$j]."' group by ServiceCode");
								    $ServiceID6=$sepASDD[0]['ServiceID'];
									$ServiceName=$sepASDD[0]['ServiceName'];
									$ServiceCode=$sepASDD[0]['ServiceCode'];
									$Time=$sepASDD[0]['Time'];
									$ServiceCost=$sepASDD[0]['ServiceCost'];
									$ServiceCommission=$sepASDD[0]['ServiceCommission'];
									$GMPercentage=$sepASDD[0]['GMPercentage'];
									$MRPLessTax=$sepASDD[0]['MRPLessTax'];
									$DirectCost=$sepASDD[0]['DirectCost'];
									$GrossMargin=$sepASDD[0]['GrossMargin'];
									$Status=$sepASDD[0]['Status'];
									$ProductPrice=$sepASDD[0]['ProductPrice'];
									
									$sqlInsert888 = "INSERT INTO tblServices (ServiceName, ServiceCode, ServiceCost, ServiceCommission, MRPLessTax, DirectCost, GMPercentage, GrossMargin,Status,StoreID,Time,ProductPrice) VALUES
									('".$ServiceName."', '".$ServiceCode."', '".$ServiceCost."', '".$ServiceCommission."','".$MRPLessTax."', '".$DirectCost."','".$GMPercentage."', '".$GrossMargin."', '".$Status."' , '".$newstoreid."' , '".$Time."', '".$ProductPrice."')";
								  //echo $sqlInsert888;
									
						        	if ($DB->query($sqlInsert888) === TRUE) 
									{
										
										$last_idp6 = $DB->insert_id;
									}
									else
									{
										echo "Error: " . $sql . "<br>" . $conn->error;
									}  
									 
									
									////////////////////Add TAX for service/////////////////////////////////
									
									$pqr="Insert into tblServicesCharges (ServiceID,ChargeNameID,Status) Values ('$last_idp6','1','0')";
			                      // echo $pqr;
				
										  if ($DB->query($pqr) === TRUE) 
										{
												// echo "Record Update successfully.";
										}
										else
										{
											//echo "Error2";
										}  
										
										
										 $sql = "SELECT ProductID FROM tblProductsServices WHERE CategoryID='$Categories5' and ServiceID='$ServiceID6'";
									//echo $sql."<br>";
									
									$RS = $DB->query($sql);

										if ($RS->num_rows > 0)

										{
                               	while($row = $RS->fetch_assoc())

														{
															$prod=$row['ProductID'];
															$selp=select("ProductID","tblNewProducts","ProductID='".$prod."'");
												
															$ProductID1234=$selp[0]['ProductID'];
															if($ProductID1234!="")
															{
																		
																$sqlInsert2 = "Insert into tblProductsServices(ProductID,ProductStockID,ServiceID, Status,CategoryID,StoreID,DateTimeStamp) values ('".$prod."','0', '".$last_idp6."','0','".$Categories5."','".$newstoreid."','".$getfrom."')";
																//echo $sqlInsert2."<br/>";
																ExecuteNQ($sqlInsert2);
																
																$sql1="INSERT INTO  tblStoreProduct(ProductID,StoreID,Stock,ProductStockID,UpdatePerQtyServe) VALUES ('".$prod."','".$newstoreid."','5','0','1')";	
		                                                      
																 if ($DB->query($sql1) === TRUE) 
																{
																	$data=3;
																} 
																else 
																{
																	$data="Error: " . $sql1 . "<br>" . $DB->error;
																} 
															
															}
															else
															{
																$selpT=select("*","tblNewProductStocks","ProductStockID='".$prod."'");
																foreach($selpT as $val)
																{
																	$sqlInsert4 = "Insert into tblProductsServices(ProductID,ProductStockID, ServiceID, Status,CategoryID,StoreID,DateTimeStamp) values
																	('".$val['ProductID']."','".$val['ProductStockID']."','".$last_idp6."','1','".$Categories5."','".$newstoreid."','".$getfrom."')";
																   // echo $sqlInsert4."<br/>";
																	ExecuteNQ($sqlInsert4);
																	
																	$sql1="INSERT INTO  tblStoreProduct(ProductID,StoreID,Stock,ProductStockID,UpdatePerQtyServe) VALUES ('".$val['ProductID']."','".$newstoreid."','5','".$val['ProductStockID']."','1')";	
		                                                       // echo $sql1;
																 if ($DB->query($sql1) === TRUE) 
																{
																	
																} 
																else 
																{
																	$data="Error: " . $sql1 . "<br>" . $DB->error;
																} 
															
																	
																	
																}
															}
												
													$sqlInsert3 = "Insert into tblProductServiceCategory(ProductID,ServiceID,StoredID,CategoryID) values('".$prod."','".$last_idp6."','".$newstoreid."','".$Categories5."')";
													//echo $sqlInsert3."<br/>";
												    ExecuteNQ($sqlInsert3);
														}
										}
									 }
								
								
							  }
							  unset($sertu5);
						}
						if(!empty($sertu6))
						{
							for($j=0;$j<count($sertu6);$j++)
							  {
								
								     if($sertu6[0]!='')
								     {
										 	$sepASDD=select("*","tblServices","ServiceID='".$sertu6[$j]."' group by ServiceCode");
								    $ServiceID7=$sepASDD[0]['ServiceID'];
									$ServiceName=$sepASDD[0]['ServiceName'];
									$ServiceCode=$sepASDD[0]['ServiceCode'];
									$Time=$sepASDD[0]['Time'];
									$ServiceCost=$sepASDD[0]['ServiceCost'];
									$ServiceCommission=$sepASDD[0]['ServiceCommission'];
									$GMPercentage=$sepASDD[0]['GMPercentage'];
									$MRPLessTax=$sepASDD[0]['MRPLessTax'];
									$DirectCost=$sepASDD[0]['DirectCost'];
									$GrossMargin=$sepASDD[0]['GrossMargin'];
									$Status=$sepASDD[0]['Status'];
									$ProductPrice=$sepASDD[0]['ProductPrice'];
									
									$sqlInsert888 = "INSERT INTO tblServices (ServiceName, ServiceCode, ServiceCost, ServiceCommission, MRPLessTax, DirectCost, GMPercentage, GrossMargin,Status,StoreID,Time,ProductPrice) VALUES
									('".$ServiceName."', '".$ServiceCode."', '".$ServiceCost."', '".$ServiceCommission."','".$MRPLessTax."', '".$DirectCost."','".$GMPercentage."', '".$GrossMargin."', '".$Status."' , '".$newstoreid."' , '".$Time."', '".$ProductPrice."')";
								  //echo $sqlInsert888;
									
						        	if ($DB->query($sqlInsert888) === TRUE) 
									{
										
										$last_idp7 = $DB->insert_id;
									}
									else
									{
										echo "Error: " . $sql . "<br>" . $conn->error;
									}  
									 
									
									////////////////////Add TAX for service/////////////////////////////////
									
									$pqr="Insert into tblServicesCharges (ServiceID,ChargeNameID,Status) Values ('$last_idp7','1','0')";
			                      // echo $pqr;
				
										  if ($DB->query($pqr) === TRUE) 
										{
												// echo "Record Update successfully.";
										}
										else
										{
											//echo "Error2";
										}  
										
										
										 $sql = "SELECT ProductID FROM tblProductsServices WHERE CategoryID='$Categories6' and ServiceID='$ServiceID7'";
									//echo $sql."<br>";
									
									$RS = $DB->query($sql);

										if ($RS->num_rows > 0)

										{
                               	while($row = $RS->fetch_assoc())

														{
															$prod=$row['ProductID'];
															$selp=select("ProductID","tblNewProducts","ProductID='".$prod."'");
												
															$ProductID1234=$selp[0]['ProductID'];
															if($ProductID1234!="")
															{
																		
																$sqlInsert2 = "Insert into tblProductsServices(ProductID,ProductStockID,ServiceID, Status,CategoryID,StoreID,DateTimeStamp) values ('".$prod."','0', '".$last_idp7."','0','".$Categories6."','".$newstoreid."','".$getfrom."')";
																//echo $sqlInsert2."<br/>";
																ExecuteNQ($sqlInsert2);
																
																$sql1="INSERT INTO  tblStoreProduct(ProductID,StoreID,Stock,ProductStockID,UpdatePerQtyServe) VALUES ('".$prod."','".$newstoreid."','5','0','1')";	
		                                                      
																 if ($DB->query($sql1) === TRUE) 
																{
																	$data=3;
																} 
																else 
																{
																	$data="Error: " . $sql1 . "<br>" . $DB->error;
																} 
															
															}
															else
															{
																$selpT=select("*","tblNewProductStocks","ProductStockID='".$prod."'");
																foreach($selpT as $val)
																{
																	$sqlInsert4 = "Insert into tblProductsServices(ProductID,ProductStockID, ServiceID, Status,CategoryID,StoreID,DateTimeStamp) values
																	('".$val['ProductID']."','".$val['ProductStockID']."','".$last_idp7."','1','".$Categories6."','".$newstoreid."','".$getfrom."')";
																   // echo $sqlInsert4."<br/>";
																	ExecuteNQ($sqlInsert4);
																	
																	$sql1="INSERT INTO  tblStoreProduct(ProductID,StoreID,Stock,ProductStockID,UpdatePerQtyServe) VALUES ('".$val['ProductID']."','".$newstoreid."','5','".$val['ProductStockID']."','1')";	
		                                                      //  echo $sql1;
																 if ($DB->query($sql1) === TRUE) 
																{
																	
																} 
																else 
																{
																	$data="Error: " . $sql1 . "<br>" . $DB->error;
																} 
															
																	
																	
																}
															}
												
													$sqlInsert3 = "Insert into tblProductServiceCategory(ProductID,ServiceID,StoredID,CategoryID) values('".$prod."','".$last_idp7."','".$newstoreid."','".$Categories6."')";
													//echo $sqlInsert3."<br/>";
												    ExecuteNQ($sqlInsert3);
														}
										}
									 }
								
							  }
							  unset($sertu6);
						}
						if(!empty($sertu7))
						{
							for($j=0;$j<count($sertu7);$j++)
							  {
								   if($sertu7[0]!='')
								     {
										 $sepASDD=select("*","tblServices","ServiceID='".$sertu7[$j]."' group by ServiceCode");
								    $ServiceID8=$sepASDD[0]['ServiceID'];
									$ServiceName=$sepASDD[0]['ServiceName'];
									$ServiceCode=$sepASDD[0]['ServiceCode'];
									$Time=$sepASDD[0]['Time'];
									$ServiceCost=$sepASDD[0]['ServiceCost'];
									$ServiceCommission=$sepASDD[0]['ServiceCommission'];
									$GMPercentage=$sepASDD[0]['GMPercentage'];
									$MRPLessTax=$sepASDD[0]['MRPLessTax'];
									$DirectCost=$sepASDD[0]['DirectCost'];
									$GrossMargin=$sepASDD[0]['GrossMargin'];
									$Status=$sepASDD[0]['Status'];
									$ProductPrice=$sepASDD[0]['ProductPrice'];
									
									$sqlInsert888 = "INSERT INTO tblServices (ServiceName, ServiceCode, ServiceCost, ServiceCommission, MRPLessTax, DirectCost, GMPercentage, GrossMargin,Status,StoreID,Time,ProductPrice) VALUES
									('".$ServiceName."', '".$ServiceCode."', '".$ServiceCost."', '".$ServiceCommission."','".$MRPLessTax."', '".$DirectCost."','".$GMPercentage."', '".$GrossMargin."', '".$Status."' , '".$newstoreid."' , '".$Time."', '".$ProductPrice."')";
								  //echo $sqlInsert888;
									
						        	if ($DB->query($sqlInsert888) === TRUE) 
									{
										
										$last_idp8 = $DB->insert_id;
									}
									else
									{
										echo "Error: " . $sql . "<br>" . $conn->error;
									}  
									 
									
									////////////////////Add TAX for service/////////////////////////////////
									
									$pqr="Insert into tblServicesCharges (ServiceID,ChargeNameID,Status) Values ('$last_idp8','1','0')";
			                      // echo $pqr;
				
										  if ($DB->query($pqr) === TRUE) 
										{
												// echo "Record Update successfully.";
										}
										else
										{
											//echo "Error2";
										}  
										
										
										 $sql = "SELECT ProductID FROM tblProductsServices WHERE CategoryID='$Categories7' and ServiceID='$ServiceID8'";
									//echo $sql."<br>";
									
									$RS = $DB->query($sql);

										if ($RS->num_rows > 0)

										{
                               	while($row = $RS->fetch_assoc())

														{
															$prod=$row['ProductID'];
															$selp=select("ProductID","tblNewProducts","ProductID='".$prod."'");
												
															$ProductID1234=$selp[0]['ProductID'];
															if($ProductID1234!="")
															{
																		
																$sqlInsert2 = "Insert into tblProductsServices(ProductID,ProductStockID,ServiceID, Status,CategoryID,StoreID,DateTimeStamp) values ('".$prod."','0', '".$last_idp8."','0','".$Categories7."','".$newstoreid."','".$getfrom."')";
																//echo $sqlInsert2."<br/>";
																ExecuteNQ($sqlInsert2);
																
																$sql1="INSERT INTO  tblStoreProduct(ProductID,StoreID,Stock,ProductStockID,UpdatePerQtyServe) VALUES ('".$prod."','".$newstoreid."','5','0','1')";	
		                                                      
																 if ($DB->query($sql1) === TRUE) 
																{
																	$data=3;
																} 
																else 
																{
																	$data="Error: " . $sql1 . "<br>" . $DB->error;
																} 
															
															}
															else
															{
																$selpT=select("*","tblNewProductStocks","ProductStockID='".$prod."'");
																foreach($selpT as $val)
																{
																	$sqlInsert4 = "Insert into tblProductsServices(ProductID,ProductStockID, ServiceID, Status,CategoryID,StoreID,DateTimeStamp) values
																	('".$val['ProductID']."','".$val['ProductStockID']."','".$last_idp8."','1','".$Categories7."','".$newstoreid."','".$getfrom."')";
																   // echo $sqlInsert4."<br/>";
																	ExecuteNQ($sqlInsert4);
																	
																	$sql1="INSERT INTO  tblStoreProduct(ProductID,StoreID,Stock,ProductStockID,UpdatePerQtyServe) VALUES ('".$val['ProductID']."','".$newstoreid."','5','".$val['ProductStockID']."','1')";	
		                                                       // echo $sql1;
																 if ($DB->query($sql1) === TRUE) 
																{
																	
																} 
																else 
																{
																	$data="Error: " . $sql1 . "<br>" . $DB->error;
																} 
															
																	
																	
																}
															}
												
													$sqlInsert3 = "Insert into tblProductServiceCategory(ProductID,ServiceID,StoredID,CategoryID) values('".$prod."','".$last_idp8."','".$newstoreid."','".$Categories7."')";
													//echo $sqlInsert3."<br/>";
												    ExecuteNQ($sqlInsert3);
														}
										}
									 }
								
									
							  }
							  unset($sertu7);
						}
						
						
						
			
			
			$DB->close();
			//die();
			?>
					<!--<div class="alert-content">
						<h4 class="alert-title">Record Added Successfully.</h4>
					</div>-->
					
			<?php
	 	die('<div class="alert alert-close alert-success">
					<div class="bg-green alert-icon"><i class="glyph-icon icon-check"></i></div>
					<div class="alert-content">
						<h4 class="alert-title">Record Added Successfully.</h4>
					</div>
				</div>'); 
          
			}
			
			// die();

		}
		
	}	
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<?php require_once("incMetaScript.fya"); ?>
</head>

<body>
	 <div id="sb-site">
        
		<?php require_once("incOpenLayout.fya"); ?>
		
		
        <?php require_once("incLoader.fya"); ?>
		
		<div id="page-wrapper">
			<div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>
			<?php require_once("incLeftMenu.fya"); ?>
			
			<div id="page-content-wrapper">
				<div id="page-content">
                    
					<?php require_once("incHeader.fya"); ?>
					
					<div id="page-title">
                        <h2>Add New Outlet</h2>
                    
                    </div>
				
					<div class="panel">
						<div class="panel-body">
							<div class="fa-hover">	
								<a class="btn btn-primary btn-lg btn-block"><i class="fa fa-backward">Following Details Required</i></a>
							</div>
						
							<div class="panel-body">
								<!--<form role="form" class="form-horizontal bordered-row enquiry_form" onSubmit="proceed_formsubmit('.enquiry_form', '<?=$strMyActionPage?>', '.result_message', '', '', '','.imageupload'); return false;" enctype="multipart/form-data">-->
							<form role="form" class="form-horizontal bordered-row enquiry_form" onSubmit="proceed_formsubmit('.enquiry_form', '<?=$strMyActionPage?>', '.result_message', '', '','', '.imageupload'); return false;">
								<span class="result_message">&nbsp; <br>
								</span>
								<br>
								<input type="hidden" name="step" value="edit">

								
									<h3 class="title-hero">Transfer Data</h3>
									<div class="example-box-wrapper">
<?php

$strID = $strAdminID;
// echo $strID."<br>";
$DB = Connect();
?>
<script>
		function checktypewqfge(evt)
		{
			var type=$(evt).val();
	/*      typef=[];
		 var typef = $('#Categories').val(); */
		// alert(typef)
	 	$.ajax({
		type:"post",	
		data:"typef="+type,
		url: "servicecatnewstore.php",
		success:function(res)
				{
			//alert(res)
				var rep= $.trim(res);
					$("#serviceid").show();
					$("#serviceid").html("");
					$("#serviceid").html("");
					$("#serviceid").html(rep);
				}
			})
		}
		function checktypewqfge1(evt)
		{
			var type=$(evt).val();
	/*      typef=[];
		 var typef = $('#Categories').val(); */
		// alert(typef)
	 	$.ajax({
		type:"post",	
		data:"typef="+type,
		url: "servicecatnewstore1.php",
		success:function(res)
				{
			//alert(res)
				var rep= $.trim(res);
					$("#serviceid1").show();
					$("#serviceid1").html("");
					$("#serviceid1").html("");
					$("#serviceid1").html(rep);
				}
			})
		}
	function checktypewqfge2(evt)
		{
			var type=$(evt).val();
	/*      typef=[];
		 var typef = $('#Categories').val(); */
		// alert(typef)
	 	$.ajax({
		type:"post",	
		data:"typef="+type,
		url: "servicecatnewstore2.php",
		success:function(res)
				{
			//alert(res)
				var rep= $.trim(res);
					$("#serviceid2").show();
					$("#serviceid2").html("");
					$("#serviceid2").html("");
					$("#serviceid2").html(rep);
				}
			})
		}
		function checktypewqfge3(evt)
		{
			var type=$(evt).val();
	/*      typef=[];
		 var typef = $('#Categories').val(); */
		// alert(typef)
	 	$.ajax({
		type:"post",	
		data:"typef="+type,
		url: "servicecatnewstore3.php",
		success:function(res)
				{
			//alert(res)
				var rep= $.trim(res);
					$("#serviceid3").show();
					$("#serviceid3").html("");
					$("#serviceid3").html("");
					$("#serviceid3").html(rep);
				}
			})
		}
		function checktypewqfge4(evt)
		{
			var type=$(evt).val();
	/*      typef=[];
		 var typef = $('#Categories').val(); */
		// alert(typef)
	 	$.ajax({
		type:"post",	
		data:"typef="+type,
		url: "servicecatnewstore4.php",
		success:function(res)
				{
			//alert(res)
				var rep= $.trim(res);
					$("#serviceid4").show();
					$("#serviceid4").html("");
					$("#serviceid4").html("");
					$("#serviceid4").html(rep);
				}
			})
		}
		function checktypewqfge5(evt)
		{
			var type=$(evt).val();
	/*      typef=[];
		 var typef = $('#Categories').val(); */
		// alert(typef)
	 	$.ajax({
		type:"post",	
		data:"typef="+type,
		url: "servicecatnewstore5.php",
		success:function(res)
				{
			//alert(res)
				var rep= $.trim(res);
					$("#serviceid5").show();
					$("#serviceid5").html("");
					$("#serviceid5").html("");
					$("#serviceid5").html(rep);
				}
			})
		}
	function checktypewqfge6(evt)
		{
			var type=$(evt).val();
	/*      typef=[];
		 var typef = $('#Categories').val(); */
		// alert(typef)
	 	$.ajax({
		type:"post",	
		data:"typef="+type,
		url: "servicecatnewstore6.php",
		success:function(res)
				{
			//alert(res)
				var rep= $.trim(res);
					$("#serviceid6").show();
					$("#serviceid6").html("");
					$("#serviceid6").html("");
					$("#serviceid6").html(rep);
				}
			})
		}
		function checktypewqfge7(evt)
		{
			var type=$(evt).val();
	/*      typef=[];
		 var typef = $('#Categories').val(); */
		// alert(typef)
	 	$.ajax({
		type:"post",	
		data:"typef="+type,
		url: "servicecatnewstore7.php",
		success:function(res)
				{
			//alert(res)
				var rep= $.trim(res);
					$("#serviceid7").show();
					$("#serviceid7").html("");
					$("#serviceid7").html("");
					$("#serviceid7").html(rep);
				}
			})
		}
	function checkproduct()
	{                
	
	     valuable=[];
		 var valuable = $('#ServiceID').val();
		
		 var catt=$("#Categories").val();
		
		$.ajax({
			type: 'POST',
			url: "GetProductTransfer.php",
			data: {
				id:valuable,
				catt:catt
			},
			success: function(response) {
				//alert(response)
				$(".ProductCatWise").html(response);
					
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				$(".ProductCatWise").html("<center><font color='red'><b>Please try again after some time</b></font></center>");
				return false;
				// alert (response);
			}
		});

	}
</script>
											<div class="form-group"><label class="col-sm-3 control-label">Store Name<span>*</span></label>
												<div class="col-sm-3"><input type="text" name="store" class="form-control required"  id="store"></div>
											</div>	
                                             <div class="form-group"><label class="col-sm-3 control-label">Store Official Address<span>*</span></label>
															<div class="col-sm-3"><textarea rows="4" name="StoreOfficialAddress" id="StoreOfficialAddress" class="form-control required  " placeholder="Store Official Address"></textarea></div>
											</div>
											  <div class="form-group"><label class="col-sm-3 control-label">Store Billing Address<span>*</span></label>
															<div class="col-sm-3"><textarea rows="4" name="StoreBillingAddress" id="StoreBillingAddress" class="form-control required  " placeholder="Store Billing Address"></textarea></div>
											</div>
											  <div class="form-group"><label class="col-sm-3 control-label">Store Official Email<span>*</span></label>
															<div class="col-sm-3"><input rows="4" name="StoreOfficialEmailID" id="StoreOfficialEmailID" class="form-control required  " placeholder="Store Official Email"></div>
											</div>
											  <div class="form-group"><label class="col-sm-3 control-label">Store Billing Email<span>*</span></label>
															<div class="col-sm-3"><input rows="4" name="StoreBillingEmailID" id="StoreBillingEmailID" class="form-control required  " placeholder="Store Billing Email"></div>
											</div>
											  <div class="form-group"><label class="col-sm-3 control-label">Store Official Number<span>*</span></label>
															<div class="col-sm-3"><input rows="4" name="StoreOfficialNumber" id="StoreOfficialNumber" class="form-control required  " placeholder="Store Official Number"></div>
											</div>
										
											
											<div class="form-group"><label class="col-sm-3 control-label">Store Billing Number<span>*</span></label>
															<div class="col-sm-3"><input type="text" name="StoreBillingNumber" id="StoreBillingNumber" class="form-control required" placeholder="Store Billing Number"></div>
													</div>	
                                             <div class="form-group"><label class="col-sm-3 control-label">Nail Category<span>*</span></label>				

												<div class="col-sm-3">	
                                                 <select class="form-control required" id="Categories" name="Categories" onChange="checktypewqfge(this);" >
												
												<option value="" selected>--SELECT Category--</option>

												<?php 
												
                                          $DB = Connect();
                                                        
														$sep=select("*","tblCategories","CategoryID='22'");
                                                        $catid=$sep[0]['CategoryID'];
														$catname=$sep[0]['CategoryName'];

														?>
														  <option value="<?php echo $catid?>"><?php echo $catname?></option>
														<?php
													   
												$DB->close();																

												?>

												</select>

												</div>

												</div>
											<span id="serviceid"></span>
											 <div class="form-group"><label class="col-sm-3 control-label">Eyelash Category<span>*</span></label>	
											<div class="col-sm-3">	
                                                 <select class="form-control required" id="Categories" name="Categories1" onChange="checktypewqfge1(this);" >
												
												<option value="" selected>--SELECT Category--</option>

												<?php 
												
                                          $DB = Connect();
                                                        
														$sep=select("*","tblCategories","CategoryID='21'");
                                                        $catid=$sep[0]['CategoryID'];
														$catname=$sep[0]['CategoryName'];

														?>
														  <option value="<?php echo $catid?>"><?php echo $catname?></option>
														<?php
													   
												$DB->close();																

												?>

												</select>

												</div>

												</div>
											<span id="serviceid1"></span>
											 <div class="form-group"><label class="col-sm-3 control-label">Hand & Feet Category<span>*</span></label>	
												<div class="col-sm-3">	
                                                 <select class="form-control required" id="Categories" name="Categories2" onChange="checktypewqfge2(this);" >
												
												<option value="" selected>--SELECT Category--</option>

												<?php 
												
                                          $DB = Connect();
                                                        
														$sep=select("*","tblCategories","CategoryID='20'");
                                                        $catid=$sep[0]['CategoryID'];
														$catname=$sep[0]['CategoryName'];

														?>
														  <option value="<?php echo $catid?>"><?php echo $catname?></option>
														<?php
													   
												$DB->close();																

												?>

												</select>

												</div>

												</div>
											<span id="serviceid2"></span>
											 <div class="form-group"><label class="col-sm-3 control-label">Beauty Category<span>*</span></label>	
													<div class="col-sm-3">	
                                                 <select class="form-control required" id="Categories" name="Categories3" onChange="checktypewqfge3(this);" >
												
												<option value="" selected>--SELECT Category--</option>

												<?php 
												
                                                       $DB = Connect();
                                                        
														$sep=select("*","tblCategories","CategoryID='19'");
                                                        $catid=$sep[0]['CategoryID'];
														$catname=$sep[0]['CategoryName'];

														?>
														  <option value="<?php echo $catid?>"><?php echo $catname?></option>
														<?php
													   
												$DB->close();																

												?>

												</select>

												</div>

												</div>
											<span id="serviceid3"></span>
											 <div class="form-group"><label class="col-sm-3 control-label">Hair Category<span>*</span></label>	
											<div class="col-sm-3">	
                                                 <select class="form-control required" id="Categories" name="Categories4" onChange="checktypewqfge4(this);" >
												
												<option value="" selected>--SELECT Category--</option>

												<?php 
												
                                                       $DB = Connect();
                                                        
														$sep=select("*","tblCategories","CategoryID='18'");
                                                        $catid=$sep[0]['CategoryID'];
														$catname=$sep[0]['CategoryName'];

														?>
														  <option value="<?php echo $catid?>"><?php echo $catname?></option>
														<?php
													   
												$DB->close();																

												?>

												</select>

												</div>

												</div>
											<span id="serviceid4"></span>
											 <div class="form-group"><label class="col-sm-3 control-label">Manicure Category<span>*</span></label>	
													<div class="col-sm-3">	
                                                 <select class="form-control required" id="Categories" name="Categories5" onChange="checktypewqfge5(this);" >
												
												<option value="" selected>--SELECT Category--</option>

												<?php 
												
                                                       $DB = Connect();
                                                        
														$sep=select("*","tblCategories","CategoryID='32'");
                                                        $catid=$sep[0]['CategoryID'];
														$catname=$sep[0]['CategoryName'];

														?>
														  <option value="<?php echo $catid?>"><?php echo $catname?></option>
														<?php
													   
												$DB->close();																

												?>

												</select>

												</div>

												</div>
											<span id="serviceid5"></span>
											 <div class="form-group"><label class="col-sm-3 control-label">Pedicure Category<span>*</span></label>	
												<div class="col-sm-3">	
                                                 <select class="form-control required" id="Categories" name="Categories6" onChange="checktypewqfge6(this);" >
												
												<option value="" selected>--SELECT Category--</option>

												<?php 
												
                                                       $DB = Connect();
                                                        
														$sep=select("*","tblCategories","CategoryID='31'");
                                                        $catid=$sep[0]['CategoryID'];
														$catname=$sep[0]['CategoryName'];

														?>
														  <option value="<?php echo $catid?>"><?php echo $catname?></option>
														<?php
													   
												$DB->close();																

												?>

												</select>

												</div>

												</div>
											<span id="serviceid6"></span>
											 <div class="form-group"><label class="col-sm-3 control-label">Nail Polish Category<span>*</span></label>	
											<div class="col-sm-3">	
                                                 <select class="form-control required" id="Categories" name="Categories7" onChange="checktypewqfge7(this);" >
												
												<option value="" selected>--SELECT Category--</option>

												<?php 
												
                                                       $DB = Connect();
                                                        
														$sep=select("*","tblCategories","CategoryID='33'");
                                                        $catid=$sep[0]['CategoryID'];
														$catname=$sep[0]['CategoryName'];

														?>
														  <option value="<?php echo $catid?>"><?php echo $catname?></option>
														<?php
													   
												$DB->close();																

												?>

												</select>

												</div>

												</div>
											<span id="serviceid7"></span>
											
											
											
											 <span class="ProductCatWise" ></span>
											
											
											<div class="form-group"><label class="col-sm-3 control-label"></label>
												<input type="submit" class="btn ra-100 btn-primary" value="Update">
												
												<div class="col-sm-1"><a class="btn ra-100 btn-black-opacity" href="javascript:;" onclick="ClearInfo('enquiry_form');" title="Clear"><span>Clear</span></a></div>
											</div>
<?php
$DB->close();
?>										
				</div>
								</form>
							</div>
						</div>
					</div>			
                  
                </div>
            </div>
        </div>
		
        <?php require_once 'incFooter.fya'; ?>
		
    </div>
</body>

</html>									