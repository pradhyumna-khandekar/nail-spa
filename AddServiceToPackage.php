<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php

$strPageTitle = "Manage Customers | Nailspa";
$strDisplayTitle = "Manage Customers for Nailspa";
$strMenuID = "10";
$strMyTable = "tblServices";
$strMyTableID = "ServiceID";
//$strMyField = "CustomerMobileNo";
$strMyActionPage = "appointment_invoice.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights	

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $DB = Connect();
    $PackageID = $_POST["packageid"];
    $app_id = $_POST["app_id"];
    $cust_packageid = $_POST["cust_packageid"];
    $validdate = $_POST["validdate"];
    $service = $_POST["service"];
    $store = $_POST["store"];
    $app_date = $_POST["app_date"];
    $date = date("Y-m-d");
    $selp = select("*", "tblAppointments", "AppointmentID='" . $app_id . "'");
    $FreeService = $selp[0]['FreeService'];
    /* $seldata = select("ValidTill", "tblAppointmentPackageValidity", "AppointmentID='" . $app_id . "'");
      $ValidTill = $seldata[0]['ValidTill'];
      if ($date > $ValidTill) {
      echo 3;
      } else { */


    $apt_service_qty = select("*", "tblAppointmentsDetailsInvoice", "AppointmentID='" . $app_id . "' AND ServiceID='" . $service . "' AND PackageService=0");

    if (isset($apt_service_qty) && is_array($apt_service_qty) && count($apt_service_qty) > 0) {
        $invoice_service_quantity = $apt_service_qty[0]['qty'];

        $package_service_qty = select("*", "tblCustomerAppointmentPackage", "appointment_id='" . $app_id . "' AND service_id='" . $service . "'");
        //$package_service_quantity = $package_service_qty[0]['service_qty'];
        //$package_service_done = $package_service_qty[0]['service_qty_done'] && $package_service_qty[0]['service_qty_done'] != Null ? $package_service_qty[0]['service_qty_done'] : 0;
        $package_service_balance = isset($package_service_qty[0]['qty_remaining']) && $package_service_qty[0]['qty_remaining'] != '' ? $package_service_qty[0]['qty_remaining'] : 0;
        $service_cost = $package_service_qty[0]['service_amount'];
        $pre_used_qty = isset($package_service_qty[0]['service_qty_done']) ? $package_service_qty[0]['service_qty_done'] : 0;
        //echo "invoice_service_quantity".$invoice_service_quantity;
        //echo "package_service_balance".$package_service_balance;
        if ($invoice_service_quantity == $package_service_balance) {
            $qty_used = $invoice_service_quantity  + $pre_used_qty;
            $balance = 0;
            $package_cost_used = $service_cost * $invoice_service_quantity;
        } else if ($invoice_service_quantity > $package_service_balance) {
            $qty_used = $package_service_balance + $pre_used_qty;
            $balance = 0;
            $package_cost_used = $service_cost * $package_service_balance;
        } else if ($invoice_service_quantity < $package_service_balance) {
            $qty_used = $invoice_service_quantity + $pre_used_qty;
            $balance = $package_service_balance - $invoice_service_quantity;
            $package_cost_used = $service_cost * $invoice_service_quantity;
        }

        /* echo "qty_used".$qty_used;
          echo "balance".$balance;
          echo "package_cost_used".$package_cost_used;
          http_response_code(400); */
        if (isset($qty_used) && isset($balance) && isset($package_cost_used)) {
            /*
             * Update Customer Appointment Package Balance
             */

            $package_update = "UPDATE tblCustomerAppointmentPackage SET service_qty_done='" . $qty_used . "' ,qty_remaining='" . $balance . "',service_apt_id='" . $app_id . "',modified_date='" . date('Y-m-d H:i:s') . "'"
            . " WHERE appointment_id='" . $app_id . "' AND service_id='" . $service . "'";
            //echo $package_update;
            $DB->query($package_update);

            $package_cost_used_update = "UPDATE tblCustomerPackageAmt SET package_amt_used = package_amt_used + " . $package_cost_used . ",modified_date='" . date('Y-m-d H:i:s') . "'"
            . " WHERE appointment_id='" . $app_id . "' AND package_id='" . $PackageID . "'";
            //echo $package_cost_used_update;
            $DB->query($package_cost_used_update);

            /*
             * Update Package Discount
             */
            $package_service_amt_data = select("*", "tblCustomerPackageAmt", "appointment_id='" . $app_id . "'");
            $package_data = select("*", "tblPackages", "PackageID='" . $PackageID . "'");
            $package_actual_amount = isset($package_data[0]['PackagePrice']) ? $package_data[0]['PackagePrice'] : '';
            $package_sale_amount = isset($package_data[0]['PackageNewPrice']) ? $package_data[0]['PackageNewPrice'] : '';
            $service_used_amount = $package_service_amt_data[0]['package_amt_used'];
            if ($package_actual_amount > 0 && $package_sale_amount > 0) {
                $package_service_amount = ($package_sale_amount / $package_actual_amount) * $service_used_amount;
                $package_service_amount = round($package_service_amount);
                $package_disc = $service_used_amount - $package_service_amount;
                /*echo "<br>package_sale_amount=".$package_sale_amount;
                echo "<br>package_actual_amount=".$package_actual_amount;
                echo "<br>service_used_amount=".$service_used_amount;
                echo "<br>package_service_amount=".$package_service_amount;
                echo "<br>package_disc=".$package_disc.'=';
                http_response_code(400);*/
                $package_discount_update = "UPDATE tblAppointmentsDetailsInvoice SET package_discount = " . $package_disc . ""
                . " WHERE AppointmentID='" . $app_id . "' AND ServiceID='" . $service . "' AND PackageService = 0";
                $DB->query($package_discount_update);
            }
        }

        $service_data = select("*", "tblServices", "ServiceID='" . $service . "'");
        if (isset($service_data) && is_array($service_data) && count($service_data) > 0) {
            $package_service_data = select("*", "tblCustomerAppointmentPackage", "appointment_id='" . $app_id . "' AND service_code='" . $service_data[0]['ServiceCode'] . "'");
            $cust_package_id = $package_service_data[0]['CustomerPackageId'];
            if ($package_service_data[0]['service_qty_done'] >= $package_service_data[0]['service_qty']) {

                $qry_package_update = "UPDATE tblCustomerAppointmentPackage SET service_status='2' ,modified_date='" . date('Y-m-d H:i:s') . "'"
                . " WHERE id='" . $package_service_data[0]['id'] . "'";
                //echo $qry_package_update;
                $DB->query($qry_package_update);
            }
        }

        $package_data = select("*", "tblCustomerAppointmentPackage", "appointment_id='" . $app_id . "' AND qty_remaining > 0");

        if ($package_data == 0) {
            $qry_package_update = "UPDATE tblCustomerPackage SET package_status='2' ,modified_date='" . date('Y-m-d H:i:s') . "'"
            . " WHERE id='" . $cust_package_id . "'";
            $DB->query($qry_package_update);
        }

        /*
         * Deduct Service Discount if MemberShip Applied
         */
        $apt_detail_data = select("*", "tblAppointmentsDetailsInvoice", "AppointmentID='" . $app_id . "' AND ServiceID='" . $service . "'");

        $seldata = select("distinct(NotValidOnServices),MembershipName,Discount", "tblMembership", "MembershipID='" . $selp[0]['memberid'] . "'");
        $Discount = $seldata[0]['Discount'];
        $amount = $qty_used * $service_cost;
        $discountamount = $amount * $Discount / 100;

        $taxable_amt = $amount - $discountamount;
        $tax_deduct = $taxable_amt * 0.18;
        $discount_update = "UPDATE tblAppointmentsChargesInvoice SET ChargeAmount = ChargeAmount - " . round($tax_deduct) . ""
        . " WHERE AppointmentID='" . $app_id . "' AND AppointmentDetailsID='" . $apt_detail_data[0]['AppointmentDetailsID'] . "'";
        // echo $discount_update;
        $DB->query($discount_update);
    }



    $sqlInsert1 = "Insert into tblBillingPackage(PackageID, AppointmentID, ServiceID,Status,StoreID,ValidityStart,ValidityEnd) values('" . $PackageID . "','" . $app_id . "', '" . $service . "','1','" . $store . "','" . $app_date . "','" . $validdate . "')";
    if ($DB->query($sqlInsert1) === TRUE) {
        $last_id7 = $DB->insert_id;
        echo 2;
    } else {
        echo "Error: " . $sqlInsert1 . "<br>" . $conn->error;
    }
}


$DB->close();
/* } */
?>