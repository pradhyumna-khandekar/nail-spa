<?php require_once("setting.fya"); ?>
<?php

require_once 'incFirewall.fya';
require_once("order_helper.php");
$path = 'dompdf-master/autoload.inc.php';
require_once($path);

use Dompdf\Dompdf;
use Dompdf\Options;

$dompdf = new Dompdf();
$contxt = stream_context_create([
    'ssl' => [
        'verify_peer' => FALSE,
        'verify_peer_name' => FALSE,
        'allow_self_signed' => TRUE
    ]
        ]);
$options = new Options();
$dompdf->setHttpContext($contxt);
$dompdf->set_option('enable_css_float', TRUE);
$dompdf->set_option('isRemoteEnabled', TRUE);
$dompdf->set_option('debugKeepTemp', TRUE);
$dompdf->set_option('isHtml5ParserEnabled', TRUE);
$order_id = isset($_GET['id']) ? $_GET['id'] : 0;
$body = get_detail_data($order_id, 1);

$dompdf->loadHtml($body);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'portrait');
// Render the HTML as PDF
$dompdf->render();
$output = $dompdf->output();
$year = date("Y");
$month = date("m");
$day = date("d");
$filefolder = "pdfs/";
$filename = "pdfs/" . $year;
$filename2 = "pdfs/" . $year . "/" . $month;

if (file_exists($filefolder) == false) {
    mkdir($filefolder, 0777);
}

if (file_exists($filename)) {
    if (file_exists($filename2) == false) {
        mkdir($filename2, 0777);
    }
} else {
    mkdir($filename, 0777);
}
if (file_exists($filename)) {
    if (file_exists($filename2) == false) {
        mkdir($filename2, 0777);
    }
} else {
    mkdir($filename, 0777);
}
//exit;
$invoicename = $year . '/' . $month . '/_' . $order_id . '_order_' . $year . $month . $day . '_' . date('His') . '_' . rand(000, 999) . '.pdf';
$path = 'pdfs/' . $invoicename;


file_put_contents($path, $output);

header("Content-type: pdf");
header('Content-Disposition: attachment; filename="' . $invoicename . '"');
readfile($path);
unlink($path);
?>