<?php require_once("setting.fya"); ?>
<!DOCTYPE html>
<html>
    <head>
        <style>
            .log_in{
                text-decoration: none;
                color: rgb(255, 255, 255);
                background-color: rgb(208, 172, 82);
                padding: 4px 20px 8px 18px;
                margin-left: 30%;
                font-size: 15px;
                font-weight: 600;
            }
            .admin_panel{
                margin-left: 33%;
            }
        </style>
    <body>
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
            <tbody>
                <tr>
                    <td>
                        <table border="0" cellspacing="0" cellpadding="0" width="800" align="center">
                            <tbody>

                                <tr>
                                    <td>
                                        <table style="border-bottom:#790000 10px solid;border-left:#790000 10px solid;border-top:#790000 10px solid;border-right:#790000 10px solid;background-position: 50% -55px;" border="0" cellspacing="0" cellpadding="0" width="800" bgcolor="#ffffff" align="center">
                                            <tbody>
                                                <tr>
                                                    <td align="middle">
                                                        <table border="0" cellspacing="0" cellpadding="0" width="790" align="center">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="150" align="left" style="padding-left: 19px;padding-top: 40px;"><span style="background-color: #790000;color: #fff;padding: 5px;">Client Consent Form</span></td>
                                                                    <td width="290" align="left" style="padding-left: 19px;padding-top: 40px;">
                                                                        <a target="_blank" style="background-color: #3f7900;color: #fff;padding: 5px;" href="<?php echo FindHostAdmin(); ?>/approve_concent.php?apt_id={apt_id}">Approve</a>
                                                                    </td>
                                                                    <td width="290" align="right" style="padding:1%;"><img border="0" src="http://nailspaexperience.com/images/nailspa-logo-main.png" width="80" height="80" alt="NailSpa Experience"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="line-height:17px;padding:8px 20px 8px 20px;font-family:Verdana,Geneva,sans-serif;color:#000;font-size:13px;" width="500" align="justify">Name : {Name_Detail}</td>
                                                </tr>

                                                <tr>
                                                    <td style="line-height:17px;padding:8px 20px 8px 20px;font-family:Verdana,Geneva,sans-serif;color:#000;font-size:13px;" width="500" align="justify">Address : {Cust_addr}</td>
                                                </tr>

                                                <?php /* <tr>
                                                  <td style="line-height:17px;padding:8px 20px 8px 20px;font-family:Verdana,Geneva,sans-serif;color:#000;font-size:13px;" width="500" align="justify">
                                                  The nature and method of the proposed permanent make up (cosmetic tattoo) procedure has been explained to me by Nailspa Experience, including the usual risks inherent in the procedure process, and the possibility of complications during or following its performance.
                                                  </td>
                                                  </tr>

                                                  <tr>
                                                  <td style="line-height:17px;padding:8px 20px 8px 20px;font-family:Verdana,Geneva,sans-serif;color:#000;font-size:13px;" width="500" align="justify">
                                                  I understand there may be a certain amount of discomfort or pain associated with the procedure and that other adverse side effects may include minor or temporary bleeding, bruising, redness, or other discoloration and swelling.
                                                  Fading or loss of pigment may occur. Secondary infection in the area of the procedure may occur, however if properly cared for, is rare.

                                                  </td>
                                                  </tr>

                                                  <tr>
                                                  <td style="line-height:17px;padding:8px 20px 8px 20px;font-family:Verdana,Geneva,sans-serif;color:#000;font-size:13px;" width="500" align="justify">
                                                  I acknowledge that the procedure will result in a semi-permanent change to my appearance and that no representations have been made to me as to the ability to later change or remove the result or as to the complete permanency thereof.
                                                  </td>
                                                  </tr>

                                                  <tr>
                                                  <td style="line-height:17px;padding:8px 20px 8px 20px;font-family:Verdana,Geneva,sans-serif;color:#000;font-size:13px;" width="500" align="justify">
                                                  I understand that Nailspa Experience uses quality products for this service, however, I have been explained that Nailspa Experience does not understand the import of the allergies/ health problems that I have which may result in adverse side effects and/ or complications by use of the service/ products. I have been advised by Nailspa Experience to consult my physician regarding treatment of any medical condition or the use of this service/ products.
                                                  </td>
                                                  </tr>

                                                  <tr>
                                                  <td style="line-height:17px;padding:8px 20px 8px 20px;font-family:Verdana,Geneva,sans-serif;color:#000;font-size:13px;" width="500" align="justify">
                                                  I understand that future laser treatments or other skin altering procedures, such as plastic surgery, implants and injections may alter and degrade my Permanent Make up. I further understand that such changes are not the responsibility of Nailspa Experience.
                                                  </td>
                                                  </tr>

                                                  <tr>
                                                  <td style="line-height:17px;padding:8px 20px 8px 20px;font-family:Verdana,Geneva,sans-serif;color:#000;font-size:13px;" width="500" align="justify">
                                                  I hereby release any and all persons representing Nailspa Experience from all
                                                  claims, demands, damages, actions and cause of action arising out of the performance of the service.
                                                  </td>
                                                  </tr>
                                                  <tr>
                                                  <td style="line-height:17px;padding:8px 20px 8px 20px;font-family:Verdana,Geneva,sans-serif;color:#000;font-size:13px;" width="500" align="justify">
                                                  The nature and method of the proposed eyelash extensions procedure has been explained to me by Nailspa Experience, including the usual risks inherent in the procedure process, and the possibility of complications during or following its performance.
                                                  </td>
                                                  </tr>

                                                  <tr>
                                                  <td style="line-height:17px;padding:8px 20px 8px 20px;font-family:Verdana,Geneva,sans-serif;color:#000;font-size:13px;" width="500" align="justify">
                                                  I understand to keep my eyes closed throughout the process as tearing can cause the lashes to bond together rather than one on one. I understand the aftercare instructions. I further understand that because of the natural lash cycle and wear and tear, I will need to maintain my extensions with touch-up appointments usually recommended about every 2 to 3 weeks to keep them full.
                                                  </td>
                                                  </tr>

                                                  <tr>
                                                  <td style="line-height:17px;padding:8px 20px 8px 20px;font-family:Verdana,Geneva,sans-serif;color:#000;font-size:13px;" width="500" align="justify">
                                                  I understand that Nailspa Experience uses quality products for this service, however, I have been explained that Nailspa Experience does not understand the import of the allergies/ health problems that I have which may result in adverse side effects and/ or complications by use of the service/ products. I have been advised by Nailspa Experience to consult my physician regarding treatment of any medical condition or the use of
                                                  this service/ products.
                                                  </td>
                                                  </tr>

                                                  <tr>
                                                  <td style="line-height:17px;padding:8px 20px 8px 20px;font-family:Verdana,Geneva,sans-serif;color:#000;font-size:13px;" width="500" align="justify">
                                                  I hereby release any and all persons representing Nailspa Experience from all claims, demands, damages, actions and cause of action arising out of the performance of the service.
                                                  </td>
                                                  </tr> */ ?>

                                                <tr>
                                                    <td style="line-height:17px;padding:8px 20px 8px 20px;font-family:Verdana,Geneva,sans-serif;color:#000;font-size:13px;" width="500" align="justify">
                                                        <p>The nature and method of the proposed permanent make up (cosmetic tattoo) procedure has been explained to me by Nailspa Experience, including the usual risks inherent in the procedure process, and the possibility of complications during or following its performance.</p>

                                                        <p>I understand there may be a certain amount of discomfort or pain associated with the procedure and that other adverse side effects may include minor or temporary bleeding, bruising, redness, or other discoloration and swelling. Fading or loss of pigment may occur. Secondary infection in the area of the procedure may occur, however if properly cared for, is rare.</p>

                                                        <p>I acknowledge that the procedure will result in a semi-permanent change to my appearance and that no representations have been made to me as to the ability to later change or remove the result or as to the complete permanency thereof.</p>

                                                        <p>I understand that Nailspa Experience uses quality products for this service, however, I have been explained that Nailspa Experience does not understand the import of the allergies/ health problems that I have which may result in adverse side effects and/ or complications by use of the service/ products. I have been advised by Nailspa Experience to consult my physician regarding treatment of any medical condition or the use of this service/ products.</p>

                                                        <p>I understand that future laser treatments or other skin altering procedures, such as plastic surgery, implants and injections may alter and degrade my Permanent Make up. I further understand that such changes are not the responsibility of Nailspa Experience.</p>

                                                        <p>I hereby release any and all persons representing Nailspa Experience from all claims, demands, damages, actions and cause of action arising out of the performance of the service.</p>

                                                        <p>The nature and method of the proposed eyelash extensions procedure has been explained to me by Nailspa Experience, including the usual risks inherent in the procedure process, and the possibility of complications during or following its performance.</p>

                                                        <p>I understand to keep my eyes closed throughout the process as tearing can cause the lashes to bond together rather than one on one. I understand the aftercare instructions. I further understand that because of the natural lash cycle and wear and tear, I will need to maintain my extensions with touch-up appointments usually recommended about every 2 to 3 weeks to keep them full.</p>

                                                        <p>I understand that Nailspa Experience uses quality products for this service, however, I have been explained that Nailspa Experience does not understand the import of the allergies/ health problems that I have which may result in adverse side effects and/ or complications by use of the service/ products. I have been advised by Nailspa Experience to consult my physician regarding treatment of any medical condition or the use of this service/ products.</p>

                                                        <p>I hereby release any and all persons representing Nailspa Experience from all claims, demands, damages, actions and cause of action arising out of the performance of the service.</p>

                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</head>
</html>