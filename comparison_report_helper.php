<?php

function Report_sale($strtoandfrom = '', $Store = '', $date_filter = array()) {
    $servicesale = array();
    $DB = Connect();
    if ($strtoandfrom != '' || count($date_filter) > 0) {

        $arraytofrom = explode("/", $strtoandfrom);

        $year = $arraytofrom[0];
        $append = "";
        if ($Store != '' && $Store != 0) {
            $append = " AND StoreID ='" . $Store . "'";
        }
        if (isset($date_filter['source']) && $date_filter['source'] != 0) {
            $append = " AND source ='" . $date_filter['source'] . "'";
        }
        if (isset($date_filter['from_date']) && $date_filter['from_date'] != '' && isset($date_filter['to_date']) && $date_filter['to_date'] != '') {
            $sqlservice = "SELECT AppointmentID,StoreID,AppointmentDate
		FROM `tblAppointments` 
		WHERE `Status` = 2 AND `IsDeleted` != 1 AND `FreeService` != 1 " . $append . "
		AND AppointmentId IN
		(
			SELECT AppointmentId  
			FROM `tblInvoiceDetails` 
			WHERE DATE_FORMAT(AppointmentDate,'%Y-%m-%d') >= '" . $date_filter['from_date'] . "' AND  DATE_FORMAT(AppointmentDate,'%Y-%m-%d') <='" . $date_filter['to_date'] . "' 
                )";
        } else {
            $sqlservice = "SELECT AppointmentID,StoreID,AppointmentDate
		FROM `tblAppointments` 
		WHERE `Status` = 2 AND `IsDeleted` != 1 AND `FreeService` != 1 " . $append . "
		AND AppointmentId IN
		(
			SELECT AppointmentId  
			FROM `tblInvoiceDetails` 
			WHERE DATE_FORMAT(AppointmentDate,'%Y') >= '" . $year . "' AND  DATE_FORMAT(AppointmentDate,'%Y') <='" . $year . "' 
                )";
        }
        $RSservice = $DB->query($sqlservice);
        if ($RSservice->num_rows > 0) {
            while ($rowservice = $RSservice->fetch_assoc()) {
                $queryresult[$rowservice['StoreID']][] = $rowservice['AppointmentID'];
                $apt_date[$rowservice['AppointmentID']] = $rowservice['AppointmentDate'];
                $appointment_store_data[$rowservice['AppointmentID']] = $rowservice['StoreID'];
            }
        }

        $servicesale = array();
        $discountgiven = array();
        $customercount = array();
        $totalsale = array();

        if (isset($queryresult) && is_array($queryresult) && count($queryresult)) {
            foreach ($queryresult as $key => $value) {

                $gAmount_store = 0;
                $memamt = 0;
                $offamt = 0;
                $TotalRoundTotal = 0;


                /*
                 * get service sale
                 */
                $sqlservice = "SELECT sum(qty * ServiceAmount) as servicesale FROM `tblAppointmentsDetailsInvoice` WHERE AppointmentID IN(" . implode(',', $value) . ")";
                $sqlservice = "SELECT inv.AppointmentId, inv.ServiceName, inv.Qty, inv.ServiceAmt, inv.DisAmt, inv.OfferAmt, inv.SubTotal, inv.RoundTotal FROM `tblInvoiceDetails` inv WHERE AppointmentID IN(" . implode(',', $value) . ")";

                $RSservice = $DB->query($sqlservice);
                if ($RSservice->num_rows > 0) {
                    while ($rowservice = $RSservice->fetch_assoc()) {

                        $offer_amount = 0;
                        $amount_exculde_dis = 0;
                        if (!empty($rowservice['ServiceName'])) {
                            $ServiceName = explode(',', $rowservice['ServiceName']);
                            $Qty = explode(',', $rowservice['Qty']);
                            $ServiceAmt = explode(',', $rowservice['ServiceAmt']);
                            $DisAmt = explode(',', $rowservice['DisAmt']);
                            $count = count($ServiceName);
                            for ($i = 0; $i < $count; $i++) {

                                $offer_amount = str_replace(array('-', ' '), '', $rowservice['OfferAmt']);

                                if (isset($DisAmt[$i])) {
                                    $amount_exculde_dis += ($ServiceAmt[$i] - $DisAmt[$i]);
                                } else {
                                    $amount_exculde_dis += $ServiceAmt[$i];
                                }
                                //echo '<br>amount_exculde_dis=' . $amount_exculde_dis;
                            }


                            if ($offer_amount > 0) {
                                $amount_exculde_dis = $amount_exculde_dis - $offer_amount;
                            }

                            if ($amount_exculde_dis < 0) {
                                $amount_exculde_dis = 0;
                            }


                            $date_appointment = isset($apt_date[$rowservice['AppointmentId']]) ? date('Y-m', strtotime($apt_date[$rowservice['AppointmentId']])) : 0;

                            if ($date_appointment != 0) {
                                if (isset($date_filter) && is_array($date_filter) && count($date_filter) > 0) {
                                    $apt_store_id = isset($appointment_store_data[$rowservice['AppointmentId']]) ? $appointment_store_data[$rowservice['AppointmentId']] : 0;
                                    if (isset($servicesale[$apt_store_id])) {
                                        $servicesale[$apt_store_id] += round($amount_exculde_dis, 2);
                                    } else {
                                        $servicesale[$apt_store_id] = round($amount_exculde_dis, 2);
                                    }
                                } else {
                                    if (isset($servicesale[$date_appointment])) {
                                        $servicesale[$date_appointment] += round($amount_exculde_dis, 2);
                                    } else {
                                        $servicesale[$date_appointment] = round($amount_exculde_dis, 2);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    $DB->close();

    return $servicesale;
}

function Report_category_sale($strtoandfrom = '', $Store = '') {
    $final_array = array();
    $conn = Connect();
    if ($strtoandfrom != '') {
        $arraytofrom = explode("/", $strtoandfrom);
        $year = $arraytofrom[0];
        $append = "";
        if ($Store != '' && $Store != 0) {
            $append = " AND apt.StoreID ='" . $Store . "'";
        } else {
            $append = " AND apt.StoreID != '0'";
        }
        $query = "SELECT DISTINCT apt.AppointmentId,apt.AppointmentDate,apt.StoreID ,inv.ServiceName,inv.Qty,inv.ServiceAmt,inv.DisAmt,inv.OfferAmt,inv.SubTotal,inv.RoundTotal FROM `tblAppointments` apt LEFT OUTER JOIN `tblInvoiceDetails` inv on apt.`AppointmentId` = inv.`AppointmentId` 
                                                          WHERE DATE_FORMAT(AppointmentDate,'%Y') >= '" . $year . "' AND  DATE_FORMAT(AppointmentDate,'%Y') <='" . $year . "'
                                                             " . $append . " AND apt.IsDeleted != '1'  AND apt.FreeService !=  '1'  AND apt.Status = '2'
                                                          AND inv.ServiceName != ''";

        $ans = $conn->query($query);
        if ($ans->num_rows > 0) {
            while ($result = $ans->fetch_assoc()) {
                $query_result[] = $result;
            }
        }
        if (isset($query_result) && is_array($query_result) && count($query_result)) {
            foreach ($query_result as $key => $value) {
                if (!empty($value['ServiceName'])) {


                    $ServiceName = explode(',', $value['ServiceName']);
                    $Qty = explode(',', $value['Qty']);
                    $ServiceAmt = explode(',', $value['ServiceAmt']);
                    $DisAmt = explode(',', $value['DisAmt']);
                    $count = count($ServiceName);

                    $date_appointment = date('Y-m', strtotime($value['AppointmentDate']));

                    for ($i = 0; $i < $count; $i++) {
                        $allservice[] = $ServiceName[$i];
                        $offer_deduct = 0;
                        $offer_amount = str_replace(array('-', ' '), '', $value['OfferAmt']);
                        if ($offer_amount > 0) {
                            $offer_deduct = $offer_amount / $count;
                        }
                        if (isset($DisAmt[$i])) {
                            $amount_exculde_dis = $ServiceAmt[$i] - $DisAmt[$i];
                        } else {
                            $amount_exculde_dis = $ServiceAmt[$i];
                        }

                        if ($amount_exculde_dis < $offer_deduct) {
                            $amount_exculde_dis = 0;
                        } else {
                            $amount_exculde_dis = $amount_exculde_dis - $offer_deduct;
                        }

                        if (isset($allserviceamt[$date_appointment][$ServiceName[$i]])) {
                            $allserviceamt[$date_appointment][$ServiceName[$i]] += round($amount_exculde_dis, 2);
                        } else {
                            $allserviceamt[$date_appointment][$ServiceName[$i]] = round($amount_exculde_dis, 2);
                        }
                    }
                }
            }


            $query = "SELECT CategoryId,ServiceID,StoredID,ProductId FROM `tblProductServiceCategory` WHERE `ServiceID` IN (" . implode(',', array_unique($allservice)) . ") ";
            $ans = $conn->query($query) or die($conn->error);
            $query_result = array();
            if ($ans->num_rows > 0) {
                while ($result = $ans->fetch_assoc()) {
                    $query_result[] = $result;
                }

                if (isset($query_result) && is_array($query_result) && count($query_result)) {
                    foreach ($query_result as $key => $value) {
                        $servicesgrp[$value['ServiceID']] = $value['CategoryId'];
                    }
                }


                if (isset($allserviceamt) && is_array($allserviceamt) && count($allserviceamt) > 0) {
                    foreach ($allserviceamt as $servicedatek => $servicedatev) {
                        foreach ($servicedatev as $serviceid => $categoryamount) {
                            /*
                             * Get Service Category Id
                             */
                            $category_id = isset($servicesgrp[$serviceid]) ? $servicesgrp[$serviceid] : 0;
                            if (isset($final_array['result'][$servicedatek][$category_id])) {
                                $final_array['result'][$servicedatek][$category_id] += $categoryamount;
                            } else {
                                $final_array['result'][$servicedatek][$category_id] = $categoryamount;
                            }
                        }
                    }
                }
            }
        }
    }


    $conn->close();
    return $final_array;
}

function Membership_sale($strtoandfrom = '', $Store = '') {
    $membership_sale = array();
    $DB = Connect();
    if ($strtoandfrom != '') {
        $arraytofrom = explode("/", $strtoandfrom);
        $year = $arraytofrom[0];
        $append = "";
        if ($Store != '' && $Store != 0) {
            $selp = select("*", "tblStores", "StoreID = '" . $Store . "'");
        } else {
            $selp = select("*", "tblStores", "StoreID > '0'");
        }
        $sqlTempfrom1 = " and DATE_FORMAT(tblCustomerMemberShip.ExpireDate,'%Y')>= '" . $year . "'";
        $sqlTempfrom2 = " and DATE_FORMAT(tblAppointments.AppointmentDate,'%Y')>='" . $year . "'";
        $sqlTempfrom3 = " and DATE_FORMAT(tblCustomerMemberShip.StartDay,'%Y')>='" . $year . "'";

        $sqlTempto1 = " and DATE_FORMAT(tblCustomerMemberShip.ExpireDate,'%Y')<= '" . $year . "'";
        $sqlTempto2 = " and DATE_FORMAT(tblAppointments.AppointmentDate,'%Y')<='" . $year . "'";
        $sqlTempto3 = " and DATE_FORMAT(tblCustomerMemberShip.StartDay,'%Y')<='" . $year . "'";

        foreach ($selp as $vat) {
            $appointment_ids = array();
            $reappointment_ids = array();
            $in_apt_ids = '';
            $rein_apt_ids = '';

            /*
             * Get New Membership Detail
             */
            $sqlservice = "SELECT DISTINCT tblCustomers.`CustomerID`,tblAppointments.AppointmentID,tblCustomerMemberShip.StartDay FROM  tblCustomers "
                    . " LEFT JOIN tblAppointments ON tblCustomers.`CustomerID` = tblAppointments.CustomerID "
                    . " LEFT JOIN tblCustomerMemberShip ON tblCustomerMemberShip.`CustomerID` = tblCustomers.CustomerID "
                    . " WHERE  tblAppointments.StoreID =  '" . $vat['StoreID'] . "' AND tblAppointments.memberid !=  '0' AND tblCustomerMemberShip.Status =  '1' "
                    . " AND tblCustomerMemberShip.RenewStatus =  '0' AND tblAppointments.Status=2"
                    . " $sqlTempfrom3 $sqlTempto3  AND tblCustomerMemberShip.StartDay = tblAppointments.AppointmentDate";


            $RSservice = $DB->query($sqlservice);
            if ($RSservice->num_rows > 0) {
                while ($rowservice = $RSservice->fetch_assoc()) {
                    $appointment_ids[$rowservice['AppointmentID']] = $rowservice['AppointmentID'];
                    $new_appointment_date[$rowservice['AppointmentID']] = $rowservice['StartDay'];
                }
            }

            if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                $in_apt_ids = implode(",", $appointment_ids);
            }


            /*
             * Get Renewal Appointment Detail
             */
            /*  $setrenewmemcount = "SELECT DISTINCT tblCustomerMemberShip.RenewCount,tblCustomerMemberShip.CustomerID,tblAppointments.AppointmentID  FROM tblCustomerMemberShip "
              . " LEFT JOIN tblAppointments ON tblCustomerMemberShip.CustomerID = tblAppointments.CustomerID "
              . " and tblCustomerMemberShip.MembershipID=tblAppointments.memberid "
              . " WHERE tblCustomerMemberShip.Status =  '1' AND tblAppointments.StoreID =  '" . $vat['StoreID'] . "' "
              . " AND tblAppointments.memberid !=  '0'  AND tblAppointments.Status=2 and tblCustomerMemberShip.RenewStatus='1' "
              . " $sqlTempfrom3 $sqlTempto3 $sqlTempfrom2 $sqlTempto2";


              $RSsetrenewmemcount = $DB->query($setrenewmemcount);

              if ($RSsetrenewmemcount->num_rows > 0) {
              $ren_cust_id = 0;
              while ($rowRSsetrenewmemcount = $RSsetrenewmemcount->fetch_assoc()) {
              $toatlseramt = "";
              $reappointment_ids[$rowRSsetrenewmemcount['AppointmentID']] = $rowRSsetrenewmemcount['AppointmentID'];
              $new_appointment_date[$rowRSsetrenewmemcount['AppointmentID']] = $rowRSsetrenewmemcount['StartDay'];
              }
              }
              if (isset($reappointment_ids) && is_array($reappointment_ids) && count($reappointment_ids) > 0) {
              $rein_apt_ids = implode(",", $reappointment_ids);
              }


              if (isset($in_apt_ids) && $in_apt_ids != '') {
              $append = " AND AppointmentID IN (" . $in_apt_ids . ")";
              $appoint_date = "SELECT AppointmentDate,AppointmentID FROM tblAppointments WHERE Status=2 " . $append;
              $appoint_dateQ = $DB->query($appoint_date);
              if ($appoint_dateQ->num_rows > 0) {
              while ($AppointData = $appoint_dateQ->fetch_assoc()) {
              $appointment_date[$AppointData['AppointmentID']] = $AppointData['AppointmentDate'];
              }
              }
              }

              if (isset($rein_apt_ids) && $rein_apt_ids != '') {
              $append = " AND AppointmentID IN (" . $rein_apt_ids . ")";
              $appoint_date = "SELECT AppointmentDate,AppointmentID FROM tblAppointments WHERE Status=2 " . $append;
              $appoint_dateQ = $DB->query($appoint_date);
              if ($appoint_dateQ->num_rows > 0) {
              while ($AppointData = $appoint_dateQ->fetch_assoc()) {
              $appointment_date[$AppointData['AppointmentID']] = $AppointData['AppointmentDate'];
              }
              }
              } */


            if ($in_apt_ids != '') {

                $toatlseramt = "";

                $setmemamount123 = "SELECT tblInvoiceDetails.Membership_Amount,AppointmentId FROM tblInvoiceDetails "
                        . " WHERE AppointmentId IN(" . $in_apt_ids . ")";

                $setmemamount = $DB->query($setmemamount123);

                if ($setmemamount->num_rows > 0) {

                    while ($rowsetmemamount = $setmemamount->fetch_assoc()) {
                        $Membership_Amount = $rowsetmemamount["Membership_Amount"];
                        $memamtfirst = explode(",", $Membership_Amount);
                        $memamtfirst = str_replace("+", "", $Membership_Amount);
                        $memamtfirst2 = str_replace(".00", "", $memamtfirst);
                        $memamtfirst3 = str_replace("+", "", $memamtfirst2);
                        $memamtfirst4 = str_replace(",", "", $memamtfirst3);
                        $memamtfirst5 = str_replace(".00", "", $memamtfirst4);
                        if ($memamtfirst5 == '') {
                            $memamtfirst5 = "0.00";
                        }


                        /*
                         * Get Appointment Date
                         */
                        $apt_date = isset($new_appointment_date[$rowsetmemamount['AppointmentId']]) ? date('Y-m', strtotime($new_appointment_date[$rowsetmemamount['AppointmentId']])) : '';
                        if (isset($membership_sale[$apt_date]['new_cust'])) {
                            $membership_sale[$apt_date]['new_cust'] += 1;
                        } else {
                            $membership_sale[$apt_date]['new_cust'] = 1;
                        }

                        if (isset($membership_sale[$apt_date]['new_cust_amount'])) {
                            $membership_sale[$apt_date]['new_cust_amount'] += $memamtfirst5;
                        } else {
                            $membership_sale[$apt_date]['new_cust_amount'] = $memamtfirst5;
                        }
                    }
                }
            }


            /* if ($rein_apt_ids != '') {
              $toatlseramt = "";
              $setmemamount123 = "SELECT tblInvoiceDetails.Membership_Amount,AppointmentId FROM tblInvoiceDetails "
              . " WHERE AppointmentId IN(" . $rein_apt_ids . ")";
              $setmemamount = $DB->query($setmemamount123);
              if ($setmemamount->num_rows > 0) {
              while ($rowsetmemamount = $setmemamount->fetch_assoc()) {
              $Membership_Amount = $rowsetmemamount["Membership_Amount"];
              $memamtfirst = explode(",", $Membership_Amount);

              $memamtfirst = str_replace("+", "", $Membership_Amount);
              $memamtfirst2 = str_replace(".00", "", $memamtfirst);
              $memamtfirst3 = str_replace("+", "", $memamtfirst2);
              $memamtfirst4 = str_replace(",", "", $memamtfirst3);
              $memamtfirst5 = str_replace(".00", "", $memamtfirst4);

              // $memamtfirst=str_replace(",", "", $Membership_Amount);
              if ($memamtfirst5 == '') {
              $memamtfirst5 = "0.00";
              }


              /*
             * Get Appointment Date
             */
            /* $apt_date = isset($appointment_date[$rowsetmemamount['AppointmentId']]) ? date('Y-m', strtotime($appointment_date[$rowsetmemamount['AppointmentId']])) : '';
              if (isset($membership_sale[$apt_date]['renew_cust'])) {
              $membership_sale[$apt_date]['renew_cust'] += 1;
              } else {
              $membership_sale[$apt_date]['renew_cust'] = 1;
              }

              if (isset($membership_sale[$apt_date]['renew_cust_amount'])) {
              $membership_sale[$apt_date]['renew_cust_amount'] += $memamtfirst5;
              } else {
              $membership_sale[$apt_date]['renew_cust_amount'] = $memamtfirst5;
              }
              }
              }
              } */
        }
    }
    $DB->close();
    return $membership_sale;
}

function Employee_sale($strtoandfrom = '', $Store = '') {
    $employee_sale = array();
    $DB = Connect();
    if ($strtoandfrom != '') {

        $arraytofrom = explode("/", $strtoandfrom);
        $year = $arraytofrom[0];
        $append = "";
        if ($Store != '' && $Store != 0) {
            $append = " AND StoreID ='" . $Store . "'";
        }
        /*
         * get employee sales data
         */
        $in_emp_ids = isset($_GET['emp_id']) ? $_GET['emp_id'] : '0';
        if ($in_emp_ids != 0) {
            /* $emp_sql = "select EID, EmployeeName, EmployeeEmailID, EmpPercentage, EmployeeMobileNo,EmployeeCode,StoreID from tblEmployees where Status='0'"
              . " AND EID IN(" . $e_id . ")";
              $emp_sql_exe = $DB->query($emp_sql);
              if ($emp_sql_exe->num_rows > 0) {
              while ($row = $emp_sql_exe->fetch_assoc()) {
              $all_emp_data[$row["EID"]] = $row;
              $all_emp_code[$row["EID"]] = $row["EmployeeCode"];
              $all_emp_ids[$row["EmployeeCode"]] = $row["EID"];
              }
              } */

            $sqlTempfrom1 = " and DATE_FORMAT(tblAppointments.AppointmentDate,'%Y')='" . $year . "'";
            $sqlTempto1 = " and DATE_FORMAT(tblAppointments.AppointmentDate,'%Y')='" . $year . "'";

            $appointment_q = "SELECT *,AppointmentID as AppointmentId FROM tblAppointments WHERE AppointmentCheckInTime!='00:00:00' AND status=2 "
                    . $append . " AND AppointmentDate!='' $sqlTempfrom1 $sqlTempto1 ";

            $appointment_q_exe = $DB->query($appointment_q);
            if ($appointment_q_exe->num_rows > 0) {
                while ($appointment_result = $appointment_q_exe->fetch_assoc()) {
                    $invoice_apt_ids[] = $appointment_result['AppointmentId'];
                    $appointment_date[$appointment_result['AppointmentId']] = $appointment_result['AppointmentDate'];
                    $invice_detail_data[$appointment_result['AppointmentId']] = $appointment_result;
                }


                if (isset($invoice_apt_ids) && is_array($invoice_apt_ids) && count($invoice_apt_ids) > 0) {
                    $inv_apt_ids = implode(",", $invoice_apt_ids);
                    if ($inv_apt_ids != '') {
                        $apt_emp_ass_q = "SELECT tblAppointmentAssignEmployee.MECID,tblAppointmentAssignEmployee.QtyParam,tblAppointmentAssignEmployee.AppointmentID, tblAppointmentAssignEmployee.Qty, tblAppointmentAssignEmployee.ServiceID, tblAppointmentAssignEmployee.Commission, tblAppointmentAssignEmployee.QtyParam"
                                . " FROM tblAppointmentAssignEmployee "
                                . " WHERE tblAppointmentAssignEmployee.MECID IN(" . $in_emp_ids . ") AND tblAppointmentAssignEmployee.AppointmentID IN (" . $inv_apt_ids . ")";
                        $apt_emp_ass_q_exe = $DB->query($apt_emp_ass_q);
                        if ($apt_emp_ass_q_exe->num_rows > 0) {
                            while ($emp_assign_result = $apt_emp_ass_q_exe->fetch_assoc()) {
                                $invoice_apt_data[] = $emp_assign_result;
                            }
                        }
                    }
                }


                if (isset($invoice_apt_data) && is_array($invoice_apt_data) && count($invoice_apt_data) > 0) {
                    foreach ($invoice_apt_data as $reskey => $resvalue) {
                        $all_sales_data[$resvalue['MECID']][] = array(
                            'EID' => $resvalue['MECID'],
                            'CustomerID' => isset($invice_detail_data[$resvalue['AppointmentID']]) ? $invice_detail_data[$resvalue['AppointmentID']]['CustomerID'] : 0,
                            'AppointmentID' => $resvalue['AppointmentID'],
                            'Qty' => $resvalue['Qty'],
                            'ServiceID' => $resvalue['ServiceID'],
                            'Commission' => $resvalue['Commission'],
                            'QtyParam' => $resvalue['QtyParam'],
                            'OfferDiscountDateTime' => isset($invice_detail_data[$resvalue['AppointmentID']]) ? $invice_detail_data[$resvalue['AppointmentID']]['OfferDiscountDateTime'] : 0,
                            'AppointmentDate' => isset($invice_detail_data[$resvalue['AppointmentID']]) ? $invice_detail_data[$resvalue['AppointmentID']]['AppointmentDate'] : 0,
                        );

                        $appointment_ids[$resvalue['AppointmentID']] = $resvalue['AppointmentID'];
                    }
                }
            }



            /*
             * get service amt
             */

            if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                $apt_in_ids = implode(",", $appointment_ids);
                $aptdate_q = "SELECT ServiceAmount,AppointmentID,ServiceID FROM tblAppointmentsDetailsInvoice WHERE AppointmentID IN(" . $apt_in_ids . ")";
                $aptdate_exe = $DB->query($aptdate_q);
                while ($aptdatedetails = $aptdate_exe->fetch_assoc()) {
                    $all_apt_service_amt[] = $aptdatedetails;
                }
                if (isset($all_apt_service_amt) && is_array($all_apt_service_amt) && count($all_apt_service_amt) > 0) {
                    foreach ($all_apt_service_amt as $servdkey => $servvalue) {
                        $all_ser_amt[$servvalue['AppointmentID']][$servvalue['ServiceID']] = $servvalue['ServiceAmount'];
                    }
                }
            }


            /*
             * get discount 
             */
            if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                $apt_in_ids = implode(",", $appointment_ids);
                $aptdate_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ")";
                $aptdate_exe = $DB->query($aptdate_q);
                while ($disdetails = $aptdate_exe->fetch_assoc()) {
                    $all_apt_dis_amt[] = $disdetails;
                }
                if (isset($all_apt_dis_amt) && is_array($all_apt_dis_amt) && count($all_apt_dis_amt) > 0) {
                    foreach ($all_apt_dis_amt as $diskey => $disvalue) {
                        $all_dis_amt[$disvalue['AppointmentID']][$disvalue['ServiceID']]['MemberShipAmount'] = $disvalue['MemberShipAmount'];
                        $all_dis_amt[$disvalue['AppointmentID']][$disvalue['ServiceID']]['OfferAmount'] = $disvalue['OfferAmount'];
                    }
                }
                if (isset($all_apt_dis_amt) && is_array($all_apt_dis_amt) && count($all_apt_dis_amt) > 0) {
                    foreach ($all_apt_dis_amt as $diskey => $disvalue) {
                        /*
                         * Get Appointment service count
                         */
                        $extra_offer_amount = 0;

                        if (isset($all_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']]) && $all_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']] > 0) {
                            $extra_offer_amount = $all_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']];
                        }
                        //echo '<br>extra_offer_amount=' . $extra_offer_amount;
                        //echo '<br>member_amount=' . $disvalue['MemberShipAmount'];
                        $all_dis_amt[$disvalue['AppointmentID']][$disvalue['ServiceID']]['MemberShipAmount'] = $disvalue['MemberShipAmount'] + round($extra_offer_amount, 2);
                        $all_apt_mem_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']] = $disvalue['MemberShipAmount'];
                        //$all_dis_amt[$disvalue['AppointmentID']][$disvalue['ServiceID']]['OfferAmount'] = $disvalue['OfferAmount'];
                    }
                }
            }

            $strEID = $in_emp_ids;

            if (isset($all_sales_data[$strEID]) && is_array($all_sales_data[$strEID]) && count($all_sales_data[$strEID]) > 0) {

                $strSID = "";
                $qty = "";
                $strSAmount = "";
                $strAmount = "";
                $strCommission = "";
                $FinalDAmount = '';
                $FinalDiscount = '';
                $UltimateSale = '';
                $AfterDivideSale = '';


                foreach ($all_sales_data[$strEID] as $key => $rowdetails) {

                    $strAID = $rowdetails["AppointmentID"];
                    $strSID = $rowdetails["ServiceID"];
                    $qty = $rowdetails["Qty"];
                    $strAmount = isset($all_ser_amt[$strAID][$strSID]) ? $all_ser_amt[$strAID][$strSID] : 0;
                    $strSAmount = $strAmount;
                    $strCommission = $rowdetails["Commission"];

                    $unique_apt[$rowdetails['AppointmentID']]['CustomerID'] = $rowdetails['CustomerID'];
                    $unique_apt[$rowdetails['AppointmentID']]['AppointmentDate'] = $rowdetails['AppointmentDate'];

                    if ($strCommission == "1") {
                        $AfterDivideSale = $strSAmount;
                        $strCommissionType = '<span class="bs-label label-success">Alone</span>';
                    } elseif ($strCommission == "2") {
                        $AfterDivideSale = ($strSAmount / 2);
                        $strCommissionType = '<span class="bs-label label-blue-alt">Split</span>';
                    }
                    $rowdiscount = isset($all_dis_amt[$strAID][$strSID]) ? $all_dis_amt[$strAID][$strSID] : array();


                    if (isset($rowdiscount) && is_array($rowdiscount) && count($rowdiscount) > 0) {
                        $strOfferAmount = $rowdiscount["OfferAmount"];
                        $strDiscountAmount = $rowdiscount["MemberShipAmount"];

                        if ($strOfferAmount == "0") {
                            $FinalDAmount = $strDiscountAmount;
                        } elseif ($strDiscountAmount == "0") {
                            $FinalDAmount = $strOfferAmount;
                        }
                    } else {
                        $FinalDAmount = "0";
                    }

                    $FinalDiscount = $FinalDAmount / $qty;
                    $UltimateSale = $AfterDivideSale - $FinalDiscount;

                    $apt_date = isset($appointment_date[$strAID]) ? date('Y-m', strtotime($appointment_date[$strAID])) : '';
                    if (isset($employee_sale[$apt_date])) {
                        $employee_sale[$apt_date] += $UltimateSale;
                    } else {
                        $employee_sale[$apt_date] = $UltimateSale;
                    }
                }
            }
        }
    }
    $DB->close();

    return $employee_sale;
}

function Employee_sale_com($strtoandfrom = '', $Store = '', $emp_ids = '') {
    $employee_sale = array();
    $DB = Connect();
    if ($strtoandfrom != '') {

        $arraytofrom = explode("/", $strtoandfrom);
        $date1 = $arraytofrom[0];
        $date2 = $arraytofrom[1];
        $append = "";
        $append_emp = "";
        if ($Store != '' && $Store != 0) {
            $append = " AND StoreID ='" . $Store . "'";
        }
        /*
         * get employee sales data
         */
        $in_emp_ids = isset($emp_ids) ? $emp_ids : '0';
        if ($in_emp_ids != 0) {
            $append_emp = " AND tblAppointmentAssignEmployee.MECID IN(" . $in_emp_ids . ") ";
        }
        $sqlTempfrom1 = " and DATE_FORMAT(tblAppointments.AppointmentDate,'%Y-%m-%d')>='" . $date1 . "'";
        $sqlTempto1 = " and DATE_FORMAT(tblAppointments.AppointmentDate,'%Y-%m-%d')<='" . $date2 . "'";

        $appointment_q = "SELECT *,AppointmentID as AppointmentId FROM tblAppointments WHERE AppointmentCheckInTime!='00:00:00' AND status=2 "
                . $append . " AND IsDeleted = 0 AND AppointmentDate!='' $sqlTempfrom1 $sqlTempto1 ";

        $appointment_q_exe = $DB->query($appointment_q);
        if ($appointment_q_exe->num_rows > 0) {
            while ($appointment_result = $appointment_q_exe->fetch_assoc()) {
                $invoice_apt_ids[] = $appointment_result['AppointmentId'];
                $appointment_date[$appointment_result['AppointmentId']] = $appointment_result['AppointmentDate'];
                $invice_detail_data[$appointment_result['AppointmentId']] = $appointment_result;
            }


            if (isset($invoice_apt_ids) && is_array($invoice_apt_ids) && count($invoice_apt_ids) > 0) {
                $inv_apt_ids = implode(",", $invoice_apt_ids);
                if ($inv_apt_ids != '') {
                    $apt_emp_ass_q = "SELECT tblAppointmentAssignEmployee.MECID,tblAppointmentAssignEmployee.QtyParam,tblAppointmentAssignEmployee.AppointmentID, tblAppointmentAssignEmployee.Qty, tblAppointmentAssignEmployee.ServiceID, tblAppointmentAssignEmployee.Commission, tblAppointmentAssignEmployee.QtyParam"
                            . " FROM tblAppointmentAssignEmployee "
                            . " WHERE tblAppointmentAssignEmployee.AppointmentID IN (" . $inv_apt_ids . ")" . $append_emp;
                    $apt_emp_ass_q_exe = $DB->query($apt_emp_ass_q);
                    if ($apt_emp_ass_q_exe->num_rows > 0) {
                        while ($emp_assign_result = $apt_emp_ass_q_exe->fetch_assoc()) {
                            $invoice_apt_data[] = $emp_assign_result;
                        }
                    }
                }
            }


            if (isset($invoice_apt_data) && is_array($invoice_apt_data) && count($invoice_apt_data) > 0) {
                foreach ($invoice_apt_data as $reskey => $resvalue) {
                    $all_sales_data[$resvalue['MECID']][] = array(
                        'EID' => $resvalue['MECID'],
                        'CustomerID' => isset($invice_detail_data[$resvalue['AppointmentID']]) ? $invice_detail_data[$resvalue['AppointmentID']]['CustomerID'] : 0,
                        'AppointmentID' => $resvalue['AppointmentID'],
                        'Qty' => $resvalue['Qty'],
                        'ServiceID' => $resvalue['ServiceID'],
                        'Commission' => $resvalue['Commission'],
                        'QtyParam' => $resvalue['QtyParam'],
                        'OfferDiscountDateTime' => isset($invice_detail_data[$resvalue['AppointmentID']]) ? $invice_detail_data[$resvalue['AppointmentID']]['OfferDiscountDateTime'] : 0,
                        'AppointmentDate' => isset($invice_detail_data[$resvalue['AppointmentID']]) ? $invice_detail_data[$resvalue['AppointmentID']]['AppointmentDate'] : 0,
                    );

                    $appointment_ids[$resvalue['AppointmentID']] = $resvalue['AppointmentID'];
                }
            }
        }

        /*
         * get service amt
         */

        if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
            $apt_in_ids = implode(",", $appointment_ids);
            $aptdate_q = "SELECT ServiceAmount,AppointmentID,ServiceID FROM tblAppointmentsDetailsInvoice WHERE AppointmentID IN(" . $apt_in_ids . ")";
            $aptdate_exe = $DB->query($aptdate_q);
            while ($aptdatedetails = $aptdate_exe->fetch_assoc()) {
                $all_apt_service_amt[] = $aptdatedetails;
            }
            if (isset($all_apt_service_amt) && is_array($all_apt_service_amt) && count($all_apt_service_amt) > 0) {
                foreach ($all_apt_service_amt as $servdkey => $servvalue) {
                    $all_ser_amt[$servvalue['AppointmentID']][$servvalue['ServiceID']] = $servvalue['ServiceAmount'];
                }
            }
        }


        /*
         * get discount 
         */
        if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
            $apt_in_ids = implode(",", $appointment_ids);
            $aptdate_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ")";
            $aptdate_exe = $DB->query($aptdate_q);
            while ($disdetails = $aptdate_exe->fetch_assoc()) {
                $all_apt_dis_amt[] = $disdetails;
            }

            /*
             * Get Appointment Offer AMount
             */
            $apt_in_ids = implode(",", $appointment_ids);
            $aptoffer_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID,OfferID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ") AND OfferAmount > 0";
            $aptoffer_exe = $DB->query($aptoffer_q);
            while ($offeretails = $aptoffer_exe->fetch_assoc()) {
                $apt_service_count[$offeretails['AppointmentID']][$offeretails['ServiceID']] = $offeretails['OfferAmount'];
            }

            $apt_in_ids = implode(",", $appointment_ids);
            $aptoffer_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID,OfferID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ") AND OfferAmount > 0";
            $aptoffer_exe = $DB->query($aptoffer_q);
            while ($offeretails = $aptoffer_exe->fetch_assoc()) {
                if (isset($all_offers_name[$offeretails['OfferID']]) && $all_offers_name[$offeretails['OfferID']]['Type'] == 1 && $offeretails['OfferID'] > 0) {
                    $offer_service_cnt = isset($apt_service_count[$offeretails['AppointmentID']]) ? count($apt_service_count[$offeretails['AppointmentID']]) : 1;
                    $all_apt_off_dis[$offeretails['AppointmentID']][$offeretails['ServiceID']] = $offeretails['OfferAmount'] / $offer_service_cnt;
                } else {
                    $all_apt_off_dis[$offeretails['AppointmentID']][$offeretails['ServiceID']] = $offeretails['OfferAmount'];
                }
            }

            if (isset($all_apt_dis_amt) && is_array($all_apt_dis_amt) && count($all_apt_dis_amt) > 0) {
                foreach ($all_apt_dis_amt as $diskey => $disvalue) {
                    /*
                     * Get Appointment service count
                     */
                    $extra_offer_amount = 0;

                    if (isset($all_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']]) && $all_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']] > 0) {
                        $extra_offer_amount = $all_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']];
                    }
                    //echo '<br>extra_offer_amount=' . $extra_offer_amount;
                    //echo '<br>member_amount=' . $disvalue['MemberShipAmount'];
                    $all_dis_amt[$disvalue['AppointmentID']][$disvalue['ServiceID']]['MemberShipAmount'] = $disvalue['MemberShipAmount'] + round($extra_offer_amount, 2);
                    $all_apt_mem_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']] = $disvalue['MemberShipAmount'];
                    //$all_dis_amt[$disvalue['AppointmentID']][$disvalue['ServiceID']]['OfferAmount'] = $disvalue['OfferAmount'];
                }
            }
        }


        /*
         * get employee appointment services
         */
        $all_emp_apt_ser_data = array();
        if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
            $eapt_in_ids = implode(",", $appointment_ids);
            if ($eapt_in_ids != '' && $in_emp_ids != '') {
                $empaptservice_q = "SELECT DISTINCT ServiceID,AppointmentID,MECID FROM tblAppointmentAssignEmployee WHERE AppointmentID IN(" . $apt_in_ids . ") AND MECID IN(" . $in_emp_ids . ")";
                $empaptservice_q_exe = $DB->query($empaptservice_q);
                while ($empaptser = $empaptservice_q_exe->fetch_assoc()) {
                    $empaptser_result[] = $empaptser;
                }
                if (isset($empaptser_result) && is_array($empaptser_result) && count($empaptser_result) > 0) {
                    foreach ($empaptser_result as $eservdkey => $eservvalue) {
                        $all_emp_apt_ser_data[$eservvalue['MECID']][$eservvalue['AppointmentID']][] = $eservvalue['ServiceID'];
                    }
                }
            }
        }
        if (!empty($in_emp_ids)) {
            $emp_data = select("*", "tblEmployees", "EID IN (" . $in_emp_ids . ")");
        } else {
            $emp_data = select("*", "tblEmployees", "EID > 0");
        }
        if (isset($emp_data) && is_array($emp_data) && count($emp_data) > 0) {
            foreach ($emp_data as $ekey => $evalue) {
                $empid_arr[$evalue['EID']] = $evalue;
            }
        }

        if (isset($empid_arr) && is_array($empid_arr) && count($empid_arr) > 0) {
            foreach ($empid_arr as $ekey => $row) {

                $strEID = $row["EID"];
                $strEmpPercentage = $row["EmpPercentage"];

                if (isset($all_sales_data[$strEID]) && is_array($all_sales_data[$strEID]) && count($all_sales_data[$strEID]) > 0) {

                    $strSID = "";
                    $qty = "";
                    $strSAmount = "";
                    $strAmount = "";
                    $strCommission = "";
                    $FinalDAmount = '';
                    $FinalDiscount = '';
                    $UltimateSale = '';
                    $AfterDivideSale = '';


                    foreach ($all_sales_data[$strEID] as $key => $rowdetails) {

                        $cnttcntser = 0;
                        $strEIDa = $rowdetails["EID"];
                        $strAID = $rowdetails["AppointmentID"];
                        $strSID = $rowdetails["ServiceID"];
                        //$qty = $rowdetails["QtyParam"];
                        $qty = $rowdetails["Qty"];
                        $strEmployeeName = $row["EmployeeName"];
                        $strAmount = isset($all_ser_amt[$strAID][$strSID]) ? $all_ser_amt[$strAID][$strSID] : 0;
                        $strSAmount = $strAmount;
                        $strCommission = $rowdetails["Commission"];
                        $StoreIDd = $rowdetails["StoreID"];

                        $unique_apt[$rowdetails['AppointmentID']]['CustomerID'] = $rowdetails['CustomerID'];
                        $unique_apt[$rowdetails['AppointmentID']]['AppointmentDate'] = $rowdetails['AppointmentDate'];


                        $seep = isset($all_emp_apt_ser_data[$strEID][$strAID]) ? $all_emp_apt_ser_data[$strEID][$strAID] : array();
                        $sey[] = $seep;

                        if ($strCommission == "1") {
                            $AfterDivideSale = $strSAmount;
                        } elseif ($strCommission == "2") {
                            $AfterDivideSale = ($strSAmount);
                        }

                        $FinalDiscount = isset($all_apt_mem_dis[$strAID][$strSID]) ? ($all_apt_mem_dis[$strAID][$strSID]) / $qty : 0;
                        $FinalOtherDiscount = isset($all_apt_off_dis[$strAID][$strSID]) ? ($all_apt_off_dis[$strAID][$strSID]) / $qty : 0;
                        $UltimateSale = $AfterDivideSale - $FinalDiscount - $FinalOtherDiscount;
                        if ($strCommission == "1") {
                            $CommssionFinal = ($UltimateSale / 100) * $strEmpPercentage;
                        } elseif ($strCommission == "2") {
                            $CommssionFinal = ($UltimateSale / 200) * $strEmpPercentage;
                        }
                        $apt_date = isset($appointment_date[$strAID]) ? date('Y-m-d', strtotime($appointment_date[$strAID])) : '';
                        if (isset($employee_sale[$strEID][$apt_date])) {
                            $employee_sale[$strEID][$apt_date] += $CommssionFinal;
                        } else {
                            $employee_sale[$strEID][$apt_date] = $CommssionFinal;
                        }
                    }
                }
            }
        }
    }
    $DB->close();


    return $employee_sale;
}

?>