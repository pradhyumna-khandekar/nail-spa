<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>

<?php

$post_data = $_POST;
if (isset($post_data['offername']) && $post_data['offername'] > 0) {
    $offer_data = select("*", "product_offers", "status=1 AND id='" . $post_data['offername'] . "'");
    if (isset($offer_data) && is_array($offer_data) && count($offer_data) > 0) {
        $offerdateto = $offer_data[0]['OfferDateTo'];
        $date = date('Y-m-d');
        if ($date > $offerdateto) {
            $response = array(
                'status' => 'fail',
                'msg' => 'Offer Expired'
            );
        } else {
            /*
             * Get Offer Store
             */
            $offer_stores = select("store_id", "product_offers_stores", "status=1 AND offer_id='" . $post_data['offername'] . "'");
            if (isset($offer_stores) && is_array($offer_stores) && count($offer_stores) > 0) {
                foreach ($offer_stores as $oskey => $osvalue) {
                    $offer_store_id[$osvalue['store_id']] = $osvalue['store_id'];
                }
            }

            $invoice_store = isset($post_data['storet']) ? $post_data['storet'] : 0;
            if (in_array($invoice_store, $offer_store_id)) {

                /*
                 * Get offer Products
                 */
                $offer_products = select("product_id", "product_offers_product_id", "status=1  AND offer_id='" . $post_data['offername'] . "'");
                if (isset($offer_products) && is_array($offer_products) && count($offer_products) > 0) {
                    foreach ($offer_products as $opkey => $opvalue) {
                        $offer_product_id[$opvalue['product_id']] = $opvalue['product_id'];
                    }
                }

                $invoice_pdt_ids = isset($post_data['product_id']) ? $post_data['product_id'] : array();
                if (isset($invoice_pdt_ids) && is_array($invoice_pdt_ids) && count($invoice_pdt_ids) > 0) {
                    foreach ($invoice_pdt_ids as $ipkey => $ipvalue) {
                        if ($ipvalue != '') {
                            if (!in_array($ipvalue, $offer_product_id)) {
                                $response = array(
                                    'status' => 'fail',
                                    'msg' => 'Offer Not valid for some Product.'
                                );
                                echo json_encode($response);
                                exit;
                            }
                        }
                    }
                }

                /*
                 * Get Total Amount
                 */
                $offer_amount = 0;
                $roundtotal = 0;
                $pdt_amount = isset($post_data['pdt_amount']) ? $post_data['pdt_amount'] : 0;
                if (isset($pdt_amount) && is_array($pdt_amount) && count($pdt_amount) > 0) {
                    foreach ($pdt_amount as $akey => $avalue) {
                        $roundtotal += $avalue;
                    }
                }
                if ($offer_data[0]['Type'] == 1) {
                    $offer_amount = $offer_data[0]['OfferAmount'];
                } else if ($offer_data[0]['Type'] == 2) {
                    $offer_per = $offer_data[0]['OfferAmount'];
                    if ($offer_per > 0) {
                        $offer_amount = ($roundtotal * $offer_per ) / 100;
                    }
                }

                $new_total = $roundtotal - $offer_amount;
                if ($new_total > 0) {
                    
                } else {
                    $new_total = 0;
                }
                $response = array(
                    'status' => 'success',
                    'msg' => 'Offer Applied Successfully.',
                    'dis_amount' => $offer_amount,
                    'new_total' => $new_total
                );
            } else {
                $response = array(
                    'status' => 'fail',
                    'msg' => 'Offer Not valid for this store.'
                );
            }
        }
    } else {
        $response = array(
            'status' => 'fail',
            'msg' => 'Offer Not found'
        );
    }
} else {
    $response = array(
        'status' => 'fail',
        'msg' => 'No Offer Selected'
    );
}
echo json_encode($response);
?>