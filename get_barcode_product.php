<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php

$response_data['res'] = "Error";
$response_data['res_msg'] = "Barcode Not Found.";
if (isset($_POST['barcode']) && $_POST['barcode'] != '') {
    $barcode = trim($_POST['barcode']);
    $type = isset($_GET['type']) ? $_GET['type'] : '';
    if ($type == 'service_use') {
        /*
         * Get Barcode Product Name
         */
        $barcode_data = select("*", "product_barcode", "status=1 AND full_barcode='" . $barcode . "'");
        if (isset($barcode_data) && is_array($barcode_data) && count($barcode_data) > 0) {
            $product_id = $barcode_data[0]['product_id'];
            $product_data = select("*", "tblNewProducts", "ProductID='" . $product_id . "' AND Status=0");
            if (isset($product_data) && is_array($product_data) && count($product_data) > 0) {
                $response_data = $product_data[0];
                $product_service = select("*", "product_service_use", "product_id='" . $product_id . "' AND barcode = '" . $barcode . "' AND status=1");

                if (isset($product_service) && is_array($product_service) && count($product_service) > 0) {

                    if ($product_service[0]['use_status'] == 2) {
                        $response_data['res'] = "Error";
                        $start_date = date('d M,Y h:i a', strtotime($product_service[0]['start_date']));
                        $end_date = date('d M,Y h:i a', strtotime($product_service[0]['end_date']));
                        $response_data['res_msg'] = "Product Start and End Date Already Marked.Start Date = " . $start_date
                                . " End Date = " . $end_date;
                    } else {
                        $response_data['use_id'] = $product_service[0]['id'];
                        $response_data['product_start'] = $product_service[0]['start_date'];
                        $response_data['product_end'] = date('Y-m-d H:i:s');

                        $response_data['opening_date'] = date('d M,Y h:i a', strtotime($product_service[0]['start_date']));
                        $response_data['closing_date'] = date('d M,Y h:i a');

                        $response_data['use_status'] = 2; //product closed
                        $response_data['res'] = "success";
                        $response_data['res_msg'] = "Product Found.";
                    }
                } else {
                    $response_data['product_start'] = date('Y-m-d H:i:s');
                    $response_data['product_end'] = "0000-00-00 00:00:00";

                    $response_data['opening_date'] = date('d M,Y h:i a');
                    $response_data['closing_date'] = "0000-00-00 00:00:00";

                    $response_data['use_status'] = 1; //product open
                    $response_data['res'] = "success";
                    $response_data['res_msg'] = "Product Found.";
                }
            } else {
                $response_data['res'] = "Error";
                $response_data['res_msg'] = "Product Not Found.";
            }
        } else {
            $response_data['res'] = "Error";
            $response_data['res_msg'] = "Barcode Not Found.";
        }
    } else {
        $barcode_data = select("*", "product_barcode", "status=1 AND full_barcode='" . $barcode . "'");
        if (isset($barcode_data) && is_array($barcode_data) && count($barcode_data) > 0) {
            if ($barcode_data[0]['is_sold'] == 1) {
                $response_data['res'] = "Error";
                $response_data['res_msg'] = "Product With this Barcode Already Sold.";
            } else if ($barcode_data[0]['is_verified'] == 2) {
                $response_data['res'] = "Error";
                $response_data['res_msg'] = "Product is Not Verified.";
            } else if ($barcode_data[0]['product_detail'] > 0) {
                $response_data['res'] = "Error";
                $response_data['res_msg'] = "Product is not Available.";
            }  else {
                /*
                 * Get Product Info
                 */
                $pdt_id = $barcode_data[0]['product_id'];
                $product_data = select("*", "tblNewProducts", "ProductID='" . $pdt_id . "' AND Status=0");
                if (isset($product_data) && is_array($product_data) && count($product_data) > 0) {
                    $response_data = $product_data[0];
                    $response_data['res'] = "success";
                    $response_data['res_msg'] = "Product Found.";
                } else {
                    $response_data['res'] = "Error";
                    $response_data['res_msg'] = "Product Not Found.";
                }
            }
        } else {
            $response_data['res'] = "Error";
            $response_data['res_msg'] = "Barcode Not Found.";
        }
    }
}

echo json_encode($response_data);
?>