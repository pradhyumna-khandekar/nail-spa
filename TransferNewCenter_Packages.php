<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
	$strPageTitle = "Package addition Step 3 | Nailspa";
	$strDisplayTitle = "New addition of center with module & service selection for Nailspa";
	$strMenuID = "3";
	$strMyTable = "tblAdmin";
	$strMyTableID = "AdminID";
	$strMyField = "Title";
	$strMyActionPage = "TransferNewCenter_Packages.php";
	$strMessage = "";
	$sqlColumn = "";
	$sqlColumnValues = "";
// code for not allowing the normal admin to access the super admin rights	
if($strAdminType!="0")
{
	die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights	

	if ($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$strStep = Filter($_POST["step"]);
		if($strStep=="edit")
		{	
			$DB=Connect();
			$Packages=$_POST["Packages"];
			$StoreIDfetch=Filter($_POST["StoreID"]);
		
			// echo $StoreIDfetch."<br>";
			// echo "<br>";
			foreach($Packages as $Package)
			{
				$SeStoreID="Select * from tblPackages where PackageID='$Package'";
				
				$RSabc = $DB->query($SeStoreID);
				if ($RSabc->num_rows > 0) 
				{
					// echo "In if<br>";
					while($rowabc = $RSabc->fetch_assoc())
					{
						$PackageID = $rowabc["PackageID"];
						$Code = $rowabc["Code"];
						$Name = $rowabc["Name"];
						$StoreID = $rowabc["StoreID"];
						$CategoryID = $rowabc["CategoryID"];
						$ServiceID = $rowabc["ServiceID"];
						$PackagePrice = $rowabc["PackagePrice"];
						$PackageNewPrice = $rowabc["PackageNewPrice"];
						$Validity = $rowabc["Validity"];
						$Status = $rowabc["Status"];
						$Tax = $rowabc["Tax"];
						$Qty = $rowabc["Qty"];
						
						
						$InsertPckg = "Insert into tblPackages 
						(Code, 
						Name, 
						StoreID, 
						CategoryID, 
						ServiceID, 
						PackagePrice, 
						PackageNewPrice, 
						Validity, 
						Status, 
						Tax, 
						Qty) values
						('".$Code."',
						'".$Name."', 
						'".$StoreIDfetch."',
						'".$CategoryID."', 
						'".$ServiceID."',
						'".$PackagePrice."',
						'".$PackageNewPrice."',
						'".$Validity."', 
						'0', 
						'".$Tax."',
						'".$Qty."')";
						if ($DB->query($InsertPckg) === TRUE) 
						{
							echo("<script>location.href='http://pos.nailspaexperience.com/admin/ManageEmployees.php';</script>");
							echo ('<div class="alert alert-close alert-success">
									<div class="bg-green alert-icon"><i class="glyph-icon icon-check"></i></div>
										<div class="alert-content">
										<h4 class="alert-title">'.$Code.'Package updated Successfully.</h4>
										</div>
									</div>');
						}
						else
						{
							echo "Error: " . $InsertPckg . "<br>" . $conn->error;
						} 
						
					}
				}
				else
				{
									
				}
			}
			die();
		}
		
	}	
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<?php require_once("incMetaScript.fya"); ?>
</head>

<body>
	 <div id="sb-site">
        
		<?php require_once("incOpenLayout.fya"); ?>
		
		
        <?php require_once("incLoader.fya"); ?>
		
		<div id="page-wrapper">
			<div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>
			<?php require_once("incLeftMenu.fya"); ?>
			
			<div id="page-content-wrapper">
				<div id="page-content">
                    
					<?php require_once("incHeader.fya"); ?>
					
					<div id="page-title">
                        <h2>Packages for New Outlet</h2>
                    
                    </div>
				
					<div class="panel">
						<div class="panel-body">
							<div class="fa-hover">	
								<a class="btn btn-primary btn-lg btn-block"><i class="fa fa-backward">Packages</i></a>
							</div>
						
							<div class="panel-body">
								<!--<form role="form" class="form-horizontal bordered-row enquiry_form" onSubmit="proceed_formsubmit('.enquiry_form', '<?=$strMyActionPage?>', '.result_message', '', '', '','.imageupload'); return false;" enctype="multipart/form-data">-->
							<form role="form" class="form-horizontal bordered-row enquiry_form" onSubmit="proceed_formsubmit('.enquiry_form', '<?=$strMyActionPage?>', '.result_message', '', '','', '.imageupload'); return false;">
								<span class="result_message">&nbsp; <br>
								</span>
								<br>
								<input type="hidden" name="step" value="edit">

								
									<h3 class="title-hero">Step 3 : Select Packages for your store.</h3>
									<div class="example-box-wrapper">
<?php

$strID = $strAdminID;
// echo $strID."<br>";
$DB = Connect();
?>

<?php
$strID = DecodeQ(Filter($_GET["uid"]));
if(isset($_GET["uid"])&& !empty($_GET["uid"]))
{
	// echo $strID;
?>

											
											<input type="hidden" name="StoreID" value="<?=$strID?>">
											
                                            <div class="form-group"><label class="col-sm-3 control-label">Select Offers<span>*</span></label>				
												
												<div class="col-sm-6">	
                                                 <select class="form-control required" id="Packages" name="Packages[]" onChange="checktypewqfge(this);" multiple style="height:200pt;">
												
												<option value="" selected>--SELECT Packages--</option>

												<?php 
												$DB = Connect();
																$sep="Select * from tblPackages Group BY Code";
																$RSabc = $DB->query($sep);
																if ($RSabc->num_rows > 0) 
																{
																	while($rowabc = $RSabc->fetch_assoc())
																	{
																		$PackageID = $rowabc["PackageID"];
																		$Codes = $rowabc["Code"];
																		$Status = $rowabc["Status"];
																		if($Status==0)
																		{
																			$Status1="Live";
																		}
																		else
																		{
																			$Status1="Offline";
																		}
?>
																		<option value="<?php echo $PackageID?>"><?php echo $Codes?>  - <?=$Status1?>  </option>
<?php																
																	}
																}
														
														?>
														
														<?php
													   
												$DB->close();																

												?>

												</select>

												</div>

											</div>
											
											
											
											<div class="form-group"><label class="col-sm-3 control-label"></label>
												
												
												<div class="col-sm-3"><a class="btn ra-100 btn-black-opacity" href="http://pos.nailspaexperience.com/admin/ManageEmployees.php" title="Skip"><span>Skip and move to Employee addition</span></a></div>
												<div class="col-sm-3"><a class="btn ra-100 btn-red-opacity" href="http://pos.nailspaexperience.com/admin/ManageAdmin.php" title="Skip"><span>Skip and move to Create Admin</span></a></div>
												<input type="submit" class="btn ra-100 btn-primary" value="Update Packages">
											</div>
<?php
// $DB->close();
?>										
				</div>
								</form>
							</div>
<?php	// echo $strID;
}
else
{
	echo "You are on the Wrong Page. StoreID is missing";
}
// die();
?>							
						</div>
					</div>			
                  
                </div>
            </div>
        </div>
		
        <?php require_once 'incFooter.fya'; ?>
		
    </div>
</body>

</html>									