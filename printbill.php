<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php require_once("save_invoice_database.php"); ?>
<?php

date_default_timezone_set('Asia/Kolkata');
$strPageTitle = "Manage Customers | Nailspa";
$strDisplayTitle = "Manage Customers for Nailspa";
$strMenuID = "10";
$strMyTable = "tblServices";
$strMyTableID = "ServiceID";
//$strMyField = "CustomerMobileNo";
$strMyActionPage = "appointment_invoice.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $DB = Connect();
    $retail_invoice = isset($_POST['retail_invoice']) ? $_POST['retail_invoice'] : 0;
    if ($retail_invoice == 1) {
        $strCustomerID = $_POST['cust_id'];
        $customer_data = select("*", "tblCustomers", "CustomerID='" . $strCustomerID . "'");

        $strStoreID = $_POST['storet'];
        $strAppointmentDate1 = date('Y-m-d');
        $pqr = date('H:i:s');
        $strAppointmentCheckInTime = date('H:i:s');
        $strAppointmentCheckOutTime = date('H:i:s');
        $pay_type = $_POST['paytype'] == 'Complete' ? 1 : 2;
        $pendamt = isset($_POST['pendamt']) ? $_POST['pendamt'] : 0;
        $paidamt = isset($_POST['partialamt']) ? $_POST['partialamt'] : 0;
        $offerid = isset($_POST['offername']) ? $_POST['offername'] : 0;
        $flag_type = isset($_POST['type']) ? $_POST['type'] : 0;
        $off_discount = isset($_POST['off_discount']) ? $_POST['off_discount'] : 0;

        /*
         * Get Pending Amount Info
         */
        $pending_amt_apt_id = isset($_POST['pending_amt_apt_id']) ? $_POST['pending_amt_apt_id'] : 0;
        $totalpend = isset($_POST['totalpend']) ? $_POST['totalpend'] : 0;

        if ($pendamt != "0" && $pendamt != "" && $_POST['paytype'] == 'Partial') {
            $retail_paid_amount = $paidamt;
        } else {
            $retail_paid_amount = $_POST['totalpaymentamt'];
        }

        if ($flag_type == 'H') {
            $retail_paid_amount = 0;
            $hold_pending_amount = $_POST['totalpaymentamt'];
        }

        $card_amount = 0;
        $cash_amount = 0;

        if (isset($_POST['type']) && $_POST['type'] == 'C') {
            $card_amount = $retail_paid_amount;
        } else if (isset($_POST['type']) && $_POST['type'] == 'CS') {
            $cash_amount = $retail_paid_amount;
        }
        $sqlInsert = "INSERT INTO tblAppointments (CustomerID, StoreID, AppointmentDate,
                        SuitableAppointmentTime, AppointmentCheckInTime,
                        AppointmentCheckOutTime, Status,
                        source,created_date,appointment_type,payment_type,offerid)
                        VALUES('" . $strCustomerID . "', '" . $strStoreID . "', '" . $strAppointmentDate1 . "', '"
        . "" . $pqr . "', '" . $strAppointmentCheckInTime . "', '" .
        $strAppointmentCheckOutTime . "','2','1'"
        . ",'" . date('Y-m-d H:i:s') . "','2','" . $pay_type . "','" . $offerid . "')";
        $DB->query($sqlInsert);
        $retail_appoint_id = $DB->insert_id;

        $sqlInsert = "INSERT INTO  tblInvoice (AppointmentID, EmailMessageID, DateOfCreation) VALUES ('" . $retail_appoint_id . "', 'Null', '" . date('Y-m-d H:i:s') . "')";
        $DB->query($sqlInsert);
        $retail_invoice_id = $DB->insert_id;

        /*
         * Add Retail Product
         */
        $unique_pdt_id = array();
        if (isset($_POST['product_id']) && is_array($_POST['product_id']) && count($_POST['product_id']) > 0) {
            $product_barcode = $_POST['barcode'];
            $product_amount = $_POST['pdt_amount'];
            $amount_ded_discount = $_POST['amount_ded_discount'];
            $tax_per = $_POST['tax_per'];
            $tax_amount = $_POST['tax_amount'];
            foreach ($_POST['product_id'] as $pkey => $pvalue) {
                if (isset($unique_pdt_id[$pvalue])) {
                    $unique_pdt_id[$pvalue] = $unique_pdt_id[$pvalue] + 1;
                } else {
                    $unique_pdt_id[$pvalue] = 1;
                }
                $final_pdt_amount = $tax_amount[$pkey] + $amount_ded_discount[$pkey];
                $psqlInsert = "INSERT INTO appointment_retail_product (appointment_id, product_id, barcode,quantity,amount,created_date,created_by,tax_per,tax_amount,amount_ded_discount,final_amount)"
                . "  VALUES ('" . $retail_appoint_id . "','" . $pvalue . "','" . $product_barcode[$pkey] . "',"
                . "'1','" . $product_amount[$pkey] . "','" . date('Y-m-d H:i:s') . "','" . $strAdminRoleID . "','" . $tax_per[$pkey] . "','" . $tax_amount[$pkey] . "','" . $amount_ded_discount[$pkey] . "','" . $final_pdt_amount . "')";
                $DB->query($psqlInsert);

                /*
                 * Mark Product Sold in barcode table
                 */
                $barcode_soldq = "UPDATE product_barcode SET is_sold='1',appointment_id='" . $retail_appoint_id . "'"
                . " WHERE product_id='" . $pvalue . "' AND full_barcode='" . $product_barcode[$pkey] . "'";
                $DB->query($barcode_soldq);


                /*
                 * add inventory log
                 */
                $log_insert = "INSERT INTO inventory_apt_log(appointment_id,appointment_type,product_id,store_id,quantity,created_date,created_by) "
                . " VALUES('" . $retail_appoint_id . "','2','" . $pvalue . "','" . $strStoreID . "','1','" . date('Y-m-d H:i:s') . "','" . $strAdminRoleID . "')";
                $DB->query($log_insert);

                /*
                 * Deduct Product Quantity
                 */
                $product_inv_exist = select("*", "product_inventory", "status=1 AND ProductID='" . $pvalue . "' AND StoreID='" . $strStoreID . "'");
                if (isset($product_inv_exist) && is_array($product_inv_exist) && count($product_inv_exist) > 0) {
                    $new_available = $product_inv_exist[0]['Stock'] - 1;
                    $new_sold_qty = $product_inv_exist[0]['sold_qty'] + 1;
                    $updateinv = "UPDATE product_inventory SET Stock='" . $new_available . "',sold_qty='" . $new_sold_qty . "',"
                    . "modified_date='" . date('Y-m-d H:i:s') . "',modified_by='" . $strAdminID . "' "
                    . " WHERE id='" . $product_inv_exist[0]['id'] . "'";
                    $DB->query($updateinv);
                }
            }
        }

        if (isset($unique_pdt_id) && is_array($unique_pdt_id) && count($unique_pdt_id) > 0) {
            foreach ($unique_pdt_id as $key => $value) {
                $u_pdt_ids[] = $key;
                $u_pdt_qty[] = $value;
            }
        }
        /*
         * Add Invoice details
         *
         */
        $invsqlInsert = "INSERT INTO retailinvoicedetails (AppointmentId, Billaddress, InvoiceId,CustomerID,CustomerFullName,Email,Mobile,Qty,product_id,Total,RoundTotal,TotalPayment,Flag,created_date,created_by,PaidAmount,CashAmount,CardAmount,PendingAmount,pending_amt_apt_id,offer_discount,sub_total,offer_id,tax_amount)"
        . "  VALUES ('" . $retail_appoint_id . "','" . $_POST['billaddress'] . "','" . $retail_invoice_id . "',"
        . "'" . $strCustomerID . "','" . $_POST['CustomerFullName'] . "','" . $_POST['email'] . "','" . $_POST['mobile'] . "','" . implode(",", $u_pdt_qty) . "',"
        . "'" . implode(",", $u_pdt_ids) . "','" . $_POST['roundtotal'] . "','" . $_POST['roundtotal'] . "','" . $_POST['totalpaymentamt'] . "','" . $_POST['type'] . "','" . date('Y-m-d H:i:s') . "','" . $strAdminRoleID . "',"
        . "'" . $retail_paid_amount . "','" . $cash_amount . "','" . $card_amount . "','" . $totalpend . "','" . $pending_amt_apt_id . "','" . $off_discount . "','" . $_POST['sub_total'] . "','" . $offerid . "','" . $_POST['tax_amount_total'] . "')";
        $DB->query($invsqlInsert);

        /*
         * Add Pending Aamount if any
         */

        $CustomerFullName = isset($customer_data[0]['CustomerFullName']) ? $customer_data[0]['CustomerFullName'] : '';
        if (($pendamt != "0" && $pendamt != "" && $_POST['paytype'] == 'Partial') || (isset($hold_pending_amount) && $hold_pending_amount > 0)) {
            $sqlInsert2 = "Insert into tblPendingPayments(PendingAmount, AppointmentID, InvoiceID,DateTimeStamp,PaidAmount,Status,CustomerID,PendingStatus) "
            . " values('" . $pendamt . "','" . $retail_appoint_id . "', '" . $retail_invoice_id . "','" . date('Y-m-d') . "','" . $paidamt . "','1','" . $CustomerFullName . "','2')";
            $DB->query($sqlInsert2);
        } else {
            $sqlInsert2 = "Insert into tblPendingPayments(PaidAmount, AppointmentID, InvoiceID,DateTimeStamp,PendingAmount,Status,CustomerID,PendingStatus) "
            . " values('" . $retail_paid_amount . "','" . $retail_appoint_id . "', '" . $retail_invoice_id . "','" . date('Y-m-d') . "','" . $paidamt . "','0','" . $CustomerFullName . "','1')";
            $DB->query($sqlInsert2);
        }

        $getUID = EncodeQ($retail_appoint_id);
        $json_data = array('res' => 'success', 'apt_id' => $getUID);
        echo json_encode($json_data);
        exit;
    }

    $type = $_POST['type'];
    $date = date('Y-m-d h:i:s');
    $appointment_id = isset($retail_appoint_id) ? $retail_appoint_id : $_POST['appointment_idd'];
    $billaddress = $_POST['billaddress'];
    $invoiceid = isset($retail_invoice_id) ? $retail_invoice_id : $_POST['invoiceid'];
    $CustomerFullName = $_POST['CustomerFullName'];
    $email = $_POST['email'];
    $mobile = $_POST['mobile'];
    $servicecode = $_POST['servicecode'];
    $serviceid = $_POST['serviceid'];
    $qty = $_POST['qty'];
    $serviceamt = $_POST['serviceamt'];
    $membershipname = $_POST['membershipname'];
    $Discount = $_POST['Discount'];
    $disamt = $_POST['disamt'];
    $sub_total = $_POST['sub_total'];
    $offeramt = $_POST['offeramt'];
    $chargename = $_POST['chargename'];
    $chargeamount = $_POST['chargeamount'];
    $total = $_POST['total'];
    $roundtotal = $_POST['roundtotal'];
    $cashamt = $_POST['cashamt'];
    $totalpayment = $_POST['totalpayment'];
    $servicecode1 = implode(",", $servicecode);
    $serviceid1 = implode(",", $serviceid);
    $qty1 = implode(",", $qty);
    $serviceamt1 = implode(",", $serviceamt);
    $membershipname1 = implode(",", $membershipname);
    $Discount1 = implode(",", $Discount);
    $chargename1 = implode(",", $chargename);
    $chargeamount1 = implode(",", $chargeamount);
    $disamt1 = implode(",", $disamt);
    $cardboth = $_POST['cardboth'];
    $cashboth = $_POST['cashboth'];
    $pendamt = $_POST['pendamt'];
    $paidamt = $_POST['completeamt'];
    $membercost = $_POST['membercost'];
    $serviceidm = $_POST['serviceidm'];
    $discountm = $_POST['discountm'];
    $memid = $_POST['memid'];
    $serviceido = $_POST['serviceido'];
    $offeramt = $_POST['offeramttt'];
    $offerid = $_POST['offerid'];
    $offeramtt = $_POST['offeramt'];
    $totalpend = $_POST['totalpend'];
    $pending_amt_apt_id = isset($_POST['pending_amt_apt_id']) ? $_POST['pending_amt_apt_id'] : 0;
    $purchaseid = $_POST['purchaseid'];
    $Redemptid = $_POST['Redemptid'];
    $date = date('Y-m-d H:i:s');
    $PackageID = $_POST['PackageID'];
    $packagee = implode(",", $PackageID);
    $totalredamt = $_POST['totalredamt'];
    $timestamp = date("H:i:s", time());
    $seldata = select("count(*)", "tblInvoiceDetails", "AppointmentId='" . $appointment_id . "'");
    $app_id = $seldata[0]['count(*)'];
    $seldatap = select("*", "tblInvoiceDetails", "AppointmentId='" . $appointment_id . "'");
    $flagtype = $seldatap[0]['Flag'];
    $PackageIDFlag = $seldatap[0]['PackageIDFlag'];
    $seldatapamt = select("*", "tblGiftVouchers", "GiftVoucherID='" . $Redemptid . "'");
    $gftamt = $seldatapamt[0]['Amount'];

/*
 * Check Employee assign or not
 */
    $invoice_all_services = select("*", "tblAppointmentsDetailsInvoice", "AppointmentID='" . $appointment_id . "' AND ServiceID > 0 AND PackageService =0");
    if(isset($invoice_all_services) && is_array($invoice_all_services) && count($invoice_all_services) >0){
        foreach ($invoice_all_services as $key => $val) {
            $seldppay = select("*", "tblAppointmentAssignEmployee", "AppointmentID='" . $appointment_id . "' and ServiceID='" . $val['ServiceID'] . "'");
            if (isset($seldppay[0]['MECID']) && $seldppay[0]['MECID'] == '0') {
                echo "Assign Employee for All Services.";
                exit;
            }
        }
    }


    ////////////////////////////////////////////////////////////////////////
    $seldata = select("CustomerID,StoreID,AppointmentDate", "tblAppointments", "AppointmentID='" . $appointment_id . "'");
    $customer = $seldata[0]['CustomerID'];

    /*
     * GET Last 3 Appointment Detail Of Customer
     */
    $last_apt_qry = "SELECT * FROM `tblAppointments` WHERE status=2 AND CustomerID = '" . $customer . "' ORDER BY AppointmentID DESC LIMIT 3";
    $last_apt_qry_exe = $DB->query($last_apt_qry);
    if ($last_apt_qry_exe->num_rows > 0) {
        while ($alst_apt_data = $last_apt_qry_exe->fetch_assoc()) {
            $last_appointment[$alst_apt_data['AppointmentID']] = $alst_apt_data;
        }
    }


    if ($type != '') {
        if ($membercost != '0') {
            $sqlUpdate3 = "UPDATE tblCustomerMemberShip SET Status='1' WHERE CustomerID='" . $customer . "'";
            //ExecuteNQ($sqlUpdate3);
            if ($DB->query($sqlUpdate3) === TRUE) {
                // echo "Record Update successfully.";
            } else {
                echo "Error2";
            }
        }
        /*
         *//////////////////////////////////////////////////////UpdateStock/////////////////////////////////////////////////////////////////////
        $passingID1 = $appointment_id;
        $str = "Hello";

        $sqlUpdate1 = "UPDATE tblAppointments SET AppointmentCheckOutTime = '" . $timestamp . "', Status = '2' WHERE AppointmentID='" . $appointment_id . "'";
        $passingID = EncodeQ(DecodeQ($passingID1));
        ExecuteNQ($sqlUpdate1);

        $seldata = select("CustomerID,StoreID,AppointmentDate", "tblAppointments", "AppointmentID='" . $appointment_id . "'");
        $customer = $seldata[0]['CustomerID'];
        $stores = $seldata[0]['StoreID'];
        $appoint_date = $seldata[0]['AppointmentDate'];

        $sqlUpdate2 = "UPDATE tblAppointmentsDetailsInvoice SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";

        ExecuteNQ($sqlUpdate2);

        $seldatat = select("*", "tblAppointmentsDetailsInvoice", "AppointmentID='" . $appointment_id . "'");
        foreach ($seldatat as $valp) {

            //print_r($valp);
            $servicee = $valp['ServiceID'];
            $qty = $valp['qty'];
            $seldataty = select("distinct(ProductID)", "tblProductsServices", "ServiceID='" . $servicee . "'");
            //	print_r($seldataty);
            foreach ($seldataty as $valw) {
                $seldatatqq = select("count(ProductID)", "tblNewProducts", "ProductID='" . $valw['ProductID'] . "'");
                $cnt = $seldatatqq[0]['count(ProductID)'];

                if ($cnt == '0') {

                    $seldatatqt = select("*", "tblNewProductStocks", "ProductStockID='" . $valw['ProductID'] . "'");

                    $PerQtyServe = $seldatatqt[0]['PerQtyServe'];

                    $septq = select("*", "tblStoreProduct", "ProductStockID='" . $valw['ProductID'] . "' and StoreID='" . $stores . "'");
                    $UpdatePerQtyServe = $septq[0]['UpdatePerQtyServe'];
                    $stock = $septq[0]['Stock'];
                    if ($UpdatePerQtyServe == $PerQtyServe) {
                        $newstock = $stock - 1;
                        $sqlUpdate = "UPDATE  tblStoreProduct SET UpdatePerQtyServe='0',Stock='" . $newstock . "' WHERE ProductStockID='" . $valw['ProductID'] . "' and StoreID='" . $stores . "'";
                        ExecuteNQ($sqlUpdate);
                        $UpdatePerQtyServe1 = 1;
                        $newupdate = $UpdatePerQtyServe1;
                        $sqlUpdate1 = "UPDATE  tblStoreProduct SET UpdatePerQtyServe='" . $newupdate . "' WHERE ProductStockID='" . $valw['ProductID'] . "'  and StoreID='" . $stores . "'";
                        ExecuteNQ($sqlUpdate1);
                        //echo ExecuteNQ($sqlUpdate1);
                    } else {
                        $newupdate = $UpdatePerQtyServe + 1;
                        $sqlUpdate = "UPDATE  tblStoreProduct SET UpdatePerQtyServe='" . $newupdate . "' WHERE ProductStockID='" . $valw['ProductID'] . "'  and StoreID='" . $stores . "'";
                        ExecuteNQ($sqlUpdate);
                    }
                } else {
                    $seldatatq = select("*", "tblNewProducts", "ProductID='" . $valw['ProductID'] . "'");

                    $variation = $seldatatq[0]['HasVariation'];
                    if ($variation != "0") {

                        $seldatatqt = select("*", "tblNewProductStocks", "ProductID='" . $valw['ProductID'] . "'");
                        foreach ($seldatatqt as $vqq) {
                            $PerQtyServe = $vqq['PerQtyServe'];

                            $septq = select("*", "tblStoreProduct", "ProductStockID='" . $vqq['ProductStockID'] . "' and StoreID='" . $stores . "'");
                            $UpdatePerQtyServe = $septq[0]['UpdatePerQtyServe'];
                            $stock = $septq[0]['Stock'];
                            if ($UpdatePerQtyServe == $PerQtyServe) {
                                $newstock = $stock - 1;
                                $sqlUpdate = "UPDATE  tblStoreProduct SET UpdatePerQtyServe='0',Stock='" . $newstock . "' WHERE ProductStockID='" . $vqq['ProductStockID'] . "' and StoreID='" . $stores . "'";
                                ExecuteNQ($sqlUpdate);
                            } else {
                                $newupdate = $UpdatePerQtyServe + 1;
                                $sqlUpdate = "UPDATE  tblStoreProduct SET UpdatePerQtyServe='" . $newupdate . "' WHERE ProductStockID='" . $vqq['ProductStockID'] . "' and StoreID='" . $stores . "'";
                                ExecuteNQ($sqlUpdate);
                            }
                        }
                    } else {
                        $PerQtyServe = $seldatatq[0]['PerQtyServe'];


                        /*
                         * add inventory log
                         */
                        if ($PerQtyServe > 0) {
                            $PerQtyServe = $PerQtyServe;
                        } else {
                            $PerQtyServe = 1;
                        }
                        $service_qty = $valp['qty'];
                        $deduct_qty = $PerQtyServe * $service_qty;
                        $log_insert = "INSERT INTO inventory_apt_log(appointment_id,appointment_type,product_id,store_id,quantity,created_date,created_by,service_id) "
                        . " VALUES('" . $appointment_id . "','1','" . $valw['ProductID'] . "','" . $stores . "','" . $deduct_qty . "','" . date('Y-m-d H:i:s') . "','" . $strAdminRoleID . "','" . $servicee . "')";
                        $DB->query($log_insert);

                        $inven_exist = select("*", "product_inventory", "ProductID='" . $valw['ProductID'] . "' and StoreID='" . $stores . "' AND status=1");
                        if (isset($inven_exist) && is_array($inven_exist) && count($inven_exist) > 0) {


                            $sold_qty = $inven_exist[0]['sold_qty'] + $deduct_qty;
                            $available_qty = $inven_exist[0]['Stock'] - $deduct_qty;

                            $updateinv = "UPDATE product_inventory SET sold_qty='" . $sold_qty . "',Stock='" . $available_qty . "',"
                            . "modified_date='" . date('Y-m-d H:i:s') . "',modified_by='" . $strAdminID . "' "
                            . " WHERE id='" . $inven_exist[0]['id'] . "'";
                            //echo $updateinv;exit;
                            $DB->query($updateinv);
                        }
                        $seldatatqu = select("*", "tblStoreProduct", "ProductID='" . $valw['ProductID'] . "' and StoreID='" . $stores . "'");

                        foreach ($seldatatqu as $sq) {
                            $stock = $sq['Stock'];
                            $UpdatePerQtyServe = $sq['UpdatePerQtyServe'];
                            if ($UpdatePerQtyServe == $PerQtyServe) {
                                $newstock = $stock - 1;
                                $sqlUpdate = "UPDATE  tblStoreProduct SET UpdatePerQtyServe='0',Stock='" . $newstock . "' WHERE ProductID='" . $sq['ProductID'] . "' and StoreID='" . $stores . "'";
                                ExecuteNQ($sqlUpdate);
                                $UpdatePerQtyServe1 = 1;
                                $newupdate = $UpdatePerQtyServe1;
                                $sqlUpdate1 = "UPDATE  tblStoreProduct SET UpdatePerQtyServe='" . $newupdate . "' WHERE ProductID='" . $sq['ProductID'] . "' and StoreID='" . $stores . "'";
                                ExecuteNQ($sqlUpdate1);
                            } else {
                                $newupdate = $UpdatePerQtyServe + 1;
                                $sqlUpdate = "UPDATE  tblStoreProduct SET UpdatePerQtyServe='" . $newupdate . "' WHERE ProductID='" . $sq['ProductID'] . "' and StoreID='" . $stores . "'";
                                ExecuteNQ($sqlUpdate);
                                // echo $sqlUpdate;
                            }
                        }
                    }
                }
            }
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }




    ////////////////////////////////////////////////////////////////////////

    if ($app_id != "0") {
        if ($flagtype == 'H') {
            if ($type == 'BOTH') {
                $sqp = select("CustomerID", "tblCustomers", "CustomerFullName='" . $CustomerFullName . "'");
                $cust = $sqp[0]['CustomerID'];
                if ($packagee != "") {
                    $sqlUpdate = "UPDATE tblInvoiceDetails SET Billaddress='" . $billaddress . "',InvoiceId='" . $invoiceid . "',CustomerID='" . $customer . "',CustomerFullName='" . $CustomerFullName . "',Email='" . $email . "',Mobile='" . $mobile . "',SubTotal='" . $sub_total . "',OfferAmt='" . $offeramtt . "',Total='" . $total . "',RoundTotal='" . $roundtotal . "',CashAmount='" . $cashboth . "',CardAmount='" . $cardboth . "',TotalPayment='" . $totalpayment . "',ServiceCode='" . $servicecode1 . "',ServiceName='" . $serviceid1 . "',Qty='" . $qty1 . "',ServiceAmt='" . $serviceamt1 . "',MembershipName='" . $membershipname1 . "',Discount='" . $Discount1 . "',DisAmt='" . $disamt1 . "',ChargeName='" . $chargename1 . "',ChargeAmount='" . $chargeamount1 . "',Flag='" . $type . "',PendingAmount='" . $totalpend . "',pending_amt_apt_id='" . $pending_amt_apt_id . "',Membership_Amount='" . $membercost . "',PackageID='" . $packagee . "',PackageIDFlag='P' WHERE AppointmentId='" . $appointment_id . "'";
                } else {
                    $sqlUpdate = "UPDATE tblInvoiceDetails SET Billaddress='" . $billaddress . "',InvoiceId='" . $invoiceid . "',CustomerID='" . $customer . "',CustomerFullName='" . $CustomerFullName . "',Email='" . $email . "',Mobile='" . $mobile . "',SubTotal='" . $sub_total . "',OfferAmt='" . $offeramtt . "',Total='" . $total . "',RoundTotal='" . $roundtotal . "',CashAmount='" . $cashboth . "',CardAmount='" . $cardboth . "',TotalPayment='" . $totalpayment . "',ServiceCode='" . $servicecode1 . "',ServiceName='" . $serviceid1 . "',Qty='" . $qty1 . "',ServiceAmt='" . $serviceamt1 . "',MembershipName='" . $membershipname1 . "',Discount='" . $Discount1 . "',DisAmt='" . $disamt1 . "',ChargeName='" . $chargename1 . "',ChargeAmount='" . $chargeamount1 . "',Flag='" . $type . "',PendingAmount='" . $totalpend . "',pending_amt_apt_id='" . $pending_amt_apt_id . "',Membership_Amount='" . $membercost . "' WHERE AppointmentId='" . $appointment_id . "'";
                }

                if ($DB->query($sqlUpdate) === TRUE) {
                    $data = 2; //last id of tblCustomers insert
                }
                //////////////////////gift vouch change//////////////
                if ($purchaseid != "") {
                    $sqlUpdate2 = "UPDATE tblInvoiceDetails SET GVPurchasedID='" . $purchaseid . "' WHERE AppointmentId='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate2);
                } elseif ($purchaseid != "" && $Redemptid != "") {
                    $remainamt = $gftamt - $totalredamt;
                    $sqlUpdate3 = "UPDATE tblInvoiceDetails SET GVPurchasedID='" . $purchaseid . "',GVRedeemedID='" . $Redemptid . "' WHERE AppointmentId='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate3);
                    $sqlUpdate4 = "UPDATE tblGiftVouchers SET RemainingGiftVoucherAmount='" . $remainamt . "' WHERE RedempedBy='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate4);
                } else {
                    $remainamt = $gftamt - $totalredamt;
                    $sqlUpdate3 = "UPDATE tblInvoiceDetails SET GVRedeemedID='" . $Redemptid . "' WHERE AppointmentId='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate3);
                    $sqlUpdate4 = "UPDATE tblGiftVouchers SET RemainingGiftVoucherAmount='" . $remainamt . "' WHERE RedempedBy='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate4);
                }
                ////////////////////////////////////////////////////////////////////////////
                if ($pendamt != "0" && $pendamt != "") {
                    $sqlDelete = "DELETE FROM tblPendingPayments WHERE CustomerID='" . $CustomerFullName . "'";
                    ExecuteNQ($sqlDelete);

                    $sqlInsert2 = "Insert into tblPendingPayments(PendingAmount, AppointmentID, InvoiceID,DateTimeStamp,PaidAmount,Status,CustomerID,PendingStatus) values('" . $pendamt . "','" . $appointment_id . "', '" . $invoiceid . "','" . $date . "','" . $paidamt . "','1','" . $CustomerFullName . "','2')";
                    $DB->query($sqlInsert2);
                } else {
                    $sqlUpdate3 = "UPDATE tblPendingPayments SET Status = '0',PendingStatus='1' WHERE CustomerID='" . $CustomerFullName . "'";
                    ExecuteNQ($sqlUpdate3);
                }

                $sqlInsert2 = "UPDATE tblInvoiceDetails SET OfferDiscountDateTime='" . $date . "' WHERE AppointmentId='" . $appointment_id . "'";
                $DB->query($sqlInsert2);

                $sqlUpdate2 = "UPDATE tblAppointmentsDetailsInvoice SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate2);

                $sqlUpdate3 = "UPDATE tblAppointments SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate3);

                $sqlDelete = "DELETE FROM tblAppointmentlog WHERE appointment_id='" . $appointment_id . "'";
                ExecuteNQ($sqlDelete);

                $sqlInsert1 = "Insert into tblAppointmentlog (appointment_id, invoice_name, appointment_date,store,status) values('" . $appointment_id . "','" . $customer . "', '" . $appoint_date . "','" . $stores . "','2')";
                $DB->query($sqlInsert1);
            } elseif ($type == 'H') {
                $sqp = select("CustomerID", "tblCustomers", "CustomerFullName='" . $CustomerFullName . "'");
                $cust = $sqp[0]['CustomerID'];

                if ($packagee != "") {
                    $sqlUpdate = "UPDATE tblInvoiceDetails SET Billaddress='" . $billaddress . "',InvoiceId='" . $invoiceid . "',CustomerID='" . $customer . "',CustomerFullName='" . $CustomerFullName . "',Email='" . $email . "',Mobile='" . $mobile . "',SubTotal='" . $sub_total . "',OfferAmt='" . $offeramtt . "',Total='" . $total . "',RoundTotal='" . $roundtotal . "',CashAmount='" . $roundtotal . "',TotalPayment='" . $totalpayment . "',ServiceCode='" . $servicecode1 . "',ServiceName='" . $serviceid1 . "',Qty='" . $qty1 . "',ServiceAmt='" . $serviceamt1 . "',MembershipName='" . $membershipname1 . "',Discount='" . $Discount1 . "',DisAmt='" . $disamt1 . "',ChargeName='" . $chargename1 . "',ChargeAmount='" . $chargeamount1 . "',Flag='" . $type . "',CardAmount='" . $cardboth . "',Membership_Amount='" . $membercost . "',PendingAmount='" . $totalpend . "',pending_amt_apt_id='" . $pending_amt_apt_id . "',PackageID='" . $packagee . "',PackageIDFlag='P' WHERE AppointmentId='" . $appointment_id . "'";
                } else {
                    $sqlUpdate = "UPDATE tblInvoiceDetails SET Billaddress='" . $billaddress . "',InvoiceId='" . $invoiceid . "',CustomerID='" . $customer . "',CustomerFullName='" . $CustomerFullName . "',Email='" . $email . "',Mobile='" . $mobile . "',SubTotal='" . $sub_total . "',OfferAmt='" . $offeramtt . "',Total='" . $total . "',RoundTotal='" . $roundtotal . "',CashAmount='" . $roundtotal . "',TotalPayment='" . $totalpayment . "',ServiceCode='" . $servicecode1 . "',ServiceName='" . $serviceid1 . "',Qty='" . $qty1 . "',ServiceAmt='" . $serviceamt1 . "',MembershipName='" . $membershipname1 . "',Discount='" . $Discount1 . "',DisAmt='" . $disamt1 . "',ChargeName='" . $chargename1 . "',ChargeAmount='" . $chargeamount1 . "',Flag='" . $type . "',CardAmount='" . $cardboth . "',Membership_Amount='" . $membercost . "',PendingAmount='" . $totalpend . "',pending_amt_apt_id='" . $pending_amt_apt_id . "' WHERE AppointmentId='" . $appointment_id . "'";
                }
                //	ExecuteNQ($sqlUpdate);
                if ($DB->query($sqlUpdate) === TRUE) {
                    $data = 2; //last id of tblCustomers insert
                }
                ///////////////////////////////////gift vouch////////////////
                if ($purchaseid != "") {
                    $sqlUpdate2 = "UPDATE tblInvoiceDetails SET GVPurchasedID='" . $purchaseid . "' WHERE AppointmentId='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate2);
                } elseif ($purchaseid != "" && $Redemptid != "") {
                    $remainamt = $gftamt - $totalredamt;
                    $sqlUpdate3 = "UPDATE tblInvoiceDetails SET GVPurchasedID='" . $purchaseid . "',GVRedeemedID='" . $Redemptid . "' WHERE AppointmentId='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate3);

                    $sqlUpdate4 = "UPDATE tblGiftVouchers SET RemainingGiftVoucherAmount='" . $remainamt . "' WHERE RedempedBy='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate4);
                } else {
                    $remainamt = $gftamt - $totalredamt;
                    $sqlUpdate3 = "UPDATE tblInvoiceDetails SET GVRedeemedID='" . $Redemptid . "' WHERE AppointmentId='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate3);

                    $sqlUpdate4 = "UPDATE tblGiftVouchers SET RemainingGiftVoucherAmount='" . $remainamt . "' WHERE RedempedBy='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate4);
                }
                ////////////////////////////////////
                if ($pendamt != "") {
                    $sep = select("PendingAmount", "tblPendingPayments", "CustomerID='" . $CustomerFullName . "' and Status='1' and PendingStatus='2'");
                    foreach ($sep as $avp) {
                        $pendamtt = $avp['PendingAmount'];
                        $totalpend = $totalpend + $pendamtt;
                    }
                    $pend = $totalpend + $pendamt;
                    $sqlInsert2 = "UPDATE tblPendingPayments SET PendingAmount='" . $pendamt . "',DateTimeStamp='" . $date . "',PaidAmount='" . $paidamt . "',CustomerID='" . $CustomerFullName . "' where AppointmentId='" . $appointment_id . "'";
                    ExecuteNQ($sqlInsert2);
                } else {

                    $sep = select("PendingAmount", "tblPendingPayments", "CustomerID='" . $CustomerFullName . "' and Status='1' and PendingStatus='2'");
                    foreach ($sep as $avp) {
                        $pendamtt = $avp['PendingAmount'];
                        $totalpend = $totalpend + $pendamtt;
                    }
                    $pend = $totalpend + $paidamt;

                    $sqlInsert2 = "UPDATE tblPendingPayments SET PendingAmount='" . $roundtotal . "',DateTimeStamp='" . $date . "',PaidAmount='0' where CustomerID='" . $CustomerFullName . "' and Status='1' and PendingStatus='2'";
                    // echo $sqlInsert2;
                    ExecuteNQ($sqlInsert2);
                }

                $sqlInsert2 = "UPDATE tblInvoiceDetails SET OfferDiscountDateTime='" . $date . "' WHERE AppointmentId='" . $appointment_id . "'";
                //$DB->query($sqlInsert1);
                $DB->query($sqlInsert2);

                $sqlUpdate2 = "UPDATE tblAppointmentsDetailsInvoice SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate2);


                $sqlUpdate3 = "UPDATE tblAppointments SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate3);

                $sqlDelete = "DELETE FROM tblAppointmentlog WHERE appointment_id='" . $appointment_id . "'";
                ExecuteNQ($sqlDelete);

                $sqlInsert1 = "Insert into tblAppointmentlog (appointment_id, invoice_name, appointment_date,store,status) values('" . $appointment_id . "','" . $customer . "', '" . $appoint_date . "','" . $stores . "','2')";
                $DB->query($sqlInsert1);
                ///////////////////////////////////////////////////////////////////////////////////
            } else if ($type == 'CS') {
                $sqp = select("CustomerID", "tblCustomers", "CustomerFullName='" . $CustomerFullName . "'");
                $cust = $sqp[0]['CustomerID'];
                if ($packagee != "") {

                    $sqlUpdate = "UPDATE tblInvoiceDetails SET Billaddress='" . $billaddress . "',InvoiceId='" . $invoiceid . "',CustomerID='" . $customer . "',CustomerFullName='" . $CustomerFullName . "',Email='" . $email . "',Mobile='" . $mobile . "',SubTotal='" . $sub_total . "',OfferAmt='" . $offeramtt . "',Total='" . $total . "',RoundTotal='" . $roundtotal . "',CashAmount='" . $roundtotal . "',TotalPayment='" . $totalpayment . "',ServiceCode='" . $servicecode1 . "',ServiceName='" . $serviceid1 . "',Qty='" . $qty1 . "',ServiceAmt='" . $serviceamt1 . "',MembershipName='" . $membershipname1 . "',Discount='" . $Discount1 . "',DisAmt='" . $disamt1 . "',ChargeName='" . $chargename1 . "',ChargeAmount='" . $chargeamount1 . "',Flag='" . $type . "',Membership_Amount='" . $membercost . "',PendingAmount='" . $totalpend . "',pending_amt_apt_id='" . $pending_amt_apt_id . "',PackageID='" . $packagee . "',PackageIDFlag='P' WHERE AppointmentId='" . $appointment_id . "'";
                } else {
                    $sqlUpdate = "UPDATE tblInvoiceDetails SET Billaddress='" . $billaddress . "',InvoiceId='" . $invoiceid . "',CustomerID='" . $customer . "',CustomerFullName='" . $CustomerFullName . "',Email='" . $email . "',Mobile='" . $mobile . "',SubTotal='" . $sub_total . "',OfferAmt='" . $offeramtt . "',Total='" . $total . "',RoundTotal='" . $roundtotal . "',CashAmount='" . $roundtotal . "',TotalPayment='" . $totalpayment . "',ServiceCode='" . $servicecode1 . "',ServiceName='" . $serviceid1 . "',Qty='" . $qty1 . "',ServiceAmt='" . $serviceamt1 . "',MembershipName='" . $membershipname1 . "',Discount='" . $Discount1 . "',DisAmt='" . $disamt1 . "',ChargeName='" . $chargename1 . "',ChargeAmount='" . $chargeamount1 . "',Flag='" . $type . "',Membership_Amount='" . $membercost . "',PendingAmount='" . $totalpend . "',pending_amt_apt_id='" . $pending_amt_apt_id . "' WHERE AppointmentId='" . $appointment_id . "'";
                }
                //	ExecuteNQ($sqlUpdate);
                if ($DB->query($sqlUpdate) === TRUE) {
                    $data = 2; //last id of tblCustomers insert
                }
                /////////////////////////////////////////gift vouch//////////////////
                if ($purchaseid != "") {
                    $sqlUpdate2 = "UPDATE tblInvoiceDetails SET GVPurchasedID='" . $purchaseid . "' WHERE AppointmentId='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate2);
                } elseif ($purchaseid != "" && $Redemptid != "") {
                    $remainamt = $gftamt - $totalredamt;
                    $sqlUpdate3 = "UPDATE tblInvoiceDetails SET GVPurchasedID='" . $purchaseid . "',GVRedeemedID='" . $Redemptid . "' WHERE AppointmentId='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate3);

                    $sqlUpdate4 = "UPDATE tblGiftVouchers SET RemainingGiftVoucherAmount='" . $remainamt . "' WHERE RedempedBy='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate4);
                } else {
                    $remainamt = $gftamt - $totalredamt;
                    $sqlUpdate3 = "UPDATE tblInvoiceDetails SET GVRedeemedID='" . $Redemptid . "' WHERE AppointmentId='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate3);

                    $sqlUpdate4 = "UPDATE tblGiftVouchers SET RemainingGiftVoucherAmount='" . $remainamt . "' WHERE RedempedBy='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate4);
                }
                ///////////////////////////////////////
                if ($pendamt != "0" && $pendamt != "") {
                    $sqlDelete = "DELETE FROM tblPendingPayments WHERE CustomerID='" . $CustomerFullName . "'";
                    ExecuteNQ($sqlDelete);

                    $sqlInsert2 = "Insert into tblPendingPayments(PendingAmount, AppointmentID, InvoiceID,DateTimeStamp,PaidAmount,Status,CustomerID,PendingStatus) values('" . $pendamt . "','" . $appointment_id . "', '" . $invoiceid . "','" . $date . "','" . $paidamt . "','1','" . $CustomerFullName . "','2')";
                    $DB->query($sqlInsert2);
                } else {
                    $sqlUpdate3 = "UPDATE tblPendingPayments SET Status = '0',PendingStatus='1' WHERE CustomerID='" . $CustomerFullName . "'";
                    ExecuteNQ($sqlUpdate3);
                }

                $sqlInsert2 = "UPDATE tblInvoiceDetails SET OfferDiscountDateTime='" . $date . "' WHERE AppointmentId='" . $appointment_id . "'";
                //$DB->query($sqlInsert1);
                $DB->query($sqlInsert2);

                $sqlUpdate2 = "UPDATE tblAppointmentsDetailsInvoice SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate2);

                $sqlUpdate3 = "UPDATE tblAppointments SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate3);

                $sqlDelete = "DELETE FROM tblAppointmentlog WHERE appointment_id='" . $appointment_id . "'";
                ExecuteNQ($sqlDelete);

                $sqlInsert1 = "Insert into tblAppointmentlog (appointment_id, invoice_name, appointment_date,store,status) values('" . $appointment_id . "','" . $customer . "', '" . $appoint_date . "','" . $stores . "','2')";
                $DB->query($sqlInsert1);
                ///////////////////////////////////////////////////////////////////////////////////
            } else if ($type == 'C') {
                $sqp = select("CustomerID", "tblCustomers", "CustomerFullName='" . $CustomerFullName . "'");
                $cust = $sqp[0]['CustomerID'];

                if ($packagee != "") {
                    $sqlUpdate = "UPDATE tblInvoiceDetails SET Billaddress='" . $billaddress . "',InvoiceId='" . $invoiceid . "',CustomerID='" . $customer . "',CustomerFullName='" . $CustomerFullName . "',Email='" . $email . "',Mobile='" . $mobile . "',SubTotal='" . $sub_total . "',OfferAmt='" . $offeramtt . "',Total='" . $total . "',RoundTotal='" . $roundtotal . "',CardAmount='" . $roundtotal . "',TotalPayment='" . $totalpayment . "',ServiceCode='" . $servicecode1 . "',ServiceName='" . $serviceid1 . "',Qty='" . $qty1 . "',ServiceAmt='" . $serviceamt1 . "',MembershipName='" . $membershipname1 . "',Discount='" . $Discount1 . "',DisAmt='" . $disamt1 . "',ChargeName='" . $chargename1 . "',ChargeAmount='" . $chargeamount1 . "',Flag='" . $type . "',Membership_Amount='" . $membercost . "',PendingAmount='" . "',pending_amt_apt_id='" . $pending_amt_apt_id . $totalpend . "',PackageID='" . $packagee . "',PackageIDFlag='P' WHERE AppointmentId='" . $appointment_id . "'";
                } else {
                    $sqlUpdate = "UPDATE tblInvoiceDetails SET Billaddress='" . $billaddress . "',InvoiceId='" . $invoiceid . "',CustomerID='" . $customer . "',CustomerFullName='" . $CustomerFullName . "',Email='" . $email . "',Mobile='" . $mobile . "',SubTotal='" . $sub_total . "',OfferAmt='" . $offeramtt . "',Total='" . $total . "',RoundTotal='" . $roundtotal . "',CardAmount='" . $roundtotal . "',TotalPayment='" . $totalpayment . "',ServiceCode='" . $servicecode1 . "',ServiceName='" . $serviceid1 . "',Qty='" . $qty1 . "',ServiceAmt='" . $serviceamt1 . "',MembershipName='" . $membershipname1 . "',Discount='" . $Discount1 . "',DisAmt='" . $disamt1 . "',ChargeName='" . $chargename1 . "',ChargeAmount='" . $chargeamount1 . "',Flag='" . $type . "',Membership_Amount='" . $membercost . "',PendingAmount='" . "',pending_amt_apt_id='" . $pending_amt_apt_id . $totalpend . "' WHERE AppointmentId='" . $appointment_id . "'";
                }
                //	ExecuteNQ($sqlUpdate);
                if ($DB->query($sqlUpdate) === TRUE) {
                    $data = 2; //last id of tblCustomers insert
                }
                ///////////////////////////gift vouch//////////////////////////////////
                if ($purchaseid != "") {
                    $sqlUpdate2 = "UPDATE tblInvoiceDetails SET GVPurchasedID='" . $purchaseid . "' WHERE AppointmentId='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate2);
                    $sqlUpdate4 = "UPDATE tblGiftVouchers SET RemainingGiftVoucherAmount='" . $remainamt . "' WHERE RedempedBy='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate4);
                } elseif ($purchaseid != "" && $Redemptid != "") {
                    $remainamt = $gftamt - $totalredamt;
                    $sqlUpdate3 = "UPDATE tblInvoiceDetails SET GVPurchasedID='" . $purchaseid . "',GVRedeemedID='" . $Redemptid . "' WHERE AppointmentId='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate3);

                    $sqlUpdate4 = "UPDATE tblGiftVouchers SET RemainingGiftVoucherAmount='" . $remainamt . "' WHERE RedempedBy='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate4);
                } else {
                    $remainamt = $gftamt - $totalredamt;
                    $sqlUpdate3 = "UPDATE tblInvoiceDetails SET GVRedeemedID='" . $Redemptid . "' WHERE AppointmentId='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate3);

                    $sqlUpdate4 = "UPDATE tblGiftVouchers SET RemainingGiftVoucherAmount='" . $remainamt . "' WHERE RedempedBy='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate4);
                }
                /////////////////////////////////////////////////
                if ($pendamt != "0" && $pendamt != "") {
                    $sqlDelete = "DELETE FROM tblPendingPayments WHERE CustomerID='" . $CustomerFullName . "'";
                    ExecuteNQ($sqlDelete);

                    $sqlInsert2 = "Insert into tblPendingPayments(PendingAmount, AppointmentID, InvoiceID,DateTimeStamp,PaidAmount,Status,CustomerID,PendingStatus) values('" . $pendamt . "','" . $appointment_id . "', '" . $invoiceid . "','" . $date . "','" . $paidamt . "','1','" . $CustomerFullName . "','2')";
                    $DB->query($sqlInsert2);
                } else {
                    $sqlUpdate3 = "UPDATE tblPendingPayments SET Status = '0',PendingStatus='1' WHERE CustomerID='" . $CustomerFullName . "'";
                    ExecuteNQ($sqlUpdate3);
                }

                $sqlInsert2 = "UPDATE tblInvoiceDetails SET OfferDiscountDateTime='" . $date . "' WHERE AppointmentId='" . $appointment_id . "'";
                //$DB->query($sqlInsert1);
                $DB->query($sqlInsert2);

                $sqlUpdate2 = "UPDATE tblAppointmentsDetailsInvoice SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate2);

                $sqlUpdate3 = "UPDATE tblAppointments SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate3);

                $sqlDelete = "DELETE FROM tblAppointmentlog WHERE appointment_id='" . $appointment_id . "'";
                ExecuteNQ($sqlDelete);

                $sqlInsert1 = "Insert into tblAppointmentlog (appointment_id, invoice_name, appointment_date,store,status) values('" . $appointment_id . "','" . $customer . "', '" . $appoint_date . "','" . $stores . "','2')";
                $DB->query($sqlInsert1);
                ///////////////////////////////////////////////////////////////////////////////////
            } else {

            }
        } else {
            if ($PackageIDFlag == 'P') {
                if ($type == 'BOTH') {
                    $sqp = select("CustomerID", "tblCustomers", "CustomerFullName='" . $CustomerFullName . "'");
                    $cust = $sqp[0]['CustomerID'];
                    if ($packagee != "") {
                        $sqlUpdate = "UPDATE tblInvoiceDetails SET Billaddress='" . $billaddress . "',InvoiceId='" . $invoiceid . "',CustomerID='" . $customer . "',CustomerFullName='" . $CustomerFullName . "',Email='" . $email . "',Mobile='" . $mobile . "',SubTotal='" . $sub_total . "',OfferAmt='" . $offeramtt . "',Total='" . $total . "',RoundTotal='" . $roundtotal . "',CashAmount='" . $cashboth . "',CardAmount='" . $cardboth . "',TotalPayment='" . $totalpayment . "',ServiceCode='" . $servicecode1 . "',ServiceName='" . $serviceid1 . "',Qty='" . $qty1 . "',ServiceAmt='" . $serviceamt1 . "',MembershipName='" . $membershipname1 . "',Discount='" . $Discount1 . "',DisAmt='" . $disamt1 . "',ChargeName='" . $chargename1 . "',ChargeAmount='" . $chargeamount1 . "',Flag='" . $type . "',PendingAmount='" . "',pending_amt_apt_id='" . $pending_amt_apt_id . $totalpend . "',Membership_Amount='" . $membercost . "',PackageID='" . $packagee . "',PackageIDFlag='P' WHERE AppointmentId='" . $appointment_id . "'";
                    } else {
                        $sqlUpdate = "UPDATE tblInvoiceDetails SET Billaddress='" . $billaddress . "',InvoiceId='" . $invoiceid . "',CustomerID='" . $customer . "',CustomerFullName='" . $CustomerFullName . "',Email='" . $email . "',Mobile='" . $mobile . "',SubTotal='" . $sub_total . "',OfferAmt='" . $offeramtt . "',Total='" . $total . "',RoundTotal='" . $roundtotal . "',CashAmount='" . $cashboth . "',CardAmount='" . $cardboth . "',TotalPayment='" . $totalpayment . "',ServiceCode='" . $servicecode1 . "',ServiceName='" . $serviceid1 . "',Qty='" . $qty1 . "',ServiceAmt='" . $serviceamt1 . "',MembershipName='" . $membershipname1 . "',Discount='" . $Discount1 . "',DisAmt='" . $disamt1 . "',ChargeName='" . $chargename1 . "',ChargeAmount='" . $chargeamount1 . "',Flag='" . $type . "',PendingAmount='" . $totalpend . "',pending_amt_apt_id='" . $pending_amt_apt_id . "',Membership_Amount='" . $membercost . "' WHERE AppointmentId='" . $appointment_id . "'";
                    }


                    //	ExecuteNQ($sqlUpdate);
                    if ($DB->query($sqlUpdate) === TRUE) {
                        $data = 2; //last id of tblCustomers insert
                    }
                    ////////////////////////////gift vouch////////////////////////////////////
                    if ($purchaseid != "") {
                        $sqlUpdate2 = "UPDATE tblInvoiceDetails SET GVPurchasedID='" . $purchaseid . "' WHERE AppointmentId='" . $appointment_id . "'";
                        ExecuteNQ($sqlUpdate2);
                    } elseif ($purchaseid != "" && $Redemptid != "") {
                        $remainamt = $gftamt - $totalredamt;
                        $sqlUpdate3 = "UPDATE tblInvoiceDetails SET GVPurchasedID='" . $purchaseid . "',GVRedeemedID='" . $Redemptid . "' WHERE AppointmentId='" . $appointment_id . "'";
                        ExecuteNQ($sqlUpdate3);

                        $sqlUpdate4 = "UPDATE tblGiftVouchers SET RemainingGiftVoucherAmount='" . $remainamt . "' WHERE RedempedBy='" . $appointment_id . "'";
                        ExecuteNQ($sqlUpdate4);
                    } else {
                        $remainamt = $gftamt - $totalredamt;

                        $sqlUpdate3 = "UPDATE tblInvoiceDetails SET GVRedeemedID='" . $Redemptid . "' WHERE AppointmentId='" . $appointment_id . "'";
                        ExecuteNQ($sqlUpdate3);

                        $sqlUpdate4 = "UPDATE tblGiftVouchers SET RemainingGiftVoucherAmount='" . $remainamt . "' WHERE RedempedBy='" . $appointment_id . "'";
                        ExecuteNQ($sqlUpdate4);
                    }
                    //////////////////////////////////////////////////
                    if ($pendamt != "0" && $pendamt != "") {
                        $sqlDelete = "DELETE FROM tblPendingPayments WHERE CustomerID='" . $CustomerFullName . "'";
                        ExecuteNQ($sqlDelete);

                        $sqlInsert2 = "Insert into tblPendingPayments(PendingAmount, AppointmentID, InvoiceID,DateTimeStamp,PaidAmount,Status,CustomerID,PendingStatus) values('" . $pendamt . "','" . $appointment_id . "', '" . $invoiceid . "','" . $date . "','" . $paidamt . "','1','" . $CustomerFullName . "','2')";
                        $DB->query($sqlInsert2);
                    } else {
                        $sqlUpdate3 = "UPDATE tblPendingPayments SET Status = '0',PendingStatus='1' WHERE CustomerID='" . $CustomerFullName . "'";
                        ExecuteNQ($sqlUpdate3);
                    }


                    $sqlInsert2 = "UPDATE tblInvoiceDetails SET OfferDiscountDateTime='" . $date . "' WHERE AppointmentId='" . $appointment_id . "'";
                    //$DB->query($sqlInsert1);
                    $DB->query($sqlInsert2);

                    $sqlUpdate2 = "UPDATE tblAppointmentsDetailsInvoice SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate2);


                    $sqlUpdate3 = "UPDATE tblAppointments SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate3);

                    $sqlDelete = "DELETE FROM tblAppointmentlog WHERE appointment_id='" . $appointment_id . "'";
                    ExecuteNQ($sqlDelete);

                    $sqlInsert1 = "Insert into tblAppointmentlog (appointment_id, invoice_name, appointment_date,store,status) values('" . $appointment_id . "','" . $customer . "', '" . $appoint_date . "','" . $stores . "','2')";
                    $DB->query($sqlInsert1);
                    ///////////////////////////////////////////////////////////////////////////////////
                } elseif ($type == 'H') {
                    $sqp = select("CustomerID", "tblCustomers", "CustomerFullName='" . $CustomerFullName . "'");
                    $cust = $sqp[0]['CustomerID'];

                    if ($packagee != "") {
                        $sqlUpdate = "UPDATE tblInvoiceDetails SET Billaddress='" . $billaddress . "',InvoiceId='" . $invoiceid . "',CustomerID='" . $customer . "',CustomerFullName='" . $CustomerFullName . "',Email='" . $email . "',Mobile='" . $mobile . "',SubTotal='" . $sub_total . "',OfferAmt='" . $offeramtt . "',Total='" . $total . "',RoundTotal='" . $roundtotal . "',CashAmount='" . $roundtotal . "',TotalPayment='" . $totalpayment . "',ServiceCode='" . $servicecode1 . "',ServiceName='" . $serviceid1 . "',Qty='" . $qty1 . "',ServiceAmt='" . $serviceamt1 . "',MembershipName='" . $membershipname1 . "',Discount='" . $Discount1 . "',DisAmt='" . $disamt1 . "',ChargeName='" . $chargename1 . "',ChargeAmount='" . $chargeamount1 . "',Flag='" . $type . "',CardAmount='" . $cardboth . "',Membership_Amount='" . $membercost . "',PendingAmount='" . $totalpend . "',pending_amt_apt_id='" . $pending_amt_apt_id . "',PackageID='" . $packagee . "',PackageIDFlag='P' WHERE AppointmentId='" . $appointment_id . "'";
                    } else {
                        $sqlUpdate = "UPDATE tblInvoiceDetails SET Billaddress='" . $billaddress . "',InvoiceId='" . $invoiceid . "',CustomerID='" . $customer . "',CustomerFullName='" . $CustomerFullName . "',Email='" . $email . "',Mobile='" . $mobile . "',SubTotal='" . $sub_total . "',OfferAmt='" . $offeramtt . "',Total='" . $total . "',RoundTotal='" . $roundtotal . "',CashAmount='" . $roundtotal . "',TotalPayment='" . $totalpayment . "',ServiceCode='" . $servicecode1 . "',ServiceName='" . $serviceid1 . "',Qty='" . $qty1 . "',ServiceAmt='" . $serviceamt1 . "',MembershipName='" . $membershipname1 . "',Discount='" . $Discount1 . "',DisAmt='" . $disamt1 . "',ChargeName='" . $chargename1 . "',ChargeAmount='" . $chargeamount1 . "',Flag='" . $type . "',CardAmount='" . $cardboth . "',Membership_Amount='" . $membercost . "',PendingAmount='" . $totalpend . "',pending_amt_apt_id='" . $pending_amt_apt_id . "' WHERE AppointmentId='" . $appointment_id . "'";
                    }
                    //	ExecuteNQ($sqlUpdate);
                    if ($DB->query($sqlUpdate) === TRUE) {
                        $data = 2; //last id of tblCustomers insert
                    }
                    ///////////////////gift vouch//////////////////////////////

                    if ($purchaseid != "") {
                        $sqlUpdate2 = "UPDATE tblInvoiceDetails SET GVPurchasedID='" . $purchaseid . "' WHERE AppointmentId='" . $appointment_id . "'";
                        ExecuteNQ($sqlUpdate2);
                    } elseif ($purchaseid != "" && $Redemptid != "") {
                        $remainamt = $gftamt - $totalredamt;
                        $sqlUpdate3 = "UPDATE tblInvoiceDetails SET GVPurchasedID='" . $purchaseid . "',GVRedeemedID='" . $Redemptid . "' WHERE AppointmentId='" . $appointment_id . "'";
                        ExecuteNQ($sqlUpdate3);

                        $sqlUpdate4 = "UPDATE tblGiftVouchers SET RemainingGiftVoucherAmount='" . $remainamt . "' WHERE RedempedBy='" . $appointment_id . "'";
                        ExecuteNQ($sqlUpdate4);
                    } else {
                        $remainamt = $gftamt - $totalredamt;
                        $sqlUpdate3 = "UPDATE tblInvoiceDetails SET GVRedeemedID='" . $Redemptid . "' WHERE AppointmentId='" . $appointment_id . "'";
                        ExecuteNQ($sqlUpdate3);

                        $sqlUpdate4 = "UPDATE tblGiftVouchers SET RemainingGiftVoucherAmount='" . $remainamt . "' WHERE RedempedBy='" . $appointment_id . "'";
                        ExecuteNQ($sqlUpdate4);
                    }
                    ////////////////////////////////////////////
                    if ($pendamt != "") {
                        $sep = select("PendingAmount", "tblPendingPayments", "CustomerID='" . $CustomerFullName . "' and Status='1' and PendingStatus='2'");
                        foreach ($sep as $avp) {
                            $pendamtt = $avp['PendingAmount'];
                            $totalpend = $totalpend + $pendamtt;
                        }

                        $pend = $totalpend + $pendamt;
                        $sqlInsert2 = "UPDATE tblPendingPayments SET PendingAmount='" . $pendamt . "',DateTimeStamp='" . $date . "',PaidAmount='" . $paidamt . "',CustomerID='" . $CustomerFullName . "' where AppointmentId='" . $appointment_id . "'";
                        ExecuteNQ($sqlInsert2);
                    } else {

                        $sep = select("PendingAmount", "tblPendingPayments", "CustomerID='" . $CustomerFullName . "' and Status='1' and PendingStatus='2'");
                        foreach ($sep as $avp) {
                            $pendamtt = $avp['PendingAmount'];
                            $totalpend = $totalpend + $pendamtt;
                        }
                        $pend = $totalpend + $paidamt;
                        $sqlInsert2 = "UPDATE tblPendingPayments SET PendingAmount='" . $roundtotal . "',DateTimeStamp='" . $date . "',PaidAmount='0' where CustomerID='" . $CustomerFullName . "' and Status='1' and PendingStatus='2'";
                        // echo $sqlInsert2;
                        ExecuteNQ($sqlInsert2);
                    }

                    $sqlInsert2 = "UPDATE tblInvoiceDetails SET OfferDiscountDateTime='" . $date . "' WHERE AppointmentId='" . $appointment_id . "'";
                    //$DB->query($sqlInsert1);
                    $DB->query($sqlInsert2);

                    $sqlUpdate2 = "UPDATE tblAppointmentsDetailsInvoice SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate2);

                    $sqlUpdate3 = "UPDATE tblAppointments SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate3);

                    $sqlDelete = "DELETE FROM tblAppointmentlog WHERE appointment_id='" . $appointment_id . "'";
                    ExecuteNQ($sqlDelete);

                    $sqlInsert1 = "Insert into tblAppointmentlog (appointment_id, invoice_name, appointment_date,store,status) values('" . $appointment_id . "','" . $customer . "', '" . $appoint_date . "','" . $stores . "','2')";
                    $DB->query($sqlInsert1);
                    ///////////////////////////////////////////////////////////////////////////////////
                } else if ($type == 'CS') {

                    $sqp = select("CustomerID", "tblCustomers", "CustomerFullName='" . $CustomerFullName . "'");
                    $cust = $sqp[0]['CustomerID'];
                    if ($packagee != "") {

                        $sqlUpdate = "UPDATE tblInvoiceDetails SET Billaddress='" . $billaddress . "',InvoiceId='" . $invoiceid . "',CustomerID='" . $customer . "',CustomerFullName='" . $CustomerFullName . "',Email='" . $email . "',Mobile='" . $mobile . "',SubTotal='" . $sub_total . "',OfferAmt='" . $offeramtt . "',Total='" . $total . "',RoundTotal='" . $roundtotal . "',CashAmount='" . $roundtotal . "',TotalPayment='" . $totalpayment . "',ServiceCode='" . $servicecode1 . "',ServiceName='" . $serviceid1 . "',Qty='" . $qty1 . "',ServiceAmt='" . $serviceamt1 . "',MembershipName='" . $membershipname1 . "',Discount='" . $Discount1 . "',DisAmt='" . $disamt1 . "',ChargeName='" . $chargename1 . "',ChargeAmount='" . $chargeamount1 . "',Flag='" . $type . "',Membership_Amount='" . $membercost . "',PendingAmount='" . $totalpend . "',pending_amt_apt_id='" . $pending_amt_apt_id . "',PackageID='" . $packagee . "',PackageIDFlag='P' WHERE AppointmentId='" . $appointment_id . "'";
                    } else {
                        $sqlUpdate = "UPDATE tblInvoiceDetails SET Billaddress='" . $billaddress . "',InvoiceId='" . $invoiceid . "',CustomerID='" . $customer . "',CustomerFullName='" . $CustomerFullName . "',Email='" . $email . "',Mobile='" . $mobile . "',SubTotal='" . $sub_total . "',OfferAmt='" . $offeramtt . "',Total='" . $total . "',RoundTotal='" . $roundtotal . "',CashAmount='" . $roundtotal . "',TotalPayment='" . $totalpayment . "',ServiceCode='" . $servicecode1 . "',ServiceName='" . $serviceid1 . "',Qty='" . $qty1 . "',ServiceAmt='" . $serviceamt1 . "',MembershipName='" . $membershipname1 . "',Discount='" . $Discount1 . "',DisAmt='" . $disamt1 . "',ChargeName='" . $chargename1 . "',ChargeAmount='" . $chargeamount1 . "',Flag='" . $type . "',Membership_Amount='" . $membercost . "',PendingAmount='" . $totalpend . "',pending_amt_apt_id='" . $pending_amt_apt_id . "' WHERE AppointmentId='" . $appointment_id . "'";
                    }

                    //ExecuteNQ($sqlUpdate);
                    if ($DB->query($sqlUpdate) === TRUE) {

                        $data = 2; //last id of tblCustomers insert
                    }

                    //////////////////////////gift vouch///////////////////////////////////////////
                    if ($purchaseid != "") {
                        $sqlUpdate2 = "UPDATE tblInvoiceDetails SET GVPurchasedID='" . $purchaseid . "' WHERE AppointmentId='" . $appointment_id . "'";
                        ExecuteNQ($sqlUpdate2);
                    } elseif ($purchaseid != "" && $Redemptid != "") {
                        $remainamt = $gftamt - $totalredamt;
                        $sqlUpdate3 = "UPDATE tblInvoiceDetails SET GVPurchasedID='" . $purchaseid . "',GVRedeemedID='" . $Redemptid . "' WHERE AppointmentId='" . $appointment_id . "'";
                        ExecuteNQ($sqlUpdate3);
                        $sqlUpdate4 = "UPDATE tblGiftVouchers SET RemainingGiftVoucherAmount='" . $remainamt . "' WHERE RedempedBy='" . $appointment_id . "'";
                        ExecuteNQ($sqlUpdate4);
                    } else {
                        $remainamt = $gftamt - $totalredamt;
                        $sqlUpdate3 = "UPDATE tblInvoiceDetails SET GVRedeemedID='" . $Redemptid . "' WHERE AppointmentId='" . $appointment_id . "'";
                        ExecuteNQ($sqlUpdate3);
                        $sqlUpdate4 = "UPDATE tblGiftVouchers SET RemainingGiftVoucherAmount='" . $remainamt . "' WHERE RedempedBy='" . $appointment_id . "'";
                        ExecuteNQ($sqlUpdate4);
                    }

                    ///////////////////////////////////////////////////
                    if ($pendamt != "0" && $pendamt != "") {
                        $sqlDelete = "DELETE FROM tblPendingPayments WHERE CustomerID='" . $CustomerFullName . "'";
                        ExecuteNQ($sqlDelete);

                        $sqlInsert2 = "Insert into tblPendingPayments(PendingAmount, AppointmentID, InvoiceID,DateTimeStamp,PaidAmount,Status,CustomerID,PendingStatus) values('" . $pendamt . "','" . $appointment_id . "', '" . $invoiceid . "','" . $date . "','" . $paidamt . "','1','" . $CustomerFullName . "','2')";
                        $DB->query($sqlInsert2);
                    } else {

                        $sqlUpdate3 = "UPDATE tblPendingPayments SET Status = '0',PendingStatus='1' WHERE CustomerID='" . $CustomerFullName . "'";
                        ExecuteNQ($sqlUpdate3);
                    }

                    $sqlInsert2 = "UPDATE tblInvoiceDetails SET OfferDiscountDateTime='" . $date . "' WHERE AppointmentId='" . $appointment_id . "'";
                    //$DB->query($sqlInsert1);
                    $DB->query($sqlInsert2);

                    $sqlUpdate2 = "UPDATE tblAppointmentsDetailsInvoice SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate2);

                    $sqlUpdate3 = "UPDATE tblAppointments SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate3);

                    $sqlDelete = "DELETE FROM tblAppointmentlog WHERE appointment_id='" . $appointment_id . "'";
                    ExecuteNQ($sqlDelete);

                    $sqlInsert1 = "Insert into tblAppointmentlog (appointment_id, invoice_name, appointment_date,store,status) values('" . $appointment_id . "','" . $customer . "', '" . $appoint_date . "','" . $stores . "','2')";
                    $DB->query($sqlInsert1);
                    ///////////////////////////////////////////////////////////////////////////////////
                } else if ($type == 'C') {
                    $sqp = select("CustomerID", "tblCustomers", "CustomerFullName='" . $CustomerFullName . "'");
                    $cust = $sqp[0]['CustomerID'];

                    if ($packagee != "") {
                        $sqlUpdate = "UPDATE tblInvoiceDetails SET Billaddress='" . $billaddress . "',InvoiceId='" . $invoiceid . "',CustomerID='" . $customer . "',CustomerFullName='" . $CustomerFullName . "',Email='" . $email . "',Mobile='" . $mobile . "',SubTotal='" . $sub_total . "',OfferAmt='" . $offeramtt . "',Total='" . $total . "',RoundTotal='" . $roundtotal . "',CardAmount='" . $roundtotal . "',TotalPayment='" . $totalpayment . "',ServiceCode='" . $servicecode1 . "',ServiceName='" . $serviceid1 . "',Qty='" . $qty1 . "',ServiceAmt='" . $serviceamt1 . "',MembershipName='" . $membershipname1 . "',Discount='" . $Discount1 . "',DisAmt='" . $disamt1 . "',ChargeName='" . $chargename1 . "',ChargeAmount='" . $chargeamount1 . "',Flag='" . $type . "',Membership_Amount='" . $membercost . "',PendingAmount='" . $totalpend . "',pending_amt_apt_id='" . $pending_amt_apt_id . "',PackageID='" . $packagee . "',PackageIDFlag='P' WHERE AppointmentId='" . $appointment_id . "'";
                    } else {
                        $sqlUpdate = "UPDATE tblInvoiceDetails SET Billaddress='" . $billaddress . "',InvoiceId='" . $invoiceid . "',CustomerID='" . $customer . "',CustomerFullName='" . $CustomerFullName . "',Email='" . $email . "',Mobile='" . $mobile . "',SubTotal='" . $sub_total . "',OfferAmt='" . $offeramtt . "',Total='" . $total . "',RoundTotal='" . $roundtotal . "',CardAmount='" . $roundtotal . "',TotalPayment='" . $totalpayment . "',ServiceCode='" . $servicecode1 . "',ServiceName='" . $serviceid1 . "',Qty='" . $qty1 . "',ServiceAmt='" . $serviceamt1 . "',MembershipName='" . $membershipname1 . "',Discount='" . $Discount1 . "',DisAmt='" . $disamt1 . "',ChargeName='" . $chargename1 . "',ChargeAmount='" . $chargeamount1 . "',Flag='" . $type . "',Membership_Amount='" . $membercost . "',PendingAmount='" . $totalpend . "',pending_amt_apt_id='" . $pending_amt_apt_id . "' WHERE AppointmentId='" . $appointment_id . "'";
                    }
                    //	ExecuteNQ($sqlUpdate);
                    if ($DB->query($sqlUpdate) === TRUE) {
                        $data = 2; //last id of tblCustomers insert
                    }
                    //////////////////////////////gift vouch/////////////
                    if ($purchaseid != "") {
                        $sqlUpdate2 = "UPDATE tblInvoiceDetails SET GVPurchasedID='" . $purchaseid . "' WHERE AppointmentId='" . $appointment_id . "'";
                        ExecuteNQ($sqlUpdate2);
                    } elseif ($purchaseid != "" && $Redemptid != "") {
                        $remainamt = $gftamt - $totalredamt;
                        $sqlUpdate3 = "UPDATE tblInvoiceDetails SET GVPurchasedID='" . $purchaseid . "',GVRedeemedID='" . $Redemptid . "' WHERE AppointmentId='" . $appointment_id . "'";
                        ExecuteNQ($sqlUpdate3);

                        $sqlUpdate4 = "UPDATE tblGiftVouchers SET RemainingGiftVoucherAmount='" . $remainamt . "' WHERE RedempedBy='" . $appointment_id . "'";
                        ExecuteNQ($sqlUpdate4);
                    } else {
                        $remainamt = $gftamt - $totalredamt;
                        $sqlUpdate3 = "UPDATE tblInvoiceDetails SET GVRedeemedID='" . $Redemptid . "' WHERE AppointmentId='" . $appointment_id . "'";
                        ExecuteNQ($sqlUpdate3);

                        $sqlUpdate4 = "UPDATE tblGiftVouchers SET RemainingGiftVoucherAmount='" . $remainamt . "' WHERE RedempedBy='" . $appointment_id . "'";
                        ExecuteNQ($sqlUpdate4);
                    }
                    //////////////////////////////
                    if ($pendamt != "0" && $pendamt != "") {
                        $sqlDelete = "DELETE FROM tblPendingPayments WHERE CustomerID='" . $CustomerFullName . "'";
                        ExecuteNQ($sqlDelete);

                        $sqlInsert2 = "Insert into tblPendingPayments(PendingAmount, AppointmentID, InvoiceID,DateTimeStamp,PaidAmount,Status,CustomerID,PendingStatus) values('" . $pendamt . "','" . $appointment_id . "', '" . $invoiceid . "','" . $date . "','" . $paidamt . "','1','" . $CustomerFullName . "','2')";
                        $DB->query($sqlInsert2);
                    } else {
                        $sqlUpdate3 = "UPDATE tblPendingPayments SET Status = '0',PendingStatus='1' WHERE CustomerID='" . $CustomerFullName . "'";
                        ExecuteNQ($sqlUpdate3);
                    }

                    $sqlInsert2 = "UPDATE tblInvoiceDetails SET OfferDiscountDateTime='" . $date . "' WHERE AppointmentId='" . $appointment_id . "'";
                    //$DB->query($sqlInsert1);
                    $DB->query($sqlInsert2);

                    $sqlUpdate2 = "UPDATE tblAppointmentsDetailsInvoice SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate2);


                    $sqlUpdate3 = "UPDATE tblAppointments SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";
                    ExecuteNQ($sqlUpdate3);



                    $sqlDelete = "DELETE FROM tblAppointmentlog WHERE appointment_id='" . $appointment_id . "'";
                    ExecuteNQ($sqlDelete);

                    $sqlInsert1 = "Insert into tblAppointmentlog (appointment_id, invoice_name, appointment_date,store,status) values('" . $appointment_id . "','" . $customer . "', '" . $appoint_date . "','" . $stores . "','2')";
                    $DB->query($sqlInsert1);
                    ///////////////////////////////////////////////////////////////////////////////////
                } else {

                }
            } else {
                $data = "This Invoice Details Already Saved";
            }
        }
    } else {

        if ($type == 'CS') {

            $sqp = select("CustomerID", "tblCustomers", "CustomerFullName='" . $CustomerFullName . "'");
            $cust = $sqp[0]['CustomerID'];


            if ($packagee != "") {
                $sqlInsert1 = "Insert into tblInvoiceDetails(AppointmentId, Billaddress, InvoiceId,CustomerID,CustomerFullName,Email,Mobile,SubTotal,OfferAmt,Total,RoundTotal,CashAmount,TotalPayment,Flag,Membership_Amount,OfferDiscountDateTime,PendingAmount,pending_amt_apt_id,PackageID,PackageIDFlag) values('" . $appointment_id . "','" . $billaddress . "', '" . $invoiceid . "','" . $customer . "','" . $CustomerFullName . "','" . $email . "','" . $mobile . "','" . $sub_total . "','" . $offeramtt . "','" . $total . "','" . $roundtotal . "','" . $roundtotal . "','" . $totalpayment . "','CS','" . $membercost . "','" . $date . "','" . $totalpend . "','" . $pending_amt_apt_id . "','" . $packagee . "','P')";
            } else {
                $sqlInsert1 = "Insert into tblInvoiceDetails(AppointmentId, Billaddress, InvoiceId,CustomerID,CustomerFullName,Email,Mobile,SubTotal,OfferAmt,Total,RoundTotal,CashAmount,TotalPayment,Flag,Membership_Amount,OfferDiscountDateTime,PendingAmount,pending_amt_apt_id) values('" . $appointment_id . "','" . $billaddress . "', '" . $invoiceid . "','" . $customer . "','" . $CustomerFullName . "','" . $email . "','" . $mobile . "','" . $sub_total . "','" . $offeramtt . "','" . $total . "','" . $roundtotal . "','" . $roundtotal . "','" . $totalpayment . "','CS','" . $membercost . "','" . $date . "','" . $totalpend . "','" . $pending_amt_apt_id . "')";
            }

            if ($DB->query($sqlInsert1) === TRUE) {
                $last_id = $DB->insert_id;  //last id of tblCustomers insert
            } else {
                echo "Error: " . $sqlInsert1 . "<br>" . $conn->error;
            }


            $sqlUpdatepp = "UPDATE tblCustomers SET memberflag='1' WHERE CustomerMobileNo='" . $mobile . "'";
            ExecuteNQ($sqlUpdatepp);

            $sqlUpdate = "UPDATE tblInvoiceDetails SET ServiceCode='" . $servicecode1 . "',ServiceName='" . $serviceid1 . "',Qty='" . $qty1 . "',ServiceAmt='" . $serviceamt1 . "',MembershipName='" . $membershipname1 . "',Discount='" . $Discount1 . "',DisAmt='" . $disamt1 . "',ChargeName='" . $chargename1 . "',ChargeAmount='" . $chargeamount1 . "' WHERE AppointmentId='" . $appointment_id . "'";

            if ($DB->query($sqlUpdate) === TRUE) {
                $data = 2; //last id of tblCustomers insert
            }
            //////////////////////////////////////gift vouch///////////////
            if ($purchaseid != "") {
                $sqlUpdate2 = "UPDATE tblInvoiceDetails SET GVPurchasedID='" . $purchaseid . "' WHERE AppointmentId='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate2);
            } elseif ($purchaseid != "" && $Redemptid != "") {
                $remainamt = $gftamt - $totalredamt;
                $sqlUpdate3 = "UPDATE tblInvoiceDetails SET GVPurchasedID='" . $purchaseid . "',GVRedeemedID='" . $Redemptid . "' WHERE AppointmentId='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate3);
                $sqlUpdate4 = "UPDATE tblGiftVouchers SET RemainingGiftVoucherAmount='" . $remainamt . "' WHERE RedempedBy='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate4);
            } else {
                $remainamt = $gftamt - $totalredamt;
                $sqlUpdate3 = "UPDATE tblInvoiceDetails SET GVRedeemedID='" . $Redemptid . "' WHERE AppointmentId='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate3);
                $sqlUpdate4 = "UPDATE tblGiftVouchers SET RemainingGiftVoucherAmount='" . $remainamt . "' WHERE RedempedBy='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate4);
            }
            //////////////////////////////////////////////
            if ($pendamt != "0" && $pendamt != "") {
                $sqlDelete = "DELETE FROM tblPendingPayments WHERE CustomerID='" . $CustomerFullName . "'";
                ExecuteNQ($sqlDelete);

                $sqlInsert2 = "Insert into tblPendingPayments(PendingAmount, AppointmentID, InvoiceID,DateTimeStamp,PaidAmount,Status,CustomerID,PendingStatus) values('" . $pendamt . "','" . $appointment_id . "', '" . $invoiceid . "','" . $date . "','" . $paidamt . "','1','" . $CustomerFullName . "','2')";
                $DB->query($sqlInsert2);
            } else {


                $sqlUpdate3 = "UPDATE tblPendingPayments SET Status = '0',PendingStatus='1' WHERE CustomerID='" . $CustomerFullName . "'";

                ExecuteNQ($sqlUpdate3);
            }

            $sqlDelete = "DELETE FROM tblAppointmentMembershipDiscount WHERE AppointmentID='" . $appointment_id . "'";
            ExecuteNQ($sqlDelete);
            for ($i = 0; $i < count($serviceido); $i++) {
                if ($serviceido[$i] != "") {
                    //$sqlDelete = "DELETE FROM tblAppointmentMembershipDiscount WHERE AppointmentID='" . $appointment_id . "' and MembershipID!='0'";
                    $sqlInsert4 = "Insert into tblAppointmentMembershipDiscount(AppointmentID, Id, ServiceID,OfferID,OfferAmount,DateTimeStamp,MembershipAmount) values('" . $appointment_id . "','" . $last_id . "', '" . $serviceido[$i] . "','" . $offerid[$i] . "','" . $offeramt[$i] . "','" . $date . "','0')";
                    //$sqlInsert4 = "Insert into tblAppointmentMembershipDiscount(AppointmentID,Id, ServiceID,OfferID,OfferAmount,MembershipID,MembershipAmount,DateTimeStamp) values('" . $appointment_id . "','" . $last_id . "', '" . $serviceido[$i] . "','" . $offerid[$i] . "','" . $offeramt[$i] . "','" . $memid[$i] . "','" . $discountm[$i] . "','" . $date . "')";

                    if ($DB->query($sqlInsert4) === TRUE) {
                        $last_idd = $DB->insert_id;  //last id of tblCustomers insert
                    } else {
                        echo "Error: " . $sqlInsert4 . "<br>" . $conn->error;
                    }
                } else {

                }
            }

            for ($i = 0; $i < count($serviceidm); $i++) {
                if ($serviceidm[$i] != "") {
                    //$sqlDelete = "DELETE FROM tblAppointmentMembershipDiscount WHERE AppointmentID='" . $appointment_id . "' and OfferID!='0'";
                    $sqlInsert4 = "Insert into tblAppointmentMembershipDiscount(AppointmentID, Id, ServiceID,MembershipID,MembershipAmount,DateTimeStamp,OfferAmount) values('" . $appointment_id . "','" . $last_id . "', '" . $serviceidm[$i] . "','" . $memid[$i] . "','" . $discountm[$i] . "','" . $date . "','0')";
                    //$sqlInsert4 = "Insert into tblAppointmentMembershipDiscount(AppointmentID,Id, ServiceID,OfferID,OfferAmount,MembershipID,MembershipAmount,DateTimeStamp) values('" . $appointment_id . "','" . $last_id . "', '" . $serviceidm[$i] . "','" . $offerid[$i] . "','" . $offeramt[$i] . "','" . $memid[$i] . "','" . $discountm[$i] . "','" . $date . "')";

                    if ($DB->query($sqlInsert4) === TRUE) {
                        $last_idd = $DB->insert_id;  //last id of tblCustomers insert
                    } else {
                        echo "Error: " . $sqlInsert4 . "<br>" . $conn->error;
                    }
                }
            }
            $sqlUpdate2 = "UPDATE tblAppointmentsDetailsInvoice SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";
            ExecuteNQ($sqlUpdate2);

            $sqlUpdate3 = "UPDATE tblAppointments SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";
            ExecuteNQ($sqlUpdate3);

            $sqlDelete = "DELETE FROM tblAppointmentlog WHERE appointment_id='" . $appointment_id . "'";
            ExecuteNQ($sqlDelete);

            $sqlInsert1 = "Insert into tblAppointmentlog (appointment_id, invoice_name, appointment_date,store,status) values('" . $appointment_id . "','" . $customer . "', '" . $appoint_date . "','" . $stores . "','2')";
            $DB->query($sqlInsert1);
            ///////////////////////////////////////////////////////////////////////////////////
        } elseif ($type == 'H') {
            $sqp = select("CustomerID", "tblCustomers", "CustomerFullName='" . $CustomerFullName . "'");
            $cust = $sqp[0]['CustomerID'];
            if ($packagee != "") {
                $sqlInsert1 = "Insert into tblInvoiceDetails(AppointmentId, Billaddress, InvoiceId,CustomerID,CustomerFullName,Email,Mobile,SubTotal,OfferAmt,Total,RoundTotal,CashAmount,TotalPayment,Flag,Membership_Amount,OfferDiscountDateTime,PendingAmount,pending_amt_apt_id,PackageID,PackageIDFlag) values('" . $appointment_id . "','" . $billaddress . "', '" . $invoiceid . "','" . $customer . "','" . $CustomerFullName . "','" . $email . "','" . $mobile . "','" . $sub_total . "','" . $offeramtt . "','" . $total . "','" . $roundtotal . "','" . $roundtotal . "','" . $totalpayment . "','H','" . $membercost . "','" . $date . "','" . $totalpend . "','" . $pending_amt_apt_id . "','" . $packagee . "','P')";
            } else {
                $sqlInsert1 = "Insert into tblInvoiceDetails(AppointmentId, Billaddress, InvoiceId,CustomerID,CustomerFullName,Email,Mobile,SubTotal,OfferAmt,Total,RoundTotal,CashAmount,TotalPayment,Flag,Membership_Amount,OfferDiscountDateTime,PendingAmount,pending_amt_apt_id) values('" . $appointment_id . "','" . $billaddress . "', '" . $invoiceid . "','" . $customer . "','" . $CustomerFullName . "','" . $email . "','" . $mobile . "','" . $sub_total . "','" . $offeramtt . "','" . $total . "','" . $roundtotal . "','" . $roundtotal . "','" . $totalpayment . "','H','" . $membercost . "','" . $date . "','" . $totalpend . "','" . $pending_amt_apt_id . "')";
            }
            if ($DB->query($sqlInsert1) === TRUE) {
                $last_id = $DB->insert_id;  //last id of tblCustomers insert
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }


            $idp = '3' . "," . $appointment_id;
            $sqlUpdate2 = "UPDATE tblCustomers SET memberflag='" . $idp . "' WHERE CustomerMobileNo='" . $mobile . "'";
            ExecuteNQ($sqlUpdate2);

            $sqlUpdate = "UPDATE tblInvoiceDetails SET ServiceCode='" . $servicecode1 . "',ServiceName='" . $serviceid1 . "',Qty='" . $qty1 . "',ServiceAmt='" . $serviceamt1 . "',MembershipName='" . $membershipname1 . "',Discount='" . $Discount1 . "',DisAmt='" . $disamt1 . "',ChargeName='" . $chargename1 . "',ChargeAmount='" . $chargeamount1 . "' WHERE AppointmentId='" . $appointment_id . "'";
            //	ExecuteNQ($sqlUpdate);
            if ($DB->query($sqlUpdate) === TRUE) {
                $data = 2; //last id of tblCustomers insert
            }
            ////////////////////////////////////gift vouch///////////////////////////
            if ($purchaseid != "") {
                $sqlUpdate2 = "UPDATE tblInvoiceDetails SET GVPurchasedID='" . $purchaseid . "' WHERE AppointmentId='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate2);
            } elseif ($purchaseid != "" && $Redemptid != "") {
                $remainamt = $gftamt - $totalredamt;
                $sqlUpdate3 = "UPDATE tblInvoiceDetails SET GVPurchasedID='" . $purchaseid . "',GVRedeemedID='" . $Redemptid . "' WHERE AppointmentId='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate3);
                $sqlUpdate4 = "UPDATE tblGiftVouchers SET RemainingGiftVoucherAmount='" . $remainamt . "' WHERE RedempedBy='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate4);
            } else {
                $remainamt = $gftamt - $totalredamt;
                $sqlUpdate3 = "UPDATE tblInvoiceDetails SET GVRedeemedID='" . $Redemptid . "' WHERE AppointmentId='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate3);
                $sqlUpdate4 = "UPDATE tblGiftVouchers SET RemainingGiftVoucherAmount='" . $remainamt . "' WHERE RedempedBy='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate4);
            }
            /////////////////////////////////////////////////////////////////////

            if ($pendamt != "") {
                $sqlDelete = "DELETE FROM tblPendingPayments WHERE CustomerID='" . $CustomerFullName . "'";
                ExecuteNQ($sqlDelete);

                $sep = select("PendingAmount", "tblPendingPayments", "CustomerID='" . $CustomerFullName . "'");
                foreach ($sep as $avp) {
                    $pendamtt = $avp['PendingAmount'];
                    $totalpend = $totalpend + $pendamtt;
                }
                $pend = $totalpend + $pendamt;
                $sqlInsert2 = "Insert into tblPendingPayments(PendingAmount, AppointmentID, InvoiceID,DateTimeStamp,PaidAmount,Status,CustomerID,PendingStatus) values('" . $pendamt . "','" . $appointment_id . "', '" . $invoiceid . "','" . $date . "','" . $paidamt . "','0','" . $CustomerFullName . "','2')";
                $DB->query($sqlInsert2);
            } else {
                $sqlDelete = "DELETE FROM tblPendingPayments WHERE CustomerID='" . $CustomerFullName . "'";
                ExecuteNQ($sqlDelete);
                $sep = select("PendingAmount", "tblPendingPayments", "CustomerID='" . $CustomerFullName . "'");
                foreach ($sep as $avp) {
                    $pendamtt = $avp['PendingAmount'];
                    $totalpend = $totalpend + $pendamtt;
                }
                $pend = $totalpend + $totalpayment;
                $apoin_data = select("*", "tblAppointments", "AppointmentID='" . $appointment_id . "'");
                if (isset($apoin_data) && is_array($apoin_data) && count($apoin_data) > 0) {
                    if (isset($apoin_data[0]['prepaid_amount']) && $apoin_data[0]['prepaid_amount'] > 0) {
                        $totalpayment = $totalpayment - $apoin_data[0]['prepaid_amount'];
                        if ($totalpayment < 0) {
                            $totalpayment = 0;
                        }
                    }
                }
                $sqlInsert2 = "Insert into tblPendingPayments(PendingAmount, AppointmentID, InvoiceID,DateTimeStamp,PaidAmount,Status,CustomerID,PendingStatus) values('" . $totalpayment . "','" . $appointment_id . "', '" . $invoiceid . "','" . $date . "','0','1','" . $CustomerFullName . "','2')";
                $DB->query($sqlInsert2);
            }
            $sqlDelete = "DELETE FROM tblAppointmentMembershipDiscount WHERE AppointmentID='" . $appointment_id . "'";
            ExecuteNQ($sqlDelete);
            for ($i = 0; $i < count($serviceido); $i++) {
                if ($serviceido[$i] != "") {
                    //$sqlDelete = "DELETE FROM tblAppointmentMembershipDiscount WHERE AppointmentID='" . $appointment_id . "' and MembershipID!='0'";
                    $sqlInsert4 = "Insert into tblAppointmentMembershipDiscount(AppointmentID, Id, ServiceID,OfferID,OfferAmount,DateTimeStamp,MembershipAmount) values('" . $appointment_id . "','" . $last_id . "', '" . $serviceido[$i] . "','" . $offerid[$i] . "','" . $offeramt[$i] . "','" . $date . "','0')";
                    //$sqlInsert4 = "Insert into tblAppointmentMembershipDiscount(AppointmentID,Id, ServiceID,OfferID,OfferAmount,MembershipID,MembershipAmount,DateTimeStamp) values('" . $appointment_id . "','" . $last_id . "', '" . $serviceido[$i] . "','" . $offerid[$i] . "','" . $offeramt[$i] . "','" . $memid[$i] . "','" . $discountm[$i] . "','" . $date . "')";

                    if ($DB->query($sqlInsert4) === TRUE) {
                        $last_idd = $DB->insert_id;  //last id of tblCustomers insert
                    } else {
                        echo "Error: " . $sqlInsert4 . "<br>" . $conn->error;
                    }
                } else {

                }
            }

            for ($i = 0; $i < count($serviceidm); $i++) {
                if ($serviceidm[$i] != "") {
                    //$sqlDelete = "DELETE FROM tblAppointmentMembershipDiscount WHERE AppointmentID='" . $appointment_id . "' and OfferID!='0'";
                    $sqlInsert4 = "Insert into tblAppointmentMembershipDiscount(AppointmentID, Id, ServiceID,MembershipID,MembershipAmount,DateTimeStamp,OfferAmount) values('" . $appointment_id . "','" . $last_id . "', '" . $serviceidm[$i] . "','" . $memid[$i] . "','" . $discountm[$i] . "','" . $date . "','0')";
                    //$sqlInsert4 = "Insert into tblAppointmentMembershipDiscount(AppointmentID,Id, ServiceID,OfferID,OfferAmount,MembershipID,MembershipAmount,DateTimeStamp) values('" . $appointment_id . "','" . $last_id . "', '" . $serviceidm[$i] . "','" . $offerid[$i] . "','" . $offeramt[$i] . "','" . $memid[$i] . "','" . $discountm[$i] . "','" . $date . "')";

                    if ($DB->query($sqlInsert4) === TRUE) {
                        $last_idd = $DB->insert_id;  //last id of tblCustomers insert
                    } else {
                        echo "Error: " . $sqlInsert4 . "<br>" . $conn->error;
                    }
                }
            }
            $sqlUpdate2 = "UPDATE tblAppointmentsDetailsInvoice SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";
            ExecuteNQ($sqlUpdate2);
            $sqlUpdate3 = "UPDATE tblAppointments SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";
            ExecuteNQ($sqlUpdate3);


            $sqlDelete = "DELETE FROM tblAppointmentlog WHERE appointment_id='" . $appointment_id . "'";
            ExecuteNQ($sqlDelete);

            $sqlInsert1 = "Insert into tblAppointmentlog (appointment_id, invoice_name, appointment_date,store,status) values('" . $appointment_id . "','" . $customer . "', '" . $appoint_date . "','" . $stores . "','2')";
            $DB->query($sqlInsert1);
            ///////////////////////////////////////////////////////////////////////////////////
        } elseif ($type == 'C') {
            $sqp = select("CustomerID", "tblCustomers", "CustomerFullName='" . $CustomerFullName . "'");
            $cust = $sqp[0]['CustomerID'];
            if ($packagee != "") {
                $sqlInsert1 = "Insert into tblInvoiceDetails(AppointmentId, Billaddress, InvoiceId,CustomerID,CustomerFullName,Email,Mobile,SubTotal,OfferAmt,Total,RoundTotal,CardAmount,TotalPayment,Flag,Membership_Amount,OfferDiscountDateTime,PendingAmount,pending_amt_apt_id,PackageID,PackageIDFlag) values('" . $appointment_id . "','" . $billaddress . "', '" . $invoiceid . "','" . $customer . "','" . $CustomerFullName . "','" . $email . "','" . $mobile . "','" . $sub_total . "','" . $offeramtt . "','" . $total . "','" . $roundtotal . "','" . $roundtotal . "','" . $totalpayment . "','C','" . $membercost . "','" . $date . "','" . $totalpend . "','" . $pending_amt_apt_id . "','" . $packagee . "','P')";
            } else {
                $sqlInsert1 = "Insert into tblInvoiceDetails(AppointmentId, Billaddress, InvoiceId,CustomerID,CustomerFullName,Email,Mobile,SubTotal,OfferAmt,Total,RoundTotal,CardAmount,TotalPayment,Flag,Membership_Amount,OfferDiscountDateTime,PendingAmount,pending_amt_apt_id) values('" . $appointment_id . "','" . $billaddress . "', '" . $invoiceid . "','" . $customer . "','" . $CustomerFullName . "','" . $email . "','" . $mobile . "','" . $sub_total . "','" . $offeramtt . "','" . $total . "','" . $roundtotal . "','" . $roundtotal . "','" . $totalpayment . "','C','" . $membercost . "','" . $date . "','" . $totalpend . "','" . $pending_amt_apt_id . "')";
            }




            //$DB->query($sqlInsert1);
            if ($DB->query($sqlInsert1) === TRUE) {
                $last_id = $DB->insert_id;  //last id of tblCustomers insert
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }


            $sqlUpdate = "UPDATE tblInvoiceDetails SET ServiceCode='" . $servicecode1 . "',ServiceName='" . $serviceid1 . "',Qty='" . $qty1 . "',ServiceAmt='" . $serviceamt1 . "',MembershipName='" . $membershipname1 . "',Discount='" . $Discount1 . "',DisAmt='" . $disamt1 . "',ChargeName='" . $chargename1 . "',ChargeAmount='" . $chargeamount1 . "' WHERE AppointmentId='" . $appointment_id . "'";
            //	ExecuteNQ($sqlUpdate);
            if ($DB->query($sqlUpdate) === TRUE) {
                $data = 2; //last id of tblCustomers insert
            }
            /////////////////gift vouch////////////
            if ($purchaseid != "") {
                $sqlUpdate2 = "UPDATE tblInvoiceDetails SET GVPurchasedID='" . $purchaseid . "' WHERE AppointmentId='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate2);
            } elseif ($purchaseid != "" && $Redemptid != "") {
                $remainamt = $gftamt - $totalredamt;
                $sqlUpdate3 = "UPDATE tblInvoiceDetails SET GVPurchasedID='" . $purchaseid . "',GVRedeemedID='" . $Redemptid . "' WHERE AppointmentId='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate3);
                $sqlUpdate4 = "UPDATE tblGiftVouchers SET RemainingGiftVoucherAmount='" . $remainamt . "' WHERE RedempedBy='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate4);
            } else {
                $remainamt = $gftamt - $totalredamt;
                $sqlUpdate3 = "UPDATE tblInvoiceDetails SET GVRedeemedID='" . $Redemptid . "' WHERE AppointmentId='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate3);
                $sqlUpdate4 = "UPDATE tblGiftVouchers SET RemainingGiftVoucherAmount='" . $remainamt . "' WHERE RedempedBy='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate4);
            }
            //////////////////////////////
            $sqlUpdate2 = "UPDATE tblCustomers SET memberflag='2' WHERE CustomerMobileNo='" . $mobile . "'";
            ExecuteNQ($sqlUpdate2);


            $sep = select("PendingAmount", "tblPendingPayments", "CustomerID='" . $CustomerFullName . "'");
            foreach ($sep as $avp) {
                $pendamtt = $avp['PendingAmount'];
                $totalpend = $totalpend + $pendamtt;
            }

            $pend = $totalpend + $pendamt;
            if ($pendamt != "0" && $pendamt != "") {
                $sqlDelete = "DELETE FROM tblPendingPayments WHERE CustomerID='" . $CustomerFullName . "'";
                ExecuteNQ($sqlDelete);
                $sqlInsert2 = "Insert into tblPendingPayments(PendingAmount, AppointmentID, InvoiceID,DateTimeStamp,PaidAmount,Status,CustomerID,PendingStatus) values('" . $pendamt . "','" . $appointment_id . "', '" . $invoiceid . "','" . $date . "','" . $paidamt . "','1','" . $CustomerFullName . "','2')";
                $DB->query($sqlInsert2);
            } else {
                $sqlUpdate3 = "UPDATE tblPendingPayments SET Status = '0',PendingStatus='1' WHERE CustomerID='" . $CustomerFullName . "'";
                ExecuteNQ($sqlUpdate3);
            }

            $sqlDelete = "DELETE FROM tblAppointmentMembershipDiscount WHERE AppointmentID='" . $appointment_id . "'";
            ExecuteNQ($sqlDelete);
            for ($i = 0; $i < count($serviceido); $i++) {
                if ($serviceido[$i] != "") {
                    //$sqlDelete = "DELETE FROM tblAppointmentMembershipDiscount WHERE AppointmentID='" . $appointment_id . "' and MembershipID!='0'";
                    $sqlInsert4 = "Insert into tblAppointmentMembershipDiscount(AppointmentID, Id, ServiceID,OfferID,OfferAmount,DateTimeStamp,MembershipAmount) values('" . $appointment_id . "','" . $last_id . "', '" . $serviceido[$i] . "','" . $offerid[$i] . "','" . $offeramt[$i] . "','" . $date . "','0')";
                    //$sqlInsert4 = "Insert into tblAppointmentMembershipDiscount(AppointmentID,Id, ServiceID,OfferID,OfferAmount,MembershipID,MembershipAmount,DateTimeStamp) values('" . $appointment_id . "','" . $last_id . "', '" . $serviceido[$i] . "','" . $offerid[$i] . "','" . $offeramt[$i] . "','" . $memid[$i] . "','" . $discountm[$i] . "','" . $date . "')";

                    if ($DB->query($sqlInsert4) === TRUE) {
                        $last_idd = $DB->insert_id;  //last id of tblCustomers insert
                    } else {
                        echo "Error: " . $sqlInsert4 . "<br>" . $conn->error;
                    }
                } else {

                }
            }

            for ($i = 0; $i < count($serviceidm); $i++) {
                if ($serviceidm[$i] != "") {
                    //$sqlDelete = "DELETE FROM tblAppointmentMembershipDiscount WHERE AppointmentID='" . $appointment_id . "' and OfferID!='0'";
                    $sqlInsert4 = "Insert into tblAppointmentMembershipDiscount(AppointmentID, Id, ServiceID,MembershipID,MembershipAmount,DateTimeStamp,OfferAmount) values('" . $appointment_id . "','" . $last_id . "', '" . $serviceidm[$i] . "','" . $memid[$i] . "','" . $discountm[$i] . "','" . $date . "','0')";
                    //$sqlInsert4 = "Insert into tblAppointmentMembershipDiscount(AppointmentID,Id, ServiceID,OfferID,OfferAmount,MembershipID,MembershipAmount,DateTimeStamp) values('" . $appointment_id . "','" . $last_id . "', '" . $serviceidm[$i] . "','" . $offerid[$i] . "','" . $offeramt[$i] . "','" . $memid[$i] . "','" . $discountm[$i] . "','" . $date . "')";

                    if ($DB->query($sqlInsert4) === TRUE) {
                        $last_idd = $DB->insert_id;  //last id of tblCustomers insert
                    } else {
                        echo "Error: " . $sqlInsert4 . "<br>" . $conn->error;
                    }
                }
            }
            $sqlUpdate2 = "UPDATE tblAppointmentsDetailsInvoice SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";
            ExecuteNQ($sqlUpdate2);

            $sqlUpdate3 = "UPDATE tblAppointments SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";
            ExecuteNQ($sqlUpdate3);

            $sqlDelete = "DELETE FROM tblAppointmentlog WHERE appointment_id='" . $appointment_id . "'";
            ExecuteNQ($sqlDelete);

            $sqlInsert1 = "Insert into tblAppointmentlog (appointment_id, invoice_name, appointment_date,store,status) values('" . $appointment_id . "','" . $customer . "', '" . $appoint_date . "','" . $stores . "','2')";
            $DB->query($sqlInsert1);
            ///////////////////////////////////////////////////////////////////////////////////
        } elseif ($type == 'BOTH') {
            $sqp = select("CustomerID", "tblCustomers", "CustomerFullName='" . $CustomerFullName . "'");
            $cust = $sqp[0]['CustomerID'];
            if ($packagee != "") {
                $sqlInsert1 = "Insert into tblInvoiceDetails(AppointmentId, Billaddress, InvoiceId,CustomerID,CustomerFullName,Email,Mobile,SubTotal,OfferAmt,Total,RoundTotal,CashAmount,TotalPayment,Flag,CardAmount,Membership_Amount,OfferDiscountDateTime,PendingAmount,pending_amt_apt_id,PackageID,PackageIDFlag) values('" . $appointment_id . "','" . $billaddress . "', '" . $invoiceid . "','" . $customer . "','" . $CustomerFullName . "','" . $email . "','" . $mobile . "','" . $sub_total . "','" . $offeramtt . "','" . $total . "','" . $roundtotal . "','" . $cashboth . "','" . $totalpayment . "','BOTH','" . $cardboth . "','" . $membercost . "','" . $date . "','" . $totalpend . "','" . $pending_amt_apt_id . "','" . $packagee . "','P')";
            } else {
                $sqlInsert1 = "Insert into tblInvoiceDetails(AppointmentId, Billaddress, InvoiceId,CustomerID,CustomerFullName,Email,Mobile,SubTotal,OfferAmt,Total,RoundTotal,CashAmount,TotalPayment,Flag,CardAmount,Membership_Amount,OfferDiscountDateTime,PendingAmount,pending_amt_apt_id) values('" . $appointment_id . "','" . $billaddress . "', '" . $invoiceid . "','" . $customer . "','" . $CustomerFullName . "','" . $email . "','" . $mobile . "','" . $sub_total . "','" . $offeramtt . "','" . $total . "','" . $roundtotal . "','" . $cashboth . "','" . $totalpayment . "','BOTH','" . $cardboth . "','" . $membercost . "','" . $date . "','" . $totalpend . "','" . $pending_amt_apt_id . "')";
            }
            if ($DB->query($sqlInsert1) === TRUE) {
                $last_id = $DB->insert_id;  //last id of tblCustomers insert
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }

            $sqlUpdate = "UPDATE tblInvoiceDetails SET ServiceCode='" . $servicecode1 . "',ServiceName='" . $serviceid1 . "',Qty='" . $qty1 . "',ServiceAmt='" . $serviceamt1 . "',MembershipName='" . $membershipname1 . "',Discount='" . $Discount1 . "',DisAmt='" . $disamt1 . "',ChargeName='" . $chargename1 . "',ChargeAmount='" . $chargeamount1 . "' WHERE AppointmentId='" . $appointment_id . "'";
            //	ExecuteNQ($sqlUpdate);
            if ($DB->query($sqlUpdate) === TRUE) {
                $data = 2; //last id of tblCustomers insert
            }
            ////////////gift vouch/////////////////
            if ($purchaseid != "") {
                $sqlUpdate2 = "UPDATE tblInvoiceDetails SET GVPurchasedID='" . $purchaseid . "' WHERE AppointmentId='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate2);
            } elseif ($purchaseid != "" && $Redemptid != "") {
                $remainamt = $gftamt - $totalredamt;
                $sqlUpdate3 = "UPDATE tblInvoiceDetails SET GVPurchasedID='" . $purchaseid . "',GVRedeemedID='" . $Redemptid . "' WHERE AppointmentId='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate3);

                $sqlUpdate4 = "UPDATE tblGiftVouchers SET RemainingGiftVoucherAmount='" . $remainamt . "' WHERE RedempedBy='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate4);
            } else {
                $remainamt = $gftamt - $totalredamt;
                $sqlUpdate3 = "UPDATE tblInvoiceDetails SET GVRedeemedID='" . $Redemptid . "' WHERE AppointmentId='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate3);

                $sqlUpdate4 = "UPDATE tblGiftVouchers SET RemainingGiftVoucherAmount='" . $remainamt . "' WHERE RedempedBy='" . $appointment_id . "'";
                ExecuteNQ($sqlUpdate4);
            }
            ///////////////////////////////////////////////
            $sqlUpdate2 = "UPDATE tblCustomers SET memberflag='4' WHERE CustomerMobileNo='" . $mobile . "'";
            ExecuteNQ($sqlUpdate2);

            $sep = select("PendingAmount", "tblPendingPayments", "CustomerID='" . $CustomerFullName . "'");
            foreach ($sep as $avp) {
                $pendamtt = $avp['PendingAmount'];
                $totalpend = $totalpend + $pendamtt;
            }
            $pend = $totalpend + $pendamt;

            if ($pendamt != "0" && $pendamt != "") {

                $sqlDelete = "DELETE FROM tblPendingPayments WHERE CustomerID='" . $CustomerFullName . "'";
                ExecuteNQ($sqlDelete);
                $sqlInsert2 = "Insert into tblPendingPayments(PendingAmount, AppointmentID, InvoiceID,DateTimeStamp,PaidAmount,Status,CustomerID,PendingStatus) values('" . $pendamt . "','" . $appointment_id . "', '" . $invoiceid . "','" . $date . "','" . $paidamt . "','1','" . $CustomerFullName . "','2')";
                $DB->query($sqlInsert2);
            } else {

                $sqlUpdate3 = "UPDATE tblPendingPayments SET Status = '0',PendingStatus='1' WHERE CustomerID='" . $CustomerFullName . "'";
                ExecuteNQ($sqlUpdate3);
            }

            $sqlDelete = "DELETE FROM tblAppointmentMembershipDiscount WHERE AppointmentID='" . $appointment_id . "'";
            ExecuteNQ($sqlDelete);
            for ($i = 0; $i < count($serviceido); $i++) {
                if ($serviceido[$i] != "") {
                    //$sqlDelete = "DELETE FROM tblAppointmentMembershipDiscount WHERE AppointmentID='" . $appointment_id . "' and MembershipID!='0'";
                    $sqlInsert4 = "Insert into tblAppointmentMembershipDiscount(AppointmentID, Id, ServiceID,OfferID,OfferAmount,DateTimeStamp,MembershipAmount) values('" . $appointment_id . "','" . $last_id . "', '" . $serviceido[$i] . "','" . $offerid[$i] . "','" . $offeramt[$i] . "','" . $date . "','0')";
                    //$sqlInsert4 = "Insert into tblAppointmentMembershipDiscount(AppointmentID,Id, ServiceID,OfferID,OfferAmount,MembershipID,MembershipAmount,DateTimeStamp) values('" . $appointment_id . "','" . $last_id . "', '" . $serviceido[$i] . "','" . $offerid[$i] . "','" . $offeramt[$i] . "','" . $memid[$i] . "','" . $discountm[$i] . "','" . $date . "')";

                    if ($DB->query($sqlInsert4) === TRUE) {
                        $last_idd = $DB->insert_id;  //last id of tblCustomers insert
                    } else {
                        echo "Error: " . $sqlInsert4 . "<br>" . $conn->error;
                    }
                } else {

                }
            }

            for ($i = 0; $i < count($serviceidm); $i++) {
                if ($serviceidm[$i] != "") {
                    //$sqlDelete = "DELETE FROM tblAppointmentMembershipDiscount WHERE AppointmentID='" . $appointment_id . "' and OfferID!='0'";
                    $sqlInsert4 = "Insert into tblAppointmentMembershipDiscount(AppointmentID,Id, ServiceID,OfferID,OfferAmount,MembershipID,MembershipAmount,DateTimeStamp) values('" . $appointment_id . "','" . $last_id . "', '" . $serviceidm[$i] . "','" . $offerid[$i] . "','" . $offeramt[$i] . "','" . $memid[$i] . "','" . $discountm[$i] . "','" . $date . "')";
                    //$sqlInsert4 = "Insert into tblAppointmentMembershipDiscount(AppointmentID,Id, ServiceID,OfferID,OfferAmount,MembershipID,MembershipAmount,DateTimeStamp) values('" . $appointment_id . "','" . $last_id . "', '" . $serviceidm[$i] . "','" . $offerid[$i] . "','" . $offeramt[$i] . "','" . $memid[$i] . "','" . $discountm[$i] . "','" . $date . "')";

                    if ($DB->query($sqlInsert4) === TRUE) {
                        $last_idd = $DB->insert_id;  //last id of tblCustomers insert
                    } else {
                        echo "Error: " . $sqlInsert4 . "<br>" . $conn->error;
                    }
                }
            }

            $sqlUpdate2 = "UPDATE tblAppointmentsDetailsInvoice SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";
            ExecuteNQ($sqlUpdate2);


            $sqlUpdate3 = "UPDATE tblAppointments SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";
            ExecuteNQ($sqlUpdate3);




            $sqlDelete = "DELETE FROM tblAppointmentlog WHERE appointment_id='" . $appointment_id . "'";
            ExecuteNQ($sqlDelete);

            $sqlInsert1 = "Insert into tblAppointmentlog (appointment_id, invoice_name, appointment_date,store,status) values('" . $appointment_id . "','" . $customer . "', '" . $appoint_date . "','" . $stores . "','2')";
            $DB->query($sqlInsert1);
            ///////////////////////////////////////////////////////////////////////////////////
        } elseif ($type == 'CompleteAmt') {

            $sqp = select("CustomerID", "tblCustomers", "CustomerFullName='" . $CustomerFullName . "'");
            $cust = $sqp[0]['CustomerID'];

            $sqlInsert1 = "Insert into tblFreeServices(AppointmentId, Billaddress, InvoiceId,CustomerID,CustomerFullName,Email,Mobile,SubTotal,OfferAmt,Total,RoundTotal,CashAmount,TotalPayment,Flag,Membership_Amount,OfferDiscountDateTime,PendingAmount,AdminID) values('" . $appointment_id . "','" . $billaddress . "', '" . $invoiceid . "','" . $customer . "','" . $CustomerFullName . "','" . $email . "','" . $mobile . "','" . $sub_total . "','" . $offeramtt . "','" . $total . "','" . $roundtotal . "','" . $roundtotal . "','" . $totalpayment . "','Complete','" . $membercost . "','" . $date . "','" . $totalpend . "','" . $strAdminID . "')";

            if ($DB->query($sqlInsert1) === TRUE) {
                $last_id = $DB->insert_id;  //last id of tblCustomers insert
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }




            $sqlUpdate = "UPDATE tblFreeServices SET ServiceCode='" . $servicecode1 . "',ServiceName='" . $serviceid1 . "',Qty='" . $qty1 . "',ServiceAmt='" . $serviceamt1 . "',MembershipName='" . $membershipname1 . "',Discount='" . $Discount1 . "',DisAmt='" . $disamt1 . "',ChargeName='" . $chargename1 . "',ChargeAmount='" . $chargeamount1 . "' WHERE AppointmentId='" . $appointment_id . "'";

            if ($DB->query($sqlUpdate) === TRUE) {
                $data = 2; //last id of tblCustomers insert
            }

            $sqlUpdate2 = "UPDATE tblAppointmentsDetailsInvoice SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";

            ExecuteNQ($sqlUpdate2);


            $sqlUpdate3 = "UPDATE tblAppointments SET Status = '2' WHERE AppointmentID='" . $appointment_id . "'";

            ExecuteNQ($sqlUpdate3);

            $sqlDelete = "DELETE FROM tblAppointmentlog WHERE appointment_id='" . $appointment_id . "'";
            ExecuteNQ($sqlDelete);

            $sqlInsert1 = "Insert into tblAppointmentlog (appointment_id, invoice_name, appointment_date,store,status) values('" . $appointment_id . "','" . $customer . "', '" . $appoint_date . "','" . $stores . "','2')";
            $DB->query($sqlInsert1);
            ///////////////////////////////////////////////////////////////////////////////////
        }
    }



    /*
     * Send Feedback SMS and Email
     */
    $appointment_id = $_POST['appointment_idd'];
    $seldata = select("CustomerID,StoreID,AppointmentDate", "tblAppointments", "AppointmentID='" . $appointment_id . "'");
    $customerId = $seldata[0]['CustomerID'];

    $customer_data = select("*", "tblCustomers", "CustomerID='" . $customerId . "'");

    /*
     * get feedback sms and email template
     */
    if (isset($customer_data) && is_array($customer_data) && count($customer_data) > 0) {
        $customer_email = $customer_data[0]['CustomerEmailID'];
        $customer_phone = $customer_data[0]['CustomerMobileNo'];
        if (filter_var($customer_email, FILTER_VALIDATE_EMAIL)) {
            /*
             * Get Email Template
             */
            //$email_template = select("*", "tblnotification_msg", "status = 1 AND message_type='customer_feedback_email'");
            //if (isset($email_template) && is_array($email_template) && count($email_template) > 0) {
            $strTo = $customer_email;
            //$strTo = 'kanchan@orggen.com';
            //$email_content = $email_template[0]['message'];
            $email_content = file_get_contents('EmailFormat/Feedback_email.php');
            $strSubject = "Customer Feedback";
            //$strSubject = isset($email_template[0]['subject']) && $email_template[0]['subject'] != '' ? $email_template[0]['subject'] : 'Nailspa Feedback';

            $customer_name = $customer_data[0]['CustomerFullName'];
            $email_content = isset($customer_name) ? str_replace('{member_name}', ucwords($customer_name), $email_content) : $email_content;

            //$feedback_link = '<br><a href="' . FindHostAdmin() . '/feedback_form.php?customer_id=' . $customerId . '&apt_id=' . $appointment_id . '&src=1' . '">Click Here</a>';
            // $feedback_link = '<br><a href="http://pos.nailspaexperience.com/og/feedback_form.php?customer_id=' . $customerId . '&apt_id=' . $appointment_id . '&src=1' . '">Click Here</a>';
            $feedback_link = 'http://pos.nailspaexperience.com/og/feedback_form.php?customer_id=' . $customerId . '&apt_id=' . $appointment_id . '&src=1';
            $email_content = str_replace('{feedback_link}', $feedback_link, $email_content);

            $strFrom = "appnt@nailspaexperience.com";
            $strbody1 = $email_content;
            $headers = "From: $strFrom\r\n";
            //$headers .= 'Cc: kanchan@orggen.com' . "\r\n";
            //$headers .= 'Cc: operations@nailspaexperience.com,noor@nailspaexperience.com' . "\r\n";
            $headers .= "Content-type: text/html\r\n";
            $strBodysa = $strbody1;

            /*
             * Insert Into message table
             */
            $col_val = array('ToEmail', 'FromEmail', 'Subject', 'Body', 'DateTime', 'Status', 'created_date', 'created_by', 'description', 'CustomerID');
            $user['ToEmail'] = "'" . $strTo . "'";
            $user['FromEmail'] = "'" . $strFrom . "'";
            $user['Subject'] = "'" . $strSubject . "'";
            $user['Body'] = "'" . $strBodysa . "'";
            $user['DateTime'] = "'" . date('Y-m-d H:i:s') . "'";
            $user['Status'] = "'0'";
            $user['created_date'] = "'" . date('Y-m-d H:i:s') . "'";
            $user['created_by'] = 0;
            //$user['AppointmentID'] = "'" . $appointment_id. "'";
            $user['description'] = "'Feedback Mail Customer ID =" . $customerId . ",Appointment ID= " . $appointment_id . "Date=" . date('Y-m-d H:i:s') . "'";
            $user['CustomerID'] = "'" . $customerId . "'";
            $field_values = implode(',', $col_val);
            $data_values = implode(',', $user);
            $insert_sql = "INSERT into tblEmailMessages (" . $field_values . ") VALUES(" . $data_values . ")";
            $DB->query($insert_sql);
            $last_email_id = $DB->insert_id;
            unset($user);


            $retval = mail($strTo, $strSubject, $strBodysa, $headers);
            if ($retval == true) {
                $update_qry = "UPDATE tblEmailMessages SET Status='1',DateOfSending='" . date('Y-m-d H:i:s') . "'"
                . " WHERE ID='" . $last_email_id . "'";
                $DB->query($update_qry);
            }
            //}
        }

        $appointment_services = select("*", "tblAppointmentsDetailsInvoice", "AppointmentID='" . $appointment_id . "'");

        $appointment_data = select("*", "tblAppointments", "AppointmentID='" . $appointment_id . "'");

        if (isset($appointment_services) && is_array($appointment_services) && count($appointment_services) > 0) {
            foreach ($appointment_services as $ser_key => $ser_value) {
                $deduct_amount = ($ser_value['ServiceAmount'] * $ser_value['qty']) - $ser_value['offer_discount'] - $ser_value['member_discount'] - $ser_value['super_gv_discount'] - $ser_value['package_discount'];
                if ($deduct_amount < 0) {
                    $deduct_amount = 0;
                }
                if ($appointment_data[0]['new_package'] == 1 && $appointment_data[0]['PackageID'] > 0 && $ser_value['PackageService'] > 0) {
                    $deduct_amount = 0;
                }
                $update_Ser = "UPDATE tblAppointmentsDetailsInvoice SET service_amount_deduct_dis='" . $deduct_amount . "' WHERE AppointmentDetailsID='" . $ser_value['AppointmentDetailsID'] . "'";
                $DB->query($update_Ser);
                $log_data['query'][$ser_value['AppointmentDetailsID']] = "Amount=" . $deduct_amount . " AND AppointmentDetailsID =" . $ser_value['AppointmentDetailsID'];
            }
        }


        $log_data['post'] = $_POST;
        $log_data['apt_services'] = $appointment_services;

        $payment_query = "INSERT INTO `tbl_online_payment_info` (`id`, `appointment_id`,"
        . " `name`, `contact`, `email`, `amount`, `service_code`, `store`, "
        . "`source`, `payment_id`, `txnid`, `payment_date`, `payment_status`, "
        . "`error`, `error_message`, `created_date`,`service_provider`,`data`)"
        . " VALUES ('', '" . $appointment_id . "', '" . $CustomerFullName . "', '" . $mobile . "', '" . $email . "', '', '',"
        . " '', '1', '', '" . $txnid . "', '" . date('Y-m-d H:i:s') . "', '1',"
        . " '0', '', '" . $created_date . "','POS Bill','" . serialize($log_data) . "');";
        $DB->query($payment_query);


        /*
         * Add Invoice details to new database based on terminal id of store
         */
        $save_invoice_db = save_invoice_db($appointment_id);

        /*
         * Save Package Info if it is New Package
         */
        $appointment_data = select("*", "tblAppointments", "AppointmentID='" . $appointment_id . "'");

        if (isset($appointment_data) && is_array($appointment_data) && count($appointment_data) > 0) {
            if ($appointment_data[0]['new_package'] == 1 && $appointment_data[0]['PackageID'] > 0) {
                $seld = select("*", "tblPackages", "PackageID='" . $appointment_data[0]['PackageID'] . "'");
                $Validityp = $seld[0]['Validity'];
                $valid = "+" . $Validityp . "Months";
                $validpack = date('Y-m-d', strtotime($valid));

                $package_exist = select("id", "tblCustomerPackage", "customer_id='" . $appointment_data[0]['CustomerID'] . "' AND status=1
  AND package_id='".$appointment_data[0]['PackageID']."' and package_status=1 AND created_date LIKE '%".date('Y-m-d')."%'");

                if($package_exist==0){
                    $sqlInsertpq = "Insert into tblCustomerPackage(customer_id,package_id,package_expiry,package_status,created_date) "
                    . " values('" . $appointment_data[0]['CustomerID'] . "','" . $appointment_data[0]['PackageID'] . "', '" . $validpack . "','1','" . date('Y-m-d H:i:s') . "')";
                    $DB->query($sqlInsertpq);
                }
            }

            /*
             * Save Super Gift Voucher ID if added in Bill
             */
            if ($appointment_data[0]['super_gv_status'] == 1 && $appointment_data[0]['super_gv_id'] > 0) {
                $gift_voucher_data = select("*", "super_gift_voucher", "id='" . $appointment_data[0]['super_gv_id'] . "' AND status=1");
                $expiry_date = $gift_voucher_data[0]['GVDateTo'];
                $super_gv_amount = $gift_voucher_data[0]['GVAmount'];
                $percentage = $gift_voucher_data[0]['percentage'];
                $super_per_amount = ($super_gv_amount * $percentage) / 100;

                $gv_amount = $super_gv_amount + $super_per_amount;
                $sqlsuper_gv = "Insert into customer_super_gv(CustomerID,AppointmnetID,super_gv_id,expiry_date,gv_amount,used_amount,gv_status,created_date,created_by) "
                . " values('" . $appointment_data[0]['CustomerID'] . "','" . $appointment_id . "','" . $appointment_data[0]['super_gv_id'] . "', '" . $expiry_date . "',"
                . "'" . $gv_amount . "','0','1','" . date('Y-m-d H:i:s') . "','" . $strAdminID . "')";
                $DB->query($sqlsuper_gv);
            } else if ($appointment_data[0]['super_gv_status'] == 2 && $appointment_data[0]['super_gv_id'] > 0) {
                $customer_voucher_data = select("*", "customer_super_gv", "id='" . $_POST['cust_super_gv_id'] . "'");
                $new_used_amount = $customer_voucher_data[0]['used_amount'] + $customer_voucher_data[0]['pre_used_amount'];
                $used_amount = 0;
                $updateCust = "UPDATE customer_super_gv SET used_amount='" . $used_amount . "',pre_used_amount='" . $new_used_amount . "'
 WHERE id='" . $_POST['cust_super_gv_id'] . "'";

                $DB->query($updateCust);
            }
        }
    }

    $DB->close();
    echo $data;
}
?>