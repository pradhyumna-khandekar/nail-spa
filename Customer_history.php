    <style>	
        #loading .svg-icon-loader {
            position: absolute;
            top: 50%;
            left: 50%;
            margin: -50px 0 0 -50px;
        }
    </style>
    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>Customer History | Nailspa</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/images/icons/App-Icon114x114.jpg">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/images/icons/App-Icon114x114.jpg">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/images/icons/App-Icon72x72.jpg">
    <link rel="apple-touch-icon-precomposed" href="assets/images/icons/App-Icon57x57.jpg">
    <link rel="shortcut icon" href="assets/images/icons/favicon.png">
    <link rel="stylesheet" type="text/css" href="assets/helpers/animate.css">
    <link rel="stylesheet" type="text/css" href="assets/helpers/boilerplate.css">
    <link rel="stylesheet" type="text/css" href="assets/helpers/border-radius.css">
    <link rel="stylesheet" type="text/css" href="assets/helpers/grid.css">
    <link rel="stylesheet" type="text/css" href="assets/helpers/page-transitions.css">
    <link rel="stylesheet" type="text/css" href="assets/helpers/spacing.css">
    <link rel="stylesheet" type="text/css" href="assets/helpers/typography.css">
    <link rel="stylesheet" type="text/css" href="assets/helpers/utils.css">
    <link rel="stylesheet" type="text/css" href="assets/helpers/colors.css">
    <link rel="stylesheet" type="text/css" href="assets/material/ripple.css">
    <link rel="stylesheet" type="text/css" href="assets/elements/badges.css">
    <link rel="stylesheet" type="text/css" href="assets/elements/buttons.css">
    <link rel="stylesheet" type="text/css" href="assets/elements/content-box.css">
    <link rel="stylesheet" type="text/css" href="assets/elements/dashboard-box.css">
    <link rel="stylesheet" type="text/css" href="assets/elements/forms.css">
    <link rel="stylesheet" type="text/css" href="assets/elements/images.css">
    <link rel="stylesheet" type="text/css" href="assets/elements/info-box.css">
    <link rel="stylesheet" type="text/css" href="assets/elements/invoice.css">
    <link rel="stylesheet" type="text/css" href="assets/elements/loading-indicators.css">
    <link rel="stylesheet" type="text/css" href="assets/elements/menus.css">
    <link rel="stylesheet" type="text/css" href="assets/elements/panel-box.css">
    <link rel="stylesheet" type="text/css" href="assets/elements/response-messages.css">
    <link rel="stylesheet" type="text/css" href="assets/elements/responsive-tables.css">
    <link rel="stylesheet" type="text/css" href="assets/elements/ribbon.css">
    <link rel="stylesheet" type="text/css" href="assets/elements/social-box.css">
    <link rel="stylesheet" type="text/css" href="assets/elements/tables.css">
    <link rel="stylesheet" type="text/css" href="assets/elements/tile-box.css">
    <link rel="stylesheet" type="text/css" href="assets/elements/timeline.css">
    <link rel="stylesheet" type="text/css" href="assets/icons/fontawesome/fontawesome.css">
    <link rel="stylesheet" type="text/css" href="assets/icons/linecons/linecons.css">
    <link rel="stylesheet" type="text/css" href="assets/icons/spinnericon/spinnericon.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/accordion-ui/accordion.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/calendar/calendar.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/carousel/carousel.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/charts/justgage/justgage.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/charts/morris/morris.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/charts/piegage/piegage.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/charts/xcharts/xcharts.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/chosen/chosen.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/colorpicker/colorpicker.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/datatable/datatable.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/datepicker/datepicker.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/datepicker-ui/datepicker.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/dialog/dialog.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/dropdown/dropdown.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/dropzone/dropzone.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/file-input/fileinput.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/input-switch/inputswitch.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/input-switch/inputswitch-alt.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/ionrangeslider/ionrangeslider.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/jcrop/jcrop.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/jgrowl-notifications/jgrowl.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/loading-bar/loadingbar.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/maps/vector-maps/vectormaps.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/markdown/markdown.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/modal/modal.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/multi-select/multiselect.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/multi-upload/fileupload.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/nestable/nestable.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/noty-notifications/noty.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/popover/popover.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/pretty-photo/prettyphoto.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/progressbar/progressbar.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/range-slider/rangeslider.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/slidebars/slidebars.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/slider-ui/slider.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/summernote-wysiwyg/summernote-wysiwyg.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/timepicker/timepicker.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/tocify/tocify.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/tooltip/tooltip.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/touchspin/touchspin.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/uniform/uniform.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/wizard/wizard.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/xeditable/xeditable.css">
    <link rel="stylesheet" type="text/css" href="assets/snippets/chat.css">
    <link rel="stylesheet" type="text/css" href="assets/snippets/files-box.css">
    <link rel="stylesheet" type="text/css" href="assets/snippets/login-box.css">
    <link rel="stylesheet" type="text/css" href="assets/snippets/notification-box.css">
    <link rel="stylesheet" type="text/css" href="assets/snippets/progress-box.css">
    <link rel="stylesheet" type="text/css" href="assets/snippets/todo.css">
    <link rel="stylesheet" type="text/css" href="assets/snippets/user-profile.css">
    <link rel="stylesheet" type="text/css" href="assets/snippets/mobile-navigation.css">
    <link rel="stylesheet" type="text/css" href="assets/applications/mailbox.css">
    <link rel="stylesheet" type="text/css" href="assets/themes/admin/layout.css">
    <link rel="stylesheet" type="text/css" href="assets/themes/admin/color-schemes/default.css">
    <link rel="stylesheet" type="text/css" href="assets/themes/components/default.css">
    <link rel="stylesheet" type="text/css" href="assets/themes/components/border-radius.css">
    <link rel="stylesheet" type="text/css" href="assets/helpers/responsive-elements.css">
    <link rel="stylesheet" type="text/css" href="assets/helpers/admin-responsive.css">
    <link rel="stylesheet" type="text/css" href="assets/widgets/modal/modal.css">
	
    <script type="text/javascript" src="assets/js-core/jquery-core.js"></script>
    <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>
    <script type="text/javascript" src="assets/js-core/jquery-ui-widget.js"></script>
    <script type="text/javascript" src="assets/js-core/jquery-ui-mouse.js"></script>
    <script type="text/javascript" src="assets/js-core/jquery-ui-position.js"></script>
    <script type="text/javascript" src="assets/js-core/transition.js"></script>
    <script type="text/javascript" src="assets/js-core/modernizr.js"></script>
    <script type="text/javascript" src="assets/js-core/jquery-cookie.js"></script>
	<script type="text/javascript" src="assets/js-init/widgets-init.js"></script>
	<?php /* <script type="text/javascript" src="assets/widgets/modal/modal.js"></script> */ ?>
	  <script type="text/javascript" src="assets/widgets/daterangepicker/moment.js"></script>
                    <script type="text/javascript" src="assets/widgets/calendar/calendar.js"></script>
					 
                  
					   <script type="text/javascript" src="assets/widgets/datepicker/datepicker.js"></script>
                    <script type="text/javascript">
                        /* Datepicker bootstrap */

                        $(function() {
                            "use strict";
                            $('.bootstrap-datepicker').bsdatepicker({
                                format: 'mm-dd-yyyy'
                            });
                        });
                    </script>
                    <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker.js"></script>
                    <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker-demo.js"></script>
                    <script type="text/javascript" src="assets/widgets/daterangepicker/moment.js"></script>
                    <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker.js"></script>
                    <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
                    <script type="text/javascript" src="assets/widgets/timepicker/timepicker.js"></script>
                    <script type="text/javascript">
                        /* Timepicker */

                        $(function() {
                            "use strict";
                            $('.timepicker-example').timepicker();
                        });
                    </script>
                    <script type="text/javascript" src="assets/widgets/colorpicker/colorpicker.js"></script>
                    <script type="text/javascript" src="assets/widgets/colorpicker/colorpicker-demo.js"></script>
	
	<!-- Tabs related css and js start -->
	<script type="text/javascript" src="assets/widgets/tabs-ui/tabs.js"></script>
	<script type="text/javascript">
		/* jQuery UI Tabs */

		$(function() {
			"use strict";
			$(".tabs").tabs();
		});

		$(function() {
			"use strict";
			$(".tabs-hover").tabs({
				event: "mouseover"
			});
		});
	</script>
	<script type="text/javascript" src="assets/widgets/tabs/tabs.js"></script>
	<script type="text/javascript" src="assets/widgets/tabs/tabs-responsive.js"></script>
	<script type="text/javascript">
		/* Responsive tabs */
		$(function() {
			"use strict";
			$('.nav-responsive').tabdrop();
		});
	</script>
	<!-- Tabs related css and js end -->
	
	<!-- Responsive table related css and js start -->
	<script type="text/javascript" src="assets/widgets/datatable/datatable.js"></script>
	<script type="text/javascript" src="assets/widgets/datatable/datatable-bootstrap.js"></script>
	<script type="text/javascript" src="assets/widgets/datatable/datatable-responsive.js"></script>
	<script type="text/javascript">
		/* Datatables responsive */

		$(document).ready(function() {
			$('#datatable-responsive').DataTable({
				responsive: true
			});
		});

		$(document).ready(function() {
			$('.dataTables_filter input').attr("placeholder", "Search...");
		});
	</script>
	<!-- Responsive table related css and js end -->
	
	
	<!-- Form elements related css and js start -->
	<script type="text/javascript" src="assets/widgets/spinner/spinner.js"></script>
	<script type="text/javascript">
		/* jQuery UI Spinner */

		$(function() {
			"use strict";
			$(".spinner-input").spinner();
		});
	</script>
	<script type="text/javascript" src="assets/widgets/autocomplete/autocomplete.js"></script>
	<script type="text/javascript" src="assets/widgets/autocomplete/menu.js"></script>
	<script type="text/javascript" src="assets/widgets/autocomplete/autocomplete-demo.js"></script>
	<script type="text/javascript" src="assets/widgets/touchspin/touchspin.js"></script>
	<script type="text/javascript" src="assets/widgets/touchspin/touchspin-demo.js"></script>
	<script type="text/javascript" src="assets/widgets/input-switch/inputswitch.js"></script>
	       
	<script type="text/javascript">
		/* Input switch */

		$(function() {
			"use strict";
			$('.input-switch').bootstrapSwitch();
		});
	</script>
	<script type="text/javascript" src="assets/widgets/textarea/textarea.js"></script>
	<script type="text/javascript">
		/* Textarea autoresize */

		$(function() {
			"use strict";
			$('.textarea-autosize').autosize();
		});
	</script>
	<script type="text/javascript" src="assets/widgets/multi-select/multiselect.js"></script>
	<script type="text/javascript">
		/* Multiselect inputs */

		$(function() {
			"use strict";
			$(".multi-select").multiSelect();
			$(".ms-container").append('<i class="glyph-icon icon-exchange"></i>');
		});
	</script>
	<script type="text/javascript" src="assets/widgets/uniform/uniform.js"></script>
	<script type="text/javascript" src="assets/widgets/uniform/uniform-demo.js"></script>
	<script type="text/javascript" src="assets/widgets/chosen/chosen.js"></script>
	<script type="text/javascript" src="assets/widgets/chosen/chosen-demo.js"></script>
	<!-- Form elements related css and js end -->
	
	<script src="tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
        $(window).load(function() {
            setTimeout(function() {
                $('#loading').fadeOut(400, "linear");
            }, 300);
        });
    </script>
	<style>
		#overlay {
			position: fixed;
			left: 0;
			top: 0;
			bottom: 0;
			right: 0;
			//background: #BDB7B7;
			color: #ccc;
			border-color: #000;
			background: #272634;

			opacity: 0.8;
			z-index: 9999;
			filter: alpha(opacity=80);
		}
		#loadingsaif
		{
			position: absolute;
		    top: 50%;
		    left: 50%;
		    margin: -28px 0 0 -25px;
		}
	</style>
	

	

	
		<script type="text/javascript" src="assets/widgets/jgrowl-notifications/jgrowl.js"></script>
		<script type="text/javascript" src="assets/widgets/jgrowl-notifications/jgrowl-demo.js"></script>
		<script type="text/javascript" src="assets/widgets/noty-notifications/noty.js"></script>
		<script type="text/javascript" src="assets/widgets/noty-notifications/noty-demo.js"></script>

	
	
	
	
	
	
	
	
<html>
<head>

<style type="text/css">
@media print {
  body * {
    visibility: hidden;
  }
  #printarea * {
    visibility: visible;
  }
  #printarea{
    position: absolute;
    left: 0;
    top: 0;
  }
}
</style>
</head>
   <body onload="saifusmaniisgreat();" id="displays">                


<div class="alert-content">
	<h4 class="alert-title"><center><b></b></center></h4>
</div><br/>
  <div class="alert-content" id="displaymsg" style="display:none">
	
</div><br/>       
<div id="printarea">
<table border="0" cellspacing="0" cellpadding="0" width="100%" >
    <tbody style="border:1px" >
        <tr style="background-image: url(http://nailspaexperience.com/images/test3.png);background-repeat: no-repeat; background-position:50% 20%; media:print; -webkit-print-color-adjust: exact;">
            <td>
                <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center" >
                    <tbody>
                        
                        <tr >
                            <td>
                                <table id="printarea" style="BORDER-BOTTOM:#d0ac52 1px solid;BORDER-LEFT:#d0ac52 1px solid;BORDER-TOP:#d0ac52 1px solid;BORDER-RIGHT:#d0ac52 1px solid;" cellspacing="0" cellpadding="0" width="98%" align="center" media="print">
                                    <tbody >
										
									                                        <tr>
									
                                            <td align="middle">
												<input type="hidden" name="appointment_id" id="appointment_id" value="�S�I<o;jb�~$���|�0o����q�Z;`h�" />
                                                <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                                    <tbody>
                                                        <tr>
														
                                                            <td width="50%" align="left" style="padding:1%;"><img border="0" src="http://nailspaexperience.com/header/Nailspa-logo.png" width="117" height="60"><input type="hidden" name="app_id" id="app_id" value="�S�I<o;jb�~$���|�0o����q�Z;`h�" /></td>
                                                            <td width="50%" align="right" style="LINE-HEIGHT:15px; padding:1%; FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal;"><b>
															</b>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="LINE-HEIGHT:0;BACKGROUND:#d0ad53;FONT-SIZE:0px;" height="5"></td>
                                        </tr>
                                          
										
                                        
                                        <tr>
                                            <td height="8"></td>
                                        </tr>
                                           <tr>
                                            <td style="LINE-HEIGHT:0;BACKGROUND:#d0ad53;FONT-SIZE:20px;text-align:center;" height="30"><b>Customer History</b></td>
                                        </tr>
                                        <tr>
                                            <td height="8">
                                                <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                                    <tbody>
                                                        <tr>
                                                            <td bgcolor="#e4e4e4" height="4"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
										<?php    
require_once('setting.fya');


	

	$DB = Connect();
	$sql = "SELECT * from tblCustomers where CustomerMobileNo='9820432303'";
	$RS = $DB->query($sql); 
   ?>
                                        <tr>
										
                                            <td>
											
                                                <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                              
                                                
                                                <tbody>
                                                        <tr>
                                                          <th width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">CustomerID</th>
                                                          <th width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:center; padding-left:2%;">Registration Date</th>
														   <th width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">Customer Name</th>
                                                         <th width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">Customer Email</th>
														  <th width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">Customer Mobile</th>
														
														
                                                        </tr>
														<?php 
															   
	if ($RS->num_rows > 0) 
	{
		while($row = $RS->fetch_assoc())
		{ ?>
			<tr>
			  <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;text-align: center;"><?=$row['CustomerID'];?></td>
                                              
			<td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;text-align: center;">2018-05-10 04:00:00</td>
			<td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;text-align: center;"> Rajesh</td>
			<td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;text-align: center;">  bandra@nailspaexperience.com</td>
                      <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;text-align: center;">  9869395363</td>
                        </tr>

		<?php }
	}	

?>														
															
							
																											
															

														
									
														
                                                        
                                                        
                                                      </tbody>
                                              
                                                </table>
                                            </td>
											
											 
                                       
                                        
                                                         
                                        <tr>
                                        
                                         <td  style="BACKGROUND:#d0ad53;">
                                                <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                              
                                                <tbody>
                                        
                                        
                                        
                                            <td width="33%" style="FONT-FAMILY:Arial,Helvetica,sans-serif;BACKGROUND:#d0ad53;COLOR:#000;FONT-SIZE:12px; padding:1%;" height="32" align="center">
                                            <span style="font-size:14px; font-weight:600;" >KHAR | BREACH CANDY | ANDHERI | COLABA | LOKHANDWALA</span><br>
                                            </td>
											<tr>
                                            	<td style="BACKGROUND:#d0ad53;font-size:18px;font-weight:bold;" align="center"> Go Green, Go Paperless !</td>
                                            </tr>
                                            
                                            </tbody>
                                            </table>
                                            </td>
                                            
                                            
                                        	</tr>
											
                                    	</tbody>
                                	</table>
                            	</td>
                        	</tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>

</div>
</body>

</html>
			

