<?php

$email_body = '<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Email Template</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            body{color:#000;background-color:#dbb75f}
            p{margin:3px;}
            tr, td{font-size: 14px;}
            th{color:#795548;padding:5px;}
            h1{margin: 0;text-align:center;font-size:25px;color:#607D8B;}
            h2{font-size:16px;}
            button{font-size: 16px;
                   display: block;
                   margin: 20px 0;
                   background-color: #F44336;
                   color: #fff;
                   padding: 10px;
                   border: none;
                   border-radius: 5px;}
            .main-table{background-color:#dbb75f;font-family: "Helvetica" , sans-serif; font-size:14px; margin:0; padding:0;}
            .report-table{border-collapse: collapse;display:block;text-align:center;}
            .report-table tr{border-bottom: 2px solid #795548;}
            .report-table tr td{padding:5px;}
            .main-table tr td{font-family: "Roboto Slab", serif;"}
            @media only screen and (max-width: 600px) {
                
            }
        </style>
    </head>
    <body>
        <table class="main-table" cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
            <tbody><tr>
                    <td align="center" valign="top" style="padding:20px 0 20px 0">
                        <table bgcolor="#FFFFFF" cellspacing="0" cellpadding="10" border="0" width="600" style="border:1px solid #dddddd;">
                            <tbody>
                                <tr>
                                    <td colspan="1">
                                        <a href="#" target="_blank"><img src="https://pos.nailspaexperience.com/og/feedback/images/nailspa.png" style="width:100px; display:block; margin:0 auto"></a>
                                    </td>
                                   
                                </tr>
                                
                                
                                
                               
                                <tr>
                                    <td colspan="2" style="padding:0px;padding-left: 22px;padding-right: 22px;">
                                        <table width="100%" cellspacing="0" cellpadding="0" style="
                                               / background: #edf7f0; /
                                               ">
                                            <tbody><tr>
                                                    <td colspan="2" style="text-transform: capitalize;line-height: 24px;padding:0 20px 10px;text-align:left;/ margin-left: 16px; /">
                                                        <p style="margin-top:15px; color: #742d2a;font-size: 19px; font-weight:600;">Feedback Rating :  <strong style="font-weight:300">' . date('d/m/Y') . '</strong></p>
                                                        <p style="margin-top:15px;color: #742d2a;font-size: 19px; font-weight:600;font-family: "Roboto Slab", serif;">Customer Name : <strong style="font-weight:300">{customer_name}</strong></p>
                                                        <p style="margin-top:15px;color: #742d2a;font-size: 19px;font-weight:600;font-family: "Roboto Slab", serif;">Customer Mobile No. : <strong style="font-weight:300">{customer_mobile}</strong></p>
                                                        <p style="margin-top:15px;color: #742d2a;font-size: 19px;font-weight:600;font-family: "Roboto Slab", serif;">Sale Amount : <strong style="font-weight:300">{sale_amount}</strong> </p>
                                                        <p style="margin-top:15px;color: #742d2a;font-size: 19px;font-weight:600;font-family: "Roboto Slab", serif;">Service Quality  <strong style="font-weight:300">{service_quality}</strong></p>
                                                        <p style="margin-top:15px;color: #742d2a;font-size: 19px;font-weight:600;font-family: "Roboto Slab", serif;">Value for money : <strong style="font-weight:300">{value_for_money}</strong></p>
                                                        <p style="margin-top:15px;color: #742d2a;font-size: 19px;font-weight:600;font-family: "Roboto Slab", serif;">Salon Atmosphere : <strong style="font-weight:300">{salon_atmosphere}</strong></p>
                                                        <p style="margin-top:15px;color: #742d2a;font-size: 19px;font-weight:600;font-family: "Roboto Slab", serif;">Staff Presentation & Attitude :  <strong style="font-weight:300"> {staff_presentation_attitude}</strong></p>
														<p style="margin-top:15px;color: #742d2a;font-size: 19px;font-weight:600;font-family: "Roboto Slab", serif;">Cleanliness :  <strong style="font-weight:300"> {cleanliness}</strong></p>
														<p style="margin-top:15px;color: #742d2a;font-size: 19px;font-weight:600;font-family: "Roboto Slab", serif;">Appointment Date :  <strong style="font-weight:300"> {appointment_date}</strong></p>
														<p style="margin-top:15px;color: #742d2a;font-size: 19px;font-weight:600;font-family: "Roboto Slab", serif;">Store : <strong style="font-weight:300"> {appointment_store}</strong></p>
														<p style="margin-top:15px;color: #742d2a;font-size: 19px;font-weight:600;font-family: "Roboto Slab", serif;">Services Done : <strong style="font-weight:300"> {service_name}</strong></p>
														<p style="margin-top:15px;color: #742d2a;font-size: 19px;font-weight:600;font-family: "Roboto Slab", serif;">Employee Name : <strong style="font-weight:300"> {employee_name}</strong></p>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                    </td>
                                </tr>
                                
                                
                                
                            </tbody></table>
                    </td>
                </tr>
            </tbody></table>
    </body>
</html>';


return $email_body;
