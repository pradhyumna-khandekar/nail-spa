<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
$strPageTitle = "Calendar | Nailspa";
$strDisplayTitle = "Calendar of Nailspa Experience";
$strMenuID = "2";


// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights	
?>	


<!DOCTYPE html>
<html lang="en">

    <head>
<?php require_once("incMetaScript.fya"); ?>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
        <script>

            $(document).ready(function () {
                var calendar = $('#calendar').fullCalendar({
                    editable: true,
                    eventLimit: true,
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay'
                    },
                    events: 'load.php',
                    selectable: true,
                    selectHelper: true,
                    select: function (start, end, allDay)
                    {
                        //var title = prompt("Enter Event Title");
                        //if(title)
                        //{
                        var start = $.fullCalendar.formatDate(start, "Y-MM-DD");
                        var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");
                        $.ajax({
                            url: "DemoLoadCalender.php",
                            type: "POST",
                            data: {start: start},
                            success: function (json)
                            {
                                $('#calresult').html(json);
                            }
                        })

                    },
                    /*editable: true,
                     eventDrop: function(event, delta) {
                     var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
                     var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
                     $.ajax({
                     url: 'update_events.php',
                     data: 'title='+ event.title+'&start='+ start +'&end='+ end +'&id='+ event.id ,
                     type: "POST",
                     success: function(json) {
                     alert("Updated Successfully");
                     }
                     });
                     },
                     /*eventClick: function(event) {
                     var decision = confirm("Do you really want to do that?"); 
                     if (decision) {
                     $.ajax({
                     type: "POST",
                     url: "delete_event.php",
                     data: "&id=" + event.id,
                     success: function(json) {
                     $('#calendar').fullCalendar('removeEvents', event.id);
                     alert("Updated Successfully");}
                     });
                     }
                     },
                     /*eventResize: function(event) {
                     var start = $.fullCalendar.formatDate(event.start, "yyyy-MM-dd HH:mm:ss");
                     var end = $.fullCalendar.formatDate(event.end, "yyyy-MM-dd HH:mm:ss");
                     $.ajax({
                     url: 'update_events.php',
                     data: 'title='+ event.title+'&start='+ start +'&end='+ end +'&id='+ event.id ,
                     type: "POST",
                     success: function(json) {
                     alert("Updated Successfully");
                     }
                     });
                     }*/

                });

            });


        </script>

        <style>
            .container {
                width: 600px !important;
                float: left;
            }
            .fc-basic-view .fc-body .fc-row {
                max-height: 85.71px !important;
            }
            div#calresult {

                overflow-y: auto;
                height: 500px;
            }
        </style>
    </head>

    <body>
        <div id="sb-site">

<?php require_once("incOpenLayout.fya"); ?>


<?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

            <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                <?php require_once("incHeader.fya"); ?>


                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                        </div>


                        <div class="panel">
                            <div class="panel">
                                <div class="panel-body">


                                    <div class="example-box-wrapper">
                                        <div class="tabs">

                                            <div id="normal-tabs-1">

                                                <span class="form_result">&nbsp; <br>
                                                </span>

                                                <div class="panel-body">
                                                    <h3 class="title-hero">Calendar</h3>



                                                    <br />
                                                    <div class="container">
                                                        <div id="calendar"></div>

                                                    </div>
                                                    <h4 align="center"><a href="http://pos.nailspaexperience.com/admin/ManageCustomers2.php">Book a Appointment</a></h4>
                                                    <div id="calresult"></div>



                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>




                        </div>
                    </div>

                </div>




            </div>
<?php require_once 'incFooter.fya'; ?>

        </div>
    </body>

</html>