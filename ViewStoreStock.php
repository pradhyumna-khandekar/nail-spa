<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Store Stock Management | Nailspa";
$strDisplayTitle = "View inventory for Store Nailspa";
$strMenuID = "2";
$strMyTable = "tblStoreStock";
$strMyTableID = "StoreStockID";
$strMyField = "";
$strMyActionPage = "ViewStoreStock.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights	



if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $strStep = Filter($_POST["step"]);

    if ($strStep == "add") {
        
    }

    if ($strStep == "edit") {
        
    }
}
?>


<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");  ?>
            <!----------commented by gandhali 1/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>


                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                        </div>
                        <?php
                        if (!isset($_GET["uid"])) {
                            ?>					

                            <div class="panel">
                                <div class="panel">
                                    <div class="panel-body">


                                        <div class="example-box-wrapper">
                                            <div class="tabs">

                                                <div id="normal-tabs-1">

                                                    <span class="form_result">&nbsp; <br>
                                                    </span>

                                                    <div class="panel-body">
                                                        <h3 class="title-hero">List of all products available in <?= $strStoreNamedisplay ?> Store</h3>
                                                        <div class="example-box-wrapper">
                                                            <table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Sr. no</th>
                                                                        <th>Product Name</th>
                                                                        <th>Available Stock</th>
                                                                    </tr>
                                                                </thead>
                                                                <tfoot>
                                                                    <tr>
                                                                        <th>Sr. no</th>
                                                                        <th>Product Name</th>
                                                                        <th>Available Stock</th>
                                                                    </tr>
                                                                </tfoot>
                                                                <tbody>

                                                                    <?php
// Create connection And Write Values
                                                                    if ($strStore == "" || $strStore == "0") {
                                                                        $strStore = DecodeQ(Filter($_GET["q"]));
                                                                        $SelectStore = "YES";
                                                                    } else {
                                                                        $strStore = $strStore;
                                                                        $SelectStore = "NO";
                                                                    }



                                                                    $DB = Connect();
                                                                    $sql = "SELECT tblNewProducts.ProductName,product_inventory.*
		FROM tblNewProducts
		LEFT JOIN product_inventory
		ON tblNewProducts.ProductID=product_inventory.ProductID
		where product_inventory.StoreID='$strStore'
		Group by product_inventory.ProductID";

                                                                    $RS = $DB->query($sql);
                                                                    if ($RS->num_rows > 0) {
                                                                        $counter = 0;

                                                                        while ($row = $RS->fetch_assoc()) {
                                                                            $counter ++;
                                                                            ?>														

                                                                            <tr id="my_data_tr_<?= $counter ?>">
                                                                                <td><?= $counter ?></td>
                                                                                <td><?= $row["ProductName"] ?></td>
                                                                                <td><?= $row["Stock"] ?></td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                    } else {
                                                                        ?>															
                                                                        <tr>
                                                                            <td></td>
                                                                            <td>No Records Found</td>
                                                                            <td></td>
                                                                        </tr>

                                                                        <?php
                                                                    }
                                                                    $DB->close();
                                                                    ?>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>

                                                <?php
                                                if ($SelectStore == "YES") {
                                                    ?>
                                                    <a class="btn btn-primary btn-lg btn-block" href="ManageProductStoreWise.php"><i class="fa fa-backward"></i> &nbsp; Select Store to view inventory</a>
                                                    <?php
                                                }
                                                ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>

                <?php require_once 'incFooter.fya'; ?>

            </div>
    </body>

</html>