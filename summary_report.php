<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Summary Report | Nailspa";
$strDisplayTitle = "Summary Report for Nailspa";
$strMenuID = "3";
$strMyTable = "tblOffers";
$strMyTableID = "OfferID";
$strMyField = "OfferName";
$strMyActionPage = "summary_report.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <!-----------css & js files added for tabs by gandhali 3/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");     ?>
            <!----------commented by gandhali 3/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>

                        <script type="text/javascript" src="assets/widgets/datepicker/datepicker.js"></script>
                        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker.js"></script>
                        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker-demo.js"></script>
                        <script type="text/javascript" src="assets/widgets/daterangepicker/moment.js"></script>

                        <script>
                            $(function ()
                            {
                                $(".datepicker-example").datepicker();
                            });
                        </script>

                        <?php
                        $DB = Connect();


                        $DB->close();
                        ?>
                        <div id="page-title">
                            <h2>View Summary Report</h2>
                        </div>

                        <div class="panel">
                            <div class="panel-body">
                                <form method="get" class="form-horizontal bordered-row" role="form" action="summary_report_export.php">
                                    <div class="form-group">
                                        <label for="month" class="col-sm-1 control-label">Select Month :</label>
                                        <div class="col-sm-3">
                                            <select name="month" class="form-control" id="month" required="required">
                                                <option value="">Select Month.</option>
                                                <?php for($m=1; $m<=12; ++$m){ ?>
                                                    <option value="<?php echo $m; ?>" <?php echo isset($_GET["month"]) && $_GET["month"] == $m ? 'selected' : ''; ?>><?php echo date('F', mktime(0, 0, 0, $m, 1)); ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>



                                        <label for="date1" class="col-sm-1 control-label">Select Year :</label>
                                        <div class="col-sm-3">
                                            <div class="input-prepend input-group">
                                                <span class="add-on input-group-addon">
                                                    <i class="glyph-icon icon-calendar"></i>
                                                </span> 
                                                <?php /* <input type="text" name="date1" id="ReportDate1"  class="form-control datepicker-example" data-date-format="yy/mm/dd" value="<?php echo isset($_GET["date1"]) ? $_GET["date1"] : ''; ?>"> */ ?>

                                                <select name="date1" class="form-control" id="date1" required="required">
                                                    <option value="">Select Year.</option>
                                                    <?php for ($i = date('Y'); $i >= 2010; $i--) { ?>
                                                        <option value="<?php echo $i; ?>" <?php echo isset($_GET["date1"]) && $_GET["date1"] == $i ? 'selected' : ''; ?>><?php echo $i; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>


                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-1 control-label"></label>
                                        <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Download Report</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                        &nbsp;&nbsp;&nbsp;
                                        <a class="btn btn-link" href="summary_report.php">Clear All Filter</a>
                                        &nbsp;&nbsp;&nbsp;

                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <?php require_once 'incFooter.fya'; ?>


        </div>
    </body>
</html>
