<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php

$result['show'] = 2;
$result['today_noti'] = array();
$result['future_noti'] = array();
$DB = Connect();
$today = date('Y-m-d');
$notiq = "SELECT * FROM tblAppointments WHERE StoreID ='" . $strStore . "' AND source=2"
        . " AND IsDeleted = 0 AND notification_seen = 1 AND AppointmentDate >='" . $today . "'";
$notiq_exe = $DB->query($notiq);
if ($notiq_exe->num_rows > 0) {
    while ($noti_data = $notiq_exe->fetch_assoc()) {
        $all_notification[$noti_data['AppointmentID']] = $noti_data;
        $cust_id_arr[$noti_data['CustomerID']] = $noti_data['CustomerID'];
    }
}


/*
 * Get Customer Data
 */
if (isset($cust_id_arr) && is_array($cust_id_arr) && count($cust_id_arr) > 0) {
    $custin_ids = implode(",", $cust_id_arr);
    if ($custin_ids != '') {
        $custq = "SELECT * FROM tblCustomers WHERE CustomerID IN (" . $custin_ids . ")";
        $custq_exe = $DB->query($custq);
        if ($custq_exe->num_rows > 0) {
            while ($cust_data = $custq_exe->fetch_assoc()) {
                $customer_data[$cust_data['CustomerID']] = $cust_data;
            }
        }
    }
}
if (isset($all_notification) && is_array($all_notification) && count($all_notification) > 0) {
    $result['show'] = 1;
    foreach ($all_notification as $nkey => $nvalue) {
        if ($nvalue['AppointmentDate'] == $today) {
            $result['today_noti'][$nvalue['AppointmentID']] = $nvalue;
            $result['today_noti'][$nvalue['AppointmentID']]['name'] = isset($customer_data[$nvalue['CustomerID']]) ? $customer_data[$nvalue['CustomerID']]['CustomerFullName'] : '';
            $result['today_noti'][$nvalue['AppointmentID']]['mobile'] = isset($customer_data[$nvalue['CustomerID']]) ? $customer_data[$nvalue['CustomerID']]['CustomerMobileNo'] : '';
            $result['today_noti'][$nvalue['AppointmentID']]['apt_date'] = date('d/m/Y', strtotime($nvalue['AppointmentDate'])) . ' ' . date('h:i a', strtotime($nvalue['SuitableAppointmentTime']));
        } else {
            $result['future_noti'][$nvalue['AppointmentID']] = $nvalue;
            $result['future_noti'][$nvalue['AppointmentID']]['name'] = isset($customer_data[$nvalue['CustomerID']]) ? $customer_data[$nvalue['CustomerID']]['CustomerFullName'] : '';
            $result['future_noti'][$nvalue['AppointmentID']]['mobile'] = isset($customer_data[$nvalue['CustomerID']]) ? $customer_data[$nvalue['CustomerID']]['CustomerMobileNo'] : '';
            $result['future_noti'][$nvalue['AppointmentID']]['apt_date'] = date('d/m/Y', strtotime($nvalue['AppointmentDate'])) . ' ' . date('h:i a', strtotime($nvalue['SuitableAppointmentTime']));
        }
    }
}
$result['today_count'] = count($result['today_noti']);
$result['future_count'] = count($result['future_noti']);
echo json_encode($result);
?>
