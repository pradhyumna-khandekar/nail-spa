<?php require_once("setting.fya"); ?>
<?php

function add_appointment($post_data = null) {
    $DB = Connect();
//    $post_data = array(
//        'fname' => 'Test',
//        'lname' => 'API3',
//        'email' => 'john@gmail.com',
//        'mobile' => '1122334455',
//        'gender' => 1,
//        'cust_location' => 'Churchgate',
//        'store_name' => 'Khar',
//        'store_id' => '2',
//        'appointment_date' => '2019-05-07',
//        'appointment_time' => '19:00',
//        'paid_amount' => '500',
//        'services' => array(
//            '0' => array(
//                'name' => 'Nailspa',
//                'quantity' => 2,
//                'service_id' => '200'
//            )
//        )
//    );

    if (isset($post_data) && is_array($post_data) && count($post_data) > 0) {
        $mobile_no = isset($post_data['mobile']) ? trim($post_data['mobile']) : '';

        if ($mobile_no != '') {
            /*
             * Check Customer Exist Or Not
             */
            $custexist_q = "Select * from tblCustomers Where CustomerMobileNo LIKE '%" . $mobile_no . "%'";
            $custexist_exe = $DB->query($custexist_q);

            if ($custexist_exe->num_rows > 0) {
                $cust_data = $custexist_exe->fetch_assoc();
                $strCustomerID = $cust_data['CustomerID'];
            } else {
                /*
                 * Add Customer
                 */

                $strFirstName = isset($post_data['fname']) ? $post_data['fname'] : '';
                $strLastName = isset($post_data['lname']) ? $post_data['lname'] : '';
                $strCustomerFullName = $strFirstName . " " . $strLastName;
                $strCustomerEmailID = isset($post_data['email']) ? $post_data['email'] : '';
                $strCustomerMobileNo = isset($post_data['mobile']) ? $post_data['mobile'] : '';
                $strStatus = 0;
                $strGender = isset($post_data['gender']) ? $post_data['gender'] : '';
                $Acquisition = 0;
                $CustomerLocation = isset($post_data['cust_location']) ? $post_data['cust_location'] : '';

                $sqlInsert = "INSERT INTO tblCustomers (FirstName, LastName, CustomerFullName, CustomerEmailID, CustomerMobileNo, Status,RegDate,Gender,Acquisition,CustomerLocation) VALUES 
					('" . $strFirstName . "', '" . $strLastName . "', '" . $strCustomerFullName . "', '" . $strCustomerEmailID . "', '" . $strCustomerMobileNo . "', '" . $strStatus . "','" . date('Y-m-d H:i:s') . "','" . $strGender . "','" . $Acquisition . "', '" . $CustomerLocation . "')";
//echo $sqlInsert . "<br>";
                $DB->query($sqlInsert);
                $strCustomerID = $DB->insert_id;
            }

            $strStoreID = isset($post_data['store_id']) ? $post_data['store_id'] : 0;
            $strAppointmentDate1 = isset($post_data['appointment_date']) ? date('Y-m-d', strtotime($post_data['appointment_date'])) : 0;

            $pqr = isset($post_data['appointment_time']) ? date('H:i:s', strtotime($post_data['appointment_time'])) : 0;

            $strAppointmentCheckInTime = '00:00:00';
            $strAppointmentCheckOutTime = '00:00:00';
            $strAppointmentOfferID = 0;
            $remark_type = 0;
            $remark_date = 0;
            $app_source = isset($post_data['source_id']) ? $post_data['source_id'] : 2;
            $Type_Service = $post_data['type_services'];

            /*
             * ADD Appointment
             */
            $paid_amount = isset($post_data['paid_amount']) ? trim($post_data['paid_amount']) : '0';

            if (!empty($Type_Service)) {

                if ($Type_Service == '0') {
                    $sqlInsert = "INSERT INTO tblAppointments (CustomerID, StoreID, AppointmentDate,
                        SuitableAppointmentTime, AppointmentCheckInTime, 
                        AppointmentCheckOutTime, AppointmentOfferID, Status,FreeService,
                        PackageID,non_visiting_update_value,non_visiting_update_date,
                        source,created_date,prepaid_amount) 
                        VALUES('" . $strCustomerID . "', '" . $strStoreID . "', '" . $strAppointmentDate1 . "', '"
                            . "" . $pqr . "', '" . $strAppointmentCheckInTime . "', '" .
                            $strAppointmentCheckOutTime . "', '" . $strAppointmentOfferID . "', '0','0','0','"
                            . "" . $remark_type . "','" . $remark_date . "','" . $app_source . "','"
                            . "" . date('Y-m-d H:i:s') . "','" . $paid_amount . "')";
                } else {
                    $sqlInsert = "INSERT INTO tblAppointments (CustomerID, StoreID, AppointmentDate, SuitableAppointmentTime, AppointmentCheckInTime, AppointmentCheckOutTime, AppointmentOfferID, Status,FreeService,PackageID,non_visiting_update_value,non_visiting_update_date,source,created_date,prepaid_amount) VALUES 
				('" . $strCustomerID . "', '" . $strStoreID . "', '" . $strAppointmentDate1 . "', '" . $pqr . "', '" . $strAppointmentCheckInTime . "', '" .
                            $strAppointmentCheckOutTime . "', '" . $strAppointmentOfferID . "', '0','1','0','" . $remark_type . "','" . $remark_date . "','" . $app_source . "','" . date('Y-m-d H:i:s') . "','" . $paid_amount . "')";
                }
            } else {
                $sqlInsert = "INSERT INTO tblAppointments (CustomerID, StoreID, AppointmentDate, SuitableAppointmentTime, AppointmentCheckInTime, AppointmentCheckOutTime, AppointmentOfferID, Status,PackageID,non_visiting_update_value,non_visiting_update_date,source,created_date,prepaid_amount) VALUES 
				('" . $strCustomerID . "', '" . $strStoreID . "', '" . $strAppointmentDate1 . "', '" . $pqr . "', '" . $strAppointmentCheckInTime . "', '" .
                        $strAppointmentCheckOutTime . "', '" . $strAppointmentOfferID . "', '0','0','" . $remark_type . "','" . $remark_date . "','" . $app_source . "','" . date('Y-m-d H:i:s') . "','" . $paid_amount . "')";
            }

            $dateyu = Date('Y-m-d');

            if ($DB->query($sqlInsert) === TRUE) {
                $last_idp = $DB->insert_id;  //last id of tblCustomers insert

                $sepptu = select("*", "tblCustomerMemberShip", "CustomerID='" . $strCustomerID . "'");
                $enddate = $sepptu[0]['EndDay'];
                $seldatapq = select("memberid", "tblCustomers", "CustomerID='" . $strCustomerID . "'");
                $memid = $seldatapq[0]['memberid'];
            }

            if ($dateyu <= $enddate) {
                if ($memid != "") {
                    $seldoffert = select("*", "tblAppointments", "AppointmentID='" . $last_idp . "'");

                    $FreeService = $seldoffert[0]['FreeService'];
                    if ($FreeService == "0") {
                        $sqlUpdate1 = "UPDATE tblAppointments SET memberid='" . $memid . "' WHERE AppointmentID='" . $last_idp . "'";
                        ExecuteNQ($sqlUpdate1);
                    }
                }
            }


            $strServicesused = isset($post_data["services"]) ? $post_data["services"] : array();
//             echo "<pre>";
//            print_r($last_idp);
//            echo "<pre>";
//            print_r($strServicesused);
//            exit;
//            exit;
            if (isset($strServicesused) && is_array($strServicesused) && count($strServicesused) > 0) {
                foreach ($strServicesused as $skey => $svalue) {
                    $selpw = select("*", "tblServices", "ServiceID='" . $svalue['service_id'] . "' and StoreID='" . $strStoreID . "'");
                    foreach ($selpw as $vapt) {
                        $ServiceID = $vapt['ServiceID'];
                        $ServiceCost = $vapt["ServiceCost"];
                        $categoryselect[$i] = 0;
                        $qty[$i] = $svalue['quantity'];

                        $sqlInsert3 = "INSERT INTO tblAppointmentsDetails (AppointmentID, ServiceID, ServiceAmount,  Status,qty,employeecategory) VALUES 
							('" . $last_idp . "', '" . $ServiceID . "', '" . $ServiceCost . "', '0','" . $qty[$i] . "','" . $categoryselect[$i] . "')";
                        if ($DB->query($sqlInsert3) === TRUE) {
                            $last_id3 = $DB->insert_id;  //last id of tblAppointments insert
                        }
                        $sqlInsert3p = "INSERT INTO tblAppointmentsDetailsInvoice (AppointmentID, ServiceID, ServiceAmount,Status,qty,employeecategory,is_upsell) VALUES 
									('" . $last_idp . "', '" . $ServiceID . "',  '" . $ServiceCost . "','0','" . $qty[$i] . "','" . $categoryselect[$i] . "','2')";
                        if ($DB->query($sqlInsert3p) === TRUE) {
                            $last_id7 = $DB->insert_id;  //last id of tblAppointments insert
                        }

                        if ($FreeService != "0") {
                            $sqlInsertpo = "INSERT INTO tblAppointmentAssignEmployee(AppointmentID, ServiceID, MECID, Commission,Qty,FreeService) VALUES
						('" . $last_idp . "', '" . $ServiceID . "', '0', '0','" . $qty[$i] . "','1')";
                            ExecuteNQ($sqlInsertpo);
                        } else {
                            $sqlInsertpo = "INSERT INTO tblAppointmentAssignEmployee(AppointmentID, ServiceID, MECID, Commission,Qty) VALUES
						('" . $last_idp . "', '" . $ServiceID . "', '0', '0','" . $qty[$i] . "')";
                            ExecuteNQ($sqlInsertpo);
                        }

                        $sqlcharges = "Select ChargeNameId , (select GROUP_CONCAT(distinct ChargeSetID) from tblCharges where 
								ChargeNameID=tblServicesCharges.ChargeNameID) as ArrayChargeSet from tblServicesCharges where ServiceID= '" . $ServiceID . "'";
                        $charges = $DB->query($sqlcharges);
                        if ($charges->num_rows > 0) {
                            while ($row = $charges->fetch_assoc()) {
                                $ArrayChargeSet = $row["ArrayChargeSet"];
                                $strChargeSet = explode(",", $ArrayChargeSet);
                            }
                        }
                    }

                    for ($j = 0; $j < count($strChargeSet); $j++) {
                        $strChargeSetforwork = $strChargeSet[$j];
                        $sqlchargeset = "select SetName, ChargeAmt, ChargeFPType from tblChargeSets where ChargeSetID=$strChargeSetforwork";

                        $RS2 = $DB->query($sqlchargeset);
                        if ($RS2->num_rows > 0) {
                            while ($row2 = $RS2->fetch_assoc()) {
                                $strChargeAmt = $row2["ChargeAmt"];
                                $strSetName = $row2["SetName"];
                                $strChargeFPType = $row2["ChargeFPType"];
// Calculation of charges
                                $ServiceCost = $ServiceCost;
                                if ($strChargeFPType == "0") {
                                    $strChargeAmt = $strChargeAmt;
                                } else {

                                    $percentage = $strChargeAmt;
//echo "percentage=".$percentage."<br/>";
                                    $outof = $ServiceCost;
//echo "ServiceCost=".$ServiceCost."<br/>";
                                    $strChargeAmt = ($percentage / 100) * $outof;
//echo "strChargeAmt=".$strChargeAmt."<br/>";
                                }

                                $totalamt = $strChargeAmt * $qty[$i];
                                $sqlInsertcharges = "INSERT INTO tblAppointmentsCharges(AppointmentDetailsID, ChargeName, ChargeAmount,AppointmentID) VALUES 
									('" . $last_id3 . "', '" . $strSetName . "', '" . $totalamt . "','" . $last_idp . "')";
                                $sqlInsertcharges . "<br/>";
                                ExecuteNQ($sqlInsertcharges);

                                if ($last_id7 != 0) {
                                    $sqlInsertchargesd = "INSERT INTO tblAppointmentsChargesInvoice(AppointmentDetailsID, ChargeName, ChargeAmount,AppointmentID,TaxGVANDM) VALUES 
									('" . $last_id7 . "', '" . $strSetName . "', '" . $totalamt . "','" . $last_idp . "','2')";
                                    $sqlInsertcharges . "<br/>";
                                    ExecuteNQ($sqlInsertchargesd);
                                }
                            }
                        }
                    }
                    unset($strChargeSet);
                }

                unset($strServicesused);
                unset($strChargeSet);

                $selpqitq = select("*", "tblAppointmentsDetailsInvoice", "AppointmentID='" . $last_idp . "'");
                foreach ($selpqitq as $SQ) {
                    $serr[] = $SQ['ServiceID'];
                }
                for ($j = 0; $j < count($serr); $j++) {
                    $counterservices++;
                    $Servicestaken = $serr[$j];

                    $selpqi = select("*", "tblAppointmentAssignEmployee", "AppointmentID='" . $last_idp . "' and ServiceID='" . $Servicestaken . "'");
                    $qtyu = $selpqi[0]['Qty'];
                    if ($qtyu == '1') {
                        $sqlUpdate1 = "UPDATE tblAppointmentAssignEmployee SET QtyParam='" . $qtyu . "' WHERE AppointmentID='" . $last_idp . "' and ServiceID='" . $Servicestaken . "'";
                        ExecuteNQ($sqlUpdate1);
                    } elseif ($qtyu == '0') {
// nothimng 
                    } else {
//echo 11;
                        $sqlUpdate1 = "UPDATE tblAppointmentAssignEmployee SET QtyParam='" . $qtyu . "' WHERE AppointmentID='" . $last_idp . "' and ServiceID='" . $Servicestaken . "'";
//echo $sqlUpdate1;
                        ExecuteNQ($sqlUpdate1);
//UPDATE PARAM 
                        for ($i = 1; $i < $qtyu; $i++) {

                            if ($FreeService != "0") {
                                $sqlInsert3ptr = "INSERT INTO tblAppointmentAssignEmployee(AppointmentID,ServiceID, Qty,QtyParam,MECID,Commission,FreeService) VALUES 
									('" . $last_idp . "', '" . $Servicestaken . "','" . $qtyu . "','" . $i . "','0','0','1')";
                                if ($DB->query($sqlInsert3ptr) === TRUE) {
                                    $last_id50 = $DB->insert_id;  //last id of tblAppointments insert
                                }
                            } else {
//echo $i;
                                $sqlInsert3ptr = "INSERT INTO tblAppointmentAssignEmployee(AppointmentID,ServiceID, Qty,QtyParam,MECID,Commission,FreeService) VALUES 
									('" . $last_idp . "', '" . $Servicestaken . "','" . $qtyu . "','" . $i . "','0','0','0')";
//echo $sqlInsert3ptr;
                                if ($DB->query($sqlInsert3ptr) === TRUE) {
                                    $last_id50 = $DB->insert_id;  //last id of tblAppointments insert
                                }
                            }
                        }
                    }
                }
                unset($strServices);

                $sqlInsert = "INSERT INTO  tblInvoice (AppointmentID, EmailMessageID, DateOfCreation) VALUES ('" . $last_idp . "', 'Null', 'Null')";
                $DB->query($sqlInsert);

                $seldata = select("*", "tblAppointments", "AppointmentID='" . $last_idp . "'");
//print_r($seldata);
                $strCustomerID = $seldata[0]['CustomerID'];
                $strStoreID = $seldata[0]['StoreID'];
                $AppointmentDate = $seldata[0]['AppointmentDate'];
                $timep = $seldata[0]['SuitableAppointmentTime'];
                $strSuitableAppointmentTime = $seldata[0]['SuitableAppointmentTime'];
                $time_in_12_hour_format = date("g:i a", strtotime($timep));
                $storename = isset($post_data['store_name']) ? $post_data['store_name'] : '';
                $CustomerMobileNo = isset($post_data['mobile']) ? $post_data['mobile'] : '';

                $weekday = date('l', strtotime($AppointmentDate)); // note: first arg to date() is lower-case L

                $dateforsms = FormatDatetime($time_in_12_hour_format);
                $Timebefore45min = date("H:i:s", strtotime("-45 minutes", strtotime($pqr)));

                $SMSContent = "Dear Customer Name, your appnt is scheduled for " . $weekday . " " . $dateforsms . " at " . $strSuitableAppointmentTime . " at " . $storename . ". Pls cancel or reschedule 45mins in advance to avoid cancellation charges.";
                $InsertSMSDetails = "Insert into tblAppointmentsReminderSMS (AppointmentID, StoreID, CustomerID, AppointmentDate, SuitableAppointmentTime,  SMSSendTime, Status, ContentSMS,SendSMSTo)values('" . $last_idp . "','" . $strStoreID . "','" . $strCustomerID . "','" . $AppointmentDate . "','" . $pqr . "','" . $Timebefore45min . "',0,'" . $SMSContent . "', '" . $CustomerMobileNo . "')";

                ExecuteNQ($InsertSMSDetails);
            }
        }
    }
    $DB->close();
}

?>