<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php require_once 'comparison_report_helper.php'; ?>
<?php
$strPageTitle = "Reconcillation Details | NailSpa";
$strDisplayTitle = "Reconcillation Details for NailSpa";
$strMenuID = "10";
$strMyTable = "tblAppointments";
$strMyTableID = "AppointmentID";
$strMyField = "AppointmentDate";
$strMyActionPage = "DisplayReconcillationDetail.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
//error_reporting(E_ALL);
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>

        <script>

            //$("#reconcillation_confirm").click(function () {
            function proceed_reconcillation() {
                var reconcille_val = [];
                $.each($("input[class='reconciile_check']:checked"), function () {
                    reconcille_val.push($(this).val());
                });

                //var emp_id = '<?php echo $strEmpID; ?>';
                if (confirm('Are you sure you want to Confirm?')) {
                    $.ajax({
                        url: "reconcillation_confirm.php",
                        type: 'post',
                        data: {"emp_apt": reconcille_val, "type": '2', "created_id": '<?php echo $strEmpID ?>'},
                        success: function (response) {
                            if (response == 1) {
                                $msg = "Reconcillation Confirmed Successfully!";
                                alert($msg);
                                window.location.reload(true);
                            } else {
                                $msg = "Failed To Confirm!";
                                alert($msg);
                                window.location.reload(true);
                            }
                        }
                    });
                }
            }


        </script>

        <script type="text/javascript">
            /* Datatables responsive */
            $(document).ready(function () {
                $('#datatable-responsive1').DataTable({
                    "order": [[1, "asc"]],
                    responsive: true
                });
            });

        </script>

    </head>

    <body>

        <div id="sb-site">


            <?php require_once("incOpenLayout.fya"); ?>


            <?php require_once("incLoader.fya"); ?>


            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>
                        <?php
                        if (isset($_GET["toandfrom"])) {
                            $appendq = "";
                            $strtoandfrom = $_GET["toandfrom"];
                            $arraytofrom = explode("-", $strtoandfrom);

                            $from = $arraytofrom[0];
                            $datetime = new DateTime($from);
                            $getfrom = $datetime->format('Y-m-d');


                            $to = $arraytofrom[1];
                            $datetime = new DateTime($to);
                            $getto = $datetime->format('Y-m-d');

                            if (!IsNull($from)) {
                                $sqlTempfrom = " WHERE DateOfAttendance>=Date('" . $getfrom . "')";
                            }

                            if (!IsNull($to)) {
                                $sqlTempto = " and DateOfAttendance<=Date('" . $getto . "')";
                            }
                            if (!IsNull($from) && !IsNull($to)) {
                                $date1 = $getfrom . '/' . $getto;
                                $emp_ids = 0;
                                $Store = isset($_GET["Store"]) ? $_GET["Store"] : 0;
                                if (isset($_GET['report_type']) && $_GET['report_type'] == '1') {
                                    $emp_sales_data = Employee_sale_com($date1, $Store, $emp_ids);
                                }
                            }

                            $period = new DatePeriod(
                                    new DateTime($getfrom), new DateInterval('P1D'), new DateTime($getto)
                            );


                            foreach ($period as $key => $value) {
                                $all_dates[] = $value->format('Y-m-d');
                            }

                            $all_dates[] = $getto;



                            $selp = select("*", "tblStores", "Status = '0'");
                            if (isset($selp) && is_array($selp) && count($selp) > 0) {
                                foreach ($selp as $val) {
                                    $store_in_arr[$val["StoreID"]] = $val["StoreID"];
                                }
                            }

                            $emp_append = '';
                            if (isset($store_in_arr) && is_array($store_in_arr) && count($store_in_arr) > 0) {
                                $emp_append = " AND StoreID IN(" . implode(",", $store_in_arr) . ")";
                            }
                            if (isset($_GET["Store"]) && !empty($_GET["Store"])) {
                                $emp_data = select("*", "tblEmployees", "StoreID='" . $_GET["Store"] . "' AND Status=0 " . $emp_append . " ORDER BY StoreID");
                            } else {
                                $emp_data = select("*", "tblEmployees", "Status=0 " . $emp_append . " ORDER BY StoreID");
                            }


                            if (isset($emp_data) && is_array($emp_data) && count($emp_data) > 0) {
                                foreach ($emp_data as $se_key => $se_value) {
                                    $store_emp_code[$se_value['EID']] = $se_value['EmployeeCode'];
                                }
                            }

                            if (isset($store_emp_code) && is_array($store_emp_code) && count($store_emp_code) > 0) {
                                $emp_code_in = "'" . implode("','", $store_emp_code) . "'";
                                $appendq = " AND EmployeeCode IN(" . $emp_code_in . ")";
                            }

                            $DB = Connect();
                            $attendq = "SELECT * FROM tblEmployeesRecords " . $sqlTempfrom . $sqlTempto . $appendq;

                            $attendq_exe = $DB->query($attendq);
                            while ($result = $attendq_exe->fetch_assoc()) {
                                $attendance_data[$result['EmployeeCode']][$result['DateOfAttendance']] = $result;
                            }

                            $store_data = select("*", "tblStores", "StoreID>0");
                            if (isset($store_data) && is_array($store_data) && count($store_data) > 0) {
                                foreach ($store_data as $skey => $svalue) {
                                    $store_name_data[$svalue['StoreID']] = $svalue['StoreName'];
                                }
                            }
                        }
                        ?>

                        <div class="panel">
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="example-box-wrapper">

                                        <div id="normal-tabs-1">
                                            <span class="form_result">&nbsp; <br>
                                            </span>
                                            <h4 class="title-hero"><center>Employee Attendance Report | NailSpa</center></h4>
                                            <form method="get" class="form-horizontal bordered-row" role="form" action="emp_attendance_report.php">
                                                <div class="form-group"><label for="" class="col-sm-4 control-label">Select date</label>
                                                    <div class="col-sm-4">
                                                        <div class="input-prepend input-group">
                                                            <span class="add-on input-group-addon">
                                                                <i class="glyph-icon icon-calendar"></i>
                                                            </span>
                                                            <input type="text" name="toandfrom" id="daterangepicker-example" class="form-control" value="<?php echo isset($strtoandfrom) ? $strtoandfrom : '' ?>">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Select Store</label>
                                                    <div class="col-sm-4">
                                                        <select name="Store" class="form-control">
                                                            <option value="0">All</option>
                                                            <?php
                                                            $selp = select("*", "tblStores", "Status = '0'");
                                                            foreach ($selp as $val) {
                                                                $strStoreName = $val["StoreName"];
                                                                $strStoreID = $val["StoreID"];
                                                                $store = $_GET["Store"];
                                                                if ($store == $strStoreID) {
                                                                    ?>
                                                                    <option  selected value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <option value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Report Type</label>
                                                    <div class="col-sm-4">
                                                        <input type="radio" name="report_type" value="1" id="report_short" <?php echo isset($_GET['report_type']) && $_GET['report_type'] == '1' ? 'checked' : 'checked'; ?>/><label for="report_short">Short View</label>&nbsp;&nbsp;
                                                        <input type="radio" name="report_type" value="2" id="report_detail" <?php echo isset($_GET['report_type']) && $_GET['report_type'] == '2' ? 'checked' : ''; ?>/><label for="report_detail">Detail View</label>
                                                    </div>
                                                </div>

                                                <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                    <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button> &nbsp;&nbsp;&nbsp;
                                                    <a class="btn btn-link" href="emp_attendance_report.php">Clear All Filter</a> &nbsp;&nbsp;&nbsp;
                                                </div>

                                            </form>
                                            <?php
                                            if (isset($_GET["toandfrom"])) {
                                                if (isset($all_dates) && is_array($all_dates) && count($all_dates) > 0 && count($all_dates) <= 31) {
                                                    ?>
                                                    <form method="post" action="emp_attendance_report_export.php">
                                                        <input type="hidden" value="<?php echo $_GET['toandfrom']; ?>" name="date">
                                                        <input type="hidden" value="<?php echo $_GET['Store']; ?>" name="store">
                                                        <input type="hidden" value="<?php echo isset($_GET['report_type']) ? $_GET['report_type'] : '1'; ?>" name="report_type">
                                                        <input type="submit" value="download csv">
                                                    </form>
                                            <!--<a href ="emp_attendance_report_export.php?date=<?php echo $_GET['toandfrom']; ?>&store=<?php echo $_GET['Store']; ?>" target="_blank"><button>Download CSV</button></a>-->
                                                    <table id = "datatable-responsive1" class="table table-bordered table-striped table-condensed cf table-hover" width="100%">
                                                        <thead class="cf">
                                                            <tr>
                                                                <th>Employee Name</th>
                                                                <th>Store name</th>

                                                                                                                                                                                <!--                                                                <th>Full Day</th>-->
                                                                <?php if (isset($_GET['report_type']) && $_GET['report_type'] == '2') { ?>
                                                                    <th>Total Full Day</th>
                                                                    <th>Total Half Day</th>
                                                                    <th>Total Absent Day</th>
                                                                    <?php
                                                                    if (isset($all_dates) && is_array($all_dates) && count($all_dates) > 0) {
                                                                        foreach ($all_dates as $dkey => $dvalue) {
                                                                            ?>

                                                                            <th><?php echo date('d M,Y', strtotime($dvalue)); ?></th>
                                                                            <?php
                                                                        }
                                                                    }
                                                                } else {
                                                                    ?>
                                                                    <th>Total Manual Full Day</th>
                                                                    <th>Total Manual Half Day</th>
                                                                    <th>Total Full Day</th>
                                                                    <th>Total Half Day</th>
                                                                    <th>Total Absent Day</th>
                                                                    <th>Manual Commission</th>
                                                                    <th>Total Commission</th>
                                                                <?php }
                                                                ?>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            if (isset($emp_data) && is_array($emp_data) && count($emp_data) > 0) {
                                                                foreach ($emp_data as $ekey => $evalue) {

                                                                    $emp_summary[$evalue['EID']]['full_day'] = 0;
                                                                    $emp_summary[$evalue['EID']]['half_day'] = 0;
                                                                    $emp_summary[$evalue['EID']]['absent_day'] = 0;
                                                                    $emp_summary[$evalue['EID']]['man_full_day'] = 0;
                                                                    $emp_summary[$evalue['EID']]['man_half_day'] = 0;
                                                                    if (isset($all_dates) && is_array($all_dates) && count($all_dates) > 0) {
                                                                        foreach ($all_dates as $dkey => $dvalue) {
                                                                            $emp_att = isset($attendance_data[$evalue['EmployeeCode']][$dvalue]) ? $attendance_data[$evalue['EmployeeCode']][$dvalue] : array();
                                                                            if (isset($_GET['report_type']) && $_GET['report_type'] == '1') {
                                                                                if (isset($emp_sales_data[$evalue['EID']][$dvalue])) {
                                                                                    if ($emp_att['check_in_by'] == $emp_att['emp_id']) {
                                                                                        if (isset($emp_summary[$evalue['EID']]['manual_com'])) {
                                                                                            $emp_summary[$evalue['EID']]['manual_com'] += $emp_sales_data[$evalue['EID']][$dvalue];
                                                                                        } else {
                                                                                            $emp_summary[$evalue['EID']]['manual_com'] = $emp_sales_data[$evalue['EID']][$dvalue];
                                                                                        }
                                                                                    } else {
                                                                                        if (isset($emp_summary[$evalue['EID']]['com'])) {
                                                                                            $emp_summary[$evalue['EID']]['com'] += $emp_sales_data[$evalue['EID']][$dvalue];
                                                                                        } else {
                                                                                            $emp_summary[$evalue['EID']]['com'] = $emp_sales_data[$evalue['EID']][$dvalue];
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }

                                                                            if (isset($emp_att['LoginTime']) && $emp_att['LoginTime'] != '00:00:00') {
                                                                                if ($emp_att['LoginTime'] != '00:00:00' && $emp_att['LogoutTime'] != '00:00:00') {
                                                                                    $intime = $dvalue . " " . $emp_att['LoginTime'];
                                                                                    $outtime = $dvalue . " " . $emp_att['LogoutTime'];
                                                                                    $date = new DateTime($intime);
                                                                                    $date2 = new DateTime($outtime);
                                                                                    $diffInSeconds = $date2->getTimestamp() - $date->getTimestamp();



                                                                                    if ($diffInSeconds >= 25200) {
                                                                                        if (isset($_GET['report_type']) && $_GET['report_type'] == '2') {
                                                                                            $emp_summary[$evalue['EID']]['full_day'] += 1;
                                                                                        } else {
                                                                                            if ($emp_att['check_in_by'] == $emp_att['emp_id']) {
                                                                                                $emp_summary[$evalue['EID']]['man_full_day'] += 1;
                                                                                            } else {
                                                                                                $emp_summary[$evalue['EID']]['full_day'] += 1;
                                                                                            }
                                                                                        }
                                                                                    } else {
                                                                                        if (isset($_GET['report_type']) && $_GET['report_type'] == '2') {
                                                                                            $emp_summary[$evalue['EID']]['half_day'] += 1;
                                                                                        } else {
                                                                                            if ($emp_att['check_in_by'] == $emp_att['emp_id']) {
                                                                                                $emp_summary[$evalue['EID']]['man_half_day'] += 1;
                                                                                            } else {
                                                                                                $emp_summary[$evalue['EID']]['half_day'] += 1;
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                } else {
                                                                                    if (isset($_GET['report_type']) && $_GET['report_type'] == '2') {
                                                                                        $emp_summary[$evalue['EID']]['full_day'] += 1;
                                                                                    } else {
                                                                                        if ($emp_att['check_in_by'] == $emp_att['emp_id']) {
                                                                                            $emp_summary[$evalue['EID']]['man_full_day'] += 1;
                                                                                        } else {
                                                                                            $emp_summary[$evalue['EID']]['full_day'] += 1;
                                                                                        }
                                                                                    }
                                                                                }
                                                                            } else {
                                                                                $emp_summary[$evalue['EID']]['absent_day'] += 1;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            if (isset($emp_data) && is_array($emp_data) && count($emp_data) > 0) {
                                                                // $fcount = 0;
                                                                foreach ($emp_data as $ekey => $evalue) {
                                                                    $store_name = isset($store_name_data[$evalue['StoreID']]) ? $store_name_data[$evalue['StoreID']] : '';
                                                                    ?>
                                                                    <tr>
                                                                        <td>
                                                                            <b>Name : </b><?php echo ucwords($evalue['EmployeeName']); ?></br>
                                                                            <b>Emp Code : </b><?php echo $evalue['EmployeeCode']; ?>
                                                                        </td>
                                                                        <td><?php echo ucwords($store_name); ?></td>
                                                                        <?php if (isset($_GET['report_type']) && $_GET['report_type'] == '2') { ?>
                                                                            <td><?php echo isset($emp_summary[$evalue['EID']]['full_day']) ? $emp_summary[$evalue['EID']]['full_day'] : 0; ?></td>
                                                                            <td><?php echo isset($emp_summary[$evalue['EID']]['half_day']) ? $emp_summary[$evalue['EID']]['half_day'] : 0; ?></td>
                                                                            <td><?php echo isset($emp_summary[$evalue['EID']]['absent_day']) ? $emp_summary[$evalue['EID']]['absent_day'] : 0; ?></td>
                                                                            <?php
                                                                            if (isset($all_dates) && is_array($all_dates) && count($all_dates) > 0) {
                                                                                foreach ($all_dates as $dkey => $dvalue) {
                                                                                    $emp_att = isset($attendance_data[$evalue['EmployeeCode']][$dvalue]) ? $attendance_data[$evalue['EmployeeCode']][$dvalue] : array();
                                                                                    ?>
                                                                                    <td>
                                                                                        <?php
                                                                                        if (isset($emp_att['LoginTime']) && $emp_att['LoginTime'] != '00:00:00') {
                                                                                            $intime = $dvalue . " " . $emp_att['LoginTime'];
                                                                                            $outtime = $dvalue . " " . $emp_att['LogoutTime'];
                                                                                            $date = new DateTime($intime);
                                                                                            $date2 = new DateTime($outtime);
                                                                                            $diffInSeconds = $date2->getTimestamp() - $date->getTimestamp();
                                                                                            ?>
                                                                                            Login Time : <?php echo $emp_att['LoginTime'] != '00:00:00' ? date('h:i a', strtotime($emp_att['LoginTime'])) : 0; ?><br>
                                                                                            Logout Time : <?php echo $emp_att['LogoutTime'] != '00:00:00' ? date('h:i a', strtotime($emp_att['LogoutTime'])) : 0; ?><br>
                                                                                            Login Store: <?php echo isset($store_name_data[$emp_att['login_store_id']]) ? $store_name_data[$emp_att['login_store_id']] : ''; ?><br>
                                                                                            Logout Store: <?php echo isset($store_name_data[$emp_att['login_store_id']]) ? $store_name_data[$emp_att['login_store_id']] : ''; ?><br>
                                                                                            Working Time : <?php
                                                                                            if ($emp_att['LoginTime'] != '00:00:00' && $emp_att['LogoutTime'] != '00:00:00') {
                                                                                                if ($diffInSeconds >= 25200) {
                                                                                                    echo "Full Day";
                                                                                                    // $fcount++;
                                                                                                } else {
                                                                                                    echo "Half Day";
                                                                                                }
                                                                                            } else {
                                                                                                echo "Full Day";
                                                                                            }
                                                                                            ?>
                                                                                        <?php } else { ?>
                                                                                            Absent
                                                                                        <?php } ?>
                                                                                    </td>
                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </tr>
                                                                    <?php } else {
                                                                        ?>
                                                                    <td><?php echo isset($emp_summary[$evalue['EID']]['man_full_day']) ? $emp_summary[$evalue['EID']]['man_full_day'] : 0; ?></td>
                                                                    <td><?php echo isset($emp_summary[$evalue['EID']]['man_half_day']) ? $emp_summary[$evalue['EID']]['man_half_day'] : 0; ?></td>
                                                                    <td><?php echo isset($emp_summary[$evalue['EID']]['full_day']) ? $emp_summary[$evalue['EID']]['full_day'] : 0; ?></td>
                                                                    <td><?php echo isset($emp_summary[$evalue['EID']]['half_day']) ? $emp_summary[$evalue['EID']]['half_day'] : 0; ?></td>
                                                                    <td><?php echo isset($emp_summary[$evalue['EID']]['absent_day']) ? $emp_summary[$evalue['EID']]['absent_day'] : 0; ?></td>
                                                                    <td><?php echo isset($emp_summary[$evalue['EID']]['manual_com']) ? $emp_summary[$evalue['EID']]['manual_com'] : 0; ?></td>
                                                                    <td><?php echo isset($emp_summary[$evalue['EID']]['com']) ? $emp_summary[$evalue['EID']]['com'] : 0; ?></td>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        ?>


                                                        </tbody>

                                                    </table>
                                                <?php } else { ?>
                                                    <h4>Only 31 Days Data can be displayed.</h4>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <h4>Please Select Date and Month.</h4>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php require_once 'incFooter.fya'; ?>
        </div>
    </body>
</html>