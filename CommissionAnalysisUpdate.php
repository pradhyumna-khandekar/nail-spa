<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Employee Commission Report | Nailspa";
$strDisplayTitle = "Employee Commission of Nailspa Experience";
$strMenuID = "2";
$strMyTable = "";
$strMyTableID = "";
$strMyField = "";
$strMyActionPage = "CommissionAnalysisUpdate.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights	



if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $strStep = Filter($_POST["step"]);

    if ($strStep == "add") {

    }

    if ($strStep == "edit") {

    }
}
?>


<?php
if (isset($_GET["toandfrom"])) {

    $strtoandfrom = $_GET["toandfrom"];
    $arraytofrom = explode("-", $strtoandfrom);

    $from = $arraytofrom[0];
    $datetime = new DateTime($from);
    $getfrom = $datetime->format('Y-m-d');


    $to = $arraytofrom[1];
    $datetime = new DateTime($to);
    $getto = $datetime->format('Y-m-d');

    if (!IsNull($from)) {
        $sqlTempfrom = " and Date(tblInvoiceDetails.OfferDiscountDateTime)>=Date('" . $getfrom . "')";
    }

    if (!IsNull($to)) {
        $sqlTempto = " and Date(tblInvoiceDetails.OfferDiscountDateTime)<=Date('" . $getto . "')";
    }
}
?>	


<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>

        <script type="text/javascript" src="assets/widgets/datepicker/datepicker.js"></script>
        <script type="text/javascript">
            /* Datepicker bootstrap */

            $(function () {
                "use strict";
                $('.bootstrap-datepicker').bsdatepicker({
                    format: 'mm-dd-yyyy'
                });


            });


        </script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker-demo.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/moment.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
    </head>
    <script>
        function printDiv(divName)
        {
            $("#heading").show();
            var divToPrint = document.getElementById("kakkabiryani");
            var htmlToPrint = '' +
                '<style type="text/css">' +
                'table th, table td {' +
                'border:1px solid #000;' +
                'padding;0.5em;' +
                '}' +
                '</style>';
            htmlToPrint += divToPrint.outerHTML;
            newWin = window.open("");
            newWin.document.write(htmlToPrint);
            newWin.print();
            newWin.close();
            // var printContents = document.getElementById(divName);
            // var originalContents = document.body.innerHTML;

            // document.body.innerHTML = printContents;

            // window.print();

            // document.body.innerHTML = originalContents;
        }

    </script>
    <body>
        <div id="sb-site">

        <?php // require_once("incOpenLayout.fya");       ?>
        <!----------commented by gandhali 5/9/18---------------->


<?php require_once("incLoader.fya"); ?>

        <div id="page-wrapper">
            <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

            <?php require_once("incLeftMenu.fya"); ?>

            <div id="page-content-wrapper">
                <div id="page-content">

                    <?php require_once("incHeader.fya"); ?>


                    <div id="page-title">
                        <h2><?= $strDisplayTitle ?></h2>
                    </div>
                    <style type="text/css">
                        @media print {
                            body * {
                                visibility: hidden;
                            }
                            #printarea * {
                                visibility: visible;
                            }
                            #printarea{
                                position: absolute;
                                left: 0;
                                top: 0;
                            }
                        }
                    </style>
                    <?php
                    //echo "Start";
                    //echo (microtime(true));
                    if (!isset($_GET["uid"])) {
                        ?>

                    <div class="panel">
                        <div class="panel">
                            <div class="panel-body">


                                <div class="example-box-wrapper">
                                    <div class="tabs">

                                        <div id="normal-tabs-1">

                                            <span class="form_result">&nbsp; <br>
                                            </span>

                                            <div class="panel-body">
                                                <h3 class="title-hero">Employee Commission</h3>

                                                <form method="get" class="form-horizontal bordered-row" role="form">

                                                    <div class="form-group"><label for="" class="col-sm-4 control-label">Select Date Range</label>
                                                        <div class="col-sm-4">
                                                            <div class="input-prepend input-group">
                                                                <span class="add-on input-group-addon">
                                                                    <i class="glyph-icon icon-calendar"></i>
                                                                </span>
                                                                <input type="text" name="toandfrom" id="daterangepicker-example" class="form-control" value="<?= $strtoandfrom ?>">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group"><label for="" class="col-sm-4 control-label">Select Employee to get commission</label>
                                                        <div class="col-sm-4">
                                                            <select class="form-control required"  name="store">
                                                                <?php
                                                                if ($strStore == '0') {
                                                                    $strStatement = "";
                                                                } else {
                                                                    $strStatement = " and StoreID='$strStore'";
                                                                }


                                                                $selp = select("*", "tblEmployees", "Status='0' $strStatement");
                                                                //$selp = select("*", "tblEmployees", "Status='0'");


                                                                foreach ($selp as $val) {
                                                                    $EIDD = $val["EID"];
                                                                    $EMPNAME = $val["EmployeeName"];
                                                                    $EID = $_GET["store"];
                                                                    if ($EID == $EIDD) {
                                                                        $selpT = select("*", "tblEmployees", "EID='" . $EID . "'");
                                                                        $EmployeeName = $selpT[0]['EmployeeName'];
                                                                        ?>
                                                                <option  selected value="<?= $EID ?>" ><?= $EmployeeName ?></option>
                                                                <?php
                                                            } else {
                                                                if ($EIDD == "35" || $EIDD == "8" || $EIDD == "6" || $EIDD == "34" || $EIDD == "22" || $EIDD == "49" || $EIDD == "43") {
                                                                    // List of managers, HO and Audit whose details need not to be shown
                                                                } else {
                                                                    ?>
                                                                <option value="<?= $EIDD ?>"><?= $EMPNAME ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                            </select>
                                                        </div>
                                                    </div>




                                                    <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                        <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                                        &nbsp;&nbsp;&nbsp;
                                                        <a class="btn btn-link" href="CommissionAnalysisUpdate.php">Clear All Filter</a> &nbsp;&nbsp;&nbsp;

                                                            <?php
                                                            $datedrom = $_GET["toandfrom"];
                                                            if ($datedrom != "") {
                                                                ?>
                                                        <button onclick="printDiv('printarea')" class="btn btn-blue-alt">Print<div class="ripple-wrapper"></div></button>
                                                        <?php
                                                    }
                                                    ?>
                                                    </div>

                                                </form>


                                                <div class="example-box-wrapper">
                                                    <?php
                                                    $EID = $_GET["store"];

                                                    $datedrom = $_GET["toandfrom"];
                                                    if ($datedrom != "") {
                                                        $EID = $_GET["store"];
                                                        if ($EID == '0') {
                                                            $emp_id = 'All';
                                                        } else {
                                                            $selpT = select("*", "tblEmployees", "EID='" . $EID . "'");
                                                            $EmployeeName = $selpT[0]['EmployeeName'];
                                                            $emp_id = $EmployeeName;
                                                        }
                                                        ?>
                                                    <br><br>
                                                    <saif id="kakkabiryani">
                                                        <h2 class="title-hero" id="heading" style="display:none"><center>Report Employee Commission</center></h2>
                                                        <span>
                                                            <h3 >Date Range selected : FROM - <?= $getfrom ?> / TO - <?= $getto ?> / Employee Name - <?= $emp_id ?></h3>

                                                        </span>

                                                                <?php
                                                                $DB = Connect();

                                                                $all_offers = select("OfferID,OfferName,OfferCode,Type,TypeAmount", "tblOffers", "OfferID >0");

                                                                if (isset($all_offers) && is_array($all_offers) && count($all_offers) > 0) {
                                                                    foreach ($all_offers as $offkey => $offvalue) {
                                                                        $all_offers_name[$offvalue['OfferID']] = $offvalue;
                                                                    }
                                                                }



                                                                $EmployeeID = $_GET["store"];
                                                                if (!empty($EmployeeID)) {
                                                                    $sql = "select EID, EmployeeName, EmployeeEmailID, EmpPercentage, EmployeeMobileNo from tblEmployees where Status='0' and EID='" . $EmployeeID . "'";
                                                                    $empq = " AND tblEmployees.Status='0' and tblEmployees.EID='" . $EmployeeID . "'";
                                                                } else {
                                                                    $sql = "select EID, EmployeeName, EmployeeEmailID, EmpPercentage, EmployeeMobileNo from tblEmployees where Status='0'";
                                                                    $empq = " AND tblEmployees.Status='0'";
                                                                }


                                                                $RS = $DB->query($sql);
                                                                while ($emp_row = $RS->fetch_assoc()) {
                                                                    $emp_list[] = $emp_row;
                                                                }


                                                                $emp_count = count($emp_list);

                                                                if (isset($_GET['page']) && $_GET['page'] != '') {
                                                                    $search_emp[0] = $emp_list[$_GET['page']];
                                                                    $get_qry = "";
                                                                    if (isset($_GET['toandfrom']) && $_GET['toandfrom'] != '') {
                                                                        $get_qry .= "?toandfrom=" . $_GET['toandfrom'];
                                                                    }
                                                                    $previous_key = $_GET['page'] - 1;
                                                                    if (isset($emp_list[$previous_key])) {
                                                                        $page_qry = "&page=" . $previous_key;
                                                                        echo " <a class='btn btn-blue-alt' href='CommissionAnalysisUpdate.php" . $get_qry . $page_qry . "'>Previous</a>";
                                                                    }

                                                                    $next_key = $_GET['page'] + 1;
                                                                    if (isset($emp_list[$next_key])) {
                                                                        $page_qry = "&page=" . $next_key;
                                                                        echo " <a class='btn btn-blue-alt' href='CommissionAnalysisUpdate.php" . $get_qry . $page_qry . "'>Next</a>";
                                                                    }
                                                                } else {
                                                                    $search_emp[0] = $emp_list[0];
                                                                    if (count($emp_list) > 1) {
                                                                        $next_id = 1;
                                                                        $get_qry = "";
                                                                        if (isset($_GET['toandfrom']) && $_GET['toandfrom'] != '') {
                                                                            $get_qry .= "?toandfrom=" . $_GET['toandfrom'] . "&page=" . $next_id;
                                                                        }

                                                                        echo "<a class='btn btn-blue-alt' href='CommissionAnalysisUpdate.php" . $get_qry . "'>Next</a>";
                                                                    }
                                                                }


                                                                if ($RS->num_rows > 0) {

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                /*
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * get all emoloyee invoice
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 */
                                                                    $sqldetails = "SELECT tblEmployees.EID,tblInvoiceDetails.CustomerID,tblAppointmentAssignEmployee.AppointmentID,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                tblAppointmentAssignEmployee.Qty,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                tblAppointmentAssignEmployee.ServiceID,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                tblAppointmentAssignEmployee.Commission,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                tblAppointmentAssignEmployee.QtyParam,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                tblInvoiceDetails.OfferDiscountDateTime,tblEmployees.StoreID
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                FROM tblEmployees
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        join tblAppointmentAssignEmployee on tblEmployees.EID = tblAppointmentAssignEmployee.MECID
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                join tblInvoiceDetails on tblAppointmentAssignEmployee.AppointmentID=tblInvoiceDetails.AppointmentId
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        join tblAppointments on tblAppointments.AppointmentID=tblInvoiceDetails.AppointmentId
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                where tblAppointmentAssignEmployee.AppointmentID!='NULL'
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                and tblInvoiceDetails.OfferDiscountDateTime!='NULL'
                                                                    $sqlTempfrom
                                                                    $sqlTempto
                                                                    $empq and tblEmployees.EID = '" . $search_emp[0]['EID'] . "' AND tblAppointments.Status=2 AND tblAppointments.IsDeleted=0
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        group by tblAppointmentAssignEmployee.AppointmentID,ServiceID,QtyParam";


                                                                    $RSdetails = $DB->query($sqldetails);

                                                                    while ($rowdetails = $RSdetails->fetch_assoc()) {
                                                                        $invoice_data[$rowdetails['EID']][] = $rowdetails;
                                                                    }

                                                                    if (isset($invoice_data) && is_array($invoice_data) && count($invoice_data) > 0) {
                                                                        $service_ids = array();
                                                                        $store_ids = array();
                                                                        $appointment_ids = array();
                                                                        $customer_ids = array();

                                                                        foreach ($invoice_data as $detailkey => $detailvalue) {
                                                                            foreach ($detailvalue as $invkey => $invvalue) {
                                                                                $service_ids[$invvalue["ServiceID"]] = $invvalue["ServiceID"];
                                                                                $store_ids[$invvalue["StoreID"]] = $invvalue["StoreID"];
                                                                                $appointment_ids[$invvalue["AppointmentID"]] = $invvalue["AppointmentID"];
                                                                                $customer_ids[$invvalue["CustomerID"]] = $invvalue["CustomerID"];
                                                                            }
                                                                        }


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    /*
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     * get service name
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     */
                                                                        if (isset($service_ids) && is_array($service_ids) && count($service_ids) > 0) {
                                                                            $service_in_ids = implode(",", $service_ids);
                                                                            $service_q = "SELECT ServiceID,ServiceName FROM tblServices WHERE ServiceID IN(" . $service_in_ids . ")";
                                                                            $service_exe = $DB->query($service_q);
                                                                            while ($servdetails = $service_exe->fetch_assoc()) {
                                                                                $all_services[] = $servdetails;
                                                                            }

                                                                            if (isset($all_services) && is_array($all_services) && count($all_services) > 0) {
                                                                                foreach ($all_services as $serkey => $servalue) {
                                                                                    $sep[$servalue['ServiceID']] = $servalue['ServiceName'];
                                                                                }
                                                                            }
                                                                        }


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    /*
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     * get store name
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     */
                                                                        if (isset($store_ids) && is_array($store_ids) && count($store_ids) > 0) {
                                                                            $store_in_ids = implode(",", $store_ids);
                                                                            $store_q = "SELECT StoreID,StoreName FROM tblStores WHERE StoreID IN(" . $store_in_ids . ")";
                                                                            $store_exe = $DB->query($store_q);
                                                                            while ($storedetails = $store_exe->fetch_assoc()) {
                                                                                $all_store[] = $storedetails;
                                                                            }
                                                                            if (isset($all_store) && is_array($all_store) && count($all_store) > 0) {
                                                                                foreach ($all_store as $stokey => $stovalue) {
                                                                                    $stpp[$stovalue['StoreID']] = $stovalue['StoreName'];
                                                                                }
                                                                            }
                                                                        }

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    /*
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     * get invoice number
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     */
                                                                        if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                                                                            $apt_in_ids = implode(",", $appointment_ids);
                                                                            $apt_q = "SELECT InvoiceID,AppointmentID FROM tblInvoice WHERE AppointmentID IN(" . $apt_in_ids . ")";
                                                                            $apt_exe = $DB->query($apt_q);
                                                                            while ($aptdetails = $apt_exe->fetch_assoc()) {
                                                                                $all_apt[] = $aptdetails;
                                                                            }

                                                                            if (isset($all_apt) && is_array($all_apt) && count($all_apt) > 0) {
                                                                                foreach ($all_apt as $aptkey => $aptvalue) {
                                                                                    $sql_invoice_number[$aptvalue['AppointmentID']] = $aptvalue['InvoiceID'];
                                                                                }
                                                                            }
                                                                        }

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    /*
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     * get AppointmentDate
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     */
                                                                        if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                                                                            $apt_in_ids = implode(",", $appointment_ids);
                                                                            $aptdate_q = "SELECT AppointmentDate,AppointmentID FROM tblAppointments WHERE AppointmentID IN(" . $apt_in_ids . ")";
                                                                            $aptdate_exe = $DB->query($aptdate_q);
                                                                            while ($aptdatedetails = $aptdate_exe->fetch_assoc()) {
                                                                                $alldate_apt[] = $aptdatedetails;
                                                                            }
                                                                            if (isset($alldate_apt) && is_array($alldate_apt) && count($alldate_apt) > 0) {
                                                                                foreach ($alldate_apt as $aptdkey => $aptdvalue) {
                                                                                    $sql_apt_date[$aptdvalue['AppointmentID']] = $aptdvalue['AppointmentDate'];
                                                                                }
                                                                            }
                                                                        }

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    /*
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     * get customer
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     */
                                                                        if (isset($customer_ids) && is_array($customer_ids) && count($customer_ids) > 0) {
                                                                            $cust_in_ids = implode(",", $customer_ids);

                                                                            $cust_q = "SELECT CustomerID,CustomerFullName FROM tblCustomers WHERE CustomerID IN(" . $cust_in_ids . ")";
                                                                            $cust_exe = $DB->query($cust_q);
                                                                            while ($custdetails = $cust_exe->fetch_assoc()) {
                                                                                $all_cust[] = $custdetails;
                                                                            }
                                                                            if (isset($all_cust) && is_array($all_cust) && count($all_cust) > 0) {
                                                                                foreach ($all_cust as $custkey => $custvalue) {
                                                                                    $sql_customer[$custvalue['CustomerID']] = $custvalue['CustomerFullName'];
                                                                                }
                                                                            }
                                                                        }

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    /*
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     * get service amt
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     */

                                                                        if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                                                                            $apt_in_ids = implode(",", $appointment_ids);
                                                                            $aptdate_q = "SELECT * FROM tblAppointmentsDetailsInvoice WHERE AppointmentID IN(" . $apt_in_ids . ") AND PackageService=0";
                                                                            $aptdate_exe = $DB->query($aptdate_q);
                                                                            while ($aptdatedetails = $aptdate_exe->fetch_assoc()) {
                                                                                $all_apt_service_amt[] = $aptdatedetails;
                                                                            }
                                                                            if (isset($all_apt_service_amt) && is_array($all_apt_service_amt) && count($all_apt_service_amt) > 0) {
                                                                                foreach ($all_apt_service_amt as $servdkey => $servvalue) {
                                                                                    $tall_ser_amt[$servvalue['AppointmentID']][$servvalue['ServiceID']] = round(($servvalue['service_amount_deduct_dis'] / $servvalue['qty']),2);
                                                                                    $all_ser_amt[$servvalue['AppointmentID']][$servvalue['ServiceID']] = $servvalue['ServiceAmount'];
                                                                                    $super_gv_deduct[$servvalue['AppointmentID']][$servvalue['ServiceID']] = $servvalue['super_gv_discount'] / $servvalue['qty'];
                                                                                    $package_discount_deduct[$servvalue['AppointmentID']][$servvalue['ServiceID']] = round(($servvalue['package_discount'] / $servvalue['qty']),2);
                                                                                }
                                                                            }
                                                                        }

                                                                        //echo '<pre>';print_r($all_ser_amt);exit;

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    /*
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     * get discount
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     */
                                                                        if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                                                                            $apt_in_ids = implode(",", $appointment_ids);
                                                                            $aptdate_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ")";
                                                                            $aptdate_exe = $DB->query($aptdate_q);
                                                                            while ($disdetails = $aptdate_exe->fetch_assoc()) {
                                                                                $all_apt_dis_amt[] = $disdetails;
                                                                            }

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        /*
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         * Get Appointment Offer AMount
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         */
                                                                            $apt_in_ids = implode(",", $appointment_ids);
                                                                            $aptoffer_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID,OfferID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ") AND OfferAmount > 0";
                                                                            $aptoffer_exe = $DB->query($aptoffer_q);
                                                                            while ($offeretails = $aptoffer_exe->fetch_assoc()) {
                                                                                $apt_service_count[$offeretails['AppointmentID']][$offeretails['ServiceID']] = $offeretails['OfferAmount'];
                                                                            }

                                                                            $apt_in_ids = implode(",", $appointment_ids);
                                                                            $aptoffer_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID,OfferID,AppointmentMembershipDiscountID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ") AND OfferAmount > 0";
                                                                            $aptoffer_exe = $DB->query($aptoffer_q);

                                                                            while ($offeretails = $aptoffer_exe->fetch_assoc()) {
                                                                                if (isset($all_offers_name[$offeretails['OfferID']]) && $all_offers_name[$offeretails['OfferID']]['Type'] == 1 && $offeretails['OfferID'] > 0) {
                                                                                    $offer_service_cnt = isset($apt_service_count[$offeretails['AppointmentID']]) ? count($apt_service_count[$offeretails['AppointmentID']]) : 1;
                                                                                    //$all_apt_off_dis[$offeretails['AppointmentID']][$offeretails['ServiceID']] = $offeretails['OfferAmount'] / $offer_service_cnt;
                                                                                    $all_apt_off_dis[$offeretails['AppointmentID']][$offeretails['ServiceID']] = round($offeretails['OfferAmount'], 2);
                                                                                } else {
                                                                                    $all_apt_off_dis[$offeretails['AppointmentID']][$offeretails['ServiceID']] = round($offeretails['OfferAmount'], 2);
                                                                                }
                                                                            }

                                                                            if (isset($all_apt_dis_amt) && is_array($all_apt_dis_amt) && count($all_apt_dis_amt) > 0) {
                                                                                foreach ($all_apt_dis_amt as $diskey => $disvalue) {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                /*
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * Get Appointment service count
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 */
                                                                                    $extra_offer_amount = 0;

                                                                                    if (isset($all_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']]) && $all_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']] > 0) {
                                                                                        $extra_offer_amount = $all_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']];
                                                                                    }
                                                                                    //echo '<br>extra_offer_amount=' . $extra_offer_amount;
                                                                                    //echo '<br>member_amount=' . $disvalue['MemberShipAmount'];
                                                                                    $all_dis_amt[$disvalue['AppointmentID']][$disvalue['ServiceID']]['MemberShipAmount'] = $disvalue['MemberShipAmount'] + round($extra_offer_amount, 2);
                                                                                    $all_apt_mem_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']] = $disvalue['MemberShipAmount'];
                                                                                    //$all_dis_amt[$disvalue['AppointmentID']][$disvalue['ServiceID']]['OfferAmount'] = $disvalue['OfferAmount'];
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }


                                                                if (isset($search_emp) && is_array($search_emp) && count($search_emp) > 0) {
                                                                    foreach ($search_emp as $key => $row) {
                                                                        $strEID = $row["EID"];
                                                                        $strEmployeeName = $row["EmployeeName"];
                                                                        $strEmployeeEmailID = $row["EmployeeEmailID"];
                                                                        $strEmpPercentage = $row["EmpPercentage"];
                                                                        $strEmployeeMobileNo = $row["EmployeeMobileNo"];

                                                                        $TotalAfterDivideSale = '';
                                                                        $strTotalAmount = '';
                                                                        $TotalFinalDiscount = '';
                                                                        $TotalUltimateSale = '';
                                                                        $grand_package_discount = 0;
                                                                        $ComFinal = '';
                                                                        ?>
                                                        <div id="printdata">
                                                        <?php
                                                        echo "<saif style='float:right'><b>Name :</b> " . $strEmployeeName . "</saif>";
                                                        echo "<b>Email :</b> " . $strEmployeeEmailID . "<br>";
                                                        echo "<saif style='float:right'><b>Commission Percentage :</b> " . $strEmpPercentage . "</saif>";
                                                        echo "<b>Mobile no :</b> " . $strEmployeeMobileNo . "<br>";
                                                        echo "<hr>";
                                                        ?>
                                                        <table id="printdata" class="table table-striped table-bordered responsive no-wrap printdata" cellspacing="0" width="100%">

                                                        <thead>
                                                            <tr>
                                                                <th>Sr. No.</th>
                                                                <th># Invoice No</th>
                                                                <th>Store</th>
                                                                <th>Client Name</th>
                                                                <th>Commission Date</th>
                                                                <th>Service(s) Done</th>
                                                                <th>Service Amount</th>
                                                                <th>Final Amount</th>
                                                                <th>Super GV Discount</th>
                                                                <th>Package Discount</th>
                                                                <th>Discount</th>
                                                                <th>Total Sales</th>
                                                                <th>Commission Amount</th>
                                                                <th>Commission Type</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody>

                                                                        <?php
                                                                        if ($_SERVER['REMOTE_ADDR'] == "111.119.219.70") {
                                                                            //echo $sqldetails;
                                                                        } else {

                                                                        }



                                                                        $counter = 0;
                                                                        $strSID = "";
                                                                        $qty = "";
                                                                        $strSAmount = "";
                                                                        $strAmount = "";
                                                                        $strCommission = "";
                                                                        $FinalDAmount = '';
                                                                        $FinalDiscount = '';
                                                                        $UltimateSale = '';
                                                                        $AfterDivideSale = '';
                                                                        $CommssionFinal = "";
                                                                        $totalgv_dis = 0;
                                                                        //echo("Error description: " . mysqli_error($DB));

                                                                        if (isset($invoice_data[$strEID]) && is_array($invoice_data[$strEID]) && count($invoice_data[$strEID]) > 0) {

                                                                            foreach ($invoice_data[$strEID] as $rowkey => $rowdetails) {
                                                                                if (isset($emp_service_qty[$rowdetails['AppointmentID']][$rowdetails["ServiceID"]][$strEID])) {
                                                                                    $emp_service_qty[$rowdetails['AppointmentID']][$rowdetails["ServiceID"]][$strEID] += 1;
                                                                                } else {
                                                                                    $emp_service_qty[$rowdetails['AppointmentID']][$rowdetails["ServiceID"]][$strEID] = 1;
                                                                                }
                                                                            }
                                                                        }
                                                                        if (isset($invoice_data[$strEID]) && is_array($invoice_data[$strEID]) && count($invoice_data[$strEID]) > 0) {

                                                                            foreach ($invoice_data[$strEID] as $rowkey => $rowdetails) {
                                                                                $counter ++;
                                                                                $strEIDa = $rowdetails["EID"];
                                                                                $strAID = $rowdetails["AppointmentID"];
                                                                                $strSID = $rowdetails["ServiceID"];
                                                                                $CustomerID = $rowdetails['CustomerID'];
                                                                                $qty = $rowdetails["Qty"];
                                                                                $AfterDivideSale = 0;

                                                                                //$ser_amt = select("ServiceAmount", "tblappointmentsdetailsinvoice", "AppointmentID='" . $strAID . "' AND ServiceID='" . $strSID . "'");
                                                                                //$strAmount = $ser_amt[0]['ServiceAmount'];
                                                                                $strAmount = isset($all_ser_amt[$strAID][$strSID]) ? $all_ser_amt[$strAID][$strSID] : '';

                                                                                
                                                                                

                                                                                $strSAmount = $strAmount;
                                                                                $strCommission = $rowdetails["Commission"];
                                                                                $StoreIDd = $rowdetails["StoreID"];

                                                                                $strTotalAmount += $strSAmount;  //Total of Service sale amount
                                                                                // Service Name Yogita query
                                                                                //$sep = select("ServiceName", "tblServices", "ServiceID='" . $strSID . "'");
                                                                                //$servicename = $sep[0]['ServiceName'];
                                                                                $servicename = isset($sep[$strSID]) ? $sep[$strSID] : '';

                                                                                // Store Name Yogita query
                                                                                //$stpp = select("StoreName", "tblStores", "StoreID='" . $StoreIDd . "'");
                                                                                //$StoreName = $stpp[0]['StoreName'];
                                                                                $StoreName = isset($stpp[$StoreIDd]) ? $stpp[$StoreIDd] : '';

                                                                                // Invoice no Yogita query
                                                                                //$sql_invoice_number = select("InvoiceID", "tblInvoice", "AppointmentID='" . $strAID . "'");
                                                                                //$Invoice_Number = $sql_invoice_number[0]['InvoiceID'];
                                                                                $Invoice_Number = isset($sql_invoice_number[$strAID]) ? $sql_invoice_number[$strAID] : '';

                                                                                // Customer ID Yogita query
                                                                                //$sql_customer = select("CustomerID,AppointmentDate", "tblAppointments", "AppointmentID='" . $strAID . "'");
                                                                                //$AppointmentDate = $sql_customer[0]['AppointmentDate'];
                                                                                $AppointmentDate = isset($sql_apt_date[$strAID]) ? $sql_apt_date[$strAID] : '';


                                                                                // Customer name Yogita query
                                                                                //$sql_customers = select("CustomerFullName", "tblCustomers", "CustomerID='" . $CustomerID . "'");
                                                                                //$CustomerFullName = $sql_customers[0]['CustomerFullName'];
                                                                                $CustomerFullName = isset($sql_customer[$CustomerID]) ? $sql_customer[$CustomerID] : '';


                                                                                if ($strCommission == "1") {
                                                                                    $AfterDivideSale = $strSAmount;
                                                                                    $strCommissionType = '<span class="bs-label label-success">Alone</span>';
                                                                                } elseif ($strCommission == "2") {
                                                                                    $AfterDivideSale = ($strSAmount);
                                                                                    $strCommissionType = '<span class="bs-label label-blue-alt">Split</span>';
                                                                                }



                                                                                $rowdiscount = isset($all_dis_amt[$strAID][$strSID]) ? $all_dis_amt[$strAID][$strSID] : array();
                                                                                if (isset($rowdiscount) && is_array($rowdiscount) && count($rowdiscount) > 0) {
                                                                                    $strOfferAmount = $rowdiscount["OfferAmount"];
                                                                                    $strDiscountAmount = $rowdiscount["MemberShipAmount"];

                                                                                    if ($strOfferAmount > 0) {
                                                                                        $FinalDAmount = $strOfferAmount;
                                                                                    } elseif ($strDiscountAmount > 0) {
                                                                                        $FinalDAmount = $strDiscountAmount;
                                                                                    }
                                                                                } else {
                                                                                    $FinalDAmount = "0";
                                                                                }

                                                                                // $FinalDiscount = $FinalDAmount / $qty;
                                                                                $FinalDiscount = isset($all_apt_mem_dis[$strAID][$strSID]) ? ($all_apt_mem_dis[$strAID][$strSID]) / $qty : 0;
                                                                                $FinalOtherDiscount = isset($all_apt_off_dis[$strAID][$strSID]) ? ($all_apt_off_dis[$strAID][$strSID]) / $qty : 0;


                                                                                $UltimateSale = $AfterDivideSale - $FinalDiscount - $FinalOtherDiscount - $super_gv_deduct_amount;
                                                                                $UltimateSale = round($UltimateSale, 2);

                                                                                $emp_ser_count = isset($emp_service_qty[$strAID][$strSID][$strEID]) ? $emp_service_qty[$strAID][$strSID][$strEID] : 1;
                                                                                $UltimateSale = isset($tall_ser_amt[$strAID][$strSID]) ? $tall_ser_amt[$strAID][$strSID] * $emp_ser_count : '';

$package_discount_deduct_amount = isset($package_discount_deduct[$strAID][$strSID]) ? $package_discount_deduct[$strAID][$strSID] * $emp_ser_count : '0';
$super_gv_deduct_amount = isset($super_gv_deduct[$strAID][$strSID]) ? $super_gv_deduct[$strAID][$strSID] * $emp_ser_count : '0';

                                                                                $all_discount = $FinalDiscount + $FinalOtherDiscount;
                                                                                if ($strCommission == 2) {
                                                                                    $UltimateSale = $UltimateSale / 2;
                                                                                    $AfterDivideSale = $AfterDivideSale / 2;
                                                                                    $super_gv_deduct_amount = $super_gv_deduct_amount / 2;
                                                                                    $all_discount = $all_discount / 2;
                                                                                }
                                                                                //$TotalUltimateSale += $UltimateSale; //Total of discounted amount
                                                                                $TotalAfterDivideSale += $AfterDivideSale;
                                                                                $totalgv_dis += $super_gv_deduct_amount;
                                                                                $TotalFinalDiscount += $all_discount;


                                                                                $CommssionFinal = ($UltimateSale / 100) * $strEmpPercentage;
                                                                                //Calculation for commission
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        /* if ($strCommission == "1") {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          $CommssionFinal = ($UltimateSale / 100) * $strEmpPercentage;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          } elseif ($strCommission == "2") {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          $CommssionFinal = ($UltimateSale / 200) * $strEmpPercentage;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          } */
                                                                                //$ComFinal += $CommssionFinal; //Total of Commission
                                                                                ?>
                                                                                <?php

                                                                                $final_id = $rowdetails['AppointmentID'] . '-' . $rowdetails["ServiceID"];
                                                                                $final_res[$final_id]['Invoice_Number'] = $Invoice_Number;
                                                                                $final_res[$final_id]['StoreName'] = $StoreName;
                                                                                $final_res[$final_id]['CustomerFullName'] = $CustomerFullName;
                                                                                $final_res[$final_id]['AppointmentDate'] = date("d/m/Y", strtotime($AppointmentDate));
                                                                                $final_res[$final_id]['servicename'] = $servicename;
                                                                                $final_res[$final_id]['UltimateSale'] = $UltimateSale;
                                                                                $final_res[$final_id]['package_discount_deduct_amount'] = $package_discount_deduct_amount;
                                                                                $final_res[$final_id]['AfterDivideSale'] = isset($final_res[$final_id]['AfterDivideSale']) ? $final_res[$final_id]['AfterDivideSale']+ $AfterDivideSale : $AfterDivideSale;
                                                                                $final_res[$final_id]['super_gv_deduct_amount'] = isset($final_res[$final_id]['super_gv_deduct_amount']) ? $final_res[$final_id]['super_gv_deduct_amount']+ $super_gv_deduct_amount : $super_gv_deduct_amount;
                                                                                $final_res[$final_id]['all_discount'] = isset($final_res[$final_id]['all_discount']) ? $final_res[$final_id]['all_discount']+ $all_discount : $all_discount;
                                                                                $final_res[$final_id]['CommssionFinal'] = $CommssionFinal;
                                                                                $final_res[$final_id]['strCommissionType'] = $strCommissionType;
                                                                                ?>
                                                                                <?php /* <tr id="my_data_tr_<?= $counter ?>">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <td><?= $counter; ?></td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <td>#<?= $Invoice_Number ?></td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <td><?= $StoreName ?></td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <td><?= $CustomerFullName ?></td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <td><?= date("d/m/Y", strtotime($AppointmentDate)); ?></td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <td><?= $servicename ?></td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <td>Rs.<?= $UltimateSale ?>/-</td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <td>Rs.<?= $AfterDivideSale ?>/-</td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <td>Rs.<?php echo $super_gv_deduct_amount; ?>/-</td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <td>Rs.<?= $all_discount; ?>/-</td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <td>Rs.<?= $UltimateSale ?>/-</td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <td>Rs.<?= $CommssionFinal ?>/-</td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <td><font color="red"><?= $strCommissionType ?></font></td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </tr> */ ?>
                                                                                <?php
                                                                            }

                                                                            $row_counter = 1;
                                                                            if(isset($final_res) && is_array($final_res) && count($final_res) >0){
                                                                                foreach ($final_res as $key => $val) {  ?>
                                                        <tr id="my_data_tr_<?= $row_counter ?>">
                                                            <td><?= $row_counter; ?></td>
                                                            <td>#<?= $val['Invoice_Number'] ?></td>
                                                            <td><?= $val['StoreName'] ?></td>
                                                            <td><?= $val['CustomerFullName'] ?></td>
                                                            <td><?= date("d/m/Y", strtotime($val['AppointmentDate'])); ?></td>
                                                            <td><?= $val['servicename'] ?></td>
                                                            <td>Rs.<?= $val['UltimateSale'] ?>/-</td>
                                                            <td>Rs.<?= $val['AfterDivideSale'] ?>/-</td>
                                                            <td>Rs.<?php echo $val['super_gv_deduct_amount']; ?>/-</td>
                                                            <td>Rs.<?php echo $val['package_discount_deduct_amount']; ?>/-</td>
                                                            <td>Rs.<?= $val['all_discount']; ?>/-</td>
                                                            <td>Rs.<?= $val['UltimateSale'] ?>/-</td>
                                                            <td>Rs.<?= $val['CommssionFinal'] ?>/-</td>
                                                            <td><font color="red"><?= $val['strCommissionType'] ?></font></td>
                                                        </tr>
                                                        <?php
                                                        $ComFinal += $val['CommssionFinal'];
                                                        $grand_package_discount += $val['package_discount_deduct_amount'];
                                                        $TotalUltimateSale = $TotalUltimateSale + $val['UltimateSale'];
                                                        $row_counter++; }
                                                }
                                                ?>
                                                        <tr>
                                                            <td colspan="6"><b><center>Total commission for selected period : </center></b></td>

                                                            <td><b>Rs.<?= $TotalUltimateSale ?>/-</b></td>
                                                            <td><b>Rs.<?= $TotalAfterDivideSale ?>/-</b></td>
                                                            <td><b>Rs.<?php echo $totalgv_dis; ?></b></td>
                                                            <td><b>Rs.<?php echo $grand_package_discount; ?></b></td>
                                                            <td><b>Rs.<?= $TotalFinalDiscount ?></b></td>
                                                            <?php /* <td><b>Rs.<?= $strTotalAmount ?>/-</b></td> */ ?>
                                                            <td><b>Rs.<?= $TotalUltimateSale ?>/-</b></td>
                                                            <td><b>Rs.<?= $ComFinal ?>/-</b></td>
                                                            <td></td>
                                                        </tr>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td>No Services Done by this Employee in selected time period</td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                        <?php
                                                    }

                                                    echo "</tbody></table></div><br><br>";
                                                }
                                            } else {
                                                echo "No Employees found for this store";
                                            }

                                            $DB->close();
                                            //echo "End";
                                            //echo (microtime(true));
                                            ?>
                                            <?php
                                        } else {
                                            echo "<br><center><h3>Please Select Month And Year!</h3></center>";
                                        }
                                        ?>
                                                    </saif>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                } // End null condition
                else {

                }
                ?>
                </div>
            </div>

            <?php require_once 'incFooter.fya'; ?>

        </div>
    </body>

</html>
