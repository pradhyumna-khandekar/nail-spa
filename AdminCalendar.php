<?php require_once("setting.fya"); ?>
<?php require_once'incFirewall.fya'; ?>
<?php
session_start();

$strPageTitle = "Calendar | Nailspa";
$strDisplayTitle = "Calendar of Nailspa Experience";
$strMenuID = "2";


// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights	
?>

<?php
/*
 * Function requested by Ajax
 */
if (isset($_POST['func']) && !empty($_POST['func'])) {
    switch ($_POST['func']) {
        case 'getCalender':
            getCalender($_POST['year'], $_POST['month']);
            break;
        case 'getEvents':
            getEvents($_POST['date']);
            break;
        default:
            break;
    }
}

/*
 * Get calendar full HTML
 */

function getCalender($year = '', $month = '') {

    $dateYear = ($year != '') ? $year : date("Y");
    $dateMonth = ($month != '') ? $month : date("m");
    $date = $dateYear . '-' . $dateMonth . '-01';
    $currentMonthFirstDay = date("N", strtotime($date));
    $totalDaysOfMonth = cal_days_in_month(CAL_GREGORIAN, $dateMonth, $dateYear);
    $totalDaysOfMonthDisplay = ($currentMonthFirstDay == 7) ? ($totalDaysOfMonth) : ($totalDaysOfMonth + $currentMonthFirstDay);
    $boxDisplay = ($totalDaysOfMonthDisplay <= 35) ? 35 : 42;
    ?>

    <div id="calender_section">
        <h2>
            <a href="javascript:void(0);" onclick="getCalendar('calendar_div', '<?php echo date("Y", strtotime($date . ' - 1 Month')); ?>', '<?php echo date("m", strtotime($date . ' - 1 Month')); ?>');">&lt;&lt;</a>
            <select name="month_dropdown" class="month_dropdown dropdown"><?php echo getAllMonths($dateMonth); ?></select>
            <select name="year_dropdown" class="year_dropdown dropdown"><?php echo getYearList($dateYear); ?></select>
            <a href="javascript:void(0);" onclick="getCalendar('calendar_div', '<?php echo date("Y", strtotime($date . ' + 1 Month')); ?>', '<?php echo date("m", strtotime($date . ' + 1 Month')); ?>');">&gt;&gt;</a>
        </h2>
        <div id="event_list" class="none"></div>
        <div id="calender_section_top">
            <ul>
                <li>Sun <?= $_SESSION['StoreID'] ?></li>
                <li>Mon</li>
                <li>Tue</li>
                <li>Wed</li>
                <li>Thu</li>
                <li>Fri</li>
                <li>Sat</li>
            </ul>
        </div>
        <div id="calender_section_bot">
            <ul>
                <?php
                $strStoreID = $StoreID;
                $dayCount = 1;
                for ($cb = 1; $cb <= $boxDisplay; $cb++) {
                    if (($cb >= $currentMonthFirstDay + 1 || $currentMonthFirstDay == 7) && $cb <= ($totalDaysOfMonthDisplay)) {
                        //Current date
                        $currentDate = $dateYear . '-' . $dateMonth . '-' . $dayCount;
                        $eventNum = 0;
                        //Include db configuration file
                        include 'calendar1/dbConfig.php';
                        //Get number of events based on the current date
                         $result = $db->query("SELECT CustomerID FROM tblAppointments WHERE AppointmentDate = '" . $currentDate . "' AND (Status='2' OR Status='0') ");

                        $eventNum = $result->num_rows;
                        //Define date cell color
                        if (strtotime($currentDate) == strtotime(date("Y-m-d"))) {
                            echo '<li date="' . $currentDate . '" id="' . $strAdminID . '" value="' . $currentDate . '" data-toggle="modal" data-target="#myModal" class="grey date_cell">';
                        } elseif ($eventNum > 0) {
                            echo '<li date="' . $currentDate . '" id="' . $strAdminID . '" value="' . $currentDate . '"  data-toggle="modal" data-target="#myModal" class="light_sky date_cell">';
                        } else {
                            echo '<li date="' . $currentDate . '" id="' . $strAdminID . '" value="' . $currentDate . '"  data-toggle="modal" data-target="#myModal" class="date_cell">';
                        }
                        //Date cell
                        echo '<span>';
                        echo $dayCount;
                        echo '</span>';

                        //Hover event popup
                        echo '<div id="date_popup_' . $currentDate . '" class="date_popup_wrap none">';
                        echo '<div class="date_window">';
                        echo '<div class="popup_event">Total Appointments (' . $eventNum . ')</div>';
                        //echo ($eventNum > 0)?'<a href="javascript:;" onclick="getEvents(\''.$currentDate.'\');">view events</a>':'';
                        echo '</div></div>';

                        echo '</li>';
                        $dayCount++;
                        ?>
                    <?php } else { ?>
                        <li><span>&nbsp;</span></li>
                        <?php
                    }
                }
                ?>
            </ul>
        </div>
    </div>

    <script type="text/javascript">
        function getCalendar(target_div, year, month) {
            $.ajax({
                type: 'POST',
                url: 'CalendarFunction.php',
                data: 'func=getCalender&year=' + year + '&month=' + month,
                success: function (html) {
                    $('#' + target_div).html(html);
                }
            });
        }

        /* function getEvents(date){
         $.ajax({
         type:'POST',
         url:'CalendarFunction.php',
         data:'func=getEvents&date='+date,
         success:function(html){
         $('#event_list').html(html);
         $('#event_list').slideDown('slow');
         }
         });
         }*/

        function addEvent(date) {
            $.ajax({
                type: 'POST',
                url: 'CalendarFunction.php',
                data: 'func=addEvent&date=' + date,
                success: function (html) {
                    $('#event_list').html(html);
                    $('#event_list').slideDown('slow');
                }
            });
        }

        $(document).ready(function () {
            /*$('.date_cell').mouseenter(function(){
             date = $(this).attr('date');
             $(".date_popup_wrap").fadeOut();
             $("#date_popup_"+date).fadeIn();    
             });
             $('.date_cell').mouseleave(function(){
             $(".date_popup_wrap").fadeOut();        
             });*/
            $('.month_dropdown').on('change', function () {
                getCalendar('calendar_div', $('.year_dropdown').val(), $('.month_dropdown').val());
            });
            $('.year_dropdown').on('change', function () {
                getCalendar('calendar_div', $('.year_dropdown').val(), $('.month_dropdown').val());
            });
            $(document).click(function () {
                $('#event_list').slideUp('slow');
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            $(".date_cell").click(function () {


                var t = $(this).attr('value');

                // alert(t);
                $.ajax({
                    method: 'post',
                    url: 'calendar1/AdminCalendarRetrive.php',
                    data: {t: t},
                    success: function (responce) {
                        $("#result").html(responce);
                    }
                });

            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            $("body").on("contextmenu", function (e) {
                return false;
            });
            $(".date_cell").on("contextmenu", function (e) {
                location.href = "ManageCustomers2.php";



            });
        });
    </script>
    <?php
}

/*
 * Get months options list.
 */

function getAllMonths($selected = '') {
    $options = '';
    for ($i = 1; $i <= 12; $i++) {
        $value = ($i < 10) ? '0' . $i : $i;
        $selectedOpt = ($value == $selected) ? 'selected' : '';
        $options .= '<option value="' . $value . '" ' . $selectedOpt . ' >' . date("F", mktime(0, 0, 0, $i + 1, 0, 0)) . '</option>';
    }
    return $options;
}

/*
 * Get years options list.
 */

function getYearList($selected = '') {
    $options = '';
    for ($i = 2017; $i <= 2050; $i++) {
        $selectedOpt = ($i == $selected) ? 'selected' : '';
        $options .= '<option value="' . $i . '" ' . $selectedOpt . ' >' . $i . '</option>';
    }
    return $options;
}

/*
 * Get events by date
 */

function getEvents($date = '') {
    $strStoreID = $StoreID;
    //Include db configuration file
    include 'calendar1/dbConfig.php';
    $eventListHTML = '';
    $date = $date ? $date : date("Y-m-d");
    //Get events based on the current date
    $result = $db->query("SELECT CustomerID FROM tblAppointments WHERE AppointmentDate = '" . $date . "' AND (Status='2' OR Status='0') ");

    if ($result->num_rows > 0) {
        $eventListHTML = '<h2>Events on ' . date("l, d M Y", strtotime($date)) . '</h2>';
        $eventListHTML .= '<ul>';
        while ($row = $result->fetch_assoc()) {
            $eventListHTML .= '<li>' . $row['CustomerID'] . '</li>';
        }
        $eventListHTML .= '</ul>';
    }
    echo $eventListHTML;
}
?>	


<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>

        <link href="calendar1/includes/bootstrap.css" rel="stylesheet">
        <link href="calendar1/includes/style.css" rel="stylesheet">
        <!-- Include the stylesheet -->
        <link type="text/css" rel="stylesheet" href="calendar1/style.css"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <!-----------css & js files added for tabs by gandhali 3/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/calendar/calendar.css">
        <script type="text/javascript" src="assets/widgets/calendar/calendar.js"></script>
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
        <!--<script src="calendar1/includes/bootstrap.js"></script>
        <script src="https://apis.google.com/js/platform.js" async defer></script>-->

        <?php require_once 'incFooter.fya'; ?>

        <style>
            .container {
                width: 600px !important;
                float: left;
            }
            .fc-basic-view .fc-body .fc-row {
                max-height: 85.71px !important;
            }
            div#calresult {

                overflow-y: auto;
                height: 500px;
            }
            .col-lg-12 h2 {
                text-align: center;
            }
            h2{font-family: Verdana, sans-serif;color:#383838;font-size: 20px;font-weight: bold;}
            h2 span{color:#FB4314;}
            h2 span span{color:#1CA86F;}
            .modal-backdrop
            {
                display:none;
            }
            .date_popup_wrap {
                position: absolute;
                width: 100%;
                height: 100%;
                z-index: 2;
                top: 0;
                left:0;
                background: transparent url(add-new-event.png) no-repeat top left;
                color: #666 !important;
            }
            .popup_event {
                margin-bottom: 2px;
                padding: 2px;
                font-size: 12px;
                width:100%;
            }
            .todayappointment{
                clear:both;
            }
            #page-title
            {
                margin: -20px -30px 0;
            }
            .panel-body {
                padding: 5px;
            }
            #calender_section {
                width: 100%;
            }
            #calender_section_top ul li
            {
                width: 14.285%;
            }
            #calender_section {
                width: 100%;
            }
            #calender_section_bot ul li
            {
                width: 14.285%;
            }
            .light_sky {
                background-color: #ff9b6d !important;
            }
            #calender_section_bot ul li span {
                font-weight: bold;
            }
            .date_popup_wrap {
                color: #000 !important;
            }
            .date_popup_wrap {
                position: absolute;
                width: 100%;
                height: 100%;
                z-index: 2;
                top: 0;
                left: 0;
                background: transparent url(add-new-event.png) no-repeat top left;
                color: #000 !important;
            }
            .popup_event {
                margin-bottom: 2px;
                padding: 2px;
                font-size: 12px;
                width: 100%;
            }
            .date_window {
                margin-top: 28px;
                margin-bottom: 0;
                padding: 0 5px;
                font-size: 16px;
                text-align: left;
                margin-left: 0;
                margin-right: 0;
            }

            #header-nav-right>.dropdown>a:last-child {
                width: auto;
            }
            .table>thead>tr>th
            {
                background: #700000;
                color: #fff;
            }
            .table-striped > tbody > tr:nth-child(odd) > td
            {
                background-color: #ebb240;
                color: #000;
            }
            .table>tbody>tr>td
            {
                padding: 5px 5px !important;
            }
            #calender_section_bot {
                margin-top: 0;
            }
            #calender_section_top ul li {
                width: 14.285%;
                background: #700000;
                color: #fff;
                font-weight: bold;
                padding: 10px 0;
            }
            #page-title
            {
                margin: -20px -30px 0;
            }
        </style>

    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");   ?>
            <!----------commented by gandhali 3/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>


                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                        </div>


                        <div class="panel">
                            <div class="panel">
                                <div class="panel-body">


                                    <div class="example-box-wrapper">
                                        <div class="tabs">

                                            <div id="normal-tabs-1">

                                                <span class="form_result">&nbsp; <br>
                                                </span>

                                                <div class="panel-body">




                                                    <br />
                                                    <div id="calendar_div">
                                                        <?php echo getCalender(); ?>
                                                    </div>



                                                    <div class="row">
                                                        <div class="panel">
                                                            <div class="panel-body">

                                                                <div class="example-box-wrapper">
                                                                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="datatable-example">
                                                                        <thead>
                                                                            <tr>
                                                                                <th style="text-align:center">Store</th>
                                                                                <th style="text-align:center">Open Time</th>
                                                                                <th style="text-align:center">Close Time</th>
                                                                                <th  style="text-align:center">Total Today's Sales</th>
                                                                                <th  style="text-align:center">Total Appointments</th>
                                                                                <th  style="text-align:center">Done Appointments</th>
                                                                                <th  style="text-align:center">Pending Appointments</th>
                                                                                <th  style="text-align:center">Upcoming Appointments</th>
                                                                                <th  style="text-align:center">Cancel Appointments</th>
                                                                                <th  style="text-align:center">Delayed Appointments</th>
                                                                                <th  style="text-align:center">Rescheduled Appointments</th>



                                                                                <?php
                                                                                if ($strAdminRoleID != '36') {
                                                                                    ?>
                                                                                    <?php
                                                                                } else {
                                                                                    ?>														
                                                                                    <th  style="text-align:center">Average Ticket Size</th>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                        </thead>


                                                                        <tbody>
                                                                            <?php
                                                                            $date = date('y-m-d');
                                                                            $DB = Connect();
                                                                            $FindStore = "Select * from tblOpenNClose where DateNTime='" . $date . "'";
                                                                            // echo $FindStore;
                                                                            $RSf = $DB->query($FindStore);
                                                                            if ($RSf->num_rows > 0) {
                                                                                while ($rowf = $RSf->fetch_assoc()) {
                                                                                    $strStoreIDd = $rowf["StoreID"];
                                                                                    $selp = select("StoreName", "tblStores", "StoreID='" . $strStoreIDd . "'");
                                                                                    $StoreName = $selp[0]['StoreName'];
                                                                                    $OpenTime = $rowf["OpenTime"];
                                                                                    $CloseTime = $rowf["CloseTime"];

                                                                                    $OpenTimet = date("H:i:s", strtotime($OpenTime));
                                                                                    $OpenTimett = get12hour($OpenTimet);
                                                                                    $CloseTimet = date("H:i:s", strtotime($CloseTime));
                                                                                    $CloseTimett = get12hour($CloseTimet);
                                                                                    $sepq = select("SUM(tblInvoiceDetails.TotalPayment) as TOTAL", "tblAppointments Left join tblInvoiceDetails
											ON tblAppointments.AppointmentID=tblInvoiceDetails.AppointmentID", "tblAppointments.AppointmentDate='" . $date . "' and tblAppointments.StoreID='$strStoreIDd'");
                                                                                    $TOTAL = $sepq[0]['TOTAL'];
                                                                                    if ($TOTAL == '' || $TOTAL == '') {
                                                                                        $TOTAL = 0;
                                                                                    }
                                                                                    $sepqt = select("count(tblAppointments.AppointmentID) as cntp", "tblAppointments", "tblAppointments.AppointmentDate='" . $date . "' and tblAppointments.StoreID='$strStoreIDd'");
                                                                                    $cntp = $sepqt[0]['cntp'];
                                                                                    $sepqtpending = select("count(tblAppointments.AppointmentID) as cntpen", "tblAppointments", "tblAppointments.AppointmentDate='" . $date . "' and tblAppointments.StoreID='$strStoreIDd' and Status='1'");
                                                                                    $cntpen = $sepqtpending[0]['cntpen'];
                                                                                    $sepqtdone = select("count(tblAppointments.AppointmentID) as cntdone", "tblAppointments", "tblAppointments.AppointmentDate='" . $date . "' and tblAppointments.StoreID='$strStoreIDd' and Status='2'");
                                                                                    $cntdone = $sepqtdone[0]['cntdone'];

                                                                                    $sepqtcancel = select("count(tblAppointments.AppointmentID) as cntcancel", "tblAppointments", "tblAppointments.AppointmentDate='" . $date . "' and tblAppointments.StoreID='$strStoreIDd' and Status='3'");
                                                                                    $cntcancel = $sepqtcancel[0]['cntcancel'];

                                                                                    $sepqtupcome = select("count(tblAppointments.AppointmentID) as cntupcome", "tblAppointments", "tblAppointments.AppointmentDate='" . $date . "' and tblAppointments.StoreID='$strStoreIDd' and Status='0'");
                                                                                    $cntupcome = $sepqtupcome[0]['cntupcome'];

                                                                                    $sepqtdelayed = select("count(tblAppointments.AppointmentID) as cntdealyed", "tblAppointments", "tblAppointments.AppointmentDate='" . $date . "' and tblAppointments.StoreID='$strStoreIDd' and Status='5'");
                                                                                    $cntdealyed = $sepqtdelayed[0]['cntdealyed'];


                                                                                    $sepqtresche = select("count(tblAppointments.AppointmentID) as cntresh", "tblAppointments", "tblAppointments.AppointmentDate='" . $date . "' and tblAppointments.StoreID='$strStoreIDd' and Status='6'");
                                                                                    $cntres = $sepqtresche[0]['cntresh'];
                                                                                    $size = $TOTAL / $cntp;
                                                                                    ?>
                                                                                    <tr><td><center><b><?= $StoreName ?></b></center></td><td><center><b><?= $OpenTimett ?></b></center></td><td><center><b><?= $CloseTimett ?></b></center></td><td><b><center><?= 'Rs.' . $TOTAL ?></center></b></td><td><b><center><?= $cntp ?></center></b></td><td><b><center><?= $cntdone ?></center></b></td><td><b><center><?= $cntpen ?></center></b></td><td><b><center><?= $cntupcome ?></center></b></td><td><b><center><?= $cntcancel ?></center></b></td><td><b><center><?= $cntdealyed ?></center></b></td>
                                                                                <td><b><center><?= $cntres ?></center></b></td>
                                                                                <?php
                                                                                if ($strAdminRoleID != '36') {
                                                                                    ?>												

                                                                                    <?php
                                                                                } else {
                                                                                    ?>
                                                                                    <td><b><center><?= round($size) ?></center></b></td>
                                                                                    <?php
                                                                                }
                                                                                ?>

                                                                                </tr>
                                                                                <?php
                                                                                // echo $strStoreID;
                                                                                // echo "Hello";
                                                                            }
                                                                        }
                                                                        $DB->close();
                                                                        ?>										
                                                                        </tbody>
                                                                    </table>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>	




                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>




                        </div>
                    </div>

                </div>




            </div>


        </div>
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modal Header</h4>
                    </div>
                    <div class="modal-body" id="result">
                        <p>Some text in the modal.<</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>


    </body>

</html>