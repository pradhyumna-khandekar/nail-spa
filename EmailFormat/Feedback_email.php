<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Email Template</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            body{color:#3a020a;;}
            p{margin:3px;}
            tr, td{font-size: 14px;}
            th{color:#795548;padding:5px;}
            h1{margin: 0;text-align:center;font-size:25px;color:#607D8B;}
            h2{font-size:16px;}
            button{font-size: 16px;
                   display: block;
                   margin: 20px 0;
                   background-color: #F44336;
                   color: #fff;
                   padding: 10px;
                   border: none;
                   border-radius: 5px;}
            .main-table{background-color:#fafafa;font-family: "Helvetica" , sans-serif; font-size:14px; margin:0; padding:0;}
            .report-table{border-collapse: collapse;display:block;text-align:center;}
            .report-table tr{border-bottom: 2px solid #795548;}
            .report-table tr td{padding:5px;}
            @media only screen and (max-width: 600px) {

            }
        </style>
    </head>
    <body>
        <table class="main-table" cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
            <tbody><tr>
                    <td align="center" valign="top" style="padding:20px 0 20px 0">
                        <table bgcolor="#FFFFFF" cellspacing="0" cellpadding="10" border="0" width="600" style="border:1px solid #dddddd;">
                            <tbody>
                                <tr>
                                    <td colspan="1" style="padding:0px;background-color: #FFF;">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody><tr>
                                                    <td  style="padding:10px;text-align:center;color: #fff;">
                                                        <a href="http://nailspaexperience.com" target="_blank"><img src="http://pos.nailspaexperience.com/og/images/nail_logo_new.png" width="100px"/></a>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                    </td>

                                </tr>

                                <tr style="
                                    /* background: #edf7f0; */
                                    ">
                                    <td colspan="2" style="padding-left: 0px; padding-right: 0px;padding-top: 0px;padding-bottom: 15px;">
                                        <table width="100%" cellspacing="0" cellpadding="0" style="
                                               background: #2a0206;
                                               ">
                                            <tbody><tr>
                                                    <td colspan="2" style="padding: 5px 20px;text-align:center;font-weight:700;/* margin-left: 16px; */">
                                                        <p style="text-transform: uppercase"></p>

                                                    </td>
                                                </tr>
                                            </tbody></table>
                                    </td>
                                </tr>


                                <tr>
                                    <td colspan="2" style="padding:0px;padding-left: 22px;padding-right: 22px;">
                                        <table width="100%" cellspacing="0" cellpadding="0" style="
                                               /* background: #edf7f0; */
                                               ">
                                            <tbody><tr>
                                                    <td colspan="2" style="line-height: 24px;padding:0 20px 10px;text-align:left;/* margin-left: 16px; */">
                                                        <p style="margin-top:15px;">Dear {member_name}</p>
                                                        <p style="margin-top:15px;">
                                                            THANK YOU FOR VISITING NS STYLE SALON 
                                                        </p>
                                                        <p>YOUR VALUABLE FEEDBACK WILL HELP US SERVE BETTER!</p>
                                                        <p style="margin-top:15px; text-align:center;">
                                                            <span>HOW WAS YOUR EXPERIENCE ?</span><stong>

                                                </p>



                                                </td>

                                                </tr>

                                                <tr>
                                                    <td colspan="1" style="padding:0px;background-color: #FFF">
                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                            <tbody><tr>
                                                                    <td colspan="2" style="padding:10px;text-align:center;color: #fff;">
                                                                        <a href="#" target="_blank"><img src="http://pos.nailspaexperience.com/og/images/happy_new.png" width="90px" height="90px"/></a>

                                                                    </td>
                                                                </tr>
                                                            </tbody></table>
                                                    </td>
                                                    <td colspan="1" style="padding:0px;background-color:#FFF">
                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                            <tbody><tr>
                                                                    <td colspan="2" style="padding:10px;text-align:center;color:#fff;">
                                                                        <a href="#" target="_blank"><img src="http://pos.nailspaexperience.com/og/images/angry_new.png" width="90px" height="90px"/></a>

                                                                    </td>


                                                                </tr>
                                                                <tr>

                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>

                                                </tbody>
                                                <table>
                                                    <td colspan="2" style="line-height: 24px; padding:0 20px 10px 94px;text-align:center;/* */">


                                                        <p style="margin-top:15px; text-align:center;">
                                                            <span><a href="#" style="color:#3a020a;">CLICK ON THE LINK BELOW TO GIVE YOUR FEEDBACK</a></span><stong>

                                                        </p>
                                                        <p style="margin-top:15px; text-align:center; margin-bottom:50px;"> <a href="{feedback_link}" style="text-align:center; background-color:#2a0206; padding: 6px 6px 6px 6px; color:#fff;">CLICK HERE</a></p>
                                                        </td>

                                                </table>
                                                </td>
                                                </tr>


                                                <tr>
                                                    <td colspan="2" style="padding: 0px;background: #3a020a;">
                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                            <tbody><tr>
                                                                    <td colspan="2" style="background-color: #3a020a;padding: 15px 0;text-align: center;color: #fff;">
                                                                        <p class="text-center" style="font-size:14px;"></p>
                                                                        <p style="color:#dcc36f;">11+ LOCATIONS | T1 & T2 MUMBAI AIRPORT &nbsp;| MUMBAI | KOCHI </p>
                                                                        <p style="color: #dcc36f; text-align:center;"><strong>GO GREEN GO PAPERLESS</strong></p>

                                                                    </td>
                                                                </tr>
                                                            </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                    </td>
                                </tr>
                            </tbody></table>
                        </body>
                        </html>