<?php require_once 'setting.fya'; ?>
<?php require_once 'incFirewall.fya'; ?>

<div class="form-group"><label class="col-sm-2 control-label">Employee<span>*</span></label>

    <div class="col-sm-8">			
        <?php
        if (isset($_POST["store_id"]) && !empty($_POST["store_id"])) {
            $store_id = $_POST["store_id"];
            $emp_data = select("*", "tblEmployees", "StoreID='" . $store_id . "' AND status=0");
        }
        ?>
        <select class="form-control chosen-select required" name="emp_id[]" id="emp_id" multiple="multiple">
            <?php
            if (isset($emp_data) && is_array($emp_data) && count($emp_data) > 0) {
                foreach ($emp_data as $rowk => $row2) {
                    $strEmpId = $row2["EID"];
                    $strEmpName = $row2["EmployeeName"];
                    ?>
                    <option value="<?= $strEmpId ?>" ><?= $strEmpName ?></option>														
                    <?php
                }
            }
            ?>
        </select>


    </div>
</div>