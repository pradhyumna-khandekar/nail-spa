<?php

$email_body = '<html id="printarea">
    <head>
        <style>
            .main-table{background-color:#fafafa;font-family:Arial, Helvetica, sans-serif; font-size:14px; margin:0; padding:0;}
            tr, td{font-size: 14px;}
            th{color:#795548;padding:5px;}
            h1{margin: 0;padding: 10px;background-color: #f5f5f5;text-align:left;font-size:25px;color:#607D8B;}
            .report-table{border-collapse: collapse;display:block;text-align:center;}
            .report-table tr{border-bottom: 2px solid #795548;}
            .report-table tr td{padding:5px;}
        </style>
    </head>


    <body>

        <table  class="main-table" cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">

            <td align="center" valign="top" style="padding:20px 0 20px 0">

                <table bgcolor="#FFFFFF" cellspacing="0" cellpadding="10" border="0" width="600" style="border:1px solid #dddddd;">


                    <tr>
                        <td colspan="2" style="padding:10px;">
                            <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td colspan="2" style="background-color: #f5f5f5;padding:15px 0 0;text-align:center;">
                                        <h1 class="text-center" style="padding:0 0 15px;background:none;text-align:center;font-size:25px;color:#607D8B;">Feedback Rating : ' . date('d/m/Y') . '</h1>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>';


$email_body.= '<tr>
                                                    <td colspan="2" valign="top" style="border-bottom:1px solid #dddddd; padding:10px;background-color: #f5f5f5; ">
                                                        <table class="report-table">
                                                            <tr style="color:#607D8B;">
                                                               <h3 style="color:#607D8B;display: inline;">Customer Name : </h3><span>{customer_name}</span><br><br>
                                                               <h3 style="color:#607D8B;display: inline;">Customer Mobile No. : </h3><span>{customer_mobile}</span><br><br>
                                                               <h3 style="color:#607D8B;display: inline;">Sale Amount : </h3><span>{sale_amount}</span><br><br>
                                                                <h3 style="color:#607D8B;display: inline;">Service Quality : </h3><span>{service_quality}</span><br><br>
                                                                <h3 style="color:#607D8B;display: inline;">Value for money : </h3><span>{value_for_money}</span><br><br>
                                                                <h3 style="color:#607D8B;display: inline;">Salon Atmosphere : </h3><span>{salon_atmosphere}</span><br><br>
                                                                <h3 style="color:#607D8B;display: inline;">Staff Presentation & Attitude : </h3><span>{staff_presentation_attitude}</span><br><br>
                                                                <h3 style="color:#607D8B;display: inline;">Cleanliness : </h3><span>{cleanliness}</span><br><br>
                                                                <h3 style="color:#607D8B;display: inline;">Appointment Date : </h3><span>{appointment_date}</span><br><br>
                                                                <h3 style="color:#607D8B;display: inline;">Store : </h3><span>{appointment_store}</span><br><br>
                                                                <h3 style="color:#607D8B;display: inline;">Services Done : </h3><span>{service_name}</span><br><br>
                                                                <h3 style="color:#607D8B;display: inline;">Employee Name : </h3><span>{employee_name}</span>
                                                                </tr>';




$email_body .= '</table>
            </td>
        </tr>
    </table>
</body>
</html>';


return $email_body;
