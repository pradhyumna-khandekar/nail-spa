<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Manage Offers | Nailspa";
$strDisplayTitle = "Offers Logs for Nailspa";
$strMenuID = "3";
$strMyTable = "tblOffers";
$strMyTableID = "OfferID";
$strMyField = "OfferName";
$strMyActionPage = "OffersHistoryList.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <!-----------css & js files added for tabs by gandhali 3/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");     ?>
            <!----------commented by gandhali 3/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>
                        <div id="page-title">
                            <h2>Telecalling Dashboard</h2>
                        </div>
                        <?php
                        $DB = Connect();
                        /*
                         * get all status Count
                         */
                        $book_countq = "SELECT COUNT(AppointmentID) as cust_count FROM tblAppointments "
                                . " WHERE  non_visiting_update_date LIKE '%" . date('Y-m-d') . "%' AND non_visiting_update_value=1";
                        $book_countq_exe = $DB->query($book_countq);
                        if ($book_countq_exe->num_rows > 0) {
                            while ($book_count_res = $book_countq_exe->fetch_assoc()) {
                                $book_now_count = $book_count_res['cust_count'];
                            }
                        }

                        $call_countq = "SELECT COUNT(AppointmentID) as cust_count FROM tblAppointments "
                                . " WHERE  non_visiting_update_date LIKE '%" . date('Y-m-d') . "%' AND non_visiting_update_value=2";
                        $call_countq_exe = $DB->query($call_countq);
                        if ($call_countq_exe->num_rows > 0) {
                            while ($call_count_res = $call_countq_exe->fetch_assoc()) {
                                $call_count = $call_count_res['cust_count'];
                            }
                        }

                        $nint_countq = "SELECT COUNT(AppointmentID) as cust_count FROM tblAppointments "
                                . " WHERE  non_visiting_update_date LIKE '%" . date('Y-m-d') . "%' AND non_visiting_update_value=9";
                        $nint_countq_exe = $DB->query($nint_countq);
                        if ($nint_countq_exe->num_rows > 0) {
                            while ($nint_count_res = $nint_countq_exe->fetch_assoc()) {
                                $not_interested_count = $nint_count_res['cust_count'];
                            }
                        }

                        $client_call_countq = "SELECT COUNT(AppointmentID) as cust_count FROM tblAppointments "
                                . " WHERE  non_visiting_update_date LIKE '%" . date('Y-m-d') . "%' AND non_visiting_update_value=10";
                        $client_call_exe = $DB->query($client_call_countq);
                        if ($client_call_exe->num_rows > 0) {
                            while ($cli_call_count_res = $client_call_exe->fetch_assoc()) {
                                $client_call_count = $cli_call_count_res['cust_count'];
                            }
                        }
                        ?>

                        <div class="panel" style="padding:2%;">
                            <div class="row">
                                <span id="abc" style="display:none"></span>
                                <!--1st Block-->
                                <div class="col-md-3">
                                    <a href="#" title="" class="tile-box tile-box-shortcut btn-danger" style="background: #50b7b6; border-color: #50b7b6;"><span class="bs-badge badge-absolute">
                                            <?php echo isset($book_now_count) ? $book_now_count : 0; ?>
                                        </span><div class="tile-header">Book Now</div><div class="tile-content-wrapper"><i class=""></i></div></a>
                                </div>
                                <!--End 1st Block-->


                                <!--2nd Block-->
                                <div class="col-md-3">
                                    <a href="#" title="" class="tile-box tile-box-shortcut btn-danger" style="background: #dc1156; border-color: #dc1156;"><span class="bs-badge badge-absolute">
                                            <?php echo isset($call_count) ? $call_count : 0; ?>
                                        </span><div class="tile-header">Call Later</div><div class="tile-content-wrapper"><i class=""></i></div></a>
                                </div>
                                <!--End 2nd Block-->

                                <!--3rd Block-->
                                <div class="col-md-3">
                                    <a href="#" title="" class="tile-box tile-box-shortcut btn-danger" style="background: #808080; border-color: #808080;" id="ModalOpenBtn" data-toggle="modal" data-target="#myModalstock"><span class="bs-badge badge-absolute">
                                            <?php echo isset($not_interested_count) ? $not_interested_count : 0; ?>
                                        </span><div class="tile-header">Not Interested</div><div class="tile-content-wrapper"><i class=""></i></div></a>
                                </div>
                                <!--End 3rd Block-->

                                <!--4th Block-->
                                <div class="col-md-3">
                                    <a href="#" title="" class="tile-box tile-box-shortcut btn-danger" style="background: #2e689a; border-color: #2e689a;"><span class="bs-badge badge-absolute">
                                            <?php echo isset($client_call_count) ? $client_call_count : 0; ?>
                                        </span>
                                        <div class="tile-header">Client will call</div><div class="tile-content-wrapper"><i class=""></i></div></a>
                                </div>
                                <!--End 4th Block-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php $DB->close(); ?>

            <?php require_once 'incFooter.fya'; ?>
        </div>
    </body>
</html
