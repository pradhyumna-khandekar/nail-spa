<?php require_once 'setting.fya'; ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
$test = "";
if (isset($_POST["id"])) {
    $test = $_POST["id"];
}


// echo $test;
$DB = Connect();

if (isset($_POST['appid']) && $_POST['appid'] > 0) {
    /*
     * Get previous Appointment Services
     */
    $appoint_services = select("*", "tblAppointmentsDetailsInvoice", "AppointmentID='" . $_POST['appid'] . "'");

    if (isset($appoint_services) && is_array($appoint_services) && count($appoint_services) > 0) {
        foreach ($appoint_services as $skey => $svalue) {
            $sel_services[$svalue['ServiceID']] = $svalue['ServiceID'];
        }
    }
}
$sql = "SELECT ServiceID, ServiceName, ServiceCost FROM tblServices WHERE StoreID=$test AND active_status=1";
$RS = $DB->query($sql);
if ($RS->num_rows > 0) {
    ?>
    <div class="form-group"><label class="col-sm-3 control-label">Appointment for <span>*</span></label>
        <div class="col-sm-3 ">
            <select class="form-control chosen-select required" onChange="LoadValueasmita();" name="Services[]"  id="Services" style="width:161%" multiple>
                <?php
                while ($row = $RS->fetch_assoc()) {
                    $ServiceID = $row["ServiceID"];
                    $ServiceName = $row["ServiceName"];
                    $ServiceCost = $row["ServiceCost"];
                    ?>
                    <option value="<?= $ServiceID ?>"
                            <?php echo isset($sel_services) && in_array($ServiceID, $sel_services) ? 'selected' : ''; ?>><?= $ServiceName ?>, Rs. <?= $ServiceCost ?></option>
                            <?php
                        }
                        ?>
            </select>
        </div>
    </div>
    <?php
} else {
    ?>
    <div class="form-group"><label class="col-sm-3 control-label">Appointment for <span>*</span></label>
        <div class="col-sm-3 ">
            <select class="form-control required" onChange="LoadValueasmita();" name="Services[]" style="height:100pt" id="Services" multiple>
                <option value="" ></option>
                <?php
                ?>

            </select>
            <?php // echo "Services are not added for Selected Store. Add Services Here";  ?>
            Services are not added for Selected Store. Add Services <a href="http://nailspa.fyatest.website/pos/admin/ManageServices.php">Here</a>
        </div>
    </div>
    <?php
}
?>