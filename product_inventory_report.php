<?php require_once("setting.fya"); ?>
<?php

$allPdtData = select("ProductID,ProductName,ProductUniqueCode,stock_notification_qty", "tblNewProducts", "ProductID>0");
if (isset($allPdtData) && is_array($allPdtData) && count($allPdtData) > 0) {
    foreach ($allPdtData as $pdkey => $pdvalue) {
        $product_stock[$pdvalue['ProductID']] = $pdvalue;
    }
}

$store_data = select("*", "tblStores", "StoreID > 0");
if (isset($store_data) && is_array($store_data) && count($store_data) > 0) {
    foreach ($store_data as $skey => $svalue) {
        $store_name[$svalue['StoreID']] = $svalue;
    }
}
/*
 * Get Product Inventory Store wise
 */
$store_inventory = select("*", "product_inventory", "status=1");
if (isset($store_inventory) && is_array($store_inventory) && count($store_inventory) > 0) {
    foreach ($store_inventory as $skey => $svalue) {
        if (isset($product_stock[$svalue['ProductID']])) {
            $noti_qty = $product_stock[$svalue['ProductID']]['stock_notification_qty'];
            $avail_qty = $svalue['Stock'];
            if ($noti_qty > 0 && $avail_qty < $noti_qty) {
                $less_product[$svalue['StoreID']][$svalue['ProductID']]['name'] = $product_stock[$svalue['ProductID']]['ProductName'];
                $less_product[$svalue['StoreID']][$svalue['ProductID']]['avail_qty'] = $svalue['Stock'];
                $less_product[$svalue['StoreID']][$svalue['ProductID']]['code'] = $product_stock[$svalue['ProductID']]['ProductUniqueCode'];
            }
        }
    }
}
$email_body = '';
if (isset($less_product) && is_array($less_product) && count($less_product) > 0) {

    $email_body = '<html>
    <head>
   
</head>


    <body>

        <table border="0" cellspacing="0" cellpadding="0" width="100%">

            <tbody>
        <tr>
            <td>
                <table border="0" cellspacing="0" cellpadding="0" width="800" align="center">
                    <tbody>
                        
                        <tr>
                            <td>
                                <table style="BORDER-BOTTOM:#d0ac52 1px solid;BORDER-LEFT:#d0ac52 1px solid;BORDER-TOP:#d0ac52 1px solid;BORDER-RIGHT:#d0ac52 1px solid;background: url({path}) no-repeat;background-position: 50% -20px;" border="0" cellspacing="0" cellpadding="0" width="800" bgcolor="#ffffff" align="center">
                                    <tbody>
                                        <tr>
                                            <td align="middle">
                                                <table border="0" cellspacing="0" cellpadding="0" width="790" align="center">
                                                    <tbody>
                                                        <tr>
                                                            <td width="290" align="left" style="padding:1%;"><img border="0" src="http://nailspaexperience.com/header/Nailspa-logo.png" width="117" height="60" alt="NailSpa Experience"></td>
                                                           
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
										   <tr>
                                            <td style="LINE-HEIGHT:0;BACKGROUND:#d0ac52;FONT-SIZE:0px;" height="5"></td>
                                        </tr>
                                        <tr>
                                            <td style="PADDING-LEFT:10px;PADDING-RIGHT:10px;PADDING-TOP:10px;" align="left">
                                                <div style="BORDER-BOTTOM:#dedcdc 1px solid; FONT-FAMILY:Verdana,Geneva,sans-serif; BORDER-LEFT:#dedcdc 1px solid;PADDING-BOTTOM:15px;PADDING-LEFT:5px;PADDING-RIGHT:5px;BACKGROUND:#e4e4e4;FONT-SIZE:14px;BORDER-TOP:#dedcdc 1px solid;FONT-WEIGHT:bold;BORDER-RIGHT:#dedcdc 1px solid;PADDING-TOP:15px; text-align:center;">
                                             Less Product Inventory On ' . date('d M,Y') . '<br>
                                                                                                
                                                </div>
                                            </td>';


    $email_body.= '<tr>
                                                    <td colspan="2" valign="top" style="border-bottom:1px solid #dddddd; padding:10px; ">
                                                        <table class="report-table" border="1" cellspacing="0" style="FONT-FAMILY:Verdana,Geneva,sans-serif;text-align:center;FONT-SIZE:14px;width: 100%;">
                                                            <tr style="background-color: #e4e4e4;">
                                                                <th>Sr. No.</th>
                                                                <th>Product Name</th>
                                                                <th>Product Code</th>
                                                                <th>Branch</th>                                                                 
                                                                <th>Stock</th> 
                                                                </tr>';


    $counter = 1;
    if (isset($less_product) && is_array($less_product) && count($less_product) > 0) {
        foreach ($less_product as $skey => $svalue) {
            foreach ($svalue as $pkey => $pvalue) {

                $email_body .= '<tr id="my_data_tr_<?= $counter ?>"> 
                <td>' . $counter . '</td>
                <td>' . $pvalue['name'] . '</td>
                <td>' . $pvalue['code'] . '</td>
                <td>' . $store_name[$skey]['StoreName'] . '</td>
                <td>' . $pvalue['avail_qty'] . '</td>


            </tr>';
                $counter++;
            }
        }
    }

    $email_body .='</td></tr></table><tr><td  style="BACKGROUND:#d0ad53;">
                                                <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                              
                                                <tbody>
                                        
                                        
                                        
                                            <td width="33%" style="FONT-FAMILY:Arial,Helvetica,sans-serif;BACKGROUND:#d0ad53;COLOR:#000;FONT-SIZE:12px; padding:1%;" height="32" align="center">
                                            <span style="font-size:14px; font-weight:600;" >KHAR | BREACH CANDY | ANDHERI | COLABA | LOKHANDWALA</span><br>
                                            </td>
											<tr>
                                            	<td style="BACKGROUND:#d0ad53;font-size:24px;font-weight:bold;" align="center"> Go Green, Go Paperless !</td>
                                            </tr>
                                          
                                            </tbody>
                                            </table>
                                            </td>
                                            
                                            
                                        	</tr>
                                    	</tbody>
                                	</table>
                            	</td>
                        	</tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
</body>
</html>';
}
if ($email_body != '') {
    $report_data = select("*", 'report_config', 'status =1 AND report_name="less_product_report"');
    if (isset($report_data) && is_array($report_data) && count($report_data) > 0) {
        foreach ($report_data as $key => $value) {
            $strTo = $value["email_id"];
            $strFrom = "invoice@nailspaexperience.com";
            $strSubject = "Less Product Report";

            $strbody1 = $email_body;
            $headers = "From: $strFrom\r\n";
            $headers .= "Content-type: text/html\r\n";
            $strBodysa = AntiFilter1($strbody1);

// Mail sending 
            $retval = mail($strTo, $strSubject, $strBodysa, $headers);
        }
    }
}
?>