<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
$strPageTitle = "Upsell Report | NailSpa";
$strDisplayTitle = "Upsell Report for NailSpa";
$strMenuID = "10";
$strMyTable = "tblAppointments";
$strMyTableID = "AppointmentID";
$strMyField = "AppointmentDate";
$strMyActionPage = "upsell_report.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <?php require_once("incChart-SalonDashboard.fya"); ?>

        <!-----------------included datatable.css & js files by gandhali 1/9/18----------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/datatable/datatable.css">
        <script type="text/javascript" src="assets/widgets/datatable/datatable.js"></script>
        <script type="text/javascript" src="assets/widgets/datatable/datatable-bootstrap.js"></script>
        <script type="text/javascript" src="assets/widgets/datatable/datatable-responsive.js"></script>
        <!-----------------included datatable.css by gandhali 1/9/18----------------->
        <style>
            .btn-danger:hover
            {
                border-color: #fc8213;
                background: #fc8213;
            }

        </style>
        <!-- Styles -->


    </head>

    <body>

        <div id="sb-site">


            <?php // require_once("incOpenLayout.fya");   ?>
            <!----------commented by gandhali 1/9/18---------------->


            <?php require_once("incLoader.fya"); ?>


            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>

                        <script type="text/javascript" src="assets/widgets/skycons/skycons.js"></script>
                        <script type="text/javascript" src="assets/widgets/datatable/datatable.js"></script>
                        <script type="text/javascript" src="assets/widgets/datatable/datatable-bootstrap.js"></script>
                        <script type="text/javascript" src="assets/widgets/datatable/datatable-tabletools.js"></script>

                        <?php
                        $sqlTempfrom = '';
                        $sqlTempto = '';
                        if (isset($_GET["toandfrom"])) {
                            $strtoandfrom = $_GET["toandfrom"];
                            $arraytofrom = explode("-", $strtoandfrom);

                            $from = $arraytofrom[0];
                            $datetime = new DateTime($from);
                            $getfrom = $datetime->format('Y-m-d');


                            $to = $arraytofrom[1];
                            $datetime = new DateTime($to);
                            $getto = $datetime->format('Y-m-d');

                            if (!IsNull($from)) {
                                $sqlTempfrom = " AND Date(non_visiting_update_date)>=Date('" . $getfrom . "')";
                            }

                            if (!IsNull($to)) {
                                $sqlTempto = " AND Date(non_visiting_update_date)<=Date('" . $getto . "')";
                            }

                            $append = '';
                            if (isset($_GET["Store"]) && $_GET["Store"] != '') {
                                $append .= " AND StoreID ='" . $_GET["Store"] . "'";
                            }
                            $DB = Connect();
                            $query = "SELECT  apt.*,inv.service_amount_deduct_dis,inv.is_upsell FROM `tblAppointments` apt 
                                 JOIN `tblAppointmentsDetailsInvoice` inv on apt.AppointmentId = inv.AppointmentID 
                                                          WHERE (`AppointmentDate` BETWEEN '" . $getfrom . "' AND '" . $getto . "')
                                                             AND  apt.StoreID != '0'  AND apt.IsDeleted != '1'  AND apt.FreeService !=  '1'  
                                                             AND apt.Status = '2' AND apt.source IN(2,4)";
                           
                            $apt_data = $DB->query($query);
                            if ($apt_data->num_rows > 0) {
                                while ($result = $apt_data->fetch_assoc()) {
                                    if ($result['source'] == 4 && $result['is_upsell'] == 0) {
                                        $is_upsell = 2;
                                    } else {
                                        $is_upsell = $result['is_upsell'];
                                    }
                                    if (isset($final_res[$result['StoreID']])) {
                                        $final_res[$result['StoreID']][$result['source']][$is_upsell] += $result['service_amount_deduct_dis'];
                                    } else {
                                        $final_res[$result['StoreID']][$result['source']][$is_upsell] = $result['service_amount_deduct_dis'];
                                    }

                                    if (isset($final_res[0])) {
                                        $final_res[0][$result['source']][$is_upsell] += $result['service_amount_deduct_dis'];
                                    } else {
                                        $final_res[0][$result['source']][$is_upsell] = $result['service_amount_deduct_dis'];
                                    }
                                }
                            }
                        }
                        ?>

                        <div class="panel">
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="example-box-wrapper">
                                        <div id="normal-tabs-1">
                                            <span class="form_result">&nbsp; <br>
                                            </span>

                                            <div class="panel-body">
                                                <h4 class="title-hero"><center>Upsell Report | NailSpa</center></h4>
                                                <br>

                                                <form method="get" class="form-horizontal bordered-row" role="form" action="upsell_report.php">
                                                    <div class="form-group"><label for="" class="col-sm-4 control-label">Select date</label>
                                                        <div class="col-sm-4">
                                                            <div class="input-prepend input-group">
                                                                <span class="add-on input-group-addon">
                                                                    <i class="glyph-icon icon-calendar"></i>
                                                                </span> 
                                                                <input type="text" name="toandfrom" id="daterangepicker-example" class="form-control" value="<?php echo isset($strtoandfrom) ? $strtoandfrom : ''; ?>">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                        <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                                        &nbsp;&nbsp;&nbsp;
                                                        <a class="btn btn-link" href="telecalling_report.php">Clear All Filter</a>
                                                        &nbsp;&nbsp;&nbsp;

                                                    </div>
                                                </form>

                                                <div class="example-box-wrapper">
                                                    <?php if (isset($_GET["toandfrom"]) && $_GET["toandfrom"] != '') { ?>
                                                        <table class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                                                            <thead>
                                                                <tr>
                                                                    <th>Store</th>
                                                                    <th>Online actual Sale</th>
                                                                    <th>Online Upsale</th>
                                                                    <th>Online Enquiry Sale</th>
                                                                    <th>Telecaller actual Sale</th>
                                                                    <th>Telecaller Upsale</th>
                                                                    <th>Total</th>
                                                                </tr>
                                                            </thead>


                                                            <tbody>
                                                                <tr>
                                                                    <td>All</td>
                                                                    <td><?php
                                                                        $all_onli_actu = isset($final_res[0][2][2]) ? $final_res[0][2][2] : 0;
                                                                        echo $all_onli_actu;
                                                                        ?></td>
                                                                    <td><?php
                                                                        $all_onli_upsale = isset($final_res[0][2][1]) ? $final_res[0][2][1] : 0;
                                                                        echo $all_onli_upsale;
                                                                        ?></td>
                                                                    <td><?php
                                                                        $all_enquiry_sale = isset($final_res[0][2][0]) ? $final_res[0][2][0] : 0;
                                                                        echo $all_enquiry_sale;
                                                                        ?></td>
                                                                    <td><?php
                                                                        $all_tele_actu = isset($final_res[0][4][2]) ? $final_res[0][4][2] : 0;
                                                                        echo $all_tele_actu;
                                                                        ?></td>
                                                                    <td><?php
                                                                        $all_tele_upsale = isset($final_res[0][4][1]) ? $final_res[0][4][1] : 0;
                                                                        echo $all_tele_upsale;
                                                                        ?></td>
                                                                    <td>
                                                                        <?php
                                                                        $all_total = $all_enquiry_sale + $all_onli_actu + $all_onli_upsale + $all_tele_actu + $all_tele_actu;
                                                                        echo $all_total;
                                                                        ?>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                                $total_actual_sale = 0;
                                                                $total_enquiry_sale = 0;
                                                                $total_actual_upsale = 0;
                                                                $selp = select("*", "tblStores", "Status='0'");
                                                                if (isset($selp) && is_array($selp) && count($selp) > 0) {
                                                                    foreach ($selp as $skey => $svalue) {
                                                                        ?>
                                                                        <tr>
                                                                            <td><?php echo ucwords($svalue["StoreName"]); ?></td>
                                                                            <td><?php
                                                                                $all_onli_actu = isset($final_res[$svalue['StoreID']][2][2]) ? $final_res[$svalue['StoreID']][2][2] : 0;
                                                                                echo $all_onli_actu;
                                                                                $total_actual_sale += $all_onli_actu;
                                                                                ?></td>
                                                                            <td><?php
                                                                                $all_onli_upsale = isset($final_res[$svalue['StoreID']][2][1]) ? $final_res[$svalue['StoreID']][2][1] : 0;
                                                                                echo $all_onli_upsale;
                                                                                $total_actual_upsale += $all_onli_upsale;
                                                                                ?></td>
                                                                            <td><?php
                                                                                $all_enquiry_sale = isset($final_res[$svalue['StoreID']][2][0]) ? $final_res[$svalue['StoreID']][2][0] : 0;
                                                                                echo $all_enquiry_sale;
                                                                                $total_enquiry_sale += $all_enquiry_sale;
                                                                                ?></td>
                                                                            <td><?php
                                                                                $all_tele_actu = isset($final_res[$svalue['StoreID']][4][2]) ? $final_res[$svalue['StoreID']][4][2] : 0;
                                                                                echo $all_tele_actu;
                                                                                $total_actual_sale += $all_tele_actu;
                                                                                ?></td>
                                                                            <td><?php
                                                                                $all_tele_upsale = isset($final_res[$svalue['StoreID']][4][1]) ? $final_res[$svalue['StoreID']][4][1] : 0;
                                                                                echo $all_tele_upsale;
                                                                                $total_actual_upsale += $all_tele_upsale;
                                                                                ?></td>
                                                                            <td>
                                                                                <?php
                                                                                $all_total = $all_enquiry_sale + $all_onli_actu + $all_onli_upsale + $all_tele_actu + $all_tele_actu;
                                                                                echo $all_total;
                                                                                ?>
                                                                            </td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </tbody>
                                                            <thead>
                                                                <tr>
                                                                    <th colspan="4">Total actual Sale</th>
                                                                    <th colspan="3"><?php echo $total_actual_sale; ?></th>
                                                                </tr>
                                                                <tr>
                                                                    <th colspan="4">Total Upsale</th>
                                                                    <th colspan="3"><?php echo $total_actual_upsale; ?></th>
                                                                </tr>
                                                                <tr>
                                                                    <th colspan="4">Total Enquiry Sale</th>
                                                                    <th colspan="3"><?php echo $total_enquiry_sale; ?></th>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                        <?php
                                                    } else {
                                                        echo "<br><center><h3>Please Select Month And Year!</h3></center>";
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php require_once 'incFooter.fya'; ?>
        </div>
    </body>
</html>