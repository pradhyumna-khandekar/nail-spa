<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
	$strPageTitle = "Offer addition Step 2 | Nailspa";
	$strDisplayTitle = "New addition of center with module & service selection for Nailspa";
	$strMenuID = "3";
	$strMyTable = "tblAdmin";
	$strMyTableID = "AdminID";
	$strMyField = "Title";
	$strMyActionPage = "TransferNewCenter_Offers.php";
	$strMessage = "";
	$sqlColumn = "";
	$sqlColumnValues = "";
// code for not allowing the normal admin to access the super admin rights	
if($strAdminType!="0")
{
	die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights	

	if ($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$strStep = Filter($_POST["step"]);
		if($strStep=="edit")
		{	
			$DB=Connect();
			$Offerss=$_POST["Offers"];
			$StoreIDfetch=Filter($_POST["StoreID"]);
			
			// echo $StoreIDfetch;
			// die();
			$first=array($StoreIDfetch);
			// print_r($first);
			$risk=",".$StoreIDfetch;
			// echo "<br>";
			foreach($Offerss as $offr)
			{
				$SeStoreID="Select StoreID from tblOffers where OfferID='$offr'";
				
				$RSabc = $DB->query($SeStoreID);
				if ($RSabc->num_rows > 0) 
				{
					// echo "In if<br>";
					while($rowabc = $RSabc->fetch_assoc())
					{
						$Offerstores = $rowabc["StoreID"];
						
						
						$second=array($Offerstores);					
						
						
						$finalarr=array_combine($second, $first);
						
						$finalidd=implode(",",$finalarr);
						$string = $Offerstores;
						$string .= ",".$StoreIDfetch;
						 // echo $string;
						
						$DestArray = array();
						$DestArray = explode(",",$Offerstores);
						
						if(in_array($StoreIDfetch, $DestArray))
						{
							// echo "In if<br>";
							echo ('<div class="alert alert-close alert-success">
								<div class="bg-green alert-icon"><i class="glyph-icon icon-check"></i></div>
									<div class="alert-content">
									<h4 class="alert-title">This Store is already added to selected offers.</h4><br>
									</div>
								</div>');
						}
						else
						{
?>
<?php							
							// echo "In else<br>";
							$updation="Update tblOffers SET StoreID='$string' where OfferID='$offr'";
				
							if ($DB->query($updation) === TRUE) 
							{
								//$last_id = $DB->insert_id;
								// $newstoreid = $DB->insert_id;
								$getStoreID=EncodeQ($StoreIDfetch);
?>
									<!--<button class="btn ra-100 btn-azure"><a href="TransferNewCenter_Packages.php?uid=<?=$getStoreID?>">Next<div class="ripple-wrapper"></div></a></button>-->
<?php
								
									echo("<script>location.href='TransferNewCenter_Packages.php?uid=".$getStoreID."';</script>");
									die('<div class="alert alert-close alert-success">
									<div class="bg-green alert-icon"><i class="glyph-icon icon-check"></i></div>
										<div class="alert-content">
										<h4 class="alert-title">Store updated Successfully.</h4>
										</div>
									</div>');
									die();
									
									
?>
									<!--<a class="btn btn-link font-green pull-right" href="TransferNewCenter_Packages.php?uid=<?=$getStoreID?>"><button class="btn btn-success">Next</button></a>-->
<?php								
								
							}
							else
							{
								echo "Error: " . $sql . "<br>" . $conn->error;
							} 
						}
						
						
					}
				}
				else
				{
									
				}
				
					
			}
			die();
		}
		
	}	
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<?php require_once("incMetaScript.fya"); ?>
</head>

<body>
	 <div id="sb-site">
        
		<?php require_once("incOpenLayout.fya"); ?>
		
		
        <?php require_once("incLoader.fya"); ?>
		
		<div id="page-wrapper">
			<div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>
			<?php require_once("incLeftMenu.fya"); ?>
			
			<div id="page-content-wrapper">
				<div id="page-content">
                    
					<?php require_once("incHeader.fya"); ?>
					
					<div id="page-title">
                        <h2>Add offers for New Outlet</h2>
                    
                    </div>
				
					<div class="panel">
						<div class="panel-body">
							<div class="fa-hover">	
								<a class="btn btn-primary btn-lg btn-block"><i class="fa fa-backward">Offers</i></a>
							</div>
						
							<div class="panel-body">
								<!--<form role="form" class="form-horizontal bordered-row enquiry_form" onSubmit="proceed_formsubmit('.enquiry_form', '<?=$strMyActionPage?>', '.result_message', '', '', '','.imageupload'); return false;" enctype="multipart/form-data">-->
							<form role="form" class="form-horizontal bordered-row enquiry_form" onSubmit="proceed_formsubmit('.enquiry_form', '<?=$strMyActionPage?>', '.result_message', '', '','', '.imageupload'); return false;">
								<span class="result_message">&nbsp; <br>
								</span>
								<br>
								<input type="hidden" name="step" value="edit">

								
									<h3 class="title-hero">Step 2 : Select Offers for your store.</h3>
									<div class="example-box-wrapper">
<?php

$strID = $strAdminID;
// echo $strID."<br>";
$DB = Connect();
?>

<?php
$strID = DecodeQ(Filter($_GET["uid"]));
// echo $strID;
if(isset($_GET["uid"]) && !empty($_GET["uid"]))
{
	// echo $strID;
?>

											
											<input type="hidden" name="StoreID" value="<?=$strID?>">
											
                                            <div class="form-group"><label class="col-sm-3 control-label">Select Offers<span>*</span></label>				

												<div class="col-sm-6">	
                                                 <select class="form-control required" id="Offers" name="Offers[]" onChange="checktypewqfge(this);" multiple style="height:200pt;">
												
												<option value="" selected>--SELECT Offers--</option>

												<?php 
												
														$DB = Connect();
                                                        
														$sep="Select * from tblOffers";
														$RSabc = $DB->query($sep);
														if ($RSabc->num_rows > 0) 
														{
															while($rowabc = $RSabc->fetch_assoc())
															{
																$OfferCode = $rowabc["OfferCode"];
																$OfferID = $rowabc["OfferID"];
																$Status = $rowabc["Status"];
																if($Status==0)
																{
																	$Status1="Live";
																}
																else
																{
																	$Status1="Offline";
																}
?>
																<option value="<?php echo $OfferID?>"><?php echo $OfferCode?>  - <?=$Status1?>  </option>
<?php																
															}
														}
														
												$DB->close();																

												?>

												</select>

												</div>

											</div>
											
											
											
											<div class="form-group"><label class="col-sm-3 control-label"></label>
												<input type="submit" class="btn ra-100 btn-primary" value="Update">
												
												<div class="col-sm-1"><a class="btn ra-100 btn-black-opacity" href="TransferNewCenter_Packages.php?uid=<?=EncodeQ($strID)?>"  title="Skip"><span>Skip this Step</span></a></div>
											</div>
<?php
// $DB->close();
?>										
				</div>
								</form>
							</div>
<?php	// echo $strID;
}
else
{
	echo "You are on the Wrong Page. StoreID is missing";
}
// die();
?>							
						</div>
					</div>			
                  
                </div>
            </div>
        </div>
		
        <?php require_once 'incFooter.fya'; ?>
		
    </div>
</body>

</html>									