<?php require_once("setting.fya"); ?>
<?php

$DB = Connect();
/*
 * Get All Active Store
 */

$store = select("*", "tblStores", "Status = 0");
if (isset($store) && is_array($store) && count($store) > 0) {
    foreach ($store as $key => $value) {
        $store_ids[$value['StoreID']] = $value;
        $ids_arr[$value['StoreID']] = $value['StoreID'];
    }
}


if (isset($ids_arr) && is_array($ids_arr) && count($ids_arr) > 0) {
    $in_store_ids = implode(",", $ids_arr);
    if ($in_store_ids != '') {
        /*
         * Get Count Of Todays Booked Appointment
         */
        $apt_sql = "SELECT COUNT(AppointmentID) as apt_count,StoreID FROM tblAppointments WHERE  "
                . " StoreID IN(" . $in_store_ids . ") and created_date LIKE '%" . date('Y-m-d') . "%' "
                . " AND source=1 GROUP BY StoreID";
        $apt_sql_exe = $DB->query($apt_sql);
        if ($apt_sql_exe->num_rows > 0) {
            while ($count_data = $apt_sql_exe->fetch_assoc()) {
                $future_count[$count_data['StoreID']] = isset($count_data['apt_count']) ? $count_data['apt_count'] : 0;
            }
        }
    }
}

$email_body = '<html id="printarea">
    <head>
        <style>
            .main-table{background-color:#fafafa;font-family:Arial, Helvetica, sans-serif; font-size:14px; margin:0; padding:0;}
            tr, td{font-size: 14px;}
            th{color:#795548;padding:5px;}
            h1{margin: 0;padding: 10px;background-color: #f5f5f5;text-align:left;font-size:25px;color:#607D8B;}
            .report-table{border-collapse: collapse;display:block;text-align:center;}
            .report-table tr{border-bottom: 2px solid #795548;}
            .report-table tr td{padding:5px;}
        </style>
    </head>


    <body>

        <table  class="main-table" cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">

            <td align="center" valign="top" style="padding:20px 0 20px 0">

                <table bgcolor="#FFFFFF" cellspacing="0" cellpadding="10" border="0" width="600" style="border:1px solid #dddddd;">


                    <tr>
                        <td colspan="2" style="padding:10px;">
                            <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td colspan="2" style="background-color: #f5f5f5;padding:15px 0 0;text-align:center;">
                                        <h1 class="text-center" style="padding:0 0 15px;background:none;text-align:center;font-size:25px;color:#607D8B;">Future Appointment Booked On ' . date('d/m/Y') . '</h1>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>';


$email_body.= '<tr>
                                                    <td colspan="2" valign="top" style="border-bottom:1px solid #dddddd; padding:10px; ">
                                                        <table class="report-table">
                                                            <tr style="background-color: #000;">
                                                                <th style="color: #fff;">Sr. No.</th>
                                                                <th style="color: #fff;">Store</th>
                                                                <th style="color: #fff;">Count</th>
                                                            </tr>';




if (isset($store_ids) && is_array($store_ids) && count($store_ids) > 0) {
    $counter = 1;
    foreach ($store_ids as $key => $value) {

        $appo_count = isset($future_count[$value['StoreID']]) ? $future_count[$value['StoreID']] : 0;

        $email_body .= '<tr id="my_data_tr_<?= $counter ?>"> 
                                                                    <td>' . $counter . '</td>
                                                                    <td>' . $value['StoreName'] . '</td>
                                                                    <td>' . $appo_count . '</td>
                                                                    
     
</tr>';
        $counter++;
    }
}
$email_body .= '</tbody>

                                                        </table>
                                                    </td>
                                                </tr>';

$DB->close();

$email_body .= '</table>
            </td>
        </tr>
    </table>
</body>
</html>';
return $email_body;
