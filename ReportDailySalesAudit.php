<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Report Daily Audit Sales | Nailspa";
$strDisplayTitle = "Report Daily Audit Sales Nailspa";
$strMenuID = "2";
$strMyTable = "tblStoreStock";
$strMyTableID = "StoreStockID";
$strMyField = "";
$strMyActionPage = "ReportDailySalesAudit.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights	



if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $strStep = Filter($_POST["step"]);

    if ($strStep == "add") {
        
    }

    if ($strStep == "edit") {
        
    }
}
$csvdata = array();
?>


<?php
if (isset($_GET["toandfrom"])) {
    $strtoandfrom = $_GET["toandfrom"];
    $arraytofrom = explode("-", $strtoandfrom);

    $from = $arraytofrom[0];
    $datetime = new DateTime($from);
    $getfrom = $datetime->format('Y-m-d');


    $to = $arraytofrom[1];
    $datetime = new DateTime($to);
    $getto = $datetime->format('Y-m-d');

    if (!IsNull($from)) {
        //$sqlTempfrom = " and Date(tblInvoiceDetails.OfferDiscountDateTime)>='" . $getfrom . "'";
        $sqlTempfrom = " and Date(tblAppointments.AppointmentDate)>='" . $getfrom . "'";
        $sqlTempfrom1 = " and Date(tblAppointmentMembershipDiscount.DateTimeStamp)>='" . $getfrom . "'";
        $sqlTempfrom2 = " and Date(tblGiftVouchers.RedempedDateTime)>='" . $getfrom . "'";
        $sqlTempfrom3 = " and Date(tblPendingPayments.DateTimeStamp)>='" . $getfrom . "'";
        $sqlTempfrom4 = " and Date(tblGiftVouchers.Date)>='" . $getfrom . "'";
    }

    if (!IsNull($to)) {
        //$sqlTempto = " and Date(tblInvoiceDetails.OfferDiscountDateTime)<='" . $getto . "'";
        $sqlTempto = " and Date(tblAppointments.AppointmentDate)<='" . $getto . "'";
        $sqlTempto1 = " and Date(tblAppointmentMembershipDiscount.DateTimeStamp)<='" . $getto . "'";
        $sqlTempto2 = " and Date(tblGiftVouchers.RedempedDateTime)>='" . $getto . "'";
        $sqlTempto3 = " and Date(tblPendingPayments.DateTimeStamp)>='" . $getto . "'";
        $sqlTempto4 = " and Date(tblGiftVouchers.Date)>='" . $getto . "'";
    }
}

if (!IsNull($_GET["Store"])) {
    $strStoreID = $_GET["Store"];

    $sqlTempStore = " StoreID='$strStoreID'";
}
?>	


<!DOCTYPE html>
<html lang="en">

<head>
<?php require_once("incMetaScript.fya"); ?>

<script type="text/javascript" src="assets/widgets/datepicker/datepicker.js"></script>
<script type="text/javascript">
/* Datepicker bootstrap */

$(function () {
    "use strict";
    $('.bootstrap-datepicker').bsdatepicker({
        format: 'mm-dd-yyyy'
    });



});
</script>
<script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker-demo.js"></script>
<script type="text/javascript" src="assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
</head>
<script>

function printDiv(divName)
{
    $("#heading").show();
    var divToPrint = document.getElementById("printdata");
    $("#main").removeAttr("style")

    var htmlToPrint = '' +
            '<style type="text/css">' +
            'table th, table td {' +
            'border:1px solid #000;' +
            'padding;0.5em;' +
            '}' +
            '</style>';
    htmlToPrint += divToPrint.outerHTML;
    newWin = window.open("");
    newWin.document.write(htmlToPrint);
    newWin.print();
    newWin.close();
    // var printContents = document.getElementById(divName);
    // var originalContents = document.body.innerHTML;

    // document.body.innerHTML = printContents;

    // window.print();

    // document.body.innerHTML = originalContents; 
}

</script>
<body>
<div id="sb-site">

<?php // require_once("incOpenLayout.fya");      ?>
<!----------commented by gandhali 5/9/18---------------->


<?php require_once("incLoader.fya"); ?>

<div id="page-wrapper">
    <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

    <?php require_once("incLeftMenu.fya"); ?>

    <div id="page-content-wrapper">
        <div id="page-content">

            <?php require_once("incHeader.fya"); ?>


            <style type="text/css">
                @media print {
                    body * {
                        visibility: hidden;
                    }
                    #printarea * {
                        visibility: visible;
                    }
                    #printarea{
                        position: absolute;
                        left: 0;
                        top: 0;
                    }
                }
                #di table
                {
                    border:none;
                }
                .maindata
                {
                    overflow-x: scroll;
                }
            </style>

            <div id="page-title">
                <h2><?= $strDisplayTitle ?></h2>
            </div>
            <?php
            if (!isset($_GET["uid"])) {
                ?>					

                <div class="panel">
                    <div class="panel">
                        <div class="panel-body">


                            <div class="example-box-wrapper">
                                <div class="tabs">

                                    <div id="normal-tabs-1">

                                        <span class="form_result">&nbsp; <br>
                                        </span>

                                        <div class="panel-body">
                                            <h3 class="title-hero">List of Daily Sales</h3>

                                            <form method="get" class="form-horizontal bordered-row" role="form">

                                                <div class="form-group"><label for="" class="col-sm-4 control-label">Select Date Range</label>
                                                    <div class="col-sm-4">
                                                        <div class="input-prepend input-group">
                                                            <span class="add-on input-group-addon">
                                                                <i class="glyph-icon icon-calendar"></i>
                                                            </span> 
                                                            <input type="text" name="toandfrom" id="daterangepicker-example" class="form-control" value="<?= $strtoandfrom ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Select Store</label>
                                                    <div class="col-sm-4">
                                                        <select name="Store" class="form-control">
                                                            <option value="0">All</option>
                                                            <?php
                                                            $selp = select("*", "tblStores", "Status='0'");
                                                            foreach ($selp as $val) {
                                                                $strStoreName = $val["StoreName"];
                                                                $strStoreID = $val["StoreID"];
                                                                $store = $_GET["Store"];
                                                                if ($store == $strStoreID) {
                                                                    ?>
                                                                    <option  selected value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <option value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Select Appointment Source</label>
                                                    <div class="col-sm-4">
                                                        <select name="source" class="form-control">
                                                            <option value="0">All</option>
                                                            <?php
                                                            $sel_source = select("*", "tblsource", "status = '1'");
                                                            foreach ($sel_source as $val) {
                                                                ?>
                                                                <option value="<?= $val['id']; ?>" <?php echo isset($_GET['source']) && $_GET['source'] == $val['id'] ? 'selected' : ''; ?>><?= $val['name'] ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                    <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a class="btn btn-link" href="ReportDailySalesAudit.php">Clear All Filter</a>




                                                    <?php
                                                    $datedrom = $_GET["toandfrom"];

                                                    if ($datedrom != "") {
                                                        ?>
                                                        <button onclick="printDiv('printarea')" class="btn btn-blue-alt">Print<div class="ripple-wrapper"></div></button>

                                                        &nbsp;&nbsp;&nbsp;


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <!--<a class="btn btn-border btn-alt border-primary font-primary" href="ExcelExportData.php?from=<?= $getfrom ?>&to=<?= $getto ?>" title="Excel format 2016"><span>Export To Excel</span><div class="ripple-wrapper"></div></a>-->
                                                        <?php
                                                    }
                                                    ?>

                                                    <!--<a class="btn btn-link" href="ReportKaushalAudit.php?dev=star" target="_blank">Export Excel</a>-->
                                                </div>
                                            </form>

                                            <div id="printdata">	
                                                <button onclick="download_csv()">Download CSV</button> 
                                                <h2 class="title-hero" id="heading" style="display:none"><center>Report Daily Sales Audit</center></h2>
                                                <br>
                                                <?php
                                                $datedrom = $_GET["toandfrom"];
                                                if ($datedrom != "" && !IsNull($_GET["Store"])) {
                                                    $storrr = $_GET["Store"];
                                                    if ($storrr == '0') {
                                                        $storrrp = 'All';
                                                    } else {
                                                        $stpp = select("StoreName", "tblStores", "StoreID='" . $storrr . "'");
                                                        $StoreName = $stpp[0]['StoreName'];
                                                        $storrrp = $StoreName;
                                                    }
                                                    ?>

                                                    <h3 >Date Range selected : FROM - <?= $getfrom ?> / TO - <?= $getto ?> / Store Filter selected : <?= $storrrp ?> </h3>

                                                    <br>



                                                    <?php
                                                    $DB = Connect();

                                                    $counter = 0;
                                                    ?>
                                                    <div class="panel">

                                                        <div class="panel-body" id="main" class="maindata" style="overflow-x: scroll;">

                                                            <div class="example-box-wrapper">
                                                                <div class="scroll-columns">



                                                                    <table  class="table table-bordered table-striped table-condensed cf datadisplay">
                                                                        <thead class="cf">
                                                                            <tr>
                                                                                <?php
                                                                                if ($storrr == '0') {
                                                                                    ?>
                                                                                    <th>Sr</th>
                                                                                    <th>Customer Name</th>
                                                                                    <th>Customer Email</th>
                                                                                    <th>Invoice No</th>
                                                                                    <th class="numeric">Service Sale</th>
                                                                                    <th class="numeric">Product Sale</th>
                                                                                    <th class="numeric">GV Sale</th>
                                                                                    <th class="numeric">GV Discount</th>
                                                                                    <th class="numeric">Super GV Sale</th>
                                                                                    <th class="numeric">Super GV Discount</th>
                                                                                    <th class="numeric">Package Sale</th>
                                                                                    <th class="numeric">Package Discount</th>
                                                                                    <th class="numeric">Membership Sale</th>
                                                                                    <th class="numeric">Membership Discount</th>
                                                                                    <th class="numeric">Offer Discount</th>
                                                                                    <th class="numeric">SGST @ 9%</th>
                                                                                    <th class="numeric">CGST @ 9%</th>
                                                                                    <th class="numeric">Invoice Amount</th>
                                                                                    <th class="numeric">Pending Received In Cash</th>
                                                                                    <th class="numeric">Pending Received In Card</th>
                                                                                    <th class="numeric">Total Sale</th>
                                                                                    <th class="numeric">Current Pending</th>
                                                                                    <th class="numeric">Card Amount</th>
                                                                                    <th class="numeric">Cash Amount</th>
                                                                                    <th>Check In Time</th>
                                                                                    <th>Check Out Time</th>
                                                                                    <th>Store</th>
                                                                                    <?php
                                                                                } else {
                                                                                    ?>
                                                                                    <th>Sr</th>
                                                                                    <th>Customer Name</th>
                                                                                    <th>Customer Email</th>
                                                                                    <th>Invoice No</th>
                                                                                    <th class="numeric">Service Sale</th>
                                                                                    <th class="numeric">Product Sale</th>
                                                                                    <th class="numeric">GV Sale</th>
                                                                                    <th class="numeric">GV Discount</th>
                                                                                    <th class="numeric">Super GV Sale</th>
                                                                                    <th class="numeric">Super GV Discount</th>
                                                                                    <th class="numeric">Package Sale</th>
                                                                                    <th class="numeric">Package Discount</th>
                                                                                    <th class="numeric">Membership Sale</th>
                                                                                    <th class="numeric">Membership Discount</th>
                                                                                    <th class="numeric">Offer Discount</th>
                                                                                    <th class="numeric">SGST @ 9%</th>
                                                                                    <th class="numeric">CGST @ 9%</th>
                                                                                    <th class="numeric">Invoice Amount</th>
                                                                                    <th class="numeric">Pending Received In Cash</th>
                                                                                    <th class="numeric">Pending Received In Card</th>
                                                                                    <th class="numeric">Total Sale</th>
                                                                                    <th class="numeric">Current Pending</th>
                                                                                    <th class="numeric">Card Amount</th>
                                                                                    <th class="numeric">Cash Amount</th>
                                                                                    <th>Check In Time</th>
                                                                                    <th>Check Out Time</th>

                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                            </tr>
                                                                        </thead>

                                                                        <tbody class="datadisplay">

                                                                            <?php
                                                                            $total_net_amount = 0;
                                                                            $TotalPackageDis = 0;
                                                                            $csvdata = array();
                                                                            $storr = $_GET["Store"];
                                                                            $sepqtsTYU = select("*", "tblAppointments", "IsDeleted!='0'");
                                                                            foreach ($sepqtsTYU as $vaqp) {
                                                                                $appp[] = $vaqp['AppointmentID'];
                                                                            }
                                                                            $CustomerEmailID = $sepqts[0]['CustomerEmailID'];

                                                                            $append = '';
                                                                            $get_source = $_GET["source"];
                                                                            if (!empty($get_source)) {
                                                                                $append = " AND tblAppointments.source='" . $get_source . "'";
                                                                            }

                                                                            if (!empty($storr)) {

                                                                                $sqlservice = "SELECT DISTINCT tblAppointments.total_tax,tblAppointments.PackageID,tblAppointments.new_package,tblAppointments.super_gv_amount,tblAppointments.super_gv_status,tblAppointments.super_gv_id,tblInvoiceDetails.CustomerFullName,tblInvoiceDetails.CustomerID,tblInvoiceDetails.ServiceAmt,tblInvoiceDetails.ServiceName,tblInvoiceDetails.RoundTotal,tblInvoiceDetails.CashAmount,tblInvoiceDetails.CardAmount,tblInvoiceDetails.Membership_Amount,tblAppointments.StoreID,tblInvoiceDetails.AppointmentId,tblAppointments.AppointmentCheckInTime,tblAppointments.AppointmentCheckOutTime,tblInvoiceDetails.GVPurchasedID,tblInvoiceDetails.PendingAmount,tblInvoiceDetails.Flag from tblInvoiceDetails left join tblAppointments 
on tblInvoiceDetails.AppointmentId=tblAppointments.AppointmentID where tblAppointments.StoreID='" . $storr . "' and tblAppointments
.Status='2' AND tblAppointments.IsDeleted=0 $sqlTempfrom $sqlTempto $append ORDER BY tblInvoiceDetails.OfferDiscountDateTime DESC";

//echo $sqlservice;exit;
// echo $sqlservice;
                                                                            } else {
                                                                                $sqlservice = "SELECT DISTINCT tblAppointments.total_tax,tblAppointments.PackageID,tblAppointments.new_package,tblAppointments.super_gv_amount,tblAppointments.super_gv_status,tblAppointments.super_gv_id,tblInvoiceDetails.AppointmentId, tblInvoiceDetails.CustomerFullName,tblInvoiceDetails.CustomerID,tblInvoiceDetails.ServiceAmt,tblInvoiceDetails.ServiceName,tblInvoiceDetails.RoundTotal,tblInvoiceDetails.CashAmount,tblInvoiceDetails.CardAmount,tblInvoiceDetails.Membership_Amount,tblAppointments.StoreID,tblInvoiceDetails.AppointmentId,tblAppointments.AppointmentCheckInTime,tblAppointments.AppointmentCheckOutTime,tblInvoiceDetails.GVPurchasedID,tblInvoiceDetails.PendingAmount,tblInvoiceDetails.Flag from tblInvoiceDetails left join tblAppointments on tblInvoiceDetails.AppointmentId=tblAppointments.AppointmentID left join tblPendingPayments on tblInvoiceDetails.AppointmentId=tblPendingPayments.AppointmentID  where tblAppointments.StoreID!='0' and tblAppointments
.Status='2'  AND tblAppointments.IsDeleted=0 $sqlTempfrom $sqlTempto $append ORDER BY tblInvoiceDetails.OfferDiscountDateTime DESC";
                                                                            }




                                                                            $RSservice = $DB->query($sqlservice);
                                                                            if ($RSservice->num_rows > 0) {
                                                                                $counterservice = 0;
                                                                                $total_balance_reveived = 0;
                                                                                $total_invoice_amount = 0;
                                                                                $total_sale_amount = 0;
                                                                                $total_super_gv_amout = 0;
                                                                                $total_super_gv_discount = 0;
                                                                                while ($rowservice = $RSservice->fetch_assoc()) {


                                                                                    $toatlseramt = "";

                                                                                    $SaifAppointmentID = $rowservice["AppointmentId"];
                                                                                    $getUID = EncodeQ($SaifAppointmentID);
                                                                                    $CustomerFullName = $rowservice["CustomerFullName"];
                                                                                    $RoundTotal = $rowservice["RoundTotal"];
                                                                                    $AppointmentId = $rowservice["AppointmentId"];
                                                                                    if (in_array("$AppointmentId", $appp)) {
                                                                                        
                                                                                    } else {
                                                                                        $counterservice ++;
                                                                                        $ServiceAmt = $rowservice["ServiceAmt"];
                                                                                        $ServiceName = $rowservice["ServiceName"];
                                                                                        $CustomerID = $rowservice["CustomerID"];

                                                                                        $Flag = $rowservice["Flag"];

                                                                                        if ($Flag == 'CS') {
                                                                                            $PendingAmountycs = $rowservice["PendingAmount"];
                                                                                            $PendingAmountyc = 0;
                                                                                        } else {
                                                                                            $PendingAmountyc = $rowservice["PendingAmount"];
                                                                                            $PendingAmountycs = 0;
                                                                                        }

                                                                                        $sepqts = select("*", "tblCustomers", "CustomerID='" . $CustomerID . "'");
                                                                                        $CustomerEmailID = $sepqts[0]['CustomerEmailID'];
                                                                                        $StoreID = $rowservice["StoreID"];

                                                                                        $sepqto = select("*", "tblInvoice", "AppointmentID='" . $AppointmentId . "'");
                                                                                        $invoice = $sepqto[0]['InvoiceID'];

                                                                                        if ($rowservice['new_package'] == 1) {
                                                                                            $sepqtpack = select("PackageID", "tblInvoiceDetails", "AppointmentId='" . $AppointmentId . "' and PackageIDFlag='P'");
                                                                                            $PACKID = $sepqtpack[0]['PackageID'];
                                                                                        } else {
                                                                                            $PACKID = 0;
                                                                                        }

                                                                                        $package = explode(",", $PACKID);

                                                                                        /* $sepqt = select("*", "tblPendingPayments", "PendingStatus='2' and AppointmentID='" . $AppointmentId . "' $sqlTempfrom3 $sqlTempto3");
                                                                                          $PendingAmount = $sepqt[0]['PendingAmount'];

                                                                                          $sepqtQW = select("count(*)", "tblPendingPayments", "Status='1' and PendingStatus='2' and AppointmentID='" . $AppointmentId . "' $sqlTempfrom3 $sqlTempto3");
                                                                                          $cnttt = $sepqtQW[0]['count(*)']; */

                                                                                        $SuperGVAmount = 0;
                                                                                        $row_package_discount = 0;
                                                                                        if ($rowservice['super_gv_status'] == 1 && $rowservice['super_gv_id'] != 0) {
                                                                                            $new_super_gv = select("*", "super_gift_voucher", "id='" . $rowservice['super_gv_id'] . "'");
                                                                                            $SuperGVAmount = $new_super_gv[0]['GVAmount'];
                                                                                            $total_super_gv_amout += $SuperGVAmount;
                                                                                        }
                                                                                        $superGVDiscount = 0;
                                                                                        if ($rowservice['super_gv_status'] == 2 && $rowservice['super_gv_id'] != 0) {
                                                                                            $superGVDiscount = $rowservice['super_gv_amount'];
                                                                                            $total_super_gv_discount += $superGVDiscount;
                                                                                        }

                                                                                        $sepqt = select("*", "tblPendingPayments", "AppointmentID='" . $AppointmentId . "'");
                                                                                        $PendingAmount = $sepqt[0]['PendingAmount'];
                                                                                        $cnttt = count($sepqt);


                                                                                        if ($Flag == 'H') {
                                                                                            $CashAmount = 0;
                                                                                            $CardAmount = 0;
                                                                                        } else if ($sepqt[0]['PendingAmount'] && $sepqt[0]['PendingAmount'] > 0) {
                                                                                            $CashAmount = $rowservice["CashAmount"];
                                                                                            $CardAmount = $rowservice["CardAmount"];
                                                                                            if ($CashAmount > 0) {
                                                                                                $CashAmount = $RoundTotal - $PendingAmount;
                                                                                            } elseif ($CardAmount > 0) {
                                                                                                $CardAmount = $RoundTotal - $PendingAmount;
                                                                                            }
                                                                                        } else {
                                                                                            $CashAmount = $rowservice["CashAmount"];
                                                                                            $CardAmount = $rowservice["CardAmount"];
                                                                                        }



                                                                                        /*
                                                                                         * Get Current Pending
                                                                                         */
                                                                                        $cur_pending = select("*", "tblPendingPayments", "PendingStatus='2' and AppointmentID='" . $AppointmentId . "'");
                                                                                        $cur_pending_amount = isset($cur_pending[0]['PendingAmount']) ? $cur_pending[0]['PendingAmount'] : 0;



                                                                                        $Membership_Amount = $rowservice["Membership_Amount"];
                                                                                        $memamtfirst = explode(",", $Membership_Amount);

                                                                                        $memamtfirst = str_replace("+", "", $Membership_Amount);
                                                                                        $memamtfirst = str_replace(".00", "", $memamtfirst);
                                                                                        $memamtfirst = str_replace(".", "", $memamtfirst);

                                                                                        $memamtfirst = str_replace(",", "", $memamtfirst);
                                                                                        //	$memamtfirst=str_replace("+", "", $Membership_Amount);
                                                                                        if ($memamtfirst == '') {
                                                                                            $memamtfirst = "0.00";
                                                                                        }

                                                                                        $Totalmemamtfirst += $memamtfirst;


                                                                                        $sepqtpt = select("sum(ChargeAmount) as sumcharge", "tblAppointmentsChargesInvoice", "AppointmentID='" . $AppointmentId . "' AND AppointmentDetailsID !=0");
                                                                                        // echo $sepqtpt;
                                                                                        //$tax = $sepqtpt[0]['sumcharge'];
                                                                                        $tax = $rowservice['total_tax'];


                                                                                        $OpenTime = $rowservice['AppointmentCheckInTime'];
                                                                                        $CloseTime = $rowservice['AppointmentCheckOutTime'];
                                                                                        //$sepqtp = select("sum(OfferAmount) as offamt,sum(MembershipAmount) as memamt", "tblAppointmentMembershipDiscount", "AppointmentID='" . $AppointmentId . "' $sqlTempfrom1 $sqlTempto1");

                                                                                        $offamt = 0;
                                                                                        //$offer_amt_data = select("OfferID,OfferAmount", "tblAppointmentMembershipDiscount", "AppointmentID='" . $AppointmentId . "' $sqlTempfrom1 $sqlTempto1 GROUP BY OfferID");
                                                                                        $offer_amt_data = select("DISTINCT AppointmentID,OfferID,OfferAmount", "tblAppointmentMembershipDiscount", "AppointmentID='" . $AppointmentId . "'");
                                                                                        if (isset($offer_amt_data) && is_array($offer_amt_data) && count($offer_amt_data) > 0) {
                                                                                            foreach ($offer_amt_data as $okey => $ovalue) {
                                                                                                $offamt = $offamt + $ovalue['OfferAmount'];
                                                                                            }
                                                                                        }

                                                                                        $sepqtp = select("sum(OfferAmount) as offamt,sum(MembershipAmount) as memamt", "tblAppointmentMembershipDiscount", "AppointmentID='" . $AppointmentId . "' $sqlTempfrom1 $sqlTempto1");
                                                                                        //$offamt = $sepqtp[0]['offamt'];
                                                                                        $memamt = $sepqtp[0]['memamt'];

                                                                                        $GVPurchasedID = $rowservice["GVPurchasedID"];
                                                                                        $sepqteww = select("*", "tblGiftVouchers", "GiftVoucherID='" . $GVPurchasedID . "' and AppointmentID='" . $AppointmentId . "' $sqlTempfrom4 $sqlTempto4");

                                                                                        $gPurchaseAmountt = $sepqteww[0]['Amount'];

                                                                                        $sepqtew = select("*", "tblGiftVouchers", "Status='1' and RedempedBy='" . $AppointmentId . "'");


                                                                                        $gAmount = $sepqtew[0]['Amount'];
                                                                                        if ($gAmount == "") {
                                                                                            $gAmount = "0.00";
                                                                                        } else {

                                                                                            $gAmount = $gAmount;
                                                                                        }

                                                                                        $TotalgAmount += $gAmount;
                                                                                        $saleamount = $RoundTotal - $gAmount;
                                                                                        $saleamount = $saleamount + $PendingAmountyc;
                                                                                        $saleamount = $saleamount + $PendingAmountycs;
                                                                                        if ($saleamount == "") {
                                                                                            $saleamount = "0.00";
                                                                                        } else {

                                                                                            $saleamount = $saleamount;
                                                                                        }
                                                                                        $Totalsaleamount += $saleamount;
                                                                                        if ($gPurchaseAmountt == "") {
                                                                                            $gPurchaseAmountt = "0.00";
                                                                                        } else {
                                                                                            $gPurchaseAmountt = $gPurchaseAmountt;
                                                                                        }
                                                                                        $TotalgPurchaseAmountt += $gPurchaseAmountt;
                                                                                        if ($OpenTime != '0000-00-00 00:00:00') {
                                                                                            $LoginTimet = date('h:i:s', strtotime($OpenTime));
                                                                                        } else {
                                                                                            $LoginTimet = "00:00:00";
                                                                                        }

                                                                                        if ($CloseTime != '0000-00-00 00:00:00') {
                                                                                            $LogoutTimet = date('h:i:s', strtotime($CloseTime));
                                                                                        } else {
                                                                                            $LogoutTimet = "00:00:00";
                                                                                        }

                                                                                        $serviceamts = explode(",", $ServiceAmt);
                                                                                        $ServiceNames = explode(",", $ServiceName);
                                                                                        $sep = select("*", "tblStores", "StoreID='" . $StoreID . "'");
                                                                                        $storename = $sep[0]['StoreName'];
                                                                                        if ($CashAmount == '') {
                                                                                            $CashAmount = "0.00";
                                                                                        }
                                                                                        if ($CardAmount == '') {
                                                                                            $CardAmount = "0.00";
                                                                                        }


                                                                                        if (isset($rowservice['PackageID']) && $rowservice['PackageID'] > 0) {
                                                                                            $package_dis_data = select("*", "tblCustomerPackageAmt", "appointment_id='" . $AppointmentId . "' AND package_id=" . $rowservice['PackageID'] . " AND status=1");
                                                                                            if (isset($package_dis_data[0]['package_amt_used']) && $package_dis_data[0]['package_amt_used'] > 0) {
                                                                                                $row_package_discount = $package_dis_data[0]['package_amt_used'];
                                                                                            }
                                                                                        }
                                                                                        $TotalPackageDis += $row_package_discount;
                                                                                        
                                                                                        $service_deduct = select("*", "tblAppointmentsDetailsInvoice", "AppointmentID='" . $AppointmentId . "'");

                                                                                        if (isset($service_deduct) && is_array($service_deduct) && count($service_deduct) > 0) {
                                                                                            foreach ($service_deduct as $skey => $svalue) {
                                                                                                if ($svalue['package_discount'] > 0) {
                                                                                                    $service_deduct_row_amount = ($svalue['ServiceAmount'] * $svalue['qty']) -$row_package_discount ;
                                                                                                } else {
                                                                                                    $service_deduct_row_amount = $svalue['service_amount_deduct_dis'];
                                                                                                }
                                                                                                $toatlseramt += $service_deduct_row_amount + $svalue['super_gv_discount'] - $superGVDiscount - $gAmount;
                                                                                                //echo "<br>id=".$svalue['AppointmentDetailsID']."toatlseramt=".$toatlseramt;
                                                                                                if ($toatlseramt < 0) {
                                                                                                    $toatlseramt = 0;
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        /* for ($i = 0; $i < count($serviceamts); $i++) {
                                                                                          $toatlseramt +=$serviceamts[$i];
                                                                                          } */
                                                                                        unset($serviceamts);
                                                                                        $serviceamts = "";

                                                                                        if ($_SERVER['REMOTE_ADDR'] == "111.119.219.70") {
                                                                                            for ($i = 0; $i < count($ServiceNames); $i++) {
                                                                                                //echo $ServiceNames[$i]."<br/>";
                                                                                            }
                                                                                            unset($ServiceNames);
                                                                                            $ServiceNames = "";
                                                                                        } else {
                                                                                            
                                                                                        }
                                                                                        for ($q = 0; $q < count($package); $q++) {
                                                                                            if ($package[$q] != '') {
                                                                                                $sepqtewpack = select("*", "tblPackages", "PackageID='" . $package[$q] . "'");
                                                                                                $packprice = $sepqtewpack[0]['PackageNewPrice'];
                                                                                                $PackageNewPrice +=$packprice;
                                                                                            }
                                                                                        }
                                                                                        unset($package);



                                                                                        if ($PackageNewPrice == "") {
                                                                                            $PackageNewPrice = "0.00";
                                                                                        } else {

                                                                                            $PackageNewPrice = $PackageNewPrice;
                                                                                        }
                                                                                        $TotalPackageNewPrice += $PackageNewPrice;



                                                                                        if ($toatlseramt == "") {
                                                                                            $toatlseramt = "0.00";
                                                                                        } else {

                                                                                            $toatlseramt = $toatlseramt;
                                                                                        }
                                                                                        $Totaltoatlseramt += $toatlseramt;

                                                                                        if ($offamt == "") {
                                                                                            $offamt = "0.00";
                                                                                        } else {

                                                                                            $offamt = $offamt;
                                                                                        }
                                                                                        $Totaloffamt += $offamt;
                                                                                        if ($tax == "") {
                                                                                            $tax = "0.00";
                                                                                        } else {

                                                                                            $tax = $tax;
                                                                                        }
                                                                                        $Totaltax += $tax;
                                                                                        if ($memamt == "") {
                                                                                            $memamt = "0.00";
                                                                                        } else {

                                                                                            $memamt = $memamt;
                                                                                        }
                                                                                        $Totalmemamt += $memamt;
                                                                                        if ($PendingAmount == "") {
                                                                                            $PendingAmount = "0.00";
                                                                                        } else {

                                                                                            $PendingAmount = $PendingAmount;
                                                                                        }


                                                                                        if ($PendingAmountyc == "") {
                                                                                            $PendingAmountyc = "0.00";
                                                                                        } else {

                                                                                            $PendingAmountyc = $PendingAmountyc;
                                                                                        }
                                                                                        $TotalPendingAmountyc += $PendingAmountyc;
                                                                                        if ($PendingAmountycs == "") {
                                                                                            $PendingAmountycs = "0.00";
                                                                                        } else {

                                                                                            $PendingAmountycs = $PendingAmountycs;
                                                                                        }
                                                                                        $TotalPendingAmountycs += $PendingAmountycs;
                                                                                        if ($CashAmount == "") {
                                                                                            $CashAmount = "0.00";
                                                                                        } else {

                                                                                            $CashAmount = $CashAmount;
                                                                                        }


                                                                                        if ($CardAmount == "") {
                                                                                            $CardAmount = "0.00";
                                                                                        } else {

                                                                                            $CardAmount = $CardAmount;
                                                                                        }


                                                                                        if ($RoundTotal == "") {
                                                                                            $RoundTotal = "0.00";
                                                                                        } else {

                                                                                            $RoundTotal = $RoundTotal;
                                                                                        }
                                                                                        $TotalRoundTotal += $RoundTotal;
                                                                                        ?>								<tr id="my_data_tr_<?= $counterservice ?>">

                                                                                            <?php
                                                                                            $csvdata[$counterservice]['counterservice'] = $counterservice;
                                                                                            $csvdata[$counterservice]['CustomerFullName'] = str_replace(",", " ", $CustomerFullName);
                                                                                            $csvdata[$counterservice]['CustomerEmailID'] = str_replace(",", " ", $CustomerEmailID);
                                                                                            $csvdata[$counterservice]['InvoiceNo'] = $invoice;
                                                                                            $csvdata[$counterservice]['toatlseramt'] = $toatlseramt;
                                                                                            $csvdata[$counterservice]['gv_amount'] = $gPurchaseAmountt;
                                                                                            $csvdata[$counterservice]['gv_discount'] = $gAmount;
                                                                                            $csvdata[$counterservice]['super_gv_amount'] = $SuperGVAmount;
                                                                                            $csvdata[$counterservice]['super_gv_dicsount'] = $superGVDiscount;
                                                                                            $csvdata[$counterservice]['package_amount'] = $PackageNewPrice;
                                                                                            $csvdata[$counterservice]['memsale'] = str_replace("+", "", $memamtfirst);
                                                                                            $csvdata[$counterservice]['memamt'] = $memamt;
                                                                                            $csvdata[$counterservice]['offamt'] = $offamt;
                                                                                            $csvdata[$counterservice]['sgst'] = $tax / 2;
                                                                                            $csvdata[$counterservice]['cgst'] = $tax / 2;
                                                                                            if ($PendingAmountycs != 0) {
                                                                                                $balance_rec = $PendingAmountycs;
                                                                                            } else {
                                                                                                $balance_rec = $PendingAmountyc;
                                                                                            }
                                                                                            $paid_amount = $CardAmount + $CashAmount;
                                                                                            //$csvdata[$counterservice]['balance_received'] = $balance_rec;
                                                                                            $csvdata[$counterservice]['invoiceamt'] = $RoundTotal - $balance_rec;
                                                                                            $csvdata[$counterservice]['netamount'] = $RoundTotal - $tax - $balance_rec;
                                                                                            if ($csvdata[$counterservice]['netamount'] < 0) {
                                                                                                $csvdata[$counterservice]['netamount'] = 0;
                                                                                            }
                                                                                            $csvdata[$counterservice]['saleamount'] = $saleamount - (2 * $balance_rec);

                                                                                            if ($csvdata[$counterservice]['saleamount'] < 0) {
                                                                                                $csvdata[$counterservice]['saleamount'] = 0;
                                                                                            }
                                                                                            $csvdata[$counterservice]['balance_received'] = $paid_amount - $csvdata[$counterservice]['saleamount'];

                                                                                            if ($csvdata[$counterservice]['balance_received'] < 0) {
                                                                                                $csvdata[$counterservice]['balance_received'] = 0;
                                                                                            }
                                                                                            if ($CashAmount > 0) {
                                                                                                $CashAmount = $CashAmount - $csvdata[$counterservice]['balance_received'];
                                                                                            } else if ($CardAmount > 0) {
                                                                                                $CardAmount = $CardAmount - $csvdata[$counterservice]['balance_received'];
                                                                                            }
                                                                                            $csvdata[$counterservice]['CardAmount'] = $CardAmount;
                                                                                            $csvdata[$counterservice]['CashAmount'] = $CashAmount;


                                                                                            //$cur_pending_amount = $RoundTotal - $paid_amount;
                                                                                            $TotalPendingAmount += $cur_pending_amount;

                                                                                            // $csvdata[$counterservice]['current_pending'] = $PendingAmount;
                                                                                            $csvdata[$counterservice]['current_pending'] = $cur_pending_amount;

                                                                                            $total_net_amount = $total_net_amount + $csvdata[$counterservice]['netamount'];
                                                                                            $total_balance_reveived += $csvdata[$counterservice]['balance_received'];
                                                                                            $total_invoice_amount += $csvdata[$counterservice]['invoiceamt'];
                                                                                            $total_sale_amount += $csvdata[$counterservice]['saleamount'];

                                                                                            $TotalCashAmount += $CashAmount;
                                                                                            $TotalCardAmount += $CardAmount;
                                                                                            $csvdata[$counterservice]['row_package_dis'] = $row_package_discount;
                                                                                            if ($storrr == '0') {
                                                                                                $downloadflag = $_GET['download'];

                                                                                                /* if ($PendingAmountycs != 0) {
                                                                                                  $balance_rec = $PendingAmountycs;
                                                                                                  } else {
                                                                                                  $balance_rec = $PendingAmountyc;
                                                                                                  } */
                                                                                                $balance_rec = $csvdata[$counterservice]['balance_received'];
                                                                                                ?>
                                                                                                <td><center><?= $counterservice ?></center></td>
                                                                                        <td><center><?= $CustomerFullName ?></center></td>
                                                                                        <td><center><?= $CustomerEmailID ?></center></td>
                                                                                        <td><center><?= $invoice ?></center></td>
                                                                                        <td class="numeric"><center><?= $toatlseramt ?></center></td>
                                                                                        <td class="numeric"><center>-</center></td>
                                                                                        <td class="numeric"><center><?= $gPurchaseAmountt ?></center></td>
                                                                                        <td class="numeric"><center><?= $gAmount ?></center></td>
                                                                                        <td class="numeric"><center><?php echo $SuperGVAmount; ?></center></td>
                                                                                        <td class="numeric"><center><?php echo $superGVDiscount; ?></center></td>
                                                                                        <td class="numeric"><center><?= $PackageNewPrice ?></center></td>
                                                                                        <td class="numeric"><center><?= $row_package_discount ?></center></td>
                                                                                        <td class="numeric"><center><?= str_replace("+", "", $memamtfirst) ?></center></td>
                                                                                        <td class="numeric"><center><?= $memamt ?></center></td>
                                                                                        <td class="numeric"><center><?= $offamt ?></center></td>
                                                                                        <td class="numeric"><center><?= $tax / 2 ?></center></td>
                                                                                        <td class="numeric"><center><?= $tax / 2 ?></center></td>
                                                                                        <td class="numeric"><center><?= $csvdata[$counterservice]['invoiceamt']; ?></center></td>
                                                                                        <td class="numeric"><center><?= $PendingAmountycs && $PendingAmountycs != 0 ? $csvdata[$counterservice]['balance_received'] : 0 ?></center></td>
                                                                                        <td class="numeric"><center><?= $PendingAmountyc && $PendingAmountyc != 0 ? $csvdata[$counterservice]['balance_received'] : 0 ?></center></td>
                                                                                        <td class="numeric"><center><?= $csvdata[$counterservice]['saleamount']; ?></center></td>
                                                                                        <td class="numeric"><center><?= $csvdata[$counterservice]['current_pending'] ?></center></td>
                                                                                        <td class="numeric"><center><?= $CardAmount ?></center></td>
                                                                                        <td class="numeric"><center><?= $CashAmount ?></center></td>
                                                                                        <td><center><?= $LoginTimet ?></center></td>
                                                                                        <td><center><?= $LogoutTimet ?></b></center></td>
                                                                                        <td><center><?= $storename ?></center></td>
                                                                                        <?php
                                                                                    } else {
                                                                                        ?>

                                                                                        <td><center><?= $counterservice ?></center></td>
                                                                                        <td><center><?= $CustomerFullName ?></center></td>
                                                                                        <td><center><?= $CustomerEmailID ?></center></td>
                                                                                        <td>
                                                                                        <center>
                                                                                            <?php
                                                                                            if ($_SERVER['REMOTE_ADDR'] == "120.61.169.4" || $_SERVER['REMOTE_ADDR'] == "111.119.219.70") {
                                                                                                ?>
                                                                                                <a id="status" class="btn btn-link" target='blank' href="PendingInvoicesForDisplay.php?uid=<?= $getUID; ?>"><?= $invoice ?></a>
                                                                                                <?php
                                                                                            } else {
                                                                                                ?>
                                                                                                <?= $invoice ?>
                                                                                                <?php
                                                                                            }
                                                                                            ?>
                                                                                        </center>
                                                                                        </td>
                                                                                        <td class="numeric"><center><?= $toatlseramt ?></center></td>
                                                                                        <td class="numeric"><center>-</center></td>

                                                                                        <td class="numeric"><center><?= $gPurchaseAmountt ?></center></td>
                                                                                        <td class="numeric"><center><?= $gAmount ?></center></td>
                                                                                        <td class="numeric"><center><?php echo $SuperGVAmount; ?></center></td>
                                                                                        <td class="numeric"><center><?php echo $superGVDiscount; ?></center></td>
                                                                                        <td class="numeric"><center><?= $PackageNewPrice ?></center></td>
                                                                                        <td class="numeric"><center><?= $row_package_discount ?></center></td>
                                                                                        <td class="numeric"><center><?= str_replace("+", "", $memamtfirst) ?></center></td>
                                                                                        <td class="numeric"><center><?= $memamt ?></center></td>
                                                                                        <td class="numeric"><center><?= $offamt ?></center></td>
                                                                                        <td class="numeric"><center><?= $tax / 2 ?></center></td>
                                                                                        <td class="numeric"><center><?= $tax / 2 ?></center></td>
                                                                                        <td class="numeric"><center><?= $csvdata[$counterservice]['invoiceamt'] ?></center></td>
                                                                                        <td class="numeric"><center><?= $PendingAmountycs && $PendingAmountycs != 0 ? $csvdata[$counterservice]['balance_received'] : 0 ?></center></td>
                                                                                        <td class="numeric"><center><?= $PendingAmountyc && $PendingAmountyc != 0 ? $csvdata[$counterservice]['balance_received'] : 0 ?></center></td>
                                                                                        <td class="numeric"><center><?= $csvdata[$counterservice]['saleamount']; ?></center></td>
                                                                                        <td class="numeric"><center><?= $csvdata[$counterservice]['current_pending'] ?></center></td>
                                                                                        <td class="numeric"><center><?= $CardAmount ?></center></td>
                                                                                        <td class="numeric"><center><?= $CashAmount ?></center></td>
                                                                                        <td><center><?= $LoginTimet ?></center></td>
                                                                                        <td><center><?= $LogoutTimet ?></b></center></td>

                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                    </tr>

                                                                                    <?php
                                                                                }


                                                                                $PackageNewPrice = "";
                                                                            }


                                                                            unset($serviceamts);
                                                                            $toatlseramt = "";
                                                                            $PackageNewPrice = "";
                                                                        } else {
                                                                            ?>

                                                                            <tr>
                                                                                <?php
                                                                                if ($storrr == '0') {
                                                                                    ?>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td>No Data Found</td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <?php
                                                                                } else {
                                                                                    ?>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td>No Data Found</td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                            </tr>


                                                                            <?php
                                                                        }
                                                                        ?>							
                                                                        </tbody>
                                                                        <tbody>
                                                                            <tr>

                                                                                <?php
                                                                                $csv_total['service_sale'] = $Totaltoatlseramt;
                                                                                $csv_total['membership_sale'] = $Totalmemamtfirst;
                                                                                $csv_total['membership_discount'] = $Totalmemamt;
                                                                                $csv_total['offer_discount'] = $Totaloffamt;
                                                                                $csv_total['net_amount'] = $total_net_amount;
                                                                                $csv_total['total_gv_amout'] = $TotalgPurchaseAmountt;
                                                                                $csv_total['total_gv_discount'] = $TotalgAmount;
                                                                                $csv_total['total_super_gv_amout'] = $total_super_gv_amout;
                                                                                $csv_total['total_super_gv_discount'] = $total_super_gv_discount;
                                                                                $csv_total['total_package_amout'] = $TotalPackageNewPrice;
                                                                                $csv_total['total_package_dis'] = $TotalPackageDis;
                                                                                $csv_total['sgst'] = $Totaltax / 2;
                                                                                $csv_total['cgst'] = $Totaltax / 2;
                                                                                $csv_total['total_balance_reveived'] = $total_balance_reveived;
                                                                                $csv_total['invoice_amount'] = $total_invoice_amount;
                                                                                $csv_total['total_sale'] = $total_sale_amount;
                                                                                $csv_total['card_amount'] = $TotalCardAmount;
                                                                                $csv_total['cash_amount'] = $TotalCashAmount;
                                                                                $csv_total['total_cur_pending'] = $TotalPendingAmount;


                                                                                if ($storrr == '0') {
                                                                                    ?>
                                                                                    <td colspan="3"><b>Total Amounts(s)</b></td>
                                                                                    <td class="numeric"></td>

                                                                                    <td class="numeric"><center><b>Rs. <?= $Totaltoatlseramt ?>/-</b></center></td>
                                                                            <td class="numeric"></td>


                                                                            <td class="numeric"><center><b>Rs. <?= $TotalgPurchaseAmountt ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $TotalgAmount ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?php echo $total_super_gv_amout; ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?php echo $total_super_gv_discount; ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $TotalPackageNewPrice ?></b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $TotalPackageDis ?></b></center></td>

                                                                            <td class="numeric"><center><b>Rs. <?= $Totalmemamtfirst ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $Totalmemamt ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $Totaloffamt ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $Totaltax / 2 ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $Totaltax / 2 ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $csv_total['invoice_amount'] ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $TotalPendingAmountycs && $TotalPendingAmountycs != 0 ? $csv_total['total_balance_reveived'] : 0 ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $TotalPendingAmountyc && $TotalPendingAmountyc != 0 ? $csv_total['total_balance_reveived'] : 0 ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $csv_total['total_sale'] ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $TotalPendingAmount ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $TotalCardAmount ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $TotalCashAmount ?>/-</b></center></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                            <td colspan="2"><b>Total Amounts(s)</b></td>
                                                                            <td class="numeric"></td>
                                                                            <td class="numeric"></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $Totaltoatlseramt ?>/-</b></center></td>
                                                                            <td class="numeric"></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $TotalgPurchaseAmountt ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $TotalgAmount ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?php echo $total_super_gv_amout; ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?php echo $total_super_gv_discount; ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $TotalPackageNewPrice ?></b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $TotalPackageDis ?></b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $Totalmemamtfirst ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $Totalmemamt ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $Totaloffamt ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $Totaltax / 2 ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $Totaltax / 2 ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $csv_total['invoice_amount'] ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $TotalPendingAmountycs && $TotalPendingAmountycs != 0 ? $csv_total['total_balance_reveived'] : 0 ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $TotalPendingAmountyc && $TotalPendingAmountyc != 0 ? $csv_total['total_balance_reveived'] : 0 ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $csv_total['total_sale'] ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $TotalPendingAmount ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $TotalCardAmount ?>/-</b></center></td>
                                                                            <td class="numeric"><center><b>Rs. <?= $TotalCashAmount ?>/-</b></center></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <?php
                                                //if($_SERVER['REMOTE_ADDR']=="111.119.219.70")
                                                //{
                                                // if(isset($_GET["toandfrom"]))
                                                // {
                                                // if(!$storrr=='0')
                                                // {
                                                ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <!--<a class="btn btn-danger" target="_blank" href="ReportKaushalCatAudit.php?dev=star&<?= str_replace("/admin/ReportDailySalesAudit.php?", "", $_SERVER[REQUEST_URI]); ?>&cd=<?= $TotalCashAmount ?>&crd=<?= $TotalCardAmount ?>&od=<?= $Totaloffamt ?>&md=<?= $Totalmemamt ?>&ms=<?= $Totalmemamtfirst ?>&st=<?= $Totaltax ?>&pdc=<?= $TotalPendingAmountycs ?>&pdcrd=<?= $TotalPendingAmountyc ?>&cpd=<?= $TotalPendingAmount ?>">Export Tally Excel</a>-->
                                                <?php
                                                //}
                                                ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <!--&nbsp;<a class="btn btn-danger" target="_blank" href="ReportKaushalAudit.php?dev=star&<?= str_replace("/admin/ReportDailySalesAudit.php?", "", $_SERVER[REQUEST_URI]); ?>">Export Excel</a>-->
                                                <?php
                                                // }
                                                // else
                                                // {
                                                // }
                                                //}
                                                ?>

                                                <?php
                                                $DB->close();
                                                ?>


                                                <?php
                                            } else {
                                                echo "<br><center><h3>Please Select Month And Year!</h3></center>";
                                            }
                                            ?>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            } // End null condition
            else {
                
            }
            ?>
        </div>
    </div>

    <?php require_once 'incFooter.fya'; ?>
</div>
<?php ?>
<script>
    var data = [
<?php
if (isset($csvdata) && is_array($csvdata) && count($csvdata)) {
    foreach ($csvdata as $key => $value) {
        echo "['" . $value['counterservice'] . "','" . $value['CustomerFullName'] . "','" . $value['CustomerEmailID'] . "','" . $value['InvoiceNo'] . "','" . $value['toatlseramt'] . "','"
        . $value['gv_amount'] . "','" . $value['gv_discount'] . "','" . $value['super_gv_amount'] . "','" . $value['super_gv_dicsount'] . "','" . $value['package_amount'] . "','" . $value['row_package_dis'] . "','"
        . $value['memsale'] . "','" . $value['memamt'] . "','" . $value['offamt'] . "','" . $value['netamount'] . "','" . $value['sgst'] . "','" . $value['cgst'] . "','" . $value['invoiceamt'] . "','" . $value['balance_received'] . "','" . $value['saleamount'] . "','" . $value['current_pending'] . "','" . $value['CardAmount'] . "','" . $value['CashAmount'] . "'],";
        ?>
        <?php
    }
}

if (isset($csv_total) && is_array($csv_total) && count($csv_total) > 0) {
    echo "['','','','Total','" . $csv_total['service_sale'] . "','"
    . $csv_total['total_gv_amout'] . "','" . $csv_total['total_gv_discount'] . "','" . $csv_total['total_super_gv_amout'] . "','" . $csv_total['total_super_gv_discount'] . "','" . $csv_total['total_package_amout'] . "','" . $csv_total['total_package_dis'] . "','"
    . $csv_total['membership_sale'] . "','" . $csv_total['membership_discount'] . "','" . $csv_total['offer_discount'] . "','" . $csv_total['net_amount'] . "','" . $csv_total['sgst'] . "','" . $csv_total['cgst'] . "','" . $csv_total['invoice_amount'] . "','" . $csv_total['total_balance_reveived'] . "','" . $csv_total['total_sale'] . "','" . $csv_total['total_cur_pending'] . "','" . $csv_total['card_amount'] . "','" . $csv_total['cash_amount'] . "'],";
}
?>

                ];


                function download_csv() {
                    var csv = 'Sr,Customer Name,Customer Email,Invoice No,Service Sale,GV Sale,GV Discount,Super GV Sale,Super GV Discount,Package Sale,Package Discount,Membership Sale,Membership Discount,Offer Discount,Net Amount,SGST @ 9%,CGST @ 9%,Invoice Amount,Pending Received,Total Sale,Current Pending,Card Amount,Cash Amount\n';
                    data.forEach(function (row) {
                        csv += row.join(',');
                        csv += "\n";
                    });

                    console.log(csv);
                    var hiddenElement = document.createElement('a');
                    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
                    hiddenElement.target = '_blank';
                    hiddenElement.download = 'gst.csv';
                    hiddenElement.click();
                }
            </script>
    </body>

</html>