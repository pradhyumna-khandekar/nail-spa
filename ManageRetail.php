<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
error_reporting(0);
$strPageTitle = "Manage Retail | NailSpa";
$strDisplayTitle = "Manage Retail for NailSpa";
$strMenuID = "10";
$strMyTable = "tblAppointments";
$strMyTableID = "AppointmentID";
$strMyField = "AppointmentDate";
$strMyActionPage = "ManageRetail.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
    </head>
    <body>

        <div id="sb-site">
            <?php // require_once("incOpenLayout.fya");     ?>
            <!----------commented by gandhali 3/9/18---------------->
            <?php require_once("incLoader.fya"); ?>
            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>
                        <?php $p_index = 1; ?>
                        <script type="text/javascript" src="assets/scanner/jquery-code-scanner.js"></script>
                        <script>
                            $(function () {
                                $('#printcontent').codeScanner({
                                    onScan: function ($element, code) {
                                        $element.find('input.barcode[value=""]').first().val(code);
                                    }
                                });
                            });
                            var Row_count = '<?php echo $p_index + 1; ?>';

                            function RemoveRow(row_id) {
                                $('.row_' + row_id).remove();
                                apply_offer();
                                CalculateTotal();
                            }

                            function CalculateTotal() {

                                var total_sub_amount = 0;
                                var total_amount = 0;
                                var total_tax = 0;
                                var product_sub_total = 0;
                                var pdt_amount = document.getElementsByClassName("pdt_amount");

                                for (var i = 0; i < pdt_amount.length; i++)
                                {
                                    var amount = pdt_amount[i].value;
                                    amount = parseFloat(amount);
                                    product_sub_total = product_sub_total + amount;
                                }
                                for (var i = 0; i < pdt_amount.length; i++)
                                {
                                    var amount = pdt_amount[i].value;

                                    if (amount != '') {
                                        amount = parseFloat(amount);
                                        var row_id = pdt_amount[i].id;
                                        var res = row_id.split("_");
                                        var row_no = res[1];
                                        var tax_per = $("#tax_per_" + row_no).val();
                                        tax_per = parseFloat(tax_per);

                                        if ($("#off_discount").val() != undefined && $("#off_discount").val() > 0) {
                                            var off_discount = Math.round($("#off_discount").val());
                                            console.log('product_sub_total=' + product_sub_total + 'off_discount=' + off_discount + 'amount=' + amount);
                                            var productDis = (amount * off_discount) / product_sub_total;
                                            var productDedAmount = amount - productDis;
                                        } else {
                                            var productDedAmount = amount;
                                        }

                                        $("#amount_ded_discount_" + row_no).val(productDedAmount);
                                        var tax_amount = (productDedAmount * tax_per) / 100;
                                        tax_amount = tax_amount.toFixed(2);
                                        tax_amount = parseFloat(tax_amount);
                                        $("#tax_amount_" + row_no).val(tax_amount);
                                        total_amount = total_amount + amount + tax_amount;
                                        total_tax = total_tax + tax_amount;
                                        total_sub_amount = total_sub_amount + amount;
                                    }
                                }
                                var round_total = Math.round(total_amount);
                                var totalpend = 0;

                                if ($("#totalpend").val() != undefined) {
                                    var totalpend = Math.round($("#totalpend").val());
                                }

                                off_discount = 0;
                                if ($("#off_discount").val() != undefined) {
                                    var off_discount = Math.round($("#off_discount").val());
                                }
                                round_total = round_total + totalpend - off_discount;

                                $("#sub_total").val(total_sub_amount);
                                $("#total_tax").val(total_tax);
                                $("#totalcost").val(round_total);
                                $("#roundtotal").val(round_total);
                                $("#partialamtt").val(round_total);

                                var partialamt = Math.round($("#partialamt").val());

                                var pending_amt = round_total - partialamt;

                                console.log('pending_amt=' + pending_amt);
                                //console.log('round_total=' + round_total);

                                $("#pendamt").val(pending_amt);
                                $("#totalpaymentamt").val(round_total);

                                if (Number(partialamt) > Number(round_total)) {
                                    alert('Partial Amount Cannot Be Greater Than Total Amount')
                                }

                            }

                            function checktypeofpayemnt() {
                                var type = $('input[name="paytype"]:checked').val();
                                //alert(type)
                                if (type == 'Partial') {
                                    $(".partial").show();

                                } else if (type == 'Complete') {
                                    var totalcost = $("#roundtotal").val();
                                    $(".partial").hide();
                                }
                            }


                            function calculatecomplete() {
                                var complete = $("#partialamt").val();
                                var totalcost = Math.round($("#partialamtt").val());
                                if (Number(complete) > Number(totalcost)) {
                                    $("#partialamt").val(0);
                                    $("#pendamt").val(0);
                                    alert('Complete Amount Should Not Be Greater Total Payment')
                                } else {
                                    $("#partialamt").val();
                                    var amt = parseFloat(totalcost) - parseFloat(complete);
                                    $("#pendamt").val(amt);
                                    $("#totalpaymentamt").val(totalcost);
                                }
                            }

                            function calculatepend() {
                                var pend = $("#pendamt").val();
                                var totalcost = Math.round($("#partialamtt").val());
                                if (Number(pend) > Number(totalcost)) {
                                    alert('Pending amount should not be greater then total pending amount!')
                                } else {
                                    $("#pendamt").val();
                                    var amt = parseFloat(totalcost) - parseFloat(pend);
                                    var test = parseFloat(totalcost) + parseFloat(amt);
                                    $("#partialamt").val(amt);
                                    $("#totalpaymentamt").val(totalcost);
                                }

                            }

                            function getBarcodePdt(row_id) {
                                var barcode = $("#barcode_" + row_id).val();
                                if (barcode != '') {
                                    $.ajax({
                                        type: "post",
                                        data: "barcode=" + barcode,
                                        url: "get_barcode_product.php",
                                        success: function (result)
                                        {
                                            var returnedData = JSON.parse(result);
                                            var response = returnedData['res'];
                                            if (response == 'success') {
                                                var ProductID = returnedData['ProductID'];
                                                var pdtName = returnedData['ProductName'];
                                                var ProductMRP = returnedData['ProductMRP'];
                                                var product_tax = returnedData['product_tax'];
                                                $("#product_name_" + row_id).html(pdtName);
                                                $("#product_id_" + row_id).val(ProductID);
                                                $("#tax_per_" + row_id).val(product_tax);
                                                $("#amount_" + row_id).val(ProductMRP);
                                                apply_offer();
                                                CalculateTotal();
                                                add_more_row();
                                                $(".barcode_" + row_id).focus();
                                            } else {
                                                $("#product_name_" + row_id).html('');
                                                $("#product_id_" + row_id).val('0');
                                                $("#tax_per_" + row_id).val(0);
                                                $("#amount_" + row_id).val('');
                                                $("#barcode_" + row_id).val('');
                                                var error_msg = returnedData['res_msg'];
                                                alert(error_msg);
                                                CalculateTotal();
                                            }
                                        }
                                    });
                                }
                            }

                            function apply_offer() {
                                var offervalue = $("#offername").val();
                                if (offervalue > 0) {

                                    $.ajax({
                                        url: "apply_reatil_offer.php",
                                        type: 'post',
                                        data: $("#printcontent").serialize(),
                                        success: function (msg) {
                                            var returnedData = JSON.parse(msg);
                                            if (returnedData['status'] == 'success') {
                                                var dis_amount = returnedData['dis_amount'];
                                                var new_total = returnedData['new_total'];
                                                $("#off_discount").val(dis_amount);
                                                $("#roundtotal").val(new_total);
                                                CalculateTotal();
                                                alert('Offer Applied Successfully.')
                                            } else {
                                                alert(returnedData['msg']);
                                                $("#offername").val('0');
                                            }
                                        }

                                    });
                                }
                            }

                            function add_more_row() {
                                var append_row = '<tr class="row_' + Row_count + '">';
                                append_row += '<td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;">' + Row_count + '</td>'

                                append_row += '<td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;">';
                                //append_row += '<input type="text" class="barcode" name="barcode[' + Row_count + ']" id="barcode_' + Row_count + '" onblur="getBarcodePdt(' + Row_count + ')" data-id="' + Row_count + '">';
                                append_row += '<input type="text" class="barcode" name="barcode[' + Row_count + ']" id="barcode_' + Row_count + '" data-id="' + Row_count + '">';
                                append_row += '</td>';

                                append_row += '<td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; padding-left:2%;">';
                                append_row += '<input type="hidden" name="product_id[' + Row_count + ']" id="product_id_' + Row_count + '" value=""><span id="product_name_' + Row_count + '"></span>';
                                append_row += '</td>';

                                append_row += '<td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; padding-left:2%;">1</td>';

                                append_row += '<td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;">';
                                append_row += '<input type="text" readonly="" value="00.00" id="amount_' + Row_count + '" class="pdt_amount" name="pdt_amount[' + Row_count + ']">';
                                append_row += '</td>';

                                append_row += '<td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;">';
                                append_row += '<input type="text" readonly="" value="00.00" id="tax_per_' + Row_count + '" class="tax_per" name="tax_per[' + Row_count + ']">';
                                append_row += '</td>';

                                append_row += '<td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;">';
                                append_row += '<input type="text" readonly="" value="00.00" id="tax_amount_' + Row_count + '" class="tax_amount" name="tax_amount[' + Row_count + ']">';
                                append_row += '<input type="hidden" readonly="" value="00.00" id="amount_ded_discount_' + Row_count + '" class="amount_ded_discount" name="amount_ded_discount[' + Row_count + ']">';
                                append_row += '</td>';
                                append_row += '<td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;" class="no-print">';
                                append_row += '<a class="btn btn-xs btn-primary" href="javascript:void(0)" onclick="RemoveRow(' + Row_count + ')">Remove</a></td>';

                                $('.data_body').append(append_row);
                                $('.barcode').keypress(function (event) {
                                    var data_id = $(this).attr('data-id');
                                    var keycode = (event.keyCode ? event.keyCode : event.which);
                                    if (keycode == '13') {
                                        getBarcodePdt(data_id);
                                    }
                                });
                                Row_count = parseInt(Row_count) + 1;
                            }
                            $(document).ready(function () {
                                $(document).on('click', '#addMoreRow', function () {
                                    add_more_row();
                                });

                                $('.barcode').keypress(function (event) {
                                    var data_id = $(this).attr('data-id');
                                    var keycode = (event.keyCode ? event.keyCode : event.which);
                                    if (keycode == '13') {
                                        getBarcodePdt(data_id);
                                    }
                                });
                                $(".submit_invocie").click(function () {
                                    var mode = $(this).val();
                                    var valuesSoFar = Object.create(null);
                                    var dup_bar_name = '';
                                    empty_code = '';
                                    if (confirm('Are you sure you want to save this Invoice?')) {
                                        var pendamt = $("#pendamt").val();
                                        if (pendamt != '' && pendamt < 0) {
                                            alert('Pending Amount cannot be negative.')
                                        } else {
                                            var pdt_barcode = document.getElementsByClassName("barcode");
                                            for (var i = 0; i < pdt_barcode.length; i++)
                                            {
                                                var barcode_value = pdt_barcode[i].value;

                                                if (barcode_value == '') {
                                                    empty_code = 1;
                                                }
                                                if (barcode_value in valuesSoFar) {
                                                    dup_bar_name += ',' + barcode_value;
                                                }

                                                valuesSoFar[barcode_value] = true;

                                            }
                                        }


                                        if (empty_code == 1) {
                                            alert('Barcode cannot be Empty.')
                                        } else if (dup_bar_name != '') {
                                            alert('Duplicate Barcode ' + dup_bar_name + ' Found.')
                                        } else {
                                            $(".submit_invocie").attr("disabled", "disabled");
                                            $.ajax({
                                                url: "printbill.php",
                                                type: 'post',
                                                data: $("#printcontent").serialize() + "&type=" + mode,
                                                success: function (msg) {
                                                    var returnedData = JSON.parse(msg);
                                                    if (returnedData['res'] == 'success') {
                                                        $msg = "Invoice Saved.";
                                                        var apt_id = returnedData['apt_id'];
                                                        window.location = "retail_invoice.php?uid=" + apt_id;
                                                    } else {
                                                        alert('Failed to save invoice.');
                                                    }
                                                }

                                            });
                                        }
                                    }
                                });

                                $("#offerid").click(function () {
                                    apply_offer();
                                });


                            });
                        </script>

                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                            <p>Add Retail Invoice</p>
                        </div>


                        <?php
                        $customerID = DecodeQ(Filter($_GET['bid']));
                        $customerData = select("*", "tblCustomers", "CustomerID='" . $customerID . "'");
                        ?>
                        <form id="printcontent" name="printcontent">

                            <div class="panel">
                                <div class="panel-body">
                                    <label class="control-label" style="width:25%" >Offer Name</label>
                                    <select id="offername" style="width:45%;display:inline-block;" class="form-control offername" name="offername">
                                        <option value="0">Select Here</option>
                                        <?php
                                        $seldata = select("*", "product_offers", "status='1'");
                                        foreach ($seldata as $val) {
                                            ?>
                                            <option value="<?= $val['id'] ?>"><?= $val['OfferName'] ?></option>
                                            <?php
                                        }
                                        ?>

                                    </select>
                                    &nbsp;&nbsp;&nbsp;&nbsp;<button type="button" id="offerid" style="width:25%;display:inline-block;" value="Check Offer" class="btn btn-success" data-toggle="button" ><center>Apply Offer</center></button>
                                </div>
                                <div class="panel">
                                    <div class="panel-body">
                                        <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <table style="BORDER-BOTTOM:#d0ad53 1px solid;BORDER-LEFT:#d0ad53 1px solid;BORDER-TOP:#d0ad53 1px solid;BORDER-RIGHT:#d0ad53 1px solid;background:url('http://nailspaexperience.com/images/test3.png') no-repeat; background-position:50% -140px;" border="0" cellspacing="0" cellpadding="0" width="98%" bgcolor="#ffffff" align="center">
                                                            <tbody>
                                                                <tr>
                                                            <input type="hidden" name="storet" id="storet" value="<?php echo $strStore; ?>">
                                                            <input type="hidden" name="appointment_id" id="appointment_id" value="">
                                                            <input type="hidden" name="appointment_idd" id="appointment_idd" value="0">
                                                            <input type="hidden" name="retail_invoice" id="retail_invoice" value="1">
                                                            <input type="hidden" name="invoiceid" id="invoiceid" value="0">
                                                            <input type="hidden" name="memberid" id="memberid" value="0">
                                                            <td align="middle">
                                                                <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td width="30%" align="left" style="padding:1%;"><img border="0" src="https://pos.nailspaexperience.com/og/images/Nailspa-logo1.png" width="117" height="60"></td>
                                                                            <td width="80%" align="right" style="LINE-HEIGHT:15px; padding:1%; FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:16px;FONT-WEIGHT:bold;">

                                                                                <?php
                                                                                $store_data = select("*", "tblStores", "StoreID='" . $strStore . "'");
                                                                                ?>
                                                                                <input type="hidden" name="store_id" value="<?php echo isset($store_data[0]['StoreID']) ? $store_data[0]['StoreID'] : ''; ?>"/>
                                                                                <input readonly="" name="billaddress" style="FONT-WEIGHT:bold;width:500px;text-align: center;" value="<?php echo isset($store_data[0]['StoreName']) ? $store_data[0]['StoreName'] : ''; ?>">

                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                </tr>
                                                <tr>
                                                    <td style="LINE-HEIGHT:0;BACKGROUND:#d0ad53;FONT-SIZE:0px;" height="5"></td>
                                                </tr>
                                                <tr>
                                                    <td align="middle">
                                                        <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                                            <tbody>
                                                            <input type="hidden" name="cust_id" value="<?php echo isset($customerData[0]['CustomerID']) ? $customerData[0]['CustomerID'] : ''; ?>"/>
                                                            <tr>
                                                                <td width="50%">To ,</td>
                                                            </tr>
                                                            <tr>
                                                                <td width="50%"><b><input readonly="" name="CustomerFullName" value="<?php echo isset($customerData[0]['CustomerFullName']) ? $customerData[0]['CustomerFullName'] : ''; ?>"></b></td>

                                                            </tr>
                                                            <tr>
                                                                <td width="50%"><input readonly="" name="email" value="<?php echo isset($customerData[0]['CustomerEmailID']) ? $customerData[0]['CustomerEmailID'] : ''; ?>"></td>

                                                            </tr>
                                                            <tr>
                                                                <td width="50%"><input readonly="" name="mobile" value="<?php echo isset($customerData[0]['CustomerMobileNo']) ? $customerData[0]['CustomerMobileNo'] : ''; ?>"></td>

                                                            </tr>
                                            </tbody>
                                        </table>
                                        </td>
                                        </tr>


                                        <tr>
                                            <td height="8"></td>
                                        </tr>
                                        <tr>
                                            <td style="LINE-HEIGHT:0;BACKGROUND:#d0ad53;FONT-SIZE:20px;text-align:center;" height="30"><b>Invoice</b></td>
                                        </tr>
                                        <tr>
                                            <td height="8">
                                                <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                                    <tbody>
                                                        <tr>
                                                            <td bgcolor="#e4e4e4" height="4"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>


                                        <tr>
                                            <td>
                                                <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">


                                                    <tbody class="data_body">
                                                        <tr>
                                                            <th width="5%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">Sr</th>
                                                            <th width="5%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">Barcode</th>
                                                            <th width="40%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:left; padding-left:2%;">Item Description</th>
                                                            <th width="5%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">Qty</th>
                                                            <th width="5%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">Amount</th>
                                                            <th width="5%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">Tax(In %)</th>
                                                            <th width="5%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">Tax Amount</th>
                                                            <th width="15%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">Action</th>

                                                        </tr>


                                                        <!--display service detail-->

                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;" class="no-print">
                                                                <a href="#" id="addMoreRow" class="btn btn-xs btn-info">Add</a></td> 
                                                        </tr>
                                                        <tr class="row_<?php echo $p_index; ?>">
                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;">
                                                                <?php echo $p_index; ?></td>

                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;">
<!--                                                                <input type="text" class="barcode" data-id="<?php echo $p_index; ?>" name="barcode[<?php echo $p_index; ?>]" id="barcode_<?php echo $p_index; ?>" onblur="getBarcodePdt(<?php echo $p_index; ?>)">-->
                                                                <input type="text" class="barcode" data-id="<?php echo $p_index; ?>" name="barcode[<?php echo $p_index; ?>]" id="barcode_<?php echo $p_index; ?>">
                                                            </td>

                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; padding-left:2%;">
                                                                <input type="hidden" name="product_id[<?php echo $p_index; ?>]" id="product_id_<?php echo $p_index; ?>" value=""><span id="product_name_<?php echo $p_index; ?>"></span>
                                                            </td>
                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; padding-left:2%;">1</td>

                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;">
                                                                <input type="text" readonly="" value="00.00" id="amount_<?php echo $p_index; ?>" class="pdt_amount" name="pdt_amount[<?php echo $p_index; ?>]">
                                                            </td>
                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;">
                                                                <input type="text" readonly="" value="00.00" id="tax_per_<?php echo $p_index; ?>" class="tax_per" name="tax_per[<?php echo $p_index; ?>]">
                                                            </td>
                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;">
                                                                <input type="text" readonly="" value="00.00" id="tax_amount_<?php echo $p_index; ?>" class="tax_amount" name="tax_amount[<?php echo $p_index; ?>]">
                                                                <input type="hidden" readonly="" value="00.00" id="amount_ded_discount_<?php echo $p_index; ?>" class="amount_ded_discount" name="amount_ded_discount[<?php echo $p_index; ?>]">
                                                            </td>
                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;" class="no-print">
                                                                <a class="btn btn-xs btn-primary" href="javascript:void(0)" id="addMoreRow" onclick="RemoveRow(<?php echo $p_index; ?>)">Remove</a></td>
                                                        </tr>

                                                    </tbody>

                                                </table>
                                            </td>


                                        </tr>


                                        <tr>
                                            <td>
                                                <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">

                                                    <tbody>


                                                        <tr>
                                                            <td width="50%">&nbsp;</td>
                                                            <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;"></td>
                                                            <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"></td>
                                                        </tr>


                                                        <tr>

                                                            <td>&nbsp;</td>
                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Sub Total</td>
                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;" id="totalvalue"><input type="text" name="sub_total" id="sub_total" readonly="" value="0.00"></td>
                                                        </tr>

                                                        <tr>

                                                            <td>&nbsp;</td>
                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Discount</td>
                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;" id="totalvalue">-<input type="text" name="off_discount" id="off_discount" readonly="" value="0.00"></td>
                                                        </tr>

                                                        <tr>

                                                            <td>&nbsp;</td>
                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Tax Amount</td>
                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;" id="totalvalue"><input type="text" name="tax_amount_total" id="total_tax" readonly="" value="0.00"></td>
                                                        </tr>



                                                        <?php
                                                        ////////////////////////////if pending amount is remain then it display//////////////////
                                                        $CustomerFullName = $customerData[0]['CustomerFullName'];
                                                        $sept = select("PendingAmount,AppointmentID", "tblPendingPayments", "CustomerID='" . $CustomerFullName . "' AND Status=1 AND PendingStatus=2 ORDER BY PendingPaymentID DESC LIMIT 1");


                                                        foreach ($sept as $val) {
                                                            $totalpendamt = $totalpendamt + $val['PendingAmount'];
                                                            $total_appointment[0] = $val['AppointmentID'];
                                                        }
                                                        if (isset($total_appointment) && is_array($total_appointment) && count($total_appointment) > 0) {
                                                            $DB = Connect();
                                                            $apt_in_ids = implode(",", $total_appointment);
                                                            $date_q = "SELECT AppointmentID,AppointmentDate FROM tblAppointments WHERE AppointmentID IN(" . $apt_in_ids . ")";
                                                            $date_q_exe = $DB->query($date_q);
                                                            while ($aptdetails = $date_q_exe->fetch_assoc()) {
                                                                $all_apt_date[] = $aptdetails['AppointmentDate'];
                                                            }
                                                            $DB->close();
                                                        }
                                                        ?>
                                                        <?php if ($totalpendamt > 0) { ?>
                                                            <tr id="pendingpayment">

                                                        <input type="hidden" name="pending_amt_apt_id" value="<?php echo $total_appointment[0]; ?>"/>
                                                        <td>&nbsp;</td>
                                                        <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">Pending Amount
                                                            <?php
                                                            if (isset($all_apt_date) && is_array($all_apt_date) && count($all_apt_date) > 0) {
                                                                echo "<br><b>(Invoice Date : " . implode(", ", $all_apt_date) . ")</b>";
                                                            }
                                                            ?>
                                                        </td>
                                                        <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="text" name="totalpend" id="totalpend" readonly value="<?php echo $totalpendamt; ?>"/></td>
                                            </tr>
                                        <?php } ?>
                                        <!-------display round total--------->

                                        <tr>

                                            <td>&nbsp;</td>
                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Total</td>
                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;" id="totalvalue"><input type="text" name="total" id="totalcost" readonly="" value="0.00"></td>
                                        </tr>

                                        <tr>
                                            <td>&nbsp;</td>
                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Round Off</td>
                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input id="roundtotal" type="text" name="roundtotal" readonly="" value="0.00"></td>
                                        </tr>

                                        </tbody>

                                        </table>
                                        </td>
                                        </tr>
                                        <tr>
                                            <td style="display:none" id="payment">
                                                <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">


                                                    <tbody>

                                                        <tr>
                                                            <td colspan="3" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:16px;FONT-WEIGHT:bold; text-align:center;">Payments</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                            <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Cash</td>
                                                            <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="number" id="paymentid" name="cashamt" value="" onkeyup="test()"></td>


                                                        </tr>

                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Total Payment</td>
                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input id="totalpayment" name="totalpayment" value=""></td>
                                                        </tr>


                                                    </tbody>

                                                </table>
                                            </td>
                                        </tr>


                                        <tr>
                                            <td id="paymenttype">
                                                <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                                    <tbody>
                                                        <!--decide which type of paymenyt--->

                                                        <tr id="paymenttype1">
                                                            <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                            <td width="20%" style="display:none;LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-WEIGHT:bold;FONT-SIZE:14px;"><input type="radio" name="paytype" class="paytype" id="pay_par" value="Partial" onclick="checktypeofpayemnt()"><label for="pay_par">Partial</label></td>
                                                            <td width="20%" style="display:none;LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;"><input type="radio" name="paytype" value="Complete" class="paytype" id="pay_com" checked="" onclick="checktypeofpayemnt()"><label for="pay_com">Complete</label></td>
                                                        </tr>
                                                        <!--distribute amount complete partial and pending-->


                                                        <tr style="display:none">
                                                            <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                            <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Complete Amount</td>



                                                            <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="number" id="partialamtt" name="partialamtt" readonly="" value=""></td>


                                                        </tr>


                                                        <tr style="display:none" class="partial">
                                                            <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                            <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Partial Amount</td>
                                                            <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="number" id="partialamt" name="partialamt" onkeyup="calculatecomplete()"></td>
                                                        </tr>
                                                        <tr style="display:none" class="partial">
                                                            <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                            <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Pending Amount</td>
                                                            <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="number" id="pendamt" name="pendamt" onkeyup="calculatepend()" readonly=""></td>
                                                        </tr>


                                                        <tr style="display:none">
                                                           <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>

                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Total Payment</td>
                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input id="totalpaymentamt" name="totalpaymentamt" readonly="" value=""></td>
                                                        </tr>

                                                    </tbody>

                                                </table>
                                            </td>

                                        </tr>

                                        <tr>
                                            <td style="display:none" id="payment1">
                                                <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">


                                                    <tbody>

                                                        <tr>
                                                            <td colspan="3" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:16px;FONT-WEIGHT:bold; text-align:center;">Payments</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                            <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Cash Amount</td>
                                                            <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="number" id="cashboth" name="cashboth" onkeyup="calculatecashamount()"></td>


                                                        </tr>
                                                        <tr>
                                                            <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                            <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Card Amount</td>
                                                            <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="number" id="cardboth" name="cardboth" onkeyup="calculatecardamt()"></td>
                                                        </tr>




                                                    </tbody>

                                                </table>
                                            </td>
                                        </tr>





                                        <tr>
                                            <td height="8">
                                                <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                                    <tbody>
                                                        <tr>
                                                            <td bgcolor="#e4e4e4" height="4"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>


                                        <tr>

                                            <td style="BACKGROUND:#d0ad53;">
                                                <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">

                                                    <tbody>

                                                        <tr><td width="33%" style="FONT-FAMILY:Arial,Helvetica,sans-serif;BACKGROUND:#d0ad53;COLOR:#000;FONT-SIZE:12px; padding:1%;" height="32" align="center">
                                                                <span style="font-size:14px; font-weight:600;">KHAR | BREACH CANDY | ANDHERI | COLABA | LOKHANDWALA</span><br>
                                                            </td>

                                                        </tr></tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                        </table>
                                        </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        </tbody>
                                        </table>
                                        <button type="button" id="card" value="C" class="btn btn-info submit_invocie" data-toggle="button" style="float:left;">Card</button>
                                        <button type="button" id="hold" value="H" class="btn btn-primary active submit_invocie" data-toggle="button" style="float:left;">Balance Payable</button>
                                        <?php /* <button type="button" id="both" value="both" class="btn btn-blue-alt submit_invocie" data-toggle="button" style="float:left;">Both</button> */ ?>
                                        <a href="appointment_invoice.php" class="btn btn-primary" style="float: left;">Go Back</a>
                                        <button type="button" id="cash" value="CS" class="btn btn-warning submit_invocie" data-toggle="button"><center>Cash</center></button>


                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <?php require_once 'incFooter.fya'; ?>
            </div>
        </div>

    </body>
</html>