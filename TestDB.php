<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php

$DB = Connect();
$limit = 100;
$offset = 0;
/* $invoice_data = select("AppointmentId,TotalPayment,ChargeAmount", "tblInvoiceDetails", "Id > 0 LIMIT " . $limit . " OFFSET " . $offset);

  if (isset($invoice_data) && is_array($invoice_data) && count($invoice_data) > 0) {
  foreach ($invoice_data as $key => $value) {
  $total_after_tax = $value['TotalPayment'];

  $total_tax = str_replace("+", "", $value['ChargeAmount']);
  $total_tax = round($total_tax);
  $total_before_tax = $total_after_tax - $total_tax;

  $updateq = "UPDATE tblAppointments SET total_before_tax=" . $total_before_tax . ",total_after_tax=" . $total_after_tax . ",total_tax=" . $total_tax .
  " WHERE AppointmentID='" . $value['AppointmentId'] . "'";
  ExecuteNQ($updateq);
  }
  } */

$invoice_data = select("AppointmentID,ServiceID,ServiceAmount,qty,AppointmentDetailsID", "tblAppointmentsDetailsInvoice", "service_amount_deduct_dis = 0 LIMIT " . $limit . " OFFSET " . $offset);
if (isset($invoice_data) && is_array($invoice_data) && count($invoice_data) > 0) {
    foreach ($invoice_data as $key => $value) {
        $member_dis = select("MembershipAmount", "tblAppointmentMembershipDiscount", "AppointmentID = '" . $value['AppointmentID'] . "' AND ServiceID = '" . $value['ServiceID'] . "'");
        $offer_dis = select("OfferAmount", "tblAppointmentMembershipDiscount", "AppointmentID = '" . $value['AppointmentID'] . "' AND ServiceID = '" . $value['ServiceID'] . "'");

        $member_amount = isset($member_dis[0]['MembershipAmount']) ? $member_dis[0]['MembershipAmount'] : 0;
        $offer_amount = isset($member_dis[0]['OfferAmount']) ? $member_dis[0]['OfferAmount'] : 0;
        $total_amount = $value['ServiceAmount'] * $value['qty'];
        $service_amount_deduct_dis = $total_amount - $member_amount - $offer_amount;
        if ($service_amount_deduct_dis < 0) {
            $service_amount_deduct_dis = 0;
        }
        $updateq = "UPDATE tblAppointmentsDetailsInvoice SET service_amount_deduct_dis='" . $service_amount_deduct_dis . "',"
                . "offer_discount='" . $offer_amount . "',member_discount='" . $member_amount . "'"
                . " WHERE AppointmentDetailsID='" . $value['AppointmentDetailsID'] . "'";
        ExecuteNQ($updateq);
    }
}
?>