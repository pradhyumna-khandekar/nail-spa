<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Customer Retention Report| Nailspa";
$strDisplayTitle = "Customer Retention Report of Nailspa Experience";
$strMenuID = "2";
$strMyTable = "tblStoreStock";
$strMyTableID = "StoreStockID";
$strMyField = "";
$strMyActionPage = "ReportCustomerRetention.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights	



if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $strStep = Filter($_POST["step"]);

    if ($strStep == "add") {
        
    }

    if ($strStep == "edit") {
        
    }
}
?>

<?php
if (isset($_GET["toandfrom"])) {
    $strtoandfrom = $_GET["toandfrom"];
    $arraytofrom = explode("-", $strtoandfrom);

    $from = $arraytofrom[0];
    $datetime = new DateTime($from);
    $getfrom = $datetime->format('Y-m-d');


    $to = $arraytofrom[1];
    $datetime = new DateTime($to);
    $getto = $datetime->format('Y-m-d');

    if (!IsNull($from)) {
        $sqlTempfrom = " and Date(tblAppointments.AppointmentDate)>=Date('" . $getfrom . "')";
        $sqlTempfrom1 = " and Date(tblCustomers.RegDate)>=Date('" . $getfrom . "')";
    }

    if (!IsNull($to)) {
        $sqlTempto = " and Date(tblAppointments.AppointmentDate)<=Date('" . $getto . "')";
        $sqlTempto1 = " and Date(tblCustomers.RegDate)<=Date('" . $getto . "')";
    }
}


if (isset($_GET["store"])) {
    $store = $_GET["store"];

    if (!IsNull($store)) {
        $sqlstore = " AND tblEmployees.StoreID='" . $store . "'";
    }
}
?>	


<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>

        <script type="text/javascript" src="assets/widgets/datepicker/datepicker.js"></script>
        <script type="text/javascript">
            /* Datepicker bootstrap */

            $(function () {
                "use strict";
                $('.bootstrap-datepicker').bsdatepicker({
                    format: 'mm-dd-yyyy'
                });

                $("#store").change(function ()
                {
                    var store = $(this).val();
                    $.ajax({
                        type: "post",
                        url: "changeemployee.php",
                        data: "store=" + store,
                        success: function (res)
                        {

                            $("#emp").html("");

                            $("#emp").html(res);
                        }



                    });
                });

            });

            function printDiv(divName)
            {

                var divToPrint = document.getElementById("printdata");
                var htmlToPrint = '' +
                        '<style type="text/css">' +
                        'table th, table td {' +
                        'border:1px solid #000;' +
                        'padding;0.5em;' +
                        '}' +
                        '</style>';
                htmlToPrint += divToPrint.outerHTML;
                newWin = window.open("");
                newWin.document.write(htmlToPrint);
                newWin.print();
                newWin.close();
                // var printContents = document.getElementById(divName);
                // var originalContents = document.body.innerHTML;

                // document.body.innerHTML = printContents;

                // window.print();

                // document.body.innerHTML = originalContents; 
            }
        </script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker-demo.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/moment.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");      ?>
            <!----------commented by gandhali 5/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>


                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                        </div>

                        <div class="panel">
                            <div class="panel">
                                <div class="panel-body">


                                    <div class="example-box-wrapper">
                                        <div class="tabs">

                                            <div id="normal-tabs-1">

                                                <span class="form_result">&nbsp; <br>
                                                </span>

                                                <div class="panel-body">

                                                    <form method="get" class="form-horizontal bordered-row" role="form">
                                                        <div class="form-group"><label for="" class="col-sm-4 control-label">Select date</label>
                                                            <div class="col-sm-4">
                                                                <div class="input-prepend input-group">
                                                                    <span class="add-on input-group-addon">
                                                                        <i class="glyph-icon icon-calendar"></i>
                                                                    </span> 
                                                                    <input type="text" name="toandfrom" id="daterangepicker-example" class="form-control" value="<?= $strtoandfrom ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group"><label for="" class="col-sm-4 control-label">Select Employee</label>
                                                            <div class="col-sm-4">
                                                                <select class="form-control required"  name="store">
                                                                    <?php
                                                                    if ($strStore == '0') {
                                                                        $strStatement = "";
                                                                    } else {
                                                                        $strStatement = " and StoreID='$strStore'";
                                                                    }

                                                                    $selp = select("*", "tblEmployees", "Status='0' $strStatement");
                                                                    foreach ($selp as $val) {
                                                                        $EIDD = $val["EID"];
                                                                        $EMPNAME = $val["EmployeeName"];
                                                                        $EID = $_GET["store"];
                                                                        if ($EID == $EIDD) {
                                                                            $selpT = select("*", "tblEmployees", "EID='" . $EID . "'");
                                                                            $EmployeeName = $selpT[0]['EmployeeName'];
                                                                            ?>
                                                                            <option  selected value="<?= $EID ?>" ><?= $EmployeeName ?></option>														
                                                                            <?php
                                                                        } else {
                                                                            if ($EIDD == "35" || $EIDD == "8" || $EIDD == "6" || $EIDD == "34" || $EIDD == "22" || $EIDD == "49" || $EIDD == "43") {
                                                                                // List of managers, HO and Audit whose details need not to be shown
                                                                            } else {
                                                                                ?>
                                                                                <option value="<?= $EIDD ?>"><?= $EMPNAME ?></option>
                                                                                <?php
                                                                            }
                                                                            ?>

                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Select Percentage</label>
                                                            <div class="col-sm-4">
                                                                <?php
                                                                $per = $_GET["per"];
                                                                ?>
                                                                <select name="per" class="form-control">
                                                                    <option value="0" <?php if ($per == '0') { ?> selected <?php } ?>>Without Percentage</option>
                                                                    <option value="1" <?php if ($per == '1') { ?> selected <?php } ?>>Percentage</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                </div>
                                                <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                    <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a class="btn btn-link" href="ReportCustomerRetentionAnalysis.php">Clear All Filter</a>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <?php
                                                    $datedrom = $_GET["toandfrom"];
                                                    if ($datedrom != "") {
                                                        ?>
                                                        <button onclick="printDiv('printarea')" class="btn btn-blue-alt">Print<div class="ripple-wrapper"></div></button>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>

                                                </form>

                                                <br>
                                                <div id="printdata">	
                                                    <?php
                                                    $datedrom = $_GET["toandfrom"];
                                                    if ($datedrom != "") {
                                                        $EID = $_GET["store"];
                                                        if ($EID == '0') {
                                                            $emp_id = 'All';
                                                        } else {
                                                            $selpT = select("*", "tblEmployees", "EID='" . $EID . "'");
                                                            $EmployeeName = $selpT[0]['EmployeeName'];
                                                            $emp_id = $EmployeeName;
                                                        }
                                                        ?>
                                                        <h3 class="title-hero">Date Range selected : FROM - <?= $getfrom ?> / TO - <?= $getto ?> / Employee Name - <?= $emp_id ?></h3>

                                                        <br>


                                                        <div class="example-box-wrapper">
                                                            <table class="table table-bordered table-striped table-condensed cf" width="100%">
                                                                <?php
                                                                if ($store != "0") {
                                                                    ?>
                                                                    <thead>
                                                                        <tr>
                                                                            <th style="text-align:center">Sr</th>
                                                                            <th style="text-align:center">Employee Name</th>
                                                                            <th style="text-align:center">New Walkin Count</th>
                                                                            <th style="text-align:center">Recurred customers count</th>
                                                                            <th style="text-align:center">Existing customers count</th>
                                                                            <th style="text-align:center">Reacquired customers count</th>
                                                                            <?php
                                                                            if ($per != '0') {
                                                                                ?>
                                                                                <th style="text-align:center">Total Count</th>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                            <?php
                                                                            if ($per != '0') {
                                                                                ?>
                                                                                <th style="text-align:center">New Walkin %</th>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                            <?php
                                                                            if ($per != '0') {
                                                                                ?>
                                                                                <th style="text-align:center">Recurred customers %</th>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                            <?php
                                                                            if ($per != '0') {
                                                                                ?>
                                                                                <th style="text-align:center">Existing customers %</th>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                            <?php
                                                                            if ($per != '0') {
                                                                                ?>
                                                                                <th style="text-align:center">Reacquired customers %</th>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </tr>
                                                                    </thead>

                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <thead>
                                                                        <tr>
                                                                            <th style="text-align:center">Sr</th>
                                                                            <th style="text-align:center">Employee Name</th>
                                                                            <th style="text-align:center">New Walkin Count</th>
                                                                            <th style="text-align:center">Recurred customers count</th>
                                                                            <th style="text-align:center">Existing customers count</th>
                                                                            <th style="text-align:center">Reacquired customers</th>
                                                                            <?php
                                                                            if ($per != '0') {
                                                                                ?>
                                                                                <th style="text-align:center">Total Count</th>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                            <?php
                                                                            if ($per != '0') {
                                                                                ?>
                                                                                <th style="text-align:center">New Walkin %</th>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                            <?php
                                                                            if ($per != '0') {
                                                                                ?>
                                                                                <th style="text-align:center">Recurred customers %</th>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                            <?php
                                                                            if ($per != '0') {
                                                                                ?>
                                                                                <th style="text-align:center">Existing customers %</th>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                            <?php
                                                                            if ($per != '0') {
                                                                                ?>
                                                                                <th style="text-align:center">Reacquired customers %</th>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                            <th style="text-align:center">Store</th>

                                                                        </tr>
                                                                    </thead>

                                                                    <?php
                                                                }
                                                                ?>

                                                                <tbody>

                                                                    <?php
                                                                    $DB = Connect();
                                                                    $per = $_GET["per"];
                                                                    $EmployeeID = $_GET["store"];
                                                                    if (!empty($EmployeeID)) {
                                                                        $sql = "select * from tblEmployees where Status='0' and EID='" . $EmployeeID . "'";
                                                                    } else {
                                                                        $sql = "select * from tblEmployees where Status='0'";
                                                                    }


                                                                    $RS = $DB->query($sql);
                                                                    if ($RS->num_rows > 0) {
                                                                        while ($row = $RS->fetch_assoc()) {
                                                                            $all_emp_ids[$row["EID"]] = $row["EID"];
                                                                            $search_emp_data[$row["EID"]] = $row;
                                                                        }
                                                                    }

                                                                    /*
                                                                     * get employee sales data
                                                                     */
                                                                    if (isset($all_emp_ids) && is_array($all_emp_ids) && count($all_emp_ids) > 0) {
                                                                        $in_emp_ids = implode(",", $all_emp_ids);
                                                                        if ($in_emp_ids != '') {
                                                                            $store_append = '';
                                                                            if (isset($_GET['store']) && !empty($_GET['store'])) {
                                                                                $store_append = " AND tblAppointments.StoreID='" . $_GET['store'] . "'";
                                                                            }

                                                                            $appointment_q = "SELECT *,AppointmentID as AppointmentId FROM tblAppointments "
                                                                                    . " WHERE AppointmentCheckInTime!='00:00:00' AND status=2 "
                                                                                    . " AND AppointmentDate!='' $sqlTempfrom1 $sqlTempto1 " . $store_append;
                                                                            $appointment_q_exe = $DB->query($appointment_q);
                                                                            if ($appointment_q_exe->num_rows > 0) {
                                                                                while ($appointment_result = $appointment_q_exe->fetch_assoc()) {
                                                                                    $invoice_apt_ids[] = $appointment_result['AppointmentId'];
                                                                                    $invice_detail_data[$appointment_result['AppointmentId']] = $appointment_result;
                                                                                }


                                                                                if (isset($invoice_apt_ids) && is_array($invoice_apt_ids) && count($invoice_apt_ids) > 0) {
                                                                                    $inv_apt_ids = implode(",", $invoice_apt_ids);
                                                                                    if ($inv_apt_ids != '') {
                                                                                        $apt_emp_ass_q = "SELECT tblAppointmentAssignEmployee.MECID,tblAppointmentAssignEmployee.QtyParam,tblAppointmentAssignEmployee.AppointmentID, tblAppointmentAssignEmployee.Qty, tblAppointmentAssignEmployee.ServiceID, tblAppointmentAssignEmployee.Commission, tblAppointmentAssignEmployee.QtyParam"
                                                                                                . " FROM tblAppointmentAssignEmployee "
                                                                                                . " WHERE tblAppointmentAssignEmployee.MECID IN(" . $in_emp_ids . ") AND tblAppointmentAssignEmployee.AppointmentID IN (" . $inv_apt_ids . ")";
                                                                                        //. "  group by tblAppointmentAssignEmployee.AppointmentID,ServiceID,QtyParam";
                                                                                        $apt_emp_ass_q_exe = $DB->query($apt_emp_ass_q);
                                                                                        if ($apt_emp_ass_q_exe->num_rows > 0) {
                                                                                            while ($emp_assign_result = $apt_emp_ass_q_exe->fetch_assoc()) {
                                                                                                $invoice_apt_data[] = $emp_assign_result;
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }


                                                                                if (isset($invoice_apt_data) && is_array($invoice_apt_data) && count($invoice_apt_data) > 0) {
                                                                                    foreach ($invoice_apt_data as $reskey => $resvalue) {
                                                                                        $all_sales_data[$resvalue['MECID']][] = array(
                                                                                            'EID' => $resvalue['MECID'],
                                                                                            'CustomerID' => isset($invice_detail_data[$resvalue['AppointmentID']]) ? $invice_detail_data[$resvalue['AppointmentID']]['CustomerID'] : 0,
                                                                                            'AppointmentID' => $resvalue['AppointmentID'],
                                                                                            'Qty' => $resvalue['Qty'],
                                                                                            'ServiceID' => $resvalue['ServiceID'],
                                                                                            'Commission' => $resvalue['Commission'],
                                                                                            'QtyParam' => $resvalue['QtyParam'],
                                                                                            'OfferDiscountDateTime' => isset($invice_detail_data[$resvalue['AppointmentID']]) ? $invice_detail_data[$resvalue['AppointmentID']]['OfferDiscountDateTime'] : 0,
                                                                                            'AppointmentDate' => isset($invice_detail_data[$resvalue['AppointmentID']]) ? $invice_detail_data[$resvalue['AppointmentID']]['AppointmentDate'] : 0,
                                                                                            'StoreID' => isset($all_emp_data[$resvalue['MECID']]) ? $all_emp_data[$resvalue['MECID']]['StoreID'] : 0,
                                                                                        );

                                                                                        $cust_ids = $invice_detail_data[$resvalue['AppointmentID']]['CustomerID'];
                                                                                        $customer_ids[$cust_ids] = $cust_ids;
                                                                                        $appointment_ids[$resvalue['AppointmentID']] = $resvalue['AppointmentID'];
                                                                                        $service_ids[$resvalue['MECID']][] = $resvalue['ServiceID'];
                                                                                        $store_id = isset($all_emp_data[$resvalue['MECID']]) ? $all_emp_data[$resvalue['StoreID']] : 0;
                                                                                        $store_ids[$store_id] = $store_id;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }


                                                                    /*
                                                                     * Get Customer Details
                                                                     */
                                                                    if (isset($customer_ids) && is_array($customer_ids) && count($customer_ids) > 0) {
                                                                        $cust_in_ids = implode(",", $customer_ids);
                                                                        $cust_q = "SELECT * FROM tblCustomers WHERE CustomerID IN(" . $cust_in_ids . ")";
                                                                        $cust_q_exe = $DB->query($cust_q);
                                                                        while ($custdetails = $cust_q_exe->fetch_assoc()) {
                                                                            $all_customer_data[$custdetails['CustomerID']] = $custdetails;
                                                                        }
                                                                    }


                                                                    /*
                                                                     * Get Customer Appointment Count
                                                                     */
                                                                    if (isset($customer_ids) && is_array($customer_ids) && count($customer_ids) > 0) {
                                                                        $cust_in_ids = implode(",", $customer_ids);
                                                                        $apt_countQ = "SELECT COUNT(AppointmentId) as apt_count,CustomerID FROM tblAppointments "
                                                                                . " WHERE AppointmentCheckInTime!='00:00:00' AND status=2 AND CustomerID IN(" . $cust_in_ids . ")"
                                                                                . " GROUP BY CustomerID";
                                                                        $apt_count_exe = $DB->query($apt_countQ);
                                                                        while ($count_data = $apt_count_exe->fetch_assoc()) {
                                                                            $customer_apt_count[$count_data['CustomerID']] = $count_data['apt_count'];
                                                                            if ($count_data['apt_count'] > 1) {
                                                                                $old_customer_id_data[$count_data['CustomerID']] = $count_data['CustomerID'];
                                                                            }
                                                                        }
                                                                    }


                                                                    /*
                                                                     * Get Customer Appointment Count
                                                                     */
                                                                    if (isset($customer_ids) && is_array($customer_ids) && count($customer_ids) > 0) {
                                                                        $cust_in_ids = implode(",", $customer_ids);
                                                                        $apt_countQ = "SELECT COUNT(AppointmentId) as apt_count,CustomerID FROM tblAppointments "
                                                                                . " WHERE AppointmentCheckInTime!='00:00:00' AND status=2 AND CustomerID IN(" . $cust_in_ids . ")"
                                                                                . " GROUP BY CustomerID";
                                                                        $apt_count_exe = $DB->query($apt_countQ);
                                                                        while ($count_data = $apt_count_exe->fetch_assoc()) {
                                                                            $customer_apt_count[$count_data['CustomerID']] = $count_data['apt_count'];
                                                                            if ($count_data['apt_count'] > 1) {
                                                                                $old_customer_id_data[$count_data['CustomerID']] = $count_data['CustomerID'];
                                                                            }
                                                                        }
                                                                    }

                                                                    /*
                                                                     * Get Old Customer Last Appointment Date 
                                                                     */
                                                                    if (isset($old_customer_id_data) && is_array($old_customer_id_data) && count($old_customer_id_data) > 0) {
                                                                        $old_customer_id = implode(",", $old_customer_id_data);
                                                                        $appendq = '';
                                                                        if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                                                                            $apt_in_ids = implode(",", $appointment_ids);
                                                                            if ($apt_in_ids != '') {
                                                                                $appendq .= " AND AppointmentID NOT IN(" . $apt_in_ids . ")";
                                                                            }
                                                                        }
                                                                        $apt_dateQ = "SELECT AppointmentID,AppointmentDate,CustomerID FROM tblAppointments "
                                                                                . " WHERE AppointmentID IN (SELECT AppointmentID FROM tblAppointments "
                                                                                . " WHERE Status =2 AND  AppointmentCheckInTime!='00:00:00' AND CustomerID IN(" . $old_customer_id . ")"
                                                                                . $appendq . " GROUP by CustomerID "
                                                                                . " ORDER by AppointmentDate DESC)";
                                                                        $apt_date_exe = $DB->query($apt_dateQ);
                                                                        while ($apt_date_data = $apt_date_exe->fetch_assoc()) {
                                                                            $customer_appoint_date[$apt_date_data['CustomerID']]['apt_id'] = $apt_date_data['AppointmentID'];
                                                                            $customer_appoint_date[$apt_date_data['CustomerID']]['apt_date'] = $apt_date_data['AppointmentDate'];
                                                                            $last_apt_arr[$apt_date_data['AppointmentID']] = $apt_date_data['AppointmentID'];
                                                                        }

                                                                        /*
                                                                         * get last appointment employee
                                                                         */
                                                                        if (isset($last_apt_arr) && is_array($last_apt_arr) && count($last_apt_arr) > 0) {
                                                                            $last_apt_in_ids = implode(",", $last_apt_arr);
                                                                            if ($last_apt_in_ids != '') {
                                                                                $last_empQ = "SELECT * FROM tblAppointmentAssignEmployee "
                                                                                        . " WHERE AppointmentID IN(" . $last_apt_in_ids . ")";
                                                                                $last_emp_exe = $DB->query($last_empQ);
                                                                                while ($last_apt_data = $last_emp_exe->fetch_assoc()) {
                                                                                    $last_emp_res[$last_apt_data['AppointmentID']][$last_apt_data['MECID']] = $last_apt_data['MECID'];
                                                                                }
                                                                            }
                                                                        }
                                                                    }


                                                                    if ($RS->num_rows > 0) {
                                                                        $counter = 0;

                                                                        while ($row = $RS->fetch_assoc()) {

                                                                            //$StoreID = $row["StoreID"];
                                                                            $EID = $row["EID"];
                                                                            $store = $row["StoreID"];
                                                                            if ($EID == "35" || $EID == "8" || $EID == "6" || $EID == "34" || $EID == "22" || $EID == "29" || $EID == "43" || $EID == "49") {
                                                                                // List of managers, HO and Audit whose details need not to be shown
                                                                            } else {
                                                                                $EID = $row["EID"];
                                                                                $counter ++;

                                                                                $DateTime = FormatDateTime($getfrom);
                                                                                $sep = select("StoreName", "tblStores", "StoreID='" . $store . "'");
                                                                                $storename = $sep[0]['StoreName'];
                                                                                $EmployeeName = $row["EmployeeName"];
                                                                                $strEID = $EID;
                                                                                if (isset($all_sales_data[$strEID]) && is_array($all_sales_data[$strEID]) && count($all_sales_data[$strEID]) > 0) {
                                                                                    foreach ($all_sales_data[$strEID] as $key => $rowdetails) {
                                                                                        $unique_apt[$rowdetails['AppointmentID']]['CustomerID'] = $rowdetails['CustomerID'];
                                                                                        $unique_apt[$rowdetails['AppointmentID']]['AppointmentDate'] = $rowdetails['AppointmentDate'];
                                                                                    }
                                                                                }

                                                                                $new_customer_count = 0;
                                                                                $old_customer_count = 0;
                                                                                $old_customer_non_count = 0;

                                                                                $new_cust_ids = array();
                                                                                $old_cust_repeat_ids = array();
                                                                                $old_cust_non_repeat_ids = array();

                                                                                if (isset($unique_apt) && is_array($unique_apt) && count($unique_apt) > 0) {
                                                                                    foreach ($unique_apt as $key => $value) {
                                                                                        $customer_reg_date = isset($all_customer_data[$value['CustomerID']]) ? date('Y-m-d', strtotime($all_customer_data[$value['CustomerID']]['RegDate'])) : '';
                                                                                        /*
                                                                                         * Check Customer Appointment Count
                                                                                         */
                                                                                        $apt_count = isset($customer_apt_count[$value['CustomerID']]) ? $customer_apt_count[$value['CustomerID']] : 1;

                                                                                        if ($apt_count == 1) {
                                                                                            $new_customer_count += 1;
                                                                                            $new_cust_ids[$value['CustomerID']] = $value['CustomerID'];
                                                                                        } else {
                                                                                            /*
                                                                                             * Check last Appointment Date
                                                                                             */
                                                                                            $last_appointment_date = isset($customer_appoint_date[$value['CustomerID']]) ? $customer_appoint_date[$value['CustomerID']]['apt_date'] : '';
                                                                                            $last_appointment_id = isset($customer_appoint_date[$value['CustomerID']]) ? $customer_appoint_date[$value['CustomerID']]['apt_id'] : '';
                                                                                            if ($last_appointment_date == '') {
                                                                                                $old_customer_non_count +=1;
                                                                                                $old_cust_non_repeat_ids[$value['CustomerID']] = $value['CustomerID'];
                                                                                            } else {
                                                                                                /*
                                                                                                 * Check last and current appointment date gap
                                                                                                 */
                                                                                                $d1 = new DateTime($last_appointment_date);
                                                                                                $d2 = new DateTime($value['AppointmentDate']);
                                                                                                $interval = $d1->diff($d2);
                                                                                                $day_gap = $interval->days;

                                                                                                if (isset($last_emp_res[$last_appointment_id]) && in_array($strEID, $last_emp_res[$last_appointment_id]) && $day_gap <= 60) {
                                                                                                    $old_customer_count +=1;
                                                                                                    $old_cust_repeat_ids[$value['CustomerID']] = $value['CustomerID'];
                                                                                                } else {
                                                                                                    $old_customer_non_count +=1;
                                                                                                    $old_cust_non_repeat_ids[$value['CustomerID']] = $value['CustomerID'];
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                                ?>
                                                                                <tr id="my_data_tr_<?= $counter ?>">
                                                                                    <td><center><?= $counter ?></center></td>

                                                                            <td><center><?= $EmployeeName ?></center></td>
                                                                            <td><center><?php
                                                                                $new_count = isset($new_cust_ids) ? count($new_cust_ids) : 0;
                                                                                echo $new_count;
                                                                                ?></center></td>
                                                                            <td><center><?php
                                                                                $cntsncust = 0;
                                                                                foreach ($stu1 as $vqt) {
                                                                                    $newc = $vqt['newcust'];
                                                                                    // $stu2 = newcustomerrepeatcount($getfrom, $getto, $EID, $newc);
                                                                                    $newcoldqt = $stu2[0]['newcustcnt'];

                                                                                    if ($newcoldqt != '0') {

                                                                                        if ($newcoldqt > 3) {
                                                                                            $cntsncust++;
                                                                                            $cntsncust;
                                                                                        }
                                                                                    }
                                                                                }
                                                                                echo $cntsncust;
                                                                                ?></center></td>
                                                                            <td><center><?php
                                                                                $cntsncustold = 0;
                                                                                foreach ($sqldetailsy as $vqtp) {
                                                                                    $oldcustcntc = $vqtp['oldcustcnt'];
                                                                                    // $stu2q = oldcustomer($getfrom, $getto, $EID, $oldcustcntc);
                                                                                    $newcold = $stu2q[0]['oldcustcntq'];

                                                                                    if ($newcold != '0') {
                                                                                        if ($newcold > 4) {
                                                                                            $cntsncustold++;
                                                                                        }
                                                                                    }
                                                                                }
                                                                                echo $cntsncustold;
                                                                                ?></center></td>
                                                                            <td><center><?php
                                                                                foreach ($sqldetailsy as $vqtp) {
                                                                                    $oldcustcntc = $vqtp['oldcustcnt'];
                                                                                    // $stu2qmst = oldcustomerrepeatcnt($getfrom, $getto, $EID, $oldcustcntc);

                                                                                    $maxoldcustcntq = $stu2qmst[0]['maxoldcustcntq'];
                                                                                    $sqldetailsdTy = select("tblAppointments.AppointmentDate", "tblAppointments", "tblAppointments.AppointmentID='" . $maxoldcustcntq . "'");
                                                                                    $app_date = $sqldetailsdTy[0]['AppointmentDate'];
                                                                                    $new_date = strtotime(date("Y-m-d", strtotime($app_date)) . " +12 month");
                                                                                    //$stu2qmstold = oldcustomerrepeatcnt($app_date, $new_date, $EID, $oldcustcntc);
                                                                                    $newcoldrepeat = $stu2qmstold[0]['oldcustcntq'];
                                                                                }

                                                                                if ($newcoldrepeat == "") {
                                                                                    $newcoldrepeat = 0;
                                                                                }
                                                                                echo $newcoldrepeat;
                                                                                ?></center></td>
                                                                            <?php
                                                                            $totalcustcount = $custcnt + $cntsncust + $cntsncustold + $newcoldrepeat;

                                                                            if ($per != '0') {
                                                                                ?>
                                                                                <td><center><?php echo $totalcustcount; ?></center></td>
                                                                                <?php
                                                                            }
                                                                            $totalnew = ($custcnt / $totalcustcount) * 100;
                                                                            if ($per != '0') {
                                                                                ?>
                                                                                <td><center><?php echo round($totalnew, 2); ?></center></td>
                                                                                <?php
                                                                            }
                                                                            $Recurred = ($cntsncust / $totalcustcount) * 100;
                                                                            if ($per != '0') {
                                                                                ?>
                                                                                <td><center><?php echo round($Recurred, 2); ?></center></td>
                                                                                <?php
                                                                            }
                                                                            $Existing = ($cntsncustold / $totalcustcount) * 100;
                                                                            if ($per != '0') {
                                                                                ?>
                                                                                <td><center><?php echo round($Existing, 2); ?></center></td>
                                                                                <?php
                                                                            }
                                                                            $Reacquired = ($newcoldrepeat / $totalcustcount) * 100;
                                                                            if ($per != '0') {
                                                                                ?>
                                                                                <td><center><?php echo round($Reacquiredm, 2); ?></center></td>
                                                                                <?php
                                                                            }
                                                                            ?>

                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                        $newrepeatcust = "";
                                                                        $custcnt = "";
                                                                        $newrepeatcust = "";
                                                                        $oldcustcnt = "";
                                                                        $olrepeatcust = "";
                                                                        $oldcustrepeatcustt = "";
                                                                    }
                                                                } else {
                                                                    ?>															
                                                                    <tr>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td>No Records Found</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>

                                                                    </tr>


                                                                    <?php
                                                                }


                                                                $DB->close();
                                                                ?>

                                                                </tbody>


                                                            </table>
                                                            <?php
                                                        } else {
                                                            echo "<br><center><h3>Please Select Month And Year!</h3></center>";
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <?php require_once 'incFooter.fya'; ?>

        </div>
    </body>

</html>