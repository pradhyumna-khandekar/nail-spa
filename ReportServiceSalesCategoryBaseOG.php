<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php
$strPageTitle = "ReportServiceSalesCategoryBase | Nailspa";
$strDisplayTitle = "Report Service Sales Category Base for Nailspa";
$strMenuID = "2";
$strMyTable = "tblStoreStock";
$strMyTableID = "StoreStockID";
$strMyField = "";
$strMyActionPage = "ReportServiceSalesCategoryBase.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights	



if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $strStep = Filter($_POST["step"]);

    if ($strStep == "add") {
        
    }

    if ($strStep == "edit") {
        
    }
}
?>


<?php
if (isset($_GET["toandfrom"])) {
    $strtoandfrom = $_GET["toandfrom"];
    $arraytofrom = explode("-", $strtoandfrom);

    $from = $arraytofrom[0];
    $datetime = new DateTime($from);
    $getfrom = $datetime->format('Y-m-d');


    $to = $arraytofrom[1];
    $datetime = new DateTime($to);
    $getto = $datetime->format('Y-m-d');

    if (!IsNull($from)) {
        $sqlTempfrom = " and Date(tblAppointments.AppointmentDate)>=Date('" . $getfrom . "')";
    }

    if (!IsNull($to)) {
        $sqlTempto = " and Date(tblAppointments.AppointmentDate)<=Date('" . $getto . "')";
    }
}

if (!IsNull($_GET["Store"])) {
    $strStoreID = $_GET["Store"];

    $sqlTempStore = " StoreID='$strStoreID'";
}
?>	


<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>

        <script type="text/javascript" src="assets/widgets/datepicker/datepicker.js"></script>
        <script type="text/javascript">
            /* Datepicker bootstrap */

            $(function () {
                $("#catt").hide();
                $("#applyfilter").hide();
                "use strict";
                $('.bootstrap-datepicker').bsdatepicker({
                    format: 'mm-dd-yyyy'
                });

            });


            function GetDetailReport(report_url) {
                StartLoading();
                $.ajax({
                    type: "POST",
                    url: report_url,
                    success: function (data) {
                        EndLoading();
                        $('#serviceReportModal .modal-dialog').html();//clear previous html data
                        $('#serviceReportModal').show();//
                        $('#serviceReportModal .modal-dialog').html(data);
                        $('#serviceReportModal').modal();
                    }
                });
            }

            function changecategory(evt)
            {
                $("#catt").show();
                var store = $(evt).val();
                var date = $("#daterangepicker-example").val();
                //alert(date)
                if (store != '0' || store != '')
                {
                    $.ajax({
                        type: 'post',
                        data: 'store=' + store + "&date=" + date,
                        url: 'UpdateCategory.php',
                        success: function (res)
                        {
                            $("#applyfilter").show();
                            $("#cat").html(res);
                        }

                    })
                }
            }
        </script>
        <script>
            function printDiv(divName)
            {

                var divToPrint = document.getElementById("kakkabiryani");
                var htmlToPrint = '' +
                        '<style type="text/css">' +
                        'table th, table td {' +
                        'border:1px solid #000;' +
                        'padding;0.5em;' +
                        '}' +
                        '</style>';
                htmlToPrint += divToPrint.outerHTML;
                newWin = window.open("");
                newWin.document.write(htmlToPrint);
                newWin.print();
                newWin.close();
                // var printContents = document.getElementById(divName);
                // var originalContents = document.body.innerHTML;

                // document.body.innerHTML = printContents;

                // window.print();

                // document.body.innerHTML = originalContents; 
            }

        </script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker-demo.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/moment.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
        <?php /* <script type="text/javascript" src="assets/widgets/modal/modal.js"></script> */ ?>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");      ?>
            <!----------commented by gandhali 5/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>


                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                        </div>
                        <style type="text/css">
                            @media print {
                                body * {
                                    visibility: hidden;

                                }
                                #printarea * {
                                    visibility: visible;
                                }
                                #printarea{
                                    position: absolute;
                                    left: 0;
                                    top: 0;
                                }
                            }
                        </style>
                        <?php
                        if (!isset($_GET["uid"])) {
                            ?>					

                            <div class="panel">
                                <div class="panel">

                                    <div class="panel-body">


                                        <div id="serviceReportModal" class="modal fade" role="dialog">
                                            <div class="modal-dialog" style="width:52%">
                                            </div>
                                        </div>

                                        <div class="example-box-wrapper">
                                            <div class="tabs">

                                                <div id="normal-tabs-1">

                                                    <span class="form_result">&nbsp; <br>
                                                    </span>

                                                    <div class="panel-body">
                                                        <h3 class="title-hero">List of all Services</h3>

                                                        <form method="get" class="form-horizontal bordered-row" role="form">

                                                            <div class="form-group"><label for="" class="col-sm-4 control-label">Select date</label>
                                                                <div class="col-sm-4">
                                                                    <div class="input-prepend input-group">
                                                                        <span class="add-on input-group-addon">
                                                                            <i class="glyph-icon icon-calendar"></i>
                                                                        </span> 
                                                                        <input type="text" name="toandfrom" id="daterangepicker-example" class="form-control" value="<?= $strtoandfrom ?>">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label">Select Store</label>
                                                                <div class="col-sm-4">
                                                                    <select name="Store" class="form-control">
                                                                        <option value="0">All</option>
                                                                        <?php
                                                                        $selp = select("*", "tblStores", "StoreID > '0'");
                                                                        foreach ($selp as $val) {
                                                                            $strStoreName = $val["StoreName"];
                                                                            $strStoreID = $val["StoreID"];
                                                                            $store = $_GET["Store"];
                                                                            if ($store == $strStoreID) {
                                                                                ?>
                                                                                <option  selected value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                                <?php
                                                                            } else {
                                                                                ?>
                                                                                <option value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label">Select Appointment Source</label>
                                                                <div class="col-sm-4">
                                                                    <select name="source" class="form-control">
                                                                        <option value="0">All</option>
                                                                        <?php
                                                                        $sel_source = select("*", "tblsource", "status = '1'");
                                                                        foreach ($sel_source as $val) {
                                                                            ?>
                                                                            <option value="<?= $val['id']; ?>" <?php echo isset($_GET['source']) && $_GET['source'] == $val['id'] ? 'selected' : ''; ?>><?= $val['name'] ?></option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label">Select Percentage</label>
                                                                <div class="col-sm-4">
                                                                    <?php
                                                                    $per = $_GET["per"];
                                                                    ?>
                                                                    <select name="per" class="form-control">
                                                                        <option value="0" <?php if ($per == '0') { ?> selected <?php } ?>>Without Percentage</option>
                                                                        <option value="1" <?php if ($per == '1') { ?> selected <?php } ?>>Percentage</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                                <button type="submit"  class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                                                &nbsp;&nbsp;&nbsp;
                                                                <a class="btn btn-link" href="ReportServiceSalesCategoryBaseOG.php">Clear All Filter</a>
                                                                &nbsp;&nbsp;&nbsp;
                                                                <?php
                                                                $datedrom = $_GET["toandfrom"];
                                                                if ($datedrom != "") {
                                                                    ?>
                                                                    <button onclick="printDiv('printarea')" class="btn btn-blue-alt">Print<div class="ripple-wrapper"></div></button>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </form>
                                                        <br>
                                                        <?php
                                                        $datedrom = $_GET["toandfrom"];
//                                                                echo '<pre>';
//                                                                print_r($datedrom);
//                                                                exit;
                                                        if ($datedrom != "" && !IsNull($_GET["Store"])) {

                                                            $strtoandfrom = $_GET["toandfrom"];
                                                            $arraytofrom = explode("-", $strtoandfrom);

                                                            $from = $arraytofrom[0];
                                                            $datetime = new DateTime($from);
                                                            $getfrom = $datetime->format('Y-m-d');


                                                            $to = $arraytofrom[1];
                                                            $datetime = new DateTime($to);
                                                            $getto = $datetime->format('Y-m-d');
                                                            ?>

                                                            <?php
                                                            // }
                                                            ?>
                                                            <?php
                                                            $data = select("*", "tblCategories", " CategoryType = 1");

                                                            if (isset($data) && is_array($data) && count($data)) {
                                                                foreach ($data as $key => $value) {
                                                                    $tblCategories[$value['CategoryID']] = $value['CategoryName'];
                                                                }
                                                            }
                                                            $tblCategories[0] = 'Other';

                                                            $data = select("*", "tblStores", " StoreID > 0");

                                                            if (isset($data) && is_array($data) && count($data)) {
                                                                foreach ($data as $key => $value) {
                                                                    $tblstores[$value['StoreID']] = $value['StoreName'];
                                                                }
                                                            }

//                                                        echo '<pre>';
//                                                        print_r($tblstores);
//                                                        exit;

                                                            $servername = ServerName();
                                                            $username = Username();
                                                            $password = Password();
                                                            $dbname = DBName();

                                                            $conn = new mysqli($servername, $username, $password, $dbname);
                                                            if (mysqli_connect_error()) {
                                                                die('Connect Error (' . mysqli_connect_errno() . ') '
                                                                        . mysqli_connect_error());
                                                            }

                                                            $storr = $_GET["Store"];
                                                            $append_qry = '';
                                                            if (!empty($storr)) {
                                                                $append_qry = " AND apt.StoreID='" . $storr . "'";
                                                                $store_data = select("*", "tblStores", " StoreID = " . $storr);
                                                            } else {
                                                                $store_res = select("*", "tblStores", " Status = 0");
                                                                $store_res['all']['StoreID'] = 0;
                                                                $store_res['all']['StoreName'] = 'All';
                                                                $store_data = array_reverse($store_res);
                                                            }


                                                            $get_source = $_GET["source"];
                                                            if (!empty($get_source)) {
                                                                $append_qry .= " AND apt.source='" . $get_source . "'";
                                                            }




                                                            $query = "SELECT DISTINCT apt.AppointmentId,apt.StoreID ,inv.ServiceName,inv.Qty,inv.ServiceAmt,inv.DisAmt,inv.OfferAmt,inv.SubTotal,inv.RoundTotal FROM `tblAppointments` apt LEFT OUTER JOIN `tblInvoiceDetails` inv on apt.`AppointmentId` = inv.`AppointmentId` 
                                                          WHERE (`AppointmentDate` BETWEEN '" . $getfrom . "' AND '" . $getto . "')
                                                             AND  apt.StoreID != '0'  AND apt.IsDeleted != '1'  AND apt.FreeService !=  '1'  AND apt.Status = '2'
                                                          AND inv.ServiceName != '' " . $append_qry . "ORDER BY AppointmentDate DESC";
                                                            //$query = "SELECT SUM(inv.`ServiceAmt`) as amt ,inv.`ServiceName`,inv.`Qty` FROM `tblappointments` apt LEFT OUTER JOIN `tblinvoicedetails` inv on apt.`AppointmentId` = inv.`AppointmentId` WHERE (`AppointmentDate` BETWEEN '2018-06-15' AND '2018-06-15') AND inv.`Qty` > 0 GROUP BY `ServiceName`";

                                                            $ans = $conn->query($query) or die($conn->error);
                                                            $allservice = array();
                                                            $allqty = array();
                                                            $allserviceamt = array();
                                                            $query_result = array();
                                                            if ($ans->num_rows > 0) {
                                                                while ($result = $ans->fetch_assoc()) {
                                                                    $query_result[] = $result;
                                                                }

                                                                if (isset($query_result) && is_array($query_result) && count($query_result)) {
                                                                    foreach ($query_result as $key => $value) {
                                                                        $appoint_id_arr[$value['AppointmentId']] = $value['AppointmentId'];
                                                                    }
                                                                }
                                                                if (isset($appoint_id_arr) && is_array($appoint_id_arr) && count($appoint_id_arr) > 0) {
                                                                    $appoi_in_ids = implode(",", $appoint_id_arr);
                                                                    $sqldata = " SELECT AppointmentID,ServiceID,service_amount_deduct_dis FROM tblAppointmentsDetailsInvoice WHERE ServiceID != 'NULL' AND ServiceID != '' AND AppointmentID IN (" . $appoi_in_ids . ")";
                                                                    $RSdata = $DB->query($sqldata);
                                                                    if ($RSdata->num_rows > 0) {
                                                                        while ($rowdata = $RSdata->fetch_assoc()) {
                                                                            $service_deduct_disc[$rowdata["AppointmentID"]][$rowdata["ServiceID"]] = $rowdata["service_amount_deduct_dis"];
                                                                        }
                                                                    }
                                                                }
                                                                
                                                                
                                                                if (isset($query_result) && is_array($query_result) && count($query_result)) {
                                                                    foreach ($query_result as $key => $value) {
                                                                        if (!empty($value['ServiceName'])) {
                                                                            $ServiceName = explode(',', $value['ServiceName']);
                                                                            $Qty = explode(',', $value['Qty']);
                                                                            $ServiceAmt = explode(',', $value['ServiceAmt']);
                                                                            $DisAmt = explode(',', $value['DisAmt']);
                                                                            $count = count($ServiceName);
                                                                            for ($i = 0; $i < $count; $i++) {
                                                                                $allservice[] = $ServiceName[$i];
                                                                                $apt_service_store[$ServiceName[$i]] = $value['StoreID'];
                                                                                //$allqty[] = $Qty[$i];
                                                                                //$allserviceamt[] = $ServiceAmt[$i];
                                                                                if (isset($allqty[$value['StoreID']][$ServiceName[$i]])) {
                                                                                    $allqty[$value['StoreID']][$ServiceName[$i]] += $Qty[$i];
                                                                                } else {
                                                                                    $allqty[$value['StoreID']][$ServiceName[$i]] = $Qty[$i];
                                                                                }

                                                                                $offer_deduct = 0;
                                                                                $offer_amount = str_replace(array('-', ' '), '', $value['OfferAmt']);
                                                                                //echo '<br>ServiceAmt[$i]=' . $ServiceAmt[$i];
                                                                                if ($offer_amount > 0) {
                                                                                    $offer_deduct = $offer_amount / $count;
                                                                                }
                                                                                //echo '<br>offre_dic=' . $offer_deduct;
                                                                                if (isset($DisAmt[$i])) {
                                                                                    $amount_exculde_dis = $ServiceAmt[$i] - $DisAmt[$i];
                                                                                } else {
                                                                                    $amount_exculde_dis = $ServiceAmt[$i];
                                                                                }

                                                                                if ($amount_exculde_dis < $offer_deduct) {
                                                                                    $amount_exculde_dis = 0;
                                                                                } else {
                                                                                    $amount_exculde_dis = $amount_exculde_dis - $offer_deduct;
                                                                                }

                                                                                //echo '<br>amount_exculde_dis=' . $amount_exculde_dis;
                                                                                $amount_exculde_dis = isset($service_deduct_disc[$value['AppointmentId']][$ServiceName[$i]]) ? $service_deduct_disc[$value['AppointmentId']][$ServiceName[$i]] : 0;
                                                                                if (isset($allqty[$value['StoreID']][$ServiceName[$i]])) {
                                                                                    $allserviceamt[$value['StoreID']][$ServiceName[$i]] += round($amount_exculde_dis, 2);
                                                                                } else {
                                                                                    $allserviceamt[$value['StoreID']][$ServiceName[$i]] = round($amount_exculde_dis, 2);
                                                                                }

                                                                                $allstores[] = $value['StoreID'];
                                                                            }
                                                                        }
                                                                    }
                                                                }


                                                                //echo '<pre>';
                                                                // print_r($allservice);
                                                                // print_r($allqty);exit;
                                                                // print_r($allserviceamt);
                                                                // print_r($allstores);
                                                                $total_service = array_unique($allservice);
                                                                $query = "SELECT CategoryId,ServiceID,StoredID,ProductId FROM `tblProductServiceCategory` WHERE `ServiceID` IN (" . implode(',', array_unique($allservice)) . ") ";


                                                                $ans = $conn->query($query) or die($conn->error);
                                                                $query_result = array();
                                                                if ($ans->num_rows > 0) {
                                                                    while ($result = $ans->fetch_assoc()) {
                                                                        $query_result[] = $result;
                                                                        $present_service[$result['ServiceID']] = $result['ServiceID'];
                                                                    }


                                                                    $deleted_service = array_diff($total_service, $present_service);

                                                                    if (isset($deleted_service) && is_array($deleted_service) && count($deleted_service) > 0) {
                                                                        foreach ($deleted_service as $dkey => $dvalue) {
                                                                            $finalservices[$apt_service_store[$dvalue]][$dvalue] = 0;
                                                                            $products[$dvalue] = 0;
                                                                        }
                                                                    }
                                                                    if (isset($query_result) && is_array($query_result) && count($query_result)) {
                                                                        foreach ($query_result as $key => $value) {
                                                                            $finalservices[$value['StoredID']][$value['ServiceID']] = $value['CategoryId'];
                                                                            $products[$value['ServiceID']] = $value['ProductId'];
                                                                        }
                                                                    }

//                                                                echo '<pre>';
//                                                                print_r($products);
//                                                                

                                                                    $t = array_flip($allservice);
                                                                    if (isset($finalservices) && is_array($finalservices) && count($finalservices)) {
                                                                        foreach ($finalservices as $storeid => $value) {
                                                                            foreach ($value as $serviceid => $categoryid) {
                                                                                $servicesgrp[$storeid][$serviceid] = $categoryid;
                                                                                $all_service_id[] = $serviceid;
                                                                            }
                                                                        }
                                                                    }

                                                                    if (isset($servicesgrp) && is_array($servicesgrp) && count($servicesgrp)) {
                                                                        foreach ($servicesgrp as $storekey => $value) {
                                                                            foreach ($value as $serviceid => $categoryvalue) {
                                                                                $all_category_ids[$categoryvalue] = $categoryvalue;
                                                                            }
                                                                        }
                                                                    }


                                                                    if (isset($all_category_ids) && is_array($all_category_ids) && count($all_category_ids) > 0) {
                                                                        $in_category_id = implode(",", $all_category_ids);
                                                                        if (isset($in_category_id) && $in_category_id != '') {
                                                                            /* $ser_pdtq = "SELECT psc.ServiceID,psc.StoreID,np.* FROM tblProductsServices psc JOIN tblnewproducts np ON (np.ProductID = psc.ProductID)"
                                                                              . " JOIN tblAppointments apt ON psc.StoreID=apt.StoreID "
                                                                              . " WHERE apt.IsDeleted != '1' AND apt.FreeService != '1' and apt.Status='2' "
                                                                              . " AND psc.CategoryID IN(" . $in_category_id . ") and psc.CategoryID!=''"; */
                                                                            $ser_pdtq = "SELECT psc.CategoryID,psc.ServiceID,psc.StoreID,np.* FROM `tblProductsServices` psc "
                                                                                    . " JOIN tblNewProducts np ON (np.ProductID = psc.ProductID) "
                                                                                    . " WHERE psc.CategoryID IN(" . $in_category_id . ") AND `ServiceID` IN (" . implode(',', array_unique($allservice)) . ") ";

                                                                            $serpdt_exe = $conn->query($ser_pdtq);
                                                                            while ($ser_esult = $serpdt_exe->fetch_assoc()) {
                                                                                $all_service_pdt[] = $ser_esult;
                                                                            }



                                                                            if (isset($all_service_pdt) && is_array($all_service_pdt) && count($all_service_pdt) > 0) {
                                                                                foreach ($all_service_pdt as $pdt_k => $pdt_val) {
                                                                                    $pdt_cost[$pdt_val['CategoryID']][] = array(
                                                                                        'StoredID' => $pdt_val['StoreID'],
                                                                                        'ServiceID' => $pdt_val['ServiceID'],
                                                                                        'ProductMRP' => $pdt_val['ProductMRP'],
                                                                                        'PerQtyServe' => $pdt_val['PerQtyServe'],
                                                                                    );
                                                                                }
                                                                            }



                                                                            if (isset($pdt_cost) && is_array($pdt_cost) && count($pdt_cost) > 0) {
                                                                                foreach ($pdt_cost as $servipro_k => $servipro_val) {
                                                                                    foreach ($servipro_val as $pdtpri_k => $pdtpri_val) {
                                                                                        $amounts = $pdtpri_val['ProductMRP'];
                                                                                        $qty = $pdtpri_val['PerQtyServe'];
                                                                                        $amount = $amounts / $qty;

                                                                                        if (isset($final_service_price[$servipro_k][$pdtpri_val['ServiceID']])) {
                                                                                            $final_service_price[$servipro_k][$pdtpri_val['ServiceID']]['amt'] += $amount;
                                                                                            $final_service_price[$servipro_k][$pdtpri_val['ServiceID']]['StoredID'] = $pdtpri_val['StoredID'];
                                                                                        } else {
                                                                                            $final_service_price[$servipro_k][$pdtpri_val['ServiceID']]['amt'] = $amount;
                                                                                            $final_service_price[$servipro_k][$pdtpri_val['ServiceID']]['StoredID'] = $pdtpri_val['StoredID'];
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }



                                                                            if (isset($final_service_price) && is_array($final_service_price) && count($final_service_price) > 0) {
                                                                                foreach ($final_service_price as $servipro_k => $fsvalue) {
                                                                                    foreach ($fsvalue as $fsakey => $fsavalue) {
                                                                                        if (isset($allqty[$fsavalue['StoredID']][$fsakey])) {
                                                                                            $qty = $allqty[$fsavalue['StoredID']][$fsakey];
                                                                                        } else {
                                                                                            $qty = 1;
                                                                                        }
                                                                                        if (isset($final_service_price_total[$fsavalue['StoredID']][$servipro_k])) {

                                                                                            $final_service_price_total[$fsavalue['StoredID']][$servipro_k] += (round($fsavalue['amt']) * $qty);
                                                                                        } else {

                                                                                            $final_service_price_total[$fsavalue['StoredID']][$servipro_k] = round($fsavalue['amt']) * $qty;
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }

                                                                            /* if (isset($pdt_cost) && is_array($pdt_cost) && count($pdt_cost) > 0) {
                                                                              foreach ($pdt_cost as $servipro_k => $servipro_val) {
                                                                              foreach ($servipro_val as $pdtpri_k => $pdtpri_val) {
                                                                              $amounts = $pdtpri_val['ProductMRP'];
                                                                              $qty = $pdtpri_val['PerQtyServe'];

                                                                              $amount = $amounts / $qty;

                                                                              //$amount = round($amount);

                                                                              /* if (isset($allqty[$pdtpri_val['StoredID']][$pdtpri_val['ServiceID']])) {
                                                                              $amount = $amount * $allqty[$pdtpri_val['StoredID']][$pdtpri_val['ServiceID']];
                                                                              if($pdtpri_val['ServiceID']==9517){
                                                                              echo '<br>amt='.$amount;
                                                                              }
                                                                              } */
                                                                            /*   if (isset($final_service_price_total[$pdtpri_val['StoredID']][$servipro_k])) {

                                                                              $final_service_price_total[$pdtpri_val['StoredID']][$servipro_k] += round($amount);
                                                                              } else {

                                                                              $final_service_price_total[$pdtpri_val['StoredID']][$servipro_k] = round($amount);
                                                                              }
                                                                              }
                                                                              }
                                                                              } */


                                                                            if (isset($final_service_price_total) && is_array($final_service_price_total) && count($final_service_price_total) > 0) {
                                                                                foreach ($final_service_price_total as $fikey => $fivalue) {
                                                                                    foreach ($fivalue as $cokey => $covalue) {
                                                                                        $final_service_price[$fikey][$cokey] = round($covalue);
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }

                                                                    if (isset($servicesgrp) && is_array($servicesgrp) && count($servicesgrp)) {
                                                                        foreach ($servicesgrp as $storekey => $value) {
                                                                            foreach ($value as $serviceid => $categoryvalue) {
                                                                                /*
                                                                                 * get key of service id
                                                                                 */
                                                                                //$service_id_key = array_search($serviceid, $allservice);
                                                                                $service_id_key = $serviceid;
                                                                                //$service_amt = isset($allserviceamt[$service_id_key]) ? $allserviceamt[$service_id_key] : 0;
                                                                                //$service_qty = isset($allqty[$service_id_key]) ? $allqty[$service_id_key] : 0;


                                                                                $service_amt = isset($allserviceamt[$storekey][$service_id_key]) ? $allserviceamt[$storekey][$service_id_key] : 0;
                                                                                $service_qty = isset($allqty[$storekey][$service_id_key]) ? $allqty[$storekey][$service_id_key] : 0;


                                                                                if (isset($final_array[$storekey][$categoryvalue]['qty'])) {
                                                                                    $final_array[$storekey][$categoryvalue]['qty'] += $service_qty;
                                                                                } else {
                                                                                    $final_array[$storekey][$categoryvalue]['qty'] = $service_qty;
                                                                                }

                                                                                if (isset($final_array[$storekey][$categoryvalue]['totalamount'])) {
                                                                                    $final_array[$storekey][$categoryvalue]['totalamount'] += $service_amt;
                                                                                } else {
                                                                                    $final_array[$storekey][$categoryvalue]['totalamount'] = $service_amt;
                                                                                }

                                                                                /*
                                                                                 * cost calculation
                                                                                 */
                                                                                $final_array[$storekey][$categoryvalue]['cost'] = isset($final_service_price[$storekey][$categoryvalue]) ? round($final_service_price[$storekey][$categoryvalue]) : 0;
                                                                                /* if (isset($final_array[$storekey][$categoryvalue]['cost'])) {
                                                                                  $final_array[$storekey][$categoryvalue]['cost'] +=- isset($final_service_price[$storekey][$serviceid]) ? round($final_service_price[$storekey][$serviceid]) : 0;
                                                                                  } else {
                                                                                  $final_array[$storekey][$categoryvalue]['cost'] = isset($final_service_price[$storekey][$serviceid]) ? round($final_service_price[$storekey][$serviceid]) : 0;
                                                                                  } */
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }



                                                            $all_store_res = array();

                                                            if (empty($storr)) {
                                                                if (isset($final_array) && is_array($final_array) && count($final_array) > 0) {
                                                                    foreach ($final_array as $finkey => $allvalue) {
                                                                        foreach ($allvalue as $fkey => $fvalue) {
                                                                            if (isset($all_store_res[0][$fkey])) {
                                                                                $all_store_res[0][$fkey]['qty'] += $fvalue['qty'];
                                                                                $all_store_res[0][$fkey]['totalamount'] += $fvalue['totalamount'];
                                                                                $all_store_res[0][$fkey]['cost'] += $fvalue['cost'];
                                                                            } else {
                                                                                $all_store_res[0][$fkey] = $fvalue;
                                                                            }
                                                                        }
                                                                    }
                                                                }


                                                                $final_array[0] = $all_store_res[0];
                                                            }



                                                            /*
                                                             * Sort Result
                                                             */
                                                            if (isset($final_array) && is_array($final_array) && count($final_array) > 0) {
                                                                foreach ($final_array as $skey => $svalue) {
                                                                    uasort($svalue, 'sortByName');
                                                                    $store_sort_res = array_reverse($svalue, TRUE);
                                                                    $final_res[$skey] = $store_sort_res;
                                                                }
                                                            }


                                                            $conn->close();
                                                            ?>
                                                            <div id="kakkabiryani">
                                                                <h3 class="title-hero">Date Range selected : FROM - <?= $getfrom ?> / TO - <?= $getto ?> / Store Filter selected : <?= $storrrp ?></h3>

                                                                <br>

                                                                <div class="panel">
                                                                    <div class="panel-body">
                                                                        <div class="example-box-wrapper">
                                                                            <div class="scroll-columns">
                                                                                <?php
                                                                                $per = $_GET["per"];
                                                                                if (isset($store_data) && is_array($store_data) && count($store_data) > 0) {
                                                                                    foreach ($store_data as $skey => $svalue) {
                                                                                        $storekey = $svalue['StoreID'];
                                                                                        $value = isset($final_res) && isset($final_res[$svalue['StoreID']]) ? $final_res[$svalue['StoreID']] : array();
                                                                                        //foreach ($final_res as $storekey => $value) {
                                                                                        ?>
                                                                                        <h2>Store:<?php echo $storekey == 'all' ? 'All' : $tblstores[$storekey]; ?></h2>
                                                                                        <table class="table table-bordered table-striped table-condensed cf table-hover" width="100%">
                                                                                            <thead class="cf">

                                                                                                <tr>
                                                                                                    <th class="numeric">Category</th>
                                                                                                    <th class="numeric">Amt</th>
                                                                                                    <?php
                                                                                                    if ($per != '0') {
                                                                                                        ?>
                                                                                                        <th >Amt %</th>
                                                                                                        <?php
                                                                                                    }
                                                                                                    ?>
                                                                                                    <th class="numeric">Service-Count</th>
                                                                                                    <?php
                                                                                                    if ($per != '0') {
                                                                                                        ?>
                                                                                                        <th >Service %</th>
                                                                                                        <?php
                                                                                                    }
                                                                                                    ?>
                                                                                                    <th class="numeric">Product Cost</th>
                                                                                                    <th class="numeric">Profitability</th>
                                                                                                    <?php /* <th class="numeric">ARPU</th> */ ?>
                                                                                                    <th class="numeric">ARPU %</th>
                                                                                                </tr>
                                                                                                <?php
                                                                                                $totalamt = 0;
                                                                                                $totalserviccount = 0;
                                                                                                $totalproductcost = 0;
                                                                                                $totalprofitability = 0;
                                                                                                $totalarup = 0;
                                                                                                $totalarup_per = 0;
                                                                                                $total_service_amt = 0;
                                                                                                $total_service_count = 0;
                                                                                                $totalsamt = 0;
                                                                                                $totalsqty = 0;
                                                                                                if (isset($value) && is_array($value) && count($value) > 0) {
                                                                                                    foreach ($value as $toalpk => $toalpv) {
                                                                                                        $profit = $toalpv['totalamount'] - (number_format((float) $toalpv['cost'], 2, '.', ''));
                                                                                                        $totalprofitability += $profit;
                                                                                                        $total_service_amt +=$toalpv['totalamount'];
                                                                                                        $total_service_count +=$toalpv['qty'];
                                                                                                    }



                                                                                                    foreach ($value as $categorykey => $v) {
                                                                                                        $get_param = '';
                                                                                                        if (isset($_GET['toandfrom'])) {
                                                                                                            $get_param .= "?toandfrom=" . $_GET['toandfrom'];
                                                                                                        }
                                                                                                        if ($storekey == 'all') {
                                                                                                            $get_param .= "&Store=0";
                                                                                                        } else {
                                                                                                            $get_param .= "&Store=" . $storekey;
                                                                                                        }
                                                                                                        if (isset($_GET['source'])) {
                                                                                                            $get_param .= "&source=" . $_GET['source'];
                                                                                                        }
                                                                                                        $get_param .= "&Category=" . $categorykey;
                                                                                                        $report_url = "service_analysis_modal.php" . $get_param;
                                                                                                        ?>
                                                                                                        <tr onclick="GetDetailReport('<?php echo $report_url; ?>');">
                                                                                                            <td><?php echo $tblCategories[$categorykey]; ?></td>
                                                                                                            <td><?php
                                                                                                                echo $v['totalamount'];
                                                                                                                $totalamt += $v['totalamount'];
                                                                                                                ?></td>
                                                                                                            <?php
                                                                                                            if ($per != '0') {
                                                                                                                ?>
                                                                                                                <td>
                                                                                                                    <?php
                                                                                                                    $seramtper = ($v['totalamount'] / $total_service_amt) * 100;
                                                                                                                    $totalsamt +=$seramtper;
                                                                                                                    echo round($seramtper, 2);
                                                                                                                    ?>
                                                                                                                </td>
                                                                                                                <?php
                                                                                                            }
                                                                                                            ?>
                                                                                                            <td><?php
                                                                                                                echo $v['qty'];
                                                                                                                $totalserviccount+= $v['qty'];
                                                                                                                ?></td>
                                                                                                            <?php
                                                                                                            if ($per != '0') {
                                                                                                                ?>
                                                                                                                <td><?php
                                                                                                                    $qtyper = ($v['qty'] / $total_service_count) * 100;
                                                                                                                    $totalsqty +=$qtyper;
                                                                                                                    echo round($qtyper, 2);
                                                                                                                    ?></td>
                                                                                                                <?php
                                                                                                            }
                                                                                                            ?>
                                                                                                            <td><?php
                                                                                                                $t = number_format((float) $v['cost'], 2, '.', '');
                                                                                                                echo $t;
                                                                                                                $totalproductcost += $t;
                                                                                                                ?></td>
                                                                                                            <td><?php
                                                                                                                $x = $v['totalamount'] - (number_format((float) $v['cost'], 2, '.', ''));
                                                                                                                echo $x;
                                                                                                                ?></td>
                                                                                                            <?php /* <td><?php
                                                                                                              $arpu = round(($x / $t), 2);
                                                                                                              echo $arpu;
                                                                                                              $totalarup += $arpu;
                                                                                                              ?></td> */ ?>
                                                                                                            <td><?php
                                                                                                                // $arpu = round(($x / $t), 2);
                                                                                                                $ow_profit = $v['totalamount'] - (number_format((float) $v['cost'], 2, '.', ''));
                                                                                                                $arpu_per = ($ow_profit / $totalprofitability) * 100;
                                                                                                                $arpu_per = number_format($arpu_per, 2);
                                                                                                                echo $arpu_per;
                                                                                                                $totalarup_per += $arpu_per;
                                                                                                                ?></td>
                                                                                                        </tr>

                                                                                                        <?php
                                                                                                    }
                                                                                                }

                                                                                                $total_get_param = '';
                                                                                                if (isset($_GET['toandfrom'])) {
                                                                                                    $total_get_param .= "?toandfrom=" . $_GET['toandfrom'];
                                                                                                }
                                                                                                if ($storekey == 'all') {
                                                                                                    $total_get_param .= "&Store=0";
                                                                                                } else {
                                                                                                    $total_get_param .= "&Store=" . $storekey;
                                                                                                }
                                                                                                if (isset($_GET['source'])) {
                                                                                                    $total_get_param .= "&source=" . $_GET['source'];
                                                                                                }
                                                                                                $total_report_url = "service_analysis_modal.php" . $total_get_param;
                                                                                                ?>
                                                                                                <tr onclick="GetDetailReport('<?php echo $total_report_url; ?>');">

                                                                                                    <td><b>Total</b></td>
                                                                                                    <td><?php echo $totalamt; ?></td>
                                                                                                    <?php
                                                                                                    if ($per != '0') {
                                                                                                        ?>
                                                                                                        <td><?php echo round($totalsamt, 2); ?></td>
                                                                                                    <?php } ?>
                                                                                                    <td><?php echo $totalserviccount; ?></td>
                                                                                                    <?php
                                                                                                    if ($per != '0') {
                                                                                                        ?>
                                                                                                        <td><?php echo round($totalsqty, 2); ?></td>
                                                                                                    <?php } ?>
                                                                                                    <td><?php echo $totalproductcost; ?></td>
                                                                                                    <td><?php echo $totalprofitability; ?></td>
                                                                                                    <?php /* <td><?php echo $totalarup; ?></td> */ ?>
                                                                                                    <td><?php echo round($totalarup_per); ?></td>
                                                                                                </tr>
                                                                                            </thead>
                                                                                        </table>
                                                                                        <?php
                                                                                    }
                                                                                }
                                                                                ?>


                                                                            </div>										
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <?php
                                                        } else {
                                                            echo "<br><center><h3>Please Select Month And Year!</h3></center>";
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        } // End null condition
                                        else {
                                            
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?php require_once 'incFooter.fya'; ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>

</html>