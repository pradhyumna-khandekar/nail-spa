<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Manage Servces | Nailspa";
$strDisplayTitle = "Servces Logs for Nailspa";
$strMenuID = "3";
$strMyTable = "tblServces";
$strMyTableID = "ServceID";
$strMyField = "OfferName";
$strMyActionPage = "ServiceshistoryList.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <!-----------css & js files added for tabs by gandhali 3/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");     ?>
            <!----------commented by gandhali 3/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>

                        <script type="text/javascript">
                            /* Datatables responsive */
                            $(document).ready(function () {
                                $('#datatable-responsive-scroll').DataTable({
                                    "scrollX": true
                                });
                            });
                            $(document).ready(function () {
                                $('.dataTables_filter input').attr("placeholder", "Search...");
                            });
                        </script>

                        <?php
                        $DB = Connect();


                        /*
                         * get store history
                         */
                        if (isset($_GET['service_id']) && $_GET['service_id'] != '') {
                            $historyq = "SELECT * FROM tblServices_history WHERE ServiceID='" . $_GET['service_id'] . "'";

                            $historyq_exe = $DB->query($historyq);
                            if ($historyq_exe->num_rows > 0) {
                                while ($history_row = $historyq_exe->fetch_assoc()) {
                                    $history_data[] = $history_row;
                                }
                            }
                            if (isset($history_data) && is_array($history_data) && count($history_data) > 0) {
                                /*
                                 * get all users
                                 */
                                $userq = "SELECT * FROM tblAdmin";
                                $userq_exe = $DB->query($userq);
                                if ($userq_exe->num_rows > 0) {
                                    while ($user_row = $userq_exe->fetch_assoc()) {
                                        $user_data[$user_row['AdminID']] = $user_row;
                                    }
                                }
                            }

                            /*
                             * Get All Store,Category,Service Ids
                             */
                            if (isset($history_data) && is_array($history_data) && count($history_data) > 0) {
                                foreach ($history_data as $namekey => $namevalue) {
                                    /*
                                     * Get Store Ids
                                     */
                                    $store_arr = explode(",", $namevalue['mul_store_id']);
                                    if (isset($store_arr) && is_array($store_arr) && count($store_arr) > 0) {
                                        foreach ($store_arr as $stkey => $stvalue) {
                                            if ($stvalue != '') {
                                                $all_store_id[$stvalue] = $stvalue;
                                            }
                                        }
                                    }

                                    /*
                                     * Get Category Ids
                                     */
                                    $category_arr = explode(",", $namevalue['mul_category_id']);
                                    if (isset($category_arr) && is_array($category_arr) && count($category_arr) > 0) {
                                        foreach ($category_arr as $catkey => $catvalue) {
                                            if ($catvalue != '') {
                                                $all_category_id[$catvalue] = $catvalue;
                                            }
                                        }
                                    }

                                    /*
                                     * Get Employee Ids
                                     */
                                    $emp_arr = explode(",", $namevalue['mul_employee_id']);
                                    if (isset($emp_arr) && is_array($emp_arr) && count($emp_arr) > 0) {
                                        foreach ($emp_arr as $empkey => $empval) {
                                            if ($empval != '') {
                                                $all_emp_id[$empval] = $empval;
                                            }
                                        }
                                    }

                                    /*
                                     * Get Charge Ids
                                     */
                                    $charge_arr = explode(",", $namevalue['mul_charge_id']);
                                    if (isset($charge_arr) && is_array($charge_arr) && count($charge_arr) > 0) {
                                        foreach ($charge_arr as $chargekey => $chargeval) {
                                            if ($chargeval != '') {
                                                $all_charge_id[$chargeval] = $chargeval;
                                            }
                                        }
                                    }

                                    /*
                                     * Get Product Ids
                                     */
                                    $product_arr = explode(",", $namevalue['mul_product_id']);
                                    if (isset($product_arr) && is_array($product_arr) && count($product_arr) > 0) {
                                        foreach ($product_arr as $pdtkey => $pdtval) {
                                            if ($pdtval != '') {
                                                $all_product_id[$pdtval] = $pdtval;
                                            }
                                        }
                                    }
                                }


                                /*
                                 * Get All Store,Category,Service name
                                 */
                                if (isset($all_store_id) && is_array($all_store_id) && count($all_store_id) > 0) {
                                    $in_store_ids = implode(",", $all_store_id);
                                    if ($in_store_ids != '') {
                                        $store_sql = "select * FROM tblStores WHERE StoreID IN(" . $in_store_ids . ")";
                                        $store_sql_exe = $DB->query($store_sql);
                                        if ($store_sql_exe->num_rows > 0) {
                                            while ($store_row = $store_sql_exe->fetch_assoc()) {
                                                $all_store_name[$store_row["StoreID"]] = $store_row['StoreName'];
                                            }
                                        }
                                    }
                                }

                                if (isset($all_category_id) && is_array($all_category_id) && count($all_category_id) > 0) {
                                    $in_cat_ids = implode(",", $all_category_id);
                                    if ($in_cat_ids != '') {
                                        $category_sql = "select * FROM tblCategories WHERE CategoryID IN(" . $in_cat_ids . ")";
                                        $category_sql_exe = $DB->query($category_sql);
                                        if ($category_sql_exe->num_rows > 0) {
                                            while ($category_row = $category_sql_exe->fetch_assoc()) {
                                                $all_category_name[$category_row["CategoryID"]] = $category_row['CategoryName'];
                                            }
                                        }
                                    }
                                }

                                if (isset($all_emp_id) && is_array($all_emp_id) && count($all_emp_id) > 0) {
                                    $in_emp_ids = implode(",", $all_emp_id);
                                    if ($in_emp_ids != '') {
                                        $emp_sql = "select * FROM tblEmployees WHERE EID IN(" . $in_emp_ids . ")";
                                        $emp_sql_exe = $DB->query($emp_sql);
                                        if ($emp_sql_exe->num_rows > 0) {
                                            while ($emp_row = $emp_sql_exe->fetch_assoc()) {
                                                $all_emp_data[$emp_row["EID"]] = $emp_row['EmployeeName'];
                                            }
                                        }
                                    }
                                }

                                if (isset($all_charge_id) && is_array($all_charge_id) && count($all_charge_id) > 0) {
                                    $in_charge_ids = implode(",", $all_charge_id);
                                    if ($in_charge_ids != '') {
                                        $charge_sql = "select * FROM tblChargeNames WHERE ChargeNameID IN(" . $in_charge_ids . ")";
                                        $charge_sql_exe = $DB->query($charge_sql);
                                        if ($charge_sql_exe->num_rows > 0) {
                                            while ($charge_row = $charge_sql_exe->fetch_assoc()) {
                                                $all_charge_data[$charge_row["ChargeNameID"]] = $charge_row['ChargeName'];
                                            }
                                        }
                                    }
                                }


                                if (isset($all_product_id) && is_array($all_product_id) && count($all_product_id) > 0) {
                                    $in_pdt_ids = implode(",", $all_product_id);
                                    if ($in_pdt_ids != '') {
                                        $pdt_sql = "select * FROM tblNewProducts WHERE ProductID IN(" . $in_pdt_ids . ")";
                                        $pdt_sql_exe = $DB->query($pdt_sql);
                                        if ($pdt_sql_exe->num_rows > 0) {
                                            while ($pdt_row = $pdt_sql_exe->fetch_assoc()) {
                                                $all_product_data[$pdt_row["ProductID"]] = $pdt_row['ProductName'];
                                            }
                                        }
                                    }
                                }
                            }

                            $DB->close();
                        }
                        $counter = 0;
                        ?>
                        <div id="page-title">
                            <h2>All Offers Log</h2>
                        </div>

                        <div class="panel">
                            <div class="panel">
                                <div class="panel-body">

                                    <div class="example-box-wrapper">
                                        <div class="tabs">
                                            <div id="normal-tabs-1">
                                                <span class="form_result">&nbsp; <br>
                                                </span>

                                                <div class="panel-body">
                                                    <h3 class="title-hero">List of Services Logs for POS</h3>
                                                    <div class="example-box-wrapper">
                                                        <table id="datatable-responsive-scroll" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                                                            <thead>
                                                                <tr>
                                                                    <th>Action</th>
                                                                    <th>User</th>
                                                                    <th>Date/time</th>
                                                                    <th>Service Name</th>
                                                                    <th>Time In (Min.)</th>
                                                                    <th>Service Code</th>
                                                                    <th>Service MRP</th>
                                                                    <th>Our Profit</th>
                                                                    <th>Status</th>
                                                                    <th>Store Name</th>
                                                                    <th>Category Name</th>
                                                                    <th>Product Name</th>
                                                                    <th>Technicians</th>
                                                                    <th>Charges</th>
                                                                </tr>
                                                            </thead>
                                                            <tfoot>
                                                                <tr>
                                                                    <th>Action</th>
                                                                    <th>User</th>
                                                                    <th>Date/time</th>
                                                                    <th>Service Name</th>
                                                                    <th>Time In (Min.)</th>
                                                                    <th>Service Code</th>
                                                                    <th>Service MRP</th>
                                                                    <th>Our Profit</th>
                                                                    <th>Status</th>
                                                                    <th>Store Name</th>
                                                                    <th>Category Name</th>
                                                                    <th>Product Name</th>
                                                                    <th>Technicians</th>
                                                                    <th>Charges</th>
                                                                </tr>
                                                            </tfoot>
                                                            <tbody>
                                                                <?php
                                                                if (isset($history_data) && is_array($history_data) && count($history_data) > 0) {
                                                                    foreach ($history_data as $hkey => $hvalue) {
                                                                        if ($hvalue['history_notes'] == 'insert') {
                                                                            $action_name = "<p style='color:blue;'>Add</p>";
                                                                            $action_responsible = isset($user_data[$hvalue['created_by']]) ? $user_data[$hvalue['created_by']]['AdminFullName'] : '';
                                                                        } else if ($hvalue['history_notes'] == 'update') {
                                                                            $action_name = "<p class='text-success'>Edit</p>";
                                                                            $action_responsible = isset($user_data[$hvalue['modified_by']]) ? $user_data[$hvalue['modified_by']]['AdminFullName'] : '';
                                                                        } else if ($hvalue['history_notes'] == 'delete') {
                                                                            $action_name = "<p class='text-danger'>Delete</p>";
                                                                            $action_responsible = isset($user_data[$hvalue['modified_by']]) ? $user_data[$hvalue['modified_by']]['AdminFullName'] : '';
                                                                        }
                                                                        ?> 
                                                                        <tr id="my_data_tr_<?= $counter ?>">
                                                                            <td><?php echo isset($action_name) ? $action_name : '' ?></td>
                                                                            <td><?php echo isset($action_responsible) ? $action_responsible : '' ?></td>
                                                                            <td><?php echo isset($hvalue['history_timestamp']) ? date('d/m/Y h:i a', strtotime($hvalue['history_timestamp'])) : ''; ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && $history_data[$hkey - 1]['ServiceName'] != $hvalue['ServiceName']) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . $hvalue['ServiceName'] . '</span>';
                                                                                ?></td>

                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && $history_data[$hkey - 1]['Time'] != $hvalue['Time']) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . $hvalue['Time'] . '</span>';
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && $history_data[$hkey - 1]['ServiceCode'] != $hvalue['ServiceCode']) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . $hvalue['ServiceCode'] . '</span>';
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && $history_data[$hkey - 1]['ServiceCost'] != $hvalue['ServiceCost']) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . $hvalue['ServiceCost'] . '</span>';
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && $history_data[$hkey - 1]['GrossMargin'] != $hvalue['GrossMargin']) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . $hvalue['GrossMargin'] . '</span>';
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && $history_data[$hkey - 1]['Status'] != $hvalue['Status']) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . (isset($hvalue['Status']) && $hvalue['Status'] == 1 ? 'Offline' : 'Live') . '</span>';
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && trim($history_data[$hkey - 1]['mul_store_id'], ',') != trim($hvalue['mul_store_id'], ',')) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                $history_store = explode(",", $hvalue['mul_store_id']);
                                                                                $his_store_name = array();
                                                                                if (isset($history_store) && is_array($history_store) && count($history_store) > 0) {
                                                                                    foreach ($history_store as $hstkey => $hstvalue) {
                                                                                        if ($hstvalue != '' && isset($all_store_name[trim($hstvalue)])) {
                                                                                            $his_store_name[$hstvalue] = $all_store_name[trim($hstvalue)];
                                                                                        }
                                                                                    }
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . implode(",", $his_store_name) . '</span>';
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && trim($history_data[$hkey - 1]['mul_category_id'], ',') != trim($hvalue['mul_category_id'], ',')) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                $history_cat = explode(",", $hvalue['mul_category_id']);
                                                                                $his_cat_name = array();
                                                                                if (isset($history_cat) && is_array($history_cat) && count($history_cat) > 0) {
                                                                                    foreach ($history_cat as $hcatkey => $hcatvalue) {
                                                                                        if ($hcatvalue != '' && isset($all_category_name[trim($hcatvalue)])) {
                                                                                            $his_cat_name[$hcatvalue] = $all_category_name[trim($hcatvalue)];
                                                                                        }
                                                                                    }
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . implode(",", $his_cat_name) . '</span>';
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && trim($history_data[$hkey - 1]['mul_product_id'], ',') != trim($hvalue['mul_product_id'], ',')) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                $history_product = explode(",", $hvalue['mul_product_id']);
                                                                                $his_product_name = array();
                                                                                if (isset($history_product) && is_array($history_product) && count($history_product) > 0) {
                                                                                    foreach ($history_product as $hpdtkey => $hpdtvalue) {
                                                                                        if ($hpdtvalue != '' && isset($all_product_data[trim($hpdtvalue)])) {
                                                                                            $his_product_name[$hpdtvalue] = $all_product_data[trim($hpdtvalue)];
                                                                                        }
                                                                                    }
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . implode(",", $his_product_name) . '</span>';
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && trim($history_data[$hkey - 1]['mul_employee_id'], ',') != trim($hvalue['mul_employee_id'], ',')) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                $history_emp = explode(",", $hvalue['mul_employee_id']);
                                                                                $his_emp_name = array();
                                                                                if (isset($history_emp) && is_array($history_emp) && count($history_emp) > 0) {
                                                                                    foreach ($history_emp as $hempkey => $hempvalue) {
                                                                                        if ($hempvalue != '' && isset($all_emp_data[trim($hempvalue)])) {
                                                                                            $his_emp_name[$hempvalue] = $all_emp_data[trim($hempvalue)];
                                                                                        }
                                                                                    }
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . implode(",", $his_emp_name) . '</span>';
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && trim($history_data[$hkey - 1]['mul_charge_id'], ',') != trim($hvalue['mul_charge_id'], ',')) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                $history_charge = explode(",", $hvalue['mul_charge_id']);
                                                                                $his_charge_name = array();
                                                                                if (isset($history_charge) && is_array($history_charge) && count($history_charge) > 0) {
                                                                                    foreach ($history_charge as $hcharkey => $hcharvalue) {
                                                                                        if ($hcharvalue != '' && isset($all_charge_data[trim($hcharvalue)])) {
                                                                                            $his_charge_name[$hcharvalue] = $all_charge_data[trim($hcharvalue)];
                                                                                        }
                                                                                    }
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . implode(",", $his_charge_name) . '</span>';
                                                                                ?></td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    $counter++;
                                                                } else {
                                                                    ?>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>
                                                                <?php }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <?php require_once 'incFooter.fya'; ?>
        </div>
    </body>
</html>
