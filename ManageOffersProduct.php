<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Offer Management| Nailspa";
$strDisplayTitle = "Manage Offers For Retail Product | Nailspa";
$strMenuID = "3";
$strMyTable = "tblOffers";
$strMyTableID = "OfferID";
$strMyField = "OfferCode";
$strMyActionPage = "ManageOffersProduct.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights	

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $TagID = $_POST["OfferID"];
    $strStep = Filter($_POST["step"]);
    if ($strStep == "add") {

        $strOfferName = Filter($_POST["OfferName"]);
        $strOfferDateFrom = $_POST["OfferDateFrom"];
        $strOfferDateTo = $_POST["OfferDateTo"];

        $strOfferDateFrom1 = date("Y-m-d", strtotime($strOfferDateFrom));
        $strOfferDateTo1 = date("Y-m-d", strtotime($strOfferDateTo));
        $strStoreID = $_POST["StoreID"];
        $ProductID = $_POST["ProductID"];
        $Type = $_POST["Type"];
        $OfferAmount = Filter($_POST["OfferAmount"]);

        $offercode = Filter($_POST["OfferCode"]);
        $offer_day_limit = isset($_POST["offer_day_limit"]) && $_POST["offer_day_limit"] != '' ? $_POST["offer_day_limit"] : 0;

        if (count($ProductID) == 0) {
            die('<div class="alert alert-warning alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
								</button>
								<strong>No Product Selected.</strong>
							</div>');
            die("");
        }

        $DB = Connect();
        $sql = "SELECT id FROM product_offers WHERE OfferCode=" . $_POST["OfferCode"] . " AND status=1";
        $RS = $DB->query($sql);
        if ($RS->num_rows > 0) {
            $DB->close();
            die('<div class="alert alert-warning alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
								</button>
								<strong>The Offer Code already exists in the system.</strong>
							</div>');
            die("");
        } else {

            $sqlInsert = "Insert into product_offers (OfferName,OfferCode,OfferDateFrom, OfferDateTo, offer_day_limit, Type,OfferAmount,created_date,created_by) values
						('" . $strOfferName . "','" . $offercode . "', '" . $strOfferDateFrom1 . "', '" . $strOfferDateTo1 . "', '" . $offer_day_limit . "', '" . $Type . "', '" . $OfferAmount . "' , '" . date('Y-m-d H:i:s') . "', '" . $strAdminID . "')";

            if ($DB->query($sqlInsert) === TRUE) {
                $offfid = $DB->insert_id;

                if (isset($strStoreID) && is_array($strStoreID) && count($strStoreID) > 0) {
                    foreach ($strStoreID as $skey => $svalue) {
                        if ($svalue != '') {
                            $storeInsert = "Insert into product_offers_stores (offer_id,store_id,created_date, created_by) values
						('" . $offfid . "','" . $svalue . "','" . date('Y-m-d H:i:s') . "', '" . $strAdminID . "')";
                            $DB->query($storeInsert);
                        }
                    }
                }

                if (isset($ProductID) && is_array($ProductID) && count($ProductID) > 0) {
                    foreach ($ProductID as $pkey => $pvalue) {
                        if ($pvalue != '') {
                            $productInsert = "Insert into product_offers_product_id (offer_id,product_id,created_date, created_by) values
						('" . $offfid . "','" . $pvalue . "','" . date('Y-m-d H:i:s') . "', '" . $strAdminID . "')";
                            $DB->query($productInsert);
                        }
                    }
                }

                $DB->Close();
                die('<div class="alert alert-close alert-success">
								<div class="bg-green alert-icon"><i class="glyph-icon icon-check"></i></div>
								<div class="alert-content">
									<h4 class="alert-title">Record Added Successfully</h4>
									<p>Information message box using the color scheme.</p>
								</div>
							</div>');
            }
        }
    }

    if ($strStep == "edit") {
        $ProductID = $_POST["ProductID"];
        if (count($ProductID) == 0) {
            die('<div class="alert alert-warning alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
								</button>
								<strong>No Product Selected.</strong>
							</div>');
            die("");
        }

        $DB = Connect();
        $OfferID = $_POST["OfferID"];
        $strOfferName = Filter($_POST["OfferName"]);
        $strOfferDateFrom = $_POST["OfferDateFrom"];
        $strOfferDateTo = $_POST["OfferDateTo"];

        $strOfferDateFrom1 = date("Y-m-d", strtotime($strOfferDateFrom));
        $strOfferDateTo1 = date("Y-m-d", strtotime($strOfferDateTo));
        $strStoreID = $_POST["StoreID"];
        $Type = $_POST["Type"];
        $OfferAmount = Filter($_POST["OfferAmount"]);
        $offer_day_limit = isset($_POST["offer_day_limit"]) && $_POST["offer_day_limit"] != '' ? $_POST["offer_day_limit"] : 0;

        $updateq = "UPDATE product_offers SET OfferName='" . $strOfferName . "',OfferDateFrom='" . $strOfferDateFrom1 . "',"
                . "OfferDateTo='" . $strOfferDateTo1 . "',offer_day_limit='" . $offer_day_limit . "',"
                . "Type='" . $Type . "',OfferAmount='" . $OfferAmount . "',modified_date='" . date('Y-m-d H:i:s') . "',"
                . "modified_by='" . $strAdminID . "' WHERE id='" . $OfferID . "'";
        $DB->query($updateq);

        /*
         * inactivate all store and products
         */
        $ina_store = "UPDATE product_offers_stores SET status=2,modified_date='" . date('Y-m-d H:i:s') . "',"
                . "modified_by='" . $strAdminID . "' WHERE offer_id='" . $OfferID . "'";
        $DB->query($ina_store);

        $ina_product = "UPDATE product_offers_product_id SET status=2,modified_date='" . date('Y-m-d H:i:s') . "',"
                . "modified_by='" . $strAdminID . "' WHERE offer_id='" . $OfferID . "'";
        $DB->query($ina_product);


        /*
         * Get offer stores
         */
        $offer_stores = select("store_id", "product_offers_stores", "status=1  AND offer_id='" . $OfferID . "'");
        if (isset($offer_stores) && is_array($offer_stores) && count($offer_stores) > 0) {
            foreach ($offer_stores as $oskey => $osvalue) {
                $offer_store_id[$osvalue['store_id']] = $osvalue['id'];
            }
        }

        /*
         * Get offer Products
         */
        $offer_products = select("product_id", "product_offers_product_id", "status=1  AND offer_id='" . $OfferID . "'");
        if (isset($offer_products) && is_array($offer_products) && count($offer_products) > 0) {
            foreach ($offer_products as $opkey => $opvalue) {
                $offer_product_id[$opvalue['product_id']] = $opvalue['id'];
            }
        }

        if (isset($strStoreID) && is_array($strStoreID) && count($strStoreID) > 0) {
            foreach ($strStoreID as $skey => $svalue) {
                if ($svalue != '') {
                    if (isset($offer_store_id[$svalue])) {
                        $store_update = "UPDATE product_offers_stores SET status=1 WHERE id='" . $offer_store_id[$svalue] . "'";
                        $DB->query($store_update);
                    } else {
                        $storeInsert = "Insert into product_offers_stores (offer_id,store_id,created_date, created_by) values
						('" . $OfferID . "','" . $svalue . "','" . date('Y-m-d H:i:s') . "', '" . $strAdminID . "')";
                        $DB->query($storeInsert);
                    }
                }
            }
        }

        if (isset($ProductID) && is_array($ProductID) && count($ProductID) > 0) {
            foreach ($ProductID as $pkey => $pvalue) {
                if ($pvalue != '') {
                    if (isset($offer_product_id[$pvalue])) {
                        $store_update = "UPDATE product_offers_product_id SET status=1 WHERE id='" . $offer_product_id[$pvalue] . "'";
                        $DB->query($store_update);
                    } else {
                        $productInsert = "Insert into product_offers_product_id (offer_id,product_id,created_date, created_by) values
						('" . $OfferID . "','" . $pvalue . "','" . date('Y-m-d H:i:s') . "', '" . $strAdminID . "')";
                        $DB->query($productInsert);
                    }
                }
            }
        }

        die('<div class="alert alert-close alert-success">
					<div class="bg-green alert-icon"><i class="glyph-icon icon-check"></i></div>
					<div class="alert-content">
						<h4 class="alert-title">Record Updated Successfully</h4>
					</div>
				</div>');
    }
    die();
}
?>
<!DOCTYPE html>
<html lang="en">

    <head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <?php require_once("incMetaScript.fya"); ?>
        <!-----------css & js files added for tabs by gandhali 5/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");   ?>
            <!----------commented by gandhali 5/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>
                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>

                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                            <p>Add, edit, delete POST</p>
                        </div>

                        <?php
                        if (!isset($_GET["uid"])) {
                            ?>				
                            <div class="panel">
                                <div class="panel">
                                    <div class="panel-body">

                                        <div class="example-box-wrapper">
                                            <div class="tabs">
                                                <ul>
                                                    <li><a href="#normal-tabs-1" title="Tab 1">Manage</a></li>
                                                    <li><a href="#normal-tabs-2" title="Tab 2">Add</a></li>
                                                </ul>
                                                <div id="normal-tabs-1">

                                                    <span class="form_result">&nbsp; <br>
                                                    </span>

                                                    <div class="panel-body">
                                                        <h3 class="title-hero">List of Offers | Nailspa</h3>
                                                        <div class="example-box-wrapper">
                                                            <table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                                                                <thead>
                                                                    <tr>

                                                                        <th>Offer Name</th>
                                                                        <th>Offer Code</th>
                                                                        <th>Date & Time</th>
                                                                        <th>Status</th>
                                                                        <th>Actions</th>
                                                                    </tr>
                                                                </thead>
                                                                <tfoot>
                                                                    <tr>

                                                                        <th>Offer Name</th>
                                                                        <th>Offer Code</th>
                                                                        <th>Date & Time</th>
                                                                        <th>Status</th>
                                                                        <th>Actions</th>
                                                                    </tr>
                                                                </tfoot>
                                                                <tbody>
                                                                    <?php
// Create connection And Write Values
                                                                    $DB = Connect();
                                                                    $sql = "Select * FROM product_offers WHERE status=1 order by id desc";
                                                                    $RS = $DB->query($sql);
                                                                    if ($RS->num_rows > 0) {
                                                                        $counter = 0;

                                                                        while ($row = $RS->fetch_assoc()) {
                                                                            $counter ++;
                                                                            $strOfferID = $row["id"];
                                                                            $getUID = EncodeQ($strOfferID);
                                                                            $getUIDDelete = Encode($strOfferID);
                                                                            $OfferName = $row["OfferName"];
                                                                            $OfferDateFrom = $row["OfferDateFrom"];
                                                                            $OfferDateTo = $row["OfferDateTo"];
                                                                            $Status = $row["Status"];

                                                                            $Status = "Live";
                                                                            ?>	
                                                                            <tr id="my_data_tr_<?= $counter ?>">
                                                                                <td><?= $OfferName ?></td>
                                                                                <td><?= $row["OfferCode"] ?></td>
                                                                                <td><b>From :</b> <?= $OfferDateFrom ?> <br><b>To :</b> <?= $OfferDateTo ?></td>
                                                                                <td><?= $Status ?></td>
                                                                                <td>
                                                                                    <a class="btn btn-link" href="<?= $strMyActionPage ?>?uid=<?= $getUID ?>">Edit</a>
                                                                                    <a class="btn btn-link font-red" font-redhref="javascript:;" onclick="DeleteData('StepPdtOff', '<?= $strOfferID ?>', 'Are you sure you want to delete this Offer - <?= $OfferName ?>?', 'my_data_tr_<?= $counter ?>');">Delete</a>

                                                                                    <br>
                                                                                </td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                    } else {
                                                                        ?>	
                                                                        <tr>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td>No Records Found</td>
                                                                            <td></td>
                                                                            
                                                                            <td></td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    $DB->close();
                                                                    ?>
                                                                    <!--TAB 2 START-->											

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="normal-tabs-2">

                                                    <div class="panel-body">

                                                        <form role="form" enctype="multipart/form-data" class="form-horizontal bordered-row enquiry_form" onSubmit="proceed_formsubmit('.enquiry_form', '<?= $strMyActionPage ?>', '.result_message', '', '', '', '.file_upload');
                                                                return false;">

                                                            <span class="result_message">&nbsp; <br>
                                                            </span>

                                                            <input type="hidden" name="step" value="add">

                                                            <h3 class="title-hero">New Offer</h3>
                                                            <script>
                                                                $(function () {
                                                                    $("#OfferDateTo").datepicker({minDate: 0});
                                                                    $("#OfferDateFrom").datepicker({minDate: 0});
                                                                });
                                                            </script>
                                                            <div class="example-box-wrapper">


                                                                <div class="form-group"><label class="col-sm-3 control-label">Offer Name </label>
                                                                    <div class="col-sm-4"><input type="text" name="OfferName"  class="form-control required" placeholder="Offer Name"></div>
                                                                </div>



                                                                <div class="form-group"><label class="col-sm-3 control-label">Offer Code<span>*</span></label>
                                                                    <div class="col-sm-4"><input type="text" name="OfferCode"  class="form-control required" placeholder="Offer Code"></div>
                                                                </div>

                                                                <div class="form-group"><label class="col-sm-3 control-label">Offer Date From<span>*</span></label>
                                                                    <div class="col-md-4">
                                                                        <div class="input-prepend input-group"><span class="add-on input-group-addon"><i class="glyph-icon icon-calendar"></i></span> <input type="text" name="OfferDateFrom" id="OfferDateFrom" class="form-control" value="<?php echo date('Y-m-d'); ?>"></div>
                                                                    </div>
                                                                </div>


                                                                <div class="form-group"><label class="col-sm-3 control-label">Offer Date To<span>*</span></label>
                                                                    <div class="col-md-4">
                                                                        <div class="input-prepend input-group"><span class="add-on input-group-addon"><i class="glyph-icon icon-calendar"></i></span> <input type="text" name="OfferDateTo" id="OfferDateTo" class="form-control" value="<?php echo date('Y-m-d'); ?>"></div>
                                                                    </div>
                                                                </div>


<!--                                                                <div class="form-group"><label class="col-sm-3 control-label">Offer Day Limit</label>
                                                                    <div class="col-md-4">
                                                                        <input type="text" pattern="\d*" oninvalid="this.setCustomValidity('Enter Number Only')"
                                                                               oninput="this.setCustomValidity('')"  name="offer_day_limit" class="form-control" placeholder="Offer Day Limit" value="">
                                                                    </div>
                                                                </div>-->

                                                                <div class="form-group"><label class="col-sm-3 control-label">Type<span>*</span></label>
                                                                    <div class="col-sm-4">
                                                                        <select class="form-control required"  name="Type" id="Type" >
                                                                            <option value="">--Select--</option>
                                                                            <option value="1">Amount</option>
                                                                            <option value="2">%</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group"><label class="col-sm-3 control-label">Type(Amount or %)<span>*</span></label>
                                                                    <div class="col-sm-4">
                                                                        <input type="text" name="OfferAmount" id="<?= str_replace("Type(Amount or %)", "TypeAmount", $row["Field"]) ?>" class="form-control required" placeholder="<?= str_replace("Type(Amount or %)", "TypeAmount", $row["Field"]) ?>" />
                                                                    </div>
                                                                </div>


                                                                <?php
                                                                $sql1 = "SELECT StoreID, StoreName from tblStores where Status=0";
                                                                $DB = Connect();
                                                                $RS2 = $DB->query($sql1);
                                                                if ($RS2->num_rows > 0) {
                                                                    ?>											
                                                                    <div class="form-group"><label class="col-sm-3 control-label">Store Name<span>*</span></label>
                                                                        <div class="col-sm-4">
                                                                            <select class="form-control required"  name="StoreID[]" id="StoreID" multiple style="height:100pt">
                                                                                <option value="" selected>--SELECT STORE--</option>
                                                                                <?php
                                                                                while ($row2 = $RS2->fetch_assoc()) {
                                                                                    $StoreID = $row2["StoreID"];
                                                                                    $StoreName = $row2["StoreName"];
                                                                                    ?>
                                                                                    <option value="<?= $StoreID ?>" ><?= $StoreName ?></option>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                            </select>
                                                                            <?php
                                                                        }
                                                                        $DB->close();
                                                                        ?>
                                                                    </div>
                                                                </div>


                                                                <?php
                                                                $sql1 = "SELECT ProductID, ProductUniqueCode,ProductName from tblNewProducts where Status=0";
                                                                $DB = Connect();
                                                                $RS2 = $DB->query($sql1);
                                                                if ($RS2->num_rows > 0) {
                                                                    ?>											
                                                                    <div class="form-group"><label class="col-sm-3 control-label">Product Name<span>*</span></label>
                                                                        <div class="col-sm-4">
                                                                            <select class="form-control required"  name="ProductID[]" id="ProductID" multiple style="height:100pt">
                                                                                <option value="" selected>--SELECT PRODUCT--</option>
                                                                                <?php
                                                                                while ($row2 = $RS2->fetch_assoc()) {
                                                                                    $ProductID = $row2["ProductID"];
                                                                                    $ProductName = $row2["ProductName"] . ' (' . $row2["ProductName"] . ')';
                                                                                    ?>
                                                                                    <option value="<?= $ProductID ?>" ><?= $ProductName ?></option>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                            </select>
                                                                            <?php
                                                                        }
                                                                        $DB->close();
                                                                        ?>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                                    <input type="submit" class="btn ra-100 btn-primary" value="Submit">

                                                                    <div class="col-sm-1"><a class="btn ra-100 btn-black-opacity" href="javascript:;" onclick="ClearInfo('enquiry_form');" title="Clear"><span>Clear</span></a></div>
                                                                </div>

                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        } // End null condition
                        else {
                            ?>			

                            <div class="panel">
                                <div class="panel-body">
                                    <div class="fa-hover">	
                                        <a class="btn btn-primary btn-lg btn-block" href="<?= $strMyActionPage ?>"><i class="fa fa-backward"></i> &nbsp; Go back to <?= $strPageTitle ?></a>
                                    </div>

                                    <div class="panel-body">
                                        <form role="form" enctype="multipart/form-data" class="form-horizontal bordered-row enquiry_form" onSubmit="proceed_formsubmit('.enquiry_form', '<?= $strMyActionPage ?>', '.result_message', '', '', '', '.file_upload');
                                                return false;">

                                            <span class="result_message">&nbsp; <br>
                                            </span>
                                            <br>
                                            <input type="hidden" name="step" value="edit">


                                            <h3 class="title-hero">Edit POST</h3>
                                            <div class="example-box-wrapper">
                                                <script>
                                                    $(function () {
                                                        $("#OfferDateTo").datepicker({minDate: 0});
                                                        $("#OfferDateFrom").datepicker({minDate: 0});
                                                    });

                                                </script>	
                                                <?php
                                                $strID = DecodeQ(Filter($_GET["uid"]));
                                                $DB = Connect();
                                                $sql = "select * FROM product_offers where id = '$strID'";
                                                $RS = $DB->query($sql);
                                                if ($RS->num_rows > 0) {


                                                    while ($row = $RS->fetch_assoc()) {
                                                        $edit_data = $row;
                                                    }
                                                }
                                                ?>	
                                                <div class="example-box-wrapper">
                                                    <input type="hidden" name="OfferID" value="<?php echo isset($edit_data['id']) ? $edit_data['id'] : '0' ?>"/>

                                                    <div class="form-group"><label class="col-sm-3 control-label">Offer Name </label>
                                                        <div class="col-sm-4"><input type="text" name="OfferName"  class="form-control required" placeholder="Offer Name" value="<?php echo isset($edit_data['OfferName']) ? $edit_data['OfferName'] : '' ?>"></div>
                                                    </div>



                                                    <div class="form-group"><label class="col-sm-3 control-label">Offer Code<span>*</span></label>
                                                        <div class="col-sm-4"><input type="text" name="OfferCode"  readonly="readonly" class="form-control required" placeholder="Offer Code" value="<?php echo isset($edit_data['OfferCode']) ? $edit_data['OfferCode'] : '' ?>"></div>
                                                    </div>

                                                    <div class="form-group"><label class="col-sm-3 control-label">Offer Date From<span>*</span></label>
                                                        <div class="col-md-4">
                                                            <div class="input-prepend input-group"><span class="add-on input-group-addon"><i class="glyph-icon icon-calendar"></i></span> <input type="text" name="OfferDateFrom" id="OfferDateFrom" class="form-control" value="<?php echo isset($edit_data['OfferDateFrom']) && $edit_data['OfferDateFrom'] != '0000-00-00' && $edit_data['OfferDateFrom'] != '1970-01-01' ? date('m/d/Y', strtotime($edit_data['OfferDateFrom'])) : ''; ?>"></div>
                                                        </div>
                                                    </div>


                                                    <div class="form-group"><label class="col-sm-3 control-label">Offer Date To<span>*</span></label>
                                                        <div class="col-md-4">
                                                            <div class="input-prepend input-group"><span class="add-on input-group-addon"><i class="glyph-icon icon-calendar"></i></span> <input type="text" name="OfferDateTo" id="OfferDateTo" class="form-control" value="<?php echo isset($edit_data['OfferDateTo']) && $edit_data['OfferDateTo'] != '0000-00-00' && $edit_data['OfferDateTo'] != '1970-01-01' ? date('m/d/Y', strtotime($edit_data['OfferDateTo'])) : ''; ?>"></div>
                                                        </div>
                                                    </div>


<!--                                                    <div class="form-group"><label class="col-sm-3 control-label">Offer Day Limit</label>
                                                        <div class="col-md-4">
                                                            <input type="text" pattern="\d*" oninvalid="this.setCustomValidity('Enter Number Only')"
                                                                   oninput="this.setCustomValidity('')"  name="offer_day_limit" class="form-control" placeholder="Offer Day Limit" value="<?php echo isset($edit_data['offer_day_limit']) ? $edit_data['offer_day_limit'] : '' ?>">
                                                        </div>
                                                    </div>-->

                                                    <div class="form-group"><label class="col-sm-3 control-label">Type<span>*</span></label>
                                                        <div class="col-sm-4">
                                                            <select class="form-control required"  name="Type" id="Type" >
                                                                <option value="">--Select--</option>
                                                                <option value="1" <?php echo isset($edit_data['Type']) && $edit_data['Type'] == 1 ? 'selected' : ''; ?>>Amount</option>
                                                                <option value="2" <?php echo isset($edit_data['Type']) && $edit_data['Type'] == 2 ? 'selected' : ''; ?>>%</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group"><label class="col-sm-3 control-label">Type(Amount or %)<span>*</span></label>
                                                        <div class="col-sm-4">
                                                            <input type="text" name="OfferAmount" id="<?= str_replace("Type(Amount or %)", "TypeAmount", $row["Field"]) ?>" class="form-control required" placeholder="TypeAmount" value="<?php echo isset($edit_data['OfferAmount']) ? $edit_data['OfferAmount'] : '' ?>"/>
                                                        </div>
                                                    </div>


                                                    <?php
                                                    $sql1 = "SELECT StoreID, StoreName from tblStores where Status=0";
                                                    $DB = Connect();
                                                    $RS2 = $DB->query($sql1);
                                                    /*
                                                     * Get offer stores
                                                     */
                                                    $offer_stores = select("store_id", "product_offers_stores", "status=1 AND offer_id='" . $edit_data['id'] . "'");
                                                    if (isset($offer_stores) && is_array($offer_stores) && count($offer_stores) > 0) {
                                                        foreach ($offer_stores as $oskey => $osvalue) {
                                                            $offer_store_id[$osvalue['store_id']] = $osvalue['store_id'];
                                                        }
                                                    }
                                                    if ($RS2->num_rows > 0) {
                                                        ?>											
                                                        <div class="form-group"><label class="col-sm-3 control-label">Store Name<span>*</span></label>
                                                            <div class="col-sm-4">
                                                                <select class="form-control required"  name="StoreID[]" id="StoreID" multiple style="height:100pt">
                                                                    <option value="" selected>--SELECT STORE--</option>
                                                                    <?php
                                                                    while ($row2 = $RS2->fetch_assoc()) {
                                                                        $StoreID = $row2["StoreID"];
                                                                        $StoreName = $row2["StoreName"];
                                                                        ?>
                                                                        <option value="<?= $StoreID ?>" 
                                                                                <?php echo isset($offer_store_id[$StoreID]) ? 'selected' : '' ?>><?= $StoreName ?></option>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                </select>
                                                                <?php
                                                            }
                                                            $DB->close();
                                                            ?>
                                                        </div>
                                                    </div>


                                                    <?php
                                                    $sql1 = "SELECT ProductID, ProductUniqueCode,ProductName from tblNewProducts where Status=0";
                                                    $DB = Connect();
                                                    $RS2 = $DB->query($sql1);

                                                    /*
                                                     * Get offer Products
                                                     */
                                                    $offer_products = select("product_id", "product_offers_product_id", "status=1  AND offer_id='" . $edit_data['id'] . "'");
                                                    if (isset($offer_products) && is_array($offer_products) && count($offer_products) > 0) {
                                                        foreach ($offer_products as $opkey => $opvalue) {
                                                            $offer_product_id[$opvalue['product_id']] = $opvalue['product_id'];
                                                        }
                                                    }
                                                    if ($RS2->num_rows > 0) {
                                                        ?>											
                                                        <div class="form-group"><label class="col-sm-3 control-label">Product Name<span>*</span></label>
                                                            <div class="col-sm-4">
                                                                <select class="form-control required"  name="ProductID[]" id="ProductID" multiple style="height:100pt">
                                                                    <option value="" selected>--SELECT PRODUCT--</option>
                                                                    <?php
                                                                    while ($row2 = $RS2->fetch_assoc()) {
                                                                        $ProductID = $row2["ProductID"];
                                                                        $ProductName = $row2["ProductName"] . ' (' . $row2["ProductName"] . ')';
                                                                        ?>
                                                                        <option value="<?= $ProductID ?>" 
                                                                                <?php echo isset($offer_product_id[$ProductID]) ? 'selected' : '' ?>><?= $ProductName ?></option>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                </select>
                                                                <?php
                                                            }
                                                            $DB->close();
                                                            ?>
                                                        </div>
                                                    </div>

                                                    <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                        <input type="submit" class="btn ra-100 btn-primary" value="Submit">

                                                        <div class="col-sm-1"><a class="btn ra-100 btn-black-opacity" href="javascript:;" onclick="ClearInfo('enquiry_form');" title="Clear"><span>Clear</span></a></div>
                                                    </div>

                                                </div>




                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>			
                            <?php
                        }
                        ?>	                   
                    </div>
                </div>
            </div>

            <?php require_once 'incFooter.fya'; ?>

        </div>
        <script type="text/javascript" src="assets/widgets/chosen/chosen.js"></script>
        <link rel="stylesheet" type="text/css" href="assets/widgets/chosen/chosen.css">
        <script>
                                                        function selectService() {
                                                            if ($('#sel_all_ser').is(':checked')) {
                                                                // $('.jqr_service_div option').attr('selected', 'selected');
                                                                //$('.jqr_service_div option').attr('style', 'background-color:#D3D3D3;');
                                                                $('.jqr_service_div option').prop('selected', true);
                                                                $('#sel_all_ser').trigger('liszt:updated');
                                                            } else {
                                                                //$('.jqr_service_div option').removeAttr('selected');
                                                                //$('.jqr_service_div option').attr('style', 'background-color:#fff;');
                                                                $('.jqr_service_div option').prop('selected', false);
                                                                $('#sel_all_ser').trigger('liszt:updated');
                                                            }
                                                        }
                                                        $(document).ready(function () {

                                                            $(".chosen-select").chosen();

                                                        });

        </script>
    </body>
</html>