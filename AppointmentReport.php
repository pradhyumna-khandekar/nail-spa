<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php require_once 'comparison_report_helper.php'; ?>
<?php
$strPageTitle = "AppointmentReport | Nailspa";
$strDisplayTitle = "Report Appointment Status for Nailspa";
$strMenuID = "2";
$strMyTable = "tblAppointments";
$strMyTableID = "tblAppointments";
$strMyField = "";
$strMyActionPage = "AppointmentReport.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>

        <script type="text/javascript" src="assets/widgets/datepicker/datepicker.js"></script>
        <script type="text/javascript">
            /* Datepicker bootstrap */

            $(function () {
                $("#catt").hide();
                $("#applyfilter").hide();
                "use strict";
                $('.bootstrap-datepicker').bsdatepicker({
                    format: 'mm-dd-yyyy'
                });

            });
        </script> 
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker-demo.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/moment.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");      ?>
            <!----------commented by gandhali 5/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>


                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                        </div>


                        <div class="panel">
                            <div class="panel">

                                <div class="panel-body">


                                    <div id="serviceReportModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog" style="width:52%">
                                        </div>
                                    </div>

                                    <div class="example-box-wrapper">
                                        <div class="tabs">

                                            <div id="normal-tabs-1">

                                                <span class="form_result">&nbsp; <br>
                                                </span>

                                                <div class="panel-body">
                                                    <h3 class="title-hero">List of all Appointments Status</h3>
                                                    <?php
                                                    if (isset($_GET["toandfrom"])) {
                                                        $strtoandfrom = $_GET["toandfrom"];
                                                        $arraytofrom = explode("-", $strtoandfrom);

                                                        $from = $arraytofrom[0];
                                                        $datetime = new DateTime($from);
                                                        $getfrom = $datetime->format('Y-m-d');


                                                        $to = $arraytofrom[1];
                                                        $datetime = new DateTime($to);
                                                        $getto = $datetime->format('Y-m-d');
                                                        $Store = isset($_GET['Store']) ? $_GET['Store'] : 0;
                                                        $source = isset($_GET['source']) ? $_GET['source'] : 0;
                                                        $date_filter = array('from_date' => $getfrom, 'to_date' => $getto, 'source' => $source);
                                                        $sale_data = Report_sale('', $Store, $date_filter);
                                                    }
                                                    ?>

                                                    <form method="get" class="form-horizontal bordered-row" role="form">

                                                        <div class="form-group"><label for="" class="col-sm-4 control-label">Select date</label>
                                                            <div class="col-sm-4">
                                                                <div class="input-prepend input-group">
                                                                    <span class="add-on input-group-addon">
                                                                        <i class="glyph-icon icon-calendar"></i>
                                                                    </span> 
                                                                    <input autocomplete="off" type="text" name="toandfrom" id="daterangepicker-example" class="form-control" value="<?= $strtoandfrom ?>">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Select Store</label>
                                                            <div class="col-sm-4">
                                                                <select name="Store" class="form-control">
                                                                    <option value="0">All</option>
                                                                    <?php
                                                                    $selp = select("*", "tblStores", "StoreID > '0'");
                                                                    foreach ($selp as $val) {
                                                                        $store_nameArr[$val["StoreID"]] = $val["StoreName"];
                                                                        $strStoreName = $val["StoreName"];
                                                                        $strStoreID = $val["StoreID"];
                                                                        $store = $_GET["Store"];
                                                                        if ($store == $strStoreID) {
                                                                            ?>
                                                                            <option  selected value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                            <option value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Select Appointment Source</label>
                                                            <div class="col-sm-4">
                                                                <select name="source" class="form-control">
                                                                    <option value="0">All</option>
                                                                    <?php
                                                                    $sel_source = select("*", "tblsource", "status = '1'");
                                                                    foreach ($sel_source as $val) {
                                                                        ?>
                                                                        <option value="<?= $val['id']; ?>" <?php echo isset($_GET['source']) && $_GET['source'] == $val['id'] ? 'selected' : ''; ?>><?= $val['name'] ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                            <button type="submit"  class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                                            &nbsp;&nbsp;&nbsp;
                                                            <a class="btn btn-link" href="AppointmentReport.php">Clear All Filter</a>
                                                        </div>
                                                    </form>

                                                    <br>

                                                    <?php
                                                    if (!empty($_GET["Store"])) {
                                                        $storrrp = $store_nameArr[$_GET["Store"]];
                                                    } else {
                                                        $storrrp = "All";
                                                    }
                                                    ?>
                                                    <div id="kakkabiryani">
                                                        <?php if (isset($_GET["toandfrom"]) && $_GET["toandfrom"] != '') { ?>
                                                            <h3 class="title-hero">Date Range selected : FROM - <?= $getfrom ?> / TO - <?= $getto ?> / Store Filter selected : <?= $storrrp ?></h3>

                                                            <br>
                                                        <?php } ?>
                                                        <div class="panel">
                                                            <div class="panel-body">
                                                                <div class="example-box-wrapper">
                                                                    <div class="scroll-columns">

                                                                        <?php
                                                                        $DB = Connect();
                                                                        $appendq = '';
                                                                        if (isset($_GET["toandfrom"]) && $_GET["toandfrom"] != '') {
                                                                            $strtoandfrom = $_GET["toandfrom"];
                                                                            $arraytofrom = explode("-", $strtoandfrom);

                                                                            $from = $arraytofrom[0];
                                                                            $datetime = new DateTime($from);
                                                                            $getfrom = $datetime->format('Y-m-d');


                                                                            $to = $arraytofrom[1];
                                                                            $datetime = new DateTime($to);
                                                                            $getto = $datetime->format('Y-m-d');

                                                                            if (!IsNull($from)) {
                                                                                $appendq .= " Where Date(tblAppointments.AppointmentDate)>=Date('" . $getfrom . "')";
                                                                            }

                                                                            if (!IsNull($to)) {
                                                                                $appendq .= " and Date(tblAppointments.AppointmentDate)<=Date('" . $getto . "')";
                                                                            }


                                                                            if (!empty($_GET["Store"])) {
                                                                                $appendq .= " AND tblAppointments.StoreID='" . $_GET["Store"] . "'";
                                                                                $storrrp = $store_nameArr[$_GET["Store"]];
                                                                            } else {
                                                                                $storrrp = "All";
                                                                            }

                                                                            if (!empty($_GET["source"])) {
                                                                                $appendq .= " AND tblAppointments.Source='" . $_GET["source"] . "'";
                                                                            }

                                                                            $qry = "SELECT AppointmentID,Status,StoreID,appointment_type FROM tblAppointments " . $appendq . " AND IsDeleted=0";

                                                                            $qry_exe = $DB->query($qry);
                                                                            if ($qry_exe->num_rows > 0) {
                                                                                while ($result = $qry_exe->fetch_assoc()) {
                                                                                    $final_result[] = $result;
                                                                                }
                                                                            }

                                                                            if (isset($final_result) && is_array($final_result) && count($final_result) > 0) {
                                                                                foreach ($final_result as $key => $value) {
                                                                                    if (isset($count_data[$value['StoreID']][$value['Status']])) {
                                                                                        $count_data[$value['StoreID']][$value['Status']] += 1;
                                                                                    } else {
                                                                                        $count_data[$value['StoreID']][$value['Status']] = 1;
                                                                                    }




                                                                                    if ($value['appointment_type'] == 2) {
                                                                                        if (isset($count_data[$value['StoreID']]['total_retail'])) {
                                                                                            $count_data[$value['StoreID']]['total_retail'] += 1;
                                                                                        } else {
                                                                                            $count_data[$value['StoreID']]['total_retail'] = 1;
                                                                                        }
                                                                                    } else {
                                                                                        if (isset($count_data[$value['StoreID']]['total_service'])) {
                                                                                            $count_data[$value['StoreID']]['total_service'] += 1;
                                                                                        } else {
                                                                                            $count_data[$value['StoreID']]['total_service'] = 1;
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }

                                                                            $DB->close();

                                                                            if (isset($count_data) && is_array($count_data) && count($count_data) > 0) {
                                                                                ?>
                                                                                <table  class="table table-bordered table-striped table-condensed cf table-hover" width="100%">
                                                                                    <thead class="cf">
                                                                                        <tr>
                                                                                            <th class="numeric">Store</th>
                                                                                            <th class="numeric">Total Service Appointment</th>
                                                                                            <th class="numeric">Service Amount</th>
        <!--                                                                                            <th class="numeric">Total Retails Appointment</th>-->
                                                                                            <th class="numeric">Upcoming</th>
                                                                                            <th class="numeric">In Progress</th>
                                                                                            <th class="numeric">Done</th>
                                                                                            <th class="numeric">Cancelled</th>
                                                                                            <th class="numeric">Delayed</th>
                                                                                            <th class="numeric">Rescheduled</th>
                                                                                        </tr>
                                                                                    </thead>



                                                                                    <tbody>
                                                                                        <?php
                                                                                        $total_service = 0;
                                                                                        $total_sale = 0;
                                                                                        $upcoming_grand = 0;
                                                                                        $progress_grand = 0;
                                                                                        $done_grand = 0;
                                                                                        $cancel_grand = 0;
                                                                                        $delay_grand = 0;
                                                                                        $reschedule_grand = 0;
                                                                                        foreach ($count_data as $key => $value) {
                                                                                            if (isset($store_nameArr[$key])) {
                                                                                                ?>

                                                                                                <tr>
                                                                                                    <td><?php echo ucwords($store_nameArr[$key]); ?></td>
                                                                                                    <td><?php
                                                                                                        $service_amt = isset($value['total_service']) ? $value['total_service'] : 0;
                                                                                                        $total_service = $total_service + $service_amt;
                                                                                                        echo $service_amt;
                                                                                                        ?></td>
                                                                                                    <td><?php
                                                                                                        $sales_data = isset($sale_data[$key]) ? $sale_data[$key] : 0;
                                                                                                        $total_sale = $total_sale + $sales_data;
                                                                                                        echo $sales_data;
                                                                                                        ?></td>
                <!--                                                                                                    <td><?php echo isset($value['total_retail']) ? $value['total_retail'] : 0; ?></td>-->
                                                                                                    <td><?php
                                                                                                        $upcoming_total = isset($value[0]) ? $value[0] : 0;
                                                                                                        $upcoming_grand = $upcoming_grand + $upcoming_total;
                                                                                                        echo $upcoming_total;
                                                                                                        ?></td>
                                                                                                    <td><?php
                                                                                                        $progress_total = isset($value[1]) ? $value[1] : 0;
                                                                                                        $progress_grand = $progress_grand + $progress_total;
                                                                                                        echo $progress_total;
                                                                                                        ?></td>
                                                                                                    <td><?php
                                                                                                        $done_total = isset($value[2]) ? $value[2] : 0;
                                                                                                        $done_grand = $done_grand + $done_total;
                                                                                                        echo $done_total;
                                                                                                        ?></td>
                                                                                                    <td><?php
                                                                                                        $cancel_total = isset($value[3]) ? $value[3] : 0;
                                                                                                        $cancel_grand = $cancel_grand + $cancel_total;
                                                                                                        echo $cancel_total;
                                                                                                        ?></td>
                                                                                                    <td><?php
                                                                                                        $delay_total = isset($value[5]) ? $value[5] : 0;
                                                                                                        $delay_grand = $delay_grand + $delay_total;
                                                                                                        echo $delay_total;
                                                                                                        ?></td>
                                                                                                    <td><?php
                                                                                                        $reschedule_total = isset($value[6]) ? $value[6] : 0;
                                                                                                        $reschedule_grand = $reschedule_grand + $reschedule_total;
                                                                                                        echo $reschedule_total;
                                                                                                        ?></td>

                                                                                                </tr>
                                                                                                <?php
                                                                                            }
                                                                                        }
                                                                                        ?>
                                                                                        <tr>
                                                                                            <td><b>Total</b></td>
                                                                                            <td><b><?php echo $total_service; ?></b></td>
                                                                                            <td><b><?php echo $total_sale; ?></b></td>
                                                                                            <td><b><?php echo $upcoming_grand; ?></b></td>
                                                                                            <td><b><?php echo $progress_grand; ?></b></td>
                                                                                            <td><b><?php echo $done_grand; ?></b></td>
                                                                                            <td><b><?php echo $cancel_grand; ?></b></td>
                                                                                            <td><b><?php echo $delay_grand; ?></b></td>
                                                                                            <td><b><?php echo $reschedule_grand; ?></b></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <?php
                                                                            } else {
                                                                                echo "<br><center><h3>No Appointment Found...</h3></center>";
                                                                            }
                                                                        } else {
                                                                            echo "<br><center><h3>Please Select Month And Year!</h3></center>";
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php require_once 'incFooter.fya'; ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
