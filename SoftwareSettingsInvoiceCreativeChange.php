<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>





<?php
	$strPageTitle = "Settings | Nailspa POS";
	$strDisplayTitle = "Manage Settings for Nailspa POS";
	$strMenuID = "2";
	$strMyTable = "tblSettings";
	$strMyTableID = "SettingID";
	$strMyField = "";
	$strMyActionPage = "SoftwareSettingsInvoiceCreativeChange.php";
	$strMessage = "";
	$sqlColumn = "";
	$sqlColumnValues = "";


	if ($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$strStep = Filter($_POST["step"]);


		if($strStep=="edit")
		{

			$DB = Connect();
			
			
			if(isset($_FILES['InvoiceImgURL'])){
			$errors= array();
			$file_name = $_FILES['InvoiceImgURL']['name'];
			$file_size =$_FILES['InvoiceImgURL']['size'];
			$file_tmp =$_FILES['InvoiceImgURL']['tmp_name'];
			$file_type=$_FILES['InvoiceImgURL']['type'];
			$file_ext=strtolower(end(explode('.',$_FILES['InvoiceImgURL']['name'])));

			$expensions= array("jpeg","jpg","png");

				if(in_array($file_ext,$expensions)=== false){
					$errors[]="extension not allowed, please choose a JPEG or PNG file.";
				}

				if($file_size > 500000){
					$errors[]='File size must be less then 500 KB';
				}

				if(empty($errors)==true)
				{
					$uploadFilename = UniqueStamp().$file_name;	
					move_uploaded_file($file_tmp,"images/invoice/".$uploadFilename);
					
					$sqlUpdatepath = "update $strMyTable set InvoiceImgURL ='".$uploadFilename."'";
					ExecuteNQ($sqlUpdatepath);
					
					echo ("<script>location.href='SoftwareSetting.php';</script>");
				}
				else
				{
					print_r($errors);
					die();
				}
			}
		
			
			$DB->close();	
					
		}
		die();
	}	
?>


<!DOCTYPE html>
<html lang="en">

<head>
	<?php require_once("incMetaScript.fya"); ?>
</head>

<body>
    <div id="sb-site">
        
		<?php require_once("incOpenLayout.fya"); ?>
		
        <?php require_once("incLoader.fya"); ?>
		
        <div id="page-wrapper">
            <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>
            
				<?php require_once("incLeftMenu.fya"); ?>
			
            <div id="page-content-wrapper">
                <div id="page-content">
                    
					<?php require_once("incHeader.fya"); ?>
					

                    <div id="page-title">
                        <h2><?=$strDisplayTitle?></h2>
                        <!--<p>Add, Edit, Delete Admins and manage them accordingly.</p>-->
                    </div>
<?php

if(!isset($_GET["uid"]))
{
?>								
                    <div class="panel">
						<div class="panel">
							<div class="panel-body">
								<div class="example-box-wrapper">
									<div class="tabs">
										<ul>
											<li><a href="#normal-tabs-1" title="Tab 1">Manage</a></li>
										</ul>
										<div id="normal-tabs-1">
										
											<span class="form_result">&nbsp; <br>
											</span>
											
											<div class="panel-body">
												<h3 class="title-hero">You can change the Invoice creative...</h3>
												
<script>
	function upload_image()
	{
		if( document.getElementById("fileSelect").files.length == 0 ){
			alert("Please select valid image");
			return false;
		}
		else
		{
			saif_usmani.submit();
		}
	}
	
	
</script>												
												<form role="form" method="POST" action="<?=$strMyActionPage?>" name="saif_usmani" enctype="multipart/form-data" class="form-horizontal bordered-row enquiry_form" onSubmit="upload_image(); return false;">
													<input type="hidden" name="step" value="edit">

													

														<div class="example-box-wrapper">
															
					<?php
							$strImageInvoiceURL=select("*","tblSettings","SettingID='1'");
							$ImageInvoiceURL = $strImageInvoiceURL[0]['InvoiceImgURL'];
					?>
					
																<input type="hidden" name="SettingID" value="<?=Encode('1')?>">	
																<div class="form-group"><label class="col-sm-3 control-label">Creative Image Invoice <span></span></label>
																	<div class="col-sm-6">
																		<input class="imageupload file_upload" type="file" data-source="InvoiceImgURL" name="InvoiceImgURL" id="fileSelect">
																		<span><font color="red">Click to change the Invoice creative image size refrence 900 * 399</font></span>
																	</div>
																</div>
																<div class="form-group"><label class="col-sm-3 control-label"></label>
																	<input type="submit" class="btn ra-100 btn-primary" value="Update Image">
																</div>
																
																<img src="images/invoice/<?php echo $ImageInvoiceURL; ?>" alt="Creative image not uploaded properly. Please re-upload" width="600px" height="250px" />

														</div>
												</form>
												
												
												
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
                    </div>
<?php
} // End null condition
else
{


}
?>	                   
            </div>
        </div>
		
        <?php require_once 'incFooter.fya'; ?>
		
    </div>
</body>

</html>