<?php require_once("setting.fya"); ?>
<?php

function add_appointment($post_data = null) {
    file_put_contents('apt_post_data2', serialize($post_data));
    $DB = Connect();
//    $post_data = array(
//        'fname' => 'Test',
//        'lname' => 'API3',
//        'email' => 'john@gmail.com',
//        'mobile' => '1122334455',
//        'gender' => 1,
//        'cust_location' => 'Churchgate',
//        'store_name' => 'Khar',
//        'store_id' => '2',
//        'appointment_date' => '2019-05-07',
//        'appointment_time' => '19:00',
//        'paid_amount' => '500',
//        'services' => array(
//            '0' => array(
//                'name' => 'Nailspa',
//                'quantity' => 2,
//                'service_id' => '200'
//            )
//        )
//    );

    /*
     * save payment details in database.  
     * 
     */
    $appointment_id = '';
    $name = $post_data['firstname'] . $post_data['lastname'];
    $phone = $post_data['phone'];
    $email = $post_data['email'];
    $amount = $post_data['amount'];
    $service_code = $post_data['udf4'];
    $store_code = $post_data['udf3'];
    $status = $post_data['status'];
    $payment_id = $post_data['payuMoneyId'];
    $txnid = $post_data['txnid'];
    $payment_date = date('Y-m-d H:i:s', strtotime($post_data['addedon']));
    $source = $post_data['udf2'];
    $error_number = $post_data['error'];
    $error_message = $post_data['error_Message'];
    $service_provider = isset($post_data['service_provider']) ? $post_data['service_provider'] : '';
    $log_data = serialize($post_data);
    $created_date = date('Y-m-d H:i:s');

    $payment_query = "INSERT INTO `tbl_online_payment_info` (`id`, `appointment_id`,"
            . " `name`, `contact`, `email`, `amount`, `service_code`, `store`, "
            . "`source`, `payment_id`, `txnid`, `payment_date`, `payment_status`, "
            . "`error`, `error_message`, `created_date`,`service_provider`,`data`)"
            . " VALUES ('', '" . $appointment_id . "', '" . $name . "', '" . $phone . "', '" . $email . "', '" . $amount . "', '" . $service_code . "',"
            . " '" . $store_code . "', '" . $source . "', '" . $payment_id . "', '" . $txnid . "', '" . $payment_date . "', '" . $status . "',"
            . " '" . $error_number . "', '" . $error_message . "', '" . $created_date . "','" . $service_provider . "','" . $log_data . "');";
    $DB->query($payment_query);
    $paymentdetails_id = $DB->insert_id;



    if (isset($post_data) && is_array($post_data) && count($post_data) > 0) {
        $mobile_no = isset($post_data['phone']) ? trim($post_data['phone']) : '';

        if ($mobile_no != '') {
            /*
             * Check Customer Exist Or Not
             */
            $custexist_q = "Select * from tblCustomers Where SUBSTRING(CustomerMobileNo , -10) LIKE '" . substr(trim($mobile_no), -10) . "'";
            //$custexist_q = "Select * from tblCustomers Where CustomerMobileNo LIKE '%" . $mobile_no . "%'";
            $custexist_exe = $DB->query($custexist_q);
            if ($custexist_exe->num_rows > 0) {
                $cust_data = $custexist_exe->fetch_assoc();
                $strCustomerID = $cust_data['CustomerID'];
            } else {
                /*
                 * Add Customer
                 */
                $strFirstName = isset($post_data['firstname']) ? $post_data['firstname'] : '';
                $strLastName = isset($post_data['lastname']) ? $post_data['lastname'] : '';
                $strCustomerFullName = $strFirstName . " " . $strLastName;
                $strCustomerEmailID = isset($post_data['email']) ? $post_data['email'] : '';
                $strCustomerMobileNo = isset($post_data['phone']) ? $post_data['phone'] : '';
                $strStatus = 0;
                $strGender = isset($post_data['gender']) ? $post_data['gender'] : '1';
                $CustomerLocation = isset($post_data['cust_location']) ? $post_data['cust_location'] : '';
                $Acquisition = isset($post_data['acquisition']) ? $post_data['acquisition'] : '';

                $sqlInsert = "INSERT INTO tblCustomers (FirstName, LastName, CustomerFullName, CustomerEmailID, CustomerMobileNo, Status,RegDate,Gender,Acquisition,CustomerLocation) VALUES 
					('" . $strFirstName . "', '" . $strLastName . "', '" . $strCustomerFullName . "', '" . $strCustomerEmailID . "', '" . $strCustomerMobileNo . "', '" . $strStatus . "','" . date('Y-m-d H:i:s') . "','" . $strGender . "','" . $Acquisition . "', '" . $CustomerLocation . "')";
                $DB->query($sqlInsert);
                $strCustomerID = $DB->insert_id;
            }


            $strStoreID = isset($post_data['store_id']) ? $post_data['store_id'] : 0;
            $strAppointmentDate1 = isset($post_data['appointment_date']) ? date('Y-m-d', strtotime($post_data['appointment_date'])) : 0;

            $pqr = isset($post_data['appointment_time']) ? date('H:i:s', strtotime($post_data['appointment_time'])) : 0;

            $strAppointmentCheckInTime = '00:00:00';
            $strAppointmentCheckOutTime = '00:00:00';

            if (isset($post_data['offer_code']) && $post_data['offer_code'] == 1 && isset($post_data['udf6']) && $post_data['udf6'] == 2) {
                $strAppointmentOfferID = $post_data['udf7'];
            } else {
                if (isset($post_data['udf6']) && $post_data['udf6'] == 2 && isset($post_data['udf9']) && $post_data['udf9'] > 0) {
                    $strAppointmentOfferID = $post_data['udf7'];
                } else {
                    $strAppointmentOfferID = 0;
                }
            }
            $remark_type = 0;
            $remark_date = 0;
            $app_source = isset($post_data['source_id']) ? $post_data['source_id'] : 0;
            $Type_Service = 0;
            /*
             * ADD Appointment
             */
            $paid_amount = isset($post_data['paid_amount']) ? trim($post_data['paid_amount']) : '0';
            $apt_comment = isset($post_data['apt_comment']) ? trim($post_data['apt_comment']) : '0';

            if (!empty($Type_Service)) {

                if ($Type_Service == '0') {
                    $sqlInsert = "INSERT INTO tblAppointments (CustomerID, StoreID, AppointmentDate,
                        SuitableAppointmentTime, AppointmentCheckInTime, 
                        AppointmentCheckOutTime, offerid, Status,FreeService,
                        PackageID,non_visiting_update_value,non_visiting_update_date,
                        source,created_date,prepaid_amount,comment,pre_service_code) 
                        VALUES('" . $strCustomerID . "', '" . $strStoreID . "', '" . $strAppointmentDate1 . "', '"
                            . "" . $pqr . "', '" . $strAppointmentCheckInTime . "', '" .
                            $strAppointmentCheckOutTime . "', '" . $strAppointmentOfferID . "', '0','0','0','"
                            . "" . $remark_type . "','" . $remark_date . "','" . $app_source . "','"
                            . "" . date('Y-m-d H:i:s') . "','" . $paid_amount . "','" . $apt_comment . "','".$service_code."')";
                } else {
                    $sqlInsert = "INSERT INTO tblAppointments (CustomerID, StoreID, AppointmentDate, SuitableAppointmentTime, AppointmentCheckInTime, AppointmentCheckOutTime, offerid, Status,FreeService,PackageID,non_visiting_update_value,non_visiting_update_date,source,created_date,prepaid_amount,comment,pre_service_code) VALUES 
				('" . $strCustomerID . "', '" . $strStoreID . "', '" . $strAppointmentDate1 . "', '" . $pqr . "', '" . $strAppointmentCheckInTime . "', '" .
                            $strAppointmentCheckOutTime . "', '" . $strAppointmentOfferID . "', '0','1','0','" . $remark_type . "','" . $remark_date . "','" . $app_source . "','" . date('Y-m-d H:i:s') . "','" . $paid_amount . "','" . $apt_comment . "','".$service_code."')";
                }
            } else {
                $sqlInsert = "INSERT INTO tblAppointments (CustomerID, StoreID, AppointmentDate, SuitableAppointmentTime, AppointmentCheckInTime, AppointmentCheckOutTime, offerid, Status,PackageID,non_visiting_update_value,non_visiting_update_date,source,created_date,prepaid_amount,comment,pre_service_code) VALUES 
				('" . $strCustomerID . "', '" . $strStoreID . "', '" . $strAppointmentDate1 . "', '" . $pqr . "', '" . $strAppointmentCheckInTime . "', '" .
                        $strAppointmentCheckOutTime . "', '" . $strAppointmentOfferID . "', '0','0','" . $remark_type . "','" . $remark_date . "','" . $app_source . "','" . date('Y-m-d H:i:s') . "','" . $paid_amount . "','" . $apt_comment . "','".$service_code."')";
            }


            $dateyu = Date('Y-m-d');

            if ($DB->query($sqlInsert) === TRUE) {
                $last_idp = $DB->insert_id;  //last id of tblCustomers insert

                if (isset($post_data['udf6']) && $post_data['udf6'] == 2 && isset($post_data['udf7']) && $post_data['udf7'] != 0) {
                    if (isset($post_data['udf9']) && $post_data['udf9'] > 0) {
                        /*
                         * Offer used
                         */
                        $offer_redempt_query1 = "UPDATE `tblAppointments` SET `website_offer_redempt` =2,`offer_redempt_appointment_id` = '" . $last_idp . "' WHERE `AppointmentID` = '" . $post_data['udf9'] . "';";
                        file_put_contents("qry1", $offer_redempt_query1);
                        $DB->query($offer_redempt_query1);
                    } else {
                        /*
                         * First time offer
                         */
                        if (isset($post_data['offer_code']) && $post_data['offer_code'] == 1) {
                            $offer_redempt_query2 = "UPDATE `tblAppointments` SET `website_offer_redempt` =2,`website_offer_id` = '" . $post_data['udf7'] . "' WHERE `AppointmentID` = '" . $last_idp . "';";
                        } else {
                            $offer_redempt_query2 = "UPDATE `tblAppointments` SET `website_offer_redempt` =1,`website_offer_id` = '" . $post_data['udf7'] . "' WHERE `AppointmentID` = '" . $last_idp . "';";
                        }
                        file_put_contents("qry2", $offer_redempt_query2);
                        $DB->query($offer_redempt_query2);
                    }
                }
                /*
                 * insert appointment id and customer id in online_payment_info table
                 */
                $updt_payment_query = "UPDATE `tbl_online_payment_info` SET `appointment_id` = '" . $last_idp . "' WHERE `tbl_online_payment_info`.`id` = '" . $paymentdetails_id . "'";
                $DB->query($updt_payment_query);


                /* $sepptu = select("*", "tblCustomerMemberShip", "CustomerID='" . $strCustomerID . "'");
                  $enddate = $sepptu[0]['EndDay'];
                  $seldatapq = select("memberid", "tblCustomers", "CustomerID='" . $strCustomerID . "'");
                  $memid = $seldatapq[0]['memberid']; */
                $memid = 0;
            }

            /* if ($dateyu <= $enddate) {
              if ($memid != "") {
              $seldoffert = select("*", "tblAppointments", "AppointmentID='" . $last_idp . "'");

              $FreeService = $seldoffert[0]['FreeService'];
              if ($FreeService == "0") {
              $sqlUpdate1 = "UPDATE tblAppointments SET memberid='" . $memid . "' WHERE AppointmentID='" . $last_idp . "'";
              ExecuteNQ($sqlUpdate1);
              }
              }
              } */


            $strServicesused = isset($post_data["services"]) ? $post_data["services"] : array();

            if (isset($strServicesused) && is_array($strServicesused) && count($strServicesused) > 0) {
                foreach ($strServicesused as $skey => $svalue) {

                    $selpw = select("*", "tblServices", "ServiceCode='" . $svalue['service_code'] . "' and StoreID='" . $strStoreID . "'");
//                    $selpw = select("*", "tblServices", "ServiceCode='Nailoffer' and StoreID='2'");


                    foreach ($selpw as $vapt) {
                        $ServiceID = $vapt['ServiceID'];
                        $ServiceCost = $vapt["ServiceCost"];
                        $categoryselect[$i] = 0;
                        $qty[$i] = $svalue['quantity'];

                        $sqlInsert3 = "INSERT INTO tblAppointmentsDetails (AppointmentID, ServiceID, ServiceAmount,  Status,qty,employeecategory) VALUES 
							('" . $last_idp . "', '" . $ServiceID . "', '" . $ServiceCost . "', '0','" . $qty[$i] . "','" . $categoryselect[$i] . "')";

                        if ($DB->query($sqlInsert3) === TRUE) {
                            $last_id3 = $DB->insert_id;  //last id of tblAppointments insert
                        }

                        $sqlInsert3p = "INSERT INTO tblAppointmentsDetailsInvoice (AppointmentID, ServiceID, ServiceAmount,Status,qty,employeecategory,is_upsell) VALUES 
									('" . $last_idp . "', '" . $ServiceID . "',  '" . $ServiceCost . "','0','" . $qty[$i] . "','" . $categoryselect[$i] . "','2')";

                        if ($DB->query($sqlInsert3p) === TRUE) {
                            $last_id7 = $DB->insert_id;  //last id of tblAppointments insert
                        }


                        if ($FreeService != "0") {
                            $sqlInsertpo = "INSERT INTO tblAppointmentAssignEmployee(AppointmentID, ServiceID, MECID, Commission,Qty,FreeService) VALUES
						('" . $last_idp . "', '" . $ServiceID . "', '0', '0','" . $qty[$i] . "','1')";

                            ExecuteNQ($sqlInsertpo);
                        } else {
                            $sqlInsertpo = "INSERT INTO tblAppointmentAssignEmployee(AppointmentID, ServiceID, MECID, Commission,Qty) VALUES
						('" . $last_idp . "', '" . $ServiceID . "', '0', '0','" . $qty[$i] . "')";

                            ExecuteNQ($sqlInsertpo);
                        }

                        $sqlcharges = "Select ChargeNameId , (select GROUP_CONCAT(distinct ChargeSetID) from tblCharges where 
								ChargeNameID=tblServicesCharges.ChargeNameID) as ArrayChargeSet from tblServicesCharges where ServiceID= '" . $ServiceID . "'";

                        $charges = $DB->query($sqlcharges);

                        if ($charges->num_rows > 0) {
                            while ($row = $charges->fetch_assoc()) {
                                $ArrayChargeSet = $row["ArrayChargeSet"];
                                $strChargeSet = explode(",", $ArrayChargeSet);
                            }
                        }
                    }

                    for ($j = 0; $j < count($strChargeSet); $j++) {
                        $strChargeSetforwork = $strChargeSet[$j];
                        $sqlchargeset = "select SetName, ChargeAmt, ChargeFPType from tblChargeSets where ChargeSetID=$strChargeSetforwork";

                        $RS2 = $DB->query($sqlchargeset);
                        if ($RS2->num_rows > 0) {
                            while ($row2 = $RS2->fetch_assoc()) {
                                $strChargeAmt = $row2["ChargeAmt"];
                                $strSetName = $row2["SetName"];
                                $strChargeFPType = $row2["ChargeFPType"];
// Calculation of charges
                                $ServiceCost = $ServiceCost;
                                if ($strChargeFPType == "0") {
                                    $strChargeAmt = $strChargeAmt;
                                } else {

                                    $percentage = $strChargeAmt;
//echo "percentage=".$percentage."<br/>";
                                    $outof = $ServiceCost;
//echo "ServiceCost=".$ServiceCost."<br/>";
                                    $strChargeAmt = ($percentage / 100) * $outof;
//echo "strChargeAmt=".$strChargeAmt."<br/>";
                                }

                                $totalamt = $strChargeAmt * $qty[$i];
                                $sqlInsertcharges = "INSERT INTO tblAppointmentsCharges(AppointmentDetailsID, ChargeName, ChargeAmount,AppointmentID) VALUES 
									('" . $last_id3 . "', '" . $strSetName . "', '" . $totalamt . "','" . $last_idp . "')";

                                $sqlInsertcharges . "<br/>";
                                ExecuteNQ($sqlInsertcharges);

                                if ($last_id7 != 0) {
                                    $sqlInsertchargesd = "INSERT INTO tblAppointmentsChargesInvoice(AppointmentDetailsID, ChargeName, ChargeAmount,AppointmentID,TaxGVANDM) VALUES 
									('" . $last_id7 . "', '" . $strSetName . "', '" . $totalamt . "','" . $last_idp . "','2')";
                                    $sqlInsertcharges . "<br/>";
                                    ExecuteNQ($sqlInsertchargesd);
                                }
                            }
                        }
                    }
                    unset($strChargeSet);
                }

                unset($strServicesused);
                unset($strChargeSet);

                $selpqitq = select("*", "tblAppointmentsDetailsInvoice", "AppointmentID='" . $last_idp . "'");

                foreach ($selpqitq as $SQ) {
                    $serr[] = $SQ['ServiceID'];
                }

                for ($j = 0; $j < count($serr); $j++) {
                    $counterservices++;
                    $Servicestaken = $serr[$j];

                    $selpqi = select("*", "tblAppointmentAssignEmployee", "AppointmentID='" . $last_idp . "' and ServiceID='" . $Servicestaken . "'");

                    $qtyu = $selpqi[0]['Qty'];
                    if ($qtyu == '1') {
                        $sqlUpdate1 = "UPDATE tblAppointmentAssignEmployee SET QtyParam='" . $qtyu . "' WHERE AppointmentID='" . $last_idp . "' and ServiceID='" . $Servicestaken . "'";
                        ExecuteNQ($sqlUpdate1);
                    } elseif ($qtyu == '0') {
// nothimng 
                    } else {
//echo 11;
                        $sqlUpdate1 = "UPDATE tblAppointmentAssignEmployee SET QtyParam='" . $qtyu . "' WHERE AppointmentID='" . $last_idp . "' and ServiceID='" . $Servicestaken . "'";
//echo $sqlUpdate1;
                        ExecuteNQ($sqlUpdate1);
//UPDATE PARAM 
                        for ($i = 1; $i < $qtyu; $i++) {

                            if ($FreeService != "0") {
                                $sqlInsert3ptr = "INSERT INTO tblAppointmentAssignEmployee(AppointmentID,ServiceID, Qty,QtyParam,MECID,Commission,FreeService) VALUES 
									('" . $last_idp . "', '" . $Servicestaken . "','" . $qtyu . "','" . $i . "','0','0','1')";
                                if ($DB->query($sqlInsert3ptr) === TRUE) {
                                    $last_id50 = $DB->insert_id;  //last id of tblAppointments insert
                                }
                            } else {
//echo $i;
                                $sqlInsert3ptr = "INSERT INTO tblAppointmentAssignEmployee(AppointmentID,ServiceID, Qty,QtyParam,MECID,Commission,FreeService) VALUES 
									('" . $last_idp . "', '" . $Servicestaken . "','" . $qtyu . "','" . $i . "','0','0','0')";
//echo $sqlInsert3ptr;
                                if ($DB->query($sqlInsert3ptr) === TRUE) {
                                    $last_id50 = $DB->insert_id;  //last id of tblAppointments insert
                                }
                            }
                        }
                    }
                }
                unset($strServices);

                $sqlInsert = "INSERT INTO  tblInvoice (AppointmentID, EmailMessageID, DateOfCreation) VALUES ('" . $last_idp . "', 'Null', 'Null')";

                $DB->query($sqlInsert);

                $seldata = select("*", "tblAppointments", "AppointmentID='" . $last_idp . "'");

//print_r($seldata);
                $strCustomerID = $seldata[0]['CustomerID'];
                $strStoreID = $seldata[0]['StoreID'];
                $AppointmentDate = $seldata[0]['AppointmentDate'];
                $timep = $seldata[0]['SuitableAppointmentTime'];
                $strSuitableAppointmentTime = $seldata[0]['SuitableAppointmentTime'];
                $time_in_12_hour_format = date("g:i a", strtotime($timep));
                $store_data = select("*", "tblStores", "StoreID='" . $strStoreID . "'");
                $address = $store_data[0]['StoreOfficialAddress'];
                $storename = $store_data[0]['StoreName'];
                $CustomerMobileNo = isset($post_data['mobile']) ? $post_data['mobile'] : '';

                $weekday = date('l', strtotime($AppointmentDate)); // note: first arg to date() is lower-case L

                $dateforsms = FormatDatetime($time_in_12_hour_format);
                $dateforsms = date('jS M Y', strtotime($AppointmentDate));
                $Timebefore45min = date("H:i:s", strtotime("-45 minutes", strtotime($pqr)));


                $seldatac = select("CustomerEmailID,CustomerFullName,CustomerMobileNo,FirstName", "tblCustomers", "CustomerID='" . $strCustomerID . "'");
                $emailc = $seldatac[0]['CustomerEmailID'];
                $fullname = $seldatac[0]['CustomerFullName'];
                $CustomerMobileNo = $seldatac[0]['CustomerMobileNo'];

                /*
                 * Reminder SMS
                 */
                $SMSContent = "Dear Customer Name, your appnt is scheduled for " . $weekday . " " . $dateforsms . " at " . $strSuitableAppointmentTime . " at " . $storename . ". Pls cancel or reschedule 45mins in advance to avoid cancellation charges.";
                $InsertSMSDetails = "Insert into tblAppointmentsReminderSMS (AppointmentID, StoreID, CustomerID, AppointmentDate, SuitableAppointmentTime,  SMSSendTime, Status, ContentSMS,SendSMSTo)values('" . $last_idp . "','" . $strStoreID . "','" . $strCustomerID . "','" . $AppointmentDate . "','" . $pqr . "','" . $Timebefore45min . "',0,'" . $SMSContent . "', '" . $CustomerMobileNo . "')";
                ExecuteNQ($InsertSMSDetails);

                /*
                 * Immdiate SMS
                 */
                $cust_name = $post_data['firstname'] . " " . $post_data['lastname'];
                $SMSContentforImmediate = "Dear " . ucwords($cust_name) . ", thank you for booking with us online. Our salon manager will reconfirm your appnt over a call soon or call (" . $store_data[0]['StoreOfficialNumber'] . ")";
                //$SMSContentforImmediate = "Thank you for booking your appointment with Nailspa Experience for " . $weekday . " " . $dateforsms . " at " . $strSuitableAppointmentTime . " at " . $storename . ".Pls cancel or reschedule 45mins in advance to avoid cancellation charges.";
                CreateSMSURL("Appointment Booked", "0", "0", $SMSContentforImmediate, $CustomerMobileNo);

                /*
                 * Send Appointment Email
                 */


                $seldata = select("*", "tblStores", "StoreID='" . $strStoreID . "'");
                $address = $seldata[0]['StoreOfficialAddress'];
                $storename = $seldata[0]['StoreName'];
                $storecontact = $seldata[0]['StoreOfficialNumber'];
                $branche = explode(",", $storename);
                $branchname = $branche[1];


                $strTo = $emailc;
                $strFrom = "appnt@nailspaexperience.com";
                $strSubject = "Thank you for booking an Appointment at NS Style Salon";

                $strBody = "";
                $strStatus = "0"; // Pending = 0 / Sent = 1 / Error = 2
                $Name = $fullname;
                $Email = $emailc;
//$strAdminPassword = $strAdminPassword;
//$strDate = date("Y-m-d h:i:s");
                $StoreAddress = $address;
                $strDate = date("Y-m-d");
                $strTime = $strSuitableAppointmentTime;
                $path = "`http://nailspaexperience.com/images/test2.png`";

                $message = file_get_contents('EmailFormat/appointment.html');
                $message = str_replace("[\]", '', $message);

//setup vars to replace
                $store_location = preg_replace("/<br>|\n/", "", $seldata[0]['StoreBillingAddress']);
                $map_link = "<a target=\"_blank\" href=\"" . urldecode($store_location) . "\">Go to Google Map</a>";
                $vars = array('{First_name}', '{Name_Detail}', '{Date}', '{Time}', '{Address}', '{Path}', '{Branch}', '{store_contact}', '{map_link}', '{customer_name}'); //Replace varaibles
                $values = array($seldatac[0]['FirstName'], $Name, $dateforsms, $strSuitableAppointmentTime, $StoreAddress, $path, $storename, $storecontact, $map_link, $fullname);

//replace vars
                $message = str_replace($vars, $values, $message);
//echo $message;


                $strBody = $message;
                $flag = 'APB';
                $id = $last_idp;
                if ($strTo != "") {
                    $send_status = sendmail($id, $strTo, $strFrom, $strSubject, $strBody, $strDate, $flag, $strStatus);
                }

                return true;
            } else {

                /*
                 * Send Appointment Email
                 */

                $seldatac = select("CustomerEmailID,CustomerFullName,CustomerMobileNo,FirstName", "tblCustomers", "CustomerID='" . $strCustomerID . "'");
                $emailc = $seldatac[0]['CustomerEmailID'];
                $fullname = $seldatac[0]['CustomerFullName'];
                $CustomerMobileNo = $seldatac[0]['CustomerMobileNo'];

                $seldata = select("*", "tblStores", "StoreID='" . $strStoreID . "'");
                $address = $seldata[0]['StoreOfficialAddress'];
                $storename = $seldata[0]['StoreName'];
                $storecontact = $seldata[0]['StoreOfficialNumber'];
                $branche = explode(",", $storename);
                $branchname = $branche[1];

                $appo_data = select("*", "tblAppointments", "AppointmentID='" . $last_idp . "'");
                $strCustomerID = $appo_data[0]['CustomerID'];
                $strStoreID = $appo_data[0]['StoreID'];
                $AppointmentDate = $appo_data[0]['AppointmentDate'];
                $timep = $appo_data[0]['SuitableAppointmentTime'];
                $strSuitableAppointmentTime = $appo_data[0]['SuitableAppointmentTime'];
                $time_in_12_hour_format = date("g:i a", strtotime($timep));
                $weekday = date('l', strtotime($AppointmentDate)); // note: first arg to date() is lower-case L

                $dateforsms = FormatDatetime($time_in_12_hour_format);
                $dateforsms = date('jS M Y', strtotime($AppointmentDate));
                $Timebefore45min = date("H:i:s", strtotime("-45 minutes", strtotime($pqr)));

                /*
                 * Reminder SMS
                 */
                $SMSContent = "Dear Customer Name, your appnt is scheduled for " . $weekday . " " . $dateforsms . " at " . $strSuitableAppointmentTime . " at " . $storename . ". Pls cancel or reschedule 45mins in advance to avoid cancellation charges.";
                $InsertSMSDetails = "Insert into tblAppointmentsReminderSMS (AppointmentID, StoreID, CustomerID, AppointmentDate, SuitableAppointmentTime,  SMSSendTime, Status, ContentSMS,SendSMSTo)values('" . $last_idp . "','" . $strStoreID . "','" . $strCustomerID . "','" . $AppointmentDate . "','" . $pqr . "','" . $Timebefore45min . "',0,'" . $SMSContent . "', '" . $CustomerMobileNo . "')";
                ExecuteNQ($InsertSMSDetails);

                /*
                 * Immdiate SMS
                 */
                $cust_name = $post_data['firstname'] . "  " . $post_data['lastname'];
                //$SMSContentforImmediate = "Thank you for booking your appointment with Nailspa Experience for " . $weekday . " " . $dateforsms . " at " . $strSuitableAppointmentTime . " at " . $storename . ".Pls cancel or reschedule 45mins in advance to avoid cancellation charges.";
                $SMSContentforImmediate = "Dear " . ucwords($cust_name) . ", thank you for booking with us online. Our salon manager will reconfirm your appnt over a call soon or call (" . $seldata[0]['StoreOfficialNumber'] . ")";
                CreateSMSURL("Appointment Booked", "0", "0", $SMSContentforImmediate, $CustomerMobileNo);


                $strTo = $emailc;
                $strFrom = "appnt@nailspaexperience.com";
                $strSubject = "Thank you for booking an Appointment at NS Style Salon";

                $strBody = "";
                $strStatus = "0"; // Pending = 0 / Sent = 1 / Error = 2
                $Name = $fullname;
                $Email = $emailc;
//$strAdminPassword = $strAdminPassword;
//$strDate = date("Y-m-d h:i:s");
                $StoreAddress = $address;
                $strDate = date("Y-m-d");
                $strTime = $strSuitableAppointmentTime;
                $path = "`http://nailspaexperience.com/images/test2.png`";

                $message = file_get_contents('EmailFormat/appointment.html');
                $message = str_replace("[\]", '', $message);

//setup vars to replace
                $store_location = preg_replace("/<br>|\n/", "", $seldata[0]['StoreBillingAddress']);
                $map_link = "<a target=\"_blank\" href=\"" . urldecode($store_location) . "\">Go to Google Map</a>";
                $vars = array('{First_name}', '{Name_Detail}', '{Date}', '{Time}', '{Address}', '{Path}', '{Branch}', '{store_contact}', '{map_link}', '{customer_name}'); //Replace varaibles
                $values = array($seldatac[0]['FirstName'], $Name, $dateforsms, $strSuitableAppointmentTime, $StoreAddress, $path, $storename, $storecontact, $map_link, $fullname);

//replace vars
                $message = str_replace($vars, $values, $message);
//echo $message;


                $strBody = $message;
                $flag = 'APB';
                $id = $last_idp;
                if ($strTo != "") {
                    $send_status = sendmail($id, $strTo, $strFrom, $strSubject, $strBody, $strDate, $flag, $strStatus);
                }

                return "Service not Found.";
            }
        } else {
            return 'please enter mobile number';
        }
    }
    $DB->close();
}

?>