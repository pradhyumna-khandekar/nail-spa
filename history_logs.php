<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Manage Services,Offers & Stores | Nailspa";
$strDisplayTitle = "Logs for Nailspa";
$strMenuID = "3";
$strMyActionPage = "history_logs.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <!-----------css & js files added for tabs by gandhali 3/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");     ?>
            <!----------commented by gandhali 3/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>
                        <script type="text/javascript" src="assets/widgets/datatable/date-euro.js"></script>
                        <script type="text/javascript">
                            /* Datatables responsive */
                            $(document).ready(function () {
                                $('#datatable-responsive-date').DataTable({
                                    "order": [[2, "desc"]],
                                    columnDefs: [{type: 'date-euro', targets: 2}]
                                });
                            });
                            $(document).ready(function () {
                                $('.dataTables_filter input').attr("placeholder", "Search...");
                            });
                        </script>

                        <?php
                        $DB = Connect();


                        /*
                         * get service history
                         */
                        $ser_history_data = array();
                        $historyq = "SELECT * FROM tblServices_history ORDER BY history_timestamp DESC";
                        $historyq_exe = $DB->query($historyq);
                        if ($historyq_exe->num_rows > 0) {
                            while ($history_row = $historyq_exe->fetch_assoc()) {
                                $ser_history_data[$history_row['ID']] = $history_row;
                                $ser_history_data[$history_row['ID']]['log_module'] = 'service';
                            }
                        }

                        /*
                         * get offer history
                         */
                        $offer_history_data = array();
                        $historyq = "SELECT * FROM tblOffers_history ORDER BY history_timestamp DESC";
                        $historyq_exe = $DB->query($historyq);
                        if ($historyq_exe->num_rows > 0) {
                            while ($history_row = $historyq_exe->fetch_assoc()) {
                                $offer_history_data[$history_row['ID']] = $history_row;
                                $offer_history_data[$history_row['ID']]['log_module'] = 'offer';
                            }
                        }

                        /*
                         * get store history
                         */
                        $store_history_data = array();
                        $historyq = "SELECT * FROM tblStores_history ORDER BY history_timestamp DESC";
                        $historyq_exe = $DB->query($historyq);
                        if ($historyq_exe->num_rows > 0) {
                            while ($history_row = $historyq_exe->fetch_assoc()) {
                                $store_history_data[$history_row['ID']] = $history_row;
                                $store_history_data[$history_row['ID']]['log_module'] = 'store';
                            }
                        }

                        $history_data = array_merge($ser_history_data, $offer_history_data, $store_history_data);

                        if (isset($history_data) && is_array($history_data) && count($history_data) > 0) {
                            /*
                             * get all users
                             */
                            $userq = "SELECT * FROM tblAdmin";
                            $userq_exe = $DB->query($userq);
                            if ($userq_exe->num_rows > 0) {
                                while ($user_row = $userq_exe->fetch_assoc()) {
                                    $user_data[$user_row['AdminID']] = $user_row;
                                }
                            }
                        }
                        $DB->close();
                        $counter = 0;
                        ?>
                        <div id="page-title">
                            <h2>All Services Log</h2>
                        </div>

                        <div class="panel">
                            <div class="panel">
                                <div class="panel-body">

                                    <div class="example-box-wrapper">
                                        <div class="tabs">
                                            <div id="normal-tabs-1">
                                                <span class="form_result">&nbsp; <br>
                                                </span>

                                                <div class="panel-body">
                                                    <h3 class="title-hero">List of Services,Offers & Stores Logs for POS</h3>
                                                    <div class="example-box-wrapper">
                                                        <table id="datatable-responsive-date" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                                                            <thead>
                                                                <tr>
                                                                    <th>Services/Offers/Stores</th>
                                                                    <th>Action</th>
                                                                    <th>Date/time</th>
                                                                    <th>User Name</th>
                                                                    <th>Action</th>

                                                                </tr>
                                                            </thead>
                                                            <tfoot>
                                                                <tr>
                                                                    <th>Services/Offers/Stores</th>
                                                                    <th>Action</th>
                                                                    <th>Date/time</th>
                                                                    <th>User Name</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                            </tfoot>
                                                            <tbody>
                                                                <?php
                                                                if (isset($history_data) && is_array($history_data) && count($history_data) > 0) {
                                                                    foreach ($history_data as $hkey => $hvalue) {
                                                                        ?>
                                                                        <tr id="my_data_tr_<?= $counter ?>">
                                                                            <?php
                                                                            if ($hvalue['history_notes'] == 'insert') {
                                                                                $action_name = "<p style='color:blue;'>Add</p>";
                                                                                $action_responsible = isset($user_data[$hvalue['created_by']]) ? $user_data[$hvalue['created_by']]['AdminFullName'] : '';
                                                                            } else if ($hvalue['history_notes'] == 'update') {
                                                                                $action_name = "<p class='text-success'>Edit</p>";
                                                                                $action_responsible = isset($user_data[$hvalue['modified_by']]) ? $user_data[$hvalue['modified_by']]['AdminFullName'] : '';
                                                                            } else if ($hvalue['history_notes'] == 'delete') {
                                                                                $action_name = "<p class='text-danger'>Delete</p>";
                                                                                $action_responsible = isset($user_data[$hvalue['modified_by']]) ? $user_data[$hvalue['modified_by']]['AdminFullName'] : '';
                                                                            }
                                                                            ?>

                                                                            <td>

                                                                                <?php if ($hvalue['log_module'] == 'service') { ?>
                                                                                    <b>Service Name:</b> <?= $hvalue['ServiceName'] ?> <br><b>Code:</b> <?= $hvalue['ServiceCode'] ?>
                                                                                <?php } else if ($hvalue['log_module'] == 'offer') { ?>
                                                                                    <b>Offer Name:</b> <?= $hvalue['OfferName'] ?> <br><b>Code:</b> <?= $hvalue['OfferCode'] ?>
                                                                                <?php } else if ($hvalue['log_module'] == 'store') { ?>
                                                                                    <b>Store Name:</b><?php echo $hvalue['StoreName']; ?>
                                                                                <?php } ?>
                                                                            </td>
                                                                            <td><?php echo isset($action_name) ? $action_name : ''; ?></td>
                                                                            <td><?php echo isset($hvalue['history_timestamp']) ? date('d/m/Y h:i a', strtotime($hvalue['history_timestamp'])) : ''; ?></td>
                                                                            <td><?php echo isset($action_responsible) ? $action_responsible : ''; ?></td>

                                                                            <td>
                                                                                <?php if ($hvalue['log_module'] == 'service') { ?>
                                                                                    <a class="btn btn-link" href="ServiceshistoryList.php?service_id=<?php echo $hvalue['ServiceID']; ?>">Service History</a>
                                                                                <?php } else if ($hvalue['log_module'] == 'offer') { ?>
                                                                                    <a class="btn btn-link" href="OffershistoryList.php?offer_id=<?php echo $hvalue['OfferID']; ?>">Offer History</a>
                                                                                <?php } else if ($hvalue['log_module'] == 'store') { ?>
                                                                                    <a class="btn btn-link" href="StoreHistoryList.php?store_id=<?php echo $hvalue['StoreID']; ?>">Store History</a>
                                                                                <?php } ?>
                                                                            </td>
                                                                        </tr>

                                                                        <?php
                                                                    }
                                                                    $counter++;
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <?php require_once 'incFooter.fya'; ?>
        </div>
    </body>
</html>
