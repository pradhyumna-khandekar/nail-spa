<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php require_once("summary_report_helper.php"); ?>
<?php

$store_data = select("*", "tblStores", "Status='0'");

$fields = array('Month', 'Details');
if (isset($store_data) && is_array($store_data) && count($store_data) > 0) {
    foreach ($store_data as $skey => $svalue) {
        $fields[] = $svalue['StoreName'];
    }
}

if (isset($_GET['month']) && $_GET['month'] != '' && isset($_GET['date1']) && $_GET['date1'] != '') {
    $year = $_GET['date1'];
    $month = sprintf("%02d", $_GET['month']);
    $start_date = $month . '/01/' . $year;
    $end_date = date("m/t/Y", strtotime($start_date));

    $report_start = date('d M,Y',  strtotime($start_date));
    $report_end = date('d M,Y',  strtotime($end_date));
    $strtoandfrom = $start_date . ' - ' . $end_date;

    $report_name = array(
        'avg_sale' => 'Monthly Average Sale',
        'arpu' => 'ARPU',
        'avg_time_spent' => 'Avg Time Spent by Customer',
        'new_cust_cnt' => 'New Customer Count',
        'new_cust_amt' => 'New Customer Amount',
        'exi_cust_count' => 'Existing Customer Count',
        'exi_cust_amt' => 'Existing Customer Amount',
        'new_member_count' => 'Membership Sold',
        'new_member_amt' => 'New Membership Amount'
    );


//header('Content-Type: text/csv; charset=utf-8');
//header('Content-Disposition: attachment; filename=Summary_report-' . date('Y-m-d') . '.csv');
//$output = fopen("php://output", "w");
//fputcsv($output, $fields);

    /*
     * Get Report Sale Data
     */
    $report_sale_data = Report_sale($strtoandfrom, '');

    $avg_time_data = Avg_time_spent($strtoandfrom, '');

    $membership_data = Membership_data($strtoandfrom, '');

    if (isset($report_sale_data) && is_array($report_sale_data) && count($report_sale_data) > 0) {
        foreach ($report_sale_data as $rekey => $revalue) {
            $result[$rekey]['avg_sale'] = $revalue['service_sale'];
            $result[$rekey]['arpu'] = $revalue['arpu'];
            $result[$rekey]['new_cust_cnt'] = $revalue['new_cust_cnt'];
            $result[$rekey]['new_cust_amt'] = $revalue['new_cust_amount'];
            $result[$rekey]['exi_cust_count'] = $revalue['existing_cust_cnt'];
            $result[$rekey]['exi_cust_amt'] = $revalue['exist_cust_amount'];
        }
    }

    if (isset($avg_time_data) && is_array($avg_time_data) && count($avg_time_data) > 0) {
        foreach ($avg_time_data as $tikey => $tivalue) {
            $result[$tikey]['avg_time_spent'] = $tivalue['avg_time'];
        }
    }

    if (isset($membership_data) && is_array($membership_data) && count($membership_data) > 0) {
        foreach ($membership_data as $mkey => $mvalue) {
            $result[$mkey]['new_member_count'] = $mvalue['new_member'];
            $result[$mkey]['new_member_amt'] = $mvalue['new_member_amount'];
        }
    }

    if (isset($report_name) && is_array($report_name) && count($report_name) > 0) {
        foreach ($report_name as $rkey => $rvalue) {
            $fields_data = array();
            $fields_data[] = $report_start . ' - ' . $report_end;
            $fields_data[] = $rvalue;
            if (isset($store_data) && is_array($store_data) && count($store_data) > 0) {
                foreach ($store_data as $skey => $svalue) {
                    $fields_data[] = isset($result[$svalue['StoreID']][$rkey]) ? $result[$svalue['StoreID']][$rkey] : 0;
                }
            }
            fputcsv($output, $fields_data);
        }
    }
    fclose($output);
}
?>