<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
$strPageTitle = "Customer Details | NailSpa";
$strDisplayTitle = "Customer Details for NailSpa";
$strMenuID = "10";
$strMyTable = "tblAppointments";
$strMyTableID = "AppointmentID";
$strMyField = "AppointmentDate";
$strMyActionPage = "DisplayNonCustomerCount.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <?php require_once("incChart-SalonDashboard.fya"); ?>

        <!-----------------included datatable.css & js files by gandhali 1/9/18----------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/datatable/datatable.css">
        <script type="text/javascript" src="assets/widgets/datatable/datatable.js"></script>
        <script type="text/javascript" src="assets/widgets/datatable/datatable-bootstrap.js"></script>
        <script type="text/javascript" src="assets/widgets/datatable/datatable-responsive.js"></script>
        <!-----------------included datatable.css by gandhali 1/9/18----------------->
        <style>
            .btn-danger:hover
            {
                border-color: #fc8213;
                background: #fc8213;
            }

        </style>
        <!-- Styles -->


    </head>

    <body>

        <div id="sb-site">


            <?php // require_once("incOpenLayout.fya");   ?>
            <!----------commented by gandhali 1/9/18---------------->


            <?php require_once("incLoader.fya"); ?>


            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>

                        <script type="text/javascript" src="assets/widgets/skycons/skycons.js"></script>
                        <script type="text/javascript" src="assets/widgets/datatable/datatable.js"></script>
                        <script type="text/javascript" src="assets/widgets/datatable/datatable-bootstrap.js"></script>
                        <script type="text/javascript" src="assets/widgets/datatable/datatable-tabletools.js"></script>


                        <div class="panel">
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="example-box-wrapper">
                                        <div id="normal-tabs-1">
                                            <span class="form_result">&nbsp; <br>
                                            </span>

                                            <div class="panel-body">
                                                <h4 class="title-hero"><center>List of 	Non Visting Customers | NailSpa</center></h4>
                                                <br>
                                                <div class="example-box-wrapper">
                                                    <table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap table-hover" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th><center>Store Name</center></th>
                                                        <th><center>Total Non Customers</center></th>
                                                        <th><center>Total Non Customer Contact</center></th>
                                                        </tr>
                                                        </thead>


                                                        <tbody>
                                                            <?php
                                                            $DB = Connect();
                                                            $n5_daysAgot = date('Y-m-d', strtotime('-50 days', time()));
                                                            $selp = select("*", "tblStores", "Status='0'");
                                                            
                                                            if (isset($selp) && is_array($selp) && count($selp) > 0) {
                                                                foreach ($selp as $skey => $svalue) {
                                                                    /*
                                                                     * Get Customer Count
                                                                     */
                                                                    $store_append = " AND StoreID='" . $svalue['StoreID'] . "'";
                                                                    
                                                                    $countq = "SELECT COUNT(CustomerID) as cust_count FROM tblAppointments "
                                                                            . " WHERE  NonCustomerCommentType!=9 AND AppointmentDate < '" . $n5_daysAgot . "'"
                                                                            . " AND AppointmentID IN (SELECT max(AppointmentID) FROM tblAppointments "
                                                                            . " WHERE IsDeleted=0 AND Status=2 GROUP by CustomerID "
                                                                            . " ORDER by AppointmentDate DESC) " . $store_append;
//                                                                 
                                                                    $countq_exe = $DB->query($countq);
                                                                    if ($countq_exe->num_rows > 0) {
                                                                        while ($count_res = $countq_exe->fetch_assoc()) {
                                                                            $total_customer = $count_res['cust_count'];
                                                                        }
                                                                    }


                                                                    $contact_countq = "SELECT COUNT(CustomerID) as cust_count FROM tblAppointments "
                                                                            . " WHERE  NonCustomerCommentType NOT IN (3,4,5,6,7,8,9) AND NonCustomerCommentType!='0' AND AppointmentDate < '" . $n5_daysAgot . "'"
                                                                            . " AND AppointmentID IN (SELECT max(AppointmentID) FROM tblAppointments "
                                                                            . " WHERE IsDeleted=0 AND Status=2 GROUP by CustomerID "
                                                                            . " ORDER by AppointmentDate DESC) " . $store_append;
                                                                    $contact_countq_exe = $DB->query($contact_countq);
                                                                    if ($contact_countq_exe->num_rows > 0) {
                                                                        while ($con_count_res = $contact_countq_exe->fetch_assoc()) {
                                                                            $total_customer_con = $con_count_res['cust_count'];
                                                                        }
                                                                    }
                                                                    ?>
                                                                    <tr>
                                                                        <td><?php echo $svalue['StoreName']; ?></td>
                                                                        <td style="cursor: pointer;" class="numeric" onclick="window.open('DisplaySalonNonCustomerDetails.php?Store=<?php echo $svalue['StoreID']; ?>', '_blank');"><b><?php echo isset($total_customer) ? $total_customer : 0; ?></b></td>
                                                                        <td style="cursor: pointer;" class="numeric" onclick="window.open('DisplaySalonNonCustomerDetails.php?Store=<?php echo $svalue['StoreID']; ?>&contact=1', '_blank');"><b><?php echo isset($total_customer_con) ? $total_customer_con : 0; ?></b></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </tbody>
                                                        <?php
                                                        $DB->close();
                                                        ?>
                                                    </table>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php require_once 'incFooter.fya'; ?>

                </div>
                </body>

                </html>		