<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php require_once 'comparison_report_helper.php'; ?>


<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <!-----------css & js files added for tabs by gandhali 3/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>
        <script>
            StartLoading();
        </script>
    </head>

    <body>
        <?php
        /** Error reporting */
        if (isset($_GET) && is_array($_GET) && count($_GET) > 0) {
            $date1 = isset($_GET['date1']) ? $_GET['date1'] : '';
            $date2 = isset($_GET['date2']) ? $_GET['date2'] : '';
            $date3 = isset($_GET['date3']) ? $_GET['date3'] : '';
            $Store = isset($_GET['Store']) && !empty($_GET['Store']) ? $_GET['Store'] : '';

            if ($date1 != '') {
                $date1_data = Report_category_sale($date1,$Store);
            }

            if ($date2 != '') {
                $date2_data = Report_category_sale($date2,$Store);
            }

            if ($date3 != '') {
                $date3_data = Report_category_sale($date3,$Store);
            }

            $DB = Connect();
            $all_category = select("*", "tblCategories", " CategoryType = 1");
            $DB->close();
        }
        ?>
        <div id="sb-site">
            <?php require_once("incLoader.fya"); ?>
            <div id="page-wrapper">
                <?php
                if (isset($_GET) && is_array($_GET) && count($_GET) > 0) {
                    for ($m = 01; $m <= 12; ++$m) {
                        $col1_total = 0;
                        $col2_total = 0;
                        $col3_total = 0;
                        ?>
                        <h2>Month: <?php echo date('F', mktime(0, 0, 0, $m, 1)); ?></h2>
                        <table class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Category</th>
                                    <th><?php echo isset($_GET['date1']) && $_GET['date1'] != '' ? $_GET['date1'] : 'Date1' ?></th>
                                    <th><?php echo isset($_GET['date2']) && $_GET['date2'] != '' ? $_GET['date2'] : 'Date2' ?></th>
                                    <th><?php echo isset($_GET['date3']) && $_GET['date3'] != '' ? $_GET['date3'] : 'Date3' ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $year1 = isset($_GET['date1']) ? $_GET['date1'] : '';
                                $year2 = isset($_GET['date2']) ? $_GET['date2'] : '';
                                $year3 = isset($_GET['date3']) ? $_GET['date3'] : '';
                                ?>
                                <?php
                                if (isset($all_category) && is_array($all_category) && count($all_category) > 0) {
                                    foreach ($all_category as $ckey => $cvalue) {
                                        ?>
                                        <tr>
                                            <?php
                                            if (strlen($m) == 1) {
                                                $mon = '0' . $m;
                                            } else {
                                                $mon = $m;
                                            }

                                            $chart_data[$year1]['name'] = $year1;
                                            $chart_data[$year2]['name'] = $year2;
                                            $chart_data[$year3]['name'] = $year3;

                                            $chart_data1 = isset($date1_data['result'][$year1 . '-' . $mon][$cvalue['CategoryID']]) ? $date1_data['result'][$year1 . '-' . $mon][$cvalue['CategoryID']] : 0;
                                            $chart_data2 = isset($date2_data['result'][$year2 . '-' . $mon][$cvalue['CategoryID']]) ? $date2_data['result'][$year2 . '-' . $mon][$cvalue['CategoryID']] : 0;
                                            $chart_data3 = isset($date3_data['result'][$year3 . '-' . $mon][$cvalue['CategoryID']]) ? $date3_data['result'][$year3 . '-' . $mon][$cvalue['CategoryID']] : 0;

                                            if (isset($chart_data[$year1]['data'][$mon])) {
                                                $chart_data[$year1]['data'][$mon] += $chart_data1;
                                            } else {
                                                $chart_data[$year1]['data'][$mon] = $chart_data1;
                                            }

                                            if (isset($chart_data[$year2]['data'][$mon])) {
                                                $chart_data[$year2]['data'][$mon] += $chart_data2;
                                            } else {
                                                $chart_data[$year2]['data'][$mon] = $chart_data2;
                                            }

                                            if (isset($chart_data[$year3]['data'][$mon])) {
                                                $chart_data[$year3]['data'][$mon] += $chart_data3;
                                            } else {
                                                $chart_data[$year3]['data'][$mon] = $chart_data3;
                                            }

                                            $col1_total += $chart_data1;
                                            $col2_total += $chart_data2;
                                            $col3_total += $chart_data3;
                                            ?>

                                            <td><?php echo ucwords($cvalue['CategoryName']); ?></td>
                                            <td><?php echo isset($date1_data['result'][$year1 . '-' . $mon][$cvalue['CategoryID']]) ? $date1_data['result'][$year1 . '-' . $mon][$cvalue['CategoryID']] : 0; ?></td>
                                            <td><?php echo isset($date2_data['result'][$year2 . '-' . $mon][$cvalue['CategoryID']]) ? $date2_data['result'][$year2 . '-' . $mon][$cvalue['CategoryID']] : 0; ?></td>
                                            <td><?php echo isset($date3_data['result'][$year3 . '-' . $mon][$cvalue['CategoryID']]) ? $date3_data['result'][$year3 . '-' . $mon][$cvalue['CategoryID']] : 0; ?></td>
                                        </tr>
                                    <?php }
                                    ?>
                                    <tr>
                                        <td><b>Total</b></td>
                                        <td><b><?php echo round($col1_total, 2); ?></b></td>
                                        <td><b><?php echo round($col2_total, 2); ?></b></td>
                                        <td><b><?php echo round($col3_total, 2); ?></b></td>
                                    </tr>
                                <?php }
                                ?>

                            </tbody>
                        </table>

                        <br>

                        <?php
                    }
                } else {
                    ?>
                    <h3>No Date Selected.</h3>
                    <?php
                }

                $final_data = array();

                if (isset($chart_data) && is_array($chart_data) && count($chart_data) > 0) {
                    $col_count = 0;
                    foreach ($chart_data as $ykey => $yvalue) {
                        $final_data[$col_count]['name'] = $yvalue['name'];
                        $mon_count = 0;
                        foreach ($yvalue['data'] as $ykey => $yvalue) {
                            $final_data[$col_count]['data'][$mon_count] = $yvalue;
                            $mon_count++;
                        }
                        $col_count++;
                    }
                }
                ?>
            </div>

            <script src="assets/widgets/highcharts/highcharts.js"></script>
            <script src="assets/widgets/highcharts/exporting.js"></script>
            <script src="assets/widgets/highcharts/export-data.js"></script>

            <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            <script>
            Highcharts.chart('container', {
                chart: {
                    type: 'line'
                },
                title: {
                    text: 'Service Sale Category'
                },
                xAxis: {
                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                },
                yAxis: {
                    title: {
                        text: 'Service Sale(In Rs.)'
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: true
                    }
                },
                series: <?php echo json_encode($final_data); ?>
            });

            EndLoading();
            </script>
            <?php require_once 'incFooter.fya'; ?>
        </div>
    </body>
</html>