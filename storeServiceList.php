<?php require_once 'setting.fya'; ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
$test = "";
if (isset($_POST["id"])) {
    $test = $_POST["id"];
}
// echo $test;
$DB = Connect();

$sql = "SELECT ServiceID, ServiceName, ServiceCost FROM tblServices WHERE StoreID=$test";
$RS = $DB->query($sql);
?>
<div class="form-group"><label class="col-sm-2 control-label">Services </label>
    <div class="col-sm-4 ">
        <select class="form-control chosen-select" name="Services[]"  id="Services" style="width:161%" multiple="multiple">
            <option value="" align="center"><b>--Select Services--</b></option>
            <?php
            if ($RS->num_rows > 0) {
                while ($row = $RS->fetch_assoc()) {
                    $ServiceID = $row["ServiceID"];
                    $ServiceName = $row["ServiceName"];
                    $ServiceCost = $row["ServiceCost"];
                    ?>
                    <option value="<?= $ServiceID ?>"><?= $ServiceName ?>, Rs. <?= $ServiceCost ?></option>
                    <?php
                }
            }
            ?>
        </select>
    </div>
</div>