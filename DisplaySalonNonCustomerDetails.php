<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
$strPageTitle = "Customer Details | NailSpa";
$strDisplayTitle = "Customer Details for NailSpa";
$strMenuID = "10";
$strMyTable = "tblAppointments";
$strMyTableID = "AppointmentID";
$strMyField = "AppointmentDate";
$strMyActionPage = "DisplaySalonNonCustomerDetails.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
if ($strAdminType != "0" && $strAdminRoleID != 42) {
    die("Sorry you are trying to enter Unauthorized access");
}
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <script type="text/javascript" src="assets/widgets/datepicker/datepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker-demo.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/moment.js"></script>
        <script type="text/javascript" src="assets/widgets/timepicker/timepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="assets/widgets/timepicker/timepicker.css">

        <script type="text/javascript">
            /* Timepicker */

            $(function () {
                "use strict";
                $('.timepicker-example').timepicker();
            });
        </script>
        <script>
            $(function ()
            {
                $(".datepicker-example").datepicker({minDate: 0});
            });
        </script>
        <?php require_once("incChart-SalonDashboard.fya"); ?>
        <style>
            .btn-danger:hover
            {
                border-color: #fc8213;
                background: #fc8213;
            }

        </style>
        <!-- Styles -->


    </head>

    <body>

        <div id="sb-site">


            <?php //require_once("incOpenLayout.fya");   ?>


            <?php require_once("incLoader.fya"); ?>


            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>

                        <script type="text/javascript" src="assets/widgets/skycons/skycons.js"></script>
                        <script type="text/javascript" src="assets/widgets/datatable/datatable.js"></script>
                        <script type="text/javascript" src="assets/widgets/datatable/datatable-bootstrap.js"></script>
                        <script type="text/javascript" src="assets/widgets/datatable/datatable-tabletools.js"></script>


                        <div class="panel">
                            <div class="panel">
                                <div class="panel-body">

                                    <div class="example-box-wrapper">



                                        <div id="normal-tabs-1">
                                            <span class="form_result">&nbsp; <br>
                                            </span>

                                            <div class="panel-body">
                                                <?php
                                                if (isset($_GET['Store']) && $_GET['Store'] != '') {
                                                    $store_res = select('*', 'tblStores', "StoreID='" . $_GET['Store'] . "'");
                                                    $store_name = $store_res[0]['StoreName'];
                                                }
                                                ?>
                                                <h4 class="title-hero"><center>
                                                        <?php
                                                        if (isset($_GET['contact']) && $_GET['contact'] == 1) {
                                                            echo "List of Non Visting Customer Contact";
                                                        } else {
                                                            echo "List of Non Visting Customer ";
                                                        }
                                                        ?> | NailSpa <?php echo isset($store_name) && $store_name != '' ? '(' . ucwords($store_name) . ')' : ''; ?></center></h4>
                                                <script type="text/javascript">
            $(document).ready(function () {

            });
            /* Datatables basic */

// $(document).ready(function() {
// $('#datatable-example').dataTable();
            // var abc=$("#abc").val();
            // $.ajax({
            // type:"POST",
            // data:"abc="+abc,
            // url:"chartdata.php",
            // success:function(res)
            // {
            // alert(res)
            // }


            // })
// });

            /* Datatables hide columns */

            $(document).ready(function () {

                var table = $('#datatable-hide-columns').DataTable({
                    "scrollY": "300px",
                    "paging": false
                });

                $('#datatable-responsive').on('draw.dt', function () {
                    $('.timepicker-example').timepicker();
                    $(".datepicker-example").datepicker({minDate: 0});
                });

                $('#datatable-hide-columns_filter').hide();

                $('a.toggle-vis').on('click', function (e) {
                    e.preventDefault();

// Get the column API object
                    var column = table.column($(this).attr('data-column'));

// Toggle the visibility
                    column.visible(!column.visible());
                });

            });

            /* Datatable row highlight */

            $(document).ready(function () {
                var table = $('#datatable-row-highlight').DataTable();
                $('#datatable-responsive').on('draw.dt', function () {
                    $('.timepicker-example').timepicker();
                    $(".datepicker-example").datepicker({minDate: 0});
                });

                $('#datatable-row-highlight tbody').on('click', 'tr', function () {
                    $(this).toggleClass('tr-selected');
                });
            });
            $(document).ready(function () {

                $('.dataTables_filter input').attr("placeholder", "Search...");
            });
            function checkstatus(evt, setid)
            {
                var customer = $(evt).closest('td').prev().prev().find('input').val();
                var customer_location = $(evt).closest('td').next().find('input').val();
                var checkcommenttype = $(evt).val();
                var app = $(evt).closest('td').prev().find('input').val();
                if (checkcommenttype == '1')
                {
                    $(evt).closest('td').next().find('textarea').attr("ReadOnly", false);
                    $(evt).closest('td').next().find('textarea').show();
                    $(evt).closest('td').next().find('.call_later_div').hide();
                    $(evt).closest('td').next().find('.not_interested_div').hide();
                    $(evt).closest('td').next().next().find('a').attr("href", "ManageAppointments.php?bid=" + customer + "&App=" + app + "&Rem=Y&remark=1&source=4&cust_location=" + customer_location);
                    $(evt).closest('td').next().next().find('a').text("Book Appoinment");
                    //document.getElementById("updateid").href="ManageAppointments.php?bid="+customer+"&Rem=Y"; 
                    /* $(evt).closest('td').next().next().find('a .updateid').hide();
                     $(evt).closest('td').next().next().find('a .bookid').show(); */

                } else if (checkcommenttype == '2')
                {
                    //$(evt).closest('td').next().find('textarea').hide();
                    $(evt).closest('td').next().find('.call_later_div').show();
                    $(evt).closest('td').next().find('.not_interested_div').hide();
                    $(evt).closest('td').next().next().find('a').text("Update");
                    $(evt).closest('td').next().next().find('a').attr("href", "#");
                } else if (checkcommenttype == '9')
                {
                    //$(evt).closest('td').next().find('textarea').hide();
                    $(evt).closest('td').next().find('.call_later_div').hide();
                    $(evt).closest('td').next().find('.not_interested_div').show();
                    $(evt).closest('td').next().next().find('a').text("Update");
                    $(evt).closest('td').next().next().find('a').attr("href", "#");
                } else if (checkcommenttype == '5' || checkcommenttype == '6' || checkcommenttype == '7' || checkcommenttype == '9' || checkcommenttype == '2' || checkcommenttype == '3' || checkcommenttype == '4' || checkcommenttype == '8')
                {
                    $(evt).closest('td').next().find('textarea').show();
                    $(evt).closest('td').next().find('.call_later_div').hide();
                    $(evt).closest('td').next().find('.not_interested_div').hide();
                    $(evt).closest('td').next().find('textarea').attr("ReadOnly", false);
                    $(evt).closest('td').next().next().find('a').text("Update");
                    $(evt).closest('td').next().next().find('a').attr("href", "#");
                }

            }
            function updatevalues(evt)
            {

                var typecomment = $(evt).closest('td').prev().prev().find('select').val();
                var customer = $(evt).closest('td').prev().prev().prev().prev().find('input').val();
                // var customer_location = $(evt).closest('td').pre().find('input').val();
                var customer_location = $(evt).closest('td').find('input').val();
                var app = $(evt).closest('td').prev().prev().prev().find('input').val();
                var comment = $(evt).closest('td').prev().find('textarea').val();
                var call_date = $(evt).closest('td').prev().find('.datepicker-example').val();
                var call_time = $(evt).closest('td').prev().find('.timepicker-example').val();
                var not_interested_reason = $(evt).closest('td').prev().find('select').val();
                if (typecomment != "")
                {
                    if (typecomment == '1')
                    {
                        $(evt).closest('td').find('a').attr("href", "ManageAppointments.php?bid=" + customer + "&App=" + app + "&Rem=Y&remark=1&source=4&cust_location=" + customer_location);
                    } else {
                        $.ajax({
                            type: "post",
                            data: "call_date=" + call_date + "&call_time=" + call_time + "&not_interested_reason=" + not_interested_reason + "&customer=" + customer + "&comment=" + comment + "&app=" + app + "&typecomment=" + typecomment,
                            url: "UpdateNonCustomerCallRemark.php",
                            success: function (result1)
                            {
                                if ($.trim(result1) == '2')
                                {
                                    location.reload();
                                }

                            }
                        });
                    }
                } else
                {
                    alert('Please select atleast one comment option');
                }


            }
                                                </script>
                                                    <?php /* <button onclick="download_csv()">Download CSV</button> */ ?>
                                                <div class="example-box-wrapper">
                                                    <?php
// Create connection And Write Values
                                                    $DB = Connect();
                                                    $n5_daysAgot = date('Y-m-d', strtotime('-50 days', time()));
                                                    $todaydate = date('Y-m-d');

                                                    $store_append = '';
                                                    if ($strAdminRoleID == 42 || $strAdminRoleID == 36) {
                                                        if (isset($_GET['Store']) && $_GET['Store'] != '') {
                                                            $store_append = " AND StoreID='" . $_GET['Store'] . "'";
                                                        }
                                                    } else if ($strStore != '0') {
                                                        $store_append = " AND StoreID='" . $strStore . "'";
                                                    }
                                                    /*
                                                     * GET COUNT OF All Customer
                                                     */
                                                    $total_customer = 0;
                                                    if (isset($_GET['contact']) && $_GET['contact'] == 1) {
                                                        $comment_type = " NonCustomerCommentType NOT IN (3,4,5,6,7,8,9) AND NonCustomerCommentType!='0'";
                                                    } else {
                                                        $comment_type = " NonCustomerCommentType !=9";
                                                    }

                                                    $countq = "SELECT COUNT(CustomerID) as cust_count FROM tblAppointments "
                                                            . " WHERE  $comment_type AND AppointmentDate < '" . $n5_daysAgot . "'"
                                                            . " AND AppointmentID IN (SELECT max(AppointmentID) FROM tblAppointments "
                                                            . " WHERE IsDeleted=0 AND Status=2 GROUP by CustomerID "
                                                            . " ORDER by AppointmentDate DESC) " . $store_append;
                                                    $countq_exe = $DB->query($countq);
                                                    if ($countq_exe->num_rows > 0) {
                                                        while ($count_res = $countq_exe->fetch_assoc()) {
                                                            $total_customer = $count_res['cust_count'];
                                                        }
                                                    }

                                                    $limit = 15;
                                                    $total_page = ceil($total_customer / $limit);

                                                    $page = $_GET['page'];
                                                    if ($page == 0) {
                                                        $offset = 0;
                                                    } else {
                                                        $offset = ($page - 1) * $limit;
                                                    }
                                                    if ($offset < 0) {
                                                        $offset = 0;
                                                    }



                                                    $Productst = "SELECT AppointmentID,AppointmentDate,StoreID,NonCustomerTime,CustomerID,NonCustomerRemark,NonCustomerCommentType,NonCustomerCallDate,NonCustomerCallDate,NonCustomerReason FROM tblAppointments "
                                                            . " WHERE $comment_type  AND AppointmentDate < '" . $n5_daysAgot . "'"
                                                            . " AND AppointmentID IN (SELECT max(AppointmentID) FROM tblAppointments "
                                                            . " WHERE IsDeleted=0 AND Status=2  GROUP by CustomerID "
                                                            . " ORDER by AppointmentDate DESC) " . $store_append . " ORDER BY AppointmentDate LIMIT " . $limit . " OFFSET " . $offset;
                                                    $RSaT = $DB->query($Productst);

                                                    if ($RSaT->num_rows > 0) {
                                                        $counter = 0;
                                                        while ($rowa = $RSaT->fetch_assoc()) {

                                                            /*
                                                             * If last Appointment is Greater Than 50 Days Then only Show Record
                                                             */
                                                            if ($rowa['AppointmentDate'] < $n5_daysAgot) {
                                                                $customer_res[] = $rowa;
                                                            }
                                                        }
                                                    }
                                                    ?>


                                                    <?php
                                                    if (isset($_GET['page']) && $_GET['page'] != '') {
                                                        $search_emp[0] = $emp_list[$_GET['page']];

                                                        $previous_key = $_GET['page'] - 1;
                                                        if ($previous_key < $total_page && $page > 1) {
                                                            $page_qry = "?page=" . $previous_key;
                                                            if (isset($_GET['Store']) && $_GET['Store'] != '') {
                                                                $get_qry .= "&Store=" . $_GET['Store'];
                                                            }
                                                            if (isset($_GET['contact']) && $_GET['contact'] != '') {
                                                                $get_qry .= "&contact=" . $_GET['contact'];
                                                            }
                                                            echo " <a class='btn btn-blue-alt' href='DisplaySalonNonCustomerDetails.php" . $page_qry . $get_qry . "'>Previous</a>";
                                                        }

                                                        $next_key = $_GET['page'] + 1;
                                                        if ($next_key < $total_page || $next_key == $total_page) {
                                                            $page_qry = "?page=" . $next_key;
                                                            if (isset($_GET['Store']) && $_GET['Store'] != '') {
                                                                $get_qry = "&Store=" . $_GET['Store'];
                                                            }
                                                            if (isset($_GET['contact']) && $_GET['contact'] != '') {
                                                                $get_qry .= "&contact=" . $_GET['contact'];
                                                            }
                                                            echo " <a class='btn btn-blue-alt' href='DisplaySalonNonCustomerDetails.php" . $page_qry . $get_qry . "'>Next</a>";
                                                        }
                                                    } else {
                                                        if ($total_page > 1) {
                                                            $next_id = 1;
                                                            if (isset($_GET['Store']) && $_GET['Store'] != '') {
                                                                $get_qry = "&Store=" . $_GET['Store'];
                                                            }
                                                            if (isset($_GET['contact']) && $_GET['contact'] != '') {
                                                                $get_qry .= "&contact=" . $_GET['contact'];
                                                            }
                                                            echo "<a class='btn btn-blue-alt' href='DisplaySalonNonCustomerDetails.php?page=2" . $get_qry . "'>Next</a>";
                                                        }
                                                    }
                                                    ?>
                                                    <p class="btn btn-alt btn-hover btn-default">Total Customer: <?php echo isset($total_customer) ? $total_customer : 0; ?></p>
                                                    <p class="btn btn-alt btn-hover btn-default">Per Page: <?php echo isset($customer_res) ? count($customer_res) : 0; ?></p>

                                                    <p class="btn btn-alt btn-hover btn-default">page <?php echo isset($_GET['page']) && $_GET['page'] > 0 ? $_GET['page'] : 1; ?> Of <?php echo isset($total_page) ? $total_page : 1; ?></p>

                                                    <table class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th>Customer</th>
                                                                <th>Mobile</th>
                                                                <th>Last Service Date</th>
                                                                <th>Remark Type</th>
                                                                <th id="displaycomment">Comment</th>
                                                                <th>Action</th>

                                                            </tr>
                                                        </thead>

                                                        <tbody>

                                                            <?php
                                                            /*
                                                             * get customer
                                                             */
                                                            if (isset($customer_res) && is_array($customer_res) && count($customer_res) > 0) {
                                                                foreach ($customer_res as $rkey => $rowa) {
                                                                    $customer_ids[$rowa['CustomerID']] = $rowa['CustomerID'];
                                                                }
                                                            }
                                                            if (isset($customer_ids) && is_array($customer_ids) && count($customer_ids) > 0) {
                                                                $cust_in_ids = implode(",", $customer_ids);
                                                                $cust_q = "SELECT * FROM tblCustomers WHERE CustomerID IN(" . $cust_in_ids . ")";
                                                                $cust_exe = $DB->query($cust_q);
                                                                while ($custdetails = $cust_exe->fetch_assoc()) {
                                                                    $all_cust[] = $custdetails;
                                                                }
                                                                if (isset($all_cust) && is_array($all_cust) && count($all_cust) > 0) {
                                                                    foreach ($all_cust as $custkey => $custvalue) {
                                                                        $customer_details[$custvalue['CustomerID']] = $custvalue;
                                                                    }
                                                                }
                                                            }

// Create connection And Write Values
                                                            if (isset($customer_res) && is_array($customer_res) && count($customer_res) > 0) {
                                                                $counter = 0;
                                                                foreach ($customer_res as $rkey => $rowa) {

                                                                    /*
                                                                     * If last Appointment is Greater Than 50 Days Then only Show Record
                                                                     */
                                                                    $counter++;

                                                                    $Customer = $rowa['CustomerID'];

                                                                    $EncodedCustomerID = EncodeQ($Customer);


                                                                    $ct++;
                                                                    $cust = EncodeQ($Customer);
                                                                    $app_id = $rowa['AppointmentID'];

                                                                    // $selpqtyP = select("*", "tblCustomers", "CustomerID='" . $Customer . "'");
                                                                    $selpqtyP[0] = isset($customer_details[$Customer]) ? $customer_details[$Customer] : array();
                                                                    $customerfullname = $selpqtyP[0]['CustomerFullName'];
                                                                    $CustomerMobileNo = $selpqtyP[0]['CustomerMobileNo'];
                                                                    $NonCustomerRemark = $selpqtyP[0]['NonCustomerRemark'];
                                                                    $CustomerEmailID = $selpqtyP[0]['CustomerEmailID'];
                                                                    $CustomerLocation = $selpqtyP[0]['CustomerLocation'];
                                                                    $memberid = $selpqtyP[0]['memberid'];
                                                                    //if ($customerfullname != '') {
                                                                    ?>
                                                                    <tr>
                                                                        <td>
                                                                            <a data-toggle="modal" data-target="#myModalsAppointment<?= $counter ?>"><?= $customerfullname ?></a>

                                                                        </td>
                                                                        <td>
                                                                            <input type="hidden" id="cust_id"  value="<?= EncodeQ($Customer) ?>"/>

        <?= $CustomerMobileNo ?>

                                                                        </td>
                                                                        <td>
                                                                            <input type="hidden" id="app_id" value="<?= $app_id ?>">
        <?php echo isset($rowa['AppointmentDate']) && $rowa['AppointmentDate'] != '0000-00-00' && $rowa['AppointmentDate'] != '1970-01-01' ? date('d M,Y', strtotime($rowa['AppointmentDate'])) : '' ?></td>
                                                                        <td>
                                                                            <select id="SelectOptions" class="form-control" onchange="checkstatus(this,<?= $counter ?>)">
                                                                                <option value="">-- Select --</option>
                                                                                <option value="1" <?php echo (isset($rowa['NonCustomerCommentType']) && $rowa['NonCustomerCommentType'] == 1 ? 'selected' : ''); ?>>Book Now</option>
                                                                                <option value="2" <?php echo (isset($rowa['NonCustomerCommentType']) && $rowa['NonCustomerCommentType'] == 2 ? 'selected' : ''); ?>>Call Later</option>
                                                                                <?php /* <option value="3">Client Unavailable</option>
                                                                                  <option value="4">Client Travelling</option>
                                                                                  <option value="5">Unhappy with Service/Staff</option>
                                                                                  <option value="6">Service is Expensive</option>
                                                                                  <option value="7">Unhappy with Products</option>
                                                                                  <option value="8">Occassional Visitor</option> */ ?>
                                                                                <option value="9" <?php echo (isset($rowa['NonCustomerCommentType']) && $rowa['NonCustomerCommentType'] == 9 ? 'selected' : ''); ?>>Not Interested</option>
                                                                                <option value="10" <?php echo (isset($rowa['NonCustomerCommentType']) && $rowa['NonCustomerCommentType'] == 10 ? 'selected' : ''); ?>>Client will call</option>
                                                                            </select>

                                                                        </td>
                                                                        <td>

                                                                            <input type="hidden" id="cust_location" value="<?= $CustomerLocation ?>">

                                                                            <?php
                                                                            if (isset($rowa['NonCustomerCommentType']) && $rowa['NonCustomerCommentType'] == 9) {
                                                                                $comment_div_style = 'style="display:none;"';
                                                                                $comment = '';
                                                                            } else if (isset($rowa['NonCustomerCommentType']) && $rowa['NonCustomerCommentType'] == 2) {
                                                                                $comment_div_style = 'style="display:none;"';
                                                                                $comment = '';
                                                                            } else {
                                                                                $comment_div_style = '';
                                                                                $comment = $rowa['NonCustomerRemark'];
                                                                            }
                                                                            $comment = $rowa['NonCustomerRemark'];
                                                                            ?>
                                                                            <div class="form-group comment_div">
                                                                                <textarea id="comment" class="form-control" Rows="2" cols="30"><?php echo $comment; ?></textarea>
                                                                            </div>

                                                                            <?php
                                                                            if (isset($rowa['NonCustomerCommentType']) && $rowa['NonCustomerCommentType'] == 2) {
                                                                                $call_div_style = '';
                                                                                $time_in_12_hour_format = '';
                                                                                $date_format = '';
                                                                                if ($rowa['NonCustomerCallDate'] != '0000-00-00' && $rowa['NonCustomerCallDate'] != '') {
                                                                                    $date_format = date("m/d/Y", strtotime($rowa['NonCustomerCallDate']));
                                                                                    $time_in_12_hour_format = date("g:i a", strtotime($rowa['NonCustomerTime']));
                                                                                }
                                                                            } else {
                                                                                $call_div_style = 'style="display:none;"';
                                                                                $date_format = '';
                                                                                $time_in_12_hour_format = '';
                                                                            }
                                                                            ?>

                                                                            <div class="form-group call_later_div" <?php echo $call_div_style; ?>>
                                                                                <label class="col-sm-2 control-label">Suitable Date</label>
                                                                                <div class="col-sm-4">
                                                                                    <div class="input-prepend input-group"><span class="add-on input-group-addon"><i class="glyph-icon icon-calendar"></i></span> <input type="text" name="SuitableCallDate" value="<?= isset($date_format) ? $date_format : '' ?>" class="form-control datepicker-example" data-date-format="yy/mm/dd" value="<?php echo date('Y-m-d'); ?>"></div>
                                                                                </div>
                                                                                <label class="col-sm-2 control-label">Suitable Time</label>
                                                                                <div class="col-sm-4">
                                                                                    <input type="text" name="SuitableCallTime"  value="<?= isset($time_in_12_hour_format) ? $time_in_12_hour_format : ''; ?>"  id="SuitableCallTime" class="form-control required timepicker-example">
                                                                                </div>
                                                                            </div>

                                                                            <?php
                                                                            if (isset($rowa['NonCustomerCommentType']) && $rowa['NonCustomerCommentType'] == 9) {
                                                                                $reason_div_style = '';
                                                                            } else {
                                                                                $reason_div_style = 'style="display:none;"';
                                                                            }
                                                                            ?>
                                                                            <div class="form-group not_interested_div"  <?php echo $reason_div_style; ?>>
                                                                                <select id="not_interested_reason" class="form-control">
                                                                                    <option value="3/Client Unavailable" <?php echo (isset($rowa['NonCustomerReason']) && $rowa['NonCustomerReason'] == 3 ? 'selected' : ''); ?>>Client Unavailable</option>
                                                                                    <option value="4/Client Travelling" <?php echo (isset($rowa['NonCustomerReason']) && $rowa['NonCustomerReason'] == 4 ? 'selected' : ''); ?>>Client Travelling</option>
                                                                                    <option value="5/Unhappy with Service/Staff" <?php echo (isset($rowa['NonCustomerReason']) && $rowa['NonCustomerReason'] == 5 ? 'selected' : ''); ?>>Unhappy with Service/Staff</option>
                                                                                    <option value="6/Service is Expensive" <?php echo (isset($rowa['NonCustomerReason']) && $rowa['NonCustomerReason'] == 6 ? 'selected' : ''); ?>>Service is Expensive</option>
                                                                                    <option value="7/Unhappy with Products" <?php echo (isset($rowa['NonCustomerReason']) && $rowa['NonCustomerReason'] == 7 ? 'selected' : ''); ?>>Unhappy with Products</option>
                                                                                    <option value="8/Occassional Visitor" <?php echo (isset($rowa['NonCustomerReason']) && $rowa['NonCustomerReason'] == 8 ? 'selected' : ''); ?>>Occassional Visitor</option> 
                                                                                </select>
                                                                            </div>

                                                                        </td>
                                                                        <td>
                                                                            <input type="hidden" id="AppointmentBookingURL<?= $Customer ?>" value="ManageAppointments.php?bid=<?= EncodeQ($Customer) ?>&location=<?= $CustomerLocation; ?>">
                                                                            <a class="btn btn-link updateid" id="updateid" onclick="updatevalues(this)">Update</a>
                                                                            <!--<a class="btn btn-link bookid" style="display:none" id="bookid"  href="ManageAppointments.php?bid=<?= $cust ?>&Rem=Y">Book</a>-->
                                                                        </td>

                                                                        <?php
                                                                        $csvdata[$ct]['name'] = $customerfullname;
                                                                        $csvdata[$ct]['mobile'] = $CustomerMobileNo;
                                                                        $csvdata[$ct]['remark_type'] = '';
                                                                        $csvdata[$ct]['comment'] = '';
                                                                        //}
                                                                        ?>

                                                                    </tr>
                                                                    <?php
                                                                }
                                                                if ($_SERVER['REMOTE_ADDR'] == "111.119.219.70") {
                                                                    echo $ct;
                                                                } else {
                                                                    
                                                                }
                                                            } else {
                                                                ?>
                                                                <tr><td>No Records</td><td></td><td></td><td></td><td></td></tr>
                                                                <?php
                                                            }
                                                            ?>
                                                        </tbody>

                                                        <?php
                                                        $DB->close();
                                                        ?>

                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
<?php require_once 'incFooter.fya'; ?>

                </div>

                <?php
                if ($strAdminRoleID == 42) {
                    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                    $_SESSION["non_visiting_page"] = $actual_link;
                }
                ?>
                <script>
                    var data = [
<?php
if (isset($csvdata) && is_array($csvdata) && count($csvdata)) {
    foreach ($csvdata as $key => $value) {
        echo "['" . $key . "','" . $value['name'] . "','" . $value['mobile'] . "','" . $value['remark_type'] . "','" . $value['comment'] . "'],";
    }
}
?>
                    ];
                    function download_csv() {
                        var csv = 'Sr,Customer Name,Customer Mobile,Remark Type,Comment\n';
                        data.forEach(function (row) {
                            csv += row.join(',');
                            csv += "\n";
                        });
                        var hiddenElement = document.createElement('a');
                        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
                        hiddenElement.target = '_blank';
                        hiddenElement.download = 'non_visiting_customer.csv';
                        hiddenElement.click();
                    }
                </script>
                </body>

                </html>		