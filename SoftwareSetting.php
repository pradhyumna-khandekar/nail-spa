<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
	$strPageTitle = "Settings | Nailspa POS";
	$strDisplayTitle = "Manage Settings for Nailspa POS";
	$strMenuID = "2";
	$strMyTable = "tblSettings";
	$strMyTableID = "SettingID";
	$strMyField = "";
	$strMyActionPage = "SoftwareSetting.php";
	$strMessage = "";
	$sqlColumn = "";
	$sqlColumnValues = "";


	
	if ($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$strStep = Filter($_POST["step"]);
		if($strStep=="add")
		{
				
		}
			

		if($strStep=="edit")
		{
			// foreach($_POST as $key => $val)
			// {
				// echo $key." = " .$val."<br>";
			// }
			// foreach($_FILES as $key => $val)
			// {
				// echo $key." = " .$val."<br>";
			// }
			// die();
			
			
			$DB = Connect();
			
			foreach($_POST as $key => $val)
			{
				if($key=="step" || $key==$strMyTableID || $key=="InvoiceImageURL")
				{
				
				}
				else
				{
					$sqlUpdate = "update $strMyTable set $key='$_POST[$key]' WHERE $strMyTableID='".Decode($_POST[$strMyTableID])."'";
					ExecuteNQ($sqlUpdate);
				}
			}
			
			echo('<div class="alert alert-success alert-dismissible fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"></span>
				</button>
				<strong>Settings updated Successfully</strong>
				</div>'); 
		
			
			$DB->close();	
					
		}
		die();
	}	
?>


<!DOCTYPE html>
<html lang="en">

<head>
	<?php require_once("incMetaScript.fya"); ?>
</head>

<body>
    <div id="sb-site">
        
		<?php // require_once("incOpenLayout.fya"); ?>
        <!-- commented by gandhali on8/10/2018-->
		
        <?php require_once("incLoader.fya"); ?>
		
        <div id="page-wrapper">
            <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>
            
				<?php require_once("incLeftMenu.fya"); ?>
			
            <div id="page-content-wrapper">
                <div id="page-content">
                    
					<?php require_once("incHeader.fya"); ?>
					

                    <div id="page-title">
                        <h2><?=$strDisplayTitle?></h2>
                        <!--<p>Add, Edit, Delete Admins and manage them accordingly.</p>-->
                    </div>
<?php

if(!isset($_GET["uid"]))
{
?>								
                    <div class="panel">
						<div class="panel">
							<div class="panel-body">
								<div class="example-box-wrapper">
									<div class="tabs">
										<ul>
											<li><a href="#normal-tabs-1" title="Tab 1">Manage</a></li>
										</ul>
										<div id="normal-tabs-1">
										
											<span class="form_result">&nbsp; <br>
											</span>
											
											<div class="panel-body">
												<h3 class="title-hero">You can control all the settings of POS here...</h3>
												
												
												<form role="form" enctype="multipart/form-data" class="form-horizontal bordered-row enquiry_form" onSubmit="proceed_formsubmit('.enquiry_form', '<?=$strMyActionPage?>', '.result_message', '', '', '' , '.imageupload'); return false;">
													<input type="hidden" name="step" value="edit">
													<span class="result_message">&nbsp; <br>
													</span>
													<br>
													

													
														<div class="example-box-wrapper">
															
					<?php

					$strID = "1";
					$DB = Connect();
					$sql = "SELECT * FROM $strMyTable WHERE $strMyTableID = '$strID'";
					$RS = $DB->query($sql);
					if ($RS->num_rows > 0) 
					{

						while($row = $RS->fetch_assoc())
						{
							foreach($row as $key => $val)
							{
								if($key==$strMyTableID)
								{
					?>
																<input type="hidden" name="<?=$key?>" value="<?=Encode($strID)?>">	

					<?php
								}
								elseif($key=="BusinessName")
								{
					?>	
															
																<div class="form-group"><label class="col-sm-3 control-label"><?=str_replace("BusinessName", "Business Name", $key)?> <span>*</span></label>
																	<div class="col-sm-4"><input class="form-control required" name="<?=$key?>" value="<?=$row[$key]?>"></div>
																</div>	
																
					<?php
								}
								elseif($key=="MasterGSTNo")
								{
					?>	
															
																<div class="form-group"><label class="col-sm-3 control-label"><?=str_replace("MasterGSTNo", "Master GST no", $key)?> <span></span></label>
																	<div class="col-sm-4"><input class="form-control" name="<?=$key?>" value="<?=$row[$key]?>"></div>
																</div>

					<?php
								}
								elseif($key=="CompanyName")
								{
					?>	
															
																<div class="form-group"><label class="col-sm-3 control-label"><?=str_replace("CompanyName", "Company Name", $key)?> <span>*</span></label>
																	<div class="col-sm-4"><input class="form-control required" name="<?=$key?>" value="<?=$row[$key]?>"></div>
																</div>
																
					<?php
								}
								elseif($key=="MasterAccountingCode")
								{
					?>	
															
																<div class="form-group"><label class="col-sm-3 control-label"><?=str_replace("MasterAccountingCode", "Master Accounting Code", $key)?> <span></span></label>
																	<div class="col-sm-4"><input class="form-control" name="<?=$key?>" value="<?=$row[$key]?>"></div>
																</div>
					
					<?php		
								}
								elseif($key=="Address")
								{
					?>
																<div class="form-group"><label class="col-sm-3 control-label"><?=str_replace("Address", "Address", $key)?> <span>*</span></label>
																	<div class="col-sm-6"><textarea name="<?=$key?>" rows="3" class="form-control textarea-no-resize"><?=$row[$key]?></textarea></div>
																</div>
					<?php
								}
								elseif($key=="InvoiceImgURL")
								{
					?>

																	<div class="form-group"><label class="col-sm-3 control-label">Creative Image Invoice <span>*</span></label>
																		<div class="col-sm-6">
																			<img src="images/invoice/<?=$row[$key]?>" alt="Creative image not uploaded properly. Please re-upload" width="300px" height="200px" />
																			<a href="SoftwareSettingsInvoiceCreativeChange.php" class="btn ra-100 btn-primary">Change Invoice Creative</a><br>
																			<span><font color="red">Click to change the Invoice creative image size refrence 900 * 399</font></span>
																		</div>
																	</div>
																	
					<?php
								}
								else
								{
									// Nothing
								}
							}
						}
					?>

																<div class="form-group"><label class="col-sm-3 control-label"></label>
																	<input type="submit" class="btn ra-100 btn-primary" value="Update Settings">
																</div>
					<?php
					}
					$DB->close();
					?>													
															
														</div>
												</form>
												
												
												
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
                    </div>
<?php
} // End null condition
else
{


}
?>	                   
            </div>
        </div>
		
        <?php require_once 'incFooter.fya'; ?>
		
    </div>
</body>

</html>