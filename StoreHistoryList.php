<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Manage Stores | Nailspa";
$strDisplayTitle = "Stores Logs for Nailspa";
$strMenuID = "3";
$strMyTable = "tblStores";
$strMyTableID = "StoreID";
$strMyField = "StoreName";
$strMyActionPage = "StoreHistoryList.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <!-----------css & js files added for tabs by gandhali 3/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");     ?>
            <!----------commented by gandhali 3/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>

                        <script type="text/javascript">
                            /* Datatables responsive */
                            $(document).ready(function () {
                                $('#datatable-responsive-scroll').DataTable({
                                    "scrollX": true
                                });
                            });
                            $(document).ready(function () {
                                $('.dataTables_filter input').attr("placeholder", "Search...");
                            });
                        </script>

                        <?php
                        $DB = Connect();


                        /*
                         * get store history
                         */
                        if (isset($_GET['store_id']) && $_GET['store_id'] != '') {
                            $historyq = "SELECT * FROM tblStores_history WHERE StoreID='" . $_GET['store_id'] . "'";

                            $historyq_exe = $DB->query($historyq);
                            if ($historyq_exe->num_rows > 0) {
                                while ($history_row = $historyq_exe->fetch_assoc()) {
                                    $history_data[] = $history_row;
                                }
                            }
                            if (isset($history_data) && is_array($history_data) && count($history_data) > 0) {
                                /*
                                 * get all users
                                 */
                                $userq = "SELECT * FROM tblAdmin";
                                $userq_exe = $DB->query($userq);
                                if ($userq_exe->num_rows > 0) {
                                    while ($user_row = $userq_exe->fetch_assoc()) {
                                        $user_data[$user_row['AdminID']] = $user_row;
                                    }
                                }
                            }

                            $DB->close();
                        }
                        $counter = 0;
                        ?>
                        <div id="page-title">
                            <h2>All Store Log</h2>
                        </div>

                        <div class="panel">
                            <div class="panel">
                                <div class="panel-body">

                                    <div class="example-box-wrapper">
                                        <div class="tabs">
                                            <div id="normal-tabs-1">
                                                <span class="form_result">&nbsp; <br>
                                                </span>

                                                <div class="panel-body">
                                                    <h3 class="title-hero">List of Stores Logs for POS</h3>
                                                    <div class="example-box-wrapper">
                                                        <table id="datatable-responsive-scroll" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                                                            <thead>
                                                                <tr>
                                                                    <th>Action</th>
                                                                    <th>User</th>
                                                                    <th>Date/time</th>
                                                                    <th>Store Name</th>
                                                                    <th>Billing Email ID</th>
                                                                    <th>Official Email ID</th>
                                                                    <th>Billing Number</th>
                                                                    <th>GST Number</th>
                                                                    <th>Accounting Code</th>
                                                                    <th>Company Name</th>
                                                                    <th>Status</th>
                                                                    <th>Official Address</th>
                                                                    <th>Billing Addresse</th>

                                                                </tr>
                                                            </thead>
                                                            <tfoot>
                                                                <tr>
                                                                    <th>Action</th>
                                                                    <th>User</th>
                                                                    <th>Date/time</th>
                                                                    <th>Store Name</th>
                                                                    <th>Billing Email ID</th>
                                                                    <th>Official Email ID</th>
                                                                    <th>Billing Number</th>
                                                                    <th>GST Number</th>
                                                                    <th>Accounting Code</th>
                                                                    <th>Company Name</th>
                                                                    <th>Status</th>
                                                                    <th>Official Address</th>
                                                                    <th>Billing Addresse</th>

                                                                </tr>
                                                            </tfoot>
                                                            <tbody>
                                                                <?php
                                                                if (isset($history_data) && is_array($history_data) && count($history_data) > 0) {
                                                                    foreach ($history_data as $hkey => $hvalue) {
                                                                        if ($hvalue['history_notes'] == 'insert') {
                                                                            $action_name = "<p style='color:blue;'>Add</p>";
                                                                            $action_responsible = isset($user_data[$hvalue['created_by']]) ? $user_data[$hvalue['created_by']]['AdminFullName'] : '';
                                                                        } else if ($hvalue['history_notes'] == 'update') {
                                                                            $action_name = "<p class='text-success'>Edit</p>";
                                                                            $action_responsible = isset($user_data[$hvalue['modified_by']]) ? $user_data[$hvalue['modified_by']]['AdminFullName'] : '';
                                                                        } else if ($hvalue['history_notes'] == 'delete') {
                                                                            $action_name = "<p class='text-danger'>Delete</p>";
                                                                            $action_responsible = isset($user_data[$hvalue['modified_by']]) ? $user_data[$hvalue['modified_by']]['AdminFullName'] : '';
                                                                        }
                                                                        ?> 
                                                                        <tr id="my_data_tr_<?= $counter ?>">
                                                                            <td><?php echo isset($action_name) ? $action_name : '' ?></td>
                                                                            <td><?php echo isset($action_responsible) ? $action_responsible : '' ?></td>
                                                                            <td><?php echo isset($hvalue['history_timestamp']) ? date('d/m/Y h:i a', strtotime($hvalue['history_timestamp'])) : ''; ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && $history_data[$hkey - 1]['StoreName'] != $hvalue['StoreName']) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . $hvalue['StoreName'] . '</span>';
                                                                                ?></td>

                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && $history_data[$hkey - 1]['StoreOfficialEmailID'] != $hvalue['StoreOfficialEmailID']) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . $hvalue['StoreOfficialEmailID'] . '</span>';
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && $history_data[$hkey - 1]['StoreBillingEmailID'] != $hvalue['StoreBillingEmailID']) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . $hvalue['StoreBillingEmailID'] . '</span>';
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && $history_data[$hkey - 1]['StoreBillingNumber'] != $hvalue['StoreBillingNumber']) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . $hvalue['StoreBillingNumber'] . '</span>';
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && $history_data[$hkey - 1]['GSTNo'] != $hvalue['GSTNo']) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . $hvalue['GSTNo'] . '</span>';
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && $history_data[$hkey - 1]['AccountingCode'] != $hvalue['AccountingCode']) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . $hvalue['AccountingCode'] . '</span>';
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && $history_data[$hkey - 1]['CompanyName'] != $hvalue['CompanyName']) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . $hvalue['CompanyName'] . '</span>';
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && $history_data[$hkey - 1]['Status'] != $hvalue['Status']) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . (isset($hvalue['Status']) && $hvalue['Status'] == 1 ? 'Offline' : 'Live') . '</span>';
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && $history_data[$hkey - 1]['StoreOfficialAddress'] != $hvalue['StoreOfficialAddress']) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . $hvalue['StoreOfficialAddress'] . '</span>';
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && $history_data[$hkey - 1]['StoreBillingAddress'] != $hvalue['StoreBillingAddress']) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . $hvalue['StoreBillingAddress'] . '</span>';
                                                                                ?></td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    $counter++;
                                                                } else {
                                                                    ?>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>
                                                                <?php }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <?php require_once 'incFooter.fya'; ?>
        </div>
    </body>
</html>
