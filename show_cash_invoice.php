<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php
$strPageTitle = "Delete Cash Invoice | NailSpa";
$strDisplayTitle = "Delete Cash Invoice for NailSpa";
$strMenuID = "10";
$strMyTable = "tblAppointments";
$strMyTableID = "AppointmentID";
$strMyField = "AppointmentDate";
$strMyActionPage = "show_cash_invoice.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <?php require_once("incChart-SalonDashboard.fya"); ?>

        <style>
            .btn-danger:hover
            {
                border-color: #fc8213;
                background: #fc8213;
            }

        </style>
        <!-- Styles -->


    </head>

    <body>

        <?php
        if ($_SERVER["REQUEST_METHOD"] == "GET") {
            if (isset($_GET) && is_array($_GET) && count($_GET) > 0) {
                $strtoandfrom = $_GET["toandfrom"];
                $arraytofrom = explode("-", $strtoandfrom);

                $from = $arraytofrom[0];
                $datetime = new DateTime($from);
                $getfrom = $datetime->format('Y-m-d');


                $to = $arraytofrom[1];
                $datetime = new DateTime($to);
                $getto = $datetime->format('Y-m-d');

                $store = $_GET["Store"];

                if (!empty($store)) {
                    $append .= " AND apt.StoreID ='" . $store . "'";
                }
                if (!IsNull($getfrom)) {
                    $append .= " and apt.AppointmentDate >= '" . $getfrom . "'";
                }

                if (!IsNull($getto)) {
                    $append .= " and apt.AppointmentDate <= '" . $getto . "'";
                }

                /*
                 * Get Cash Invoice Bill that has no dependency in future
                 */
                $aptq = "SELECT apt.*,inv.Total,inv.InvoiceId FROM tblAppointments apt "
                        . " JOIN tblInvoiceDetails inv ON (inv.AppointmentId = apt.AppointmentID)"
                        . " WHERE inv.Flag='CS' AND apt.Status = 2 AND apt.IsDeleted = 0 " . $append;
                $aptq_exe = $DB->query($aptq);
                if ($aptq_exe->num_rows > 0) {
                    while ($apt_res = $aptq_exe->fetch_assoc()) {
                        $apt_res_data[$apt_res['AppointmentID']] = $apt_res;
                        $aptids[$apt_res['AppointmentID']] = $apt_res['AppointmentID'];
                    }
                }

                if (isset($aptids) && is_array($aptids) && count($aptids) > 0) {
                    $in_apt_ids = implode(",", $aptids);
                    if ($in_apt_ids != '') {
                        /*
                         * Check invoice is paid or not
                         */
                        $pendq = "SELECT * FROM `tblpendingpayments` WHERE `AppointmentID` IN(" . $in_apt_ids . ")";
                        $pendq_exe = $DB->query($pendq);
                        if ($pendq_exe->num_rows > 0) {
                            while ($pen_res = $pendq_exe->fetch_assoc()) {
                                $pend_apt_ids[$pen_res['AppointmentID']] = $pen_res['AppointmentID'];
                            }
                        }
                    }
                }

                /*
                 * Check Which Bill is dependent and which is not
                 */
                $dependent_total = 0;
                $independent_total = 0;
                $independent_count = 0;

                if (isset($apt_res_data) && is_array($apt_res_data) && count($apt_res_data) > 0) {
                    foreach ($apt_res_data as $akey => $avalue) {
                        /*
                         * If voucher id OR package id OR Pending Bill then dependent bill 
                         */
                        if ($avalue['VoucherID'] != '0' || $avalue['PackageID'] != '0' || isset($pend_apt_ids[$avalue['AppointmentID']])) {
                            $dependent_total += $avalue['Total'];
                        } else {
                            $independent_count += 1;
                            $independent_total += $avalue['Total'];
                            $indep_apt[$avalue['AppointmentID']] = $avalue['AppointmentID'];

                            $indep_apt_data[$avalue['AppointmentID']]['InvoiceId'] = $avalue['InvoiceId'];
                            $indep_apt_data[$avalue['AppointmentID']]['Total'] = $avalue['Total'];
                            $indep_apt_data[$avalue['AppointmentID']]['AppointmentDate'] = $avalue['AppointmentDate'];
                        }
                    }
                }

                $total_cash_invoice = isset($apt_res_data) ? count($apt_res_data) : 0;
            }
        }
        ?>
        <div id="sb-site">


            <?php require_once("incLoader.fya"); ?>


            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>
                        <div class="panel">
                            <div class="panel-body">
                                <div class="example-box-wrapper">
                                    <div id="normal-tabs-1">
                                        <span class="form_result">&nbsp; <br>
                                        </span>



                                        <div class="panel-body">
                                            <form method="get" class="form-horizontal bordered-row" role="form" action="show_cash_invoice.php">
                                                <div class="form-group"><label for="" class="col-sm-4 control-label">Select date</label>
                                                    <div class="col-sm-4">
                                                        <div class="input-prepend input-group">
                                                            <span class="add-on input-group-addon">
                                                                <i class="glyph-icon icon-calendar"></i>
                                                            </span> 
                                                            <input type="text" name="toandfrom" id="daterangepicker-example" class="form-control" value="<?= $strtoandfrom; ?>">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Select Store</label>
                                                    <div class="col-sm-4">
                                                        <select name="Store" class="form-control">
                                                            <option value="0">All</option>
                                                            <?php
                                                            $selp = select("*", "tblStores", "Status='0'");
                                                            foreach ($selp as $val) {
                                                                $strStoreName = $val["StoreName"];
                                                                $strStoreID = $val["StoreID"];
                                                                $store = $_GET["Store"];
                                                                if ($store == $strStoreID) {
                                                                    ?>
                                                                    <option  selected value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <option value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                    <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a class="btn btn-link" href="show_cash_invoice.php">Clear All Filter</a>
                                                    &nbsp;&nbsp;&nbsp;

                                                </div>
                                            </form>
                                            <?php
                                            if (isset($_GET['error_msg']) && $_GET['error_msg'] != '') {
                                                echo "<br><center><h3>" . $_GET['error_msg'] . "</h3></center>";
                                            }
                                            ?>
                                            <div class="form-group">
                                                <?php if ($_GET["toandfrom"] != "" && !IsNull($_GET["Store"])) { ?>

                                                    <div class="col-md-6 col-xs-12 margin-bot-zero no-pad btn" style="border-color: #FC8213;"><b>Total Invoice  : </b><?php echo isset($total_cash_invoice) ? $total_cash_invoice : ''; ?></div>
                                                    <div class="col-md-6 col-xs-12 margin-bot-zero no-pad btn" style="border-color: #dc1156;"><b>Total Sale Amount  : </b><?php echo isset($dependent_total) ? $dependent_total + $independent_total : ''; ?></div>
                                                    <div class="col-md-6 col-xs-12 margin-bot-zero no-pad btn" style="border-color: #FC8213;"><b>Total Independent Invoice  : </b><?php echo isset($independent_count) ? $independent_count : ''; ?></div>
                                                    <div class="col-md-6 col-xs-12 margin-bot-zero no-pad btn" style="border-color: #FC8213;"><b>Independent Sale Amount : </b><?php echo isset($independent_total) ? $independent_total : ''; ?></div>
                                                    <?php
                                                } else {
                                                    echo "<br><center><h3>Please Select Month And Year!</h3></center>";
                                                }
                                                ?>
                                            </div>
                                            <?php if (isset($independent_total) && $independent_total > 0 && $_GET["toandfrom"] != "" && !IsNull($_GET["Store"])) { ?>
                                                <br><br><br>
                                                <div>
                                                    <form method="post" class="form-horizontal bordered-row" role="form" action="delete_cash_invoice.php" id="delete_invoice_form">

                                                        <input type="hidden" name="toandfrom" value="<?php echo isset($_GET['toandfrom']) ? $_GET['toandfrom'] : ''; ?>">
                                                        <input type="hidden" name="Store" value="<?php echo isset($_GET['Store']) ? $_GET['Store'] : ''; ?>">
                                                        <div class="form-group"><label for="" class="col-sm-4 control-label">Enter Amount in Percentage</label>
                                                            <div class="col-sm-4">
                                                                <div class="input-prepend input-group">
                                                                    <input autocomplete="off" type="text" class="form-control percentage" min="1" max="100" required="required" name="percentage"/>
                                                                    <span id="errorMsg" style="display:none;">Percentage should be from 0 to 100</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Delete</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                                            </div>

                                                        </div>

                                                    </form>
                                                </div>
                                            <?php } ?>

                                            <?php if (isset($indep_apt_data) && is_array($indep_apt_data) && count($indep_apt_data) > 0) { ?>
                                                <br><br><br>
                                                <table id="datatable-responsive-scroll" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Sr. No.</th>
                                                            <th>Invoice No.</th>
                                                            <th>Date</th>
                                                            <th>Amount</th>
                                                        </tr>
                                                    </thead>
                                                    <tfoot>
                                                        <tr>
                                                            <th>Sr. No.</th>
                                                            <th>Invoice No.</th>
                                                            <th>Date</th>
                                                            <th>Amount</th>
                                                        </tr>
                                                    </tfoot>
                                                    <tbody>
                                                        <?php
                                                        $counter = 1;
                                                        foreach ($indep_apt_data as $key => $value) {
                                                            ?> 
                                                            <tr>
                                                                <td><?php echo $counter; ?></td>
                                                                <td><?php echo $value['InvoiceId']; ?></td>
                                                                <td><?php echo date('d/m/Y', strtotime($value['AppointmentDate'])); ?></td>
                                                                <td><?php echo $value['Total']; ?></td>
                                                            </tr>
                                                            <?php
                                                            $counter++;
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            <?php }
                                            ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php require_once 'incFooter.fya'; ?>

            </div>
            <script>
                /*  function delete_conf() {
                 var perc = $(".percentage").val();
                 if (perc == '' || perc < 0 || perc > 100) {
                 $('#errorMsg').show();
                 } else {
                 $('#errorMsg').hide();
                 var conf = confirm('Are you sure want to delete?');
                 if (conf) {
                 $("#delete_invoice_form").submit();
                 }
                 }
                 }*/
                $('#delete_invoice_form').submit(function () {
                    var perc = $(".percentage").val();
                    if (perc == '' || perc < 0 || perc > 100) {
                        $('#errorMsg').show();
                        return false;  // <- cancel event
                    } else {
                        $('#errorMsg').hide();

                        var conf = confirm('Are you sure want to delete?');
                        if (conf) {
                            return true;
                        } else {
                            return false;  // <- cancel event
                        }
                    }
                });
                $(function () {

                    $(".percentage").keypress(function () {
                        var perc = $(".percentage").val()
                        var max = 100;
                        var min = 1;
                        $('#errorMsg').hide();
                        if (perc > max)
                        {
                            $('#errorMsg').show();
                            $(this).val('');
                        }
                        else if (perc < min)
                        {
                            $('#errorMsg').show();
                            $(this).val('');
                        }
                    });
                });
            </script>
    </body>

</html>		