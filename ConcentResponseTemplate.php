<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Appointment</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            body{color:#3a020a;;}
            p{margin:3px;}
            tr, td{font-size: 14px;}
            th{color:#795548;padding:5px;}
            h1{margin: 0;text-align:center;font-size:25px;color:#607D8B;}
            h2{font-size:16px;}
            button{font-size: 16px;
                   display: block;
                   margin: 20px 0;
                   background-color: #F44336;
                   color: #fff;
                   padding: 10px;
                   border: none;
                   border-radius: 5px;}
            .main-table{background-color:#fafafa;font-family: "Helvetica" , sans-serif; font-size:14px; margin:0; padding:0;}
            .report-table{border-collapse: collapse;display:block;text-align:center;}
            .report-table tr{border-bottom: 2px solid #795548;}
            .report-table tr td{padding:5px;}
            .content_color{color : #3a020a;}
        </style>
    </head>
    <body>
        <table class="main-table" cellspacing="0" cellpadding="0" border="0" width="100%">
            <tbody><tr>
                    <td align="center" valign="top" style="padding:20px 0 20px 0">
                        <table bgcolor="#FFFFFF" cellspacing="0" cellpadding="10" border="0" width="600" style="border:1px solid #dddddd;
                               BORDER-BOTTOM:#3a020a 1px solid;BORDER-LEFT:#3a020a 1px solid;BORDER-TOP:#3a020a 1px solid;BORDER-RIGHT:#3a020a 1px solid;">
                            <tbody>
                                <tr>
                                    <td colspan="1" style="padding:0px;background-color: #FFF;">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody><tr>
                                                    <td  style="padding:10px;text-align:center;color: #fff;">
                                                        <a href="http://nailspaexperience.com" target="_blank"><img src="http://pos.nailspaexperience.com/og/images/nail_logo_new.png" width="100px"/></a>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                    </td>

                                </tr>

                                <tr style="
                                    /* background: #edf7f0; */
                                    ">
                                    <td colspan="2" style="padding-left: 0px; padding-right: 0px;padding-top: 0px;padding-bottom: 15px;">
                                        <table width="100%" cellspacing="0" cellpadding="0" style="
                                               background: #2a0206;
                                               ">
                                            <tbody><tr>
                                                    <td colspan="2" style="padding: 5px 20px;text-align:center;font-weight:700;/* margin-left: 16px; */">
                                                        <p style="text-transform: uppercase"></p>

                                                    </td>
                                                </tr>
                                            </tbody></table>
                                    </td>
                                </tr>


                                <tr>
                                    <td>
                                        <table border="0" cellspacing="0" cellpadding="0" width="780" align="center">
                                            <tbody>

                                                <tr>
                                                    <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;FONT-SIZE:14px;" bgcolor="#ffffff" width="500" align="left" class="content_color">
                                                        Hey {Name_Detail},
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;FONT-SIZE:14px;" bgcolor="#ffffff" width="500" align="left" class="content_color">
                                                        Consent has been Approved Successfully.
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;FONT-SIZE:14px;" bgcolor="#ffffff" width="500" align="left" class="content_color">
                                                        <br>Warm Regards,
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;FONT-SIZE:14px;" bgcolor="#ffffff" width="500" align="left" class="content_color">
                                                        NS Style Salon
                                                    </td>
                                                </tr>


                                            </tbody>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2" style="padding: 0px;background: #3a020a;">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody><tr>
                                                    <td colspan="2" style="background-color: #3a020a;padding: 15px 0;text-align: center;color: #fff;">
                                                        <p class="text-center" style="font-size:14px;"></p>
                                                        <p style="color:#dcc36f;">11+ LOCATIONS | T1 & T2 MUMBAI AIRPORT &nbsp;| MUMBAI | KOCHI </p>
                                                        <p style="color: #dcc36f; text-align:center;"><strong>GO GREEN GO PAPERLESS</strong></p>

                                                    </td>
                                                </tr>
                                            </tbody></table>
                                    </td>
                                </tr>
                            </tbody></table>
                    </td>
                </tr>
            </tbody></table>
    </body>
</html>