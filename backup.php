<?php

// old server mysql id
$DBIUser = 'root';
$DBIPass = '';
$oldServer = 'localhost';
$originalDB = 'nailspa';

// new server mysql id
$NewUser = 'root';
$NewPass = '';
$newServer = 'localhost';
$newDB = 'nailspa_backup';

$dbold = mysql_connect($oldServer, $DBIUser, $DBIPass);
$db_check = mysql_select_db($originalDB, $dbold);


$sql = "SHOW TABLES FROM $originalDB";
$getTables = mysql_query($sql);

if (!$getTables) {
    echo "DB Error, could not list tables\n";
    echo 'MySQL Error: ' . mysql_error();
    exit;
} else {
    while ($row = mysql_fetch_row($getTables)) {
        $originalDBs[] = $row[0];
    }
}

if (isset($originalDBs) && is_array($originalDBs) && count($originalDBs) > 0) {
    $dbnew = mysql_connect($newServer, $NewUser, $NewPass);
    $dbnew_check = mysql_select_db($newDB, $dbnew);
    foreach ($originalDBs as $key => $tab) {
        $create_sql = "CREATE TABLE $tab LIKE " . $originalDB . "." . $tab;
        $create_sql_exe = mysql_query($create_sql);
    }
}

/*
 * Insert Record
 */
$conn = new mysqli($oldServer, $DBIUser, $DBIPass, $originalDB);
$get_record_sql = "SELECT * FROM " . $originalDB . ".`calendar_event`";
$get_sql_exe = $conn->query($get_record_sql);
while ($row_data = $get_sql_exe->fetch_assoc()) {
    $insert_data[] = $row_data;
}

if (isset($insert_data) && is_array($insert_data) && count($insert_data) > 0) {
    foreach ($insert_data as $ikey => $ivalue) {
        foreach ($ivalue as $ickey => $icvalue) {
            $col[$ickey] = $ickey;
            $row_value[$ikey][$ickey] = "'" . $icvalue . "'";
        }
    }
}

$replace_query = "REPLACE INTO calendar_event (" . implode(",", $col) . ") VALUES";
if (isset($row_value) && is_array($row_value) && count($row_value) > 0) {
    foreach ($row_value as $rkey => $rvalue) {
        if ((count($row_value) - 1) == $rkey) {
            $replace_query .= " (" . implode(",", $rvalue) . ");";
        } else {
            $replace_query .= " (" . implode(",", $rvalue) . "),";
        }
    }
}

echo $replace_query;
$new_conn = new mysqli($newServer, $NewUser, $NewPass, $newDB);
$get_sql_exe = $new_conn->query($replace_query);
echo '<pre>';
print_r($get_sql_exe);
exit;
?>