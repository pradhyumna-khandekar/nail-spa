<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Manage Store Opening/Closing | Nailspa";
$strDisplayTitle = "Manage Store Opening/Closing | Nailspa";
$strMenuID = "3";
$strMyActionPage = "admin_store_manage.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <!-----------css & js files added for tabs by gandhali 3/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>
        <?php /* <script type="text/javascript" src="assets/widgets/modal/modal.js"></script> */ ?>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");     ?>
            <!----------commented by gandhali 3/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>


                        <div class="panel">
                            <div class="panel-body">

                                <div class="panel-body">
                                    <h3 class="title-hero"><?php echo $strDisplayTitle; ?></h3>

                                    <span class="form_result">&nbsp; <br></span>

                                    <table id="datatable-responsive" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sr. No.</th>
                                                <th>Store Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Sr. No.</th>
                                                <th>Store Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <?php
                                            $DB = Connect();
                                            $all_store = select("*", "tblStores", "Status = 0");

                                            $today_date = date('y-m-d');
                                            $open_close_data = select("*", "tblOpenNClose", "DateNTime = '" . $today_date . "'");
                                            if (isset($open_close_data) && is_array($open_close_data) && count($open_close_data) > 0) {
                                                foreach ($open_close_data as $okey => $ovalue) {
                                                    $store_open_data[$ovalue['StoreID']] = $ovalue;
                                                }
                                            }

                                            $count = 1;
                                            if (isset($all_store) && is_array($all_store) && count($all_store) > 0) {
                                                foreach ($all_store as $key => $value) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $count; ?></td>
                                                        <td><?php echo $value['StoreName']; ?></td>
                                                        <td>
                                                            <?php
                                                            if (isset($store_open_data[$value['StoreID']])) {
                                                                $atten_data = $store_open_data[$value['StoreID']];
                                                                if ($atten_data['OpenTime'] == '0000-00-00 00:00:00') {
                                                                    ?>
                                                                    <a class="btn btn-xs btn-info" onclick="return confirm('Are you sure you want to Open <?php echo $value['StoreName'] ?>?')" href="admin_open_store.php?Store=<?= $value['StoreID'] ?>">Day Opening</a>
                                                                <?php } else if ($atten_data['CloseTime'] == '0000-00-00 00:00:00' && $atten_data['OpenTime'] != '0000-00-00 00:00:00') {
                                                                    ?>
                                                                    <p>Opening Time : <?php echo date('d M,Y h:i a', strtotime($atten_data['OpenTime'])) ?></p>
                                                                    <a class="btn btn-xs btn-success" onclick="return confirm('Are you sure you want to Close <?php echo $value['StoreName'] ?>?')" href="EnvelopeCreateandVerify.php?sid=<?= $value['StoreID'] ?>&Store=<?= $value['StoreID'] ?>">Day Closing</a>
                                                                <?php } else if ($atten_data['CloseTime'] != '0000-00-00 00:00:00' && $atten_data['OpenTime'] != '0000-00-00 00:00:00') {
                                                                    ?>
                                                                    <p>Opening Time : <?php echo date('d M,Y h:i a', strtotime($atten_data['OpenTime'])) ?></p>
                                                                    <p>Closing Time : <?php echo date('d M,Y h:i a', strtotime($atten_data['CloseTime'])) ?></p>
                                                                <?php } else {
                                                                    ?>
                                                                    <p>No Record Found.</p>
                                                                    <?php
                                                                }
                                                            } else {
                                                                ?>
                                                                <a class="btn btn-xs btn-info" onclick="return confirm('Are you sure you want to Open <?php echo $value['StoreName'] ?>?')" href="admin_open_store.php?Store=<?= $value['StoreID'] ?>">Day Opening</a>
                                                            <?php }
                                                            ?>



                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $count++;
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <?php require_once 'incFooter.fya'; ?>
        </div>

    </body>
</html>