<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Employee Performance Report | Nailspa";
$strDisplayTitle = "Employee Performance Report of Nailspa Experience";
$strMenuID = "2";
$strMyTable = "tblStoreStock";
$strMyTableID = "StoreStockID";
$strMyField = "";
$strMyActionPage = "ReportEmployeePerformance.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights	



if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $strStep = Filter($_POST["step"]);

    if ($strStep == "add") {
        
    }

    if ($strStep == "edit") {
        
    }
}
?>

<?php
if (isset($_GET["toandfrom"])) {

    $all_offers = select("OfferID,OfferName,OfferCode,Type,TypeAmount", "tblOffers", "OfferID >0");

    if (isset($all_offers) && is_array($all_offers) && count($all_offers) > 0) {
        foreach ($all_offers as $offkey => $offvalue) {
            $all_offers_name[$offvalue['OfferID']] = $offvalue;
        }
    }

    $strtoandfrom = $_GET["toandfrom"];
    $arraytofrom = explode("-", $strtoandfrom);

    $from = $arraytofrom[0];
    $datetime = new DateTime($from);
    $getfrom = $datetime->format('Y-m-d');


    $to = $arraytofrom[1];
    $datetime = new DateTime($to);
    $getto = $datetime->format('Y-m-d');

    if (!IsNull($from)) {
        $sqlTempfrom = " and Date(tblInvoiceDetails.OfferDiscountDateTime)>=Date('" . $getfrom . "')";
        $sqlTempfrom1 = " and Date(tblAppointments.AppointmentDate)>=Date('" . $getfrom . "')";
        $sqlTempfrom2 = " and Date(tblCustomers.RegDate)>=Date('" . $getfrom . "')";
    }

    if (!IsNull($to)) {
        $sqlTempto = " and Date(tblInvoiceDetails.OfferDiscountDateTime)<=Date('" . $getto . "')";
        $sqlTempto1 = " and Date(tblAppointments.AppointmentDate)<=Date('" . $getto . "')";
        $sqlTempto2 = " and Date(tblCustomers.RegDate)<=Date('" . $getto . "')";
    }
}


if (isset($_GET["store"])) {
    $store = $_GET["store"];

    if (!IsNull($store)) {
        $sqlstore = " AND tblEmployees.StoreID='" . $store . "'";
    }
}
?>	


<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>

        <script type="text/javascript" src="assets/widgets/datepicker/datepicker.js"></script>
        <script type="text/javascript">
            /* Datepicker bootstrap */

            $(function () {
                "use strict";
                $('.bootstrap-datepicker').bsdatepicker({
                    format: 'mm-dd-yyyy'
                });
            });
            function printDiv(divName)
            {

                var divToPrint = document.getElementById("printdata");
                var htmlToPrint = '' +
                        '<style type="text/css">' +
                        'table th, table td {' +
                        'border:1px solid #000;' +
                        'padding;0.5em;' +
                        '}' +
                        '</style>';
                htmlToPrint += divToPrint.outerHTML;
                newWin = window.open("");
                newWin.document.write(htmlToPrint);
                newWin.print();
                newWin.close();
                // var printContents = document.getElementById(divName);
                // var originalContents = document.body.innerHTML;

                // document.body.innerHTML = printContents;

                // window.print();

                // document.body.innerHTML = originalContents; 
            }

            function GetCustomerDetail(spanId) {
                StartLoading();
                var CustomerIds = $("." + spanId).text();
                //console.log(CustomerIds);return;
                $.ajax({
                    type: "POST",
                    data: {"customer_ids": CustomerIds},
                    url: 'get_customer_data.php',
                    success: function (data) {
                        EndLoading();
                        $('#CustomerDataModal .modal-dialog').html();//clear previous html data
                        $('#CustomerDataModal').show();//
                        $('#CustomerDataModal .modal-dialog').html(data);
                        $('#CustomerDataModal').modal();
                    }
                });
                EndLoading();
            }

        </script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker-demo.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/moment.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
<?php /* <script type="text/javascript" src="assets/widgets/modal/modal.js"></script> */ ?>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");?>
            <!----------commented by gandhali 5/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>


                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                        </div>

                        <div class="panel">
                            <div class="panel">
                                <div class="panel-body" style="overflow-x: scroll;">

                                    <div class="example-box-wrapper">
                                        <div class="scroll-columns">
                                            <div class="tabs">
                                                <div id="normal-tabs-1">
                                                    <span class="form_result">&nbsp; <br>
                                                    </span>
                                                    <div class="panel-body">
                                                        <div id="CustomerDataModal" class="modal fade" role="dialog">
                                                            <div class="modal-dialog">
                                                            </div>
                                                        </div>
                                                        <h3 class="title-hero">Employee Performance</h3>
                                                        <form method="get" class="form-horizontal bordered-row" role="form">
                                                            <div class="form-group"><label for="" class="col-sm-4 control-label">Select date</label>
                                                                <div class="col-sm-4">
                                                                    <div class="input-prepend input-group">
                                                                        <span class="add-on input-group-addon">
                                                                            <i class="glyph-icon icon-calendar"></i>
                                                                        </span> 
                                                                        <input type="text" name="toandfrom" id="daterangepicker-example" class="form-control" value="<?= $strtoandfrom ?>">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label">Select Store</label>
                                                                <div class="col-sm-4">
                                                                    <select name="Store" class="form-control">
                                                                        <option value="0">All</option>
                                                                        <?php
                                                                        $selp = select("*", "tblStores", "Status='0'");
                                                                        foreach ($selp as $val) {
                                                                            $strStoreName = $val["StoreName"];
                                                                            $strStoreID = $val["StoreID"];
                                                                            $store = $_GET["Store"];
                                                                            if ($store == $strStoreID) {
                                                                                ?>
                                                                                <option  selected value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                                <?php
                                                                            } else {
                                                                                ?>
                                                                                <option value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="form-group"><label for="" class="col-sm-4 control-label">Select Employee</label>
                                                                <div class="col-sm-4">
                                                                    <select class="form-control required"  name="store">
                                                                        <option value=''>All</option>
                                                                        <?php
                                                                        $strStatement = "";
                                                                        if ($strStore == '0') {
                                                                            $strStatement = "";
                                                                        } else {
                                                                            $strStatement = " and StoreID='$strStore'";
                                                                        }

                                                                        $selp = select("*", "tblEmployees", "Status='0' $strStatement");
                                                                        foreach ($selp as $val) {
                                                                            $EIDD = $val["EID"];
                                                                            $EMPNAME = $val["EmployeeName"];
                                                                            $EID = $_GET["store"];
                                                                            if ($EID == $EIDD) {
                                                                                $selpT = select("*", "tblEmployees", "EID='" . $EID . "'");
                                                                                $EmployeeName = $selpT[0]['EmployeeName'];
                                                                                ?>
                                                                                <option  selected value="<?= $EID ?>" ><?= $EmployeeName ?></option>														
                                                                                <?php
                                                                            } else {
                                                                                if ($EIDD == "35" || $EIDD == "8" || $EIDD == "6" || $EIDD == "34" || $EIDD == "22" || $EIDD == "49" || $EIDD == "43") {
                                                                                    // List of managers, HO and Audit whose details need not to be shown
                                                                                } else {
                                                                                    ?>
                                                                                    <option value="<?= $EIDD ?>"><?= $EMPNAME ?></option>
                                                                                    <?php
                                                                                }
                                                                                ?>

                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label">Select Percentage</label>
                                                                <div class="col-sm-4">
                                                                    <?php
                                                                    $per = $_GET["per"];
                                                                    ?>
                                                                    <select name="per" class="form-control">
                                                                        <option value="0" <?php if ($per == '0') { ?> selected <?php } ?>>Without Percentage</option>
                                                                        <option value="1" <?php if ($per == '1') { ?> selected <?php } ?>>Percentage</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                        <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                                        &nbsp;&nbsp;&nbsp;
                                                        <a class="btn btn-link" href="ReportEmployeePerformance.php">Clear All Filter</a>
                                                        &nbsp;&nbsp;&nbsp;
                                                        <?php
                                                        $datedrom = $_GET["toandfrom"];
                                                        if ($datedrom != "") {
                                                            ?>
                                                            <button onclick="printDiv('printarea')" class="btn btn-blue-alt">Print<div class="ripple-wrapper"></div></button>
                                                            <?php
                                                        }
                                                        ?>

                                                    </div>

                                                    </form>

                                                    <br>
                                                    <div id="printdata">	
                                                        <?php
                                                        $dtype = $_GET["discounttype"];
                                                        $per = $_GET["per"];
                                                        $datedrom = $_GET["toandfrom"];
                                                        if ($datedrom != "") {
                                                            $EID = $_GET["store"];
                                                            if ($EID == '0') {
                                                                $emp_id = 'All';
                                                            } else {
                                                                $selpT = select("*", "tblEmployees", "EID='" . $EID . "'");
                                                                $EmployeeName = $selpT[0]['EmployeeName'];
                                                                $emp_id = $EmployeeName;
                                                            }
                                                            ?>
                                                            <h3 class="title-hero">Date Range selected : FROM - <?= $getfrom ?> / TO - <?= $getto ?> / Store - <?= $emp_id ?></h3>

                                                            <br>


                                                            <div class="example-box-wrapper">
                                                                <table  class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                                                                    <thead>
                                                                        <tr>

                                                                            <th style="text-align:center">Employee Name</th>
                                                                            <th style="text-align:center">Total Sale</th>
                                                                            <th style="text-align:center">Service Category Wise</th>
                                                                            <th style="text-align:center">Service Count</th>
                                                                            <th style="text-align:center">Avg Service Time</th>
                                                                            <th style="text-align:center">New Customer Count</th>
                                                                            <th style="text-align:center">Repeat Customer Count</th>
                                                                            <th style="text-align:center">Non-Repeat Customer Count</th>
                                                                            <th style="text-align:center">Total Customer</th>
                                                                            <?php
                                                                            if ($per != '0') {
                                                                                ?>
                                                                                <th style="text-align:center">New Customer %</th>
                                                                                <?php
                                                                            }
                                                                            ?>

                                                                            <?php
                                                                            if ($per != '0') {
                                                                                ?>
                                                                                <th style="text-align:center">Repeat Customer %</th>
                                                                                <?php
                                                                            }
                                                                            ?>


                                                                            <th style="text-align:center">Attendance Present Count</th>
                                                                            <th style="text-align:center">Salary Cost as per attendance</th>
                                                                            <th style="text-align:center">Comm Cost</th>
                                                                            <th style="text-align:center">Product Cost</th>
                                                                            <th style="text-align:center">Free Service Count</th>
                                                                            <th style="text-align:center">ARPU</th>
                                                                            <?php /*  <th style="text-align:center">Emp Rating</th> */ ?>
                                                                        </tr>
                                                                    </thead>

                                                                    <tbody>

                                                                        <?php
                                                                        $DB = Connect();
                                                                        $per = $_GET["per"];
                                                                        $EmployeeID = $_GET["store"];
                                                                        if (!empty($EmployeeID)) {
                                                                            $sql = "select EID, EmployeeName, EmployeeEmailID, EmpPercentage, EmployeeMobileNo,EmployeeCode,StoreID from tblEmployees where Status='0' and EID='" . $EmployeeID . "'";
                                                                        } else {
                                                                            $sql = "select EID, EmployeeName, EmployeeEmailID, EmpPercentage, EmployeeMobileNo,EmployeeCode,StoreID from tblEmployees where Status='0'";
                                                                        }
                                                                        $total_service_count = 0;

                                                                        /*
                                                                         * get all employees
                                                                         */
                                                                        $RS = $DB->query($sql);
                                                                        if ($RS->num_rows > 0) {
                                                                            while ($row = $RS->fetch_assoc()) {
                                                                                $all_emp_name[] = $row['EID'];
                                                                            }
                                                                        }

                                                                        $emp_count = count($all_emp_name);

                                                                        $page_limit = 100;

                                                                        if (isset($_GET['page']) && $_GET['page'] != '') {
                                                                            $page_offset = $_GET['page'] * $page_limit;
                                                                            $search_emp = array_slice($all_emp_name, $page_offset, $page_limit, true);

                                                                            $get_qry = "";
                                                                            if (isset($_GET['toandfrom']) && $_GET['toandfrom'] != '') {
                                                                                $get_qry .= "?toandfrom=" . $_GET['toandfrom'];
                                                                            }
                                                                            if (isset($_GET['store']) && $_GET['store'] != '') {
                                                                                $get_qry .= "&store=" . $_GET['store'];
                                                                            }
                                                                            if (isset($_GET['per']) && $_GET['per'] != '') {
                                                                                $get_qry .= "&per=" . $_GET['per'];
                                                                            }
                                                                            $previous_key = $page_offset - $page_limit;
                                                                            if (isset($all_emp_name[$previous_key])) {
                                                                                $page_qry = "&page=" . ($_GET['page'] - 1);
                                                                                //echo " <a class='btn btn-blue-alt' href='ReportEmployeePerformance.php" . $get_qry . $page_qry . "'>Previous</a>";
                                                                            }

                                                                            $next_key = $page_offset + $page_limit;
                                                                            if (isset($all_emp_name[$next_key])) {
                                                                                $page_qry = "&page=" . ($_GET['page'] + 1);
                                                                                //echo " <a class='btn btn-blue-alt' href='ReportEmployeePerformance.php" . $get_qry . $page_qry . "'>Next</a>";
                                                                            }
                                                                        } else {
                                                                            $search_emp = array_slice($all_emp_name, 0, $page_limit, true);

                                                                            if (count($all_emp_name) > 1) {
                                                                                $next_id = 1;
                                                                                $get_qry = "";
                                                                                if (isset($_GET['toandfrom']) && $_GET['toandfrom'] != '') {
                                                                                    $get_qry .= "?toandfrom=" . $_GET['toandfrom'];
                                                                                }
                                                                                if (isset($_GET['store']) && $_GET['store'] != '') {
                                                                                    $get_qry .= "&store=" . $_GET['store'];
                                                                                }
                                                                                if (isset($_GET['per']) && $_GET['per'] != '') {
                                                                                    $get_qry .= "&per=" . $_GET['per'];
                                                                                }

                                                                                //echo "<a class='btn btn-blue-alt' href='ReportEmployeePerformance.php" . $get_qry . "&page=" . $next_id . "'>Next</a>";
                                                                            }
                                                                        }


                                                                        if (isset($search_emp) && is_array($search_emp) && count($search_emp) > 0) {
                                                                            $page_emp_ids = implode(",", $search_emp);
                                                                            $emp_sql = "select EID, EmployeeName, EmployeeEmailID, EmpPercentage, EmployeeMobileNo,EmployeeCode,StoreID from tblEmployees where Status='0'"
                                                                                    . " AND EID IN(" . $page_emp_ids . ")";
                                                                            $emp_sql_exe = $DB->query($emp_sql);
                                                                            if ($emp_sql_exe->num_rows > 0) {
                                                                                while ($row = $emp_sql_exe->fetch_assoc()) {
                                                                                    $all_emp_data[$row["EID"]] = $row;
                                                                                    $all_emp_code[$row["EID"]] = $row["EmployeeCode"];
                                                                                    $all_emp_ids[$row["EmployeeCode"]] = $row["EID"];
                                                                                }
                                                                            }
                                                                        }





                                                                        /*
                                                                         * get Attendance Present Count
                                                                         */
                                                                        if (isset($all_emp_code) && is_array($all_emp_code) && count($all_emp_code) > 0) {
                                                                            $in_emp_code = implode(",'", $all_emp_code);
                                                                            if ($in_emp_code != '') {
                                                                                $in_emp_code = "'" . $in_emp_code . "'";

                                                                                $monthNum = date("m", strtotime($getfrom));
                                                                                $monthNum1 = date("m", strtotime($getto));

                                                                                $monthName = date("F", mktime(0, 0, 0, $monthNum, 10));
                                                                                $monthName1 = date("F", mktime(0, 0, 0, $monthNum1, 10));
                                                                                $finalmonth = $monthName . "," . $monthName1;
                                                                                $attm = explode(",", $finalmonth);
                                                                                $attancemon = array_unique($attm);

                                                                                for ($i = 0; $i < count($attancemon); $i++) {
                                                                                    $atte_present_qry = "SELECT * FROM `tblAttendanceRecord` WHERE `EmployeeCode` IN (" . $in_emp_code . ") AND RecordMonth='" . $attancemon[$i] . "'";
                                                                                    $atten_exe = $DB->query($atte_present_qry);

                                                                                    if ($atten_exe->num_rows > 0) {
                                                                                        while ($stutrq = $atten_exe->fetch_assoc()) {
                                                                                            $CNTSER = $stutrq['Data'];
                                                                                            $cntatt = explode(",", $CNTSER);
                                                                                            if (!empty($cntatt)) {
                                                                                                $emp_code_empid = isset($all_emp_ids[$stutrq['EmployeeCode']]) ? $all_emp_ids[$stutrq['EmployeeCode']] : 0;
                                                                                                if (isset($atten_pre_data[$emp_code_empid])) {
                                                                                                    $atten_pre_data[$emp_code_empid] = $atten_pre_data[$emp_code_empid] + 1;
                                                                                                } else {
                                                                                                    $atten_pre_data[$emp_code_empid] = 1;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }



                                                                        /*
                                                                         * get free service count
                                                                         */
                                                                        if (isset($all_emp_ids) && is_array($all_emp_ids) && count($all_emp_ids) > 0) {
                                                                            $in_emp_ids = implode(",", $all_emp_ids);
                                                                            if ($in_emp_ids != '') {
                                                                                $cntFreeService = "SELECT COUNT( tblAppointmentAssignEmployee.FreeService ) AS FreeService,tblAppointmentAssignEmployee.MECID FROM tblAppointmentAssignEmployee LEFT JOIN tblAppointments ON tblAppointments.AppointmentID = tblAppointmentAssignEmployee.AppointmentID WHERE tblAppointmentAssignEmployee.FreeService =  '1' AND tblAppointmentAssignEmployee.MECID IN( $in_emp_ids ) $sqlTempfrom1 $sqlTempto1 ";
                                                                                $RSFREE = $DB->query($cntFreeService);
                                                                                if ($RSFREE->num_rows > 0) {
                                                                                    $cnt = 0;
                                                                                    while ($rowFree = $RSFREE->fetch_assoc()) {
                                                                                        $FreeServicecount_data[$rowFree["MECID"]] = $rowFree["FreeService"];
                                                                                    }
                                                                                }
                                                                            }
                                                                        }



                                                                        /*
                                                                         * get employee sales data
                                                                         */
                                                                        if (isset($all_emp_ids) && is_array($all_emp_ids) && count($all_emp_ids) > 0) {
                                                                            $in_emp_ids = implode(",", $all_emp_ids);
                                                                            if ($in_emp_ids != '') {
                                                                                $store_append = '';
                                                                                if (isset($_GET['Store']) && !empty($_GET['Store'])) {
                                                                                    $store_append = " AND tblAppointments.StoreID='" . $_GET['Store'] . "'";
                                                                                }

                                                                                $appointment_q = "SELECT *,AppointmentID as AppointmentId FROM tblAppointments "
                                                                                        . " WHERE AppointmentCheckInTime!='00:00:00' AND status=2 AND IsDeleted = 0  "
                                                                                        . " AND AppointmentDate!='' $sqlTempfrom1 $sqlTempto1 " . $store_append;

                                                                                //echo $appointment_q;exit;
                                                                                $appointment_q_exe = $DB->query($appointment_q);
                                                                                if ($appointment_q_exe->num_rows > 0) {
                                                                                    while ($appointment_result = $appointment_q_exe->fetch_assoc()) {
                                                                                        $invoice_apt_ids[] = $appointment_result['AppointmentId'];
                                                                                        $invice_detail_data[$appointment_result['AppointmentId']] = $appointment_result;
                                                                                    }


                                                                                    if (isset($invoice_apt_ids) && is_array($invoice_apt_ids) && count($invoice_apt_ids) > 0) {
                                                                                        $inv_apt_ids = implode(",", $invoice_apt_ids);
                                                                                        if ($inv_apt_ids != '') {
                                                                                            $apt_emp_ass_q = "SELECT tblAppointmentAssignEmployee.MECID,tblAppointmentAssignEmployee.QtyParam,tblAppointmentAssignEmployee.AppointmentID, tblAppointmentAssignEmployee.Qty, tblAppointmentAssignEmployee.ServiceID, tblAppointmentAssignEmployee.Commission, tblAppointmentAssignEmployee.QtyParam"
                                                                                                    . " FROM tblAppointmentAssignEmployee "
                                                                                                    . " WHERE tblAppointmentAssignEmployee.MECID IN(" . $in_emp_ids . ") AND tblAppointmentAssignEmployee.AppointmentID IN (" . $inv_apt_ids . ")";
                                                                                            //. "  group by tblAppointmentAssignEmployee.AppointmentID,ServiceID,QtyParam";
                                                                                            $apt_emp_ass_q_exe = $DB->query($apt_emp_ass_q);
                                                                                            if ($apt_emp_ass_q_exe->num_rows > 0) {
                                                                                                while ($emp_assign_result = $apt_emp_ass_q_exe->fetch_assoc()) {
                                                                                                    $invoice_apt_data[] = $emp_assign_result;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }


                                                                                    if (isset($invoice_apt_data) && is_array($invoice_apt_data) && count($invoice_apt_data) > 0) {
                                                                                        foreach ($invoice_apt_data as $reskey => $resvalue) {
                                                                                            $all_sales_data[$resvalue['MECID']][] = array(
                                                                                                'EID' => $resvalue['MECID'],
                                                                                                'CustomerID' => isset($invice_detail_data[$resvalue['AppointmentID']]) ? $invice_detail_data[$resvalue['AppointmentID']]['CustomerID'] : 0,
                                                                                                'AppointmentID' => $resvalue['AppointmentID'],
                                                                                                'Qty' => $resvalue['Qty'],
                                                                                                'ServiceID' => $resvalue['ServiceID'],
                                                                                                'Commission' => $resvalue['Commission'],
                                                                                                'QtyParam' => $resvalue['QtyParam'],
                                                                                                'OfferDiscountDateTime' => isset($invice_detail_data[$resvalue['AppointmentID']]) ? $invice_detail_data[$resvalue['AppointmentID']]['OfferDiscountDateTime'] : 0,
                                                                                                'AppointmentDate' => isset($invice_detail_data[$resvalue['AppointmentID']]) ? $invice_detail_data[$resvalue['AppointmentID']]['AppointmentDate'] : 0,
                                                                                                'StoreID' => isset($all_emp_data[$resvalue['MECID']]) ? $all_emp_data[$resvalue['MECID']]['StoreID'] : 0,
                                                                                            );

                                                                                            $cust_ids = $invice_detail_data[$resvalue['AppointmentID']]['CustomerID'];
                                                                                            $customer_ids[$cust_ids] = $cust_ids;
                                                                                            $appointment_ids[$resvalue['AppointmentID']] = $resvalue['AppointmentID'];
                                                                                            $service_ids[$resvalue['MECID']][] = $resvalue['ServiceID'];
                                                                                            $store_id = isset($all_emp_data[$resvalue['MECID']]) ? $all_emp_data[$resvalue['StoreID']] : 0;
                                                                                            $store_ids[$store_id] = $store_id;
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }

                                                                        /*
                                                                         * get service amt
                                                                         */

                                                                        if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                                                                            $apt_in_ids = implode(",", $appointment_ids);
                                                                            $aptdate_q = "SELECT ServiceAmount,AppointmentID,ServiceID FROM tblAppointmentsDetailsInvoice WHERE AppointmentID IN(" . $apt_in_ids . ")";
                                                                            $aptdate_exe = $DB->query($aptdate_q);
                                                                            while ($aptdatedetails = $aptdate_exe->fetch_assoc()) {
                                                                                $all_apt_service_amt[] = $aptdatedetails;
                                                                            }
                                                                            if (isset($all_apt_service_amt) && is_array($all_apt_service_amt) && count($all_apt_service_amt) > 0) {
                                                                                foreach ($all_apt_service_amt as $servdkey => $servvalue) {
                                                                                    $all_ser_amt[$servvalue['AppointmentID']][$servvalue['ServiceID']] = $servvalue['ServiceAmount'];
                                                                                }
                                                                            }
                                                                        }


                                                                        /*
                                                                         * Get Customer Appointment Count
                                                                         */
                                                                        if (isset($customer_ids) && is_array($customer_ids) && count($customer_ids) > 0) {
                                                                            $cust_in_ids = implode(",", $customer_ids);
                                                                            $apt_countQ = "SELECT COUNT(AppointmentId) as apt_count,CustomerID FROM tblAppointments "
                                                                                    . " WHERE AppointmentCheckInTime!='00:00:00' AND status=2 AND CustomerID IN(" . $cust_in_ids . ")"
                                                                                    . " GROUP BY CustomerID";
                                                                            $apt_count_exe = $DB->query($apt_countQ);
                                                                            while ($count_data = $apt_count_exe->fetch_assoc()) {
                                                                                $customer_apt_count[$count_data['CustomerID']] = $count_data['apt_count'];
                                                                                if ($count_data['apt_count'] > 1) {
                                                                                    $old_customer_id_data[$count_data['CustomerID']] = $count_data['CustomerID'];
                                                                                }
                                                                            }
                                                                        }

                                                                        /*
                                                                         * Get Old Customer Last Appointment Date 
                                                                         */
                                                                        if (isset($old_customer_id_data) && is_array($old_customer_id_data) && count($old_customer_id_data) > 0) {
                                                                            $old_customer_id = implode(",", $old_customer_id_data);
                                                                            $appendq = '';
                                                                            if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                                                                                $apt_in_ids = implode(",", $appointment_ids);
                                                                                if ($apt_in_ids != '') {
                                                                                    $appendq .= " AND AppointmentID NOT IN(" . $apt_in_ids . ")";
                                                                                }
                                                                            }
                                                                            $apt_dateQ = "SELECT AppointmentID,AppointmentDate,CustomerID FROM tblAppointments "
                                                                                    . " WHERE AppointmentID IN (SELECT AppointmentID FROM tblAppointments "
                                                                                    . " WHERE Status =2 AND  AppointmentCheckInTime!='00:00:00' AND CustomerID IN(" . $old_customer_id . ")"
                                                                                    . $appendq . " GROUP by CustomerID "
                                                                                    . " ORDER by AppointmentDate DESC)";
                                                                            $apt_date_exe = $DB->query($apt_dateQ);
                                                                            while ($apt_date_data = $apt_date_exe->fetch_assoc()) {
                                                                                $customer_appoint_date[$apt_date_data['CustomerID']]['apt_id'] = $apt_date_data['AppointmentID'];
                                                                                $customer_appoint_date[$apt_date_data['CustomerID']]['apt_date'] = $apt_date_data['AppointmentDate'];
                                                                                $last_apt_arr[$apt_date_data['AppointmentID']] = $apt_date_data['AppointmentID'];
                                                                            }

                                                                            /*
                                                                             * get last appointment employee
                                                                             */
                                                                            if (isset($last_apt_arr) && is_array($last_apt_arr) && count($last_apt_arr) > 0) {
                                                                                $last_apt_in_ids = implode(",", $last_apt_arr);
                                                                                if ($last_apt_in_ids != '') {
                                                                                    $last_empQ = "SELECT * FROM tblAppointmentAssignEmployee "
                                                                                            . " WHERE AppointmentID IN(" . $last_apt_in_ids . ")";
                                                                                    $last_emp_exe = $DB->query($last_empQ);
                                                                                    while ($last_apt_data = $last_emp_exe->fetch_assoc()) {
                                                                                        $last_emp_res[$last_apt_data['AppointmentID']][$last_apt_data['MECID']] = $last_apt_data['MECID'];
                                                                                    }


                                                                                    /* if (isset($last_emp) && is_array($last_emp) && count($last_emp) > 0) {
                                                                                      foreach ($last_emp as $lkey => $lvalue) {
                                                                                      $last_emp_res[$lvalue['AppointmentID']][] = $lvalue['MECID'];
                                                                                      }
                                                                                      } */
                                                                                }
                                                                            }
                                                                        }


                                                                        /*
                                                                         * Get Customer Details
                                                                         */
                                                                        if (isset($customer_ids) && is_array($customer_ids) && count($customer_ids) > 0) {
                                                                            $cust_in_ids = implode(",", $customer_ids);
                                                                            $cust_q = "SELECT * FROM tblCustomers WHERE CustomerID IN(" . $cust_in_ids . ")";
                                                                            $cust_q_exe = $DB->query($cust_q);
                                                                            while ($custdetails = $cust_q_exe->fetch_assoc()) {
                                                                                $all_customer_data[$custdetails['CustomerID']] = $custdetails;
                                                                            }
                                                                        }

                                                                        /*
                                                                         * get employee appointment services
                                                                         */
                                                                        if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                                                                            $eapt_in_ids = implode(",", $appointment_ids);
                                                                            if ($eapt_in_ids != '' && $in_emp_ids != '') {
                                                                                $empaptservice_q = "SELECT DISTINCT ServiceID,AppointmentID,MECID FROM tblAppointmentAssignEmployee WHERE AppointmentID IN(" . $apt_in_ids . ") AND MECID IN(" . $in_emp_ids . ")";
                                                                                $empaptservice_q_exe = $DB->query($empaptservice_q);
                                                                                while ($empaptser = $empaptservice_q_exe->fetch_assoc()) {
                                                                                    $empaptser_result[] = $empaptser;
                                                                                }
                                                                                if (isset($empaptser_result) && is_array($empaptser_result) && count($empaptser_result) > 0) {
                                                                                    foreach ($empaptser_result as $eservdkey => $eservvalue) {
                                                                                        $all_emp_apt_ser_data[$eservvalue['MECID']][$eservvalue['AppointmentID']][] = $eservvalue['ServiceID'];
                                                                                    }
                                                                                }
                                                                            }
                                                                        }



                                                                        /*
                                                                         * get discount 
                                                                         */
                                                                        if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                                                                            $apt_in_ids = implode(",", $appointment_ids);
                                                                            $aptdate_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ")";
                                                                            $aptdate_exe = $DB->query($aptdate_q);
                                                                            while ($disdetails = $aptdate_exe->fetch_assoc()) {
                                                                                $all_apt_dis_amt[] = $disdetails;
                                                                            }

                                                                            /*
                                                                             * Get Appointment Offer AMount
                                                                             */
                                                                            $apt_in_ids = implode(",", $appointment_ids);
                                                                $aptoffer_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID,OfferID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ") AND OfferAmount > 0";
                                                                $aptoffer_exe = $DB->query($aptoffer_q);
                                                                while ($offeretails = $aptoffer_exe->fetch_assoc()) {
                                                                    $apt_service_count[$offeretails['AppointmentID']][$offeretails['ServiceID']] = $offeretails['OfferAmount'];
                                                                }
                                                                
                                                                            $apt_in_ids = implode(",", $appointment_ids);
                                                                            $aptoffer_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID,OfferID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ") AND OfferAmount > 0";
                                                                            $aptoffer_exe = $DB->query($aptoffer_q);
                                                                            while ($offeretails = $aptoffer_exe->fetch_assoc()) {
                                                                                if (isset($all_offers_name[$offeretails['OfferID']]) && $all_offers_name[$offeretails['OfferID']]['Type'] == 1 && $offeretails['OfferID'] > 0) {
                                                                                    $offer_service_cnt = isset($apt_service_count[$offeretails['AppointmentID']]) ? count($apt_service_count[$offeretails['AppointmentID']]) : 1;
                                                                                    $all_apt_off_dis[$offeretails['AppointmentID']][$offeretails['ServiceID']] = $offeretails['OfferAmount'] / $offer_service_cnt;
                                                                                } else {
                                                                                    $all_apt_off_dis[$offeretails['AppointmentID']][$offeretails['ServiceID']] = $offeretails['OfferAmount'];
                                                                                }
                                                                            }

                                                                            if (isset($all_apt_dis_amt) && is_array($all_apt_dis_amt) && count($all_apt_dis_amt) > 0) {
                                                                                foreach ($all_apt_dis_amt as $diskey => $disvalue) {
                                                                                    /*
                                                                                     * Get Appointment service count
                                                                                     */
                                                                                    $extra_offer_amount = 0;

                                                                                    if (isset($all_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']]) && $all_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']] > 0) {
                                                                                        $extra_offer_amount = $all_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']];
                                                                                    }
                                                                                    //echo '<br>extra_offer_amount=' . $extra_offer_amount;
                                                                                    //echo '<br>member_amount=' . $disvalue['MemberShipAmount'];
                                                                                    $all_dis_amt[$disvalue['AppointmentID']][$disvalue['ServiceID']]['MemberShipAmount'] = $disvalue['MemberShipAmount'] + round($extra_offer_amount, 2);
                                                                                    $all_apt_mem_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']] = $disvalue['MemberShipAmount'];
                                                                                    //$all_dis_amt[$disvalue['AppointmentID']][$disvalue['ServiceID']]['OfferAmount'] = $disvalue['OfferAmount'];
                                                                                }
                                                                            }
                                                                        }


                                                                        /*
                                                                         * get store service product
                                                                         */

                                                                        if (isset($service_ids) && is_array($service_ids) && count($service_ids) > 0) {
                                                                            foreach ($service_ids as $ser_key => $ser_value) {

                                                                                $service_in_ids = implode(",", $ser_value);
                                                                                if ($service_in_ids != '') {
                                                                                    $serpro_q = "SELECT ProductID,StoreID,ServiceID FROM tblProductsServices WHERE ServiceID IN(" . $service_in_ids . ")";
                                                                                    $serpro_q_exe = $DB->query($serpro_q);
                                                                                    while ($serprodetails = $serpro_q_exe->fetch_assoc()) {
                                                                                        $service_pdt_result[] = $serprodetails;
                                                                                    }
                                                                                    if (isset($service_pdt_result) && is_array($service_pdt_result) && count($service_pdt_result) > 0) {
                                                                                        foreach ($service_pdt_result as $serpkey => $serpvalue) {
                                                                                            $all_service_pdt[$ser_key][$serpvalue['ServiceID']][] = $serpvalue['ProductID'];
                                                                                            $products_ids[$serpvalue['ProductID']] = $serpvalue['ProductID'];
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }



                                                                        /*
                                                                         * get product price details
                                                                         */
                                                                        if (isset($products_ids) && is_array($products_ids) && count($products_ids) > 0) {
                                                                            $product_in_ids = implode(",", $products_ids);
                                                                            $pdt_pri_q = "Select * from tblNewProducts where ProductID IN(" . $product_in_ids . ")";
                                                                            $pdt_pri_q_exe = $DB->query($pdt_pri_q);
                                                                            while ($pdt_pri_details = $pdt_pri_q_exe->fetch_assoc()) {
                                                                                $pdt_price_result[] = $pdt_pri_details;
                                                                            }
                                                                            if (isset($pdt_price_result) && is_array($pdt_price_result) && count($pdt_price_result) > 0) {
                                                                                foreach ($pdt_price_result as $pdtpkey => $pdtpvalue) {
                                                                                    $pdt_price_data[$pdtpvalue['ProductID']] = array(
                                                                                        'ProductMRP' => $pdtpvalue['ProductMRP'],
                                                                                        'PerQtyServe' => $pdtpvalue['PerQtyServe']
                                                                                    );
                                                                                }
                                                                            }
                                                                        }

                                                                        /*
                                                                         * get average service time
                                                                         */
                                                                        if (isset($service_ids) && is_array($service_ids) && count($service_ids) > 0) {
                                                                            foreach ($service_ids as $ser_key => $ser_value) {
                                                                                $service_in_ids = implode(",", $ser_value);
                                                                                $sertime_q = "Select Time as Timing,ServiceCost,ServiceID from tblServices where ServiceID IN(" . $service_in_ids . ")";
                                                                                $sertime_q_exe = $DB->query($sertime_q);
                                                                                while ($sertimedetails = $sertime_q_exe->fetch_assoc()) {
                                                                                    $service_time_result[] = $sertimedetails;
                                                                                }
                                                                                if (isset($service_time_result) && is_array($service_time_result) && count($service_time_result) > 0) {
                                                                                    foreach ($service_time_result as $sertkey => $sertvalue) {
                                                                                        $service_time_data[$sertvalue['ServiceID']] = array(
                                                                                            'Timing' => $sertvalue['Timing'],
                                                                                            'ServiceCost' => $sertvalue['ServiceCost']
                                                                                        );
                                                                                    }
                                                                                }
                                                                            }
                                                                        }




                                                                        if (isset($all_emp_data) && is_array($all_emp_data) && count($all_emp_data) > 0) {
                                                                            $counter = 0;
                                                                            $cntattemp = 0;
                                                                            $all_total_sale = 0;
                                                                            $all_service_count = 0;
                                                                            $all_AvgTime = 0;
                                                                            $all_new_cust = 0;
                                                                            $all_old_cust = 0;
                                                                            $all_old_non_repeat = 0;
                                                                            $all_total_cust = 0;
                                                                            $all_new_cli_per = 0;
                                                                            $all_exi_cli_per = 0;
                                                                            $all_att_count = 0;
                                                                            $all_comm_cost = 0;
                                                                            $all_pdt_cost = 0;
                                                                            $all_free_count = 0;
                                                                            $all_arpu = 0;
                                                                            $all_service_count_value = array();
                                                                            foreach ($all_emp_data as $rowkey => $row) {

                                                                                // echo "Hello";
                                                                                $CNTp = "";
                                                                                $strEID = $row["EID"];
                                                                                $strEmployeeName = $row["EmployeeName"];
                                                                                $strEmployeeEmailID = $row["EmployeeEmailID"];
                                                                                $strEmpPercentage = $row["EmpPercentage"];
                                                                                $strEmployeeMobileNo = $row["EmployeeMobileNo"];
                                                                                $EmployeeCode = $row["EmployeeCode"];


                                                                                $cntattemp = isset($atten_pre_data[$strEID]) ? $atten_pre_data[$strEID] : 0;
                                                                                $StoreID = $row["StoreID"];
                                                                                $counter ++;



                                                                                $FreeServicecount = isset($FreeServicecount_data[$strEID]) ? $FreeServicecount_data[$strEID] : 0;

                                                                                if (isset($all_sales_data[$strEID]) && is_array($all_sales_data[$strEID]) && count($all_sales_data[$strEID]) > 0) {

                                                                                    $counter = 0;
                                                                                    $strSID = "";
                                                                                    $qty = "";
                                                                                    $strSAmount = "";
                                                                                    $strAmount = "";
                                                                                    $strCommission = "";
                                                                                    $FinalDAmount = '';
                                                                                    $FinalDiscount = '';
                                                                                    $UltimateSale = '';
                                                                                    $AfterDivideSale = '';
                                                                                    $CommssionFinal = "";


                                                                                    foreach ($all_sales_data[$strEID] as $key => $rowdetails) {

                                                                                        $cnttcntser = 0;
                                                                                        $strEIDa = $rowdetails["EID"];
                                                                                        $strAID = $rowdetails["AppointmentID"];
                                                                                        $strSID = $rowdetails["ServiceID"];
                                                                                        //$qty = $rowdetails["QtyParam"];
                                                                                        $qty = $rowdetails["Qty"];
                                                                                        $strEmployeeName = $row["EmployeeName"];
                                                                                        $strAmount = isset($all_ser_amt[$strAID][$strSID]) ? $all_ser_amt[$strAID][$strSID] : 0;
                                                                                        $strSAmount = $strAmount;
                                                                                        $strCommission = $rowdetails["Commission"];
                                                                                        $StoreIDd = $rowdetails["StoreID"];

                                                                                        $unique_apt[$rowdetails['AppointmentID']]['CustomerID'] = $rowdetails['CustomerID'];
                                                                                        $unique_apt[$rowdetails['AppointmentID']]['AppointmentDate'] = $rowdetails['AppointmentDate'];


                                                                                        $seep = isset($all_emp_apt_ser_data[$strEID][$strAID]) ? $all_emp_apt_ser_data[$strEID][$strAID] : array();



                                                                                        $sey[] = $seep;




                                                                                        //echo $cnttcntser;
                                                                                        $strTotalAmount += $strSAmount;  //Total of Service sale amount

                                                                                        if ($strCommission == "1") {
                                                                                            $AfterDivideSale = $strSAmount;
                                                                                            $strCommissionType = '<span class="bs-label label-success">Alone</span>';
                                                                                        } elseif ($strCommission == "2") {
                                                                                            $AfterDivideSale = ($strSAmount);
                                                                                            $strCommissionType = '<span class="bs-label label-blue-alt">Split</span>';
                                                                                        }
                                                                                        $TotalAfterDivideSale += $AfterDivideSale;  //Total of Final payment


                                                                                        /* $rowdiscount = isset($all_dis_amt[$strAID][$strSID]) ? $all_dis_amt[$strAID][$strSID] : array();


                                                                                          if (isset($rowdiscount) && is_array($rowdiscount) && count($rowdiscount) > 0) {
                                                                                          $strOfferAmount = $rowdiscount["OfferAmount"];
                                                                                          $strDiscountAmount = $rowdiscount["MemberShipAmount"];

                                                                                          if ($strOfferAmount == "0") {
                                                                                          $FinalDAmount = $strDiscountAmount;
                                                                                          } if ($strDiscountAmount == "0") {
                                                                                          $FinalDAmount = $strOfferAmount;
                                                                                          }
                                                                                          } else {
                                                                                          $FinalDAmount = "0";
                                                                                          } */

                                                                                        $FinalDiscount = isset($all_apt_mem_dis[$strAID][$strSID]) ? ($all_apt_mem_dis[$strAID][$strSID]) / $qty : 0;
                                                                                        $FinalOtherDiscount = isset($all_apt_off_dis[$strAID][$strSID]) ? ($all_apt_off_dis[$strAID][$strSID]) / $qty : 0;

                                                                                        //echo "<br>discount=" . $FinalOtherDiscount . "&appointment_id=" . $strAID;
                                                                                        //$FinalDiscount = $FinalDAmount / $qty;

                                                                                        $TotalFinalDiscount += $FinalDiscount; //Total of discounted amount
                                                                                        //$UltimateSale = $AfterDivideSale - $FinalDiscount;
                                                                                        $UltimateSale = $AfterDivideSale - $FinalDiscount - $FinalOtherDiscount;

                                                                                        $TotalUltimateSale += $UltimateSale; //Total of discounted amount
                                                                                        //Calculation for commission
                                                                                        if ($strCommission == "1") {
                                                                                            $CommssionFinal = ($UltimateSale / 100) * $strEmpPercentage;
                                                                                        } elseif ($strCommission == "2") {
                                                                                            $CommssionFinal = ($UltimateSale / 200) * $strEmpPercentage;
                                                                                        }
                                                                                        $ComFinal += $CommssionFinal;



                                                                                        $ProductID[0] = isset($all_service_pdt[$strEIDa][$strSID]) ? $all_service_pdt[$strEIDa][$strSID] : array();

                                                                                        $rowdiscounttss = isset($service_time_data[$strSID]) ? $service_time_data[$strSID] : array();
                                                                                        if (isset($rowdiscounttss) && is_array($rowdiscounttss) && count($rowdiscounttss) > 0) {
                                                                                            $AVGTime = $rowdiscounttss["Timing"];
                                                                                            $ServiceCost = $rowdiscounttss["ServiceCost"];
                                                                                            $FinalAvgTime +=$AVGTime;
                                                                                            $FinalAMount +=$ServiceCost;
                                                                                        } else {
                                                                                            $FinalAMount = 0;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                /* $sqldetailsd = newcustomercount12($getfrom, $getto, $strEID);

                                                                                  foreach ($sqldetailsd as $vat) {
                                                                                  $app[] = $vat['AppointmentID'];
                                                                                  $custcnt = count($app);
                                                                                  if ($custcnt == '' || $custcnt == '0') {
                                                                                  $custcnt = 0;
                                                                                  }
                                                                                  }
                                                                                  unset($app); */

                                                                                //$stu1 = newcustomerrepeat($getfrom, $getto, $strEID);
                                                                                /* $new_custq = "select distinct(tblAppointments.CustomerID) as newcust from tblAppointments "
                                                                                  . " join tblAppointmentAssignEmployee on tblAppointmentAssignEmployee.AppointmentID=tblAppointments.AppointmentID "
                                                                                  . " join tblCustomers on tblCustomers.CustomerID=tblAppointments.CustomerID "
                                                                                  . " where tblAppointments.IsDeleted!='1' and tblAppointmentAssignEmployee.MECID='" . $strEID . "' "
                                                                                  . " and tblAppointments.Status = '2' AND tblAppointments.FreeService != '1' and "
                                                                                  . " Date(tblCustomers.RegDate)>='" . $getfrom . "' and Date(tblCustomers.RegDate)<='" . $getto . "' "
                                                                                  . " and Date(tblAppointments.AppointmentDate)>='" . $getfrom . "' and Date(tblAppointments.AppointmentDate)<='" . $getto . "'";
                                                                                  $new_custq_exe = $DB->query($new_custq);
                                                                                  while ($newcustdetails = $new_custq_exe->fetch_assoc()) {
                                                                                  $stu1[] = $newcustdetails;
                                                                                  } */

                                                                                $new_customer_count = 0;
                                                                                $old_customer_count = 0;
                                                                                $old_customer_non_count = 0;

                                                                                $new_cust_ids = array();
                                                                                $old_cust_repeat_ids = array();
                                                                                $old_cust_non_repeat_ids = array();

                                                                                if (isset($unique_apt) && is_array($unique_apt) && count($unique_apt) > 0) {
                                                                                    foreach ($unique_apt as $key => $value) {
                                                                                        $customer_reg_date = isset($all_customer_data[$value['CustomerID']]) ? date('Y-m-d', strtotime($all_customer_data[$value['CustomerID']]['RegDate'])) : '';
                                                                                        /*
                                                                                         * Check Customer Appointment Count
                                                                                         */
                                                                                        $apt_count = isset($customer_apt_count[$value['CustomerID']]) ? $customer_apt_count[$value['CustomerID']] : 1;

                                                                                        if ($apt_count == 1) {
                                                                                            $new_customer_count += 1;
                                                                                            $new_cust_ids[$value['CustomerID']] = $value['CustomerID'];
                                                                                        } else {
                                                                                            /*
                                                                                             * Check last Appointment Date
                                                                                             */
                                                                                            $last_appointment_date = isset($customer_appoint_date[$value['CustomerID']]) ? $customer_appoint_date[$value['CustomerID']]['apt_date'] : '';
                                                                                            $last_appointment_id = isset($customer_appoint_date[$value['CustomerID']]) ? $customer_appoint_date[$value['CustomerID']]['apt_id'] : '';
                                                                                            if ($last_appointment_date == '') {
                                                                                                $old_customer_non_count +=1;
                                                                                                $old_cust_non_repeat_ids[$value['CustomerID']] = $value['CustomerID'];
                                                                                            } else {
                                                                                                /*
                                                                                                 * Check last and current appointment date gap
                                                                                                 */
                                                                                                /* $datetime1 = date_create($last_appointment_date);
                                                                                                  $datetime2 = date_create($value['AppointmentDate']);
                                                                                                  $interval = date_diff($datetime2, $datetime1);
                                                                                                  $day_gap = $interval->format('%d'); */

                                                                                                $d1 = new DateTime($last_appointment_date);
                                                                                                $d2 = new DateTime($value['AppointmentDate']);
                                                                                                $interval = $d1->diff($d2);
                                                                                                $day_gap = $interval->days;
                                                                                                //echo "<br>last_appointment_id=" . $last_appointment_id . "Custoemr_id=" . $value['CustomerID'] . "Day_gap=" . $day_gap . "datetime1=" . $last_appointment_date . "datetime2=" . $value['AppointmentDate'];

                                                                                                if (isset($last_emp_res[$last_appointment_id]) && in_array($strEID, $last_emp_res[$last_appointment_id]) && $day_gap <= 60) {
                                                                                                    $old_customer_count +=1;
                                                                                                    $old_cust_repeat_ids[$value['CustomerID']] = $value['CustomerID'];
                                                                                                } else {
                                                                                                    $old_customer_non_count +=1;
                                                                                                    $old_cust_non_repeat_ids[$value['CustomerID']] = $value['CustomerID'];
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        /* if ($customer_reg_date != '') {
                                                                                          if ($customer_reg_date == $unique_apt['AppointmentDate']) {
                                                                                          $new_customer_count += 1;
                                                                                          } else {
                                                                                          $datetime1 = date_create($customer_reg_date);
                                                                                          $datetime2 = date_create($unique_apt['AppointmentDate']);
                                                                                          $interval = date_diff($datetime1, $datetime2);
                                                                                          $month_gap = $interval->format('%m');

                                                                                          /*
                                                                                         * if regDate and Appointment gap is less than 3 month then old customer else new customer
                                                                                         */
                                                                                        /*  if ($month_gap > 3) {
                                                                                          $new_customer_count += 1;
                                                                                          } else {
                                                                                          $old_customer_count += 1;
                                                                                          }
                                                                                          }
                                                                                          } */
                                                                                    }
                                                                                }

                                                                                unset($unique_apt);

                                                                                $product_cost = 0;
                                                                                foreach ($ProductID as $ser_pdts) {
                                                                                    foreach ($ser_pdts as $valt) {
                                                                                        $rowdiscountt = isset($pdt_price_data[$valt]) ? $pdt_price_data[$valt] : array();
                                                                                        if (isset($rowdiscountt) && is_array($rowdiscountt) && count($rowdiscountt) > 0) {
                                                                                            $ProductMRPs = $rowdiscountt["ProductMRP"];
                                                                                            $PerQtyServes = $rowdiscountt["PerQtyServe"];
                                                                                            $product_cost += ($ProductMRPs / $PerQtyServes);
                                                                                            $tpcost = round($product_cost);
                                                                                        } else {
                                                                                            $product_cost = "0";
                                                                                        }
                                                                                    }
                                                                                }


                                                                                unset($ProductID);
                                                                                unset($strServiceID);


                                                                                if ($ARPU == '') {
                                                                                    $ARPU = 0;
                                                                                }

                                                                                if ($tpcost == '') {
                                                                                    $tpcost = 0;
                                                                                }

                                                                                if ($strSAmount == '') {
                                                                                    $strSAmount = 0;
                                                                                }
                                                                                if ($qty == "") {
                                                                                    $qty = 0;
                                                                                }
                                                                                if ($strSAmount == "") {
                                                                                    $strSAmount = 0;
                                                                                } else {
                                                                                    $strSAmount = $strSAmount;
                                                                                }
                                                                                $totalstrServiceAmount = $totalstrServiceAmount + $strSAmount;
                                                                                if ($qty == "") {
                                                                                    $qty = 0;
                                                                                } else {
                                                                                    $qty = $qty;
                                                                                }

                                                                                $totalstrqty = $totalstrqty + $qty;
                                                                                if ($tpcost == "") {
                                                                                    $tpcost = 0;
                                                                                } else {
                                                                                    $tpcost = $tpcost;
                                                                                }
                                                                                $totaltpcost = $totaltpcost + $tpcost;





                                                                                $unique_ser = array();
                                                                                if (isset($sey) && is_array($sey) && count($sey) > 0) {
                                                                                    foreach ($sey as $parskey => $soarevalue) {
                                                                                        foreach ($soarevalue as $skey => $svalue) {
                                                                                            $unique_ser[] = $svalue;
                                                                                        }
                                                                                    }
                                                                                }
                                                                                $seyr = array_unique($unique_ser);
                                                                                $seyrp = array_values($seyr);


                                                                                /*
                                                                                 * get services category
                                                                                 */
                                                                                $catp = array();
                                                                                if (isset($seyrp) && is_array($seyrp) && count($seyrp) > 0) {
                                                                                    $cat_ser_ids = implode(",", $seyrp);
                                                                                    $service_cat_q = "Select distinct(CategoryID) as cat_id from tblProductsServices where ServiceID IN(" . $cat_ser_ids . ")";
                                                                                    $service_cat_q_exe = $DB->query($service_cat_q);
                                                                                    while ($cat_details = $service_cat_q_exe->fetch_assoc()) {
                                                                                        $catp[] = $cat_details['cat_id'];
                                                                                    }
                                                                                }



                                                                                $catq = array_unique($catp);
                                                                                $caty = array_values($catq);

                                                                                $strqt = array_unique($stutrqty1);
                                                                                $strqtq = array_values($strqt);

                                                                                if ($strprofit == "") {
                                                                                    $strprofit = 0;
                                                                                } else {
                                                                                    $strprofit = $strprofit;
                                                                                }
                                                                                $totalstrprofit = $totalstrprofit + $strprofit;

                                                                                //$newclientper = ($custcnt / $qty1) * 100;
                                                                                ?>
                                                                                <tr id="my_data_tr_<?= $counter ?>">

                                                                                    <td><center><?php echo $strEmployeeName; ?></center></td>
                                                                            <td><center>Rs. <?php
                                                                                $total_sale_amount = round($TotalUltimateSale, 2);
                                                                                echo $total_sale_amount;
                                                                                $all_total_sale += $total_sale_amount;
                                                                                ?>/-</center></td>
                                                                            <td><center><?php
                                                                                // print_r($seyrp);

                                                                                $total_service_count = 0;
                                                                                /*
                                                                                 * get category name
                                                                                 */
                                                                                if (isset($caty) && is_array($caty) && count($caty) > 0) {
                                                                                    $cat_name_ids = implode(",", $caty);
                                                                                    if ($cat_name_ids != '') {
                                                                                        $cat_name_q = "Select CategoryName,CategoryID from tblCategories where CategoryID IN(" . $cat_name_ids . ")";
                                                                                        $cat_name_q_exe = $DB->query($cat_name_q);
                                                                                        while ($cat_name_details = $cat_name_q_exe->fetch_assoc()) {
                                                                                            $cat_name_data[$cat_name_details['CategoryID']] = $cat_name_details['CategoryName'];
                                                                                        }
                                                                                    }
                                                                                }


                                                                                $cat_ser_count = array();
                                                                                if (isset($caty) && is_array($caty) && count($caty) > 0) {
                                                                                    if (isset($seyrp) && is_array($seyrp) && count($seyrp) > 0) {
                                                                                        $cat_in_ids = implode(",", $caty);
                                                                                        $ser_in_ids = implode(",", $seyrp);
                                                                                        /* $cat_ser_count_q = "select tblAppointmentAssignEmployee.Qty,tblProductsServices.CategoryID,tblProductsServices.ServiceID from tblAppointments "
                                                                                          . " left join tblAppointmentAssignEmployee on tblAppointmentAssignEmployee.AppointmentID=tblAppointments.AppointmentID "
                                                                                          . " left join tblProductsServices on tblProductsServices.ServiceID=tblAppointmentAssignEmployee.ServiceID "
                                                                                          . " where tblProductsServices.ServiceID IN(" . $ser_in_ids . ") and tblAppointments.IsDeleted!='1' "
                                                                                          . " and tblAppointmentAssignEmployee.MECID='" . $strEIDa . "' $sqlTempfrom1 $sqlTempto1 and "
                                                                                          . " tblProductsServices.CategoryID IN(" . $cat_in_ids . ") and tblProductsServices.StoreID='" . $StoreID . "'"
                                                                                          . "  group by tblProductsServices.CategoryID,tblProductsServices.ServiceID"; */

                                                                                        if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                                                                                            $apt_in_ids = implode(",", $appointment_ids);
                                                                                            if ($apt_in_ids != '') {
                                                                                                $cat_ser_count_q = "select tblAppointmentAssignEmployee.QtyParam,tblAppointmentAssignEmployee.Qty,tblProductsServices.CategoryID,tblProductsServices.ServiceID from tblAppointmentAssignEmployee "
                                                                                                        . " join tblProductsServices on tblProductsServices.ServiceID=tblAppointmentAssignEmployee.ServiceID "
                                                                                                        . " where tblProductsServices.ServiceID IN(" . $ser_in_ids . ") and tblAppointmentAssignEmployee.MECID='" . $strEIDa . "' and "
                                                                                                        . " tblProductsServices.CategoryID IN(" . $cat_in_ids . ") "
                                                                                                        . " AND tblAppointmentAssignEmployee.AppointmentID IN(" . $apt_in_ids . ")"
                                                                                                        . "  group by tblAppointmentAssignEmployee.AppointmentAssignEmployeeID";
                                                                                                // . "  group by tblProductsServices.CategoryID,tblProductsServices.ServiceID";
                                                                                                $cat_ser_count_q_exe = $DB->query($cat_ser_count_q);
                                                                                                if ($cat_ser_count_q_exe->num_rows > 0) {
                                                                                                    while ($cat_ser = $cat_ser_count_q_exe->fetch_assoc()) {
                                                                                                        if (isset($cat_ser_count[$cat_ser['CategoryID']])) {
                                                                                                            //$cat_ser_count[$cat_ser['CategoryID']] += $cat_ser['QtyParam'];
                                                                                                            $cat_ser_count[$cat_ser['CategoryID']] += 1;
                                                                                                        } else {
                                                                                                            //$cat_ser_count[$cat_ser['CategoryID']] = $cat_ser['QtyParam'];
                                                                                                            $cat_ser_count[$cat_ser['CategoryID']] = 1;
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }

                                                                                for ($p = 0; $p < count($caty); $p++) {
                                                                                    $qty3 = 0;

                                                                                    $catname = isset($cat_name_data[$caty[$p]]) && $cat_name_data[$caty[$p]] ? $cat_name_data[$caty[$p]] : '-';

                                                                                    $qty3 = isset($cat_ser_count[$caty[$p]]) ? $cat_ser_count[$caty[$p]] : 0;
                                                                                    $total_service_count += $qty3;

                                                                                    echo $catname . "-" . $qty3 . "<br/>";

                                                                                    if (isset($all_service_count_value[$catname])) {
                                                                                        $all_service_count_value[$catname] += $qty3;
                                                                                    } else {
                                                                                        $all_service_count_value[$catname] = $qty3;
                                                                                    }
                                                                                }

                                                                                /* for ($p = 0; $p < count($caty); $p++) {
                                                                                  $qty3 = 0;
                                                                                  $catname = isset($cat_name_data[$caty[$p]]) ? $cat_name_data[$caty[$p]] : '';

                                                                                  for ($t = 0; $t < count($seyrp); $t++) {

                                                                                  $stutrqty = "select tblAppointmentAssignEmployee.Qty from tblAppointments left join tblAppointmentAssignEmployee on tblAppointmentAssignEmployee.AppointmentID=tblAppointments.AppointmentID left join tblProductsServices on tblProductsServices.ServiceID=tblAppointmentAssignEmployee.ServiceID where tblProductsServices.ServiceID='" . $seyrp[$t] . "' and tblAppointments.IsDeleted!='1' and tblAppointmentAssignEmployee.MECID='" . $strEIDa . "' $sqlTempfrom1 $sqlTempto1 and tblProductsServices.CategoryID='" . $caty[$p] . "' and tblProductsServices.StoreID='" . $StoreID . "' group by tblProductsServices.CategoryID";

                                                                                  $RSstutrqty = $DB->query($stutrqty);
                                                                                  if ($RSstutrqty->num_rows > 0) {


                                                                                  while ($rowstutrqty = $RSstutrqty->fetch_assoc()) {

                                                                                  $qty2 = $rowstutrqty['Qty'];

                                                                                  $qty3 +=$qty2;
                                                                                  $total_service_count += $qty2;
                                                                                  // echo "<br>cat_name=" . $catname . "Qty=" . $qty3 . "serid=" . $seyrp[$t] . "cat_id=" . $caty[$p];
                                                                                  }
                                                                                  }
                                                                                  }


                                                                                  echo $catname . "-" . $qty3 . "<br/>";
                                                                                  } */

                                                                                unset($seyrp);
                                                                                unset($caty);
                                                                                $qty3 = "";
                                                                                ?></center></td>
                                                                            <td><center><?php
                                                                                //echo $qty1;
                                                                                echo $total_service_count;
                                                                                $all_service_count += $total_service_count;
                                                                                ?></center></td>
                                                                            <td><center>
                                                                                <?php
                                                                                if ($FinalAvgTime == '') {
                                                                                    echo $FinalAvgTime = '0' . ".min";
                                                                                    $all_AvgTime += 0;
                                                                                } else {
                                                                                    echo $FinalAvgTime . ".min";
                                                                                    $all_AvgTime += $FinalAvgTime;
                                                                                }
                                                                                ?>

                                                                            </center></td>
                                                                            <td onclick="GetCustomerDetail('<?php echo 'new_customer_data_' . $strEID; ?>');"><center><?php
                                                                                /*  if ($new_customer_count == '') {
                                                                                  $new_count =  0;
                                                                                  $all_new_cust += 0;
                                                                                  } else {
                                                                                  $new_count =  $new_customer_count;
                                                                                  $all_new_cust += $new_customer_count;
                                                                                  } */
                                                                                $new_count = isset($new_cust_ids) ? count($new_cust_ids) : 0;
                                                                                $all_new_cust += $new_count;
                                                                                ?><a class="btn btn-link" href="javascript:void(0);"><?php echo $new_count; ?></a></center>
                                                                            <span style="display:none;" class="new_customer_data_<?php echo $strEID; ?>"><?php echo json_encode($new_cust_ids); ?></span></td>
                                                                            <td onclick="GetCustomerDetail('<?php echo 'old_customer_repeat_data_' . $strEID; ?>');"><center><?php
                                                                                /* if ($old_customer_count == '') {
                                                                                  $old_repeat_count = 0;
                                                                                  $all_old_cust += 0;
                                                                                  } else {
                                                                                  $old_repeat_count = $old_customer_count;
                                                                                  $all_old_cust += $old_customer_count;
                                                                                  } */
                                                                                $old_repeat_count = isset($old_cust_repeat_ids) ? count($old_cust_repeat_ids) : 0;
                                                                                $all_old_cust += $old_repeat_count;
                                                                                ?><a class="btn btn-link" href="javascript:void(0);"><?php echo $old_repeat_count; ?></a></center>
                                                                            <span style="display:none;" class="old_customer_repeat_data_<?php echo $strEID; ?>"><?php echo json_encode($old_cust_repeat_ids); ?></span></td>
                                                                            <td onclick="GetCustomerDetail('<?php echo 'old_customer_non_repeat_data_' . $strEID; ?>');"><center><?php
                                                                                /* if ($old_customer_non_count == '') {
                                                                                  $old_non_repeat_count = 0;
                                                                                  $all_old_non_repeat += 0;
                                                                                  } else {
                                                                                  $old_non_repeat_count = $old_customer_non_count;
                                                                                  $all_old_non_repeat += $old_customer_non_count;
                                                                                  } */
                                                                                $old_non_repeat_count = isset($old_cust_non_repeat_ids) ? count($old_cust_non_repeat_ids) : 0;
                                                                                $all_old_non_repeat += $old_non_repeat_count;
                                                                                ?><a class="btn btn-link" href="javascript:void(0);"><?php echo $old_non_repeat_count; ?></a></center>
                                                                            <span style="display:none;" class="old_customer_non_repeat_data_<?php echo $strEID; ?>"><?php echo json_encode($old_cust_non_repeat_ids); ?></span></td>
                                                                            <td><center>
                                                                                <?php
                                                                                $total_cust = $new_count + $old_repeat_count + $old_non_repeat_count;
                                                                                $all_total_cust += $total_cust;
                                                                                ?>
                                                                                <?= $total_cust;
                                                                                ?></center></td>
                                                                            <?php
                                                                            if ($per != '0') {
                                                                                $newclientper = ($new_customer_count * 100 ) / $total_cust;
                                                                                //$all_new_cli_per += round($newclientper, 2);
                                                                                ?>
                                                                                <td class="numeric" id="percol2" ><center><?= round($newclientper, 2) ?></center></td>
                                                                                <?php
                                                                            }
                                                                            ?>

                                                                            <?php
                                                                            if ($per != '0') {
                                                                                $exclientper = ($old_customer_count * 100 ) / $total_cust;
                                                                                //$all_exi_cli_per += round($exclientper, 2);
                                                                                ?>
                                                                                <td class="numeric" id="percol2" ><center><?= round($exclientper, 2) ?></center></td>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                            <td><center><?php
                                                                                $all_att_count += round($cntattemp) / 2;
                                                                                echo round($cntattemp) / 2
                                                                                ?> Days</center></td>
                                                                            <td><center>--</center></td>
                                                                            <td><center>Rs. <?php
                                                                                $all_comm_cost += round($ComFinal, 2);
                                                                                echo round($ComFinal, 2)
                                                                                ?>/-</center></td>
                                                                            <td><center><?php
                                                                                $all_pdt_cost += $tpcost;
                                                                                echo $tpcost
                                                                                ?></center></td>
                                                                            <td><center><?php
                                                                                $all_free_count += $FreeServicecount;
                                                                                echo $FreeServicecount
                                                                                ?></center></td>
                                                                            <td><center><?php
                                                                                /* if ($strSAmount != '0' && $strSAmount != '') {
                                                                                  $strprofit = ($strSAmount) - ($tpcost);
                                                                                  //$ARPU = ($strprofit) / ($qty);
                                                                                  $ARPU = $TotalUltimateSale / $qty1;
                                                                                  } else {

                                                                                  $strSAmount = 0;
                                                                                  $strprofit = 0;
                                                                                  //$ARPU = ($strprofit) / ($qty);
                                                                                  $ARPU = $TotalUltimateSale / $qty1;
                                                                                  }
                                                                                  if ($ARPU == "") {
                                                                                  $ARPU = 0;
                                                                                  } else {
                                                                                  $ARPU = $ARPU;
                                                                                  }
                                                                                  $totalARPU = $totalARPU + $ARPU;


                                                                                  echo round($ARPU); */


                                                                                /*
                                                                                 * ARPU = Profitablity / product Cost
                                                                                 */
                                                                                /* $sale_amt = round($TotalUltimateSale, 2);
                                                                                  $pdt_cost = $tpcost;
                                                                                  $Profitibility = $sale_amt - $pdt_cost;
                                                                                  $x = $Profitibility / round($pdt_cost);
                                                                                  $arpu = (number_format((float) $x, 2, '.', '')); */
                                                                                //ARPU = Amount / services
                                                                                $arpu = $total_sale_amount / $total_service_count;
                                                                                $arpu = round($arpu, 2);
                                                                                echo $arpu;
                                                                                $all_arpu += $arpu;
                                                                                ?></center></td>
                                                                            <?php /*  <td class="numeric"><center>--</center></td> */ ?>


                                                                            </tr>
                                                                            <?php
                                                                            $ComFinal = "";
                                                                            $SaleFinal = "";
                                                                            $tpcost = "";
                                                                            $tpTime = "";
                                                                            // $CNTp="";
                                                                            $qty = "";
                                                                            $Sale = "";
                                                                            $custcnt = "";
                                                                            $FinalPPrice = "";
                                                                            $FinalPPrices = "";
                                                                            $newrepeatcust = "";
                                                                            // $SaleFinal="";
                                                                            $qty3 = "";
                                                                            $TotalUltimateSale = 0;
                                                                            $total_service_count = 0;
                                                                            $FinalAvgTime = "";
                                                                            unset($caty);
                                                                            unset($caty);
                                                                            unset($sey);
                                                                        }
                                                                        unset($caty);
                                                                        $cnttcntser = "";
                                                                        unset($seyrp);
                                                                        ?>
                                                                        <tr>
                                                                            <td><center>All</center></td>
                                                                        <td><center><?php echo isset($all_total_sale) ? 'Rs. ' . $all_total_sale . '/-' : 'Rs. 0/-'; ?></center></td>
                                                                        <td><center>
                                                                            <?php
                                                                            if (isset($all_service_count_value) && is_array($all_service_count_value) && count($all_service_count_value) > 0) {
                                                                                foreach ($all_service_count_value as $key => $value) {
                                                                                    echo $key . "-" . $value . "<br/>";
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </center></td>
                                                                        <td><center><?php echo isset($all_service_count) ? $all_service_count : 0; ?></center></td>
                                                                        <td><center><?php echo isset($all_AvgTime) ? $all_AvgTime . ".min" : 0 . ".min"; ?></center></td>
                                                                        <td><center><?php echo isset($all_new_cust) ? $all_new_cust : 0; ?></center></td>
                                                                        <td><center><?php echo isset($all_old_cust) ? $all_old_cust : 0; ?></center></td>
                                                                        <td><center><?php echo isset($all_old_non_repeat) ? $all_old_non_repeat : 0; ?></center></td>
                                                                        <td><center><?php echo isset($all_total_cust) ? $all_total_cust : 0; ?></center></td>
                                                                        <?php
                                                                        if ($per != '0') {
                                                                            $all_new_cli_per = ($all_new_cust * 100 ) / $all_total_cust;
                                                                            ?>
                                                                            <td><center><?php echo isset($all_new_cli_per) ? round($all_new_cli_per, 2) : 0; ?></center></td>
                                                                        <?php } ?>
                                                                        <?php
                                                                        if ($per != '0') {
                                                                            $all_exi_cli_per = ($all_old_cust * 100 ) / $all_total_cust;
                                                                            ?>
                                                                            <td><center><?php echo isset($all_exi_cli_per) ? round($all_exi_cli_per, 2) : 0; ?></center></td>
                                                                        <?php } ?>
                                                                        <td><center><?php echo isset($all_att_count) ? $all_att_count . ' Days' : '0 Days'; ?></center></td>
                                                                        <td><center>--</center></td>
                                                                        <td><center><?php echo isset($all_comm_cost) ? 'Rs. ' . $all_comm_cost . '/-' : 'Rs. 0/-'; ?></center></td>
                                                                        <td><center><?php echo isset($all_pdt_cost) ? $all_pdt_cost : 0; ?></center></td>
                                                                        <td><center><?php echo isset($all_free_count) ? $all_free_count : 0; ?></center></td>
                                                                        <td><center><?php echo isset($all_arpu) ? $all_arpu : 0; ?></center></td>
                                                                        <?php /* <td class="numeric"><center>--</center></td> */ ?>
                                                                        </tr>
                                                                    <?php } else {
                                                                        ?>															
                                                                        <tr>

                                                                            <td></td>
                                                                            <td></td>
                                                                            <td>No Records Found</td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <?php /*  <td></td> */ ?>
                                                                        </tr>


                                                                        <?php
                                                                    }
                                                                    $DB->close();
                                                                    ?>

                                                                    </tbody>
                                                                    <thead>
                                                                        <tr>

                                                                            <th style="text-align:center">Employee Name</th>
                                                                            <th style="text-align:center">Total Sale</th>
                                                                            <th style="text-align:center">Service Category Wise</th>
                                                                            <th style="text-align:center">Service Count</th>
                                                                            <th style="text-align:center">Avg Service Time</th>
                                                                            <th style="text-align:center">New Customer Count</th>
                                                                            <th style="text-align:center">Repeat Customer Count</th>
                                                                            <th style="text-align:center">Non-Repeat Customer Count</th>
                                                                            <th style="text-align:center">Total Customer</th>
                                                                            <?php
                                                                            if ($per != '0') {
                                                                                ?>
                                                                                <th style="text-align:center">New Customer %</th>
                                                                                <?php
                                                                            }
                                                                            ?>

                                                                            <?php
                                                                            if ($per != '0') {
                                                                                ?>
                                                                                <th style="text-align:center">Repeat Customer %</th>
                                                                                <?php
                                                                            }
                                                                            ?>


                                                                            <th style="text-align:center">Attendance Present Count</th>
                                                                            <th style="text-align:center">Salary Cost as per attendance</th>
                                                                            <th style="text-align:center">Comm Cost</th>
                                                                            <th style="text-align:center">Product Cost</th>
                                                                            <th style="text-align:center">Free Service Count</th>
                                                                            <th style="text-align:center">ARPU</th>
                                                                                <?php /*  <th style="text-align:center">Emp Rating</th> */ ?>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                                <?php
                                                            } else {
                                                                echo "<br><center><h3>Please Select Month And Year!</h3></center>";
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <?php require_once 'incFooter.fya'; ?>

        </div>
    </body>

</html>