<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Employee Attendance | Nailspa";
$strDisplayTitle = "Employee Attendance";
$strMenuID = "3";
$strMyActionPage = "admin_attendance.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<?php
$DB = Connect();

//                        echo '<pre>';
//                        print_r($_POST);
//                        exit;
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['attendance_date']) && $_POST['attendance_date'] != '') {
        if (isset($_POST['attendance_time']) && $_POST['attendance_time'] != '') {
            if (isset($_POST['emp_code']) && $_POST['emp_code'] != '') {

                $attendance_time = date('H:i:s', strtotime($_POST['attendance_time']));
                $attendance_date = date('Y-m-d', strtotime($_POST['attendance_date']));
                $emp_code = trim($_POST['emp_code']);
                $type = isset($_POST['type']) ? $_POST['type'] : 1;
                $storeid = isset($_POST['Store']) ? $_POST['Store'] : 0;

                $employee_data = select("EID,StoreID", "tblEmployees", "EmployeeCode='" . $emp_code . "'");
                $EID = $employee_data[0]['EID'];
                if (isset($strEmpID)) {
                    $login_id = $strEmpID;
                } else {
                    $login_id = $strAdminID;
                }

                /*
                 * Check if Record Already Exist Or Not
                 */
                if ($type == 2) {
                    $record_exist = select("*", "tblEmployeesRecords", "EmployeeCode = '" . $emp_code . "'"
                            . " AND DateOfAttendance='" . $attendance_date . "' AND LogoutTime !='00:00:00'");
                } else {
                    $record_exist = select("*", "tblEmployeesRecords", "EmployeeCode = '" . $emp_code . "'"
                            . " AND DateOfAttendance='" . $attendance_date . "' AND LoginTime !='00:00:00'");
                }

                if (isset($record_exist) && is_array($record_exist) && count($record_exist) > 0) {
                    die('<div class="alert alert-close alert-danger">
					<div class="bg-red alert-icon"><i class="glyph-icon icon-times"></i></div>
					<div class="alert-content">
						<h4 class="alert-title">Record Add Failed</h4>
						<p>Employee Attendance Already Exist.</p>
					</div>
				</div>');
                } else {
                    $TodaysDate = date("Y-m-d");
                    $seldataqddtZXC = select("count(*)", "tblEmployeeWeekOff", "EID='" . $EID . "' and  Date('" . $TodaysDate . "') between Date(WeekOffStartDate) and Date(WeekOffEndDate)");
                    $cnttt = $seldataqddtZXC[0]['count(*)'];

                    if ($type == 2) {
                        if ($cnttt > 0) {
                            $sql1 = "Update tblEmployeesRecords set LogoutTime='" . $attendance_time . "',Status='2',OnLeaveStatus='1',logout_store_id = '" . $storeid . "' "
                                    . ",check_out_by='" . $login_id . "',emp_id='" . $EID . "'  where EmployeeCode ='$emp_code' AND DateOfAttendance='" . $attendance_date . "'";
                            $DB->query($sql1);
                        } else {
                            $sql1 = "Update tblEmployeesRecords set LogoutTime='" . $attendance_time . "',Status='2',logout_store_id = '" . $storeid . "' "
                                    . ",check_out_by='" . $login_id . "',emp_id='" . $EID . "'  where EmployeeCode ='$emp_code' AND DateOfAttendance='" . $attendance_date . "'";
                            $DB->query($sql1);
                        }
                    } else {
                        if ($cnttt > 0) {
                            $sql1 = "Update tblEmployeesRecords set LoginTime='" . $attendance_time . "',Status='1',OnLeaveStatus='1',login_store_id = '" . $storeid . "' "
                                    . ",check_in_by='" . $login_id . "',emp_id='" . $EID . "'  where EmployeeCode ='$emp_code' AND DateOfAttendance='" . $attendance_date . "'";
                            $DB->query($sql1);
                        } else {
                            $sql1 = "Update tblEmployeesRecords set LoginTime='" . $attendance_time . "',Status='1',login_store_id = '" . $storeid . "' "
                                    . ",check_in_by='" . $login_id . "',emp_id='" . $EID . "'  where EmployeeCode ='$emp_code' AND DateOfAttendance='" . $attendance_date . "'";
                            $DB->query($sql1);
                        }
                    }





                    die('<div class="alert alert-success alert-dismissible fade in" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
										</button>
										<strong>Saved Successfully.</strong>
										</div>');
                }
            } else {
                die('<div class="alert alert-warning alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
								</button>
								<strong>Employee Code Cannot be blank.</strong>
							</div>');
            }
        } else {
            die('<div class="alert alert-warning alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
								</button>
								<strong>Attendance Time Cannot be blank.</strong>
							</div>');
        }
    } else {
        die('<div class="alert alert-warning alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
								</button>
								<strong>Attendance Date Cannot be blank.</strong>
							</div>');
    }
    die();
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="refresh" content="240" />
        <?php require_once("incMetaScript.fya"); ?>
        <script type="text/javascript" src="assets/widgets/datepicker/datepicker.js"></script>
        <script type="text/javascript">
            /* Datepicker bootstrap */

            $(function () {
                "use strict";
//                $('.bootstrap-datepicker').bsdatepicker({
//                    format: 'yyyy-mm-dd'
//                });
                $(".datepicker").datepicker();
            });
        </script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker-demo.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/moment.js"></script>
        <script type="text/javascript" src="assets/widgets/timepicker/timepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="assets/widgets/timepicker/timepicker.css">

        <script type="text/javascript">
            /* Timepicker */

            $(function () {
                "use strict";
                $('.timepicker-example').timepicker();
            });
        </script>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");      ?>
            <!----------commented by gandhali 3/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                        </div>
                        <?php require_once("incHeader.fya"); ?>
                        <div class="panel">
                            <div class="panel-body">
                                <div class="panel-body">
                                    <form role="form" class="form-horizontal bordered-row admin_attendance" onSubmit="proceed_formsubmit('.admin_attendance', '<?= $strMyActionPage ?>', '.result_message', '', '.admin_email', '.admin_password');
                                            return false;">
                                        <span class="result_message">&nbsp; <br>
                                        </span>
                                        <br>

                                        <h3 class="title-hero">Add Employee Attendance</h3>


                                        <?php
                                        /*
                                         * Get Data
                                         */
                                        $report_data = select("*", 'report_config', 'status =1 AND report_name="emp_sales" ');

                                        $counter = 1;
                                        ?>
                                        <div class="example-box-wrapper">
                                            <?php /* <div class="form-group"><label class="col-sm-3 control-label">Employee Code</label>
                                              <div class="col-sm-4">
                                              <input autocomplete="off" type="text" name="emp_code"  value=""  class="form-control" required>
                                              </div>
                                              </div> */ ?>

                                            <div class="form-group"><label class="col-sm-3 control-label">Employee Code</label>
                                                <div class="col-sm-4">
                                                    <select name="emp_code" class="form-control" required>
                                                        <option value="">Select Employee</option>
                                                        <?php
                                                        $emp_data = select("*", "tblEmployees", "Status='0'");
                                                        foreach ($emp_data as $val) {
                                                            ?>
                                                            <option value="<?= $val['EmployeeCode'] ?>" ><?= $val['EmployeeName'] . ' - ' . $val['EmployeeCode'] ?></option>
                                                        <?php }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group"><label class="col-sm-3 control-label">Select Store</label>
                                                <div class="col-sm-4">
                                                    <select name="Store" class="form-control" required>
                                                        <option value="">Select Store</option>
                                                        <?php
                                                        $selp = select("*", "tblStores", "Status='0'");
                                                        foreach ($selp as $val) {
                                                            $strStoreName = $val["StoreName"];
                                                            $strStoreID = $val["StoreID"];
                                                            $store = $_GET["Store"];
                                                            ?>
                                                            <option value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>
                                                        <?php }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="form-group"><label class="col-sm-3 control-label">Select Date</label>
                                                <div class="col-sm-4">
                                                    <div  class="input-prepend input-group"><span class="add-on input-group-addon"><i class="glyph-icon icon-calendar"></i></span> <input type="text" autocomplete="off"  name="attendance_date" value="<?= $row_appointments[$key] ?>" id="AppointmentDate"  class="form-control datepicker" data-date-format="yyyy/mm/dd" value="<?php echo date('Y-m-d'); ?>"></div>
                                                </div>
                                            </div>


                                            <div class="form-group"><label class="col-sm-3 control-label">Select Time</label>
                                                <div class="col-sm-4">
                                                    <input autocomplete="off" type="text" name="attendance_time"  value="<?php echo isset($time_in_12_hour_format) ? $time_in_12_hour_format : ''; ?>"  id="SuitableAppointmentTime" class="form-control timepicker-example" required>
                                                </div>
                                            </div>

                                            <div class="form-group"><label class="col-sm-3 control-label">Type</label>
                                                <div class="col-sm-4">
                                                    <input type = "radio" name = "type" value = "1" id = "type_1" checked/><b><label for="type_1">Check In</label></b>&nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                    <input type = "radio" name = "type" value = "2" id = "type_2"/><b><label for="type_2">Check Out</label></b>&nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-6">
                                                    <input type="submit" class="btn ra-100 btn-primary" value="Submit">

                                                </div>
                                            </div>
                                        </div>


                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <script type="text/javascript">
                var DivCount = '<?php echo $counter; ?>';
                $(document).ready(function () {


                    $(".add_field_button").click(function (e) {
                        e.preventDefault();
                        // Max attachment allowed
                        var append_data = '<div class="form-group" id="div_count_' + DivCount + '"><label class="col-sm-3 control-label"></label>' +
                                '<div class="col-sm-4">' +
                                '<input autocomplete="off" type="email" name="email_ids[' + DivCount + ']" value="" class="form-control required">'
                                + '</div><div class="col-sm-4"><label class="col-sm-4 control-label" style="text-align:left;"><a href="#" class="btn btn-link remove_field" data-lang="' + DivCount + '">Remove</a></label></div></div>';
                        $(".config_div").append(append_data); //add attachment
                        DivCount = parseInt(DivCount) + 1;
                    });

                    $(".config_div").on("click", ".remove_field", function (e) { //user click on to remove attachment
                        var div_count = $(this).attr('data-lang');
                        e.preventDefault();
                        $('#div_count_' + div_count).remove();
                    });
                });
            </script>
            <?php require_once 'incFooter.fya'; ?>
        </div>

    </body>
</html>
