<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php
$strPageTitle = "Super Gift Voucher Management| Nailspa";
$strDisplayTitle = "Manage Super Gift Voucher| Nailspa";
$strMenuID = "3";
$strMyTable = "tblOffers";
$strMyTableID = "OfferID";
$strMyField = "OfferCode";
$strMyActionPage = "ManageSuperGV.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights	

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $TagID = $_POST["GV_ID"];
    $strStep = Filter($_POST["step"]);
    if ($strStep == "add") {

        $strGVname = Filter($_POST["GVname"]);
        $strGVDateFrom = $_POST["GVDateFrom"];
        $strGVDateTo = $_POST["GVDateTo"];
        $strOfferDateFrom1 = date("Y-m-d", strtotime($strGVDateFrom));
        $strOfferDateTo1 = date("Y-m-d", strtotime($strGVDateTo));
        $strStoreID = $_POST["StoreID"];
        $GVAmount = Filter($_POST["GVAmount"]);
        $offercode = Filter($_POST["GVcode"]);
        $percentage = $_POST["percentage"];

        $DB = Connect();
        $sql = "SELECT id FROM super_gift_voucher WHERE GVcode='" . $offercode . "' AND status=1";
        $RS = $DB->query($sql);
        if ($RS->num_rows > 0) {
            $DB->close();
            die('<div class="alert alert-warning alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
								</button>
								<strong>The Super Gift Voucher Code already exists in the system.</strong>
							</div>');
            die("");
        } else {

            $sqlInsert = "Insert into super_gift_voucher (GVname,GVcode,GVDateFrom, GVDateTo, GVAmount, StoreID,created_date,created_by,percentage) values
						('" . $strGVname . "','" . $offercode . "', '" . $strOfferDateFrom1 . "', '" . $strOfferDateTo1 . "', '" . $GVAmount . "', '" . implode(",", $strStoreID) . "' , '" . date('Y-m-d H:i:s') . "', '" . $strAdminID . "','" . $percentage . "')";

            if ($DB->query($sqlInsert) === TRUE) {


                $DB->Close();
                die('<div class="alert alert-close alert-success">
								<div class="bg-green alert-icon"><i class="glyph-icon icon-check"></i></div>
								<div class="alert-content">
									<h4 class="alert-title">Record Added Successfully</h4>
									</div>
							</div>');
            }
        }
    }

    if ($strStep == "edit") {
        $DB = Connect();
        $GV_ID = $_POST["GV_ID"];
        $strGVname = Filter($_POST["GVname"]);
        $strGVDateFrom = $_POST["GVDateFrom"];
        $strGVDateTo = $_POST["GVDateTo"];
        $strOfferDateFrom1 = date("Y-m-d", strtotime($strGVDateFrom));
        $strOfferDateTo1 = date("Y-m-d", strtotime($strGVDateTo));
        $strStoreID = $_POST["StoreID"];
        $GVAmount = Filter($_POST["GVAmount"]);
        $offercode = Filter($_POST["GVcode"]);
        $percentage = $_POST["percentage"];

        $updateq = "UPDATE super_gift_voucher SET GVname='" . $strGVname . "',GVDateFrom='" . $strOfferDateFrom1 . "',"
                . "GVDateTo='" . $strOfferDateTo1 . "',GVAmount='" . $GVAmount . "',"
                . "StoreID='" . implode(",", $strStoreID) . "',modified_date='" . date('Y-m-d H:i:s') . "',"
                . "modified_by='" . $strAdminID . "',percentage='" . $percentage . "' WHERE id='" . $GV_ID . "'";
        $DB->query($updateq);

        die('<div class="alert alert-close alert-success">
					<div class="bg-green alert-icon"><i class="glyph-icon icon-check"></i></div>
					<div class="alert-content">
						<h4 class="alert-title">Record Updated Successfully</h4>
					</div>
				</div>');
    }
    die();
}
?>
<!DOCTYPE html>
<html lang="en">

    <head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <?php require_once("incMetaScript.fya"); ?>
        <!-----------css & js files added for tabs by gandhali 5/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");   ?>
            <!----------commented by gandhali 5/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>
                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>

                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                            <p>Add, edit, delete POST</p>
                        </div>

                        <?php
                        if (!isset($_GET["gv_id"])) {
                            ?>				
                            <div class="panel">
                                <div class="panel">
                                    <div class="panel-body">

                                        <div class="example-box-wrapper">
                                            <div class="tabs">
                                                <ul>
                                                    <li><a href="#normal-tabs-1" title="Tab 1">Manage</a></li>
                                                    <li><a href="#normal-tabs-2" title="Tab 2">Add</a></li>
                                                </ul>
                                                <div id="normal-tabs-1">

                                                    <span class="form_result">&nbsp; <br>
                                                    </span>

                                                    <div class="panel-body">
                                                        <h3 class="title-hero">List of Super Gift Voucher | Nailspa</h3>
                                                        <div class="example-box-wrapper">
                                                            <table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                                                                <thead>
                                                                    <tr>

                                                                        <th>Super Gift Voucher Name</th>
                                                                        <th>Super Gift Voucher Code</th>
                                                                        <th>Date & Time</th>
                                                                        <th>Amount</th>
                                                                        <th>Actions</th>
                                                                    </tr>
                                                                </thead>
                                                                <tfoot>
                                                                    <tr>

                                                                        <th>Super Gift Voucher Name</th>
                                                                        <th>Super Gift Voucher Code</th>
                                                                        <th>Date & Time</th>
                                                                        <th>Amount</th>
                                                                        <th>Actions</th>
                                                                    </tr>
                                                                </tfoot>
                                                                <tbody>
                                                                    <?php
// Create connection And Write Values
                                                                    $DB = Connect();
                                                                    $sql = "Select * FROM super_gift_voucher WHERE status=1 order by id desc";
                                                                    $RS = $DB->query($sql);
                                                                    if ($RS->num_rows > 0) {
                                                                        $counter = 0;

                                                                        while ($row = $RS->fetch_assoc()) {
                                                                            $counter ++;
                                                                            $strOfferID = $row["id"];
                                                                            ?>	
                                                                            <tr id="my_data_tr_<?= $counter ?>">
                                                                                <td><?= $row['GVname'] ?></td>
                                                                                <td><?= $row['GVcode'] ?></td>
                                                                                <td><b>From :</b> <?= $row['GVDateFrom'] ?> <br><b>To :</b> <?= $row['GVDateTo'] ?></td>
                                                                                <td><?= $row['GVAmount'] ?></td>
                                                                                <td>
                                                                                    <a class="btn btn-link" href="<?= $strMyActionPage ?>?gv_id=<?= $row['id'] ?>">Edit</a>
                                                                                    <a class="btn btn-link font-red" font-redhref="javascript:;" onclick="DeleteData('StepSuperGV', '<?= $row['id'] ?>', 'Are you sure you want to delete this Gift Voucher - <?= $row['GVname'] ?>?', 'my_data_tr_<?= $counter ?>');">Delete</a>

                                                                                    <br>
                                                                                </td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                    } else {
                                                                        ?>	
                                                                        <tr>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td>No Records Found</td>
                                                                            <td></td>

                                                                            <td></td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    $DB->close();
                                                                    ?>
                                                                    <!--TAB 2 START-->											

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="normal-tabs-2">

                                                    <div class="panel-body">

                                                        <form role="form" enctype="multipart/form-data" class="form-horizontal bordered-row enquiry_form" onSubmit="proceed_formsubmit('.enquiry_form', '<?= $strMyActionPage ?>', '.result_message', '', '', '', '.file_upload');
                                                                return false;">

                                                            <span class="result_message">&nbsp; <br>
                                                            </span>

                                                            <input type="hidden" name="step" value="add">

                                                            <h3 class="title-hero">New Super Gift Voucher</h3>
                                                            <script>
                                                                $(function () {
                                                                    $("#GVDateTo").datepicker({minDate: 0});
                                                                    $("#GVDateFrom").datepicker({minDate: 0});
                                                                });
                                                            </script>
                                                            <div class="example-box-wrapper">


                                                                <div class="form-group"><label class="col-sm-3 control-label">Super Gift Voucher Name <span>*</span> </label>
                                                                    <div class="col-sm-4"><input type="text" name="GVname"  class="form-control required" placeholder="Super Gift Voucher Name"></div>
                                                                </div>



                                                                <div class="form-group"><label class="col-sm-3 control-label">Super Gift Voucher Code <span>*</span></label>
                                                                    <div class="col-sm-4"><input type="text" name="GVcode"  class="form-control required" placeholder="Super Gift Voucher Code"></div>
                                                                </div>

                                                                <div class="form-group"><label class="col-sm-3 control-label">Gift Voucher Date From <span>*</span></label>
                                                                    <div class="col-md-4">
                                                                        <div class="input-prepend input-group"><span class="add-on input-group-addon"><i class="glyph-icon icon-calendar"></i></span> <input type="text" name="GVDateFrom" id="GVDateFrom" class="form-control" value="<?php echo date('Y-m-d'); ?>"></div>
                                                                    </div>
                                                                </div>


                                                                <div class="form-group"><label class="col-sm-3 control-label">Gift Voucher Date To <span>*</span></label>
                                                                    <div class="col-md-4">
                                                                        <div class="input-prepend input-group"><span class="add-on input-group-addon"><i class="glyph-icon icon-calendar"></i></span> <input type="text" name="GVDateTo" id="GVDateTo" class="form-control" value="<?php echo date('Y-m-d'); ?>"></div>
                                                                    </div>
                                                                </div>



                                                                <div class="form-group"><label class="col-sm-3 control-label">Amount <span>*</span></label>
                                                                    <div class="col-sm-4">
                                                                        <input type="text" name="GVAmount" id="<?= str_replace("Amount", "TypeAmount", $row["Field"]) ?>" class="form-control required" placeholder="GVAmount" />
                                                                    </div>
                                                                </div>

                                                                <div class="form-group"><label class="col-sm-3 control-label">Percentage <span>*</span></label>
                                                                    <div class="col-sm-4">
                                                                        <input type="text" name="percentage" id="Percentage" class="form-control required" placeholder="Percentage" />
                                                                    </div>
                                                                </div>


                                                                <?php
                                                                $sql1 = "SELECT StoreID, StoreName from tblStores where Status=0";
                                                                $DB = Connect();
                                                                $RS2 = $DB->query($sql1);
                                                                if ($RS2->num_rows > 0) {
                                                                    ?>											
                                                                    <div class="form-group"><label class="col-sm-3 control-label">Store Name<span>*</span></label>
                                                                        <div class="col-sm-4">
                                                                            <select class="form-control required"  name="StoreID[]" id="StoreID" multiple style="height:100pt">
                                                                                <option value="" selected>--SELECT STORE--</option>
                                                                                <?php
                                                                                while ($row2 = $RS2->fetch_assoc()) {
                                                                                    $StoreID = $row2["StoreID"];
                                                                                    $StoreName = $row2["StoreName"];
                                                                                    ?>
                                                                                    <option value="<?= $StoreID ?>" ><?= $StoreName ?></option>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                            </select>
                                                                            <?php
                                                                        }
                                                                        $DB->close();
                                                                        ?>
                                                                    </div>
                                                                </div>


                                                                <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                                    <input type="submit" class="btn ra-100 btn-primary" value="Submit">

                                                                    <div class="col-sm-1"><a class="btn ra-100 btn-black-opacity" href="javascript:;" onclick="ClearInfo('enquiry_form');" title="Clear"><span>Clear</span></a></div>
                                                                </div>

                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        } // End null condition
                        else {
                            ?>			

                            <div class="panel">
                                <div class="panel-body">
                                    <div class="fa-hover">	
                                        <a class="btn btn-primary btn-lg btn-block" href="<?= $strMyActionPage ?>"><i class="fa fa-backward"></i> &nbsp; Go back to <?= $strPageTitle ?></a>
                                    </div>

                                    <div class="panel-body">
                                        <form role="form" enctype="multipart/form-data" class="form-horizontal bordered-row enquiry_form" onSubmit="proceed_formsubmit('.enquiry_form', '<?= $strMyActionPage ?>', '.result_message', '', '', '', '.file_upload');
                                                return false;">

                                            <span class="result_message">&nbsp; <br>
                                            </span>
                                            <br>
                                            <input type="hidden" name="step" value="edit">


                                            <h3 class="title-hero">Edit POST</h3>
                                            <div class="example-box-wrapper">
                                                <script>
                                                    $(function () {
                                                        $("#GVDateTo").datepicker({minDate: 0});
                                                        $("#GVDateFrom").datepicker({minDate: 0});
                                                    });

                                                </script>	
                                                <?php
                                                $strID = $_GET["gv_id"];
                                                $DB = Connect();
                                                $sql = "select * FROM super_gift_voucher where id = '$strID'";
                                                $RS = $DB->query($sql);
                                                if ($RS->num_rows > 0) {
                                                    while ($row = $RS->fetch_assoc()) {
                                                        $edit_data = $row;
                                                    }
                                                }
                                                ?>	
                                                <div class="example-box-wrapper">
                                                    <input type="hidden" name="GV_ID" value="<?php echo isset($edit_data['id']) ? $edit_data['id'] : '0' ?>"/>

                                                    <div class="form-group"><label class="col-sm-3 control-label">Super Gift Voucher Name <span>*</span> </label>
                                                        <div class="col-sm-4"><input type="text" name="GVname"  class="form-control required" placeholder="Super Gift Voucher Name" value="<?php echo isset($edit_data['GVname']) ? $edit_data['GVname'] : '' ?>"></div>
                                                    </div>



                                                    <div class="form-group"><label class="col-sm-3 control-label">Super Gift Voucher Code <span>*</span></label>
                                                        <div class="col-sm-4"><input type="text" name="GVcode"  class="form-control required" placeholder="Super Gift Voucher Code" value="<?php echo isset($edit_data['GVcode']) ? $edit_data['GVcode'] : '' ?>"></div>
                                                    </div>

                                                    <div class="form-group"><label class="col-sm-3 control-label">Gift Voucher Date From <span>*</span></label>
                                                        <div class="col-md-4">
                                                            <div class="input-prepend input-group"><span class="add-on input-group-addon"><i class="glyph-icon icon-calendar"></i></span> <input type="text" name="GVDateFrom" id="GVDateFrom" class="form-control" value="<?php echo isset($edit_data['GVDateFrom']) && $edit_data['GVDateFrom'] != '1970-01-01' ? $edit_data['GVDateFrom'] : date('Y-m-d'); ?>"></div>
                                                        </div>
                                                    </div>


                                                    <div class="form-group"><label class="col-sm-3 control-label">Gift Voucher Date To <span>*</span></label>
                                                        <div class="col-md-4">
                                                            <div class="input-prepend input-group"><span class="add-on input-group-addon"><i class="glyph-icon icon-calendar"></i></span> <input type="text" name="GVDateTo" id="GVDateTo" class="form-control" value="<?php echo isset($edit_data['GVDateTo']) && $edit_data['GVDateTo'] != '1970-01-01' ? $edit_data['GVDateTo'] : date('Y-m-d'); ?>"></div>
                                                        </div>
                                                    </div>



                                                    <div class="form-group"><label class="col-sm-3 control-label">Amount <span>*</span></label>
                                                        <div class="col-sm-4">
                                                            <input type="text" name="GVAmount" id="<?= str_replace("Amount", "TypeAmount", $row["Field"]) ?>" class="form-control required" placeholder="Amount" value="<?php echo isset($edit_data['GVAmount']) ? $edit_data['GVAmount'] : '' ?>"/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group"><label class="col-sm-3 control-label">Percentage <span>*</span></label>
                                                        <div class="col-sm-4">
                                                            <input type="text" name="percentage" id="Percentage" class="form-control required" placeholder="Percentage" value="<?php echo isset($edit_data['percentage']) ? $edit_data['percentage'] : '' ?>"/>
                                                        </div>
                                                    </div>


                                                    <?php
                                                    $sql1 = "SELECT StoreID, StoreName from tblStores where Status=0";
                                                    $DB = Connect();
                                                    $RS2 = $DB->query($sql1);
                                                    $gv_store = isset($edit_data['StoreID']) ? explode(",", $edit_data['StoreID']) : array();
                                                    if ($RS2->num_rows > 0) {
                                                        ?>											
                                                        <div class="form-group"><label class="col-sm-3 control-label">Store Name<span>*</span></label>
                                                            <div class="col-sm-4">
                                                                <select class="form-control required"  name="StoreID[]" id="StoreID" multiple style="height:100pt">
                                                                    <?php
                                                                    while ($row2 = $RS2->fetch_assoc()) {
                                                                        $StoreID = $row2["StoreID"];
                                                                        $StoreName = $row2["StoreName"];
                                                                        ?>
                                                                        <option value="<?= $StoreID ?>" <?php echo isset($gv_store) && in_array($StoreID, $gv_store) ? 'selected' : ''; ?>><?= $StoreName ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                                <?php
                                                            }
                                                            $DB->close();
                                                            ?>
                                                        </div>
                                                    </div>

                                                    <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                        <input type="submit" class="btn ra-100 btn-primary" value="Submit">

                                                        <div class="col-sm-1"><a class="btn ra-100 btn-black-opacity" href="javascript:;" onclick="ClearInfo('enquiry_form');" title="Clear"><span>Clear</span></a></div>
                                                    </div>

                                                </div>




                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>			
                            <?php
                        }
                        ?>	                   
                    </div>
                </div>
            </div>

            <?php require_once 'incFooter.fya'; ?>

        </div>
        <script type="text/javascript" src="assets/widgets/chosen/chosen.js"></script>
        <link rel="stylesheet" type="text/css" href="assets/widgets/chosen/chosen.css">
        <script>
                                                        function selectService() {
                                                            if ($('#sel_all_ser').is(':checked')) {
                                                                // $('.jqr_service_div option').attr('selected', 'selected');
                                                                //$('.jqr_service_div option').attr('style', 'background-color:#D3D3D3;');
                                                                $('.jqr_service_div option').prop('selected', true);
                                                                $('#sel_all_ser').trigger('liszt:updated');
                                                            } else {
                                                                //$('.jqr_service_div option').removeAttr('selected');
                                                                //$('.jqr_service_div option').attr('style', 'background-color:#fff;');
                                                                $('.jqr_service_div option').prop('selected', false);
                                                                $('#sel_all_ser').trigger('liszt:updated');
                                                            }
                                                        }
                                                        $(document).ready(function () {

                                                            $(".chosen-select").chosen();

                                                        });

        </script>
    </body>
</html>