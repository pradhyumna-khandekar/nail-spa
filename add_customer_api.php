<?php require_once("setting.fya"); ?>
<?php

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
    $post_data = $_POST;
    $DB = Connect();
    if (isset($post_data['name']) && $post_data['name'] != '') {
        $fullname = trim($post_data['name']);
        $namearr = explode(" ", $fullname);
        $firstname = '';
        $lastname = '';
        $cust_location = '';
        $gender = 0;
        $reg_date = date('Y-m-d H:i:s');

        if (isset($namearr) && is_array($namearr) && count($namearr) > 0) {
            $firstname = $namearr[0];
            unset($namearr[0]);
            $lastname = implode(" ", $namearr);
        }


        $store_id = isset($post_data['store_id']) ? $post_data['store_id'] : '0';
        $email_id = isset($post_data['email']) ? $post_data['email'] : '';
        $mobile = isset($post_data['number']) ? $post_data['number'] : '';
        $acquisition = isset($post_data['acquisition']) ? $post_data['acquisition'] : '';
        $website_customer = isset($post_data['website_customer']) ? $post_data['website_customer'] : '2';

        /*
         * Check Customer Exist or not
         */
        $custexist_q = "Select * from tblCustomers Where CustomerMobileNo LIKE '%" . $mobile . "%'";
        $custexist_exe = $DB->query($custexist_q);
        if ($custexist_exe->num_rows == 0) {
            $insert_query = "INSERT INTO `tblCustomers` (`FirstName`, `LastName`,"
                    . " `CustomerFullName`, `CustomerEmailID`, `CustomerMobileNo`, `Gender`, `Acquisition`, `CustomerLocation`, "
                    . "`website_customer`,`RegDate`,PreferredStoreID)"
                    . " VALUES ('" . $firstname . "', '" . $lastname . "', '" . $fullname . "', '" . $email_id . "', '" . $mobile . "', '" . $gender . "',"
                    . " '" . $acquisition . "', '" . $cust_location . "', '" . $website_customer . "', '" . $reg_date . "','" . $store_id . "');";
            $DB->query($insert_query);
            $customer_id = $DB->insert_id;
        }
        $CustomerMobileNo = $mobile;
        $seldata = select("*", "tblStores", "StoreID='" . $store_id . "'");
        $SMSContentforImmediate = "Dear " . ucwords($fullname) . ", thank you for booking with us online. Our salon manager will reconfirm your appnt over a call soon or call (" . $seldata[0]['StoreOfficialNumber'] . ")";
        CreateSMSURL("Appointment Booked", "0", "0", $SMSContentforImmediate, $CustomerMobileNo);
    }
}
?>