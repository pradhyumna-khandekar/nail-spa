<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Manage Customers | Nailspa";
$strDisplayTitle = "View Details Of Customers for Nailspa";
$strMenuID = "10";
$strMyTable = "tblCustomers";
$strMyTableID = "CustomerID";
$strMyField = "CustomerMobileNo";
$strMyActionPage = "ManageCustomers.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
if (isset($_GET['renew'])) {
    $DB = Connect();
    $ID = $_GET['renew'];
    $date = date('Y-m-d');
    $new_date = strtotime(date("Y-m-d", strtotime($date)) . " +12 month");
    $new_dated = date("Y-m-d", $new_date);
    $sell = select("*", "tblMembership", "MembershipID='" . $ID . "'");
    $endate = $sell[0]['TimeForDiscountEnd'];
    $sql = "SELECT * FROM tblCustomers WHERE memberid='" . $ID . "'";
    $RS = $DB->query($sql);
    if ($RS->num_rows > 0) {
        $counter = 0;

        while ($row = $RS->fetch_assoc()) {

            $CustomerEmailID = $row['CustomerEmailID'];
            $CustomerFullName = $row['CustomerFullName'];
            $CustomerMobileNo = $row['CustomerMobileNo'];


            $strTo = $CustomerEmailID;
            $strFrom = "order@fyatest.website";
            $strSubject = "Your Membership Renew Successfully of Nailspa services";
            $strBody = "";
            $strStatus = "0"; // Pending = 0 / Sent = 1 / Error = 2
            $Name = $CustomerFullName;
            $strDate = $endate;

            $seldata = select("*", "tblMembership", "MembershipID='" . $ID . "'");
            $MembershipName = $seldata[0]['MembershipName'];
            $Type = $seldata[0]['Type'];
            if ($Type == '0') {
                $type = 'Elite Membership';
            } else {
                $type = 'Normal Membership';
            }
            $DiscountType = $seldata[0]['DiscountType'];
            $Discount = $seldata[0]['Discount'];
            $TimeForDiscountEnd = $seldata[0]['TimeForDiscountEnd'];
            $storeid = $seldata[0]['storeid'];
            $sep = select("*", "tblStores", "StoreID='" . $storeid . "'");
            $officeaddress = $sep[0]['StoreOfficialAddress'];
            $storename = $sep[0]['StoreName'];
            $branche = explode(",", $storename);
            $branchname = $branche[1];

            $message = file_get_contents('EmailFormat/membership_Renew.html');
            $message = eregi_replace("[\]", '', $message);
//setup vars to replace
            $vars = array('{membership_name}', '{member_name}', '{Discount}', '{TimeForDiscountEnd}', '{Address}', '{Branch}', '{Renewdate}'); //Replace varaibles
            $values = array($MembershipName, $Name, $Discount, $TimeForDiscountEnd, $officeaddress, $branchname, $new_dated);

//replace vars
            $message = str_replace($vars, $values, $message);

            $strBody1 = $message;

// exit();

            $flag = 'CM';
            $id = $last_id2;

            sendmail($id, $strTo, $strFrom, $strSubject, $strBody1, $strDate, $flag, $strStatus);
        }
    }


    $sqlUpdate = "UPDATE $strMyTable SET TimeForDiscountEnd='" . $new_dated . "' WHERE $strMyTableID='" . $ID . "'";
    ExecuteNQ($sqlUpdate);

    $msg = "Membership Renew Successfully";
    $DB->close();
    header('Location:viewcustomers.php?msg=' . $msg . '');
}
// code for not allowing the normal admin to access the super admin rights	
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <script type="text/javascript" src="assets/widgets/chosen/chosen.js"></script>
        <script type="text/javascript" src="assets/widgets/chosen/chosen-demo.js"></script>
        <script type="text/javascript" src="assets/widgets/datepicker/datepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="assets/widgets/chosen/chosen.css">
        <?php /* <script type="text/javascript" src="assets/widgets/modal/modal.js"></script> */ ?>
        <script src="https://cdn.ckeditor.com/4.10.1/standard/ckeditor.js"></script>

        <script type="text/javascript">

            /* Datepicker bootstrap */
            $(function () {
                "use strict";
                $('.bootstrap-datepicker').bsdatepicker({
                    format: 'yyyy-mm-dd'
                });
            });
        </script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker-demo.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/moment.js"></script>
        <script type="text/javascript" src="assets/widgets/timepicker/timepicker.js"></script>
        <script type="text/javascript">
            /* Timepicker */

            $(function () {
                "use strict";
                $('.timepicker-example').timepicker();
            });
        </script>
        <script>
            $(function ()
            {
                $("#StartDay").datepicker({minDate: 0});
                $("#EndtDay").datepicker({minDate: 0});
                $("#StartDay1").datepicker({minDate: 0});
                $("#EndtDay1").datepicker({minDate: 0});

            });
        </script>	

        <script>
            $(document).ready(function () {
                /* 	$(".deleteq").click(function(){
                 //alert(134)
                 var uid=$("#uidd").val();
                 //alert(uid)
                 $.ajax({
                 type:"post",
                 data:"uid="+uid,
                 url:"delete_customer.php",
                 success:function(result)
                 {
                 //alert(result)
                 if(result==1)
                 {
                 alert('You Cannot Delete This Customer')
                 }
                 else
                 {
                 alert(result)
                 }
                 }
                 
                 });
                 
                 }); */
            });
            function LoadValue(OptionValue)
            {
                // alert (OptionValue);
                $.ajax({
                    type: 'POST',
                    url: "GetServicesStoreWise.php",
                    data: {
                        id: OptionValue
                    },
                    success: function (response) {
                        //	alert(response)
                        $("#asmita").html("");
                        $("#asmita1").html("");
                        $("#asmita").html(response);
                        $(".chosen-select").chosen();

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $("#asmita").html("<center><font color='red'><b>Please try again after some time</b></font></center>");
                        return false;
                        alert(response);
                    }
                });

            }


            function LoadValueasmita()
            {

                valuable = [];
                var valuable = $('#Services').val();
                var store = $('#StoreID').val();

                //alert(store)
                $.ajax({
                    type: 'POST',
                    url: "servicedetail.php",
                    data: {
                        id: valuable,
                        stored: store
                    },
                    success: function (response) {
                        //alert(response)
                        $("#asmita1").html("");
                        $("#asmita1").html(response);

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $("#asmita1").html("<center><font color='red'><b>Please try again after some time</b></font></center>");
                        return false;
                        //alert (response);
                    }
                });
            }

            function LoadServiceValue(OptionValue)
            {
                // alert (OptionValue);
                $.ajax({
                    type: 'POST',
                    url: "storeServiceList.php",
                    data: {
                        id: OptionValue
                    },
                    success: function (response) {
                        //	alert(response)
                        $("#asmita").html("");
                        $("#asmita1").html("");
                        $("#asmita").html(response);
                        $(".chosen-select").chosen();

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $("#asmita").html("<center><font color='red'><b>Please try again after some time</b></font></center>");
                        return false;
                        alert(response);
                    }
                });
            }
        </script>
        <script>

            $(function () {
                $("#AppointmentDate").datepicker({minDate: 0});
                $("#AppointmentDate").datepicker({minDate: 0});

            });
        </script>
        <!-----------css & js files added for tabs by gandhali 3/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>

    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");  ?>
            <!----------commented by gandhali 3/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>


                        <div class="modal fade" id="bulk_sms_modal" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">SMS Content</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" class="form-horizontal bordered-row" role="form" action="customerBulkMessage.php">
                                            <input type="hidden" name="type" value="sms">
                                            <input type="hidden" name="StoreID" value="<?php echo isset($_GET['StoreID']) ? $_GET['StoreID'] : '' ?>">
                                            <input type="hidden" name="Services" value="<?php echo isset($_GET['Services']) ? implode(",", $_GET['Services']) : '' ?>">
                                            <input type="hidden" name="Category" value="<?php echo isset($_GET['Category']) ? implode(",", $_GET['Category']) : '' ?>">
                                            <input type="hidden" name="Gender" value="<?php echo isset($_GET['Gender']) ? $_GET['Gender'] : '' ?>">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Message<span>*</span></label>
                                                <div class="col-sm-8">
                                                    <textarea rows="5" class="form-control" name="message" required="required"></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-6">
                                                    <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Submit</span></button>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="modal fade" id="bulk_email_modal" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Email Content</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" class="form-horizontal bordered-row" role="form" action="customerBulkMessage.php">
                                            <input type="hidden" name="type" value="email">
                                            <input type="hidden" name="StoreID" value="<?php echo isset($_GET['StoreID']) ? $_GET['StoreID'] : '' ?>">
                                            <input type="hidden" name="Services" value="<?php echo isset($_GET['Services']) ? implode(",", $_GET['Services']) : '' ?>">
                                            <input type="hidden" name="Category" value="<?php echo isset($_GET['Category']) ? implode(",", $_GET['Category']) : '' ?>">
                                            <input type="hidden" name="Gender" value="<?php echo isset($_GET['Gender']) ? $_GET['Gender'] : '' ?>">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Subject<span>*</span></label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" name="subject" required="required"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Message<span>*</span></label>
                                                <div class="col-sm-8">
                                                    <textarea rows="5" class="form-control" name="email_message" required="required"></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-6">
                                                    <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Submit</span></button>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>

                            </div>
                        </div>



                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                            <p>Add, Edit, Delete Customers</p>
                        </div>	
                        <div class="panel-body">
                            <h3 class="title-hero">List of Customers | Nailspa</h3>
                            <?php
                            if (isset($_GET['error_msg']) && $_GET['error_msg'] != '') {
                                echo '<div class="alert alert-warning alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
								</button>
								<strong>' . $_GET['error_msg'] . '</strong>
							</div>';
                            }
                            ?>
                            <div class="example-box-wrapper">

                                <?php
                                /*
                                 * if super admin then only show bulk sms and bulk email functionality
                                 */
                                if ($strAdminType == 0) {
                                    ?>

                                    <form method="get" class="form-horizontal bordered-row" role="form">
                                        <?php
                                        $sql1 = "SELECT StoreID, StoreName FROM tblStores WHERE Status = 0";
                                        $RS2 = $DB->query($sql1);
                                        if ($RS2->num_rows > 0) {
                                            ?>
                                            <div class="form-group"><label class="col-sm-2 control-label">Store</label>
                                                <div class="col-sm-4">
                                                    <?php // <select class="form-control required"  id="StoreID"  onChange="LoadServiceValue(this.value);" name="StoreID" > ?>
                                                    <select class="form-control required"  id="StoreID" name="StoreID" >
                                                        <option value="" selected>All</option>
                                                        <?php
                                                        while ($row2 = $RS2->fetch_assoc()) {
                                                            $StoreID = $row2["StoreID"];
                                                            $StoreName = $row2["StoreName"];
                                                            ?>	
                                                            <option value="<?= $StoreID ?>" <?php echo isset($_GET['StoreID']) && $_GET['StoreID'] == $StoreID ? 'selected' : '' ?>><?= $StoreName ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>	
                                            <?php
                                        }
                                        ?>


                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Category</label>
                                            <div class="col-sm-4">
                                                <select name="Category[]" class="form-control">
                                                    <option value="0">All</option>
                                                    <?php
                                                    $categories = select("*", "tblCategories", "CategoryID>'0'");

                                                    if (isset($categories) && is_array($categories) && count($categories) > 0) {
                                                        foreach ($categories as $catval) {
                                                            $strCatName = $catval["CategoryName"];
                                                            $strCatID = $catval["CategoryID"];
                                                            ?>
                                                            <option value="<?= $strCatID ?>" <?php echo isset($_GET['Category']) && in_array($strCatID, $_GET['Category']) ? 'selected' : '' ?>><?= $strCatName ?></option>														
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Gender</label>
                                            <div class="col-sm-4">
                                                <input type="radio" name="Gender"  value="0" id="Gender_male" <?php echo isset($_GET['Gender']) && $_GET['Gender'] == '0' ? 'checked' : ''; ?>/><b><label for="Gender_male">Male</label></b>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input type="radio" name="Gender"  value="1" id="Gender_female" <?php echo isset($_GET['Gender']) && $_GET['Gender'] == '1' ? 'checked' : ''; ?>/><b><label for="Gender_female">Female</label></b>&nbsp;&nbsp;&nbsp;&nbsp;
                                            </div>
                                        </div>


                                        <span id="asmita">
                                            <?php /*
                                              if (isset($_GET['StoreID']) && $_GET['StoreID'] != '') {
                                              $sql = "SELECT ServiceID, ServiceName, ServiceCost FROM tblServices WHERE StoreID=" . $_GET['StoreID'];
                                              $RS = $DB->query($sql);
                                              ?>
                                              <div class="form-group"><label class="col-sm-2 control-label">Services </label>
                                              <div class="col-sm-4 ">
                                              <select class="form-control chosen-select" name="Services[]"  id="Services" style="width:161%" multiple="multiple">
                                              <option value="" align="center"><b>--Select Services--</b></option>
                                              <?php
                                              if ($RS->num_rows > 0) {
                                              while ($row = $RS->fetch_assoc()) {
                                              $ServiceID = $row["ServiceID"];
                                              $ServiceName = $row["ServiceName"];
                                              $ServiceCost = $row["ServiceCost"];
                                              ?>
                                              <option value="<?= $ServiceID ?>"
                                              <?php
                                              if (isset($_GET['Services']) && is_array($_GET['Services']) && count($_GET['Services']) > 0) {
                                              if (in_array($ServiceID, $_GET['Services'])) {
                                              echo 'selected';
                                              }
                                              }
                                              ?>><?= $ServiceName ?>, Rs. <?= $ServiceCost ?></option>
                                              <?php
                                              }
                                              }
                                              ?>
                                              </select>
                                              </div>
                                              </div>
                                              <?php } */
                                            ?>
                                        </span>

                                        <span id="asmita1">

                                        </span>
                                        <div class="form-group"><label class="col-sm-3 control-label"></label>
                                            <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                            &nbsp;&nbsp;&nbsp;
                                            <a class="btn btn-link" href="viewcustomers.php">Clear All Filter</a> &nbsp;&nbsp;&nbsp;
                                            <?php $get_param = $_SERVER['QUERY_STRING']; ?>
                                            <a class="btn btn-alt btn-hover btn-danger" data-toggle="modal" data-target="#bulk_sms_modal" href="#">Bulk SMS</a> &nbsp;&nbsp;&nbsp;
                                            <a class="btn btn-alt btn-hover btn-warning" data-toggle="modal" data-target="#bulk_email_modal" href="#">Bulk Email</a> &nbsp;&nbsp;&nbsp;
                                            <?php /* <a class="btn btn-alt btn-hover btn-warning" href="customerBulkMessage.php?type=sms&<?php echo $get_param; ?>">Bulk Email</a> &nbsp;&nbsp;&nbsp;
                                              <a class="btn btn-alt btn-hover btn-danger" href="customerBulkMessage.php?type=email&<?php echo $get_param; ?>">Bulk SMS</a> &nbsp;&nbsp;&nbsp; */ ?>
                                        </div>
                                    </form>

                                <?php } ?>


                                <?php if (isset($_GET) && is_array($_GET) && count($_GET) > 0 && !isset($_GET['error_msg'])) { ?>
                                    <a href="DownloadCutomerExcel.php" class="download_excel" style="display: block;margin: 0 auto;max-width: max-content;background: black;color: #fff;font-size: 16px;padding: 5px;font-weight: bold;">Download Excel</a>
                                <?php } ?>

                                <?php
// Create connection And Write Values
                                $DB = Connect();

                                $append_query = "";
                                if (isset($_GET['StoreID']) && $_GET['StoreID'] != '') {
                                    $storefilq = "SELECT AppointmentID,CustomerID FROM tblAppointments WHERE StoreID ='" . $_GET['StoreID'] . "' AND AppointmentCheckInTime!='00:00:00' AND Status=2";
                                    $storefilq_exe = $DB->query($storefilq);
                                    if ($storefilq_exe->num_rows > 0) {
                                        while ($store_cust = $storefilq_exe->fetch_assoc()) {
                                            $store_cust_in[$store_cust['CustomerID']] = $store_cust['CustomerID'];
                                        }
                                    }

                                    if (isset($store_cust_in) && is_array($store_cust_in) && count($store_cust_in) > 0) {
                                        $store_in_ids = implode(",", $store_cust_in);
                                    }
                                    if (isset($store_in_ids) && $store_in_ids != '') {
                                        $append_query .= " AND CustomerID IN(" . $store_in_ids . ")";
                                    } else {
                                        $append_query .= " AND CustomerID IN(0)";
                                    }
                                }




                                if (isset($_GET['Services']) && is_array($_GET['Services']) && count($_GET['Services']) > 0) {
                                    $service_in_ids = implode(",", $_GET['Services']);
                                    if ($service_in_ids != '') {
                                        $servfiq = "SELECT aptde.AppointmentID,apt.CustomerID FROM tblappointments apt "
                                                . " JOIN tblAppointmentsDetails aptde ON (apt.AppointmentID = aptde.AppointmentID)"
                                                . " WHERE aptde.ServiceID IN(" . $service_in_ids . ")";
                                        $servfiq_exe = $DB->query($servfiq);
                                        if ($servfiq_exe->num_rows > 0) {
                                            while ($service_cust = $servfiq_exe->fetch_assoc()) {
                                                $service_cust_in[$service_cust['CustomerID']] = $service_cust['CustomerID'];
                                            }
                                        }

                                        if (isset($service_cust_in) && is_array($service_cust_in) && count($service_cust_in) > 0) {
                                            $service_in_ids = implode(",", $service_cust_in);
                                        }
                                        if (isset($service_in_ids) && $service_in_ids != '') {
                                            $append_query .= " AND CustomerID IN(" . $service_in_ids . ")";
                                        } else {
                                            $append_query .= " AND CustomerID IN(0)";
                                        }
                                    }
                                }

                                /*
                                 * add category filter
                                 */

                                if (isset($_GET['Category']) && is_array($_GET['Category']) && count($_GET['Category']) > 0) {
                                    $category_data = array_filter($_GET['Category']);

                                    $cat_in_ids = implode(",", $category_data);
                                    if ($cat_in_ids != '') {
                                        $categoryq = "SELECT aptde.AppointmentID,apt.CustomerID FROM tblAppointments apt "
                                                . " JOIN tblAppointmentsDetails aptde ON (apt.AppointmentID = aptde.AppointmentID)"
                                                . " JOIN tblProductServiceCategory sercat ON(sercat.ServiceID = aptde.ServiceID)"
                                                . " WHERE sercat.CategoryID IN(" . $cat_in_ids . ")";
                                        $categoryq_exe = $DB->query($categoryq);
                                        if ($categoryq_exe->num_rows > 0) {
                                            while ($category_cust = $categoryq_exe->fetch_assoc()) {
                                                $cat_cust_in[$category_cust['CustomerID']] = $category_cust['CustomerID'];
                                            }
                                        }

                                        if (isset($cat_cust_in) && is_array($cat_cust_in) && count($cat_cust_in) > 0) {
                                            $cat_in_ids = implode(",", $cat_cust_in);
                                        }
                                        if (isset($cat_in_ids) && $cat_in_ids != '') {
                                            $append_query .= " AND CustomerID IN(" . $cat_in_ids . ")";
                                        } else {
                                            $append_query .= " AND CustomerID IN(0)";
                                        }
                                    }
                                }


                                /*
                                 * GET unique number and email count
                                 */

                                if (isset($_GET['Gender']) && $_GET['Gender'] != '') {
                                    $append_query .= " AND Gender='" . $_GET['Gender'] . "'";
                                }
                                $email_sql = "SELECT DISTINCT CustomerEmailID FROM " . $strMyTable . " WHERE Status='0' " . $append_query . " AND CustomerEmailID!=''";
                                $email_exe = $DB->query($email_sql);
                                $email_count = $email_exe->num_rows;

                                $phone_sql = "SELECT DISTINCT CustomerMobileNo FROM " . $strMyTable . " WHERE Status='0' " . $append_query . " AND CustomerMobileNo!=''";
                                $phone_exe = $DB->query($phone_sql);
                                $phone_count = $phone_exe->num_rows;

                                $sql = "SELECT * FROM " . $strMyTable . " WHERE Status='0' " . $append_query . " order by CustomerID desc";

                                $RS = $DB->query($sql);
                                if ($RS->num_rows > 0) {

                                    while ($row_data = $RS->fetch_assoc()) {
                                        $final_result[$row_data['CustomerID']] = $row_data;
                                        $mem_in_ids[$row_data["memberid"]] = $row_data["memberid"];
                                    }

                                    if (isset($mem_in_ids) && is_array($mem_in_ids) && count($mem_in_ids) > 0) {
                                        $mem_ids = implode(",", $mem_in_ids);
                                        if (isset($mem_ids) && $mem_ids != '') {
                                            $selectMembershipName = "Select MembershipId,MembershipName from tblMembership where MembershipId IN($mem_ids)";
                                            $RS3 = $DB->query($selectMembershipName);
                                            if ($RS3->num_rows > 0) {
                                                while ($row3 = $RS3->fetch_assoc()) {
                                                    $all_member_name[$row3['MembershipId']] = $row3['MembershipName'];
                                                }
                                            }
                                        }
                                    }
                                }
                                ?>
                                <p class="btn btn-alt btn-hover btn-default">Total Customer: <?php echo isset($final_result) ? count($final_result) : 0; ?></p>
                                <p class="btn btn-alt btn-hover btn-default">Total Email ID.: <?php echo isset($email_count) ? $email_count : 0; ?></p>
                                <p class="btn btn-alt btn-hover btn-default">Total Mobile No.: <?php echo isset($phone_count) ? $phone_count : 0; ?></p>
                                <table id = "datatable-responsive" class = "table table-striped table-bordered responsive no-wrap" cellspacing = "0" width = "100%">
                                    <thead>
                                        <tr>
                                            <th>Sr.No</th>
                                            <th>Full Name</th>
                                            <th>Email ID</th>
                                            <th>Mobile No.</th>
                                            <th>Membership</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Sr.No</th>
                                            <th>Full Name</th>
                                            <th>Email ID</th>
                                            <th>Mobile No.</th>
                                            <th>Membership</th>

                                        </tr>
                                    </tfoot>
                                    <tbody>

                                        <?php
                                        if (isset($final_result) && is_array($final_result) && count($final_result) > 0) {
                                            $counter = 0;

                                            foreach ($final_result as $key => $row) {

                                                $counter ++;
                                                $strCustomerID = $row["CustomerID"];
                                                $getUID = EncodeQ($strCustomerID);
                                                $getUIDDelete = Encode($strCustomerID);
                                                $CustomerFullName = $row["CustomerFullName"];
                                                $CustomerEmailID = $row["CustomerEmailID"];
                                                $CustomerMobileNo = $row["CustomerMobileNo"];
                                                $memid = $row["memberid"];
                                                // echo $memid;
                                                $Status = $row["Status"];



                                                if ($Status == "0") {
                                                    $Status = "Live";
                                                } else {
                                                    $Status = "Offline";
                                                }
                                                ?>	
                                                <tr id="my_data_tr_<?= $counter ?>">
                                                    <td><?= $counter ?></td>
                                                    <td><?= $CustomerFullName ?></td>
                                                    <td><?= $CustomerEmailID ?></td>
                                                    <td><?= $CustomerMobileNo ?></td>
                                                    <td style="text-align: center">
                                                        <?php
                                                        // echo $memid;
                                                        if ($memid == 0) {
                                                            echo "None";
                                                        } else {
                                                            echo isset($all_member_name[$memid]) ? $all_member_name[$memid] : '';
                                                            /* $selectMembershipName = "Select MembershipName from tblMembership where MembershipId='$memid'";
                                                              $RS3 = $DB->query($selectMembershipName);
                                                              if ($RS3->num_rows > 0) {
                                                              while ($row3 = $RS3->fetch_assoc()) {
                                                              $MembershipName = $row3["MembershipName"];
                                                              echo $MembershipName;
                                                              }
                                                              } */
                                                        }

                                                        // $seldata=select("*","tblMembership","MembershipID='".$memid."'");
                                                        // $discountend=$seldata[0]['TimeForDiscountEnd'];
                                                        // echo $strMembershipID;
                                                        // $edate=date('Y-m-d', strtotime($discountend));
                                                        // echo $edate;
                                                        // $todate=date('Y-m-d');
                                                        // if($edate==$todate)
                                                        // {
                                                        // 
                                                        ?>
                                                                    <!--<a class="btn btn-link" href="<?= $strMyActionPage ?>?renew=<?= $memid ?>">Renew</a>-->
                                                        <?php
                                                        // } 
                                                        // 
                                                        ?>
                                                    </td>

                                                </tr>
                                                <?php
                                            }
                                        } else {
                                            ?>															
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>No Records Found</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>

                                            <?php
                                        }
                                        $DB->close();
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
<?php require_once 'incFooter.fya'; ?>
        </div>

    </body>
    <script type="text/javascript">
            CKEDITOR.replace('email_message');
    </script>

</html>