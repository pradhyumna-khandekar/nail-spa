<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Manage Notification Messages | Nailspa";
$strDisplayTitle = "Logs for Notification Messages";
$strMenuID = "3";
$strMyActionPage = "notification_msg_list.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <!-----------css & js files added for tabs by gandhali 3/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>
        <?php /* <script type="text/javascript" src="assets/widgets/modal/modal.js"></script> */ ?>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");     ?>
            <!----------commented by gandhali 3/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>

                        <div class="panel">
                            <div class="panel-body">

                                <div class="panel-body">
                                    <h3 class="title-hero">List Of Notification Messages</h3>
                                    <table id="datatable-responsive-scroll" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Subject</th>
                                                <th>Message</th>
                                                <th>Message Type</th>
                                                <th>Created Date</th>
                                                <th>Created By</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Name</th>
                                                <th>Subject</th>
                                                <th>Message</th>
                                                <th>Message Type</th>
                                                <th>Created Date</th>
                                                <th>Created By</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <?php
                                            $DB = Connect();
                                            /*
                                             * get all users
                                             */
                                            $userq = "SELECT * FROM tblAdmin";
                                            $userq_exe = $DB->query($userq);
                                            if ($userq_exe->num_rows > 0) {
                                                while ($user_row = $userq_exe->fetch_assoc()) {
                                                    $user_data[$user_row['AdminID']] = $user_row;
                                                }
                                            }

                                            $notification_data = select("*", "tblnotification_msg", "status =1");
                                            $DB->close();

                                            if (isset($notification_data) && is_array($notification_data) && count($notification_data) > 0) {
                                                foreach ($notification_data as $key => $value) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $value['name']; ?></td>
                                                        <td><?php echo $value['subject']; ?></td>
                                                        <td>
                                                            <?php
                                                            if ($value['message_mode'] == 1) {
                                                                $message_mode = 'Email';
                                                                ?>
                                                                <a class="btn btn-link" data-toggle="modal" data-target="#show_message_<?php echo $value['id']; ?>" href="#">View Message</a>

                                                                <div class="modal fade email-prev" id="show_message_<?php echo $value['id']; ?>" role="dialog">
                                                                    <div class="modal-dialog">

                                                                        <!-- Modal content-->
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                <h4 class="modal-title">Message Content</h4>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <?php echo $value['message']; ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                            } else {
                                                                $message_mode = 'SMS';
                                                                echo $value['message'];
                                                            }
                                                            ?>
                                                        </td>
                                                        <td><?php echo isset($message_mode) ? $message_mode : ''; ?></td>
                                                        <td><?php echo date('d/m/Y h:i a', strtotime($value['created_date'])); ?></td>
                                                        <td><?php echo isset($user_data[$value['created_by']]) ? $user_data[$value['created_by']]['AdminFullName'] : ''; ?></td>
                                                        <td><a class="btn btn-alt btn-hover btn-danger" href="<?php echo 'membership_emailer.php?msg_id=' . $value['id']; ?>">Edit</a> &nbsp;&nbsp;&nbsp;
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <?php require_once 'incFooter.fya'; ?>
        </div>

    </body>
</html>