<?php

require_once("setting.fya");
require_once 'incFirewall.fya';
require_once("order_helper.php");

$order_id = isset($_GET['id']) ? $_GET['id'] : 0;
$result_data = get_detail_data($order_id, '2');
$order_data = isset($result_data['order_data']) ? $result_data['order_data'] : array();
$store_data = isset($result_data['store_data']) ? $result_data['store_data'] : array();
$created_data = isset($result_data['created_data']) ? $result_data['created_data'] : array();
$order_product = isset($result_data['order_product']) ? $result_data['order_product'] : array();
$pdt_name_data = isset($result_data['pdt_name_data']) ? $result_data['pdt_name_data'] : array();

$fields = array('Name', 'Store', 'Store Address', 'Created By', 'Created Date');
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=Order_Request_' . date('Y-m-d') . '.csv');
$output = fopen("php://output", "w");
fputcsv($output, $fields);


$header_row['name'] = isset($order_data[0]['order_name']) ? $order_data[0]['order_name'] : '';
$header_row['store'] = isset($store_data[0]['StoreName']) ? $store_data[0]['StoreName'] : '';
$header_row['address'] = isset($store_data[0]['StoreOfficialAddress']) ? $store_data[0]['StoreOfficialAddress'] : '';
$header_row['created_by'] = isset($created_data[0]['AdminFullName']) ? $created_data[0]['AdminFullName'] : '';
$header_row['created_date'] = isset($order_data[0]['created_date']) && $order_data[0]['created_date'] != '0000-00-00 00:00:00' ? date('d M, Y h:i a', strtotime($order_data[0]['created_date'])) : '';
fputcsv($output, $header_row);

$blank_fields = array('', '', '', '', '');
fputcsv($output, $blank_fields);

$heading = array('Sr. No.', 'Product', 'Code', 'Quantity');
fputcsv($output, $heading);

$counter = 1;
if (isset($order_product) && is_array($order_product) && count($order_product) > 0) {
    foreach ($order_product as $okey => $ovalue) {
        if (isset($pdt_name_data[$ovalue['product_id']])) {
            $row_data['sr'] = $counter;
            $row_data['pdt'] = $pdt_name_data[$ovalue['product_id']]['name'];
            $row_data['code'] = $pdt_name_data[$ovalue['product_id']]['code'];
            $row_data['quantity'] = $ovalue['quantity'];
            $counter++;
            fputcsv($output, $row_data);
        }
    }
}

fclose($output);
?>