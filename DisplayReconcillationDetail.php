<?php require_once("setting.fya"); ?>
<?php require_once 'incEmpLoginData.php'; ?>

<?php
$strPageTitle = "Reconcillation Details | NailSpa";
$strDisplayTitle = "Reconcillation Details for NailSpa";
$strMenuID = "10";
$strMyTable = "tblAppointments";
$strMyTableID = "AppointmentID";
$strMyField = "AppointmentDate";
$strMyActionPage = "DisplayReconcillationDetail.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>

        <script>

            //$("#reconcillation_confirm").click(function () {
            function proceed_reconcillation() {
            var reconcille_val = [];
                    $.each($("input[class='reconciile_check']:checked"), function () {
                    reconcille_val.push($(this).val());
                    });
                    //var emp_id = '<?php echo $strEmpID; ?>';
                    if (confirm('Are you sure you want to Confirm?')) {
            $.ajax({
            url: "reconcillation_confirm.php",
                    type: 'post',
                    data: {"emp_apt": reconcille_val, "type": '2', "created_id": '<?php echo $strEmpID ?>'},
                    success: function (response) {
                    if (response == 1) {
                    $msg = "Reconcillation Confirmed Successfully!";
                            alert($msg);
                            window.location.reload(true);
                    } else {
                    $msg = "Failed To Confirm!";
                            alert($msg);
                            window.location.reload(true);
                    }
                    }
            });
            }
            }


        </script>

    </head>

    <body>

        <div id="sb-site">


            <?php require_once("incOpenLayout.fya"); ?>


            <?php require_once("incLoader.fya"); ?>


            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("EmpLeftMenu.php"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>


                        <div class="panel">
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="example-box-wrapper">

                                        <div id="normal-tabs-1">
                                            <span class="form_result">&nbsp; <br>
                                            </span>
                                            <h4 class="title-hero"><center><?php echo 'Employee Attendance For ' . date('d/m/Y'); ?> | NailSpa</center></h4>

                                            <div class="col-md-3 col-xs-12 margin-bot-zero no-pad btn" style="border-color: #FC8213;">
                                                <b id="error_msg"></b>
                                                <div id="cur_distance"></div>
                                            </div>
                                            <div class="col-md-3 col-xs-12 margin-bot-zero no-pad btn" style="border-color: #FC8213;">
                                                <div id="cur_lat"></div>
                                            </div>
                                            <div class="col-md-3 col-xs-12 margin-bot-zero no-pad btn" style="border-color: #FC8213;">
                                                <div id="cur_long"></div> 
                                            </div>

                                            <div class="col-md-3 col-xs-12 margin-bot-zero no-pad btn">
                                                <button class="btn btn-alt btn-hover btn-warning" onclick="geoLocation()">Try To get Distance</button> 
                                                <?php /* <a class="btn btn-alt btn-hover btn-warning" href="location.php" class="btn-primary">Try To get Distance</a> */ ?>
                                            </div>
                                            <table id="printdata" class="table table-striped table-bordered table-responsive no-wrap printdata" cellspacing="0" width="100%">

                                                <thead>
                                                    <tr>
                                                        <th>Employee Name</th>
                                                        <th>Store Name</th>
                                                        <th>Date</th>
                                                        <th>CheckIn & CheckOut</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <?php
                                                    $emp_data = select("*", "tblEmployees", "EID='" . $strEmpID . "'");
                                                    if (isset($emp_data) && is_array($emp_data) && count($emp_data) > 0) {
                                                        $store_id = isset($emp_data[0]['StoreID']) ? $emp_data[0]['StoreID'] : 0;
                                                        $store_data = select("*", "tblStores", "StoreID='" . $store_id . "'");
                                                        $latitude = isset($store_data[0]['latitude']) ? ucwords($store_data[0]['latitude']) : '';
                                                        $longitude = isset($store_data[0]['longitude']) ? ucwords($store_data[0]['longitude']) : '';


                                                        $emp_code = isset($emp_data[0]['EmployeeCode']) ? $emp_data[0]['EmployeeCode'] : '';
                                                        $TodaysDate = date('Y-m-d');
                                                        if ($emp_code != '') {
                                                            $attendance_data = select("*", "tblEmployeesRecords", "EmployeeCode='" . $emp_code . "' AND DateOfAttendance='" . $TodaysDate . "'");
                                                        }
                                                        ?>
                                                        <tr>
                                                            <td><?php echo isset($emp_data[0]['EmployeeName']) ? ucwords($emp_data[0]['EmployeeName']) : '' ?></td>
                                                            <td><?php echo isset($store_data[0]['StoreName']) ? ucwords($store_data[0]['StoreName']) : '' ?></td>
                                                            <td><?php echo date('d M,Y'); ?></td>
                                                            <td>

                                                                <div class="in_out_div" style="display:none;">
                                                                    <?php
                                                                    if (isset($attendance_data) && is_array($attendance_data) && count($attendance_data) > 0) {
                                                                        $LoginTime = $attendance_data[0]['LoginTime'];
                                                                        $LogoutTime = $attendance_data[0]['LogoutTime'];
                                                                        $strERID = $attendance_data[0]['ERID'];

                                                                        $currtime = date('H:i:s');
                                                                        $checktimewith = date('H:i:s', strtotime($LoginTime) + 3600);

                                                                        if ($LoginTime == "00:00:00") {
                                                                            ?>
                                                                            <div align="center" id="Checkin">
                                                                                <button class="btn btn-xs btn-primary result_message<?= $strERID ?>"  value="Checkin" id="Checkin" name="Checkin" onclick="Checkin(<?= $strERID ?>);">Checkin</button>
                                                                            </div>
                                                                            <?php
                                                                        } else if ($LogoutTime == "00:00:00" && $LoginTime != "00:00:00" && $currtime > $checktimewith) {
                                                                            ?>
                                                                            <div align="center"  id="CheckOut<?= $strERID ?>"> <a class=" btn btn-xs btn-primary  result_message1<?= $strERID ?>" value="CheckOut" id="CheckOut" name="CheckOut" onclick="CheckOut(<?= $strERID ?>);" >CheckOut</a>
                                                                            </div>

                                                                            <?php
                                                                        } else {
                                                                            $timesql = "Select TIMEDIFF(LogoutTime,LoginTime) as Hour1 from tblEmployeesRecords Where ERID = $strERID";
                                                                            $RS = $DB->query($timesql);
                                                                            if ($RS->num_rows > 0) {
                                                                                $row = $RS->fetch_assoc();
                                                                                $x = $row['Hour1'];
                                                                                echo ($currtime > $checktimewith) ? "Completed " . $x . " Hours." : '';
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php } else { ?>
                                                        <tr>
                                                            <td colspan="3">No Employee Found...</td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <!-- Reconcillation Start -->

                        <div class="panel">
                            <div class="panel">
                                <div class="panel-body">

                                    <div class="example-box-wrapper">

                                        <div id="normal-tabs-1">
                                            <span class="form_result">&nbsp; <br>
                                            </span>


                                            <h4 class="title-hero"><center><?php echo 'Reconcillation Report For ' . date('d/m/Y'); ?> | NailSpa</center></h4>
                                            <?php
                                            $LoginTime = isset($attendance_data[0]['LoginTime']) ? $attendance_data[0]['LoginTime'] : '00:00:00';
                                            $LogoutTime = isset($attendance_data[0]['LogoutTime']) ? $attendance_data[0]['LogoutTime'] : '00:00:00';
                                            if ($LoginTime != '00:00:00' && $LogoutTime != '00:00:00') {
                                                ?>
                                                <a class="btn btn-alt btn-hover btn-warning" href="#" id="reconcillation_confirm" onclick="proceed_reconcillation();">Reconcille</a> &nbsp;&nbsp;&nbsp;
                                            <?php } ?>

                                            <?php /* <button onclick="download_csv()">Download CSV</button> */ ?>


                                            <?php
                                            if (isset($strEmpID) && $strEmpID != '') {
                                                /*
                                                 * GET All Child
                                                 */
                                                $all_emp_ids[$strEmpID] = $strEmpID;
                                                $emp_child = select("*", "emp_childs", "parent_emp_id='" . $strEmpID . "' AND status=1");
                                                if (isset($emp_child) && is_array($emp_child) && count($emp_child) > 0) {
                                                    foreach ($emp_child as $ckey => $cvalue) {
                                                        $all_emp_ids[$cvalue['child_emp_id']] = $cvalue['child_emp_id'];
                                                    }
                                                }
                                            }

                                            if (isset($all_emp_ids) && is_array($all_emp_ids) && count($all_emp_ids) > 0) {

                                                $all_offers = select("OfferID,OfferName,OfferCode,Type,TypeAmount", "tblOffers", "OfferID >0");

                                                if (isset($all_offers) && is_array($all_offers) && count($all_offers) > 0) {
                                                    foreach ($all_offers as $offkey => $offvalue) {
                                                        $all_offers_name[$offvalue['OfferID']] = $offvalue;
                                                    }
                                                }

                                                $final_res = array();
                                                $getfrom = date('Y-m-d');
                                                $getto = date('Y-m-d');

                                                $sqlTempfrom = " and Date(tblInvoiceDetails.OfferDiscountDateTime)>=Date('" . $getfrom . "')";

                                                $sqlTempto = " and Date(tblInvoiceDetails.OfferDiscountDateTime)<=Date('" . $getto . "')";
                                                foreach ($all_emp_ids as $ekey => $evalue) {
                                                    $search_emp[0]['EID'] = $evalue;

                                                    /* $sqldetails = "SELECT tblEmployees.EID,tblInvoiceDetails.CustomerID,tblAppointmentAssignEmployee.AppointmentID,
                                                      tblAppointmentAssignEmployee.Qty,
                                                      tblAppointmentAssignEmployee.ServiceID,
                                                      tblAppointmentAssignEmployee.Commission,
                                                      tblAppointmentAssignEmployee.QtyParam,
                                                      tblInvoiceDetails.OfferDiscountDateTime,tblEmployees.StoreID
                                                      FROM tblEmployees
                                                      join tblAppointmentAssignEmployee on tblEmployees.EID = tblAppointmentAssignEmployee.MECID
                                                      join tblInvoiceDetails on tblAppointmentAssignEmployee.AppointmentID=tblInvoiceDetails.AppointmentId
                                                      where tblAppointmentAssignEmployee.AppointmentID!='NULL'
                                                      and tblInvoiceDetails.OfferDiscountDateTime!='NULL'
                                                      $sqlTempfrom
                                                      $sqlTempto
                                                      $empq and tblEmployees.EID = '" . $search_emp[0]['EID'] . "'
                                                      group by tblAppointmentAssignEmployee.AppointmentID,ServiceID,QtyParam"; */

                                                    $sqldetails = "SELECT tblEmployees.EID,tblInvoiceDetails.CustomerID,tblAppointmentAssignEmployee.AppointmentID,
						tblAppointmentAssignEmployee.Qty,
						tblAppointmentAssignEmployee.ServiceID,
						tblAppointmentAssignEmployee.Commission, 
						tblAppointmentAssignEmployee.QtyParam,
						tblInvoiceDetails.OfferDiscountDateTime,tblEmployees.StoreID 
						FROM tblEmployees 
                                                join tblAppointmentAssignEmployee on tblEmployees.EID = tblAppointmentAssignEmployee.MECID 
						join tblInvoiceDetails on tblAppointmentAssignEmployee.AppointmentID=tblInvoiceDetails.AppointmentId 
                                                join tblAppointments on tblAppointments.AppointmentID=tblInvoiceDetails.AppointmentId 
						where tblAppointmentAssignEmployee.AppointmentID!='NULL' 
						and tblInvoiceDetails.OfferDiscountDateTime!='NULL' 
						$sqlTempfrom 
						$sqlTempto 
                                                $empq and tblEmployees.EID = '" . $search_emp[0]['EID'] . "' AND tblAppointments.Status=2 AND tblAppointments.IsDeleted=0
                                                group by tblAppointmentAssignEmployee.AppointmentID,ServiceID,QtyParam";



                                                    $RSdetails = $DB->query($sqldetails);

                                                    while ($rowdetails = $RSdetails->fetch_assoc()) {
                                                        $invoice_data[$rowdetails['EID']][] = $rowdetails;
                                                    }


                                                    if (isset($invoice_data) && is_array($invoice_data) && count($invoice_data) > 0) {
                                                        $service_ids = array();
                                                        $store_ids = array();
                                                        $appointment_ids = array();
                                                        $customer_ids = array();

                                                        foreach ($invoice_data as $detailkey => $detailvalue) {
                                                            foreach ($detailvalue as $invkey => $invvalue) {
                                                                $service_ids[$invvalue["ServiceID"]] = $invvalue["ServiceID"];
                                                                $store_ids[$invvalue["StoreID"]] = $invvalue["StoreID"];
                                                                $appointment_ids[$invvalue["AppointmentID"]] = $invvalue["AppointmentID"];
                                                                $customer_ids[$invvalue["CustomerID"]] = $invvalue["CustomerID"];
                                                            }
                                                        }


                                                        /*
                                                         * get service name
                                                         */
                                                        if (isset($service_ids) && is_array($service_ids) && count($service_ids) > 0) {
                                                            $service_in_ids = implode(",", $service_ids);
                                                            $service_q = "SELECT ServiceID,ServiceName FROM tblServices WHERE ServiceID IN(" . $service_in_ids . ")";
                                                            $service_exe = $DB->query($service_q);
                                                            while ($servdetails = $service_exe->fetch_assoc()) {
                                                                $all_services[] = $servdetails;
                                                            }

                                                            if (isset($all_services) && is_array($all_services) && count($all_services) > 0) {
                                                                foreach ($all_services as $serkey => $servalue) {
                                                                    $sep[$servalue['ServiceID']] = $servalue['ServiceName'];
                                                                }
                                                            }
                                                        }


                                                        /*
                                                         * get store name
                                                         */
                                                        if (isset($store_ids) && is_array($store_ids) && count($store_ids) > 0) {
                                                            $store_in_ids = implode(",", $store_ids);
                                                            $store_q = "SELECT StoreID,StoreName FROM tblStores WHERE StoreID IN(" . $store_in_ids . ")";
                                                            $store_exe = $DB->query($store_q);
                                                            while ($storedetails = $store_exe->fetch_assoc()) {
                                                                $all_store[] = $storedetails;
                                                            }
                                                            if (isset($all_store) && is_array($all_store) && count($all_store) > 0) {
                                                                foreach ($all_store as $stokey => $stovalue) {
                                                                    $stpp[$stovalue['StoreID']] = $stovalue['StoreName'];
                                                                }
                                                            }
                                                        }

                                                        /*
                                                         * get invoice number
                                                         */
                                                        if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                                                            $apt_in_ids = implode(",", $appointment_ids);
                                                            $apt_q = "SELECT InvoiceID,AppointmentID FROM tblInvoice WHERE AppointmentID IN(" . $apt_in_ids . ")";
                                                            $apt_exe = $DB->query($apt_q);
                                                            while ($aptdetails = $apt_exe->fetch_assoc()) {
                                                                $all_apt[] = $aptdetails;
                                                            }

                                                            if (isset($all_apt) && is_array($all_apt) && count($all_apt) > 0) {
                                                                foreach ($all_apt as $aptkey => $aptvalue) {
                                                                    $sql_invoice_number[$aptvalue['AppointmentID']] = $aptvalue['InvoiceID'];
                                                                }
                                                            }
                                                        }

                                                        /*
                                                         * get AppointmentDate
                                                         */
                                                        if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                                                            $apt_in_ids = implode(",", $appointment_ids);
                                                            $aptdate_q = "SELECT AppointmentDate,AppointmentID FROM tblAppointments WHERE AppointmentID IN(" . $apt_in_ids . ")";
                                                            $aptdate_exe = $DB->query($aptdate_q);
                                                            while ($aptdatedetails = $aptdate_exe->fetch_assoc()) {
                                                                $alldate_apt[] = $aptdatedetails;
                                                            }
                                                            if (isset($alldate_apt) && is_array($alldate_apt) && count($alldate_apt) > 0) {
                                                                foreach ($alldate_apt as $aptdkey => $aptdvalue) {
                                                                    $sql_apt_date[$aptdvalue['AppointmentID']] = $aptdvalue['AppointmentDate'];
                                                                }
                                                            }
                                                        }

                                                        /*
                                                         * get customer
                                                         */
                                                        if (isset($customer_ids) && is_array($customer_ids) && count($customer_ids) > 0) {
                                                            $cust_in_ids = implode(",", $customer_ids);

                                                            $cust_q = "SELECT CustomerID,CustomerFullName FROM tblCustomers WHERE CustomerID IN(" . $cust_in_ids . ")";
                                                            $cust_exe = $DB->query($cust_q);
                                                            while ($custdetails = $cust_exe->fetch_assoc()) {
                                                                $all_cust[] = $custdetails;
                                                            }
                                                            if (isset($all_cust) && is_array($all_cust) && count($all_cust) > 0) {
                                                                foreach ($all_cust as $custkey => $custvalue) {
                                                                    $sql_customer[$custvalue['CustomerID']] = $custvalue['CustomerFullName'];
                                                                }
                                                            }
                                                        }

                                                        /*
                                                         * get service amt
                                                         */

                                                        if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                                                            $apt_in_ids = implode(",", $appointment_ids);
                                                            $aptdate_q = "SELECT ServiceAmount,AppointmentID,ServiceID FROM tblAppointmentsDetailsInvoice WHERE AppointmentID IN(" . $apt_in_ids . ") AND PackageService=0";
                                                            $aptdate_exe = $DB->query($aptdate_q);
                                                            while ($aptdatedetails = $aptdate_exe->fetch_assoc()) {
                                                                $all_apt_service_amt[] = $aptdatedetails;
                                                            }
                                                            if (isset($all_apt_service_amt) && is_array($all_apt_service_amt) && count($all_apt_service_amt) > 0) {
                                                                foreach ($all_apt_service_amt as $servdkey => $servvalue) {
                                                                    $all_ser_amt[$servvalue['AppointmentID']][$servvalue['ServiceID']] = $servvalue['ServiceAmount'];
                                                                }
                                                            }
                                                        }


                                                        /*
                                                         * get discount 
                                                         */
                                                        if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                                                            $apt_in_ids = implode(",", $appointment_ids);
                                                            $aptdate_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ")";
                                                            $aptdate_exe = $DB->query($aptdate_q);
                                                            while ($disdetails = $aptdate_exe->fetch_assoc()) {
                                                                $all_apt_dis_amt[] = $disdetails;
                                                            }

                                                            /*
                                                             * Get Appointment Offer AMount
                                                             */
                                                            $apt_in_ids = implode(",", $appointment_ids);
                                                            $aptoffer_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID,OfferID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ") AND OfferAmount > 0";
                                                            $aptoffer_exe = $DB->query($aptoffer_q);
                                                            while ($offeretails = $aptoffer_exe->fetch_assoc()) {
                                                                $apt_service_count[$offeretails['AppointmentID']][$offeretails['ServiceID']] = $offeretails['OfferAmount'];
                                                            }

                                                            $apt_in_ids = implode(",", $appointment_ids);
                                                            $aptoffer_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID,OfferID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ") AND OfferAmount > 0";
                                                            $aptoffer_exe = $DB->query($aptoffer_q);
                                                            while ($offeretails = $aptoffer_exe->fetch_assoc()) {
                                                                /*
                                                                 * if offer is amount divide offer by service count
                                                                 */
                                                                if (isset($all_offers_name[$offeretails['OfferID']]) && $all_offers_name[$offeretails['OfferID']]['Type'] == 1 && $offeretails['OfferID'] > 0) {
                                                                    $offer_service_cnt = isset($apt_service_count[$offeretails['AppointmentID']]) ? count($apt_service_count[$offeretails['AppointmentID']]) : 1;
                                                                    $all_apt_off_dis[$offeretails['AppointmentID']][$offeretails['ServiceID']] = $offeretails['OfferAmount'] / $offer_service_cnt;
                                                                } else {
                                                                    $all_apt_off_dis[$offeretails['AppointmentID']][$offeretails['ServiceID']] = $offeretails['OfferAmount'];
                                                                }
                                                            }


                                                            if (isset($all_apt_dis_amt) && is_array($all_apt_dis_amt) && count($all_apt_dis_amt) > 0) {
                                                                foreach ($all_apt_dis_amt as $diskey => $disvalue) {
                                                                    /*
                                                                     * Get Appointment service count
                                                                     */
                                                                    $extra_offer_amount = 0;

                                                                    if (isset($all_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']]) && $all_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']] > 0) {
                                                                        $extra_offer_amount = $all_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']];
                                                                    }
                                                                    //echo '<br>extra_offer_amount=' . $extra_offer_amount;
                                                                    //echo '<br>member_amount=' . $disvalue['MemberShipAmount'];
                                                                    $all_dis_amt[$disvalue['AppointmentID']][$disvalue['ServiceID']]['MemberShipAmount'] = $disvalue['MemberShipAmount'] + round($extra_offer_amount, 2);
                                                                    $all_apt_mem_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']] = $disvalue['MemberShipAmount'];
                                                                    //$all_dis_amt[$disvalue['AppointmentID']][$disvalue['ServiceID']]['OfferAmount'] = $disvalue['OfferAmount'];
                                                                }
                                                            }
                                                        }
                                                    }
                                                    ?>



                                                    <?php
                                                    if (isset($search_emp) && is_array($search_emp) && count($search_emp) > 0) {
                                                        $emp_data = select("*", "tblEmployees", "EID='" . $search_emp[0]['EID'] . "'");
                                                        foreach ($search_emp as $key => $row) {
                                                            $strEID = $emp_data[0]["EID"];
                                                            $strEmployeeName = $emp_data[0]["EmployeeName"];
                                                            $strEmployeeEmailID = $emp_data[0]["EmployeeEmailID"];
                                                            $strEmpPercentage = $emp_data[0]["EmpPercentage"];
                                                            $strEmployeeMobileNo = $emp_data[0]["EmployeeMobileNo"];
                                                            $strEmployeeCode = $emp_data[0]["EmployeeCode"];
                                                            $strTarget = $emp_data[0]["Target"];

                                                            $TotalAfterDivideSale = '';
                                                            $strTotalAmount = '';
                                                            $TotalFinalDiscount = '';
                                                            $TotalUltimateSale = '';
                                                            $ComFinal = '';

                                                            /*
                                                             * Get Employee Target
                                                             */
                                                            $total_target = 0;
                                                            $Month = date('m');
                                                            $MonthSpell = getMonthSpelling($Month);
                                                            $target_data = select("*", "tblEmployeeTarget", "EmployeeCode='" . $strEmployeeCode . "'"
                                                                    . " AND Year='" . date('Y') . "' AND TargetForMonth='" . $MonthSpell . "'");
                                                            if (isset($target_data) && is_array($target_data) && count($target_data) > 0) {
                                                                $total_target = $target_data[0]['BaseTarget'];
                                                            } else {
                                                                $total_target = $strTarget;
                                                            }

                                                            /*
                                                             * Get Completed Target
                                                             */
                                                            $getfrom = date('Y-m-01');
                                                            $getto = date('Y-m-t');
                                                            $sqlTempfrom = " and Date(tblInvoiceDetails.OfferDiscountDateTime)>='" . $getfrom . "'";
                                                            $sqlTempto = " and Date(tblInvoiceDetails.OfferDiscountDateTime)<='" . $getto . "'";
                                                            $search_emp[0]['EID'] = $strEID;

                                                            $sqldetails = "SELECT tblEmployees.EID,tblInvoiceDetails.CustomerID,tblAppointmentAssignEmployee.AppointmentID,
						tblAppointmentAssignEmployee.Qty,
						tblAppointmentAssignEmployee.ServiceID,
						tblAppointmentAssignEmployee.Commission, 
						tblAppointmentAssignEmployee.QtyParam,
						tblInvoiceDetails.OfferDiscountDateTime,tblEmployees.StoreID 
						FROM tblEmployees 
                                                join tblAppointmentAssignEmployee on tblEmployees.EID = tblAppointmentAssignEmployee.MECID 
						join tblInvoiceDetails on tblAppointmentAssignEmployee.AppointmentID=tblInvoiceDetails.AppointmentId 
                                                join tblAppointments on tblAppointments.AppointmentID=tblInvoiceDetails.AppointmentId
						where tblAppointmentAssignEmployee.AppointmentID!='NULL' 
						and tblInvoiceDetails.OfferDiscountDateTime!='NULL' 
						$sqlTempfrom 
						$sqlTempto 
                                                $empq and tblEmployees.EID = '" . $search_emp[0]['EID'] . "' AND tblAppointments.Status=2 AND tblAppointments.IsDeleted=0
                                                group by tblAppointmentAssignEmployee.AppointmentID,ServiceID,QtyParam";


                                                            $RSdetails = $DB->query($sqldetails);

                                                            while ($rowdetails = $RSdetails->fetch_assoc()) {
                                                                $target_invoice_data[$rowdetails['EID']][] = $rowdetails;
                                                            }

                                                            if (isset($target_invoice_data) && is_array($target_invoice_data) && count($target_invoice_data) > 0) {
                                                                $appointment_ids = array();

                                                                foreach ($target_invoice_data as $detailkey => $detailvalue) {
                                                                    foreach ($detailvalue as $invkey => $invvalue) {
                                                                        $tappointment_ids[$invvalue["AppointmentID"]] = $invvalue["AppointmentID"];
                                                                    }
                                                                }
                                                                /*
                                                                 * get service amt
                                                                 */

                                                                if (isset($tappointment_ids) && is_array($tappointment_ids) && count($tappointment_ids) > 0) {
                                                                    $apt_in_ids = implode(",", $tappointment_ids);
                                                                    $aptdate_q = "SELECT ServiceAmount,AppointmentID,ServiceID,service_amount_deduct_dis,super_gv_discount,qty,package_discount FROM tblAppointmentsDetailsInvoice WHERE AppointmentID IN(" . $apt_in_ids . ") AND PackageService=0";
                                                                    $aptdate_exe = $DB->query($aptdate_q);
                                                                    while ($aptdatedetails = $aptdate_exe->fetch_assoc()) {
                                                                        $tall_apt_service_amt[] = $aptdatedetails;
                                                                    }
                                                                    
                                                                    if (isset($tall_apt_service_amt) && is_array($tall_apt_service_amt) && count($tall_apt_service_amt) > 0) {
                                                                        foreach ($tall_apt_service_amt as $servdkey => $servvalue) {
                                                                            $tall_ser_amt[$servvalue['AppointmentID']][$servvalue['ServiceID']] = round(($servvalue['service_amount_deduct_dis'] / $servvalue['qty']),2);
                                                                            $super_gv_deduct[$servvalue['AppointmentID']][$servvalue['ServiceID']] = round(($servvalue['super_gv_discount'] / $servvalue['qty']),2);
                                                                            //$tall_ser_amt[$servvalue['AppointmentID']][$servvalue['ServiceID']] = $servvalue['service_amount_deduct_dis'];
                                                                            //$super_gv_deduct[$servvalue['AppointmentID']][$servvalue['ServiceID']] = $servvalue['super_gv_discount'];
                                                                            $package_discount_deduct[$servvalue['AppointmentID']][$servvalue['ServiceID']] = round(($servvalue['package_discount'] / $servvalue['qty']),2);
                                                                        }
                                                                    }
                                                                }
                                                                //echo '<pre>';print_r($tall_ser_amt);exit;


                                                                /*
                                                                 * get discount 
                                                                 */
                                                                if (isset($tappointment_ids) && is_array($tappointment_ids) && count($tappointment_ids) > 0) {
                                                                    $apt_in_ids = implode(",", $tappointment_ids);
                                                                    $aptdate_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ")";
                                                                    $aptdate_exe = $DB->query($aptdate_q);
                                                                    while ($disdetails = $aptdate_exe->fetch_assoc()) {
                                                                        $tall_apt_dis_amt[] = $disdetails;
                                                                    }

                                                                    /*
                                                                     * Get Appointment Offer AMount
                                                                     */
                                                                    $apt_in_ids = implode(",", $tappointment_ids);
                                                                    $aptoffer_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID,OfferID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ") AND OfferAmount > 0";
                                                                    $aptoffer_exe = $DB->query($aptoffer_q);
                                                                    while ($offeretails = $aptoffer_exe->fetch_assoc()) {
                                                                        $tapt_service_count[$offeretails['AppointmentID']][$offeretails['ServiceID']] = $offeretails['OfferAmount'];
                                                                    }

                                                                    $aptoffer_q = "SELECT OfferAmount,MemberShipAmount,AppointmentID,ServiceID,OfferID FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $apt_in_ids . ") AND OfferAmount > 0";
                                                                    $aptoffer_exe = $DB->query($aptoffer_q);
                                                                    while ($offeretails = $aptoffer_exe->fetch_assoc()) {
                                                                        //$tall_apt_off_dis[$offeretails['AppointmentID']][$offeretails['ServiceID']] = $offeretails['OfferAmount'];
                                                                        if (isset($all_offers_name[$offeretails['OfferID']]) && $all_offers_name[$offeretails['OfferID']]['Type'] == 1 && $offeretails['OfferID'] > 0) {
                                                                            $toffer_service_cnt = isset($tapt_service_count[$offeretails['AppointmentID']]) ? count($tapt_service_count[$offeretails['AppointmentID']]) : 1;
                                                                            $tall_apt_off_dis[$offeretails['AppointmentID']][$offeretails['ServiceID']] = $offeretails['OfferAmount'] / $toffer_service_cnt;
                                                                        } else {
                                                                            $tall_apt_off_dis[$offeretails['AppointmentID']][$offeretails['ServiceID']] = $offeretails['OfferAmount'];
                                                                        }
                                                                    }



                                                                    if (isset($tall_apt_dis_amt) && is_array($tall_apt_dis_amt) && count($tall_apt_dis_amt) > 0) {
                                                                        foreach ($tall_apt_dis_amt as $diskey => $disvalue) {
                                                                            /*
                                                                             * Get Appointment service count
                                                                             */
                                                                            $extra_offer_amount = 0;

                                                                            if (isset($tall_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']]) && $tall_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']] > 0) {
                                                                                $extra_offer_amount = $tall_apt_off_dis[$disvalue['AppointmentID']][$disvalue['ServiceID']];
                                                                            }
                                                                            $tall_dis_amt[$disvalue['AppointmentID']][$disvalue['ServiceID']]['MemberShipAmount'] = $disvalue['MemberShipAmount'] + round($extra_offer_amount, 2);
                                                                        }
                                                                    }
                                                                }
                                                            }


                                                            $strEID = $search_emp[0]['EID'];
                                                            $emp_data[0] = isset($emp_id_data[$strEID]) ? $emp_id_data[$strEID] : array();

                                                            $tTotalAfterDivideSale = '';
                                                            $tTotalUltimateSale = 0;
                                                            $grand_total_tax = 0;
                                                            $grand_total_sale_amount = 0;

                                                            if (isset($target_invoice_data[$strEID]) && is_array($target_invoice_data[$strEID]) && count($target_invoice_data[$strEID]) > 0) {

                                                                $rowstrTotalAmount = 0;
                                                                $row_invoice = 0;
                                                                $rowComFinal = 0;
                                                                foreach ($target_invoice_data[$strEID] as $rowkey => $rowdetails) {

                                                                    $strEID = $search_emp[0]['EID'];
                                                                    $strAID = $rowdetails["AppointmentID"];
                                                                    $strSID = $rowdetails["ServiceID"];
                                                                    $rowstrAmount = isset($tall_ser_amt[$strAID][$strSID]) ? $tall_ser_amt[$strAID][$strSID] : '';
                                                                    $super_gv_deduct_amount = isset($super_gv_deduct[$strAID][$strSID]) ? $super_gv_deduct[$strAID][$strSID] : '';
                                                                    $rowstrTotalAmount += $rowstrAmount;
                                                                    $qty = $rowdetails["Qty"];

                                                                    $tstrCommission = $rowdetails["Commission"];
                                                                    if ($tstrCommission == "1") {
                                                                        $tAfterDivideSale = $rowstrAmount;
                                                                    } elseif ($tstrCommission == "2") {
                                                                        $tAfterDivideSale = ($rowstrAmount / 2);
                                                                    }

                                                                    $trowdiscount = isset($tall_dis_amt[$strAID][$strSID]) ? $tall_dis_amt[$strAID][$strSID] : array();
                                                                    if (isset($trowdiscount) && is_array($trowdiscount) && count($trowdiscount) > 0) {
                                                                        $strOfferAmount = $trowdiscount["OfferAmount"];
                                                                        $strDiscountAmount = $trowdiscount["MemberShipAmount"];
                                                                        if ($strOfferAmount > 0) {
                                                                            $tFinalDAmount = $tstrOfferAmount;
                                                                        } elseif ($strDiscountAmount > 0) {
                                                                            $tFinalDAmount = $strDiscountAmount;
                                                                        }
                                                                    } else {
                                                                        $tFinalDAmount = "0";
                                                                    }

                                                                    $FinalDiscount = $tFinalDAmount / $qty;

                                                                    //$trowUltimateSale = $tAfterDivideSale - $FinalDiscount;
                                                                    $trowUltimateSale = $rowstrAmount;
                                                                    $error_data[] = $trowUltimateSale;
                                                                    $tTotalUltimateSale += $trowUltimateSale; //Total of discounted amount
                                                                }
                                                            }

                                                            $com_perc = 0;
                                                            if (isset($total_target) && $total_target != '' && $total_target != 0) {
                                                                if ($tTotalUltimateSale > $total_target) {
                                                                    $com_perc = 100;
                                                                } else {
                                                                    $com_perc = ($tTotalUltimateSale * 100) / $total_target;
                                                                }
                                                            }
                                                            ?>
                                                            <div id="printdata">
                                                                <br><br>
                                                                <?php
                                                                $da_target = isset($total_target) ? $total_target : 0;
                                                                $da_completed = isset($tTotalUltimateSale) ? $tTotalUltimateSale : 0;
                                                                $remaining = $da_target - $da_completed;

                                                                if ($remaining > 0) {
                                                                    /*
                                                                     * Get Number Of Remaining Days
                                                                     */
                                                                    $timestamp = strtotime(date('Y-m-d'));
                                                                    $daysRemaining = (int) date('t', $timestamp) - (int) date('j', $timestamp);
                                                                    $daysRemaining = $daysRemaining + 1;
                                                                    $daily_target = $remaining / $daysRemaining;
                                                                }
                                                                ?>

                                                                <?php
                                                                if ($_SERVER['REMOTE_ADDR'] == "111.119.219.70") {
                                                                    //echo $sqldetails;
                                                                } else {
                                                                    
                                                                }

                                                                $counter = 0;
                                                                $strSID = "";
                                                                $qty = "";
                                                                $strSAmount = "";
                                                                $strAmount = "";
                                                                $strCommission = "";
                                                                $FinalDAmount = '';
                                                                $FinalDiscount = '';
                                                                $FinalOtherDiscount = 0;
                                                                $TotalOtherDiscount = 0;
                                                                $UltimateSale = '';
                                                                $AfterDivideSale = '';
                                                                $CommssionFinal = "";
                                                                //echo("Error description: " . mysqli_error($DB));

//echo '<pre>';print_r($invoice_data);exit;
                                                                if (isset($invoice_data[$strEID]) && is_array($invoice_data[$strEID]) && count($invoice_data[$strEID]) > 0) {
                                                                    $final_res = array();
                                                                    $rowstrTotalAmount = 0;
                                                                    $row_invoice = 0;
                                                                    $apt_id = 0;
                                                                    $rowComFinal = 0;
                                                                    $total_gv_deduct = 0;
                                                                    $total_package_dis_deduct = 0;
                                                                    foreach ($invoice_data[$strEID] as $rowkey => $rowdetails) {
                                                                        $strAID = $rowdetails["AppointmentID"];
                                                                        $strSID = $rowdetails["ServiceID"];
                                                                        $rowstrAmount = isset($tall_ser_amt[$strAID][$strSID]) ? $tall_ser_amt[$strAID][$strSID] : '';
                                                                        $super_gv_deduct_amount = isset($super_gv_deduct[$strAID][$strSID]) ? $super_gv_deduct[$strAID][$strSID] : '';

                                                                        $rowstrTotalAmount += $rowstrAmount;
                                                                        $qty = $rowdetails["Qty"];

                                                                        if ($apt_id != $rowdetails['AppointmentID']) {
                                                                            $row_invoice = $row_invoice + 1;
                                                                        }
                                                                        $apt_id = $rowdetails['AppointmentID'];

                                                                        $strEmpPercentage = $emp_data[0]["EmpPercentage"];

                                                                        $strCommission = $rowdetails["Commission"];
                                                                        if ($strCommission == "1") {
                                                                            $AfterDivideSale = $rowstrAmount;
                                                                        } elseif ($strCommission == "2") {
                                                                            $AfterDivideSale = ($rowstrAmount / 2);
                                                                        }

                                                                        $rowdiscount = isset($all_dis_amt[$strAID][$strSID]) ? $all_dis_amt[$strAID][$strSID] : array();
                                                                        if (isset($rowdiscount) && is_array($rowdiscount) && count($rowdiscount) > 0) {
                                                                            $strOfferAmount = $rowdiscount["OfferAmount"];
                                                                            $strDiscountAmount = $rowdiscount["MemberShipAmount"];

                                                                            if ($strOfferAmount > 0) {
                                                                                $FinalDAmount = $strOfferAmount;
                                                                            } elseif ($strDiscountAmount > 0) {
                                                                                $FinalDAmount = $strDiscountAmount;
                                                                            }
                                                                        } else {
                                                                            $FinalDAmount = "0";
                                                                        }

                                                                        $FinalDiscount = $FinalDAmount / $qty;

                                                                        $rowUltimateSale = $AfterDivideSale - $FinalDiscount;
                                                                        if ($strCommission == "1") {
                                                                            $rowCommssionFinal = ($rowUltimateSale / 100) * $strEmpPercentage;
                                                                        } elseif ($strCommission == "2") {
                                                                            $rowCommssionFinal = ($rowUltimateSale / 200) * $strEmpPercentage;
                                                                        }
                                                                        $rowComFinal += $rowCommssionFinal;

                                                                        $final_res[$strEID][$rowdetails['AppointmentID'] . '-' . $rowdetails["ServiceID"]] = $rowdetails;

                                                                        if (isset($emp_service_qty[$rowdetails['AppointmentID']][$rowdetails["ServiceID"]][$strEID])) {
                                                                            $emp_service_qty[$rowdetails['AppointmentID']][$rowdetails["ServiceID"]][$strEID] += 1;
                                                                        } else {
                                                                            $emp_service_qty[$rowdetails['AppointmentID']][$rowdetails["ServiceID"]][$strEID] = 1;
                                                                        }

                                                                        $apt_service_qty[$rowdetails['AppointmentID']][$rowdetails["ServiceID"]] = $rowdetails['Qty'];
                                                                    }
                                                                }

                                                                $DailyUltimateSale = 0;

                                                                foreach ($final_res[$strEID] as $rowkey => $rowdetails) {
                                                                    $strEIDa = $rowdetails["EID"];
                                                                    $strAID = $rowdetails["AppointmentID"];
                                                                    $strSID = $rowdetails["ServiceID"];
                                                                    $CustomerID = $rowdetails['CustomerID'];
                                                                    $qty = $rowdetails["Qty"];

                                                                    $service_type = '';

                                                                    $emp_ser_count = isset($emp_service_qty[$strAID][$strSID][$strEID]) ? $emp_service_qty[$strAID][$strSID][$strEID] : 1;
                                                                    $apt_ser_count = isset($apt_service_qty[$strAID][$strSID]) ? $apt_service_qty[$strAID][$strSID] : 1;

                                                                    $DaliyAmount = isset($tall_ser_amt[$strAID][$strSID]) ? $tall_ser_amt[$strAID][$strSID] * $emp_ser_count : '0';

                                                                    $strCommission = $rowdetails["Commission"];

                                                                    if ($strCommission == "2") {
                                                                        $DaliyAmount = $DaliyAmount / 2;
                                                                    }
                                                                    $DailyUltimateSale += $DaliyAmount; //Total of discounted amount
                                                                }
                                                                ?>
                                                                <div class="col-md-3 col-xs-12 margin-bot-zero no-pad btn" style="border-color: #FC8213;"><b>Today's Sale : </b><?php echo isset($DailyUltimateSale) ? $DailyUltimateSale : 0; ?></div>
                                                                <div class="col-md-3 col-xs-12 margin-bot-zero no-pad btn" style="border-color: #FC8213;"><b>Target Amount : </b><?php echo isset($total_target) ? $total_target : 0; ?></div>
                                                                <div class="col-md-3 col-xs-12 margin-bot-zero no-pad btn" style="border-color: #dc1156;"><b>Completed Target : </b><?php echo $tTotalUltimateSale . ' (' . round($com_perc, 2) . '%)'; ?></div>
                                                                <div class="col-md-3 col-xs-12 margin-bot-zero no-pad btn" style="border-color: #FC8213;"><b>Daily Target : </b><?php echo isset($daily_target) ? round($daily_target, 2) : 0; ?></div>
                                                                <br><br>
                                                                <div class="col-md-6 col-xs-12 margin-bot-zero no-pad"><b>Name :</b><?php echo $strEmployeeName; ?></div>
                                                                <div class="col-md-6 col-xs-12 margin-bot-zero no-pad"><b>Email :</b><?php echo $strEmployeeEmailID; ?></div>
                                                                <div class="col-md-6 col-xs-12 margin-bot-zero no-pad"><b>Commission Percentage :</b><?php echo $strEmpPercentage; ?></div>
                                                                <div class="col-md-6 col-xs-12 margin-bot-zero no-pad"><b>Mobile no. :</b><?php echo $strEmployeeMobileNo; ?></div>


                                                                <div style="overflow-x: auto;width:100%;">

                                                                    <table id="printdata" class="table table-striped table-bordered table-responsive no-wrap printdata" cellspacing="0" width="100%">

                                                                        <thead>
                                                                            <tr>
                                                                                <th>Sr. No.</th>
                                                                                <?php /* <th># Invoice No</th>
                                                                                  <th>Store</th> */ ?>
                                                                                <th>Client Name</th>
                                                                                <?php /* <th>Commission Date</th> */ ?>
                                                                                <th>Service(s) Done</th>
                                                                                <th>Service Amount</th>
                                                                                <?php /* <th>Final Amount</th> */ ?>
                                                                                <th>MemberShip Discount</th> 
                                                                                <th>Other Discount</th>
                                                                                <th>Super GV Deduct</th>
                                                                                <th>Package Discount</th>
                                                                                <th>Net Amount</th>
                                                                                <th>Tax Amount</th>
                                                                                <th>Total Sales</th>
                                                                                <th>Service Type</th>
                                                                                <?php /* <th>Commission Amount</th>
                                                                                  <th>Commission Type</th> */ ?>

                                                                            </tr>
                                                                        </thead>

                                                                        <tbody>


                                                                            <?php /* <div class="col-md-12 emp-com">
                                                                              <div class="col-md-4 no-pad">
                                                                              <h3>Total Invoice : </h3> <b><span class="text-danger"><?php echo $row_invoice; ?></span></b>
                                                                              </div>
                                                                              <div class="col-md-4 no-pad">
                                                                              <h3>Total Sales Amount : </h3> <b><span class="text-danger"><?php echo $rowstrTotalAmount; ?></span></b>
                                                                              </div>
                                                                              <div class="col-md-4 no-pad">
                                                                              <h3>Total Commission : </h3> <b><span class="text-danger"><?php echo $rowComFinal; ?></span></b>
                                                                              </div>
                                                                              </div> */ ?>
                                                                            <?php
                                                                            // forea
                                                                            if (isset($final_res) && is_array($final_res) && count($final_res) > 0) {
                                                                                foreach ($final_res[$strEID] as $rowkey => $rowdetails) {

                                                                                    $counter ++;
                                                                                    $strEIDa = $rowdetails["EID"];
                                                                                    $strAID = $rowdetails["AppointmentID"];
                                                                                    $strSID = $rowdetails["ServiceID"];
                                                                                    $CustomerID = $rowdetails['CustomerID'];
                                                                                    $qty = $rowdetails["Qty"];

                                                                                    $service_type = '';
                                                                                    $emp_ser_count = isset($emp_service_qty[$strAID][$strSID][$strEID]) ? $emp_service_qty[$strAID][$strSID][$strEID] : 1;
                                                                                    $apt_ser_count = isset($apt_service_qty[$strAID][$strSID]) ? $apt_service_qty[$strAID][$strSID] : 1;

                                                                                    $strAmount = isset($all_ser_amt[$strAID][$strSID]) ? ($all_ser_amt[$strAID][$strSID] * $qty * $emp_ser_count) / $apt_ser_count : '';
                                                                                    $rowstrAmount = isset($tall_ser_amt[$strAID][$strSID]) ? $tall_ser_amt[$strAID][$strSID] * $emp_ser_count : '';
                                                                                    $super_gv_deduct_amount = isset($super_gv_deduct[$strAID][$strSID]) ? $super_gv_deduct[$strAID][$strSID] * $emp_ser_count : '';
                                                                                    $package_discount_deduct_amount = isset($package_discount_deduct[$strAID][$strSID]) ? $package_discount_deduct[$strAID][$strSID] * $emp_ser_count : '0';
                                                                                    
                                                                                    $total_gv_deduct += $super_gv_deduct_amount;
                                                                                    $total_package_dis_deduct += $package_discount_deduct_amount;
                                                                                    $strSAmount = $rowstrAmount;
                                                                                    $strCommission = $rowdetails["Commission"];
                                                                                    $StoreIDd = $rowdetails["StoreID"];

                                                                                    $strTotalAmount += $strAmount;  //Total of Service sale amount
                                                                                    $servicename = isset($sep[$strSID]) ? $sep[$strSID] : '';

                                                                                    $StoreName = isset($stpp[$StoreIDd]) ? $stpp[$StoreIDd] : '';

                                                                                    $Invoice_Number = isset($sql_invoice_number[$strAID]) ? $sql_invoice_number[$strAID] : '';

                                                                                    $AppointmentDate = isset($sql_apt_date[$strAID]) ? $sql_apt_date[$strAID] : '';


                                                                                    $CustomerFullName = isset($sql_customer[$CustomerID]) ? $sql_customer[$CustomerID] : '';


                                                                                    if ($strCommission == "1") {
                                                                                        $AfterDivideSale = $strSAmount;
                                                                                        $strCommissionType = '<span class="bs-label label-success">Alone</span>';
                                                                                    } elseif ($strCommission == "2") {
                                                                                        //$AfterDivideSale = ($strSAmount / 2);
                                                                                        $AfterDivideSale = ($strSAmount);
                                                                                        $strCommissionType = '<span class="bs-label label-blue-alt">Split</span>';
                                                                                    }
                                                                                    $TotalAfterDivideSale += $AfterDivideSale;  //Total of Final payment
                                                                                    // discount code

                                                                                    $FinalDiscount = isset($all_apt_mem_dis[$strAID][$strSID]) ? ($all_apt_mem_dis[$strAID][$strSID] * $emp_ser_count) / $apt_ser_count : 0;
                                                                                    $FinalOtherDiscount = isset($all_apt_off_dis[$strAID][$strSID]) ? ($all_apt_off_dis[$strAID][$strSID] * $emp_ser_count) / $apt_ser_count : 0;

                                                                                    $TotalFinalDiscount += $FinalDiscount; //Total of discounted amount
                                                                                    $TotalOtherDiscount += $FinalOtherDiscount; //Total of discounted amount
                                                                                    //$UltimateSale = $AfterDivideSale - $FinalDiscount - $FinalOtherDiscount;
                                                                                    $UltimateSale = $strSAmount;

                                                                                    //Calculation for commission
                                                                                    if ($strCommission == "1") {
                                                                                        $CommssionFinal = ($UltimateSale / 100) * $strEmpPercentage;
                                                                                        $service_type = 'Alone';
                                                                                    } elseif ($strCommission == "2") {
                                                                                        $CommssionFinal = ($UltimateSale / 200) * $strEmpPercentage;
                                                                                        $UltimateSale = $UltimateSale / 2;
                                                                                        $FinalDiscount = $FinalDiscount / 2;
                                                                                        $FinalOtherDiscount = $FinalOtherDiscount / 2;
                                                                                        $super_gv_deduct_amount = $super_gv_deduct_amount / 2;
                                                                                        $strAmount = $strAmount / 2;
                                                                                        $service_type = 'Split';
                                                                                    }
                                                                                    $ComFinal += $CommssionFinal; //Total of Commission

                                                                                    $TotalUltimateSale += $UltimateSale; //Total of discounted amount
                                                                                    $ser_tax_amount = ($UltimateSale * 0.18 );
                                                                                    
                                                                                    $total_sale_tax = round($ser_tax_amount) + $UltimateSale;

                                                                                    if($package_discount_deduct_amount > 0){
                                                                                        $ser_tax_amount = 0;
                                                                                        $total_sale_tax = 0;
                                                                                    }
                                                                                    $grand_total_tax += $ser_tax_amount;
                                                                                    $grand_total_sale_amount += $total_sale_tax;
                                                                                    ?>
                                                                                    <tr id="my_data_tr_<?= $counter ?>">
                                                                                        <td><?= $counter; ?>
                                                                                            <?php
                                                                                            $row_existq = "SELECT * FROM emp_reconcillation WHERE emp_id='" . $strEID . "'"
                                                                                                    . " AND appointment_id = '" . $strAID . "' AND status=1 AND marked_status=2 AND DATE_FORMAT(created_date,'%Y-%m-%d') = '" . date('Y-m-d') . "'";
                                                                                            $row_existq_exe = $DB->query($row_existq);
                                                                                            if ($row_existq_exe->num_rows == 0 && count($invoice_data) > 0) {
                                                                                                ?>
                                                                                                <input type="checkbox" class="reconciile_check" value="<?php echo $strEIDa . '/' . $strAID ?>"/>

                                                                                            <?php } ?>

                                                                                        </td>
                                                                                        <?php /* <td>#<?= $Invoice_Number ?></td>
                                                                                          <td><?= $StoreName ?></td> */ ?>
                                                                                        <td><?= $CustomerFullName ?></td>
                                                                                        <?php /* <td><?= date("d/m/Y", strtotime($AppointmentDate)); ?></td> */ ?>
                                                                                        <td><?= $servicename ?></td>
                                                                                        <td>Rs.<?= $UltimateSale ?>/-</td>
                                                                                        <?php /* <td>Rs.<?= $AfterDivideSale ?>/-</td> */ ?>
                                                                                        <td>Rs.<?= $FinalDiscount ?>/-</td>
                                                                                        <td>Rs.<?= $FinalOtherDiscount ?>/-</td>
                                                                                        <td>Rs.<?= $super_gv_deduct_amount ?>/-</td>
                                                                                        <td>Rs.<?= $package_discount_deduct_amount ?>/-</td>
                                                                                        <td>Rs.<?= $strAmount ?>/-</td>
                                                                                        <td>Rs.<?= $ser_tax_amount ?>/-</td>
                                                                                        <td>Rs.<?= $total_sale_tax ?>/-</td>
                                                                                        <td><?php echo $service_type; ?></td>
                                                                                        <?php /* <td>Rs.<?= $CommssionFinal ?>/-</td>	
                                                                                          <td><font color="red"><?= $strCommissionType ?></font></td> */ ?>

                                                                                    </tr>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                                <tr>
                                                                                    <td colspan="3"><b><center>Total commission for selected period : </center></b></td>

                                                                                    <td><b>Rs.<?= $TotalUltimateSale ?>/-</b></td>
                                                                                    <?php /* <td><b>Rs.<?= $TotalAfterDivideSale ?>/-</b></td> */ ?>
                                                                                    <td><b>Rs.<?= $TotalFinalDiscount ?></b></td> 
                                                                                    <td><b>Rs.<?= $TotalOtherDiscount ?>/-</b></td>
                                                                                    <td><b>Rs.<?= $total_gv_deduct ?>/-</b></td>
                                                                                    <td><b>Rs.<?= $total_package_dis_deduct ?>/-</b></td>
                                                                                    <td><b>Rs.<?= $strTotalAmount ?>/-</b></td>
                                                                                    <td><b>Rs.<?= $grand_total_tax ?>/-</b></td>
                                                                                    <td><b>Rs.<?= $grand_total_sale_amount ?>/-</b></td>
                                                                                    <td></td>
                                                                                    <?php /* <td><b>Rs.<?= $ComFinal ?>/-</b></td> */ ?>

                                                                                </tr>
                                                                                <?php
                                                                            } else {
                                                                                ?>				
                                                                                <tr>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td>No Services Done by this Employee in selected time period</td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>


                                                                                </tr>
                                                                            <?php }
                                                                            ?>

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <br><br>
                                                            <?php
                                                        }
                                                    }
                                                }
                                            } else {
                                                echo "No Employees found...";
                                            }

                                            $DB->close();
//echo "End";
//echo (microtime(true));
                                            ?>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Reconcillation Close -->


                    </div>
                    <?php require_once 'incFooter.fya'; ?>
                </div>


                <script>
                            var data = [
<?php
if (isset($csvdata) && is_array($csvdata) && count($csvdata)) {
    foreach ($csvdata as $key => $value) {
        echo "['" . $key . "','" . $value['name'] . "','" . $value['mobile'] . "','" . $value['remark_type'] . "','" . $value['comment'] . "'],";
    }
}
?>
                            ];
                            function download_csv() {
                            var csv = 'Sr,Customer Name,Customer Mobile,Remark Type,Comment\n';
                                    data.forEach(function (row) {
                                    csv += row.join(',');
                                            csv += "\n";
                                    });
                                    var hiddenElement = document.createElement('a');
                                    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
                                    hiddenElement.target = '_blank';
                                    hiddenElement.download = 'non_visiting_customer.csv';
                                    hiddenElement.click();
                            }
                </script>

                <script>
                    $(document).ready(function () {
                    var totalCheckboxes = $('input:checkbox').length;
                            if (totalCheckboxes == 0) {
                    $("#reconcillation_confirm").html('Reconcillation Done').attr('disabled', 'disabled');
                    }
                    });</script>

                <script>

                            function Checkin(a1)
                            {
                            // alert (a1);
                            var ERID = a1;
                                    $.ajax({
                                    type: 'POST',
                                            url: 'AttendanceCheckin.php',
                                            data: {getERID: ERID, type: 1},
                                            success: function (response) {
                                            $('.result_message').html(response);
                                                    location.reload();
                                            }
                                    });
                            }
                    function CheckOut(a2)
                    {
                    var ERID1 = a2;
                            $.ajax({
                            type: 'POST',
                                    url: 'AttendanceCheckout.php',
                                    data: {getERID: ERID1, type: 1},
                                    success: function (response) {
                                    document.getElementById("CheckOut" + ERID1).innerHTML = response;
                                    }

                            });
                    }

                    function geocords() {
                    //alert('cord');
                    try {
                    if (navigator.geolocation){
                    navigator.permissions.query({name: 'geolocation'})
                            .then(function (result) {
                            if (result.state == 'granted') {
                            navigator.geolocation.getCurrentPosition(function (position) {
                            //console.log('Geolocation permissions granted');
                            var latcurr = position.coords.latitude;
                                    var longcurr = position.coords.longitude;
                                    var store_lat = '<?php echo isset($latitude) ? $latitude : ''; ?>';
                                    var store_long = '<?php echo isset($longitude) ? $longitude : ''; ?>';
                                    getDistanceFromLatLonInM(latcurr, longcurr, store_lat, store_long);
                            }, function (error) {
                            document.getElementById('error_msg').innerHTML = 'Location Tracking Failed : ' + error.message;
                            });
                            }
                            else
                            {
                            document.getElementById('error_msg').innerHTML = 'Please Provide Permission';
                            }
                            });
                    }
                    else
                    {
                    document.getElementById('error_msg').innerHTML = 'Geolocation is not supported for this Browser.';
                    }
                    }
                    catch
                    {
                    document.getElementById('error_msg').innerHTML = 'Geolocation is not supported for this Browser.';
                    }

                    }



                    function geoLocation() {
                    //alert('geoLocation');
                    if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showPosition);
                    } else {
                    document.getElementById('error_msg').innerHTML = 'Geolocation is not supported for this Browser.';
                    }

                    }




                    function showPosition(position) {
                    var latcurr = position.coords.latitude;
                            var longcurr = position.coords.longitude;
                            var store_lat = '<?php echo isset($latitude) ? $latitude : ''; ?>';
                            var store_long = '<?php echo isset($longitude) ? $longitude : ''; ?>';
                            getDistanceFromLatLonInM(latcurr, longcurr, store_lat, store_long);
                    }

                    function getDistanceFromLatLonInM(lat1, lon1, lat2, lon2) {
                    console.log('cu_lat=' + lat1 + 'cur_long=' + lon1 + 'storelat=' + lat2 + 'store_long=' + lon2);
                            var R = 6371000; // Radius of the earth in meter
                            var dLat = deg2rad(lat2 - lat1); // deg2rad below
                            var dLon = deg2rad(lon2 - lon1);
                            var a =
                            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                            Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                            Math.sin(dLon / 2) * Math.sin(dLon / 2)
                            ;
                            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                            var d = R * c; // Distance in meter
                            document.getElementById('cur_lat').innerHTML = 'Latitude : ' + lat1;
                            document.getElementById('cur_long').innerHTML = 'Longitude : ' + lon1;
                            document.getElementById('cur_distance').innerHTML = 'Distance in Meter : ' + d.toFixed(2);
                            document.getElementById('error_msg').innerHTML = '';
                            var distance = d.toFixed(2);
                            var emp_store_id = '<?php echo isset($store_id) ? $store_id : ''; ?>';
                            /*
                             * If kochi make distance 300 meter
                             */

                            if (emp_store_id == '109') {
                    var store_dis = 300;
                    } else {
                    var store_dis = 50;
                    }
                    if (distance <= store_dis) {
                    $(".in_out_div").show();
                    } else {
                    $(".in_out_div").hide();
                    }
                    }

                    function deg2rad(deg) {
                    return deg * (Math.PI / 180)
                    }

                    $(document).ready(function () {
                    geocords();
                    }
                    );
                </script>


                </body>

                </html>