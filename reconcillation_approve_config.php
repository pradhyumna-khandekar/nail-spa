<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Reconcillation Approve Report Configuration | Nailspa";
$strDisplayTitle = "Reconcillation Approve Report Configuration";
$strMenuID = "3";
$strMyActionPage = "reconcillation_approve_config.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <!-----------css & js files added for tabs by gandhali 3/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>

        <script type="text/javascript" src="assets/widgets/datepicker/datepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker-demo.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/moment.js"></script>
        <script type="text/javascript" src="assets/widgets/timepicker/timepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="assets/widgets/timepicker/timepicker.css">

        <script type="text/javascript">
            /* Timepicker */

            $(function () {
                "use strict";
                $('.timepicker-example').timepicker();
            });
        </script>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");     ?>
            <!----------commented by gandhali 3/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                        </div>
                        <?php require_once("incHeader.fya"); ?>

                        <?php
                        $DB = Connect();
                       
                        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
                            if (isset($_POST['email_ids']) && is_array($_POST['email_ids']) && count($_POST['email_ids']) > 0) {
                                $email_ids = $_POST['email_ids'];

                                $DeleteQry = "DELETE FROM report_config WHERE report_name='reconcillation_approve'";
                                $DB->query($DeleteQry);

                                if (isset($email_ids) && is_array($email_ids) && count($email_ids) > 0) {
                                    foreach ($email_ids as $emailk => $emailv) {
                                        $insertQry = "INSERT INTO report_config(report_name,report_title,email_id,created_date,created_by)"
                                                . " VALUES('reconcillation_approve','Employee Sales Report','" . $emailv . "','" . date('Y-m-d H:i:s') . "','" . $strAdminID . "')";
                                        $DB->query($insertQry);
                                    }
                                }
                                echo('<div class="alert alert-success alert-dismissible fade in" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
										</button>
										<strong>Saved Successfully.</strong>
										</div>');
                            } else {
                                die('<div class="alert alert-warning alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
								</button>
								<strong>Email ID Cannot be blank.</strong>
							</div>');
                            }
                        }
                        ?>

                        <div class="panel">
                            <div class="panel-body">

                                <div class="panel-body">
                                    <form role="form" enctype="multipart/form-data" class="form-horizontal bordered-row enquiry_form" action="reconcillation_approve_config.php" method="post">

                                        <span class="result_message">&nbsp; <br>
                                        </span>
                                        <br>

                                        <h3 class="title-hero">Edit Configuration</h3>


                                        <?php
                                        /*
                                         * Get Data
                                         */
                                        $report_data = select("*", 'report_config', 'status =1 AND report_name="reconcillation_approve" ');

                                        $counter = 1;
                                        ?>
                                        <div class="example-box-wrapper">
                                            <input type="hidden" name="mailer_id" value="<?php echo isset($mailer_data[0]['id']) ? $mailer_data[0]['id'] : ''; ?>"/>

                                            <div class="config_div">
                                                <?php
                                                if (isset($report_data) && is_array($report_data) && count($report_data) > 0) {
                                                    foreach ($report_data as $repkey => $repvalue) {
                                                        ?>
                                                        <div class="form-group" id="div_count_<?php echo $counter; ?>"><label class="col-sm-3 control-label"><?php echo $repkey == 0 ? 'Email ID' : ''; ?></label>
                                                            <div class="col-sm-4">
                                                                <input autocomplete="off" type="email" name="email_ids[<?php echo $counter; ?>]"  value="<?= $repvalue['email_id'] ?>"  class="form-control" required>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <?php if ($repkey == 0) { ?>
                                                                    <label class="col-sm-4 control-label" style="text-align:left;"><a href="#" class="btn btn-link add_field_button" data-lang="<?php echo $counter; ?>">Add More</a></label>
                                                                <?php } else { ?>
                                                                    <label class="col-sm-4 control-label" style="text-align:left;"><a href="#" class="btn btn-link remove_field" data-lang="<?php echo $counter; ?>">Remove</a></label>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <?php
                                                        $counter++;
                                                    }
                                                } else {
                                                    ?>
                                                    <div class="form-group" id="div_count_<?php echo $counter; ?>"><label class="col-sm-3 control-label">Email ID</label>
                                                        <div class="col-sm-4">
                                                            <input autocomplete="off" type="email" name="email_ids[<?php echo $counter; ?>]" value="" class="form-control" required>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label class="col-sm-4 control-label" style="text-align:left;"><a href="#" class="btn btn-link add_field_button" data-lang="<?php echo $counter; ?>">Add More</a></label>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    $counter++;
                                                }
                                                ?>

                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-6">
                                                    <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Submit</span></button>
                                                </div>
                                            </div>
                                        </div>


                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <script type="text/javascript">
                var DivCount = '<?php echo $counter; ?>';
                $(document).ready(function () {


                    $(".add_field_button").click(function (e) {
                        e.preventDefault();
                        // Max attachment allowed
                        var append_data = '<div class="form-group" id="div_count_' + DivCount + '"><label class="col-sm-3 control-label"></label>' +
                                '<div class="col-sm-4">' +
                                '<input autocomplete="off" type="email" name="email_ids[' + DivCount + ']" value="" class="form-control" required>'
                                + '</div><div class="col-sm-4"><label class="col-sm-4 control-label" style="text-align:left;"><a href="#" class="btn btn-link remove_field" data-lang="' + DivCount + '">Remove</a></label></div></div>';
                        $(".config_div").append(append_data); //add attachment
                        DivCount = parseInt(DivCount) + 1;
                    });

                    $(".config_div").on("click", ".remove_field", function (e) { //user click on to remove attachment
                        var div_count = $(this).attr('data-lang');
                        e.preventDefault();
                        $('#div_count_' + div_count).remove();
                    });
                });
            </script>
            <?php require_once 'incFooter.fya'; ?>
        </div>

    </body>
</html>
