<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
$strPageTitle = "Purchase Super Gift Voucher | NailSpa";
$strDisplayTitle = "Purchase Super Gift Voucher for NailSpa";
$strMenuID = "10";
$strMyTable = "super_gift_voucher";
$strMyTableID = "id";
$strMyField = "AppointmentDate";
$strMyActionPage = "PurchaseSuperGV.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights	

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $strStep = Filter($_POST["step"]);
    if ($strStep == "book") {
        $DB = Connect();
        $strCustomerID = Decode($_POST["CustomerID"]);
        $strStoreID = Filter($_POST["StoreID"]);
        $strAppointmentDate = $_POST["AppointmentDate"];
        $date = new DateTime($strAppointmentDate);
        $strAppointmentDate1 = $date->format('Y-m-d'); // 31-07-2012
        $pqr = date("H:i:s", time());
        $date = new DateTime();
        $giftvoucher = Filter($_POST["giftvoucher"]);

        $sqlInsert = "INSERT INTO tblAppointments (CustomerID, StoreID, AppointmentDate, SuitableAppointmentTime, AppointmentCheckInTime, super_gv_id,super_gv_status, Status,FreeService) VALUES
                ('" . $strCustomerID . "', '" . $strStoreID . "', '" . $strAppointmentDate1 . "', '" . $pqr . "', '" . $pqr . "', '" .
        $giftvoucher . "', '1', '1','0')";

        
        if ($DB->query($sqlInsert) === TRUE) {
            $last_idp = $DB->insert_id;  //last id of tblCustomers insert
        }
        $getUID = EncodeQ($last_idp);
        $DB = Connect();
        $sqlInsert = "INSERT INTO  tblInvoice (AppointmentID, EmailMessageID, DateOfCreation) VALUES ('" . $last_idp . "', 'Null', 'Null')";
        if ($DB->query($sqlInsert) === TRUE) {
            $last_id5 = $DB->insert_id;  //last id of tblAppointments insert
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }

        $DB->close();
        echo("<script>location.href='appointment_invoice.php?uid=$getUID'</script>");
    }

    die();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once("incMetaScript.fya"); ?>
    <script type="text/javascript" src="assets/widgets/datepicker/datepicker.js"></script>
    <script type="text/javascript">
        /* Datepicker bootstrap */

        $(function () {
            "use strict";
            $('.bootstrap-datepicker').bsdatepicker({
                format: 'yyyy-mm-dd'
            });
        });
    </script>
    <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker.js"></script>
    <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker-demo.js"></script>
    <script type="text/javascript" src="assets/widgets/daterangepicker/moment.js"></script>
    <script type="text/javascript" src="assets/widgets/timepicker/timepicker.js"></script>
    <script type="text/javascript">
        /* Timepicker */

        $(function () {
            "use strict";
            $('.timepicker-example').timepicker();
        });
    </script>
    <script>
        $(function ()
        {
            $("#AppointmentDate").datepicker({minDate: 0});
            $("#AppointmentDate").datepicker({minDate: 0});
        });
    </script>
    <script>
        function LoadValue(OptionValue)
        {
            // alert (OptionValue);
            $.ajax({
                type: 'POST',
                url: "GetSuperGVStoreWise.php",
                data: {
                    id: OptionValue
                },
                success: function (response) {
                    //	alert(response)
                    $("#asmita").html("");
                    $("#asmita1").html("");
                    $("#asmita").html(response);

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#asmita").html("<center><font color='red'><b>Please try again after some time</b></font></center>");
                    return false;
                    alert(response);
                }
            });
        }

    </script>

</head>

<body>
<div id="sb-site">

    <?php require_once("incOpenLayout.fya"); ?>


    <?php require_once("incLoader.fya"); ?>

    <div id="page-wrapper">
        <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

        <?php require_once("incLeftMenu.fya"); ?>

        <div id="page-content-wrapper">
            <div id="page-content">

                <?php require_once("incHeader.fya"); ?>


                <div id="page-title">
                    <h2><?= $strDisplayTitle ?></h2>

                </div>
                <div class="panel">
                    <div class="panel-body">
                        <div class="fa-hover">
                            <a class="btn btn-primary btn-lg btn-block" href="javascript:window.location = document.referrer;"><i class="fa fa-backward"></i> &nbsp; Go back to <?= $strPageTitle ?></a>
                        </div>

                        <div class="panel-body">
                            <form role="form" class="form-horizontal bordered-row enquiry_form" onSubmit="proceed_formsubmit('.enquiry_form', '<?= $strMyActionPage ?>', '.result_message', '', '.admin_email', '.admin_password');
                                return false;">

                            <span class="result_message">&nbsp; <br>
                            </span>
                            <br>
                            <?php
                            if (isset($_GET['bid'])) {
                                ?>
                            <input type="hidden" name="step" value="book">

                            <div class="example-box-wrapper">

                                    <?php
                                    $DB = Connect();
                                    $strID = DecodeQ(Filter($_GET['bid'])); //Booking for customer with id=bid

                                    $sql_appointments = "SELECT * FROM tblCustomers WHERE CustomerID='" . $strID . "'";
                                    $RS_appointments = $DB->query($sql_appointments);
                                    if ($RS_appointments->num_rows > 0) {
                                        while ($row_appointments = $RS_appointments->fetch_assoc()) {
                                            foreach ($row_appointments as $key1 => $val1) {
                                                if ($key1 == "CustomerID") {
                                                    ?>
                                <input type="hidden" id="cust_id" name="<?= $key1 ?>" value="<?= Encode($strID) ?>">

                                                    <?php
                                                } elseif ($key1 == "CustomerFullName") {
                                                    ?>
                                <div class="form-group"><label class="col-sm-3 control-label"><?= str_replace("CustomerFullName", "Full Name", $key1) ?> <span>*</span></label>
                                    <div class="col-sm-3"><input readonly type="text" name="<?= $key1 ?>" class="form-control required" placeholder="<?= str_replace("CustomerFullName", "Full Name", $key1) ?>" value="<?= $row_appointments[$key1] ?>"></div>
                                </div>
                                <?php
                            } elseif ($key1 == "CustomerMobileNo") {
                                ?>
                                <div class="form-group"><label class="col-sm-3 control-label"><?= str_replace("CustomerMobileNo", "Mobile No.", $key1) ?> <span>*</span></label>
                                    <div class="col-sm-3"><input readonly type="text" name="<?= $key1 ?>" pattern="[0-9]{10}" title="Enter a valid mobile number!" class="form-control required" placeholder="<?= str_replace("CustomerMobileNo", "Mobile No.", $key1) ?>" value="<?= $row_appointments[$key1] ?>"></div>
                                </div>
                                <?php
                            }
                        }
                    }

                    $sql1 = "SELECT StoreID, StoreName FROM tblStores WHERE Status = 0";
                    $RS2 = $DB->query($sql1);
                    if ($RS2->num_rows > 0) {
                        ?>
                                <div class="form-group"><label class="col-sm-3 control-label">Store<span>*</span></label>
                                    <div class="col-sm-3">
                                        <select class="form-control required"  name="StoreID" id="StoreID" onChange="LoadValue(this.value);">
                                            <option value="" selected>--Select Store--</option>
                                            <?php
                                            while ($row2 = $RS2->fetch_assoc()) {
                                                $StoreID = $row2["StoreID"];
                                                $StoreName = $row2["StoreName"];
                                                ?>
                                            <option value="<?= $StoreID ?>" <?php echo isset($strStore) && $strStore == $StoreID ? 'selected' : ''; ?>><?= $StoreName ?></option>
                                            <?php
                                        }
                                        ?>
                                        </select>
                                    </div>
                                </div>


                                            <?php
                                        }
                                        ?>
                                <div class="form-group"><label class="col-sm-3 control-label">Date<span>*</span></label>
                                    <div class="col-sm-3">
                                        <!--<span class="add-on input-group-addon"><i class="glyph-icon icon-calendar"></i></span><input type="text" name="AppointmentDate" id="AppointmentDate" class="bootstrap-datepicker form-control required" value="02/16/12" data-date-format="yyyy/dd/mm">-->
                                        <div class="input-prepend input-group"><span class="add-on input-group-addon"><i class="glyph-icon icon-calendar"></i></span> <input type="text" name="AppointmentDate" id="AppointmentDate"  class="form-control" data-date-format="YY-MM-DD" value="<?php echo date('Y-m-d'); ?>"></div>
                                    </div>
                                </div>

                                <div id="asmita">
                                    <div class="form-group"><label class="col-sm-3 control-label">Available Super Gift Vouchers<span>*</span></label>
                                        <div class="col-sm-3">
                                            <select id="giftvoucher"  class="form-control giftvoucher required" name="giftvoucher">
                                                <option value="">Select Voucher</option>
                                                <?php
                                                $selpk = select("*", "super_gift_voucher", "status=1 AND GVDateFrom <= '" . date('Y-m-d') . "'");
                                                if (isset($selpk) && is_array($selpk) && count($selpk) > 0) {
                                                    foreach ($selpk as $vapq) {
                                                        $store_id_arr = explode(",", $vapq['StoreID']);
                                                        if ($vapq['GVAmount'] != "" && date('Y-m-d') <= $vapq['GVDateTo']) {
                                                            if(isset($strStore) && $strStore > 0 && in_array($strStore,$store_id_arr)){ ?>
                                                <option value="<?= $vapq['id'] ?>"><?= $vapq['GVcode'] ?></option>
                                                <?php }else if($strStore<=0){
                                                ?>
                                                <option value="<?= $vapq['id'] ?>"><?= $vapq['GVcode'] ?></option>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>

                                            </select>
                                        </div>
                                    </div>
                                </div>

                                        <?php
                                    }
                                    ?>


                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <input type="submit" class="btn ra-100 btn-primary" value="Purchase">
                                    <div class="col-sm-1">
                                        <a class="btn ra-100 btn-black-opacity" href="javascript:;" onclick="ClearInfo('enquiry_form');" title="Clear"><span>Clear</span></a>
                                    </div>
                                </div>
                            </div>

                                <?php
                            }
                            ?>
                        </div>
                        </form>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>	

<?php require_once 'incFooter.fya'; ?>

</div>
</body>

</html>