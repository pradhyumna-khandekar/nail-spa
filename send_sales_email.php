<?php

require_once("setting.fya");
$mail_body = require_once("employee_sales_temp.php");
$DB = Connect();
/*
 * Get All Email ids for report
 */
$report_data = select("*", 'report_config', 'status =1 AND report_name="emp_sales"');
if (isset($report_data) && is_array($report_data) && count($report_data) > 0) {
    foreach ($report_data as $key => $value) {
        $strTo = $value["email_id"];
        $strFrom = "invoice@nailspaexperience.com";
        $strSubject = "Sales Report";
        /*
         * Insert Into message table
         */
        $col_val = array('ToEmail', 'FromEmail', 'Subject', 'Body', 'DateTime', 'Status', 'created_date', 'created_by', 'description', 'CustomerID');
        $user['ToEmail'] = "'" . $strTo . "'";
        $user['FromEmail'] = "'" . $strFrom . "'";
        $user['Subject'] = "'" . $strSubject . "'";
        $user['Body'] = "'" . $mail_body . "'";
        $user['DateTime'] = "'" . date('Y-m-d H:i:s') . "'";
        $user['Status'] = "'0'";
        $user['created_date'] = "'" . date('Y-m-d H:i:s') . "'";
        $user['created_by'] = '0';
        $user['description'] = "'Employee Sales Report'";
        $user['CustomerID'] = "'0'";
        $field_values = implode(',', $col_val);
        $data_values = implode(',', $user);
        $insert_sql = "INSERT into tblEmailMessages (" . $field_values . ") VALUES(" . $data_values . ")";
        $DB->query($insert_sql);
        $last_email_id = $DB->insert_id;
        unset($user);

        $strbody1 = $mail_body;
        $headers = "From: $strFrom\r\n";
        $headers .= "Content-type: text/html\r\n";
        $strBodysa = AntiFilter1($strbody1);

        // Mail sending 
        $retval = mail($strTo, $strSubject, $strBodysa, $headers);

        if ($retval == true) {
            $update_qry = "UPDATE tblEmailMessages SET Status='1',DateOfSending='" . date('Y-m-d H:i:s') . "'"
                    . " WHERE ID='" . $last_email_id . "'";
            $DB->query($update_qry);
        } else {
            $update_qry = "UPDATE tblEmailMessages SET Status='2'"
                    . " WHERE ID='" . $last_email_id . "'";
            $DB->query($update_qry);
        }
    }
}
$DB->close();
?>
