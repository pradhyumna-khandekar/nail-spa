<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>

<?php require_once("cash_invoice.php"); ?>

<?php

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $DB = Connect();
    $delete_count = 0;
    $post_data = $_POST;
    $append = '';
    if (isset($post_data) && is_array($post_data) && count($post_data) > 0) {
        if (isset($post_data['percentage']) && $post_data['percentage'] > 0 && $post_data['percentage'] <= 100) {
            $strtoandfrom = $post_data["toandfrom"];
            $arraytofrom = explode("-", $strtoandfrom);

            $from = $arraytofrom[0];
            $datetime = new DateTime($from);
            $getfrom = $datetime->format('Y-m-d');


            $to = $arraytofrom[1];
            $datetime = new DateTime($to);
            $getto = $datetime->format('Y-m-d');

            $store = $post_data["Store"];

            if (!empty($store)) {
                $append .= " AND apt.StoreID ='" . $store . "'";
            }
            if (!IsNull($getfrom)) {
                $append .= " and apt.AppointmentDate >= '" . $getfrom . "'";
            }

            if (!IsNull($getto)) {
                $append .= " and apt.AppointmentDate <= '" . $getto . "'";
            }

            /*
             * Get Cash Invoice Bill that has no dependency in future
             */
            $aptq = "SELECT apt.*,inv.Total FROM tblAppointments apt "
                    . " JOIN tblInvoiceDetails inv ON (inv.AppointmentId = apt.AppointmentID)"
                    . " WHERE inv.Flag='CS' AND apt.Status = 2 AND apt.IsDeleted = 0 " . $append;
            $aptq_exe = $DB->query($aptq);
            if ($aptq_exe->num_rows > 0) {
                while ($apt_res = $aptq_exe->fetch_assoc()) {
                    $apt_res_data[$apt_res['AppointmentID']] = $apt_res;
                    $aptids[$apt_res['AppointmentID']] = $apt_res['AppointmentID'];
                }
            }

            if (isset($aptids) && is_array($aptids) && count($aptids) > 0) {
                $in_apt_ids = implode(",", $aptids);
                if ($in_apt_ids != '') {
                    /*
                     * Check invoice is paid or not
                     */
                    $pendq = "SELECT * FROM `tblPendingPayments` WHERE `AppointmentID` IN(" . $in_apt_ids . ")";
                    $pendq_exe = $DB->query($pendq);
                    if ($pendq_exe->num_rows > 0) {
                        while ($pen_res = $pendq_exe->fetch_assoc()) {
                            $pend_apt_ids[$pen_res['AppointmentID']] = $pen_res['AppointmentID'];
                        }
                    }
                }
            }

            /*
             * Check Which Bill is dependent and which is not
             */
            $dependent_total = 0;
            $independent_total = 0;


            if (isset($apt_res_data) && is_array($apt_res_data) && count($apt_res_data) > 0) {
                foreach ($apt_res_data as $akey => $avalue) {
                    /*
                     * If voucher id OR package id OR Pending Bill then dependent bill 
                     */
                    if ($avalue['VoucherID'] != '0' || $avalue['PackageID'] != '0' || isset($pend_apt_ids[$avalue['AppointmentID']])) {
                        $dependent_total += $avalue['Total'];
                    } else {
                        $independent_total += $avalue['Total'];
                        $indep_apt[$avalue['AppointmentID']] = $avalue['AppointmentID'];
                        $independent_res[$avalue['AppointmentDate']][$avalue['AppointmentID']]['AppointmentID'] = $avalue['AppointmentID'];
                        $independent_res[$avalue['AppointmentDate']][$avalue['AppointmentID']]['amount'] = $avalue['Total'];


                        $inv_to_del[$avalue['AppointmentID']] = $avalue['AppointmentID'];
                    }
                }
            }
            $total_cash_invoice = isset($apt_res_data) ? count($apt_res_data) : 0;

            $percentage = $post_data['percentage'];

            $amount_to_remain = ($independent_total * $percentage) / 100;
            $amount_to_remain = round($amount_to_remain);

            if ($amount_to_remain <= $independent_total) {
                $amount_match = 0;
                $all_mon_year = get_month_year($getfrom, $getto);
                $day_to_find = array(7, 2, 4, 6, 1, 3, 5);

                if (isset($all_mon_year) && is_array($all_mon_year) && count($all_mon_year) > 0) {
                    foreach ($all_mon_year as $moyr_key => $moyr_value) {
                        foreach ($day_to_find as $day_key => $day_value) {
                            $get_apt_date = get_date($moyr_value, $day_value);

                            if (isset($get_apt_date) && is_array($get_apt_date) && count($get_apt_date) > 0) {
                                foreach ($get_apt_date as $adate_key => $adate_value) {

                                    /*
                                     * Get invoice for this appointment date
                                     */
                                    $get_invoice = isset($independent_res[$adate_value]) ? $independent_res[$adate_value] : array();
                                    if (isset($get_invoice) && is_array($get_invoice) && count($get_invoice) > 0) {
                                        foreach ($get_invoice as $inv_key => $inv_value) {
                                            $amount_match += $inv_value['amount'];
                                            if ($amount_match <= $amount_to_remain) {
                                                //$untoch_bill[$inv_value['AppointmentID']] = $inv_value['AppointmentID'];

                                                unset($inv_to_del[$inv_value['AppointmentID']]);
                                            }
                                            if ($amount_match >= $amount_to_remain) {
                                                break;
                                            }
                                        }
                                    }
                                    if ($amount_match >= $amount_to_remain) {
                                        break;
                                    }
                                }
                            }if ($amount_match >= $amount_to_remain) {
                                break;
                            }
                        }
                        if ($amount_match >= $amount_to_remain) {
                            break;
                        }
                    }
                }


                $table_to_del = array(
                    'tblAppointmentAssignEmployee',
                    'tblAppointmentMembershipDiscount',
                    'tblAppointments',
                    'tblAppointmentsCharges',
                    'tblAppointmentsChargesInvoice',
                    'tblAppointmentsDetails',
                    'tblAppointmentsDetailsInvoice',
                    'tblInvoice',
                    'tblInvoiceDetails'
                );

                /*
                 * Take backup 
                 */

                if (isset($inv_to_del) && is_array($inv_to_del) && count($inv_to_del) > 0) {
                    foreach ($inv_to_del as $delkey => $delvalue) {
                        $inv_in_arr[$delvalue] = $delvalue;
                    }
                }

                if (isset($inv_in_arr) && is_array($inv_in_arr) && count($inv_in_arr) > 0) {
                    $in_inv_ids = implode(",", $inv_in_arr);
                    if ($in_inv_ids != '') {
                        $appointment_id = $delvalue;
                        foreach ($table_to_del as $tkey => $tvalue) {
                            $insert_append = '';
                            $sel1_col = array();
                            $sel1_val = array();

                            if ($tvalue == 'tblInvoiceDetails') {
                                $get_backupq = "SELECT * FROM `" . $tvalue . "` WHERE `AppointmentId` IN(" . $in_inv_ids . ")";
                            } else {
                                $get_backupq = "SELECT * FROM `" . $tvalue . "` WHERE `AppointmentID` IN(" . $in_inv_ids . ")";
                            }

                            $get_backupq_exe = $DB->query($get_backupq);
                            if ($get_backupq_exe->num_rows > 0) {
                                while ($back_data_res = $get_backupq_exe->fetch_assoc()) {
                                    $sel1[] = $back_data_res;
                                }
                            }


                            if (isset($sel1) && is_array($sel1) && count($sel1) > 0) {
                                foreach ($sel1[0] as $key1 => $value1) {
                                    $sel1_col[$key1] = $key1;
                                }
                                $sel1_col['back_created_date'] = 'back_created_date';
                                foreach ($sel1 as $s1key => $s1value) {
                                    foreach ($s1value as $s1commakey => $s1commavalue) {
                                        $sel1_val[$s1key][$s1commakey] = "'" . $s1commavalue . "'";
                                    }
                                    $sel1_val[$s1key]['back_created_date'] = "'" . date('Y-m-d H:i:s') . "'";
                                }
                                if (isset($sel1_val) && is_array($sel1_val) && count($sel1_val) > 0) {
                                    foreach ($sel1_val as $sel1_ink => $sel1_inv) {
                                        if ($sel1_ink == count($sel1_val) - 1) {
                                            $insert_append .= " (" . implode(",", $sel1_inv) . ");";
                                        } else {
                                            $insert_append .= " (" . implode(",", $sel1_inv) . "),";
                                        }
                                    }
                                }
                                $query = "INSERT INTO " . $tvalue . "_obackup (" . implode(",", $sel1_col) . ")  VALUES " . $insert_append;
                                // echo "<br>query=" . $query;
                                $DB->query($query);
                                unset($sel1);
                            }
                        }
                    }
                }

                if (isset($inv_in_arr) && is_array($inv_in_arr) && count($inv_in_arr) > 0) {
                    $in_inv_ids = implode(",", $inv_in_arr);
                    if ($in_inv_ids != '') {

                        $del1 = "DELETE FROM tblAppointmentAssignEmployee WHERE AppointmentID IN(" . $in_inv_ids . ")";
                        $DB->query($del1);

                        $del2 = "DELETE FROM tblAppointmentMembershipDiscount WHERE AppointmentID IN(" . $in_inv_ids . ")";
                        $DB->query($del2);

                        $del3 = "DELETE FROM tblAppointments WHERE AppointmentID IN(" . $in_inv_ids . ")";
                        $DB->query($del3);

                        $del4 = "DELETE FROM tblAppointmentsCharges WHERE AppointmentID IN(" . $in_inv_ids . ")";
                        $DB->query($del4);

                        $del5 = "DELETE FROM tblAppointmentsChargesInvoice WHERE AppointmentID IN(" . $in_inv_ids . ")";
                        $DB->query($del5);

                        $del6 = "DELETE FROM tblAppointmentsDetails WHERE AppointmentID IN(" . $in_inv_ids . ")";
                        $DB->query($del6);

                        $del7 = "DELETE FROM tblAppointmentsDetailsInvoice WHERE AppointmentID IN(" . $in_inv_ids . ")";
                        $DB->query($del7);

                        $del7 = "DELETE FROM tblAppointmentsDetailsInvoice WHERE AppointmentID IN(" . $in_inv_ids . ")";
                        $DB->query($del7);

                        $del8 = "DELETE FROM tblInvoice WHERE AppointmentID IN(" . $in_inv_ids . ")";
                        $DB->query($del8);

                        $del9 = "DELETE FROM tblInvoiceDetails WHERE AppointmentId IN(" . $in_inv_ids . ")";
                        $DB->query($del9);
                        // $delete_count++;
                    }
                }
                $error_msg = "Invoice deleted Successfully.";
            } else {
                $error_msg = "Percentage Amount Could not greater than independent amount.";
            }
        }
    } else {
        $error_msg = "Enter Percentage Between 1 to 100.";
    }
    $DB->close();
    header('Location:show_cash_invoice.php?error_msg=' . $error_msg . '');
}
?>