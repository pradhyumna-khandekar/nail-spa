<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
	$strPageTitle = "New Invoice | Nailspa";
	$strDisplayTitle = "Invoice";

	if($strAdminType!="0")
	{
		die("Sorry you are trying to enter Unauthorized access");
	}
?>


<!DOCTYPE html>
<html>
<head>
	<link href="https://fonts.googleapis.com/css?family=Actor" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<script>
		function sendmail()
		{
			var divContents = encodeURIComponent($("#printarea").html());
			var email =$("#email").val();
			var app=$("#app_id").val();
			if(email!='')
			{
				$.ajax({
					type:"post",
					data:"divContents="+divContents+"&email="+email+"&app="+app,
					url:"SendMailstoCustomers.php",
					success:function(result1)
					{
						alert(result1)
						
						
					}
				});
			}
			else
			{
				alert('Email Id is blank')
			}
					
		}
	</script>
</head>
	<?php
		if($_SERVER['REMOTE_ADDR']=="111.119.219.70" || $_SERVER['REMOTE_ADDR']=="103.68.16.14")
		{
	?>
			<td align="right"><button onclick="sendmail()" class="btn btn-success">Send Email to Client<div class="ripple-wrapper"></div></button></td>
	<?php 
		} 
	?>
<body>
<span id="printarea">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" width="100%">
		<tbody>
			<!--<tr>
				<td height="10"></td>
			</tr>-->
			<?php
			$seldp=select("*","tblAppointments","AppointmentID='".DecodeQ($_GET['uid'])."'");
			$seldpd=select("StoreName","tblStores","StoreID='".$seldp[0]['StoreID']."'");
			$seldpde=select("InvoiceID","tblInvoice","AppointmentID='".DecodeQ($_GET['uid'])."'");
			$seldpdep=select("*","tblCustomers","CustomerID='".$seldp[0]['CustomerID']."'");
			
			$timestamp =  date("H:i:s", time());
			//sqlUpdate1 = "UPDATE tblAppointments SET AppointmentCheckOutTime = '".$timestamp."', Status = '2' WHERE AppointmentID='".DecodeQ($_GET['uid'])."'";
			//$passingID = EncodeQ(DecodeQ($passingID1));
			//ExecuteNQ($sqlUpdate1);

			$seldpdeptp=select("*"," tblAppointmentsDetailsInvoice","AppointmentID='".DecodeQ($_GET['uid'])."'");
			foreach($seldpdeptp as $ty)
			{
				$totalservices=$ty['ServiceID'];
				$seldpdepp=select("MECID","tblAppointmentAssignEmployee","ServiceID='".$totalservices."' and AppointmentID='".DecodeQ($_GET['uid'])."'");
				//print_r($seldpdepp);
			}
			
			$appp_id=DecodeQ($_GET['uid']);
	?>
			<tr>
				<td>					
					<table class="one" style="font-family: 'Actor', sans-serif; margin: 0 auto; background: url('http://pos.nailspaexperience.com/admin/images/invoice/header1.png'); height: 110px; background-size: cover; color: #807e7f;"width="900">
						<tr>
							<td width="20%">&nbsp;</td>
							<?php
								$selSettingsData=select("*","tblSettings","SettingID='1'");
							
								$selStoreGST=select("GSTNo, AccountingCode, CompanyName","tblStores","StoreID='".$seldp[0]['StoreID']."'");
								if($selStoreGST[0]['GSTNo']=="" || $selStoreGST[0]['GSTNo']==" " || $selStoreGST[0]['GSTNo']=="0")
								{
									$strGSTno = $selSettingsData[0]['MasterGSTNo'];
								}
								else
								{
									$strGSTno = $selStoreGST[0]['GSTNo'];
								}
								
								if($selStoreGST[0]['CompanyName']=="" || $selStoreGST[0]['CompanyName']==" " || $selStoreGST[0]['CompanyName']=="0")
								{
									$strCompanyName = $selSettingsData[0]['CompanyName'];
								}
								else
								{
									$strCompanyName = $selStoreGST[0]['CompanyName'];
								}
								
							?>
							<td width="50%"><?=$strCompanyName?><br><br><span class="fs">Maharashtra &bull; GSTIN - <?php  echo  $strGSTno;		?></span></td>
							<td width="40%" align="center">
								<table style="border-top:1px solid #7d7b7c;border-bottom:1px solid #7d7b7c;"><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Customer Copy &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr></table>
								<?php echo $seldpd[0]['StoreName']; ?><br>
								<?php	echo $seldp[0]['AppointmentDate'];	?> - <?php	echo $seldp[0]['AppointmentCheckOutTime'];	?><br>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table class="two" width="900" style="font-family: 'Actor', sans-serif; padding: 15px; margin: 0 auto; color: #807e7f;">
						<tr>
							<td width="30%"><?php echo $seldpdep[0]['CustomerFullName']; ?><br><?php echo $seldpdep[0]['CustomerMobileNo'] ?><br> <?php echo $seldpdep[0]['CustomerEmailID'] ?><br><span class="es" style="font-style: italic;">Membership no: <?php if($seldp[0]['memberid']!='0')	{ echo $seldp[0]['memberid']; } else {echo "-";	}; ?></span><br><br><span class="fs">Maharashtra &bull; GSTIN:"Unregistered"</span></td>
							
							
								<input type="hidden" id="email" value="<?php echo $seldpdep[0]['CustomerEmailID'] ?>" />
								<input type="hidden" name="app_id" id="app_id" value="<?=DecodeQ($_GET['uid'])?>" />
								
								
							<td width="45%"></td>
							
							<?php	 
								$seldpdeppt=select("MECID"," tblAppointmentAssignEmployee","AppointmentID='".DecodeQ($_GET['uid'])."'");	
								foreach($seldpdeppt as $vap)
								{
									$empname=$vap['MECID'];
									if($empname!="0")
									{
										$emppp=$emppp.','.$empname;
									}
								}
							?>
							<td width="25%"><br>Invoice No:<?php echo $seldpde[0]['InvoiceID']; ?><br><br>Stylist: 
							<?php
								if($emppp=="")
								{
									echo "-";
								}
								elseif($emppp=="0")
								{
									
								}
								else
								{
									echo trim($emppp,",");
								}
							?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
				<table class="two" width="900" style="font-family: 'Actor', sans-serif; padding: 15px; margin: 0 auto; color: #807e7f; border-top:1px  solid #ccc;  border-bottom:1px solid #ccc;">
				<?php 
					$seldpdept=select("*"," tblAppointmentsDetailsInvoice","AppointmentID='".DecodeQ($_GET['uid'])."' and PackageService='0'");
					$sub_total=0;
					$countsf = "0";
					$countersaif = "";
					$counterusmani = "1";
					foreach($seldpdept as $val)
					{
						$countersaif ++;
						$countsf++;
						$counterusmani = $counterusmani + 1;
						$totalammt=0;
						$AppointmentDetailsID=$val['AppointmentDetailsID'];
						$servicee=select("*","tblServices","ServiceID='".$val['ServiceID']."'");
						$qtyyy=$val['qty'];
						$amtt=$val['ServiceAmount'];
						$quantity=$val['qty'];
						$totalammt=$qtyyy*$amtt;
						$total=0;
						
						
						
				?>
					
						<tr>
							<td width="35%"><?php  echo $servicee[0]['ServiceName']; ?></td>
							<td width="20%" align="right" style="padding-right: 30px;">
								<?php
									//Stylist
									$stylistID=select("*","tblAppointmentAssignEmployee","AppointmentID='".DecodeQ($_GET['uid'])."' and ServiceID='".$val['ServiceID']."' group by MECID");
									foreach($stylistID as $strstylist)
									{
										$stylistName=select("*","tblEmployees","EID='".$strstylist['MECID']."'");
										echo $stylistName[0]['EmployeeName'].'<br>';
									}
								?>
							</td>
							<td width="5%">&nbsp;</td>
							<td width="20%"><?php  echo $quantity;   ?></td>
							<td width="20%" align="center"><?php echo $totalammt.".00"; ?></td>
						</tr>
					
						
			
				<?php
					}
				?>	
				</table>
				</td>
			</tr>		
			
			
			<tr>
				<td>
				<?php
					$selecttotal=select("*"," tblInvoiceDetails","AppointmentID='".DecodeQ($_GET['uid'])."'");
				?>
					
				
					<table class="three" width="900" style="font-family: 'Actor', sans-serif; background: url('http://pos.nailspaexperience.com/admin/images/invoice/middle.png'); background-size: contain; padding: 15px; margin: 0 auto; background-repeat: no-repeat; color: #807e7f;">
						<tr>
							<td width="25%">&nbsp;</td>
							<td width="25%">&nbsp;</td>
							<td width="10%">&nbsp;</td>
							<td width="20%" align="right">Sub Total</td>
							<td width="20%" align="center"><?php	echo $selecttotal[0]['SubTotal'];	?></td>
						</tr>
						
				<?php
				
				// Purchase memberhship amount		
				$seldember=select("*","tblAppointments","AppointmentID='".DecodeQ($_GET['uid'])."'");
					$memid=$seldember[0]['memberid'];  
					$custid=$seldember[0]['CustomerID'];  
					$getOfferID=$seldember[0]['offerid'];  
				$seldemberg=select("*","tblMembership","MembershipID='".$memid."'");
					$Cost=$seldemberg[0]['Cost'];  		
				
				$selcust=select("*","tblCustomers","CustomerID='".$custid."'");	
					$memberflag=$selcust[0]['memberflag'];  		
					$cust_name=$selcust[0]['CustomerFullName'];
				$selcustd=select("Membership_Amount","tblInvoiceDetails","CustomerFullName='".$cust_name."' and AppointmentId='".DecodeQ($_GET['uid'])."'");	
					$Membership_Amount=$selcustd[0]['Membership_Amount'];
				if($Membership_Amount!="")
				{
					$sub_total=$sub_total+$Cost;
				?>
						<tr>
							<td width="25%">&nbsp;</td>
							<td width="25%">&nbsp;</td>
							<td width="10%">&nbsp;</td>
							<td width="20" align="right"><?=	$seldemberg[0]['MembershipName'];  	?> Membership Cost</td>
							<td width="20%" align="center"><?php echo "".number_format($Cost,2);?></td>
						</tr>
				<?php
				}
				?>


				<?php
				////////////////Member Discount///////////////////////
				if($memid!="0")
				{
					$MembershipDiscountAmount=explode(",",$selecttotal[0]['DisAmt']);
					foreach($MembershipDiscountAmount as $MemAmt)
					{
						$strMembershipAmount += $MemAmt;
					}
					
				?>
						<tr>
							<td width="25%">&nbsp;</td>
							<td width="25%">&nbsp;</td>
							<td width="10%">&nbsp;</td>
							<td width="20" align="right">Memberhsip Discount</td>
							<td width="20%" align="center"><?php echo "".number_format($strMembershipAmount,2);?></td>
						</tr>
				<?php
				// Savings if Membership
				$strSavings = $strMembershipAmount;
				}
				?>
				
				
				
				<?php
				////////////////Offer Discount///////////////////////
				if($getOfferID!="0")
				{
					$OfferDiscountdisplay=str_replace("-","",$selecttotal[0]['OfferAmt']);
					
				?>
						<tr>
							<td width="25%">&nbsp;</td>
							<td width="25%">&nbsp;</td>
							<td width="10%">&nbsp;</td>
							<td width="20" align="right">Offer Discount</td>
							<td width="20%" align="center"><?php echo number_format($OfferDiscountdisplay,2);?></td>
						</tr>
				<?php
				// Savings if Offer
				$strSavings = $OfferDiscountdisplay;
				}
				?>
						
						<!--<tr>
							<td width="25%">&nbsp;</td>
							<td width="25%">&nbsp;</td>
							<td width="10%">&nbsp;</td>
							<td width="20%" align="right">Discount</td>
							<td width="20%" align="center">578001.00</td>
						</tr>-->
				
				<?php
				if($selecttotal[0]['ChargeName']=="SGST")
				{
					$gst = $selecttotal[0]['ChargeAmount'];
					$sgst = str_replace("+", "", $gst) / 2;
					$cgst = str_replace("+", "", $gst) / 2;
				}
				?>
						<tr>
							<td width="25%">&nbsp;</td>
							<td width="25%">&nbsp;</td>
							<td width="10%">&nbsp;</td>
							<td width="20%" align="right">SGST</td>
							<td width="20%" align="center"><?php	echo $sgst	?></td>
						</tr>
						<tr>
							<td width="25%">&nbsp;</td>
							<td width="25%">&nbsp;</td>
							<td width="10%">&nbsp;</td>
							<td width="20%" align="right">CGST</td>
							<td width="20%" align="center"><?php	echo $cgst	?></td>
						</tr>
						<tr>
							<td width="25%">&nbsp;</td>
							<td width="25%">&nbsp;</td>
							<td width="10%">&nbsp;</td>
							<td width="20%" align="right">Total Payment</td>
							<td width="20%" align="center"><?php	echo  number_format($selecttotal[0]['TotalPayment'],2);	?></td>
						</tr>
						<tr>
							<td width="25%">&nbsp;</td>
							<td width="25%">&nbsp;</td>
							<td width="10%">&nbsp;</td>
							<td width="20%" align="right">Round-off</td>
							<td width="20%" align="center"><?php	echo $selecttotal[0]['RoundTotal'];	?></td>
						</tr>
						<tr>
							<td width="25%">&nbsp;</td>
							<td width="25%">&nbsp;</td>
							<td width="10%">&nbsp;</td>
							<td width="20%" align="right">
								Paid by
							</td>
							<td width="20%" align="center">
								<?php	
									if($selecttotal[0]['Flag']== "C")	
									{
										echo 'Card';
									}								
									elseif($selecttotal[0]['Flag']== "CS")
									{
										echo 'Cash';
									}
									else
									{
										echo 'Cash & Card';
									}
								?>
							</td>
						</tr>
						<tr>
							<td height="5"></td>
						</tr>
						<?php
						
						?>
						
						<tr>
							<td width="25%">&nbsp;</td>
							<td width="25%">&nbsp;</td>
							<td width="10%">&nbsp;</td>
							<td width="20%" align="right">Your Savings</td>
							<td width="20%" style="border:1px solid #ccc;" align="center"><?php echo number_format($strSavings,2);?></td>
						</tr>
						<tr>
							<?php
								if($selStoreGST[0]['AccountingCode']=="" || $selStoreGST[0]['AccountingCode']==" " || $selStoreGST[0]['AccountingCode']=="0")
								{
									$strAccoutingCode = $selSettingsData[0]['MasterAccountingCode'];
								}
								else
								{
									$strAccoutingCode = $selStoreGST[0]['AccountingCode'];
								}
							?>
							<td><span class="fs" style="font-size:12.5px;">Accounting Code:<?=$strAccoutingCode?></span></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="900" class="four" style="margin: 0 auto; color: #807e7f;">
						<tr>
							<td>
								<img src="http://pos.nailspaexperience.com/admin/images/invoice/<?php echo $selSettingsData[0]['InvoiceImgURL']; ?>">
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</span>
</body>
</html>