<?php require_once("setting.fya"); ?>
<?php require_once'incFirewall.fya'; ?>
<?php
session_start();

$strPageTitle = "Calendar | Nailspa";
$strDisplayTitle = "Calendar of Nailspa Experience";
$strMenuID = "2";


// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights	
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <!-----------css & js files added for tabs by gandhali 3/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");     ?>
            <!----------commented by gandhali 3/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">

                    <div id="page-content">
                        <div id="page-title">
                            <h2><?php echo $strDisplayTitle; ?></h2>
                        </div>


                        <?php require_once("incHeader.fya"); ?>
                        <?php $DB = Connect(); ?>
                        <?php
                        $store_color = array('#DC143C', '#808000', '#32CD32', '#00CED1', '#0000FF', '#BA55D3', '#800080', '#483D8B', '#BC8F8F', '#800000', '#C71585', '#F08080', '#4B0082', '#556B2F', '#FA8072');
                        if ($strStore > 0) {
                            $sql1 = select("StoreID, StoreName", "tblStores", "Status='0' AND StoreID='" . $strStore . "'");
                        } else {
                            $sql1 = select("StoreID, StoreName", "tblStores", "Status='0'");
                        }
                        if (isset($sql1) && is_array($sql1) && count($sql1) > 0) {
                            foreach ($sql1 as $key => $value) {
                                $store_data[$value['StoreID']] = array(
                                    'name' => $value['StoreName'],
                                    'color' => isset($store_color[$key]) ? $store_color[$key] : '#000'
                                );
                            }
                        }


                        $currentDate = date('Y-m-d');
                        //$result = $DB->query("SELECT * FROM tblAppointments WHERE AppointmentDate = '" . $currentDate . "' AND (Status='2' OR Status='0') ");
                        if ($strStore > 0) {
                            $result = $DB->query("SELECT * FROM tblAppointments WHERE (Status='2' OR Status='0') AND StoreID='" . $strStore . "'");
                        } else {
                            $result = $DB->query("SELECT * FROM tblAppointments WHERE (Status='2' OR Status='0')");
                        }
                        if ($result->num_rows > 0) {
                            while ($row = $result->fetch_assoc()) {
                                if (isset($appointment_data[$row['StoreID']][$row['AppointmentDate']])) {
                                    $appointment_data[$row['StoreID']][$row['AppointmentDate']] += 1;
                                } else {
                                    $appointment_data[$row['StoreID']][$row['AppointmentDate']] = 1;
                                }
                            }
                        }

                        $calandar_data = array();
                        if (isset($appointment_data) && is_array($appointment_data) && count($appointment_data) > 0) {
                            foreach ($appointment_data as $akey => $avalue) {
                                foreach ($avalue as $datekey => $datevalue) {
                                    $calandar_data[] = array(
                                        'title' => 'Total Appointment (' . $datevalue . ')',
                                        'start' => $datekey,
                                        'color' => isset($store_data[$akey]) ? $store_data[$akey]['color'] : '#000'
                                    );
                                }
                            }
                        }

                        if (isset($appointment_data) && is_array($appointment_data) && count($appointment_data) > 0) {
                            if (isset($store_data) && is_array($store_data) && count($store_data) > 0) {
                                ?>
                                <ul>
                                    <?php foreach ($store_data as $stokey => $stovalue) {
                                        ?>
                                        <li type="square">
                                            <span style="color:#fff;background-color:<?php echo $stovalue['color']; ?>"><?php echo $stovalue['name']; ?></span>
                                        </li>
                                    <?php }
                                    ?>
                                </ul>
                                <?php
                            }
                        }
                        ?>
                        <link href='assets/widgets/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
                        <link href='assets/widgets/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
                        <script src='assets/widgets/fullcalendar/moment.min.js'></script>
                        <script src='assets/widgets/fullcalendar/jquery.min.js'></script>
                        <script src='assets/widgets/fullcalendar/fullcalendar.min.js'></script>
                        <script>

                            $(document).ready(function () {

                                $('#calendar').fullCalendar({
                                    dayClick: function (date, jsEvent, view) {
                                        window.open('ManageCustomers2.php', '_blank');
                                    },
                                    header: {
                                        left: 'prev,next today',
                                        center: 'title',
                                        right: 'month,basicWeek,basicDay'
                                    },
                                    defaultDate: '<?php echo date('Y-m-d'); ?>',
                                    navLinks: true, // can click day/week names to navigate views
                                    editable: true,
                                    eventLimit: true, // allow "more" link when too many events
                                    events:<?php echo json_encode($calandar_data); ?>
                                    /* events: [
                                     {
                                     title: 'All Day Event',
                                     start: '2018-03-01',
                                     color: '#000'
                                     },
                                     {
                                     title: 'All Day Event',
                                     start: '2018-03-01'
                                     },
                                     {
                                     title: 'All Day Event',
                                     start: '2018-03-01'
                                     },
                                     {
                                     title: 'All Day Event',
                                     start: '2018-03-01'
                                     },
                                     {
                                     title: 'All Day Event',
                                     start: '2018-03-01'
                                     },
                                     ] */
                                });

                            });

                        </script>
                        <div class="panel">
                            <div class="panel-body">
                                <div id='calendar'></div>





                                <div class="example-box-wrapper">
                                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="datatable-example">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center">Store</th>
                                                <th style="text-align:center">Open Time</th>
                                                <th style="text-align:center">Close Time</th>
                                                <th  style="text-align:center">Total Today's Sales</th>
                                                <th  style="text-align:center">Total Appointments</th>
                                                <th  style="text-align:center">Done Appointments</th>
                                                <th  style="text-align:center">Pending Appointments</th>
                                                <th  style="text-align:center">Upcoming Appointments</th>
                                                <th  style="text-align:center">Cancel Appointments</th>
                                                <th  style="text-align:center">Delayed Appointments</th>
                                                <th  style="text-align:center">Rescheduled Appointments</th>



                                                <?php
                                                if ($strAdminRoleID != '36') {
                                                    ?>
                                                    <?php
                                                } else {
                                                    ?>														
                                                    <th  style="text-align:center">Average Ticket Size</th>
                                                        <?php
                                                    }
                                                    ?>
                                        </thead>


                                        <tbody>
                                            <?php
                                            $date = date('y-m-d');
                                            $DB = Connect();
                                            $FindStore = "Select * from tblOpenNClose where DateNTime='" . $date . "'";
// echo $FindStore;
                                            $RSf = $DB->query($FindStore);
                                            if ($RSf->num_rows > 0) {
                                                while ($rowf = $RSf->fetch_assoc()) {
                                                    $strStoreIDd = $rowf["StoreID"];
                                                    $selp = select("StoreName", "tblStores", "StoreID='" . $strStoreIDd . "'");
                                                    $StoreName = $selp[0]['StoreName'];
                                                    $OpenTime = $rowf["OpenTime"];
                                                    $CloseTime = $rowf["CloseTime"];

                                                    $OpenTimet = date("H:i:s", strtotime($OpenTime));
                                                    $OpenTimett = get12hour($OpenTimet);
                                                    $CloseTimet = date("H:i:s", strtotime($CloseTime));
                                                    $CloseTimett = get12hour($CloseTimet);
                                                    $sepq = select("SUM(tblInvoiceDetails.TotalPayment) as TOTAL", "tblAppointments Left join tblInvoiceDetails
											ON tblAppointments.AppointmentID=tblInvoiceDetails.AppointmentID", "tblAppointments.AppointmentDate='" . $date . "' and tblAppointments.StoreID='$strStoreIDd'");
                                                    $TOTAL = $sepq[0]['TOTAL'];
                                                    if ($TOTAL == '' || $TOTAL == '') {
                                                        $TOTAL = 0;
                                                    }
                                                    $sepqt = select("count(tblAppointments.AppointmentID) as cntp", "tblAppointments", "tblAppointments.AppointmentDate='" . $date . "' and tblAppointments.StoreID='$strStoreIDd'");
                                                    $cntp = $sepqt[0]['cntp'];
                                                    $sepqtpending = select("count(tblAppointments.AppointmentID) as cntpen", "tblAppointments", "tblAppointments.AppointmentDate='" . $date . "' and tblAppointments.StoreID='$strStoreIDd' and Status='1'");
                                                    $cntpen = $sepqtpending[0]['cntpen'];
                                                    $sepqtdone = select("count(tblAppointments.AppointmentID) as cntdone", "tblAppointments", "tblAppointments.AppointmentDate='" . $date . "' and tblAppointments.StoreID='$strStoreIDd' and Status='2'");
                                                    $cntdone = $sepqtdone[0]['cntdone'];

                                                    $sepqtcancel = select("count(tblAppointments.AppointmentID) as cntcancel", "tblAppointments", "tblAppointments.AppointmentDate='" . $date . "' and tblAppointments.StoreID='$strStoreIDd' and Status='3'");
                                                    $cntcancel = $sepqtcancel[0]['cntcancel'];

                                                    $sepqtupcome = select("count(tblAppointments.AppointmentID) as cntupcome", "tblAppointments", "tblAppointments.AppointmentDate='" . $date . "' and tblAppointments.StoreID='$strStoreIDd' and Status='0'");
                                                    $cntupcome = $sepqtupcome[0]['cntupcome'];

                                                    $sepqtdelayed = select("count(tblAppointments.AppointmentID) as cntdealyed", "tblAppointments", "tblAppointments.AppointmentDate='" . $date . "' and tblAppointments.StoreID='$strStoreIDd' and Status='5'");
                                                    $cntdealyed = $sepqtdelayed[0]['cntdealyed'];


                                                    $sepqtresche = select("count(tblAppointments.AppointmentID) as cntresh", "tblAppointments", "tblAppointments.AppointmentDate='" . $date . "' and tblAppointments.StoreID='$strStoreIDd' and Status='6'");
                                                    $cntres = $sepqtresche[0]['cntresh'];
                                                    $size = $TOTAL / $cntp;
                                                    ?>
                                                    <tr><td><center><b><?= $StoreName ?></b></center></td><td><center><b><?= $OpenTimett ?></b></center></td><td><center><b><?= $CloseTimett ?></b></center></td><td><b><center><?= 'Rs.' . $TOTAL ?></center></b></td><td><b><center><?= $cntp ?></center></b></td><td><b><center><?= $cntdone ?></center></b></td><td><b><center><?= $cntpen ?></center></b></td><td><b><center><?= $cntupcome ?></center></b></td><td><b><center><?= $cntcancel ?></center></b></td><td><b><center><?= $cntdealyed ?></center></b></td>
                                                <td><b><center><?= $cntres ?></center></b></td>
                                                <?php
                                                if ($strAdminRoleID != '36') {
                                                    ?>												

                                                    <?php
                                                } else {
                                                    ?>
                                                    <td><b><center><?= round($size) ?></center></b></td>
                                                    <?php
                                                }
                                                ?>

                                                </tr>
                                                <?php
                                                // echo $strStoreID;
                                                // echo "Hello";
                                            }
                                        }
                                        $DB->close();
                                        ?>										
                                        </tbody>
                                    </table>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php $DB->close(); ?>
            <?php require_once 'incFooter.fya'; ?>
        </div>
    </body>
</html>