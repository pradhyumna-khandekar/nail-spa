<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php require_once 'comparison_report_helper.php'; ?>
<?php
if (isset($_GET) && is_array($_GET) && count($_GET) > 0) {
    $date1 = isset($_GET['date1']) ? $_GET['date1'] : '';
    $date2 = isset($_GET['date2']) ? $_GET['date2'] : '';
    $date3 = isset($_GET['date3']) ? $_GET['date3'] : '';
    $Store = isset($_GET['Store']) && !empty($_GET['Store']) ? $_GET['Store'] : '';

    if ($date1 != '') {
        $date1_data = Report_sale($date1,$Store);
    }

    if ($date2 != '') {
        $date2_data = Report_sale($date2,$Store);
    }

    if ($date3 != '') {
        $date3_data = Report_sale($date3,$Store);
    }
}
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <!-----------css & js files added for tabs by gandhali 3/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>
    </head>

    <body>
        <div id="sb-site">
            <?php require_once("incLoader.fya"); ?>



            <div id="page-wrapper">
                <?php
                if (isset($_GET) && is_array($_GET) && count($_GET) > 0) {
                    ?>
                    <table class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Month</th>
                                <th><?php echo isset($_GET['date1']) && $_GET['date1'] != '' ? $_GET['date1'] : 'Date1' ?></th>
                                <th><?php echo isset($_GET['date2']) && $_GET['date2'] != '' ? $_GET['date2'] : 'Date2' ?></th>
                                <th><?php echo isset($_GET['date3']) && $_GET['date3'] != '' ? $_GET['date3'] : 'Date3' ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $year1 = isset($_GET['date1']) ? $_GET['date1'] : '';
                            $year2 = isset($_GET['date2']) ? $_GET['date2'] : '';
                            $year3 = isset($_GET['date3']) ? $_GET['date3'] : '';
                            for ($m = 01; $m <= 12; ++$m) {
                                ?>
                                <tr>
                                    <td><?php echo date('F', mktime(0, 0, 0, $m, 1)); ?></td>
                                    <?php
                                    if (strlen($m) == 1) {
                                        $mon = '0' . $m;
                                    } else {
                                        $mon = $m;
                                    }

                                    $chart_data[$year1]['name'] = $year1;
                                    $chart_data[$year2]['name'] = $year2;
                                    $chart_data[$year3]['name'] = $year3;

                                    $chart_data[$year1]['data'][$mon] = isset($date1_data[$year1 . '-' . $mon]) ? $date1_data[$year1 . '-' . $mon] : 0;
                                    $chart_data[$year2]['data'][$mon] = isset($date2_data[$year2 . '-' . $mon]) ? $date2_data[$year2 . '-' . $mon] : 0;
                                    $chart_data[$year3]['data'][$mon] = isset($date3_data[$year3 . '-' . $mon]) ? $date3_data[$year3 . '-' . $mon] : 0;
                                    ?>
                                    <td><?php
                                        $date1_amount = isset($date1_data[$year1 . '-' . $mon]) ? $date1_data[$year1 . '-' . $mon] : 0;
                                        if (isset($total[$year1])) {
                                            $total[$year1] += $date1_amount;
                                        } else {
                                            $total[$year1] = 0;
                                        }
                                        echo $date1_amount;
                                        ?></td>
                                    <td><?php
                                        $date2_amount = isset($date2_data[$year2 . '-' . $mon]) ? $date2_data[$year2 . '-' . $mon] : 0;
                                        if (isset($total[$year2])) {
                                            $total[$year2] += $date2_amount;
                                        } else {
                                            $total[$year2] = 0;
                                        }
                                        echo $date2_amount;
                                        ?></td>
                                    <td><?php
                                        $date3_amount = isset($date3_data[$year3 . '-' . $mon]) ? $date3_data[$year3 . '-' . $mon] : 0;
                                        if (isset($total[$year3])) {
                                            $total[$year3] += $date3_amount;
                                        } else {
                                            $total[$year3] = 0;
                                        }
                                        echo $date3_amount;
                                        ?></td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td>Total</td>
                                <td><?php echo isset($total[$year1]) ? $total[$year1] : 0; ?></td>
                                <td><?php echo isset($total[$year2]) ? $total[$year2] : 0; ?></td>
                                <td><?php echo isset($total[$year3]) ? $total[$year3] : 0; ?></td>
                            </tr>
                        </tbody>
                    </table>

                <?php } else { ?>
                    <h3>No Date Selected.</h3>
                    <?php
                }

                $final_data = array();
                if (isset($chart_data) && is_array($chart_data) && count($chart_data) > 0) {
                    $col_count = 0;
                    foreach ($chart_data as $ykey => $yvalue) {
                        $final_data[$col_count]['name'] = $yvalue['name'];
                        $mon_count = 0;
                        foreach ($yvalue['data'] as $ykey => $yvalue) {
                            $final_data[$col_count]['data'][$mon_count] = $yvalue;
                            $mon_count++;
                        }
                        $col_count++;
                    }
                }
                ?>
            </div>

            <script src="assets/widgets/highcharts/highcharts.js"></script>
            <script src="assets/widgets/highcharts/exporting.js"></script>
            <script src="assets/widgets/highcharts/export-data.js"></script>

            <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            <script>
                Highcharts.chart('container', {
                    chart: {
                        type: 'line'
                    },
                    title: {
                        text: 'Service Sale'
                    },
                    xAxis: {
                        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                    },
                    yAxis: {
                        title: {
                            text: 'Service Sale(In Rs.)'
                        }
                    },
                    plotOptions: {
                        line: {
                            dataLabels: {
                                enabled: true
                            },
                            enableMouseTracking: true
                        }
                    },
                    series: <?php echo json_encode($final_data); ?>
                });
            </script>
            <?php require_once 'incFooter.fya'; ?>
        </div>
    </body>
</html>