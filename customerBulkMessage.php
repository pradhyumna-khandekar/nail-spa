<?php require_once 'setting.fya'; ?>
<?php require_once 'incFirewall.fya'; ?>
<?php

$DB = Connect();
$append_query = "";
$des_append = "";
$error_msg = "";

$bulk_messge = "";

if (isset($_POST['type']) && $_POST['type'] == 'email') {
    $bulk_messge = $_POST['email_message'];
} else if (isset($_POST['type']) && $_POST['type'] == 'sms') {
    $bulk_messge = $_POST['message'];
}
if (isset($bulk_messge) && $bulk_messge != '') {
    if (isset($_POST['StoreID']) && $_POST['StoreID'] != '' && $_POST['StoreID'] != '0') {
        $des_append .= " Store ID= " . $_POST['StoreID'];
        $storefilq = "SELECT AppointmentID,CustomerID FROM tblAppointments WHERE StoreID ='" . $_POST['StoreID'] . "' AND AppointmentCheckInTime!='00:00:00' AND Status=2";
        $storefilq_exe = $DB->query($storefilq);
        if ($storefilq_exe->num_rows > 0) {
            while ($store_cust = $storefilq_exe->fetch_assoc()) {
                $store_cust_in[$store_cust['CustomerID']] = $store_cust['CustomerID'];
            }
        }

        if (isset($store_cust_in) && is_array($store_cust_in) && count($store_cust_in) > 0) {
            $store_in_ids = implode(",", $store_cust_in);
        }
        if (isset($store_in_ids) && $store_in_ids != '') {
            $append_query .= " AND CustomerID IN(" . $store_in_ids . ")";
        } else {
            $append_query .= " AND CustomerID IN(0)";
        }
    }


    if (isset($_POST['Services']) && $_POST['Services'] != '') {
        $des_append .= " Services ID= " . $_POST['Services'];
        $service_in_ids = $_POST['Services'];
        if ($service_in_ids != '') {
            $servfiq = "SELECT aptde.AppointmentID,apt.CustomerID FROM tblappointments apt "
                    . " JOIN tblAppointmentsDetails aptde ON (apt.AppointmentID = aptde.AppointmentID)"
                    . " WHERE aptde.ServiceID IN(" . $service_in_ids . ")";
            $servfiq_exe = $DB->query($servfiq);
            if ($servfiq_exe->num_rows > 0) {
                while ($service_cust = $servfiq_exe->fetch_assoc()) {
                    $service_cust_in[$service_cust['CustomerID']] = $service_cust['CustomerID'];
                }
            }

            if (isset($service_cust_in) && is_array($service_cust_in) && count($service_cust_in) > 0) {
                $service_in_ids = implode(",", $service_cust_in);
            }
            if (isset($service_in_ids) && $service_in_ids != '') {
                $append_query .= " AND CustomerID IN(" . $service_in_ids . ")";
            } else {
                $append_query .= " AND CustomerID IN(0)";
            }
        }
    }


    if (isset($_POST['Category']) && $_POST['Category'] != '' && $_POST['Category'] != '0') {
        $des_append .= " Category ID= " . $_POST['Category'];
        $cat_in_ids = $_POST['Category'];
        if ($cat_in_ids != '') {
            $categoryq = "SELECT aptde.AppointmentID,apt.CustomerID FROM tblAppointments apt "
                    . " JOIN tblAppointmentsDetails aptde ON (apt.AppointmentID = aptde.AppointmentID)"
                    . " JOIN tblProductServiceCategory sercat ON(sercat.ServiceID = aptde.ServiceID)"
                    . " WHERE sercat.CategoryID IN(" . $cat_in_ids . ")";
            $categoryq_exe = $DB->query($categoryq);
            if ($categoryq_exe->num_rows > 0) {
                while ($category_cust = $categoryq_exe->fetch_assoc()) {
                    $cat_cust_in[$category_cust['CustomerID']] = $category_cust['CustomerID'];
                }
            }

            if (isset($cat_cust_in) && is_array($cat_cust_in) && count($cat_cust_in) > 0) {
                $cat_in_ids = implode(",", $cat_cust_in);
            }
            if (isset($cat_in_ids) && $cat_in_ids != '') {
                $append_query .= " AND CustomerID IN(" . $cat_in_ids . ")";
            } else {
                $append_query .= " AND CustomerID IN(0)";
            }
        }
    }

    if (isset($_POST['Gender']) && $_POST['Gender'] != '') {
        $append_query .= " AND Gender='" . $_POST['Gender'] . "'";
    }

    $sql = "SELECT * FROM tblCustomers WHERE Status='0' " . $append_query . " order by CustomerID desc";

    $RS = $DB->query($sql);
    if ($RS->num_rows > 0) {

        while ($row_data = $RS->fetch_assoc()) {
            $customer_data[$row_data['CustomerID']] = $row_data;
        }

        /*
         * if customer found then insert into table
         */

        $userRemember_me = ValidateCookie("CookieRemember_me");

        if ($userRemember_me == "Y") {
            $userAdminID = ValidateCookie("CookieAdminID");
        } else {
            session_start();
            $userAdminID = $_SESSION["AdminID"];
        }

        $success_count = 0;

        if (isset($_POST['type']) && $_POST['type'] == 'sms') {
            if (isset($customer_data) && is_array($customer_data) && count($customer_data) > 0) {
                foreach ($customer_data as $ckey => $cvalue) {
                    if ($cvalue['CustomerMobileNo'] != '') {
                        $unique_customer[$cvalue['CustomerMobileNo']] = $cvalue;
                    }
                }
            }
        } else if (isset($_POST['type']) && $_POST['type'] == 'email') {
            if (isset($customer_data) && is_array($customer_data) && count($customer_data) > 0) {
                foreach ($customer_data as $ckey => $cvalue) {
                    $to_email = $cvalue['CustomerEmailID'];

                    if (filter_var($to_email, FILTER_VALIDATE_EMAIL) && $to_email != '') {
                        $unique_customer[$cvalue['CustomerEmailID']] = $cvalue;
                    }
                }
            }
        }


        if (isset($customer_data) && is_array($customer_data) && count($customer_data) > 0) {
            foreach ($customer_data as $key => $value) {
                if (isset($_POST['type']) && $_POST['type'] == 'sms') {
                    $col_val = array('CustomerID', 'SMSTo', 'Message', 'created_date', 'created_by', 'description');
                    $user['CustomerID'] = "'" . $value['CustomerID'] . "'";
                    $user['SMSTo'] = "'" . $value['CustomerMobileNo'] . "'";
                    $user['Message'] = "'" . $bulk_messge . "'";
                    $user['created_date'] = "'" . date('Y-m-d H:i:s') . "'";
                    $user['created_by'] = isset($userAdminID) && $userAdminID != '' ? "'" . $userAdminID . "'" : '0';
                    $user['description'] = "'customer list bulk SMS " . $des_append . "'";
                    $field_values = implode(',', $col_val);
                    $data_values = implode(',', $user);
                    $insert_sql = "INSERT into tblsmsmessages (" . $field_values . ") VALUES(" . $data_values . ")";
                    $DB->query($insert_sql);
                    unset($user);
                    $success_count += 1;
                } else if (isset($_POST['type']) && $_POST['type'] == 'email') {
                    $to_email = $value['CustomerEmailID'];

                    if (filter_var($to_email, FILTER_VALIDATE_EMAIL)) {
                        $from_email = 'invoice@nailspaexperience.com';
                        $col_val = array('ToEmail', 'FromEmail', 'Subject', 'Body', 'DateTime', 'DateOfSending', 'Status', 'created_date', 'created_by', 'description', 'CustomerID');
                        $user['ToEmail'] = "'" . $to_email . "'";
                        $user['FromEmail'] = "'" . $from_email . "'";
                        $user['Subject'] = "'" . $_POST['subject'] . "'";
                        $user['Body'] = "'" . $bulk_messge . "'";
                        $user['DateTime'] = "'" . date('Y-m-d H:i:s') . "'";
                        $user['DateOfSending'] = "'" . date('Y-m-d H:i:s') . "'";
                        $user['Status'] = "'0'";
                        $user['created_date'] = "'" . date('Y-m-d H:i:s') . "'";
                        $user['created_by'] = isset($userAdminID) && $userAdminID != '' ? "'" . $userAdminID . "'" : '0';
                        $user['description'] = "'customer list bulk Email'";
                        $user['CustomerID'] = "'" . $value['CustomerID'] . "'";
                        $field_values = implode(',', $col_val);
                        $data_values = implode(',', $user);
                        $insert_sql = "INSERT into tblEmailMessages (" . $field_values . ") VALUES(" . $data_values . ")";
                        $DB->query($insert_sql);
                        unset($user);
                        $success_count += 1;
                    }
                }
            }
            //$error_msg = "SMS Sent to " . $success_count . " Customers!";
            $error_msg = $success_count . " Message are in Queue.It Will Send Within some time!";
        } else {
            /*
             * No Customer Found
             */
            $error_msg = "No Customer Found1!";
        }
    } else {
        /*
         * No Customer Found
         */
        $error_msg = "No Customer Found2!";
    }
} else {
    /*
     * No Msg Found
     */
    $error_msg = "Message Cannot be empty!";
}

if (isset($error_msg) && $error_msg != '') {
    header('Location:viewcustomers.php?error_msg=' . $error_msg . '');
} else {
    header('Location:viewcustomers.php');
}


$DB->close();
?>