<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
$strPageTitle = "Retail Product Analysis | Nailspa";
$strDisplayTitle = "Retail Product Analysis for Nailspa";
$strMenuID = "2";
$strMyTable = "tblStoreStock";
$strMyTableID = "StoreStockID";
$strMyField = "";
$strMyActionPage = "ReportServiceAnalysis.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}

// code for not allowing the normal admin to access the super admin rights	



if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $strStep = Filter($_POST["step"]);

    if ($strStep == "add") {
        
    }

    if ($strStep == "edit") {
        
    }
}


if (isset($_GET["toandfrom"])) {
    $strtoandfrom = $_GET["toandfrom"];
    $arraytofrom = explode("-", $strtoandfrom);

    $from = $arraytofrom[0];
    $datetime = new DateTime($from);
    $getfrom = $datetime->format('Y-m-d');


    $to = $arraytofrom[1];
    $datetime = new DateTime($to);
    $getto = $datetime->format('Y-m-d');

    if (!IsNull($from)) {
        $sqlTempfrom = " and Date(tblAppointments.AppointmentDate)>=Date('" . $getfrom . "')";
        $serviceTempfrom = " and Date(product_service_use.start_date)>=Date('" . $getfrom . "')";
        
    }

    if (!IsNull($to)) {
        $sqlTempto = " and Date(tblAppointments.AppointmentDate)<=Date('" . $getto . "')";
        $serviceTempto = " and Date(product_service_use.start_date)<=Date('" . $getfrom . "')";
    }

    $append_qry = "";
    if (!IsNull($from)) {
        $append_qry .= " and Date(apt.AppointmentDate)>=Date('" . $getfrom . "')";
    }

    if (!IsNull($to)) {
        $append_qry .= " and Date(apt.AppointmentDate)<=Date('" . $getto . "')";
    }

    if (!empty($_GET["Store"])) {
        $append_qry .= " AND apt.StoreID='" . $_GET["Store"] . "'";
    }

    $brand_data = select("*", "tblProductBrand", "BrandID > 0");
    if (isset($brand_data) && is_array($brand_data) && count($brand_data) > 0) {
        foreach ($brand_data as $bkey => $bvalue) {
            $brand_names[$bvalue['BrandID']] = $bvalue['BrandName'];
        }
    }
    $product_data = select("*", "tblNewProducts", "ProductID > 0");
    if (isset($product_data) && is_array($product_data) && count($product_data) > 0) {
        foreach ($product_data as $pkey => $pvalue) {
            $product_name_data[$pvalue['ProductID']]['name'] = $pvalue['ProductName'];
            $product_name_data[$pvalue['ProductID']]['mrp'] = $pvalue['ProductMRP'];
            $product_name_data[$pvalue['ProductID']]['code'] = $pvalue['ProductUniqueCode'];

            $pro_brand = explode(",", $pvalue['Brand']);
            foreach ($pro_brand as $bkey => $bvalue) {
                if (trim($bvalue) != '') {
                    $brand_info[$bvalue][$pvalue['ProductID']] = $pvalue['ProductID'];
                    if (isset($brand_names[$bvalue])) {
                        $product_brand_id[$pvalue['ProductID']][$bvalue] = $brand_names[$bvalue];
                    }
                }
            }
        }
    }


    if (!empty($_GET["brand"])) {
        $all_brand_pid = isset($brand_info[$_GET["brand"]]) ? $brand_info[$_GET["brand"]] : array();
        if (isset($all_brand_pid) && is_array($all_brand_pid) && count($all_brand_pid) > 0) {
            $pid = implode(",", $all_brand_pid);
            if ($pid != '') {
                $append_qry .= " AND apr.product_id IN(" . $pid . ")";
            }
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>

        <script type="text/javascript" src="assets/widgets/datepicker/datepicker.js"></script>
        <script type="text/javascript">
            /* Datepicker bootstrap */

            $(function () {
                "use strict";
                $('.bootstrap-datepicker').bsdatepicker({
                    format: 'mm-dd-yyyy'
                });
            });
        </script>
        <script type = "text/javascript" src = "assets/widgets/datepicker-ui/datepicker.js" ></script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker-demo.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/moment.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");           ?>
            <!----------commented by gandhali 5/9/18---------------->
            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>
                <?php require_once("incLeftMenu.fya"); ?>
                <div id="page-content-wrapper">
                    <div id="page-content">
                        <?php require_once("incHeader.fya"); ?>
                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                        </div>


                        <div class="panel">
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="example-box-wrapper">
                                        <div class="tabs">
                                            <div id="normal-tabs-1">
                                                <span class="form_result">&nbsp; <br>
                                                </span>

                                                <div class="panel-body">
                                                    <h3 class="title-hero">List of all Products</h3>

                                                    <form method="get" class="form-horizontal bordered-row" role="form">
                                                        <div class="form-group"><label for="" class="col-sm-4 control-label">Select date</label>
                                                            <div class="col-sm-4">
                                                                <div class="input-prepend input-group">
                                                                    <span class="add-on input-group-addon">
                                                                        <i class="glyph-icon icon-calendar"></i>
                                                                    </span> 
                                                                    <input type="text" autocomplete="off" name="toandfrom" id="daterangepicker-example" class="form-control" value="<?= $strtoandfrom ?>">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Select Store</label>
                                                            <div class="col-sm-4">
                                                                <select name="Store" class="form-control">
                                                                    <option value="0">All</option>
                                                                    <?php
                                                                    $selp = select("*", "tblStores", "Status='0'");
                                                                    foreach ($selp as $val) {
                                                                        $strStoreName = $val["StoreName"];
                                                                        $strStoreID = $val["StoreID"];
                                                                        $store = $_GET["Store"];
                                                                        if ($store == $strStoreID) {
                                                                            ?>
                                                                            <option  selected value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                            <option value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Select Brand</label>
                                                            <?php
                                                            $sqlBrand = "SELECT BrandID, BrandName FROM tblProductBrand WHERE Status=0 ORDER BY BrandName";
                                                            $RSBrand = $DB->query($sqlBrand);
                                                            ?>
                                                            <div class="col-sm-4">
                                                                <select class="form-control required"  name="brand" id="brand">
                                                                    <option value="" selected>All</option>
                                                                    <?php
                                                                    if ($RSBrand->num_rows > 0) {
                                                                        while ($rowBrand = $RSBrand->fetch_assoc()) {
                                                                            $strBrandName = $rowBrand["BrandName"];
                                                                            $strBrandID = $rowBrand["BrandID"];
                                                                            ?>
                                                                            <option value="<?= $strBrandID ?>" <?php echo isset($_GET['brand']) && $_GET['brand'] == $strBrandID ? 'selected' : '' ?>><?= $strBrandName ?></option>														
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Report For</label>
                                                            <div class="col-sm-4">
                                                                <input type="radio" name="report_for" value="1" id="report_1" required="required" <?php echo isset($_GET['report_for']) && $_GET['report_for'] == 1 ? 'checked' : '' ?>><label for="report_1">Service</label>&nbsp;&nbsp;&nbsp;
                                                                <input type="radio" name="report_for" value="2" id="report_2" required="required" <?php echo isset($_GET['report_for']) && $_GET['report_for'] == 2 ? 'checked' : '' ?>><label for="report_2">Retail</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                            <?php if (isset($_GET['report_for']) && $_GET['report_for'] == 1) { ?>
                                                                <a onclick="download_csv_service()" class="btn btn-alt btn-hover btn-primary">Download CSV</a> 

                                                            <?php } else if (isset($_GET['report_for']) && $_GET['report_for'] == 2) { ?> 
                                                                <a onclick="download_csv_retail()" class="btn btn-alt btn-hover btn-primary">Download CSV</a> 

                                                            <?php } ?>
                                                            <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                                            &nbsp;&nbsp;&nbsp;
                                                            <a class="btn btn-link" href="RetailProductReport.php">Clear All Filter</a>
                                                            &nbsp;&nbsp;&nbsp;
                                                        </div>
                                                    </form>

                                                    <br><br>
                                                    <div id="printdata">

                                                        <?php
                                                        $datedrom = $_GET["toandfrom"];
                                                        if ($datedrom != "") {
                                                            $store = $_GET["Store"];
                                                            $sep = select("StoreName", "tblStores", "StoreID='" . $store . "'");
                                                            $storename = $sep[0]['StoreName'];
                                                            ?>
                                                            <h3 class="title-hero">Date Range selected : FROM - <?= $getfrom ?> / TO - <?= $getto ?> / Store Filter selected : <?= $storename ?> </h3>
                                                            <br>
                                                            <?php
                                                            $DB = Connect();
                                                            $per = $_GET["per"];
                                                            ?>
                                                            <div class="panel">
                                                                <div class="panel-body">
                                                                    <div class="example-box-wrapper">
                                                                        <div class="scroll-columns">
                                                                            <?php if (isset($_GET["report_for"]) && $_GET["report_for"] == 2) { ?>
                                                                                <h3>Retail Product Report</h3>
                                                                                <table class="table table-striped table-bordered responsive no-wrap" cellspacing="0" border="1px">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th><center>Sr. No.</center></th>
                                                                                    <th><center>Product Code</center></th>
                                                                                    <th><center>Product Name</center></th>
                                                                                    <th><center>Brand</center></th>
                                                                                    <th><center>Current Stock</center></th>
                                                                                    <th><center>Count</center></th>
                                                                                    <th><center>Amount</center></th>
                                                                                    <th><center>Discount</center></th>
                                                                                    <th><center>Profitability</center></th>

                                                                                    </tr>

                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php
                                                                                        $DB = Connect();
                                                                                        $qry = "SELECT apr.*,ri.offer_discount FROM tblAppointments apt JOIN appointment_retail_product apr ON(apt.AppointmentID = apr.appointment_id)"
                                                                                                . " JOIN retailinvoicedetails ri ON(ri.AppointmentId = apt.AppointmentID)"
                                                                                                . " WHERE apt.appointment_type=2 AND apt.IsDeleted != '1'  AND apt.FreeService !=  '1'  AND apt.Status = '2' " . $append_qry;
                                                                                        $qry_exe = $DB->query($qry);
                                                                                        if ($qry_exe->num_rows > 0) {
                                                                                            while ($rowData = $qry_exe->fetch_assoc()) {
                                                                                                $result_data[] = $rowData;
                                                                                            }
                                                                                        }

                                                                                        if (isset($result_data) && is_array($result_data) && count($result_data) > 0) {
                                                                                            foreach ($result_data as $rkey => $rvalue) {
                                                                                                $pro_id_arr[$rvalue['product_id']] = $rvalue['product_id'];
                                                                                                $pdt_discount = $rvalue['amount'] - $rvalue['amount_ded_discount'];
                                                                                                if (isset($product_res[$rvalue['product_id']])) {
                                                                                                    $product_res[$rvalue['product_id']]['count'] += 1;
                                                                                                    $product_res[$rvalue['product_id']]['amount'] += $rvalue['final_amount'];
                                                                                                    $product_res[$rvalue['product_id']]['offer_discount'] += $pdt_discount;
                                                                                                } else {
                                                                                                    $product_res[$rvalue['product_id']]['count'] = 1;
                                                                                                    $product_res[$rvalue['product_id']]['amount'] = $rvalue['final_amount'];
                                                                                                    $product_res[$rvalue['product_id']]['offer_discount'] = $pdt_discount;
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        /*
                                                                                         * Get Product Stock
                                                                                         */
                                                                                        if (isset($pro_id_arr) && is_array($pro_id_arr) && count($pro_id_arr) > 0) {
                                                                                            $pro_in_ids = implode(',', $pro_id_arr);
                                                                                            $inventory_qry = '';
                                                                                            if ($pro_in_ids != '') {
                                                                                                if (!empty($_GET["Store"])) {
                                                                                                    $inventory_qry .= " AND StoreID='" . $_GET["Store"] . "'";
                                                                                                }
                                                                                                $inv_qry = "SELECT * FROM product_inventory WHERE ProductID IN(" . $pro_in_ids . ") AND status=1 " . $inventory_qry;
                                                                                                $inv_qry_exe = $DB->query($inv_qry);
                                                                                                if ($inv_qry_exe->num_rows > 0) {
                                                                                                    while ($invData = $inv_qry_exe->fetch_assoc()) {
                                                                                                        $inventory_data[$invData['ProductID']][] = $invData['Stock'];
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        $counter = 1;
                                                                                        $retail_count = 0;
                                                                                        $retail_amount = 0;
                                                                                        $retail_discount = 0;
                                                                                        $total_profit = 0;
                                                                                        if (isset($product_res) && is_array($product_res) && count($product_res) > 0) {
                                                                                            ?>
                                                                                            <?php foreach ($product_res as $rkey => $rvalue) {
                                                                                                ?>
                                                                                                <tr>
                                                                                                    <td><center><?php echo $counter; ?></center></td>
                                                                                            <td><center><?php echo isset($product_name_data[$rkey]) ? $product_name_data[$rkey]['code'] : '' ?></center></td>
                                                                                            <td><center><?php echo isset($product_name_data[$rkey]) ? $product_name_data[$rkey]['name'] : '' ?></center></td>
                                                                                            <td><center><?php echo isset($product_brand_id[$rkey]) ? implode(",", $product_brand_id[$rkey]) : '' ?></center></td>
                                                                                            <td><center><?php echo isset($inventory_data[$rkey]) ? array_sum($inventory_data[$rkey]) : '' ?></center></td>
                                                                                            <td><center><?php
                                                                                                echo $rvalue['count'];
                                                                                                $retail_count += $rvalue['count'];
                                                                                                ?></center></td>
                                                                                            <td><center><?php
                                                                                                echo $rvalue['amount'];
                                                                                                $retail_amount += $rvalue['amount'];
                                                                                                ?></center></td>

                                                                                            <td><center><?php
                                                                                                echo $rvalue['offer_discount'];
                                                                                                $retail_discount += $rvalue['offer_discount'];
                                                                                                ?></center></td>
                                                                                            <td><center>
                                                                                                <?php
                                                                                                $mrp_amt = isset($product_name_data[$rkey]['mrp']) ? $product_name_data[$rkey]['mrp'] : 0;
                                                                                                $sale_amt = $rvalue['amount'];
                                                                                                $qty = $rvalue['count'];
                                                                                                $purchase_amt = $mrp_amt * $qty;
                                                                                                $profit = $sale_amt - $purchase_amt;
                                                                                                $total_profit += $profit;
                                                                                                echo $profit;
                                                                                                ?>
                                                                                            </center></td>
                                                                                            </tr>
                                                                                            <?php
                                                                                            $retail_data[] = array(
                                                                                                'p_code' => $product_name_data[$rkey]['code'],
                                                                                                'p_name' => $product_name_data[$rkey]['name'],
                                                                                                'p_brand' => isset($product_brand_id[$rkey]) ? implode(",", $product_brand_id[$rkey]) : '',
                                                                                                'stock' => isset($inventory_data[$rkey]) ? array_sum($inventory_data[$rkey]) : '',
                                                                                                'count' => $rvalue['count'],
                                                                                                'amount' => $rvalue['amount'],
                                                                                                'discount' => $rvalue['offer_discount'],
                                                                                                'profit' => $profit,
                                                                                            );
                                                                                            $counter++;
                                                                                        }
                                                                                        ?>
                                                                                        <tr>
                                                                                            <td colspan="3"><center><b>Total</b></center></td>

                                                                                        <td><center></center></td>
                                                                                        <td><center></center></td>
                                                                                        <td><center><?php echo $retail_count; ?></center></td>
                                                                                        <td><center><?php echo round($retail_amount, 2); ?></center></td>
                                                                                        <td><center><?php echo $retail_discount; ?></center></td>
                                                                                        <td><center><?php echo $total_profit; ?></center></td>
                                                                                        </tr>
                                                                                        <?php
                                                                                        $retail_total_data = array(
                                                                                            'p_code' => '',
                                                                                            'p_name' => '',
                                                                                            'p_brand' => '',
                                                                                            'stock' => '',
                                                                                            'count' => $retail_count,
                                                                                            'amount' => round($retail_amount, 2),
                                                                                            'discount' => $retail_discount,
                                                                                            'profit' => $total_profit,
                                                                                        );
                                                                                    }
                                                                                    ?>
                                                                                    </tbody>
                                                                                </table>

                                                                            <?php } ?>

                                                                            <?php if (isset($_GET["report_for"]) && $_GET["report_for"] == 1) { ?>
                                                                                <div class="example-box-wrapper">
                                                                                    <h3>Service Product Report</h3>
                                                                                    <table class="table table-bordered table-striped table-condensed cf">
                                                                                        <thead>
                                                                                            <tr>
                                                                                                <th style="text-align:center">Sr. No.</th>
                                                                                                <th style="text-align:center">Product Code</th>
                                                                                                <th style="text-align:center">Product Name</th>
                                                                                                <th style="text-align:center">Brand</th>
                                                                                                <th style="text-align:center">Current Stock</th>
                                                                                                <th style="text-align:center">Product Qty Used</th>
                                                                                                <th style="text-align:center">Services Done</th>
                                                                                                <th style="text-align:center">Product Cost</th>
                                                                                            </tr>
                                                                                        </thead>

                                                                                        <tbody>

                                                                                            <?php
                                                                                            $DB = Connect();
                                                                                            $storrr = $_GET["Store"];
                                                                                            /*
                                                                                             * query used to get store ids
                                                                                             */
                                                                                            if (!empty($storrr)) {
                                                                                                $sql = "select group_concat(distinct tblAppointments.StoreID) as g_store from  tblAppointments join tblInvoiceDetails on tblInvoiceDetails.AppointmentId=tblAppointments.AppointmentID WHERE tblAppointments.StoreID='" . $storrr . "'  AND tblAppointments.IsDeleted !=  '1' AND tblAppointments.FreeService !=  '1' and tblAppointments.Status='2' $sqlTempfrom $sqlTempto ";
                                                                                            } else {
                                                                                                $sql = "select group_concat(distinct tblAppointments.StoreID) as g_store from  tblAppointments join tblInvoiceDetails on tblInvoiceDetails.AppointmentId=tblAppointments.AppointmentID WHERE tblAppointments.IsDeleted !=  '1' AND tblAppointments.FreeService !=  '1' and tblAppointments.Status='2' $sqlTempfrom $sqlTempto ";
                                                                                            }

                                                                                            $get_Store = $DB->query($sql);
                                                                                            if ($get_Store->num_rows > 0) {
                                                                                                while ($row_data = $get_Store->fetch_assoc()) {
                                                                                                    $group_store['store_ids'] = $row_data['g_store'];
                                                                                                }
                                                                                            }



                                                                                            $RS = $DB->query($sql);
                                                                                            if (isset($group_store['store_ids']) && $group_store['store_ids'] != '') {
                                                                                                /*
                                                                                                 * get store product
                                                                                                 */
                                                                                                // $store_pdtq = "SELECT DISTINCT ProductID FROM `tblProductsServices` WHERE `StoreID` IN (1,3,4,5,108)";
                                                                                                $store_pdtq = "SELECT DISTINCT ProductID FROM `tblProductsServices`";
                                                                                                $store_pdtq_exe = $DB->query($store_pdtq);
                                                                                                if ($store_pdtq_exe->num_rows > 0) {
                                                                                                    while ($row_data = $store_pdtq_exe->fetch_assoc()) {
                                                                                                        $result[] = $row_data;
                                                                                                        $product_ids[$row_data['ProductID']] = $row_data['ProductID'];
                                                                                                    }
                                                                                                }

                                                                                                /*
                                                                                                 * get product data
                                                                                                 */
                                                                                                $pro_append_qry = '';
                                                                                                if (!empty($_GET["brand"])) {
                                                                                                    $all_brand_pid = isset($brand_info[$_GET["brand"]]) ? $brand_info[$_GET["brand"]] : array();
                                                                                                    if (isset($all_brand_pid) && is_array($all_brand_pid) && count($all_brand_pid) > 0) {
                                                                                                        $pid = implode(",", $all_brand_pid);
                                                                                                        if ($pid != '') {
                                                                                                            $pro_append_qry .= " AND ProductID IN(" . $pid . ")";
                                                                                                        }
                                                                                                    }
                                                                                                }

                                                                                                if (isset($product_ids) && is_array($product_ids) && count($product_ids) > 0) {
                                                                                                    $product_in_ids = implode(",", $product_ids);

                                                                                                    /*
                                                                                                     * get product service ids
                                                                                                     */
                                                                                                    if (!empty($storrr)) {
                                                                                                        $store_append = " AND tblProductsServices.StoreID ='" . $storrr . "'";
                                                                                                    } else {
                                                                                                        $store_append = '';
                                                                                                    }
                                                                                                    $pdt_services = "SELECT tblProductsServices.ServiceID,tblProductsServices.ProductID FROM `tblProductsServices` WHERE ProductID IN(" . $product_in_ids . ") " . $pro_append_qry . $store_append;
                                                                                                    $pdt_services_exe = $DB->query($pdt_services);
                                                                                                    while ($pdtservices_data = $pdt_services_exe->fetch_assoc()) {
                                                                                                        $all_pdt_services[] = $pdtservices_data;
                                                                                                    }

                                                                                                    if (isset($all_pdt_services) && is_array($all_pdt_services) && count($all_pdt_services) > 0) {
                                                                                                        foreach ($all_pdt_services as $pdtskey => $pdtsvalue) {
                                                                                                            $pdt_services_res[$pdtsvalue['ProductID']][] = $pdtsvalue['ServiceID'];
                                                                                                            $service_ids_arr[$pdtsvalue['ServiceID']] = $pdtsvalue['ServiceID'];
                                                                                                        }
                                                                                                    }


                                                                                                    $pdt_q = "SELECT * FROM tblNewProducts WHERE ProductID IN(" . $product_in_ids . ")";
                                                                                                    $pdt_exe = $DB->query($pdt_q);
                                                                                                    while ($pdtdetails = $pdt_exe->fetch_assoc()) {
                                                                                                        $all_pdt[] = $pdtdetails;
                                                                                                    }

                                                                                                    if (isset($all_pdt) && is_array($all_pdt) && count($all_pdt) > 0) {
                                                                                                        foreach ($all_pdt as $pdtkey => $pdtvalue) {
                                                                                                            $pdt_data[$pdtvalue['ProductID']] = $pdtvalue;
                                                                                                        }
                                                                                                    }
                                                                                                }

                                                                                                /*
                                                                                                 * Get Product Stock
                                                                                                 */
                                                                                                $pdt_stock = "SELECT DISTINCT ProductID,ProductStockID,StoreID,Stock FROM tblStoreProduct WHERE ProductID IN(" . $product_in_ids . ")";
                                                                                                $pdt_stock_exe = $DB->query($pdt_stock);
                                                                                                while ($stock_res = $pdt_stock_exe->fetch_assoc()) {
                                                                                                    $stock_detail[$stock_res['ProductID']][$stock_res['StoreID']] = $stock_res['Stock'];
                                                                                                }

                                                                                                /*
                                                                                                 * get service qty and amount
                                                                                                 */
                                                                                                if (isset($service_ids_arr) && is_array($service_ids_arr) && count($service_ids_arr) > 0) {
                                                                                                    $service_in_ids = implode(",", $service_ids_arr);
                                                                                                    if (!empty($storrr)) {
                                                                                                        $qty_store_append = " AND tblAppointments.StoreID ='" . $storrr . "'";
                                                                                                    } else {
                                                                                                        $qty_store_append = '';
                                                                                                    }
                                                                                                    $qty_amtq = "SELECT tblInvoiceDetails.DisAmt,tblInvoiceDetails.OfferAmt,tblAppointmentsDetailsInvoice.ServiceID,tblAppointmentsDetailsInvoice.qty,tblAppointmentsDetailsInvoice.ServiceAmount FROM tblAppointmentsDetailsInvoice"
                                                                                                            . " JOIN tblAppointments on tblAppointmentsDetailsInvoice.AppointmentID=tblAppointments.AppointmentID"
                                                                                                            . " JOIN tblInvoiceDetails on tblInvoiceDetails.AppointmentId=tblAppointments.AppointmentID"
                                                                                                            . " WHERE tblAppointmentsDetailsInvoice.ServiceID!='NULL' AND tblAppointmentsDetailsInvoice.ServiceID!=''"
                                                                                                            . " AND tblAppointments.IsDeleted !=  '1' and tblAppointments.Status='2' AND tblAppointments.FreeService !=  '1' "
                                                                                                            . " AND tblAppointmentsDetailsInvoice.ServiceID IN (" . $service_in_ids . ")" . $qty_store_append . $sqlTempfrom . $sqlTempto;
                                                                                                    $qty_amtq_exe = $DB->query($qty_amtq);

                                                                                                    while ($qtyservices_data = $qty_amtq_exe->fetch_assoc()) {
                                                                                                        $all_pdt_services_qty_amt[] = $qtyservices_data;
                                                                                                    }




                                                                                                    if (isset($all_pdt_services_qty_amt) && is_array($all_pdt_services_qty_amt) && count($all_pdt_services_qty_amt) > 0) {
                                                                                                        foreach ($all_pdt_services_qty_amt as $qtykey => $qtyvalue) {
                                                                                                            $ser_offer_amount = 0;
                                                                                                            $OfferAmt = trim($qtyvalue['OfferAmt'], ' - ');
                                                                                                            if ($OfferAmt > 0) {
                                                                                                                $ser_offer_amount = $OfferAmt;
                                                                                                            }

                                                                                                            $dis_amt = 0;
                                                                                                            $DisAmt = explode(",", $qtyvalue['DisAmt']);
                                                                                                            if (isset($DisAmt) && is_array($DisAmt) && count($DisAmt) > 0) {
                                                                                                                foreach ($DisAmt as $key => $value) {
                                                                                                                    if (trim($value) > 0) {
                                                                                                                        $dis_amt += $value;
                                                                                                                    }
                                                                                                                }
                                                                                                            }

                                                                                                            $pdtservice_qty_res[$qtyvalue['ServiceID']][] = array(
                                                                                                                'qty' => $qtyvalue['qty'],
                                                                                                                'ServiceAmount' => $qtyvalue['ServiceAmount'],
                                                                                                                'membership_dis' => $dis_amt,
                                                                                                                'offer_dis' => $ser_offer_amount
                                                                                                            );
                                                                                                        }
                                                                                                    }
                                                                                                }


                                                                                                $counter = 1;
                                                                                                $total_avail_stock = 0;
                                                                                                foreach ($result as $key => $row) {


                                                                                                    $ProductID = $row["ProductID"];
                                                                                                    //$sep = select("count(*)", "tblNewProducts", "ProductID='" . $ProductID . "'");
                                                                                                    //$cntserr = $sep[0]['count(*)'];
                                                                                                    $cntserr = isset($pdt_data[$ProductID]) ? $pdt_data[$ProductID] : array();
                                                                                                    if (count($cntserr) > 0) {
                                                                                                        //$sept = select("*", "tblNewProducts", "ProductID='" . $ProductID . "'");
                                                                                                        $ProductIDT = $cntserr['ProductID'];
                                                                                                        $productname = $cntserr['ProductName'];
                                                                                                        $productcode = $cntserr['ProductUniqueCode'];
                                                                                                        $PerQtyServe = $cntserr['PerQtyServe'];
                                                                                                        $ProductMRP = $cntserr['ProductMRP'];
                                                                                                        $total_discount = 0;
                                                                                                        //$sepa = select("*", "tblStores", "StoreID='" . $storrr . "'");
                                                                                                        //$storename = $sepa[0]['StoreName'];
                                                                                                        // $sqlt=selectproductservice($storrr,$getfrom,$getto,$ProductIDT);
                                                                                                        //$sqlt = "select distinct(tblProductsServices.ServiceID) from tblProductsServices left join tblAppointments on tblProductsServices.StoreID=tblAppointments.StoreID right join tblInvoiceDetails on tblInvoiceDetails.AppointmentId=tblAppointments.AppointmentID left join tblNewProducts on tblNewProducts.ProductID=tblProductsServices.ProductID WHERE tblAppointments.StoreID='" . $storrr . "' AND tblProductsServices.StoreID='" . $storrr . "'  AND tblAppointments.IsDeleted !=  '1' AND tblAppointments.FreeService !=  '1' and tblAppointments.Status='2' $sqlTempfrom $sqlTempto  AND tblProductsServices.StoreID='" . $storrr . "' and tblProductsServices.ProductID='" . $ProductIDT . "'";


                                                                                                        $product_mul_service = isset($pdt_services_res[$ProductIDT]) ? $pdt_services_res[$ProductIDT] : array();
                                                                                                        //$RSt = $DB->query($sqlt);
                                                                                                        if (isset($product_mul_service) && is_array($product_mul_service) && count($product_mul_service) > 0) {


                                                                                                            foreach ($product_mul_service as $mulserkey => $row1) {


                                                                                                                $servicedt = $row1;
                                                                                                                // $stppsertyptup=selectproductservicedetail($storrr,$getfrom,$getto,$servicedt);
                                                                                                                //$stppsertyptup = select("tblAppointmentsDetailsInvoice.qty,tblAppointmentsDetailsInvoice.ServiceAmount", "tblAppointmentsDetailsInvoice left join tblAppointments on tblAppointmentsDetailsInvoice.AppointmentID=tblAppointments.AppointmentID right join tblInvoiceDetails on tblInvoiceDetails.AppointmentId=tblAppointments.AppointmentID", "tblAppointmentsDetailsInvoice.ServiceID!='NULL' AND tblAppointmentsDetailsInvoice.ServiceID!='' and tblAppointments.StoreID='" . $storrr . "' and tblAppointmentsDetailsInvoice.ServiceID='" . $servicedt . "' AND tblAppointments.IsDeleted !=  '1' and tblAppointments.Status='2' AND tblAppointments.FreeService !=  '1' $sqlTempfrom $sqlTempto ");
                                                                                                                //exit;
                                                                                                                $stppsertyptup = isset($pdtservice_qty_res[$servicedt]) ? $pdtservice_qty_res[$servicedt] : array();

                                                                                                                foreach ($stppsertyptup as $tr) {
                                                                                                                    $ser_mem_amount = 0;
                                                                                                                    $ser_offer_amount = 0;
                                                                                                                    $qty = $tr['qty'];
                                                                                                                    $ServiceAmount = $tr['ServiceAmount'];
                                                                                                                    if (isset($tr['membership_dis']) && $tr['membership_dis'] > 0) {
                                                                                                                        $ser_mem_amount = $tr['membership_dis'];
                                                                                                                    }

                                                                                                                    if (isset($tr['offer_dis']) && $tr['offer_dis'] > 0) {
                                                                                                                        $ser_offer_amount = $tr['offer_dis'];
                                                                                                                    }
                                                                                                                    $qttyt +=$qty;
                                                                                                                    $strServiceAmount = ($ServiceAmount * $qty) - $ser_mem_amount - $ser_offer_amount;
                                                                                                                    $totalstrServiceAmount = $totalstrServiceAmount + $strServiceAmount;
                                                                                                                    $total_discount += $ser_mem_amount + $ser_mem_amount;
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                        //$productcost = $ProductMRP * $qttyt;
                                                                                                        $pcost = $ProductMRP / $PerQtyServe;
                                                                                                        $productcost = round($pcost, 2) * $qttyt;

                                                                                                        $ProductQtyUsed = $PerQtyServe * $qttyt;
                                                                                                        if ($qttyt == "") {
                                                                                                            $qttyt = 0;
                                                                                                        }
                                                                                                        $consumperformance = $PerQtyServe - $qttyt;
                                                                                                        $profit = $productcost - $totalstrServiceAmount;


                                                                                                        if ($ProductQtyUsed == "") {
                                                                                                            $ProductQtyUsed = "0.00";
                                                                                                        } else {

                                                                                                            $ProductQtyUsed = $ProductQtyUsed;
                                                                                                        }
                                                                                                        $TotalProductQtyUsed += $ProductQtyUsed;
                                                                                                        if ($qttyt == "") {
                                                                                                            $qttyt = "0.00";
                                                                                                        } else {

                                                                                                            $qttyt = $qttyt;
                                                                                                        }
                                                                                                        $Totalqttyt += $qttyt;
                                                                                                        if ($PerQtyServe == "") {
                                                                                                            $PerQtyServe = "0.00";
                                                                                                        } else {

                                                                                                            $PerQtyServe = $PerQtyServe;
                                                                                                        }
                                                                                                        if (isset($qttyt) && $qttyt > 0) {
                                                                                                            $TotalPerQtyServe += $PerQtyServe;
                                                                                                        }
                                                                                                        if ($consumperformance == "") {
                                                                                                            $consumperformance = "0.00";
                                                                                                        } else {

                                                                                                            $consumperformance = $consumperformance;
                                                                                                        }
                                                                                                        $Totalconsumperformance += $consumperformance;
                                                                                                        if ($totalstrServiceAmount == "") {
                                                                                                            $totalstrServiceAmount = "0.00";
                                                                                                        } else {

                                                                                                            $totalstrServiceAmount = $totalstrServiceAmount;
                                                                                                        }
                                                                                                        $TotaltotalstrServiceAmount += $totalstrServiceAmount;
                                                                                                        if ($productcost == "") {
                                                                                                            $productcost = "0.00";
                                                                                                        } else {

                                                                                                            $productcost = $productcost;
                                                                                                        }
                                                                                                        $Totalproductcost += $productcost;
                                                                                                        if ($profit == "") {
                                                                                                            $profit = "0.00";
                                                                                                        } else {

                                                                                                            $profit = $profit;
                                                                                                        }
                                                                                                        $Totalprofit += $profit;
                                                                                                        ?>														

                                                                                                        <?php if (isset($qttyt) && $qttyt > 0) { ?>
                                                                                                            <tr id="my_data_tr_<?= $counter ?>">
                                                                                                                <td><center><?= $counter; ?></center></td>
                                                                                                        <td><center><?= $productcode ?></center></td>
                                                                                                        <td><center><?= $productname ?></center></td>
                                                                                                        <td><center><?php echo isset($product_brand_id[$cntserr['ProductID']]) ? implode(",", $product_brand_id[$cntserr['ProductID']]) : '' ?></center></td>
                                                                                                        <td><center><?php echo isset($inventory_data[$cntserr['ProductID']]) ? array_sum($inventory_data[$cntserr['ProductID']]) : '' ?></center></td>
                                                                                                        <td><center><?= ceil($ProductQtyUsed) ?></center></td>
                                                                                                        <td><center><?= $qttyt ?></center></td>
                                                                                                        <td><center><?= $productcost ?></center></td>

                                                                                                        </tr>
                                                                                                        <?php
                                                                                                        $service_data[] = array(
                                                                                                            'p_code' => $productcode,
                                                                                                            'p_name' => $productname,
                                                                                                            'p_brand' => isset($product_brand_id[$cntserr['ProductID']]) ? implode(",", $product_brand_id[$cntserr['ProductID']]) : '',
                                                                                                            'stock' => isset($inventory_data[$cntserr['ProductID']]) ? array_sum($inventory_data[$cntserr['ProductID']]) : '',
                                                                                                            'qty_used' => ceil($ProductQtyUsed),
                                                                                                            'count' => $qttyt,
                                                                                                            'amount' => $productcost
                                                                                                        );
                                                                                                        $counter++;
                                                                                                    }
                                                                                                    ?>
                                                                                                    <?php
                                                                                                }
                                                                                                $qttyt = "";
                                                                                                $PerQtyServe = "";
                                                                                                $ProductQtyUsed = "";
                                                                                                $consumperformance = "";
                                                                                                $totalstrServiceAmount = "";
                                                                                                $profit = "";
                                                                                                $productcost = "";
                                                                                            }
                                                                                            $stocksm = "";
                                                                                        } else {
                                                                                            ?>															
                                                                                            <tr>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td>No Records Found</td>
                                                                                                <?php /* <td></td>
                                                                                                  <td></td> */ ?>
                                                                                                <td></td>
                                                                                                <?php /* <td></td> */ ?>

                                                                                            </tr>


                                                                                            <?php
                                                                                        }


                                                                                        $DB->close();
                                                                                        // $time_elapsed_secs = microtime(true) - $start;
                                                                                        //echo 'Time elapsed=' . $time_elapsed_secs / 1000;
                                                                                        ?>

                                                                                        </tbody>

                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td colspan="5"><center><b>Total</b></center></td>
                                                                                        <td class="numeric"><center><?= round($TotalProductQtyUsed, 2) ?></center></td>
                                                                                        <td class="numeric"><center><?= $Totalqttyt ?></center></td>
                                                                                        <td class="numeric"><center><?= $Totalproductcost ?></center></td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>

                                                                                </div>
                                                                                <?php
                                                                                $service_total_data = array(
                                                                                    'p_code' => 'Total',
                                                                                    'p_name' => '',
                                                                                    'p_brand' => '',
                                                                                    'stock' => '',
                                                                                    'qty_used' => round($TotalProductQtyUsed, 2),
                                                                                    'count' => $Totalqttyt,
                                                                                    'amount' => $Totalproductcost
                                                                                );
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                } else {
                                                    echo "<br><center><h3>Please Select Month And Year!</h3></center>";
                                                }
                                                ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php require_once 'incFooter.fya'; ?>
            </div>
    </body>
</html>


<script>
    var data = [
<?php
if (isset($retail_data) && is_array($retail_data) && count($retail_data)) {
    foreach ($retail_data as $key => $value) {
        echo "['" . $value['p_code'] . "','" . $value['p_name'] . "','" . $value['p_brand'] . "','" . $value['stock']
        . "','" . $value['count'] . "','" . $value['amount'] . "','" . $value['discount'] . "','" . $value['profit'] . "'],";
        ?>
        <?php
    }
}

if (isset($retail_total_data) && is_array($retail_total_data) && count($retail_total_data) > 0) {
    echo "['" . $retail_total_data['p_code'] . "','" . $retail_total_data['p_name'] . "','" . $retail_total_data['p_brand'] . "','" . $retail_total_data['stock']
    . "','" . $retail_total_data['count'] . "','" . $retail_total_data['amount'] . "','" . $retail_total_data['discount'] . "','" . $retail_total_data['profit'] . "'],";
}
?>

    ];


    function download_csv_retail() {
        var csv = 'Product Code,Product Name,Brand,Current Stock,Count,Amount,Discount,Profitability\n';
        data.forEach(function (row) {
            csv += row.join(',');
            csv += "\n";
        });

        console.log(csv);
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        hiddenElement.target = '_blank';
        hiddenElement.download = 'retail_report.csv';
        hiddenElement.click();
    }


    /*
     * Service Report
     */
    var service_data = [
<?php
if (isset($service_data) && is_array($service_data) && count($service_data)) {
    foreach ($service_data as $key => $value) {
        echo "['" . $value['p_code'] . "','" . $value['p_name'] . "','" . $value['p_brand'] . "','" . $value['stock']
        . "','" . $value['qty_used'] . "','" . $value['count'] . "','" . $value['amount'] . "'],";
        ?>
        <?php
    }
}

if (isset($service_total_data) && is_array($service_total_data) && count($service_total_data) > 0) {
    echo "['" . $service_total_data['p_code'] . "','" . $service_total_data['p_name'] . "','" . $service_total_data['p_brand'] . "','" . $service_total_data['stock']
    . "','" . $service_total_data['qty_used'] . "','" . $service_total_data['count'] . "','" . $service_total_data['amount'] . "'],";
}
?>

    ];


    function download_csv_service() {
        var csv = 'Product Code,Product Name,Brand,Current Stock,Product Qty Used,Services Done	,Product Cost\n';
        service_data.forEach(function (row) {
            csv += row.join(',');
            csv += "\n";
        });

        console.log(csv);
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        hiddenElement.target = '_blank';
        hiddenElement.download = 'service_report.csv';
        hiddenElement.click();
    }
</script>
