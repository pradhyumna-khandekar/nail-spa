<?php require_once 'setting.fya'; ?>
<?php require_once 'incFirewall.fya'; ?>
<?php

$DB = Connect();

if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
    if (isset($_POST['from_store']) && isset($_POST['emp_id'])) {
        $from_store = $_POST['from_store'];
        $emp_ids = $_POST['emp_id'];
        $to_store = $strStore;
        $request_by = $strAdminID;
        $request_date = date('Y-m-d H:i:s');
        $cols_arr = array('request_date', 'request_status', 'from_store', 'to_store', 'emp_id', 'request_by');

        if (isset($emp_ids) && is_array($emp_ids) && count($emp_ids) > 0) {
            foreach ($emp_ids as $key => $value) {
                $row_values = array("'" . $request_date . "'", "'1'", "'" . $from_store . "'", "'" . $to_store . "'", "'" . $value . "'", "'" . $request_by . "'");
                $insertQry = "INSERT INTO emp_transfer_request (" . implode(",", $cols_arr) . ") VALUES (" . implode(",", $row_values) . ")";
                $DB->query($insertQry);
            }
        }
        header('Location:transfer_request_form.php');
    } else if (isset($_POST['type']) && $_POST['type'] == 2) {
        $transfer_id = $_POST['transfer_id'];
        if ($transfer_id != '') {
            /*
             * Get Transfer Data
             */
            $transfer_data = select("*", "emp_transfer_request", "id = '" . $transfer_id . "'");
            if (isset($transfer_data) && is_array($transfer_data) && count($transfer_data) > 0) {
                /*
                 * Update Employee Table
                 */

                $updateEmpQry = "UPDATE tblEmployees SET TransferDate='" . date('Y-m-d') . "'"
                        . ",TransferedFrom='" . $transfer_data[0]['from_store'] . "',StoreID='" . $transfer_data[0]['to_store'] . "'"
                        . "  WHERE EID='" . $transfer_data[0]['emp_id'] . "'";
                $DB->query($updateEmpQry);

                /*
                 * Insert Transfer Log
                 */
                $logInsertQry = "INSERT INTO tblEmployeeTransferLog(EID, TransferDate,TransferStoreID) "
                        . " values ('" . $transfer_data[0]['emp_id'] . "', '" . date('Y-m-d') . "', '" . $transfer_data[0]['to_store'] . "')";
                $DB->query($logInsertQry);

                /*
                 * Update Request Table
                 */
                $updateQry = "UPDATE emp_transfer_request SET request_status='2',approved_date='" . date('Y-m-d H:i:s') . "'"
                        . ",approved_by='" . $strAdminID . "' WHERE id='" . $transfer_id . "'";
                $DB->query($updateQry);
                echo 1;
            } else {
                echo 2;
            }
        }
    } else if (isset($_POST['type']) && $_POST['type'] == 3) {
        $transfer_id = $_POST['transfer_id'];
        if ($transfer_id != '') {

            /*
             * Update Request Table
             */
            $updateQry = "UPDATE emp_transfer_request SET request_status='3',approved_date='" . date('Y-m-d H:i:s') . "'"
                    . ",approved_by='" . $strAdminID . "' WHERE id='" . $transfer_id . "'";
            $DB->query($updateQry);
            echo 1;
        } else {
            echo 2;
        }
    } else if (isset($_POST['type']) && $_POST['type'] == 4) {
        $transfer_id = $_POST['transfer_id'];
        if ($transfer_id != '') {

            /*
             * Update Request Table
             */
            $updateQry = "UPDATE emp_transfer_request SET status='2' WHERE id='" . $transfer_id . "'";
            $DB->query($updateQry);
            echo 1;
        } else {
            echo 2;
        }
    }
} else {
    echo 2;
}
$DB->close();
?>