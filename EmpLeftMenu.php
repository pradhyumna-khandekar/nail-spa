<?php require_once("setting.fya"); ?>
<?php require_once 'incEmpLoginData.php'; ?>
<?php
$menu = array();
?>
<?php
if ($_SERVER['REMOTE_ADDR'] == "111.119.219.70") {
    $strlogo = "logo-admin.png";
} else {
    $strlogo = "logo-admin.png";
}
?>
<div id="page-sidebar">
    <div id="header-logo" class="logo-bg">
        <a href="Dashboard.php" class="logo-content-big" style="background: url(../admin/assets/image-resources/<?= $strlogo ?>) left 50% no-repeat;" title="Nailspa POS ">NailSpa<span>POS App</span></a> <a href="Dashboard.php" class="logo-content-small" style="background: url(../admin/assets/image-resources/<?= $strlogo ?>) left 50% no-repeat;" title="NailSpa">NailSpa <span>POS App</span></a> <a id="close-sidebar" href="#" title="Close sidebar"><i class="glyph-icon icon-outdent"></i></a>
    </div>  



    <div class="scroll-sidebar" >
        <ul id="sidebar-menu">
            <li><a href="DisplayReconcillationDetail.php" title="Reconcillation Report"><i class="glyph-icon icon-linecons-tv"></i> <span>Reconcillation Report</span></a></li>
            <li><a href="emp_attendance.php" title="Employees Attendance"><i class="glyph-icon icon-linecons-user"></i> <span>Employees Attendance</span></a></li>
            <li><a href="emplogout.php" title="Logout"><i class="glyph-icon icon-power-off"></i> <span>Logout</span></a></li>
        </ul>
    </div>


</div>