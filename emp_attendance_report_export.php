<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php

if (isset($_POST)) {

    $date = $_POST['date'];
    $store = $_POST['store'];
    $arraytofrom = explode("-", $date);

    $from = $arraytofrom[0];
    $datetime = new DateTime($from);
    $getfrom = $datetime->format('Y-m-d');

    $to = $arraytofrom[1];
    $datetime = new DateTime($to);
    $getto = $datetime->format('Y-m-d');

    $period = new DatePeriod(
            new DateTime($getfrom), new DateInterval('P1D'), new DateTime($getto)
    );


    foreach ($period as $key => $value) {
        $all_dates[] = $value->format('Y-m-d');
    }

    $all_dates[] = $getto;

    if (!IsNull($from)) {
        $sqlTempfrom = " WHERE DateOfAttendance>=Date('" . $getfrom . "')";
    }

    if (!IsNull($to)) {
        $sqlTempto = " and DateOfAttendance<=Date('" . $getto . "')";
    }

    $selp = select("*", "tblStores", "Status = '0'");
    if (isset($selp) && is_array($selp) && count($selp) > 0) {
        foreach ($selp as $val) {
            $store_in_arr[$val["StoreID"]] = $val["StoreID"];
        }
    }

    $emp_append = '';
    if (isset($store_in_arr) && is_array($store_in_arr) && count($store_in_arr) > 0) {
        $emp_append = " AND StoreID IN(" . implode(",", $store_in_arr) . ")";
    }
    if (isset($store) && !empty($store) && $store != 0) {
        $emp_data = select("*", "tblEmployees", "StoreID='" . $store . "' AND Status=0 " . $emp_append . " ORDER BY StoreID");
    } else {
        $emp_data = select("*", "tblEmployees", "Status=0 " . $emp_append . " ORDER BY StoreID");
    }


    if (isset($emp_data) && is_array($emp_data) && count($emp_data) > 0) {
        foreach ($emp_data as $se_key => $se_value) {
            $store_emp_code[$se_value['EID']] = $se_value['EmployeeCode'];
        }
    }


    if (isset($store_emp_code) && is_array($store_emp_code) && count($store_emp_code) > 0) {
        $emp_code_in = "'" . implode("','", $store_emp_code) . "'";
        $appendq = " AND EmployeeCode IN(" . $emp_code_in . ")";
    }

    $DB = Connect();
    $attendq = "SELECT * FROM tblEmployeesRecords " . $sqlTempfrom . $sqlTempto . $appendq;

    $attendq_exe = $DB->query($attendq);


    while ($result = $attendq_exe->fetch_assoc()) {
        $attendance_data[$result['EmployeeCode']][$result['DateOfAttendance']] = $result;
    }

    $store_data = select("*", "tblStores", "StoreID>0");
    if (isset($store_data) && is_array($store_data) && count($store_data) > 0) {
        foreach ($store_data as $skey => $svalue) {
            $store_name_data[$svalue['StoreID']] = $svalue['StoreName'];
        }
    }

    /*
     * first row
     */
    $field1 = array('Employee', 'store', 'Total Fulldays', 'Total Halfdays', 'Absent Days');
    if (isset($all_dates) && is_array($all_dates) && count($all_dates) > 0) {
        foreach ($all_dates as $dkey => $dvalue) {
            $field2[] = date('d-m-Y', strtotime($dvalue));
        }
    }
    if (isset($_POST['report_type']) && $_POST['report_type'] == '2') {
        $fields = array_merge($field1, $field2);
    } else {
        $fields = array_merge($field1);
    }




//    echo "<pre>";
//    print_r($fields);

    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=Employee_attendance_report-' . date('Y-m-d') . '.csv');
    $output = fopen("php://output", "w");
    fputcsv($output, $fields);
    /*
     * data
     */
    if (isset($emp_data) && is_array($emp_data) && count($emp_data) > 0) {
        foreach ($emp_data as $ekey => $evalue) {
            $employee[] = 'Name:' . $evalue['EmployeeName'];

            $store_name = isset($store_name_data[$evalue['StoreID']]) ? $store_name_data[$evalue['StoreID']] : '';

            array_push($employee, $store_name);
            $fulldaycount = 0;
            $halfdaycount = 0;
            $absentcount = 0;
            if (isset($all_dates) && is_array($all_dates) && count($all_dates) > 0) {
                foreach ($all_dates as $dkey => $dvalue) {

                    $emp_att = isset($attendance_data[$evalue['EmployeeCode']][$dvalue]) ? $attendance_data[$evalue['EmployeeCode']][$dvalue] : array();
                    if (isset($emp_att['LoginTime']) && $emp_att['LoginTime'] != '00:00:00') {
                        $intime = $dvalue . " " . $emp_att['LoginTime'];
                        $outtime = $dvalue . " " . $emp_att['LogoutTime'];
                        $date = new DateTime($intime);
                        $date2 = new DateTime($outtime);
                        $diffInSeconds = $date2->getTimestamp() - $date->getTimestamp();

                        $logintimedata = $emp_att['LoginTime'] != '00:00:00' ? date('h:i-a', strtotime($emp_att['LoginTime'])) : 0;
                        $logouttimedata = $emp_att['LogoutTime'] != '00:00:00' ? date('h:i-a', strtotime($emp_att['LogoutTime'])) : 0;
                        $loginstore = isset($store_name_data[$emp_att['login_store_id']]) ? $store_name_data[$emp_att['login_store_id']] : '';
                        $logoutstore = isset($store_name_data[$emp_att['login_store_id']]) ? $store_name_data[$emp_att['login_store_id']] : '';
                        if ($emp_att['LoginTime'] != '00:00:00' && $emp_att['LogoutTime'] != '00:00:00') {
                            if ($diffInSeconds >= 25200) {
                                $workingtimedata = "Full Day";
                                $fulldaycount++;
                            } else {
                                $workingtimedata = "Half Day";
                                $halfdaycount++;
                            }
                        } else {
                            $workingtimedata = "Full Day";
                            $fulldaycount++;
                        }
                        $logdetails[] = 'logintime: ' . $logintimedata . ' | Logouttime: ' . $logouttimedata . ' | Loginstore: ' . $loginstore . ' | logoutstore: ' . $logoutstore . ' | Working time: ' . $workingtimedata;
                        //  $csvdata = array_merge($employee, $logdetails);
                        // unset($employee);
                        //unset($logdetails);
                    } else {

                        $logdetails[] = 'absent';
                        $absentcount++;

                        // unset($employee);
                        // unset($logdetails);
                    }


//                    unset($logdetails);
//                    echo "<pre>";
//                    print_r($csvdata);
//                    exit;
                }
                $total = array($fulldaycount, $halfdaycount, $absentcount);
            }

            if (isset($_POST['report_type']) && $_POST['report_type'] == '2') {
                $csvdata = array_merge($employee, $total, $logdetails);
            } else {
                $csvdata = array_merge($employee, $total);
            }


////            echo "<pre>";
////            print_r('f'.$fulldaycount);
////            echo "<pre>";
////            print_r('h'.$halfdaycount);
////            echo "<pre>";
////            print_r('a'.$absentcount);
////            exit;
////            exit;
////            exit;
//            echo "<pre>";
//            print_r($csvdata);
//            exit;
            unset($employee);
            unset($logdetails);
            fputcsv($output, $csvdata);
        }
    }

    foreach ($csvdata as $key => $value) {
        fputcsv($output, $value);
    }

    fclose($output);
}
?>