<?php require_once("setting.fya"); ?>
<!DOCTYPE html>
<html>
    <head>
        <style>
            .log_in{
                text-decoration: none;
                color: rgb(255, 255, 255);
                background-color: rgb(208, 172, 82);
                padding: 4px 20px 8px 18px;
                margin-left: 30%;
                font-size: 15px;
                font-weight: 600;
            }
            .admin_panel{
                margin-left: 33%;
            }
        </style>
    <body>
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
            <tbody>
                <tr>
                    <td>
                        <table border="0" cellspacing="0" cellpadding="0" width="800" align="center">
                            <tbody>

                                <tr>
                                    <td>
                                        <table style="border-bottom:#3a020a 10px solid;border-left:#3a020a 10px solid;border-top:#3a020a 10px solid;border-right:#3a020a 10px solid;background-position: 50% -55px;" border="0" cellspacing="0" cellpadding="0" width="800" bgcolor="#ffffff" align="center">
                                            <tbody>
                                                <tr>
                                                    <td align="middle">
                                                        <table border="0" cellspacing="0" cellpadding="0" width="790" align="center">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="60%" align="left" style="padding-left: 19px;padding-top: 40px;"><span style="background-color: #3a020a;color: #fff;padding: 5px;">Client Consent Form</span></td>



                                                                    <td width="20%" align="right" style="padding:1%;"><img border="0" src="http://pos.nailspaexperience.com/og/images/nail_logo_new.png" width="80" height="80" alt="NailSpa Experience"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="line-height:17px;padding:8px 20px 8px 20px;font-family:Verdana,Geneva,sans-serif;color:#000;font-size:13px;" width="500" align="justify">
                                                        Please read the FAQs carefully for after care information:
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height:17px;padding:8px 20px 8px 20px;font-family:Verdana,Geneva,sans-serif;color:#000;font-size:13px;" width="500" align="justify">
                                                        U can know more about faqs on <a target="_blank" href="http://nailspaexperience.com/faq.php">http://nailspaexperience.com/faq.php</a>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="line-height:17px;padding:8px 20px 8px 20px;font-family:Verdana,Geneva,sans-serif;color:#000;font-size:13px;" width="500" align="justify">Name : {Name_Detail}</td>
                                                </tr>

                                                <tr>
                                                    <td style="line-height:17px;padding:8px 20px 8px 20px;font-family:Verdana,Geneva,sans-serif;color:#000;font-size:13px;" width="500" align="justify">Address : {Cust_addr}</td>
                                                </tr>

                                                <tr>
                                                    <td style="line-height:17px;padding:8px 20px 8px 20px;font-family:Verdana,Geneva,sans-serif;color:#000;font-size:13px;" width="500" align="justify">
                                                        {concent_text}
                                                    </td>
                                                </tr>
                                                {approve_row}
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</head>
</html>