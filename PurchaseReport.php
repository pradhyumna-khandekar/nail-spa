<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
$strPageTitle = "Purchase Report | Nailspa";
$strDisplayTitle = "Purchase Report for Nailspa";
$strMenuID = "2";
$strMyTable = "tblStoreStock";
$strMyTableID = "StoreStockID";
$strMyField = "";
$strMyActionPage = "PurchaseReport.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}

// code for not allowing the normal admin to access the super admin rights	



if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $strStep = Filter($_POST["step"]);

    if ($strStep == "add") {
        
    }

    if ($strStep == "edit") {
        
    }
}

if (isset($_GET["toandfrom"])) {
    $strtoandfrom = $_GET["toandfrom"];
    $arraytofrom = explode("-", $strtoandfrom);

    $from = $arraytofrom[0];
    $datetime = new DateTime($from);
    $getfrom = $datetime->format('Y-m-d');


    $to = $arraytofrom[1];
    $datetime = new DateTime($to);
    $getto = $datetime->format('Y-m-d');

    $append_qry = "";
    if (!IsNull($from)) {
        $append_qry .= " and Date(ord.created_date)>=Date('" . $getfrom . "')";
    }

    if (!IsNull($to)) {
        $append_qry .= " and Date(ord.created_date)<=Date('" . $getto . "')";
    }

    if (!empty($_GET["Store"])) {
        $append_qry .= " AND ord.store_id='" . $_GET["Store"] . "'";
    }

    $brand_data = select("*", "tblProductBrand", "BrandID > 0");
    if (isset($brand_data) && is_array($brand_data) && count($brand_data) > 0) {
        foreach ($brand_data as $bkey => $bvalue) {
            $brand_names[$bvalue['BrandID']] = $bvalue['BrandName'];
        }
    }
    $product_data = select("*", "tblNewProducts", "ProductID > 0");
    if (isset($product_data) && is_array($product_data) && count($product_data) > 0) {
        foreach ($product_data as $pkey => $pvalue) {
            $product_name_data[$pvalue['ProductID']]['name'] = $pvalue['ProductName'];
            $product_name_data[$pvalue['ProductID']]['mrp'] = $pvalue['ProductMRP'];
            $product_name_data[$pvalue['ProductID']]['code'] = $pvalue['ProductUniqueCode'];

            $pro_brand = explode(",", $pvalue['Brand']);
            foreach ($pro_brand as $bkey => $bvalue) {
                if (trim($bvalue) != '') {
                    $brand_info[$bvalue][$pvalue['ProductID']] = $pvalue['ProductID'];
                    if (isset($brand_names[$bvalue])) {
                        $product_brand_id[$pvalue['ProductID']][$bvalue] = $brand_names[$bvalue];
                    }
                }
            }
        }
    }


    if (!empty($_GET["brand"])) {
        $all_brand_pid = isset($brand_info[$_GET["brand"]]) ? $brand_info[$_GET["brand"]] : array();
        if (isset($all_brand_pid) && is_array($all_brand_pid) && count($all_brand_pid) > 0) {
            $pid = implode(",", $all_brand_pid);
            if ($pid != '') {
                $append_qry .= " AND opr.product_id IN(" . $pid . ")";
            }
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>

        <script type="text/javascript" src="assets/widgets/datepicker/datepicker.js"></script>
        <script type="text/javascript">
            /* Datepicker bootstrap */

            $(function () {
                "use strict";
                $('.bootstrap-datepicker').bsdatepicker({
                    format: 'mm-dd-yyyy'
                });
            });
        </script>
        <script type = "text/javascript" src = "assets/widgets/datepicker-ui/datepicker.js" ></script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker-demo.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/moment.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");            ?>
            <!----------commented by gandhali 5/9/18---------------->
            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>
                <?php require_once("incLeftMenu.fya"); ?>
                <div id="page-content-wrapper">
                    <div id="page-content">
                        <?php require_once("incHeader.fya"); ?>
                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                        </div>


                        <div class="panel">
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="example-box-wrapper">
                                        <div class="tabs">
                                            <div id="normal-tabs-1">
                                                <span class="form_result">&nbsp; <br>
                                                </span>

                                                <div class="panel-body">
                                                    <h3 class="title-hero">List of all Purchase</h3>

                                                    <form method="get" class="form-horizontal bordered-row" role="form">
                                                        <div class="form-group"><label for="" class="col-sm-4 control-label">Select date</label>
                                                            <div class="col-sm-4">
                                                                <div class="input-prepend input-group">
                                                                    <span class="add-on input-group-addon">
                                                                        <i class="glyph-icon icon-calendar"></i>
                                                                    </span> 
                                                                    <input type="text" autocomplete="off" name="toandfrom" id="daterangepicker-example" class="form-control" value="<?= $strtoandfrom ?>">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Select Store</label>
                                                            <div class="col-sm-4">
                                                                <select name="Store" class="form-control">
                                                                    <option value="0">All</option>
                                                                    <?php
                                                                    $selp = select("*", "tblStores", "Status='0'");
                                                                    foreach ($selp as $val) {
                                                                        $strStoreName = $val["StoreName"];
                                                                        $strStoreID = $val["StoreID"];
                                                                        $store = $_GET["Store"];
                                                                        if ($store == $strStoreID) {
                                                                            ?>
                                                                            <option  selected value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                            <option value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Select Brand</label>
                                                            <?php
                                                            $sqlBrand = "SELECT BrandID, BrandName FROM tblProductBrand WHERE Status=0 ORDER BY BrandName";
                                                            $RSBrand = $DB->query($sqlBrand);
                                                            ?>
                                                            <div class="col-sm-4">
                                                                <select class="form-control required"  name="brand" id="brand">
                                                                    <option value="" selected>All</option>
                                                                    <?php
                                                                    if ($RSBrand->num_rows > 0) {
                                                                        while ($rowBrand = $RSBrand->fetch_assoc()) {
                                                                            $strBrandName = $rowBrand["BrandName"];
                                                                            $strBrandID = $rowBrand["BrandID"];
                                                                            ?>
                                                                            <option value="<?= $strBrandID ?>" <?php echo isset($_GET['brand']) && $_GET['brand'] == $strBrandID ? 'selected' : '' ?>><?= $strBrandName ?></option>														
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                            <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                                            &nbsp;&nbsp;&nbsp;
                                                            <a class="btn btn-link" href="PurchaseReport.php">Clear All Filter</a>
                                                            &nbsp;&nbsp;&nbsp;
                                                        </div>
                                                    </form>

                                                    <br><br>
                                                    <div id="printdata">

                                                        <?php
                                                        $datedrom = $_GET["toandfrom"];
                                                        if ($datedrom != "") {
                                                            $store = $_GET["Store"];
                                                            $sep = select("StoreName", "tblStores", "StoreID='" . $store . "'");
                                                            $storename = $sep[0]['StoreName'];
                                                            ?>
                                                            <h3 class="title-hero">Date Range selected : FROM - <?= $getfrom ?> / TO - <?= $getto ?> / Store Filter selected : <?= $storename ?> </h3>
                                                            <br>
                                                            <?php
                                                            $DB = Connect();
                                                            $per = $_GET["per"];
                                                            ?>
                                                            <div class="panel">
                                                                <div class="panel-body">
                                                                    <div class="example-box-wrapper">
                                                                        <div class="scroll-columns">

                                                                            <table class="table table-striped table-bordered responsive no-wrap" cellspacing="0" border="1px">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th><center>Sr. No.</center></th>
                                                                                <th><center>Product Code</center></th>
                                                                                <th><center>Product Name</center></th>
                                                                                <th><center>Brand</center></th>
                                                                                <th><center>Quantity</center></th>
                                                                                <th><center>Price</center></th>
                                                                                </tr>

                                                                                </thead>
                                                                                <tbody>
                                                                                    <?php
                                                                                    $DB = Connect();
                                                                                    $qry = "SELECT opr.* FROM order_request ord JOIN order_request_product opr ON(ord.id = opr.order_request_id)"
                                                                                            . " WHERE ord.status=1 AND ord.is_verified = 1 AND opr.status=1 " . $append_qry;
                                                                                    $qry_exe = $DB->query($qry);
                                                                                    if ($qry_exe->num_rows > 0) {
                                                                                        while ($rowData = $qry_exe->fetch_assoc()) {
                                                                                            $result_data[] = $rowData;
                                                                                        }
                                                                                    }

                                                                                    if (isset($result_data) && is_array($result_data) && count($result_data) > 0) {
                                                                                        foreach ($result_data as $rkey => $rvalue) {
                                                                                            if (isset($product_res[$rvalue['product_id']])) {
                                                                                                $product_res[$rvalue['product_id']]['quantity'] += $rvalue['quantity'];
                                                                                            } else {
                                                                                                $product_res[$rvalue['product_id']]['quantity'] = $rvalue['quantity'];
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    if (isset($product_res) && is_array($product_res) && count($product_res) > 0) {
                                                                                        $counter = 1;
                                                                                        $total_count = 0;
                                                                                        $total_purchase = 0;
                                                                                        ?>
                                                                                        <?php foreach ($product_res as $rkey => $rvalue) {
                                                                                            ?>
                                                                                            <tr>
                                                                                                <td><center><?php echo $counter; ?></center></td>
                                                                                        <td><center><?php echo isset($product_name_data[$rkey]) ? $product_name_data[$rkey]['code'] : '' ?></center></td>
                                                                                        <td><center><?php echo isset($product_name_data[$rkey]) ? $product_name_data[$rkey]['name'] : '' ?></center></td>
                                                                                        <td><center><?php echo isset($product_brand_id[$rkey]) ? implode(",", $product_brand_id[$rkey]) : '' ?></center></td>
                                                                                        <td><center><?php
                                                                                            $p_count = $rvalue['quantity'];
                                                                                            $total_count += $p_count;
                                                                                            echo $p_count;
                                                                                            ?></center></td>
                                                                                        <td><center><?php
                                                                                            $mrp_price = isset($product_name_data[$rkey]) ? $product_name_data[$rkey]['mrp'] : 0;
                                                                                            $purchase_price = $mrp_price * $p_count;
                                                                                            $purchase_price = round($purchase_price, 2);
                                                                                            $total_purchase += $purchase_price;
                                                                                            echo $purchase_price;
                                                                                            ?></center></td>
                                                                                        </tr>
                                                                                        <?php
                                                                                        $counter++;
                                                                                    }
                                                                                    ?>
                                                                                    <tr>
                                                                                        <td colspan="4"><center><b>Total</b></center></td>
                                                                                    <td><center><b><?php echo $total_count; ?></b></center></td>
                                                                                    <td><center><b><?php echo $total_purchase; ?></b></center></td>
                                                                                    </tr>
                                                                                <?php }else { ?>
                                                                                    <tr>
                                                                                        <td></td>
                                                                                        <td colspan="4"><center>No Record Found...</center></td>
                                                                                    <td></td>
                                                                                    </tr>
                                                                                 <?php } ?>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        } else {
                                                            echo "<br><center><h3>Please Select Month And Year!</h3></center>";
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php require_once 'incFooter.fya'; ?>
        </div>
    </body>
</html>

