<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Manage Activity Cost Center | Nailspa";
$strDisplayTitle = "Manage Activity Cost Center for Nailspa";
$strMenuID = "10";
$strMyTable = "tblMembership";
$strMyTableID = "MembershipID";
$strMyField = "MembershipName";
$strMyActionPage = "ManageActivityCenter.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights	



if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $strStep = Filter($_POST["step"]);
    if ($strStep == "add") {
        foreach ($_POST as $key => $val) {
            if ($key != "step") {
                if (IsNull($sqlColumn)) {
                    $sqlColumn = $key;
                    $sqlColumnValues = "'" . $_POST[$key] . "'";
                } else {
                    $sqlColumn = $sqlColumn . "," . $key;
                    $sqlColumnValues = $sqlColumnValues . ", '" . $_POST[$key] . "'";
                }
            }
        }

        // foreach($_POST as $key => $val)
        // {
        // echo $key ." = ".$_POST[$key];
        // }
        // die();

        $Name = Filter($_POST["Name"]);
        $PerQtyCost = Filter($_POST["PerQtyCost"]);


        $DB = Connect();
        $sql = "SELECT ActivityCostCenterID FROM tblActivityCostCenter WHERE Name='$Name'";
        $RS = $DB->query($sql);
        if ($RS->num_rows > 0) {
            $DB->close();
            die('<div class="alert alert-close alert-danger">
					<div class="bg-red alert-icon"><i class="glyph-icon icon-times"></i></div>
					<div class="alert-content">
						<h4 class="alert-title">Record Add Failed</h4>
						<p>Activity Cost Center with the same Name already exists. Please try again with a different Name.</p>
					</div>
				</div>');
        } else {
            $sqlInsert = "INSERT INTO tblActivityCostCenter (Name, PerQtyCost) VALUES 
				('" . $Name . "', '" . $PerQtyCost . "')";
            ExecuteNQ($sqlInsert);
            //echo $sqlInsert;
            // die();

            $DB->close();
            die('<div class="alert alert-close alert-success">
					<div class="bg-green alert-icon"><i class="glyph-icon icon-check"></i></div>
					<div class="alert-content">
						<h4 class="alert-title">Record Added Successfully.</h4>
						
					</div>
				</div>');
        }
    }

    if ($strStep == "edit") {
        $DB = Connect();

        $Name = Filter($_POST["Name"]);
        $PerQtyCost = Filter($_POST["PerQtyCost"]);

        $strMyTableID = Filter($_POST["ActivityCostCenterID"]);
        ////////////////////////////////////////////////////////////////////////////////

        $sqlUpdate = "UPDATE tblActivityCostCenter SET Name='$Name',PerQtyCost='$PerQtyCost' WHERE ActivityCostCenterID='" . Decode($strMyTableID) . "'";
        ExecuteNQ($sqlUpdate);
        //echo $sqlUpdate;

        $DB->close();
        die('<div class="alert alert-close alert-success">
					<div class="bg-green alert-icon"><i class="glyph-icon icon-check"></i></div>
					<div class="alert-content">
						<h4 class="alert-title">Record Updated Successfully</h4>
					</div>
				</div>');
    }
    die();
}
?>


<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <script>
            $(function ()
            {
                $("#StartDay").datepicker({minDate: 0});
                $("#EndDay").datepicker({minDate: 0});
                $("#StartDay1").datepicker({minDate: 0});
                $("#EndtDay1").datepicker({minDate: 0});

            });
        </script>
        <!-----------css & js files added for tabs by gandhali 5/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");  ?>
            <!----------commented by gandhali 5/9/18---------------->
            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>


                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                            <p>Add, Edit, Delete Memberships</p>
                        </div>
                        <?php
                        if (!isset($_GET["uid"])) {
                            ?>					

                            <div class="panel">
                                <div class="panel">
                                    <div class="panel-body">

                                        <div class="example-box-wrapper">
                                            <div class="tabs">
                                                <ul>
                                                    <li><a href="#normal-tabs-1" title="Tab 1">Manage</a></li>
                                                    <li><a href="#normal-tabs-2" title="Tab 2">Add</a></li>
                                                </ul>
                                                <div id="normal-tabs-1">
                                                    <span class="form_result">&nbsp; <br>
                                                    </span>

                                                    <div class="panel-body">
                                                        <h3 class="title-hero">List of Campaign | Nailspa</h3>
                                                        <div class="example-box-wrapper">
                                                            <table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Sr.No</th>
                                                                        <th>Activity Cost Center</th>
                                                                        <th>Cost(Per Qty Serve)</th>
                                                                        <th>Action</th>
                                                                    </tr>
                                                                </thead>
                                                                <tfoot>
                                                                    <tr>
                                                                        <th>Sr.No</th>
                                                                        <th>Activity Cost Center</th>
                                                                        <th>Cost(Per Qty Serve)</th>
                                                                        <th>Action</th>
                                                                    </tr>
                                                                </tfoot>
                                                                <tbody>

                                                                    <?php
// Create connection And Write Values
                                                                    $DB = Connect();
                                                                    $sql = "SELECT * FROM tblActivityCostCenter";
// echo $sql."<br>";
                                                                    $RS = $DB->query($sql);
                                                                    if ($RS->num_rows > 0) {
                                                                        $counter = 0;

                                                                        while ($row = $RS->fetch_assoc()) {
                                                                            $counter ++;
                                                                            $ActivityCostCenterID = $row["ActivityCostCenterID"];
                                                                            $getUID = EncodeQ($ActivityCostCenterID);
                                                                            $getUIDDelete = Encode($ActivityCostCenterID);
                                                                            $Name = $row["Name"];

                                                                            $Descriprtion = $row["Descriprtion"];

                                                                            $PerQtyCost = $row["PerQtyCost"];
                                                                            ?>	
                                                                            <tr id="my_data_tr_<?= $counter ?>">
                                                                                <td><?= $counter ?></td>
                                                                                <td><?= $Name ?></td>

                                                                                <td>
                                                                                    <?= $PerQtyCost ?>
                                                                                </td>


                                                                                <td style="text-align: center">
                                                                                    <a class="btn btn-link" href="<?= $strMyActionPage ?>?uid=<?= $getUID ?>">Edit</a>
                                                                                    <?php
                                                                                    if ($strAdminRoleID == "36") {
                                                                                        ?>
                                                                                        <a class="btn btn-link font-red" font-redhref="javascript:;" onclick="DeleteData('Step37', '<?= $getUIDDelete ?>', 'Are you sure you want to delete - <?= $AdminFullName ?>?', 'my_data_tr_<?= $counter ?>');">Delete</a>
                                                                                        <?php
                                                                                    }
                                                                                    ?>

                                                                                    <br>

                                                                                </td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                    } else {
                                                                        ?>															
                                                                        <tr>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td>No Records Found</td>



                                                                        </tr>

                                                                        <?php
                                                                    }
                                                                    $DB->close();
                                                                    ?>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="normal-tabs-2">
                                                    <div class="panel-body">
                                                        <form role="form" class="form-horizontal bordered-row enquiry_form" onSubmit="proceed_formsubmit('.enquiry_form', '<?= $strMyActionPage ?>', '.result_message', '', '', '');
                                                                    return false;">

                                                            <span class="result_message">&nbsp; <br>
                                                            </span>
                                                            <input type="hidden" name="step" value="add">


                                                            <h3 class="title-hero">Add Acitivity Cost Center</h3>
                                                            <div class="example-box-wrapper">

                                                                <?php
// Create connection And Write Values
                                                                $DB = Connect();
                                                                $sql = "SHOW COLUMNS FROM tblActivityCostCenter";
                                                                $RS = $DB->query($sql);
                                                                if ($RS->num_rows > 0) {

                                                                    while ($row = $RS->fetch_assoc()) {
                                                                        if ($row["Field"] == "ActivityCostCenterID") {
                                                                            
                                                                        } else if ($row["Field"] == "Name") {
                                                                            ?>	
                                                                            <div class="form-group"><label class="col-sm-3 control-label"><?= str_replace("Name", "Name", $row["Field"]) ?> <span>*</span></label>
                                                                                <div class="col-sm-4"><input type="text" name="<?= $row["Field"] ?>" id="<?= str_replace("Name", "Name", $row["Field"]) ?>" class="form-control required" placeholder="<?= str_replace("Name", "Name", $row["Field"]) ?>"></div>
                                                                            </div>
                                                                            <?php
                                                                        } else if ($row["Field"] == "PerQtyCost") {
                                                                            ?>
                                                                            <div class="form-group"><label class="col-sm-3 control-label">Per Qty Cost<span>*</span></label>
                                                                                <div class="col-sm-4"><input type="text" name="<?= $row["Field"] ?>"  class="form-control required"></div>
                                                                            </div>
                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                            <div class="form-group"><label class="col-sm-3 control-label"><?= str_replace("Admin", " ", $row["Field"]) ?> <span>*</span></label>
                                                                                <div class="col-sm-4"><input type="text" name="<?= $row["Field"] ?>" id="<?= str_replace("Admin", " ", $row["Field"]) ?>" class="form-control required" placeholder="<?= str_replace("Admin", " ", $row["Field"]) ?>"></div>
                                                                            </div>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label"></label>
                                                                        <input type="submit" class="btn ra-100 btn-primary" value="Submit">
                                                                        <div class="col-sm-2">
                                                                            <a class="btn ra-100 btn-black-opacity" href="javascript:;" onclick="ClearInfo('enquiry_form');" title="Clear"><span>Clear</span></a>
                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                }
                                                                $DB->close();
                                                                ?>													
                                                            </div>
                                                        </form>
                                                    </div>



                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        } // End null condition
//-----------------Normal Edit
                        else {
                            ?>						

                            <div class="panel">
                                <div class="panel-body">
                                    <div class="fa-hover">	
                                        <a class="btn btn-primary btn-lg btn-block" href="<?= $strMyActionPage ?>"><i class="fa fa-backward"></i> &nbsp; Go back to <?= $strPageTitle ?></a>
                                    </div>

                                    <div class="panel-body">
                                        <form role="form" class="form-horizontal bordered-row enquiry_form" onSubmit="proceed_formsubmit('.enquiry_form', '<?= $strMyActionPage ?>', '.result_message', '', '.admin_email', '.admin_password');
                                                    return false;">

                                            <span class="result_message">&nbsp; <br>
                                            </span>
                                            <br>
                                            <input type="hidden" name="step" value="edit">


                                            <h3 class="title-hero">Edit Activity Center</h3>
                                            <div class="example-box-wrapper">

                                                <?php
                                                $strID = DecodeQ(Filter($_GET["uid"]));
                                                $DB = Connect();
                                                $sql = "SELECT * FROM tblActivityCostCenter WHERE  ActivityCostCenterID= '$strID'";
                                                $RS = $DB->query($sql);
                                                if ($RS->num_rows > 0) {
                                                    while ($row = $RS->fetch_assoc()) {
                                                        foreach ($row as $key => $val) {
                                                            if ($key == "ActivityCostCenterID") {
                                                                ?>
                                                                <input type="hidden" name="<?= $key ?>" value="<?= Encode($strID) ?>">	

                                                                <?php
                                                            } elseif ($key == "Name") {
                                                                ?>	
                                                                <div class="form-group"><label class="col-sm-3 control-label"><?= str_replace("Name", "Name", $key) ?> <span>*</span></label>
                                                                    <div class="col-sm-4"><input type="text" name="<?= $key ?>" class="form-control required" placeholder="<?= str_replace("Name", "Name", $key) ?>" value="<?= $row[$key] ?>"></div>
                                                                </div>
                                                                <?php
                                                            } elseif ($key == "PerQtyCost") {
                                                                ?>
                                                                <div class="form-group"><label class="col-sm-3 control-label"><?= str_replace("PerQtyCost", "PerQtyCost", $key) ?> <span>*</span></label>
                                                                    <div class="col-sm-4"><input type="text" name="<?= $key ?>" class="form-control required" placeholder="<?= str_replace("PerQtyCost", "PerQtyCost", $key) ?>" value="<?= $row[$key] ?>"></div>
                                                                </div>										


                                                                <?php
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                    <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                        <input type="submit" class="btn ra-100 btn-primary" value="Update">

                                                        <div class="col-sm-2"><a class="btn ra-100 btn-black-opacity" href="javascript:;" onclick="ClearInfo('enquiry_form');" title="Clear"><span>Clear</span></a></div>
                                                    </div>
                                                    <?php
                                                }
                                                $DB->close();
                                                ?>													

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>			
                            <?php
                        }
                        ?>	                   
                    </div>
                </div>
            </div>

            <?php require_once 'incFooter.fya'; ?>

        </div>
    </body>

</html>