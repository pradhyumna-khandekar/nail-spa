<?php
// Turn off error reporting
error_reporting(0);
//ini_set('display_errors', 'Off');
// you don't want to display errors on a prod environment
//display_errors(false); 
// you definitely wanna log any occurring
//log_errors(true); 
require_once("setting.fya");
?>
<?php
$strRemember_me = ValidateCookie("CookieRemember_me");
if ($strRemember_me == "Y") {
    $strEmpUsername = ValidateCookie("CookieEmpUsername");
} else {
    session_start();
    $strEmpUsername = $_SESSION["EmpUsername"];
}


if (!IsNull($strEmpUsername)) {
    header('Location: DisplayReconcillationDetail.php');
}

$strPageTitle = "Login | NailSpa";
$strMenuID = "1";
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once 'incMetaScript.fya'; ?>

        <script>
            function proceed_emp_login()
            {
               
                var isValid = true;
                var counter = 0;
                $('.required').each(function () {
                    if ($.trim($(this).val()) == '') {
                        counter++;
                        if (counter == 1)
                        {
                            $(this).focus();
                        }
                        isValid = false;
                        $(this).css({
                            "border": "1px solid #BF0404",
                            "color": "#000000",
                            "background-color": "rgb(234, 221, 221)"
                        });
                    } else {
                        $(this).css({
                            "border": "",
                            "background-color": "",
                        });
                    }
                });
                if (isValid == false)
                {
                    alert('Fields marked red are compulsory');
                    return false;
                }

                StartLoading();
                var email = $("#inputEmail").val();
                var password = $("#inputPassword").val();
                var rememberme = "";
                if (document.getElementById("remember_me").checked == true)
                {
                    rememberme = "Y";
                }

                //alert('email=' + email + 'password=' + password + 'rememberme=' + rememberme);
                $.ajax({
                    type: 'POST',
                    url: '<?= FindHost() ?>/emp_accountverify.php',
                    data: {
                        email: email,
                        password: password,
                        rememberme: rememberme
                    },
                    success: function (response)
                    {

                        EndLoading();
                        try {
                            var result = JSON.parse(response);
                            window.location = 'EmployeeOtpForm.php?id=' + result['id'] + '&remember_me=' + result['remember_me'];
                        } catch (e) {
                            $('.result_message').html(response);
                        }

                    }
                });
            }

        </script>

    </head>

    <body>
        <?php require_once("incLoader.fya"); ?>

        <style type="text/css">
            html,
            body {
                height: 100%;
                background: #fff;
            }
        </style>
        <div class="center-vertical">
            <div class="center-content">

                <?php
                if ($_SERVER['REMOTE_ADDR'] == "111.119.219.70") {
                    $strMPStitle = "Mypetshop";
                    $strimage = "app-mps.png";
                } else {
                    $strMPStitle = "NailSpa";
                    $strimage = "app-icon (2).png";
                }
                ?>

                <form class="col-md-4 col-sm-5 col-xs-11 col-lg-3 center-margin My_LoginForm" method="post" onsubmit="proceed_emp_login();
                        return false;">
                    <h3 class="text-center pad25B font-gray text-transform-upr font-size-23"><?= $strMPStitle ?><span class="opacity-80">&nbsp;v1.0</span></h3>

                    <div id="login-form" class="content-box bg-default">


                        <div class="content-box-wrapper pad20A"><img class="mrg25B center-margin radius-all-100 display-block" src="assets/image-resources/<?= $strimage ?>" alt="">

                            <div class="result_message" style="font-size:15px;">&nbsp;</div>
                            <h3>Login as Employee</h3><br>
                            <div class="form-group">
                                <div class="input-group"><span class="input-group-addon addon-inside bg-gray"><i class="glyph-icon icon-envelope-o"></i></span> 
                                    <input type="text" class="form-control required" id="inputEmail" placeholder="Enter username">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group"><span class="input-group-addon addon-inside bg-gray"><i class="glyph-icon icon-unlock-alt"></i></span> 
                                    <input type="password" class="form-control required" id="inputPassword" placeholder="Password">
                                </div>
                            </div>
                            <div class="form-group"><button type="submit" class="btn btn-block btn-primary">Login</button></div>
                            <div class="row">
                                <div class="checkbox-primary col-md-6" style="height: 20px"><label style="width:100%;"><input type="checkbox" id="remember_me" value="Y" checked="checked" class="custom-checkbox" > Remember me</label></div>
                            </div>
                        </div>
                    </div>

                </form>	


            </div>
        </div>

        <?php require_once 'incFooter.fya'; ?>

    </body>

</html>