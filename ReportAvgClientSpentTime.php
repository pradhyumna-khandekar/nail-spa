<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
//$start = microtime(true);
$strPageTitle = "Average Client Spent Time Report| Nailspa";
$strDisplayTitle = "Average Client Spent Time Report of Nailspa Experience";
$strMenuID = "2";
$strMyTable = "tblStoreStock";
$strMyTableID = "StoreStockID";
$strMyField = "";
$strMyActionPage = "ReportAvgClientSpentTime.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights	



if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $strStep = Filter($_POST["step"]);

    if ($strStep == "add") {
        
    }

    if ($strStep == "edit") {
        
    }
}
?>


<?php
if (isset($_GET["toandfrom"])) {
    $strtoandfrom = $_GET["toandfrom"];
    $arraytofrom = explode("-", $strtoandfrom);

    $from = $arraytofrom[0];
    $datetime = new DateTime($from);
    $getfrom = $datetime->format('Y-m-d');


    $to = $arraytofrom[1];
    $datetime = new DateTime($to);
    $getto = $datetime->format('Y-m-d');

    if (!IsNull($from)) {
        $sqlTempfrom = " and Date(tblAppointments.AppointmentDate)>=Date('" . $getfrom . "')";
    }

    if (!IsNull($to)) {
        $sqlTempto = " and Date(tblAppointments.AppointmentDate)<=Date('" . $getto . "')";
    }
}
if (isset($_GET["toandfrom"])) {
    $strtoandfrom = $_GET["toandfrom"];
    $arraytofrom = explode("-", $strtoandfrom);

    $from = $arraytofrom[0];
    $datetime = new DateTime($from);
    $getfrom = $datetime->format('Y-m-d');


    $to = $arraytofrom[1];
    $datetime = new DateTime($to);
    $getto = $datetime->format('Y-m-d');

    if (!IsNull($from)) {
        $sqlTempfrom = " and Date(tblAppointments.AppointmentDate)>=Date('" . $getfrom . "')";
    }

    if (!IsNull($to)) {
        $sqlTempto = " and Date(tblAppointments.AppointmentDate)<=Date('" . $getto . "')";
    }
}
?>	


<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>

        <script type="text/javascript" src="assets/widgets/datepicker/datepicker.js"></script>
        <script type="text/javascript">
            /* Datepicker bootstrap */

            $(function () {
                "use strict";
                $('.bootstrap-datepicker').bsdatepicker({
                    format: 'mm-dd-yyyy'
                });
            });

            function printDiv(divName)
            {

                var divToPrint = document.getElementById("printDivtable");
                var htmlToPrint = '' +
                        '<style type="text/css">' +
                        'table th, table td {' +
                        'border:1px solid #000;' +
                        'padding;0.5em;' +
                        '}' +
                        '</style>';
                htmlToPrint += divToPrint.outerHTML;
                newWin = window.open("");
                newWin.document.write(htmlToPrint);
                newWin.print();
                newWin.close();
            }
        </script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker-demo.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/moment.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");      ?>
            <!----------commented by gandhali 5/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>


                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                        </div>
                        <?php

                        function TimetoInt($val) {
                            $time_array = explode(':', $val);
                            $hours = (int) $time_array[0];
                            $minutes = (int) $time_array[1];
                            $seconds = (int) $time_array[2];
                            $total_seconds = ($hours * 3600) + ($minutes * 60) + $seconds;
                            return $total_seconds;
                        }

                        if (!isset($_GET["uid"])) {
                            ?>					

                            <div class="panel">
                                <div class="panel">
                                    <div class="panel-body">


                                        <div class="example-box-wrapper">
                                            <div class="tabs">

                                                <span class="form_result">&nbsp; <br>
                                                </span>

                                                <div class="panel-body">
                                                    <h3 class="title-hero">List of Clients</h3>

                                                    <form method="get" class="form-horizontal bordered-row" role="form">
                                                        <?php if (!isset($_GET['page'])) { ?>
                                                            <input type="hidden" name="page" value="0">
                                                        <?php } ?>
                                                        <div class="form-group"><label for="" class="col-sm-4 control-label">Select date</label>
                                                            <div class="col-sm-4">
                                                                <div class="input-prepend input-group">
                                                                    <span class="add-on input-group-addon">
                                                                        <i class="glyph-icon icon-calendar"></i>
                                                                    </span> 
                                                                    <input type="text" name="toandfrom" id="daterangepicker-example" class="form-control" value="<?= $strtoandfrom ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group"><label for="" class="col-sm-4 control-label">Select Store</label>
                                                            <div class="col-sm-4">

                                                                <select class="form-control required"  name="store">
                                                                    <option value="0" selected>All</option>
                                                                    <?php
                                                                    $selp = select("*", "tblStores", "Status='0'");
                                                                    foreach ($selp as $val) {
                                                                        $strStoreName = $val["StoreName"];
                                                                        $strStoreID = $val["StoreID"];
                                                                        $store = $_GET["store"];
                                                                        if ($store == $strStoreID) {
                                                                            ?>
                                                                            <option  selected value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                            <option value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>


                                                            </div>

                                                        </div>

                                                        <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                            <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                                            &nbsp;&nbsp;&nbsp;
                                                            <a class="btn btn-link" href="ReportAvgClientSpentTime.php">Clear All Filter</a>
                                                            &nbsp;&nbsp;&nbsp;
                                                            <?php
                                                            $datedrom = $_GET["toandfrom"];
                                                            if ($datedrom != "") {
                                                                ?>
                                                                <button onclick="printDiv('printarea')" class="btn btn-blue-alt">Print<div class="ripple-wrapper"></div></button>
                                                                <?php
                                                            }
                                                            ?>

                                                        </div>

                                                    </form>

                                                    <br>
                                                    <?php
                                                    if ($_GET["toandfrom"] != "") {
                                                        $storrr = $_GET["store"];
                                                        if ($storrr == '0') {
                                                            $storrrp = 'All';
                                                        } else {
                                                            $stpp = select("StoreName", "tblStores", "StoreID='" . $storrr . "'");
                                                            $StoreName = $stpp[0]['StoreName'];
                                                            $storrrp = $StoreName;
                                                        }
                                                        ?>
                                                        <div id="printDivtable">
                                                            <h3 class="title-hero">Date Range selected : FROM - <?= $getfrom ?> / TO - <?= $getto ?> / Store - <?= $storrrp ?></h3>

                                                            <?php
                                                            /*
                                                             * Get Total Count
                                                             */
                                                            $sql = "select distinct(tblAppointments.CustomerID), tblAppointments.StoreID,timediff(tblAppointments.AppointmentCheckInTime,tblAppointments.AppointmentCheckOutTime) as spenttime,tblAppointments.AppointmentID
from tblAppointments where tblAppointments.IsDeleted!='1' " . $store_qry . " and tblAppointments.AppointmentDate!='NULL' and tblAppointments.Status='2'  " .
                                                                    $sqlTempfrom . $sqlTempto . "";
                                                            $RS = $DB->query($sql);
                                                            $total_count = $RS->num_rows;

                                                            $page_limit = 20;
                                                            if (isset($_GET['page']) && $_GET['page'] != '') {
                                                                $search_emp[0] = $emp_list[$_GET['page']];
                                                                $get_qry = "";
                                                                if (isset($_GET['toandfrom']) && $_GET['toandfrom'] != '') {
                                                                    $get_qry .= "?toandfrom=" . $_GET['toandfrom'];
                                                                }
                                                                if (isset($_GET['store']) && $_GET['store'] != '') {
                                                                    $get_qry .= "&store=" . $_GET['store'];
                                                                }
                                                                $previous_key = $_GET['page'] - 1;

                                                                $page_qry = "&page=" . $previous_key;

                                                                if ($_GET['page'] != "0") {
                                                                    echo " <a class='btn btn-blue-alt' href='ReportAvgClientSpentTime.php" . $get_qry . $page_qry . "'>Previous</a>";
                                                                }

                                                                if ($total_count > 0) {
                                                                    $cur_page = $_GET['page'] + 1;
                                                                    $page_offset = $cur_page * $page_limit;
                                                                    $remain_count = $total_count - $page_offset;
                                                                }

                                                                if (isset($remain_count) && $remain_count > 0) {
                                                                    $next_key = $_GET['page'] + 1;

                                                                    $page_qry = "&page=" . $next_key;
                                                                    echo " <a class='btn btn-blue-alt' href='ReportAvgClientSpentTime.php" . $get_qry . $page_qry . "'>Next</a>";
                                                                }
                                                            } else {


                                                                if ($total_count > 0) {
                                                                    $page_offset = $page_limit;
                                                                    $remain_count = $total_count - $page_offset;
                                                                }

                                                                if (isset($remain_count) && $remain_count > 0) {
                                                                    $next_id = 1;
                                                                    $get_qry = "";
                                                                    if (isset($_GET['toandfrom']) && $_GET['toandfrom'] != '') {
                                                                        $get_qry .= "?toandfrom=" . $_GET['toandfrom'] . "&page=1";
                                                                    }
                                                                    if (isset($_GET['store']) && $_GET['store'] != '') {
                                                                        $get_qry .= "&store=" . $_GET['store'];
                                                                    }

                                                                    echo "<a class='btn btn-blue-alt' href='ReportAvgClientSpentTime.php" . $get_qry . "'>Next</a>";
                                                                }
                                                            }
                                                            ?>


                                                            <br>


                                                            <div class="example-box-wrapper">
                                                                <table id="printdata" class="table table-bordered table-striped table-condensed cf">
                                                                    <thead>
                                                                        <tr>
                                                                            <th style="text-align:center">Sr</th>
                                                                            <th style="text-align:center">Customer Visited</th>
                                                                            <th style="text-align:center;width:47%">Service Category</th>
                                                                            <th style="text-align:center">Time Spent</th>
                                                                            <!--<th style="text-align:center">Store</th>-->

                                                                        </tr>
                                                                    </thead>

                                                                    <tbody>

                                                                        <?php
                                                                        $DB = Connect();
                                                                        $storrr = $_GET["store"];
                                                                        if (!empty($storrr)) {
                                                                            $store_qry = " AND tblAppointments.StoreID IN(" . $storrr . ")";
                                                                            $store_ids[$storrr] = $storrr;
                                                                        } else {
                                                                            $stpp = select("*", "tblStores", "Status='0'");
                                                                            foreach ($stpp as $vapt) {
                                                                                $store_ids[$vapt['StoreID']] = $vapt['StoreID'];
                                                                            }
                                                                            if (isset($store_ids) && is_array($store_ids) && count($store_ids) > 0) {
                                                                                $store_in_ids = implode(",", $store_ids);
                                                                                if (isset($store_in_ids) && $store_in_ids != '') {
                                                                                    $store_qry = " AND tblAppointments.StoreID IN(" . $store_in_ids . ")";
                                                                                }
                                                                            }
                                                                        }

                                                                        $sql = "select distinct(tblAppointments.CustomerID), tblAppointments.StoreID,timediff(tblAppointments.AppointmentCheckInTime,tblAppointments.AppointmentCheckOutTime) as spenttime,tblAppointments.AppointmentID
from tblAppointments where tblAppointments.IsDeleted!='1' " . $store_qry . " and tblAppointments.AppointmentDate!='NULL' and tblAppointments.Status='2'  " .
                                                                                $sqlTempfrom . $sqlTempto . "";


                                                                        $sql .= " LIMIT 20";


                                                                        if ($_GET['page']) {
                                                                            $offset = $_GET['page'] * 20;
                                                                            $sql .= " OFFSET " . $offset;
                                                                        }

                                                                        $RS = $DB->query($sql);
                                                                        if ($RS->num_rows > 0) {

                                                                            while ($row = $RS->fetch_assoc()) {
                                                                                $result_data[] = $row;
                                                                                $all_cust_ids[$row['CustomerID']] = $row['CustomerID'];
                                                                                $appointment_ids[$row["AppointmentID"]] = $row["AppointmentID"];
                                                                            }


                                                                            /*
                                                                             * get customer name
                                                                             */
                                                                            if (isset($all_cust_ids) && is_array($all_cust_ids) && count($all_cust_ids) > 0) {
                                                                                $cust_in_ids = implode(",", $all_cust_ids);
                                                                                $cust_q = "SELECT CustomerID,CustomerFullName FROM tblCustomers WHERE CustomerID IN(" . $cust_in_ids . ")";
                                                                                $cust_exe = $DB->query($cust_q);
                                                                                while ($custdetails = $cust_exe->fetch_assoc()) {
                                                                                    $all_cust[] = $custdetails;
                                                                                }
                                                                                if (isset($all_cust) && is_array($all_cust) && count($all_cust) > 0) {
                                                                                    foreach ($all_cust as $custkey => $custvalue) {
                                                                                        $sql_customer[$custvalue['CustomerID']] = $custvalue['CustomerFullName'];
                                                                                    }
                                                                                }
                                                                            }

                                                                            /*
                                                                             * get services
                                                                             */
                                                                            if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {
                                                                                $apt_in_ids = implode(",", $appointment_ids);
                                                                                $apt_q = "SELECT distinct ServiceID,AppointmentID FROM tblAppointmentsDetailsInvoice WHERE AppointmentID IN(" . $apt_in_ids . ")";
                                                                                $apt_exe = $DB->query($apt_q);
                                                                                while ($aptdetails = $apt_exe->fetch_assoc()) {
                                                                                    $all_apt[] = $aptdetails;
                                                                                }

                                                                                if (isset($all_apt) && is_array($all_apt) && count($all_apt) > 0) {
                                                                                    foreach ($all_apt as $aptkey => $aptvalue) {
                                                                                        $sql_apt_services[$aptvalue['AppointmentID']][] = $aptvalue['ServiceID'];
                                                                                        $service_ids[$aptvalue['ServiceID']] = $aptvalue['ServiceID'];
                                                                                    }
                                                                                }
                                                                            }

                                                                            /*
                                                                             * get service name
                                                                             */
                                                                            if (isset($service_ids) && is_array($service_ids) && count($service_ids) > 0) {
                                                                                $service_in_ids = implode(",", $service_ids);
                                                                                $service_q = "SELECT ServiceID,ServiceName FROM tblServices WHERE ServiceID IN(" . $service_in_ids . ")";
                                                                                $service_exe = $DB->query($service_q);
                                                                                while ($servdetails = $service_exe->fetch_assoc()) {
                                                                                    $all_services[] = $servdetails;
                                                                                }

                                                                                if (isset($all_services) && is_array($all_services) && count($all_services) > 0) {
                                                                                    foreach ($all_services as $serkey => $servalue) {
                                                                                        $service_name[$servalue['ServiceID']] = $servalue['ServiceName'];
                                                                                    }
                                                                                }
                                                                            }

                                                                            /*
                                                                             * get services category ids
                                                                             */
                                                                            if (isset($store_ids) && is_array($store_ids) && count($store_ids) > 0) {
                                                                                if (isset($service_ids) && is_array($service_ids) && count($service_ids) > 0) {
                                                                                    $service_in_ids = implode(",", $service_ids);
                                                                                    $store_in_ids = implode(",", $store_ids);
                                                                                    if ($service_in_ids != '' && $service_in_ids != '') {
                                                                                        $cat_q = "SELECT DISTINCT StoreID,ServiceID,CategoryID FROM tblProductsServices WHERE ServiceID IN(" . $service_in_ids . ")"
                                                                                                . " AND StoreID IN(" . $store_in_ids . ")";
                                                                                        $cat_exe = $DB->query($cat_q);
                                                                                        while ($catdetails = $cat_exe->fetch_assoc()) {
                                                                                            $all_service_cat[] = $catdetails;
                                                                                        }

                                                                                        if (isset($all_service_cat) && is_array($all_service_cat) && count($all_service_cat) > 0) {
                                                                                            foreach ($all_service_cat as $catkey => $catvalue) {
                                                                                                $ser_category_id[$catvalue['StoreID']][$catvalue['ServiceID']][] = $catvalue['CategoryID'];
                                                                                                $all_category_ids[$catvalue['CategoryID']] = $catvalue['CategoryID'];
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }

                                                                            /*
                                                                             * get category name 
                                                                             */

                                                                            if (isset($all_category_ids) && is_array($all_category_ids) && count($all_category_ids) > 0) {
                                                                                $cat_in_ids = implode(",", $all_category_ids);
                                                                                $cat_q = "SELECT CategoryID,CategoryName FROM tblCategories WHERE CategoryID IN(" . $cat_in_ids . ")";
                                                                                $cat_exe = $DB->query($cat_q);
                                                                                while ($catnamedetails = $cat_exe->fetch_assoc()) {
                                                                                    $all_cat[] = $catnamedetails;
                                                                                }

                                                                                if (isset($all_cat) && is_array($all_cat) && count($all_cat) > 0) {
                                                                                    foreach ($all_cat as $catnkey => $catnvalue) {
                                                                                        $category_name[$catnvalue['CategoryID']] = $catnvalue['CategoryName'];
                                                                                    }
                                                                                }
                                                                            }
                                                                            $count = 0;
                                                                            $FinalTimeOutput = '00:00:00';
                                                                            $offset++;

                                                                            foreach ($result_data as $key => $row) {
                                                                                $count++;

                                                                                $emppp = "";
                                                                                $CustomerID = $row["CustomerID"];
                                                                                $spenttime = $row["spenttime"];
                                                                                $AppointmentID = $row["AppointmentID"];

                                                                                /* $sepmp = select("distinct(ServiceID)", "tblAppointmentsDetailsInvoice", "AppointmentID='" . $AppointmentID . "'");
                                                                                  foreach ($sepmp as $vaq) {
                                                                                  $serrvice[] = $vaq['ServiceID'];
                                                                                  } */

                                                                                $serrvice = isset($sql_apt_services[$AppointmentID]) ? $sql_apt_services[$AppointmentID] : array();


                                                                                //$sepmt = select("*", "tblCustomers", "CustomerID='" . $CustomerID . "'");
                                                                                //$CustomerFullName = $sepmt[0]['CustomerFullName'];
                                                                                $CustomerFullName = isset($sql_customer[$CustomerID]) ? $sql_customer[$CustomerID] : '';


                                                                                $spenttimet = str_replace("-", "", $spenttime);

                                                                                $secs = strtotime($FinalTimeOutput) - strtotime("00:00:00");
                                                                                $FinalTimeOutput = date("H:i:s", strtotime($spenttimet) + $secs);
                                                                                ?>
                                                                                <tr id="my_data_tr_<?= $count ?>">
                                                                                    <td><center><?= $offset++; ?></center></td>
                                                                            <td><center><?= $CustomerFullName ?></center></td>
                                                                            <td align="left"><?php
                                                                                for ($t = 0; $t < count($serrvice); $t++) {
                                                                                    //$sqpq = select("distinct(CategoryID)", "tblProductsServices", "StoreID='" . $row["StoreID"] . "' and ServiceID='" . $serrvice[$t] . "'");

                                                                                    $sqpq = isset($ser_category_id[$row["StoreID"]][$serrvice[$t]]) ? $ser_category_id[$row["StoreID"]][$serrvice[$t]] : array();

                                                                                    foreach ($sqpq as $ca) {
                                                                                        // $sepmq = select("ServiceName", "tblServices", "ServiceID='" . $serrvice[$t] . "'");
                                                                                        // $sername = $sepmq[0]['ServiceName'];
                                                                                        $sername = isset($service_name[$serrvice[$t]]) ? $service_name[$serrvice[$t]] : '';

                                                                                        //$cit = $ca['CategoryID'];
                                                                                        //$sepm = select("distinct(CategoryName)", "tblCategories", "CategoryID='" . $cit . "'");
                                                                                        //$catq = $sepm[0]['CategoryName'];
                                                                                        $catq = isset($category_name[$ca]) ? $category_name[$ca] : '';
                                                                                        echo "<span class='col-md-7'><b>Service</b> - " . $sername . "</span> <span class='col-md-5'><b>Category</b> - " . $catq . "</span><br/>";
                                                                                    }
                                                                                }
                                                                                unset($serrvice);


                                                                                //trim($cats,",")
                                                                                ?></td>
                                                                            <td><center><?= $spenttimet ?></center></td>
                                                                            </tr>

                                                                            <?php
                                                                            unset($cit);
                                                                        }
                                                                    } else {
                                                                        ?>															
                                                                        <tr>

                                                                            <td></td>

                                                                            <td></td>
                                                                            <td>No Records Found</td>
                                                                            <td></td>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <!--<td></td>-->
                                                                        </tr>


                                                                        <?php
                                                                    }
                                                                    $DB->close();
                                                                    //$time_elapsed_secs = microtime(true) - $start;
                                                                    //echo 'Time elapsed=' . $time_elapsed_secs/2000;
                                                                    ?>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td colspan="1"></center></td>
                                                                            <td></td>
                                                                            <td>
                                                                                <?php
                                                                                $AverageTime = TimetoInt($FinalTimeOutput) / $count;
                                                                                echo '<center><b><font color="green">Average Time Spent : ' . gmdate("H:i:s", $AverageTime) . '</font></b></center>';
                                                                                ?>
                                                                            </td>
                                                                            <td class="numeric"><center><b><?= $FinalTimeOutput; ?></b></center></td>

                                                                    </tr>
                                                                    </tbody>

                                                                    <?php
                                                                } else {
                                                                    echo "<br><center><h3>Please Select Month And Year!</h3></center>";
                                                                }
                                                                ?>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>



                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <?php
                        } // End null condition
                        else {
                            
                        }
                        ?>
                    </div>
                </div>

                <?php require_once 'incFooter.fya'; ?>

            </div>
    </body>

</html>