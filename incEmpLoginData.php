<?php

$strRemember_me = ValidateCookie("CookieEmpRemember_me");


if ($strRemember_me == "Y") {
    $strEmpID = ValidateCookie("CookieEmpID");
    $strEmpFullName = ValidateCookie("CookieEmpFullName");
    $strEmpUsername = ValidateCookie("CookieEmpUsername");
} else {
    session_start();
    $strEmpID = $_SESSION["EmpID"];
    $strEmpFullName = $_SESSION["EmpFullName"];
    $strEmpUsername = $_SESSION["EmpUsername"];
}

if (IsNull($strEmpID)) {
    header('Location: emplogout.php');
}
?>
