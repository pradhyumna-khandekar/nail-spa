<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Report Discount | Nailspa";
$strDisplayTitle = "Report Discount Nailspa";
$strMenuID = "2";
$strMyTable = "tblStoreStock";
$strMyTableID = "StoreStockID";
$strMyField = "";
$strMyActionPage = "ReportSale.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights	



if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $strStep = Filter($_POST["step"]);

    if ($strStep == "add") {
        
    }

    if ($strStep == "edit") {
        
    }
}
?>


<?php
if (isset($_GET["toandfrom"])) {
    $strtoandfrom = $_GET["toandfrom"];
    $arraytofrom = explode("-", $strtoandfrom);

    $from = $arraytofrom[0];
    $datetime = new DateTime($from);
    $getfrom = $datetime->format('Y-m-d');


    $to = $arraytofrom[1];
    $datetime = new DateTime($to);
    $getto = $datetime->format('Y-m-d');

    /* if(!IsNull($from))
      {
      $sqlTempfrom = " and Date(tblAppointments.AppointmentDate)>=Date('".$getfrom."')";
      $sqlTempfrom1 = " and Date(tblAppointmentMembershipDiscount.DateTimeStamp)>='".$getfrom."'";
      $sqlTempfrom2 = " and Date(tblGiftVouchers.RedempedDateTime)>='".$getfrom."'";
      $sqlTempfrom3 = " and Date(tblBillingPackage.ValidityStart)>='".$getfrom."'";

      $sqlTempfrom4 = " and Date(tblCustomers.RegDate)>='".$getfrom."'";

      }

      if(!IsNull($to))
      {
      $sqlTempto = " and Date(tblAppointments.AppointmentDate)<=Date('".$getto."')";
      $sqlTempto1 = " and Date(tblAppointmentMembershipDiscount.DateTimeStamp)<='".$getto."'";
      $sqlTempto2 = " and Date(tblGiftVouchers.RedempedDateTime)<='".$getto."'";
      $sqlTempto3 = " and Date(tblBillingPackage.ValidityEnd)<='".$getto."'";
      $sqlTempto4 = " and Date(tblCustomers.RegDate)<='".$getto."'";

      } */

    if (!IsNull($from)) {
        $sqlTempfrom = " and Date(tblInvoiceDetails.OfferDiscountDateTime)>=Date('" . $getfrom . "')";
        //$sqlTempfrom = " and Date(tblAppointments.AppointmentDate)>=Date('".$getfrom."')";
        $sqlTempfrom1 = " and Date(tblCustomers.RegDate)>=Date('" . $getfrom . "')";
    }

    if (!IsNull($to)) {
        $sqlTempto = " and Date(tblInvoiceDetails.OfferDiscountDateTime)<=Date('" . $getto . "')";
        $sqlTempto1 = " and Date(tblCustomers.RegDate)<=Date('" . $getto . "')";
    }
}

$sqlTempStore = '';
if (!empty($_GET["Store"])) {
    $strStoreID = $_GET["Store"];

    $sqlTempStore = " AND StoreID='$strStoreID'";
}
?>	


<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>

        <script type="text/javascript" src="assets/widgets/datepicker/datepicker.js"></script>
        <script type="text/javascript">
            /* Datepicker bootstrap */

            $(function () {
                "use strict";
                $('.bootstrap-datepicker').bsdatepicker({
                    format: 'mm-dd-yyyy'
                });
            });
            function printDiv(divName)
            {

                var divToPrint = document.getElementById("printdata");
                var htmlToPrint = '' +
                        '<style type="text/css">' +
                        'table th, table td {' +
                        'border:1px solid #000;' +
                        'padding;0.5em;' +
                        '}' +
                        '</style>';
                htmlToPrint += divToPrint.outerHTML;
                newWin = window.open("");
                newWin.document.write(htmlToPrint);
                newWin.print();
                newWin.close();
                // var printContents = document.getElementById(divName);
                // var originalContents = document.body.innerHTML;

                // document.body.innerHTML = printContents;

                // window.print();

                // document.body.innerHTML = originalContents; 
            }
        </script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker-demo.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/moment.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");       ?>
            <!----------commented by gandhali 5/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>


                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                        </div>
                        <?php
                        if (!isset($_GET["uid"])) {
                            ?>					

                            <div class="panel">
                                <div class="panel">
                                    <div class="panel-body">


                                        <div class="example-box-wrapper">
                                            <div class="tabs">

                                                <div id="normal-tabs-1">

                                                    <span class="form_result">&nbsp; <br>
                                                    </span>

                                                    <div class="panel-body">

                                                        <form method="get" class="form-horizontal bordered-row" role="form">

                                                            <div class="form-group"><label for="" class="col-sm-4 control-label">Select Date Range</label>
                                                                <div class="col-sm-4">
                                                                    <div class="input-prepend input-group">
                                                                        <span class="add-on input-group-addon">
                                                                            <i class="glyph-icon icon-calendar"></i>
                                                                        </span> 
                                                                        <input type="text" name="toandfrom" id="daterangepicker-example" class="form-control" value="<?= $strtoandfrom ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label">Select Store</label>
                                                                <div class="col-sm-4">
                                                                    <?php
                                                                    $storr = $_GET["Store"];
                                                                    ?>
                                                                    <select name="Store" class="form-control">
                                                                        <?php /* <option value="cu" <?php if ($storr == "cu") { ?> selected <?php } ?>>Cumulative</option> */ ?>
                                                                        <option value="0" <?php if ($storr == "0") { ?> selected <?php } ?>>All</option>
                                                                        <?php
                                                                        $selp = select("*", "tblStores", "Status='0'");
                                                                        foreach ($selp as $val) {
                                                                            $strStoreName = $val["StoreName"];
                                                                            $strStoreID = $val["StoreID"];
                                                                            $store = $_GET["Store"];
                                                                            if ($store == $strStoreID) {
                                                                                ?>
                                                                                <option  selected value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                                <?php
                                                                            } else {
                                                                                ?>
                                                                                <option value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label">Select Type</label>
                                                                <div class="col-sm-4">
                                                                    <?php
                                                                    $discounttype = $_GET['discounttype'];
                                                                    ?>
                                                                    <select name="discounttype" class="form-control">
                                                                        <option value="2" <?php if ($discounttype == '2') { ?> selected <?php } ?> ><?= "Membership" ?></option>	
                                                                        <option value="1" <?php if ($discounttype == '1') { ?> selected <?php } ?>><?= "Offer" ?></option> 
                                                                    </select>
                                                                </div>
                                                            </div>														
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label">Select Percentage</label>
                                                                <div class="col-sm-2">
                                                                    <?php
                                                                    $per = $_GET["per"];
                                                                    ?>
                                                                    <select name="per" class="form-control">
                                                                        <option value="0" <?php if ($per == '0') { ?> selected <?php } ?>>Without Percentage</option>
                                                                        <option value="1" <?php if ($per == '1') { ?> selected <?php } ?>>Percentage</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                                <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                                                &nbsp;&nbsp;&nbsp;
                                                                <a class="btn btn-link" href="ReportDiscount.php">Clear All Filter</a>
                                                                &nbsp;&nbsp;&nbsp;
                                                                <?php
                                                                $datedrom = $_GET["toandfrom"];
                                                                if ($datedrom != "") {
                                                                    ?>
                                                                    <button onclick="printDiv('printarea')" class="btn btn-blue-alt">Print<div class="ripple-wrapper"></div></button>
                                                                    <!--<a class="btn btn-border btn-alt border-primary font-primary" href="ExcelExportData.php?from=<?= $getfrom ?>&to=<?= $getto ?>" title="Excel format 2016"><span>Export To Excel</span><div class="ripple-wrapper"></div></a>
                                                                    -->
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </form>

                                                        <br>
                                                        <div id="printdata">	
                                                            <?php
                                                            $store = $_GET["Store"];
                                                            $dtype = $_GET["discounttype"];
                                                            $datedrom = $_GET["toandfrom"];

                                                            if ($datedrom != "" && $dtype != "0") {

                                                                $storr = $_GET["Store"];
                                                                if ($storr == '0') {
                                                                    $storrrp = 'All';
                                                                } else {
                                                                    $stpp = select("StoreName", "tblStores", "StoreID='" . $storr . "'");
                                                                    $StoreName = $stpp[0]['StoreName'];
                                                                    $storrrp = $StoreName;
                                                                }
                                                                ?>
                                                                <h3 class="title-hero">Date Range selected : FROM - <?= $getfrom ?> / TO - <?= $getto ?> / Store Filter selected : <?= $storrrp ?>  / Discount Type : 
                                                                    <?php
                                                                    if ($discounttype == '1') {
                                                                        echo "Offer";
                                                                    } else {
                                                                        echo "Membership";
                                                                    }
                                                                    ?>


                                                                </h3>

                                                                <br>



                                                                <?php
                                                                $DB = Connect();

                                                                $counter = 0;
                                                                ?>

                                                                <div class="panel">
                                                                    <div class="panel-body">

                                                                        <div class="example-box-wrapper">
                                                                            <div class="scroll-columns">
                                                                                <?php
                                                                                /*
                                                                                 * Get Membership Amount name
                                                                                 */
                                                                                if (isset($_GET['discounttype']) && $_GET['discounttype'] == 1) {
                                                                                    $all_offers = select("OfferID,OfferName,OfferCode,Type,TypeAmount", "tblOffers", "OfferID >0");

                                                                                    if (isset($all_offers) && is_array($all_offers) && count($all_offers) > 0) {
                                                                                        foreach ($all_offers as $offkey => $offvalue) {
                                                                                            $all_offers_name[$offvalue['OfferID']] = $offvalue;
                                                                                        }
                                                                                    }
                                                                                } else {
                                                                                    $all_membership = select("*", "tblMembership", "MembershipID >0");

                                                                                    if (isset($all_membership) && is_array($all_membership) && count($all_membership) > 0) {
                                                                                        foreach ($all_membership as $mkey => $mvalue) {
                                                                                            $all_mem_name[$mvalue['MembershipID']] = $mvalue['MembershipName'];
                                                                                        }
                                                                                    }
                                                                                }
                                                                                $sql = "Select * from tblAppointments WHERE  Status=2 AND Date(AppointmentDate)>='$getfrom' and Date(AppointmentDate)<='$getto' $sqlTempStore";
                                                                                $RS = $DB->query($sql);
                                                                                if ($RS->num_rows > 0) {
                                                                                    while ($row = $RS->fetch_assoc()) {
                                                                                        $all_appointment_id[$row['AppointmentID']] = $row['AppointmentID'];
                                                                                    }
                                                                                }



                                                                                if (isset($all_appointment_id) && is_array($all_appointment_id) && count($all_appointment_id) > 0) {
                                                                                    $appointment_count = count($all_appointment_id);
                                                                                    $limit = 50;
                                                                                    $iteration_cnt = ceil($appointment_count / $limit);

                                                                                    for ($i = 0; $i < $iteration_cnt; $i++) {
                                                                                        $dis_appointment = array();
                                                                                        $customer_ids = array();
                                                                                        $offset = $i * $limit;
                                                                                        $splice_appointment = array_slice($all_appointment_id, $offset, $limit);

                                                                                        if (isset($splice_appointment) && is_array($splice_appointment) && count($splice_appointment) > 0) {
                                                                                            $in_apt_ids = implode(",", $splice_appointment);
                                                                                            if ($in_apt_ids != '') {
                                                                                                /*
                                                                                                 * Get Discount
                                                                                                 */
                                                                                                if (isset($_GET['discounttype']) && $_GET['discounttype'] == 1) {
                                                                                                    $dis_q = "SELECT * FROM tblAppointmentMembershipDiscount WHERE OfferID > 0 AND OfferAmount >0"
                                                                                                            . " AND AppointmentID IN (" . $in_apt_ids . ")";
                                                                                                } else {
                                                                                                    $dis_q = "SELECT * FROM tblAppointmentMembershipDiscount WHERE MembershipID > 0 AND MembershipAmount >0"
                                                                                                            . " AND AppointmentID IN (" . $in_apt_ids . ")";
                                                                                                }

                                                                                                $dis_q_exe = $DB->query($dis_q);

                                                                                                if ($dis_q_exe->num_rows > 0) {
                                                                                                    while ($dis_row = $dis_q_exe->fetch_assoc()) {
                                                                                                        if ($dis_row['MembershipAmount'] > 0 || $dis_row['OfferAmount'] > 0) {
                                                                                                            $dis_amount = 0;
                                                                                                            if (isset($_GET['discounttype']) && $_GET['discounttype'] == 1) {
                                                                                                                $dis_amount = $dis_row['OfferAmount'];
                                                                                                            } else {
                                                                                                                $dis_amount = $dis_row['MembershipAmount'];
                                                                                                            }
                                                                                                            $appointment_data[$dis_row['AppointmentID']][$dis_row['ServiceID']]['dis_amount'] = $dis_amount;

                                                                                                            $appointment_data_pro[$dis_row['AppointmentID']][$dis_row['ServiceID']]['dis_amount'] = $dis_amount;
                                                                                                            $apt_offer_ids[$dis_row['AppointmentID']] = $dis_row['OfferID'];

                                                                                                            if (isset($_GET['discounttype']) && $_GET['discounttype'] == 1) {
                                                                                                                $apt_dis_id_map[$dis_row['AppointmentID']][$dis_row['ServiceID']] = $dis_row['OfferID'];
                                                                                                            } else {
                                                                                                                $apt_dis_id_map[$dis_row['AppointmentID']][$dis_row['ServiceID']] = $dis_row['MembershipID'];
                                                                                                            }
                                                                                                            $dis_appointment[$dis_row['AppointmentID']] = $dis_row['AppointmentID'];
                                                                                                        }
                                                                                                    }
                                                                                                }

                                                                                                if (isset($_GET['discounttype']) && $_GET['discounttype'] == 1) {

                                                                                                    if (isset($appointment_data_pro) && is_array($appointment_data_pro) && count($appointment_data_pro) > 0) {
                                                                                                        foreach ($appointment_data_pro as $appkey => $appvalue) {
                                                                                                            $first_key = key($appointment_data_pro[$appkey]);

                                                                                                            /*
                                                                                                             * Check Appointment Offer Type i.e.1:amount;2:percentage
                                                                                                             */
                                                                                                            $apt_offer_id = isset($apt_offer_ids[$appkey]) ? $apt_offer_ids[$appkey] : '';

                                                                                                            if ($apt_offer_id != '') {
                                                                                                                $apt_offer_data = isset($all_offers_name[$apt_offer_id]) ? $all_offers_name[$apt_offer_id]['Type'] : '';

                                                                                                                if ($apt_offer_data == 1) {
                                                                                                                    /*
                                                                                                                     * Get Appointment Service count
                                                                                                                     */
                                                                                                                    $apt_ofer_service_cnt = count($appvalue);
                                                                                                                    $dist_offer = $appointment_data_pro[$appkey][$first_key]['dis_amount'] / $apt_ofer_service_cnt;
                                                                                                                    foreach ($appvalue as $adistkey => $adistvalue) {
                                                                                                                        $appointment_data[$appkey][$adistkey]['dis_amount'] = round($dist_offer, 2);
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }




                                                                                                /*
                                                                                                 * Get Service Count And Service amount
                                                                                                 */
                                                                                                if (isset($dis_appointment) && is_array($dis_appointment) && count($dis_appointment) > 0) {
                                                                                                    $dis_apt_ids = implode(",", $dis_appointment);

                                                                                                    if ($dis_apt_ids != '') {
                                                                                                        $sql = "Select * from tblInvoiceDetails WHERE AppointmentId IN(" . $dis_apt_ids . ")";

                                                                                                        $RS = $DB->query($sql);
                                                                                                        if ($RS->num_rows > 0) {
                                                                                                            while ($row = $RS->fetch_assoc()) {
                                                                                                                $ServiceName = trim($row['ServiceName']);
                                                                                                                $serviceID = explode(",", $ServiceName);

                                                                                                                $ServiceAmt = trim($row['ServiceAmt']);
                                                                                                                $ServiceAmt_data = explode(",", $ServiceAmt);

                                                                                                                $OfferAmt = trim($row['OfferAmt'], ' - ');

                                                                                                                $ser_offer_amount = 0;
                                                                                                                $OfferAmt = trim($row['OfferAmt'], ' - ');
                                                                                                                if ($OfferAmt > 0) {
                                                                                                                    $ser_offer_amount = $OfferAmt;
                                                                                                                }

                                                                                                                $dis_amt = 0;
                                                                                                                $DisAmt = explode(",", $row['DisAmt']);
                                                                                                                if (isset($DisAmt) && is_array($DisAmt) && count($DisAmt) > 0) {
                                                                                                                    foreach ($DisAmt as $key => $value) {
                                                                                                                        if (trim($value) > 0) {
                                                                                                                            $dis_amt += $value;
                                                                                                                        }
                                                                                                                    }
                                                                                                                }

                                                                                                                if (isset($serviceID) && is_array($serviceID) && count($serviceID) > 0) {
                                                                                                                    foreach ($serviceID as $serkey => $servalue) {
                                                                                                                        $appointment_data[$row['AppointmentId']][$servalue]['service_count'] = 1;
                                                                                                                        $appointment_data[$row['AppointmentId']][$servalue]['service_amt'] = $ServiceAmt_data[$serkey] - $ser_offer_amount - $dis_amt;
                                                                                                                        $amancheck[] = $ServiceAmt_data[$serkey] - $ser_offer_amount - $dis_amt;

                                                                                                                        $appointment_data[$row['AppointmentId']][$servalue]['customer_id'] = $row['CustomerID'];
                                                                                                                    }
                                                                                                                }

                                                                                                                $customer_ids[$row['CustomerID']] = $row['CustomerID'];
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }


                                                                                                /*
                                                                                                 * Get Customer Appointment Count
                                                                                                 */
                                                                                                if (isset($customer_ids) && is_array($customer_ids) && count($customer_ids) > 0) {
                                                                                                    $cust_in_ids = implode(",", $customer_ids);
                                                                                                    $apt_countQ = "SELECT COUNT(AppointmentId) as apt_count,CustomerID FROM tblAppointments "
                                                                                                            . " WHERE AppointmentCheckInTime!='00:00:00' AND status=2 AND CustomerID IN(" . $cust_in_ids . ")"
                                                                                                            . " GROUP BY CustomerID";
                                                                                                    $apt_count_exe = $DB->query($apt_countQ);
                                                                                                    while ($count_data = $apt_count_exe->fetch_assoc()) {
                                                                                                        $customer_apt_count[$count_data['CustomerID']] = $count_data['apt_count'];
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }

                                               
                                                                                if (isset($appointment_data) && is_array($appointment_data) && count($appointment_data) > 0) {
                                                                                    foreach ($appointment_data as $aptkey => $aptvalue) {
                                                                                        foreach ($aptvalue as $serkey => $servalue) {
                                                                                            
                                                                                            if(strpos($servalue['service_amt'], '-' ) !== false)
                                                                                            {
                                                                                                $amankey[] = $aptkey;
                                                                                            }
                                                                                            $dis_ids = isset($apt_dis_id_map[$aptkey][$serkey]) ? $apt_dis_id_map[$aptkey][$serkey] : 0;
                                                                                            $smsnid[] = isset($apt_dis_id_map[$aptkey][$serkey]) ? $apt_dis_id_map[$aptkey][$serkey] : 0;

                                                                                            $final_res[$dis_ids]['dis_type'] = $dis_ids;

                                                                                            if (isset($final_res[$dis_ids]['service_count'])) {
                                                                                                $final_res[$dis_ids]['service_count'] += 1;
                                                                                            } else {
                                                                                                $final_res[$dis_ids]['service_count'] = 1;
                                                                                            }

                                                                                            if (isset($final_res[$dis_ids]['service_amount'])) {
                                                                                                $final_res[$dis_ids]['service_amount'] += $servalue['service_amt'];
                                                                                                $amanamtcheck[] += $servalue['service_amt'];
                                                                                            } else {
                                                                                                $final_res[$dis_ids]['service_amount'] = $servalue['service_amt'];
                                                                                                $amanamtcheck[] = $servalue['service_amt'];
                                                                                            }



                                                                                            $apt_count = isset($customer_apt_count[$servalue['customer_id']]) ? $customer_apt_count[$servalue['customer_id']] : 1;
                                                                                            if ($apt_count > 1) {

                                                                                                if (isset($final_res[$dis_ids]['old_service_amount'])) {
                                                                                                    $final_res[$dis_ids]['old_service_amount'] += $servalue['service_amt'];
                                                                                                } else {
                                                                                                    $final_res[$dis_ids]['old_service_amount'] = $servalue['service_amt'];
                                                                                                }


                                                                                                $final_res[$dis_ids]['old_cust_count'][$aptkey] = $aptkey;


                                                                                                if (isset($final_res[$dis_ids]['old_cust_dis'])) {
                                                                                                    $final_res[$dis_ids]['old_cust_dis'] += $servalue['dis_amount'];
                                                                                                } else {
                                                                                                    $final_res[$dis_ids]['old_cust_dis'] = $servalue['dis_amount'];
                                                                                                }
                                                                                            } else {
                                                                                                if (isset($final_res[$dis_ids]['new_service_amount'])) {
                                                                                                    $final_res[$dis_ids]['new_service_amount'] += $servalue['service_amt'];
                                                                                                } else {
                                                                                                    $final_res[$dis_ids]['new_service_amount'] = $servalue['service_amt'];
                                                                                                }

                                                                                                $final_res[$dis_ids]['new_cust_count'][$aptkey] = $aptkey;

                                                                                                if (isset($final_res[$dis_ids]['new_cust_dis'])) {
                                                                                                    $final_res[$dis_ids]['new_cust_dis'] += $servalue['dis_amount'];
                                                                                                } else {
                                                                                                    $final_res[$dis_ids]['new_cust_dis'] = $servalue['dis_amount'];
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    echo "<pre>";
                                                                                    print_r($amankey);
                                                                                    exit;
                                                                                }


                                                                                $per = $_GET["per"];
                                                                                ?>

                                                                                <table class="table table-bordered table-striped table-condensed cf">
                                                                                    <thead class="cf">

                                                                                        <tr>
                                                                                            <th><center>Discount Type</center></th>
                                                                                    <th><center>Service Count</center></th>
                                                                                    <th><center>Service Amount</center></th>
                                                                                    <th><center>New Client Count</center></th>	
                                                                                    <?php
                                                                                    if ($per != '0') {
                                                                                        ?>
                                                                                        <th><center>New Client %</center></th>
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                    <?php /* <th><center>New Client Offer/Membership Amount</center></th> */ ?>
                                                                                    <th><center>New Client Amount</center></th>
                                                                                    <th><center>Existing Client Count</center></th>
                                                                                    <?php
                                                                                    if ($per != '0') {
                                                                                        ?>
                                                                                        <th><center>Existing Client %</center></th>	
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                    <?php /* <th><center>Existing Client Offer/Membership Amount</center></th */ ?>
                                                                                    <th><center>Existing Client Amount</center></th>
                                                                                    </tr>


                                                                                    </thead>
                                                                                    <?php
                                                                                    if (isset($final_res) && is_array($final_res) && count($final_res) > 0) {
                                                                                        $totalnew_custcnt = 0;
                                                                                        $total_new_dis = 0;
                                                                                        $totalold_custcnt = 0;
                                                                                        $total_old_dis = 0;
                                                                                        $totalsercnt = 0;
                                                                                        $toserviceamt = 0;
                                                                                        ?>
                                                                                        <tbody>
                                                                                            <?php
                                                                                            foreach ($final_res as $key => $value) {
                                                                                                $row_old = isset($value['old_cust_count']) ? count($value['old_cust_count']) : 0;
                                                                                                $row_new = isset($value['new_cust_count']) ? count($value['new_cust_count']) : 0;
                                                                                                $sum_row_cust = $row_old + $row_new;
                                                                                                if (isset($_GET['discounttype']) && $_GET['discounttype'] == 1) {
                                                                                                    $discount_name = isset($all_offers_name[$key]) ? $all_offers_name[$key]['OfferName'] : '';
                                                                                                } else {
                                                                                                    $discount_name = isset($all_mem_name[$key]) ? $all_mem_name[$key] : '';
                                                                                                }
                                                                                                if ($discount_name != '') {
                                                                                                    ?>
                                                                                                    <tr>
                                                                                                        <td><center><?php echo $discount_name; ?></center></td>
                                                                                                <td><center><?php
                                                                                                    $totalsercnt += $value['service_count'];
                                                                                                    echo $value['service_count'];
                                                                                                    ?></center></td>
                                                                                                <td><center><?php
                                                                                                    $toserviceamt += $value['service_amount'];
                                                                                                    echo $value['service_amount'];
                                                                                                    ?></center></td>
                                                                                                <td><center><?php
                                                                                                    $new_customer = isset($value['new_cust_count']) ? count($value['new_cust_count']) : 0;
                                                                                                    $totalnew_custcnt += $new_customer;
                                                                                                    echo $new_customer;
                                                                                                    ?></center></td>
                                                                                                <?php
                                                                                                if ($per != '0') {
                                                                                                    ?>
                                                                                                    <td class="numeric"><center><b><?php
                                                                                                            $row_new_per = ($new_customer / $sum_row_cust) * 100;
                                                                                                            echo round($row_new_per, 2)
                                                                                                            ?></b></center></td>
                                                                                                    <?php
                                                                                                }
                                                                                                ?>
                                                                                                <td><center><?php
                                                                                                    // $new_cus_dis = isset($value['new_cust_dis']) ? $value['new_cust_dis'] : 0;
                                                                                                    $new_cus_dis = isset($value['new_service_amount']) ? $value['new_service_amount'] : 0;
                                                                                                    $total_new_dis += $new_cus_dis;
                                                                                                    echo $new_cus_dis;
                                                                                                    ?></center></td>
                                                                                                <td><center><?php
                                                                                                    $old_custcnt = isset($value['old_cust_count']) ? count($value['old_cust_count']) : 0;
                                                                                                    $totalold_custcnt += $old_custcnt;
                                                                                                    echo $old_custcnt;
                                                                                                    ?></center></td>
                                                                                                <?php
                                                                                                if ($per != '0') {
                                                                                                    ?>
                                                                                                    <td class="numeric"><center><b><?php
                                                                                                            $row_old_per = ($row_old / $sum_row_cust) * 100;
                                                                                                            echo round($row_old_per, 2)
                                                                                                            ?></b></center></td>
                                                                                                    <?php
                                                                                                }
                                                                                                ?>
                                                                                                <td><center><?php
                                                                                                    //$old_cus_dis = isset($value['old_cust_dis']) ? $value['old_cust_dis'] : 0;
                                                                                                    $old_cus_dis = isset($value['old_service_amount']) ? $value['old_service_amount'] : 0;
                                                                                                    $total_old_dis += $old_cus_dis;
                                                                                                    echo $old_cus_dis;
                                                                                                    ?></center></td>
                                                                                                </tr>   
                                                                                                <?php
                                                                                            }
                                                                                        }
                                                                                        ?>
                                                                                        <tr>
                                                                                            <?php $sum_total_cust = $totalold_custcnt + $totalnew_custcnt; ?>
                                                                                            <td><center><b>Total Amounts(s)</b></center></td>

                                                                                        <td class="numeric"><center><b><?= $totalsercnt ?></b></center></td>
                                                                                        <td class="numeric"><center><b>Rs. <?= $toserviceamt ?>/-</b></center></td>
                                                                                        <td class="numeric"><center><b><?= $totalnew_custcnt ?></b></center></td>
                                                                                        <?php
                                                                                        if ($per != '0') {
                                                                                            ?>
                                                                                            <td class="numeric"><center><b><?php
                                                                                                    $total_new_per = ($totalnew_custcnt / $sum_total_cust) * 100;
                                                                                                    echo round($total_new_per, 2)
                                                                                                    ?></b></center></td>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                                        <td class="numeric"><center><b>Rs. <?= round($total_new_dis, 2) ?>/-</b></center></td>
                                                                                        <td class="numeric"><center><b><?= $totalold_custcnt ?></b></center></td>
                                                                                        <?php
                                                                                        if ($per != '0') {
                                                                                            ?>                                                                                
                                                                                            <td class="numeric"><center><b><?php
                                                                                                    $total_old_per = ($totalold_custcnt / $sum_total_cust) * 100;
                                                                                                    echo round($total_old_per, 2)
                                                                                                    ?></b></center></td>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                                        <td class="numeric"><center><b>Rs. <?= round($total_old_dis, 2) ?>/-</b></center></td>

                                                                                        </tr>
                                                                                        </tbody>
                                                                                    <?php } else { ?>
                                                                                        <tbody>

                                                                                            <tr>

                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td>No Data Found</td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    <?php } ?>
                                                                                </table>
                                                                                <?php
                                                                                $DB->close();
                                                                                ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        </div>
                                        <?php
                                    } else {
                                        echo "<br><center><h3>Please Select Month And Year!</h3></center>";
                                    }
                                }
                                ?>  
                                <?php require_once 'incFooter.fya'; ?>

                            </div>
                            </body>

                            </html>