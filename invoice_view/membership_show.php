<div class="panel-body col-md-8" >
    <div  class="enddate" style="background-color: lightgreen; color: black; margin-top:10px;padding: inherit; width: 50%;text-transform: uppercase; font-weight: 700;">
        <?php
        $DB = Connect();
        $CustomerID = $apt_all_data[0]['CustomerID'];
        $Memeber = "SELECT * FROM tblCustomerMemberShip where CustomerID='" . $CustomerID . "'";
        $RS = $DB->query($Memeber);
        $row = $RS->fetch_assoc();
        $enddate = $row['EndDay'];
        /*
         * Check Membership id present in customer table or not
         */

        $cust_memberid = $apt_all_customer[0]['memberid'];
        if ($enddate == '' || $cust_memberid == 0) {
            echo 'Currently No MemberShip Active';
        } else {
            echo 'MemberShip Expiry Date:' . '' . $enddate;
        }
        ?>
    </div>
    <div id="displaymembership" class="panel-body">
        <label class="control-label" style="width:25%" >Select Membership</label>
        <?php
        $seldata = select("*", "tblMembership", "Status='0'");
        ?>
        <select class="form-control" style="width:45%;display:inline-block;" style="text-align:center" id="memebr" onchange="assignmember()">
            <option value="0">Select Here</option>
            <?php
            foreach ($seldata as $val) {
                ?>
                <option value="<?= $val['MembershipID']; ?>"><?= $val['MembershipName']; ?></option>

                <?php
            }
            ?>
        </select>
    </div>
    <?php if ($invoiceflag != 'H') { ?>
        <div id="displaymembership1" class="panel-body">
            <button type="button" id="remove_membership"  class="btn btn-success" data-toggle="button" ><center>Remove MemberShip</center></button>
        </div>                                                                                            
        <?php
    }
///////////////////////////////////////////assign same membership or renew membership////////////////////////////////////////////////////////////////
    $CustomerID = $apt_all_data[0]['CustomerID'];

    $sqp = select("count(memberid)", "tblAppointments", "CustomerID='" . $CustomerID . "' and memberid!='0'");
    $memberidp = $apt_all_customer[0]['memberid'];
    $seldoffertqtm = select("*", "tblMembership", "MembershipID='" . $memberidp . "'");
    $MembershipName = $seldoffertqtm[0]['MembershipName'];
//print_r($sqp);
    $cnt = $sqp[0]['count(memberid)'];
    $seldatapttptqppp = select("count(*)", "tblCustomerMemberShip", "CustomerID='" . $CustomerID . "'");
    $cntttp = $seldatapttptqppp[0]['count(*)'];
    $seldatapttptqp = select("*", "tblCustomerMemberShip", "CustomerID='" . $CustomerID . "'");
//print_r($seldataptt);
    $date = date('Y-m-d');
    $enddate = $seldatapttptqp[0]['EndDay'];
    $RenewStatus = $seldatapttptqp[0]['RenewStatus'];
    if ($FreeService == "0" && $invoiceflag != 'H') {
        ?>
        <div id="displaymembership2" class="panel-body" style="display:none">
            <input type="hidden" id="memidd" value="<?= $memberidp ?>" />
            <button type="button" id="assignmembership"  class="btn btn-success" data-toggle="button" ><center>Assign Membership <?= $MembershipName ?></center></button>
        </div>
        <?php
        //$enddate = '2017-10-25';
        if ($cntttp > 0) {
            if ($date >= $enddate) {
                ?>
                <div id="displaymembership3" class="panel-body" >
                    <input type="hidden" id="memidd" value="<?= $memberidp ?>" />
                    <button type="button" id="renewmem"  class="btn btn-success" data-toggle="button" ><center>Renew & Assign Membership <?= $MembershipName ?></center></button>
                </div>
                <?php
            }
        }
    }
///////////////////////////////////////////////////display gift voucher code apply and remove div ///////////////////////////////////////////////////////////////////////
    ?>
    <div class="panel-body">
        <?php
        $VoucherID = $apt_all_data[0]['VoucherID'];
        $memberid = $apt_all_data[0]['memberid'];
        $seldvoucher = select("count(GiftVoucherID),RedemptionECode,RedemptionCode", "tblGiftVouchers", "RedempedBy='" . DecodeQ($_GET['uid']) . "' and Status='1'");
        $cnt = $seldvoucher[0]['count(GiftVoucherID)'];
        $RedemptionECode = $seldvoucher[0]['RedemptionECode'];
        $RedemptionCode = $seldvoucher[0]['RedemptionCode'];
        if ($FreeService == "0" && $invoiceflag != 'H') {

            if ($cnt != '0') {
                ?>
                <label class="control-label" style="width:25%" >Gift Voucher Code</label>
                <input type="text" style="width:45%;display:inline-block;"  name="giftname" id="giftname" class="form-control" value="<?= $RedemptionCode ?>" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" id="removevoucher" style="width:12%;display:inline-block;" value="<?php echo $RedemptionECode ?>" class="btn btn-success" data-toggle="button" ><center>Remove</center></button>	
                <?php
            } else {
                ?>
                <label class="control-label" style="width:25%" >Gift Voucher Code</label>
                <input type="text" style="width:45%;display:inline-block;"  name="giftname" id="giftname" class="form-control" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" id="giftapply" style="width:25%;display:inline-block;"  class="btn btn-success" data-toggle="button" ><center>Voucher Code</center></button>
                <?php
            }
        }
        //////////////////////////////////////////////////////////////////////////////////
        ?>             

    </div>

    <div class="panel-body">
        <?php
////////////////////////////////////////////////display offer div/////////////////////////////////////
        $offerid = $apt_all_data[0]['offerid'];
        $seldofferp = select("*", "tblOffers", "OfferID='" . $offerid . "'");
        $OfferCode = $seldofferp[0]['OfferCode'];
        if ($FreeService == "0" && $invoiceflag != 'H') {
            if ($offerid != "0") {
                ?>
                <label class="control-label" style="width:25%" >Offer Name</label>
                <input type="text"  readonly style="width:45%;display:inline-block;" value="<?php echo $OfferCode ?>" name="offername" id="offername" class="form-control" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" id="offeridremove" style="width:12%;display:inline-block;" value="<?php echo $OfferCode ?>" class="btn btn-success" data-toggle="button" ><center>Remove</center></button>

                <?php
            } else {
                ?>
                <label class="control-label" style="width:25%" >Offer Name</label>
                <select id="offername" style="width:45%;display:inline-block;" class="form-control offername" name="offername">
                    <option value="0">Select Here</option>
                    <?php
                    $seldata = select("*", "tblOffers", "Status='0'");
                    foreach ($seldata as $val) {
                        ?>
                        <option value="<?= $val['OfferID'] ?>"><?= $val['OfferName'] ?></option>
                        <?php
                    }
                    ?>
                </select>
                &nbsp;&nbsp;&nbsp;&nbsp;<button type="button" id="offerid" style="width:25%;display:inline-block;" value="Check Offer" class="btn btn-success" data-toggle="button" ><center>Apply Offer</center></button>

                <?php }
            ?>
        </div>
    <?php
    }
/////////////////////////////////////////////////////////start bill code//////////////////////////////////////
    ?>
