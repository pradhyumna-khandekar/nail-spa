<script type="text/javascript" src="assets/widgets/datepicker/datepicker.js"></script>
<script type="text/javascript">
    /* Datepicker bootstrap */
    $(function () {
        "use strict";
        $('#daterangepicker-example').daterangepicker({
            locale: {
                format: 'mm/dd/YYYY - mm/dd/YYYY'
            }
        });
    });
</script>
<script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker.js"></script>
<script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker-demo.js"></script>
<script type="text/javascript" src="assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker-demo.js"></script>


<script type="text/javascript">
            function AddBalanceAmount() {
                var totalpendamt = $("#totalpendamt").val();
                if (totalpendamt != undefined) {
                    $("#pendingpayment").show();
                    $("#totalpend").val('+' + totalpendamt);
                    var totalvalue = $("#completeamtt").val();
                    var totalpay = parseFloat(totalvalue) + parseFloat(totalpendamt);
                    var roundtotal = Math.round(totalpay)
                    $("#completeamtt").val(totalpay);
                    $("#roundtotal").val(roundtotal);
                    $("#completeamtt").val(roundtotal);
                    $("#totalpayment").val(roundtotal);
                    $("#totalpaymentamt").val(roundtotal);
                    $("#showpend").hide();
                }
            }
            $(document).ready(function ()
            {
                $(document).keydown(function (event) {
                    var pressedKey = String.fromCharCode(event.keyCode).toLowerCase();
                    if (event.ctrlKey && (pressedKey == "c" || pressedKey == "u" || pressedKey == "v" || pressedKey == "a" || pressedKey == "s")) {
                        //alert('Sorry, This Functionality Has Been Disabled For Security Reasons!');
                        //disable key press porcessing
                        //return false;
                    }
                });

                $(document).keyup(function (e) {
                    if (e.keyCode == 44)
                        return false;
                });

                //$("#payment").show();
                $("#discounttr").hide();
                $("#displaydetail").show();
                // $("#displaymembership").hide();

                ////////////////////assign membership////////////////////////////////
                //alert(222)
                $("#assignmembership").click(function () {
                    var app_id = $("#appointment_idd").val();
                    var app_idd = $("#appointment_id").val();
                    var mem = $("#memidd").val();
                    if (confirm('Are you sure you want to Assign Membership?')) {
                        $.ajax({
                            url: "Updatememid.php",
                            type: 'post',
                            data: "app_id=" + app_id + "&mem=" + mem,
                            success: function (msg) {
                                //alert(msg)
                                if ($.trim(msg) == '2') {
                                    $msg = "Membership Assigned Successfully!";
                                    alert($msg)
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                } else if ($.trim(msg) == '6') {
                                    alert('Membership you are trying to apply has expired.Please renew the Membership!')
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                } else if ($.trim(msg) == '3') {
                                    alert('Please Remove Membership or Gift Voucher or Offer Code to perform this action!')
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                } else if ($.trim(msg) == '7') {
                                    d
                                    alert('Kindly note membership discount is not applicable on packages and gift vouchers!')
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                }

                            }

                        });
                    } else {
                    }
                });
                $("#renewmem").click(function () {
                    var app_id = $("#appointment_idd").val();
                    var app_idd = $("#appointment_id").val();
                    var mem = $("#memidd").val();
                    if (confirm('Are you sure you want to Renew Membership?')) {
                        $.ajax({
                            url: "renewmem.php",
                            type: 'post',
                            data: "app_id=" + app_id + "&mem=" + mem,
                            success: function (msg) {
                                if ($.trim(msg) == '2') {
                                    $msg = "Membership Renew Successfully";
                                    alert($msg)
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                } else if ($.trim(msg) == '3') {
                                    alert('Please Remove Membership or Gift Voucher or Offer Code to perform this action!')
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                } else if ($.trim(msg) == '4') {
                                    $msg = "Kindly note membership discount is not applicable on packages and gift vouchers!";
                                    alert($msg)
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                }

                            }

                        });
                    } else {
                    }
                });

                var app_id = $("#appointment_idd").val();
                //alert(app_id)
                ////////////////////////////
                $.ajax({
                    type: "post",
                    data: "app_id=" + app_id,
                    url: "checkmembership.php",
                    success: function (result) {
                        //alert(result)
                        if ($.trim(result) != '0') {
                            if ($.trim(result) == 'Present') {
                                $("#displaymembership2").show();
                                $("#displaymembership").hide();
                                $("#displaymembership1").hide();
                            } else if ($.trim(result) == 'Absent') {
                                $("#displaymembership").hide();
                                $("#displaymembership1").show();
                            } else if ($.trim(result) == 'Already') {
                                $("#displaymembership").show();
                                $("#displaymembership1").hide();
                            }
                        }
                    }
                });
                //////////////////////////////////////////////////////
                /////////////////////////////////remove gift voucher///////////////////////////////
                $("#removevoucher").click(function () {
                    var app_id = $("#appointment_idd").val();
                    var app_idd = $("#appointment_id").val();
                    var giftname = $("#giftname").val();
                    if (confirm('Are you sure you want to Remove This Voucher?')) {
                        $.ajax({
                            url: "RemoveVoucher.php",
                            type: 'post',
                            data: "app_id=" + app_id + "&giftname=" + giftname,
                            success: function (msg) {
                                //alert(msg)
                                if ($.trim(msg) == '3') {
                                    $msg = "Gift voucher removed successfully!";
                                    alert($msg)
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                } else if ($.trim(msg) == '1') {
                                    alert('Cannot Removed')
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                }
                            }

                        });
                    } else {
                    }
                });
                $("#giftapply").click(function () {
                    var giftname = $("#giftname").val();
                    var app_idd = $("#appointment_id").val();
                    var app_id = $("#appointment_idd").val();
                    var cust_id = $("#cust_id").val();
                    if (confirm('Are you sure you want to Apply Gift Voucher?')) {
                        if (giftname != "") {
                            $.ajax({
                                url: "checkvoucher.php",
                                type: 'post',
                                data: "giftname=" + giftname + "&app_id=" + app_id + "&cust_id=" + cust_id,
                                success: function (msg) {
                                    if ($.trim(msg) == '8') {
                                        alert('Gift voucher expired!')
                                        window.location = "appointment_invoice.php?uid=" + app_idd;
                                    } else if ($.trim(msg) == '2') {
                                        alert('Gift voucher redemption code is incorrect!')
                                        window.location = "appointment_invoice.php?uid=" + app_idd;
                                    } else if ($.trim(msg) == '3') {
                                        alert('Please Remove Membership or Gift Voucher or Offer Code to perform this action!')
                                        window.location = "appointment_invoice.php?uid=" + app_idd;
                                    } else if ($.trim(msg) == '5') {
                                        alert('Gift Voucher Apply Successfully')
                                        window.location = "appointment_invoice.php?uid=" + app_idd;
                                    } else if ($.trim(msg) == '4') {
                                        alert('Gift Voucher Already Used')
                                        window.location = "appointment_invoice.php?uid=" + app_idd;
                                    }


                                }
                            });
                        } else {
                            alert('Gift Voucher Code Cannot Be Blank')
                        }
                    } else {
                    }
                });
                $("#gift").click(function () {
                    var gift = $("#giftvoucher").val();
                    var app_id = $("#appointment_idd").val();
                    var app_idd = $("#appointment_id").val();
                    var store = $("#storet").val();
                    var cust_id = $("#cust_id").val();
                    var giftcvalidity = $("#giftcvalidity").val();
                    var giftqty = $("#giftqty").val();
                    if (confirm('Are you sure you want to Add Gift Voucher?')) {
                        if (gift != "0" && giftcvalidity != "0" && giftqty != "0") {
                            $.ajax({
                                url: "AddVoucher.php",
                                type: 'post',
                                data: "id=" + gift + "&store=" + store + "&app_id=" + app_id + "&cust_id=" + cust_id + "&giftcvalidity=" + giftcvalidity,
                                success: function (msg) {
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                }
                            });
                        } else {
                            alert('Please select proper amount, quantity and validity to add a Gift voucher!')
                        }
                    } else {
                    }

                });
                ////////////////////////////////////////////////////////////////////////////////////////////
                ///////////////////////////////offer/////////////////////////////////////////////////////////
                $("#offerid").click(function () {
                    var offervalue = $("#offername").val();
                    //	alert(offervalue)
                    var app_id = $("#appointment_idd").val();
                    var app_idd = $("#appointment_id").val();
                    if (offervalue != "") {
                        $.ajax({
                            type: "post",
                            data: "offervalue=" + offervalue + "&app_id=" + app_id,
                            url: "checkoffer.php",
                            success: function (result) {
                                if ($.trim(result) == '2') {
                                    var msg = "Invalid Offer Code";
                                    //$("#displayoffererror").html(msg);
                                    alert(msg);
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                } else if ($.trim(result) == '3') {
                                    var msg = "Please note, offer applied is not valid for some of the services in the bill!";
                                    alert(msg);
                                    //$("#displayoffererror").html(msg);
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                } else if ($.trim(result) == '1') {
                                    var msg = "Offer Assigned Successfully";
                                    alert(msg);
                                    //$("#displayoffererror").html(msg);
                                    window.location = "appointment_invoice.php?uid=" + app_idd;

                                } else if ($.trim(result) == '8') {
                                    var msg = "Offer expired";
                                    alert(msg);
                                    //$("#displayoffererror").html(msg);
                                    window.location = "appointment_invoice.php?uid=" + app_idd;

                                } else if ($.trim(result) == '9') {
                                    var msg = "Please note the service amount is less then offer amount!";
                                    alert(msg);
                                    //$("#displayoffererror").html(msg);
                                    window.location = "appointment_invoice.php?uid=" + app_idd;

                                } else if ($.trim(result) == '4') {
                                    var msg = "This offer is not valid for this store";
                                    alert(msg);
                                    //$("#displayoffererror").html(msg);
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                } else if ($.trim(result) == '6') {
                                    var msg = "Offer Code Cannot Be Blank";
                                    alert(msg);
                                    //$("#displayoffererror").html(msg);
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                } else if ($.trim(result) == '10') {
                                    var msg = "Please Remove Membership or Gift Voucher or Offer Code to perform this action!";
                                    alert(msg);
                                    //$("#displayoffererror").html(msg);
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                } else if ($.trim(result) == '14') {
                                    var msg = "Please Remove Membership or Gift Voucher or Offer Code to perform this action!";
                                    alert(msg);
                                    //$("#displayoffererror").html(msg);
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                } else if ($.trim(result) == '20') {
                                    var msg = "Kindly note Offer discount is not applicable on packages and gift vouchers!";
                                    alert(msg);
                                    //$("#displayoffererror").html(msg);
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                } else if ($.trim(result) == '15') {
                                    var msg = "Kindly note Offer discount is not applicable on packages and gift vouchers!";
                                    alert(msg);
                                    //$("#displayoffererror").html(msg);
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                } else {
                                    var msg = $.trim(result);
                                    alert(msg);
                                    //$("#displayoffererror").html(msg);
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                }
                            }
                        })
                    } else {
                        alert('offer code cannot be blank')
                        return false;
                    }
                });
                $("#offeridremove").click(function () {
                    var offervalue = $("#offername").val();
                    var app_id = $("#appointment_idd").val();
                    var app_idd = $("#appointment_id").val();
                    if (offervalue != "") {
                        $.ajax({
                            type: "post",
                            data: "offervalue=" + offervalue + "&app_id=" + app_id,
                            url: "removeoffer.php",
                            success: function (result1) {
                                //alert(result1)
                                if ($.trim(result1) == '') {
                                } else if ($.trim(result1) == '6') {
                                    var msg = "Offer Code Cannot Be Blank";
                                    alert(msg);
                                    //$("#displayoffererror").html(msg);
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                } else {
                                    var msg = "Offer Removed Successfully";
                                    alert(msg);
                                    //$("#displayoffererror").html(msg);
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                }
                            }
                        });
                    } else {
                        alert('offer code cannot be blank')
                        return false;
                    }
                });
                $("#offeridnew").click(function () {
                    var offervalue = $("#offername").val();
                    var app_id = $("#appointment_idd").val();
                    var app_idd = $("#appointment_id").val();
                    if (offervalue != "") {
                        $.ajax({
                            type: "post",
                            data: "offervalue=" + offervalue + "&app_id=" + app_id,
                            url: "newoffer.php",
                            success: function (result1) {
                                //alert(result1)
                                if ($.trim(result1) == '2') {
                                    var msg = "Invalid Offer Code";
                                    alert(msg);
                                    //$("#displayoffererror").html(msg);
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                } else if ($.trim(result1) == '3') {
                                    var msg = "Please note, offer applied is not valid for some of the services in the bill!";
                                    alert(msg);
                                    //$("#displayoffererror").html(msg);
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                } else if ($.trim(result1) == '1') {
                                    var msg = "Offer Assigned Successfully";
                                    alert(msg);
                                    //$("#displayoffererror").html(msg);
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                } else if ($.trim(result1) == '4') {
                                    var msg = "This offer is not valid for this store";
                                    alert(msg);
                                    //$("#displayoffererror").html(msg);
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                } else if ($.trim(result1) == '5') {
                                    var msg = "This offer is already assigned";
                                    alert(msg);
                                    //$("#displayoffererror").html(msg);
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                } else if ($.trim(result1) == '6') {
                                    var msg = "Offer Code Cannot Be Blank";
                                    alert(msg);
                                    //$("#displayoffererror").html(msg);
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                }
                            }
                        });
                    } else {
                        alert('offer code cannot be blank')
                        return false;
                    }
                });
                //////////////////////////////////////////////////////////////////////////////////////////////
                //////////////////////////////////////cash payment mode///////////////////////////////
                $("#cash").click(function () {
                    var app_id = $("#appointment_idd").val();
                    var app_idd = $("#appointment_id").val();
                    if (confirm('Are you sure you want to save this Invoice?')) {
                        var test = $("#cash").val();
                        //alert(test)
                        if (test == 'cash') {
                            $("#paymenttype").show();
                            //$("#paymenttype1").show();

                            var type = $('input[name="paytype"]:checked').val();
                            //alert(type)
                            if (type == 'Partial') {
                                var completeamt = $("#roundtotal").val();
                                var amt = $("#totalpayment").val();
                                // alert(amt)
                            } else if (type == 'Complete') {
                                //var completeamt = $("#roundtotal").val();
                                //alert(completeamt)
                                var amt = $("#totalpayment").val();
                                //alert(amt)
                            }


                            if (Number(completeamt) > Number(amt)) {
                                alert('Amount Cannot Be Greater Than Total Amount')
                            } else {
                                $.ajax({
                                    url: "printbill.php",
                                    type: 'post',
                                    data: $("#printcontent").serialize() + "&type=" + "CS",
                                    success: function (msg) {

                                        if ($.trim(msg) == '2') {
                                            $msg = "Cash Received";
                                            window.location = "CheckInvoices.php?automsg=1&uid=" + app_idd + "&msg=" + $msg;
                                        } else {
                                            alert(msg);
                                        }
                                    }
                                });
                            }

                        }
                    } else {
                        // Do nothing!
                    }

                });
                $("#CompleteAmt").click(function () {
                    var app_id = $("#appointment_idd").val();
                    var app_idd = $("#appointment_id").val();
                    if (confirm('Are you sure you want to save this Invoice?')) {
                        $("#paymenttype").show();
                        //$("#paymenttype1").show();

                        var type = $('input[name="paytype"]:checked').val();
                        //alert(type)
                        if (type == 'Partial') {
                            var completeamt = $("#roundtotal").val();
                            var amt = $("#totalpayment").val();
                            // alert(amt)
                        } else if (type == 'Complete') {
                            var completeamt = $("#roundtotal").val();
                            //alert(completeamt)
                            var amt = $("#totalpayment").val();
                            //alert(amt)
                        }


                        if (Number(completeamt) > Number(amt)) {
                            alert('Amount Cannot Be Greater Than Total Amount')
                        } else {
                            $.ajax({
                                url: "printbill.php",
                                type: 'post',
                                data: $("#printcontent").serialize() + "&type=" + "CompleteAmt",
                                success: function (msg) {
                                    //alert(msg)
                                    if ($.trim(msg) == '2') {
                                        $msg = "Invoice Complete";
                                        window.location = "CheckInvoices.php?automsg=1&uid=" + app_idd + "&msg=" + $msg;
                                    } else {
                                        alert(msg);
                                    }
                                }
                            });
                        }

                    } else {
                        // Do nothing!
                    }
                });

                $('input[type="radio"]').click(function () {
                    if ($(this).attr("value") == "Partial") {
                        $(".partial").show();
                        $("#cardboth").val('');
                        $("#cashboth").val('');
                    }
                    if ($(this).attr("value") == "Complete") {
                        $(".partial").hide();
                        $("#cardboth").val('');
                        $("#cashboth").val('');
                    }
                });

                $("#both").click(function () {
                    var app_id = $("#appointment_idd").val();
                    var app_idd = $("#appointment_id").val();
                    var test = $("#both").val();
                    if (test == 'both') {
                        $("#card").hide();
                        $("#cash").hide();
                        $("#confirm").show();
                        $("#both").hide();
                        //$("#hold").hide();
                        $("#payment1").show();
                        var totalcost = $("#roundtotal").val();
                        var cardamount = $("#cardboth").val();
                        var cashamount = $("#cashboth").val();
                        var bothamt = parseFloat(cardamount) + parseFloat(cashamount);
                        //alert(totalcost)
                        if (Number(bothamt) > Number(totalcost)) {
                            alert('Card and Cash Amount Cannot Be Greater Than Total Amount')
                        } else {
                        }
                    }
                });
                $("#confirm").click(function () {
                    if (confirm('Are you sure you want to save this Invoice?')) {
                        var cardamt = $("#cardboth").val();
                        var cashboth = $("#cashboth").val();
                        var amt = $("#totalpayment").val();
                        // alert(amt)
                        if (cardamt == "" || cashboth == "") {
                            alert('Card Amount And Cash Amount Cannot Blank')
                        } else if (Number(cardamt) > Number(amt)) {
                            alert('Card Amount Cannot Be Greater Total Amount');
                        } else if (Number(cashboth) > Number(amt)) {
                            alert('Cash Amount Cannot Be Greater Total Amount');
                        } else {
                            var app_id = $("#appointment_idd").val();
                            var app_idd = $("#appointment_id").val();
                            $.ajax({
                                url: "printbill.php",
                                type: 'post',
                                data: $("#printcontent").serialize() + "&type=" + "BOTH",
                                success: function (msg) {
                                    //alert(msg)
                                    if ($.trim(msg) == '2') {
                                        $msg = "Amount Received";
                                        window.location = "CheckInvoices.php?automsg=1&uid=" + app_idd + "&msg=" + $msg;
                                    } else {
                                        alert(msg)
                                    }
                                }
                            });
                        }
                    } else {
                    }
                });
                $("#hold").click(function () {
                    var app_id = $("#appointment_idd").val();
                    var app_idd = $("#appointment_id").val();
                    if (confirm('Are you sure you want to save this Invoice?')) {
                        var test = $("#hold").val();
                        //alert(test)
                        if (test == 'hold') {
                            var type = $('input[name="paytype"]:checked').val();
                            //alert(type)
                            if (type == 'Partial') {
                                var completeamt = $("#roundtotal").val();
                                var amt = $("#totalpayment").val();
                                // alert(amt)
                            } else if (type == 'Complete') {
                                var completeamt = Math.round($("#roundtotal").val());
                                //alert(completeamt)
                                var amt = $("#totalpayment").val();
                                //alert(amt)
                            }

                            if (Number(completeamt) > Number(amt)) {
                                alert('Amount Cannot Be Greater Than Total Amount')
                            } else {
                                var abc = $("#paymentid").val();
                                $("#totalpayment").html(abc);
                                $.ajax({
                                    url: "printbill.php",
                                    type: 'post',
                                    data: $("#printcontent").serialize() + "&type=" + "H",
                                    success: function (msg) {
                                        //alert(msg)
                                        if ($.trim(msg) == '2') {
                                            $msg = "Amount is Pending";
                                            window.location = "CheckInvoices.php?automsg=1&uid=" + app_idd + "&msg=" + $msg;
                                        } else {
                                            alert(msg)
                                        }
                                    }

                                });
                            }

                        }
                    } else {
                        // Do nothing!
                    }

                });
                $("#btnPrint").click(function () {
                    //alert(111)
                    var divContents = $("#printbill").html();
                    //alert(divContents)
                    var printWindow = window.open('', '', 'height=400,width=800');
                    printWindow.document.write('<html><head><title>Invoice</title>');
                    printWindow.document.write('</head><body >');
                    printWindow.document.write(divContents);
                    printWindow.document.write('</body></html>');
                    printWindow.document.close();
                    printWindow.print();
                });
                $("#ManagePackage").click(function () {
                    $("#packagedetail").show();
                });
                $("#card").click(function () {
                    var app_id = $("#appointment_idd").val();
                    var app_idd = $("#appointment_id").val();

                    if (confirm('Are you sure you want to save this Invoice?')) {
                        var test = $("#card").val();
                        if (test == 'card') {
                            $("#paymenttype").show();
                            var type = $('input[name="paytype"]:checked').val();
                            if (type == 'Partial') {
                                var completeamt = $("#roundtotal").val();
                                var amt = $("#totalpayment").val();
                            } else if (type == 'Complete') {
                                var completeamt = $("#roundtotal").val();
                                var amt = $("#totalpayment").val();
                            }

                            if (Number(completeamt) > Number(amt)) {
                                alert('Amount Cannot Be Greater Than Total Amount')
                            } else {
                                $.ajax({
                                    url: "printbill.php",
                                    type: 'post',
                                    data: $("#printcontent").serialize() + "&type=" + "C",
                                    success: function (msg) {
                                        //alert(msg)
                                        if ($.trim(msg) == '2') {
                                            $msg = "Card Payment Received";
                                            window.location = "CheckInvoices.php?automsg=1&uid=" + app_idd + "&msg=" + $msg;
                                        } else {
                                            alert(msg)
                                        }
                                    }
                                });
                            }
                        }
                    } else {
                        // Do nothing!
                    }
                });
                $("#addtobill").click(function () {
                    AddBalanceAmount();
                });
                $("#remove_membership").click(function () {
                    var app_id = $("#appointment_idd").val();
                    var app_idd = $("#appointment_id").val();
                    if (app_id != "") {
                        $.ajax({
                            type: "post",
                            data: "&app_id=" + app_id,
                            url: "removemember.php",
                            success: function (respp) {
                                //alert(respp)
                                if ($.trim(respp) == '2') {
                                    window.location = "appointment_invoice.php?uid=" + app_idd;
                                }
                            }
                        })
                    }
                });
            });
            var message = "Please dont try this as we are keeping a track of who is trying this!";
            if (document.layers) {
                document.captureEvents(Event.MOUSEDOWN);
                document.onmousedown = clickNS4;
            } else if (document.all && !document.getElementById) {
                document.onmousedown = clickIE4;
            }
            function removesinglevoucher() {
                var gift = $("#GiftVoucherID").val();
                var app_id = $("#appointment_idd").val();
                var app_idd = $("#appointment_id").val();
                if (gift != "") {
                    $.ajax({
                        url: "DeleteVoucher.php",
                        type: 'post',
                        data: "gift=" + gift + "&app_idd=" + app_id,
                        success: function (msg) {
                            //alert(msg)
                            if ($.trim(msg) == '3') {
                                $msg = "Gift Voucher Removed Successfully";
                                alert($msg)
                                window.location = "appointment_invoice.php?uid=" + app_idd;
                            } else if ($.trim(msg) == '1') {
                                alert('Cannot Removed Voucher')
                                window.location = "appointment_invoice.php?uid=" + app_idd;
                            }
                        }
                    });
                }
            }

            function refreshdropdown() {
                var dropDown = document.getElementById("giftqty");
                dropDown.selectedIndex = 0;
            }
            function checkamt(evt) {
                //alert(111)
                var qty = $(evt).val();
                var gift = $("#giftvoucher").val();
                //alert(gift)
                var app_id = $("#appointment_idd").val();
                //alert(app_id)
                var app_idd = $("#appointment_id").val();
                var store = $("#storet").val();
                // alert(store)
                var cust_id = $("#cust_id").val();
                if (gift != "0" && gift != "" && gift != null && qty != "0" && qty != "") {
                    $.ajax({
                        url: "CalculateVoucherAmt.php",
                        type: 'post',
                        data: "id=" + gift + "&store=" + store + "&app_id=" + app_id + "&cust_id=" + cust_id + "&qty=" + qty,
                        success: function (msg) {
                            //alert(msg)
                            $("#validitylimit").show();
                            $("#validitylimit").html(msg);

                        }
                    });
                } else {
                    alert('Please select amount first!')
                }

            }
            function checktypeofpayemnt() {
                var type = $('input[name="paytype"]:checked').val();
                //alert(type)
                if (type == 'Partial') {
                    $("#paymentcheck").show();
                } else if (type == 'Complete') {
                    var totalcost = $("#roundtotal").val();
                    $("#completeamt").val(totalcost);
                    $("#paymentcheck").hide();
                }
            }
            function test() {
                //alert(111)
                var abc = $("#paymentid").val();
                //alert(abc)
                var value = $("#totalvalue").text();
                // alert(value)
                if (Number(value) > Number(abc)) {
                    alert('Amount Should Not Be Greater Than Total Amount')
                }
                $("#totalpayment").html(abc);
            }
            function checkinsert(evt) {
                var serviceid = $(evt).closest('td').prev().prev().find('input').val();
                var app_id = $("#appointment_idd").val();
                var app_idd = $("#appointment_id").val();
                //alert(app_id)
                if (serviceid != "") {
                    $.ajax({
                        type: "post",
                        data: "serviceid=" + serviceid + "&app_id=" + app_id,
                        url: "insertservice.php",
                        success: function (res) {
                            //alert(res)
                            if ($.trim(res) == '2') {
                                window.location = "appointment_invoice.php?uid=" + app_idd;
                            } else if ($.trim(res) == '3')
                            {
                                alert('You Cannot Add Same Service Again')
                                window.location = "appointment_invoice.php?uid=" + app_idd;
                            } else {
                                alert('Please Remove Membership or Gift Voucher or Offer Code to perform this action!')
                                window.location = "appointment_invoice.php?uid=" + app_idd;
                            }
                        }
                    })
                }
            }
            function checkdelete(evt) {
                var serviceid = $(evt).closest('td').prev().prev().prev().find('input').val();
                var idd = $(evt).closest('td').find('input').val();
                //alert(serviceid);
                var app_id = $("#appointment_idd").val();
                var app_idd = $("#appointment_id").val();
                //alert(app_id)
                if (serviceid != "") {
                    $.ajax({
                        type: "post",
                        data: "serviceid=" + serviceid + "&app_id=" + app_id + "&idd=" + idd,
                        url: "deleteservice.php",
                        success: function (res) {
                            //	alert(res)
                            if ($.trim(res) == '2') {
                                window.location = "appointment_invoice.php?uid=" + app_idd;
                            } else if ($.trim(res) == '3') {
                                alert('Please Remove Membership or Gift Voucher or Offer Code to perform this action!')
                                window.location = "appointment_invoice.php?uid=" + app_idd;
                            } else if ($.trim(res) == '4') {
                                alert('There should be atleast one service in this appointment')
                                window.location = "appointment_invoice.php?uid=" + app_idd;
                            }
                        }
                    })
                }
            }
            function checkdeletepackage(evt) {
                var idd = $(evt).closest('td').find('input').val();
                var app_id = $("#appointment_idd").val();
                var app_idd = $("#appointment_id").val();
                //alert(app_id)
                if (idd != "") {
                    $.ajax({
                        type: "post",
                        data: "app_id=" + app_id + "&idd=" + idd,
                        url: "deletepackage.php",
                        success: function (res) {
                            //alert(res)
                            if ($.trim(res) == '2') {
                                alert('Package removed successfully!')
                                window.location = "appointment_invoice.php?uid=" + app_idd;
                            }
                        }
                    })
                }
            }
            function assignmember() {
                // alert(1111)
                var member = $("#memebr").val();
                var app_id = $("#appointment_idd").val();
                var app_idd = $("#appointment_id").val();
                $(".membertype").val(member);
                if (member != "0") {
                    $.ajax({
                        type: "post",
                        data: "member=" + member + "&app_id=" + app_id,
                        url: "updatemember.php",
                        success: function (respp) {
                            if ($.trim(respp) == '2') {
                                window.location = "appointment_invoice.php?uid=" + app_idd;
                            } else if ($.trim(respp) == '3') {
                                alert('First Remove Gift Voucher')
                                window.location = "appointment_invoice.php?uid=" + app_idd;
                            } else if ($.trim(respp) == '4') {
                                alert('Remove Offer Discount Or Gift Voucher And Then Apply Membership')
                                window.location = "appointment_invoice.php?uid=" + app_idd;
                            } else if ($.trim(respp) == '5') {
                                alert('Remove Offer Discount Or Gift Voucher And Then Apply Membership')
                                window.location = "appointment_invoice.php?uid=" + app_idd;
                            } else if ($.trim(respp) == '6') {
                                alert('Membership you are trying to apply has expired. Please renew the Membership!')
                                window.location = "appointment_invoice.php?uid=" + app_idd;
                            } else if ($.trim(respp) == '7') {
                                alert('Kindly note membership discount is not applicable on packages and gift vouchers!')
                                window.location = "appointment_invoice.php?uid=" + app_idd;
                            } else if ($.trim(respp) == '8') {
                                alert('Kindly note membership discount is not applicable on packages and gift vouchers!')
                                window.location = "appointment_invoice.php?uid=" + app_idd;
                            }
                        }
                    })
                }
            }
            function test1(evt) {
                var qty = $(evt).val();
                var id = $(evt).attr("id");
                var nexttdid = parseInt(id) + 1;
                var nexttdidfinal = "saif" + nexttdid;
                var Amount = $('#' + nexttdidfinal).val();
                var amt = parseFloat(Amount);
                var qqty = parseFloat(qty);
                var AmountAfterMultiply = 0;
                AmountAfterMultiply = qqty * amt;
                var totalamt = AmountAfterMultiply + ".00";
                $('#' + nexttdidfinal).val(totalamt) + ".00"
                var serviceid = $(evt).closest('td').prev().find('input').val();
                var idd = $(evt).closest('td').next().next().find('input').val();
                var app_id = $("#appointment_idd").val();
                var app_idd = $("#appointment_id").val();
                if (qty != "") {
                    $.ajax({
                        type: "post",
                        data: "qty=" + qty + "&amt=" + totalamt + "&serviceid=" + serviceid + "&app_id=" + app_id + "&idd=" + idd,
                        url: "updateqty.php",
                        success: function (res) {
                            //alert(res)
                            if ($.trim(res) == '2') {
                                window.location = "appointment_invoice.php?uid=" + app_idd;
                            } else if ($.trim(res) == '3') {
                                alert('Please Remove Membership or Gift Voucher or Offer Code to perform this action!')
                                window.location = "appointment_invoice.php?uid=" + app_idd;
                            }
                        }
                    })
                }
            }
            function calculatecashamount() {
                var totalcost = Math.round($("#completeamtt").val());
                var cashamt = $("#cashboth").val();
                var completeamt = Math.round($("#completeamt").val());
                var ans = $('input:radio:checked').val();
                if (ans == 'Complete') {
                    if (Number(cashamt) > Number(totalcost)) {
                        alert('Cash Amount Greater Than Complete Amount');
                    } else {
                        var amt = parseFloat(totalcost) - parseFloat(cashamt);
                        //	alert(amt)
                        $("#cardboth").val(amt);
                        var cardboth = $("#cardboth").val();
                        $("#totalpayment").val(totalcost);
                    }
                } else {
                    if (Number(cashamt) > Number(completeamt)) {
                        alert('Cash Amount Greater Than Partial Amount');
                    } else {
                        var amt = parseFloat(completeamt) - parseFloat(cashamt);
                        $("#cardboth").val(amt);
                        var cardboth = $("#cardboth").val();
                        $("#totalpayment").val(totalcost);
                    }
                }
            }
            function calculatecardamt() {
                //alert(111)
                var totalcost = Math.round($("#completeamtt").val());
                var cardamtt = $("#cardboth").val();
                var completeamt = Math.round($("#completeamt").val());
                //alert(cardamtt)
                var ans = $('input:radio:checked').val();
                if (ans == 'Complete') {
                    if (Number(cardamtt) > Number(totalcost)) {
                        alert('Card Amount Greater Than Complete Amount');
                    } else {
                        var amt = parseFloat(totalcost) - parseFloat(cardamtt);
                        //alert(amt)
                        $("#cashboth").val(amt);
                        $("#totalpayment").val(totalcost);
                    }
                } else {
                    if (Number(cardamtt) > Number(completeamt)) {
                        alert('Card Amount Greater Than Partial Amount');
                    } else {
                        var amt = parseFloat(completeamt) - parseFloat(cardamtt);
                        //alert(amt)
                        $("#cashboth").val(amt);
                        $("#totalpayment").val(completeamt);
                    }
                }
            }
            //////////////////////////////////////
            function calculatecomplete() {

                var complete = $("#completeamt").val();
                var totalcost = Math.round($("#completeamtt").val());
                if (Number(complete) > Number(totalcost)) {
                    alert('Complete Amount Should Not Be Greater Total Payment')
                } else {
                    $("#completeamt").val();
                    var amt = parseFloat(totalcost) - parseFloat(complete);
                    //	alert(amt)
                    $("#pendamt").val(amt);
                    $("#totalpaymentamt").val(totalcost);
                }

            }
            function checkinsertpackage(evt) {
                var packageid = $(evt).closest('td').prev().prev().find('input').val();
                var app_id = $("#appointment_idd").val();
                var app_idd = $("#appointment_id").val();
                //alert(app_id)
                if (packageid != "") {
                    $.ajax({
                        type: "post",
                        data: "packageid=" + packageid + "&app_id=" + app_id,
                        url: "InsertPackage.php",
                        success: function (res) {
                            // alert(res)
                            if ($.trim(res) == '2') {
                                alert('Package added successfully!')
                                window.location = "appointment_invoice.php?uid=" + app_idd;
                            }
                        }
                    })
                }
            }
            function calculatepend() {
                //alert(111)
                var pend = $("#pendamt").val();
                //alert(cardamtt)
                var totalcost = Math.round($("#completeamtt").val());
                if (Number(pend) > Number(totalcost)) {
                    alert('Pending amount should not be greater then total pending amount!')
                } else {
                    $("#pendamt").val();
                    var amt = parseFloat(totalcost) - parseFloat(pend);
                    //alert(amt)
                    var test = parseFloat(totalcost) + parseFloat(amt);
                    $("#completeamt").val(amt);
                    $("#totalpaymentamt").val(totalcost);
                }
            }
            function addtopackageservice(evt) {
                var packageid = $(evt).closest('td').next().find('input').val();
                var cust_packageid = $(evt).closest('td').next().next().find('input').val();
                var validdate = $(evt).closest('td').prev().find('input').val();
                var service = $(evt).closest('td').prev().prev().find('input').val();
                var app_id = $("#appointment_idd").val();
                var app_idd = $("#appointment_id").val();
                var store = $("#storet").val();
                var app_date = $("#app_date").val();
                if (packageid != "") {
                    $.ajax({
                        type: "post",
                        data: "packageid=" + packageid + "&cust_packageid=" + cust_packageid + "&app_id=" + app_id + "&validdate=" + validdate + "&service=" + service + "&store=" + store + "&app_date=" + app_date,
                        url: "AddServiceToPackage.php",
                        success: function (res) {
                            // alert(res)
                            if ($.trim(res) == '2') {
                                window.location = "appointment_invoice.php?uid=" + app_idd;
                            } else if ($.trim(res) == '3') {
                                alert('Package expired!')
                                window.location = "appointment_invoice.php?uid=" + app_idd;
                            }
                        }
                    });
                }
            }

            function assignprepackage(app_id) {
                var custpackid = $('#cust_package :selected').val();
                //alert(custpackid);
                if (custpackid != "" && app_id != "") {
                    $.ajax({
                        type: "post",
                        data: "custpackid=" + custpackid + "&app_id=" + app_id,
                        url: "addPackagetoAppointment.php",
                        success: function (res) {
                            window.location.reload(true);
                        }
                    });
                }
            }
</script>

<style>
    .btn-success {
        background-color: lightgreen;
        color: black;
    }
    .btn-success:hover {
        background-color: lightgreen;
        color: black;
    }
    @media print {
        .no-print {
            display: none;
        }
    }
</style>