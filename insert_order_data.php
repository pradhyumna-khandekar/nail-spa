<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php

$post = $_POST;
if (isset($post) && is_array($post) && count($post) > 0) {
    $order_name = $post['order_name'] != '' ? $post['order_name'] : 'Order - ' . date('d/m/Y h:i a');
    $strStore = isset($post['order_store_id']) ? $post['order_store_id'] : 0;
    $order_id = $post['order_id'] != '' ? $post['order_id'] : '';
    $DB = Connect();

    if ($order_id != '') {
        $updateq = "UPDATE order_request SET approve_status_audit='1',approve_status_admin='1',store_id='" . $strStore . "',order_name='" . $order_name . "',modified_date='" . date('Y-m-d H:i:s') . "',"
                . "modified_by='" . $strAdminID . "' WHERE id='" . $order_id . "'";
        $DB->query($updateq);

        $prodcutq = "UPDATE order_request_product SET status=2,modified_date='" . date('Y-m-d H:i:s') . "',"
                . "modified_by='" . $strAdminID . "' WHERE order_request_id='" . $order_id . "'";
        $DB->query($prodcutq);
    } else {
        $order_insert = "INSERT INTO order_request(order_name,created_date,created_by,store_id)"
                . " VALUES('" . $order_name . "','" . date('Y-m-d H:i:s') . "','" . $strAdminID . "','" . $strStore . "')";
        $DB->query($order_insert);
        $order_id = $DB->insert_id;
    }


    $quantity = isset($post['pdt_qty']) ? $post['pdt_qty'] : array();
    $map_id = isset($post['map_id']) ? $post['map_id'] : array();
    if (isset($post['pdt_id']) && is_array($post['pdt_id']) && count($post['pdt_id']) > 0) {
        foreach ($post['pdt_id'] as $pkey => $pvalue) {
            if ($pvalue != '' && isset($quantity[$pkey]) && $quantity[$pkey] != '') {

                if (isset($map_id[$pkey]) && $map_id[$pkey] > 0) {
                    $order_pdt_update = "UPDATE order_request_product SET product_id='" . $pvalue . "',quantity='" . $quantity[$pkey] . "',"
                            . "modified_date='" . date('Y-m-d H:i:s') . "',modified_by='" . $strAdminID . "',status='1'"
                            . " WHERE id='" . $map_id[$pkey] . "'";
                    $DB->query($order_pdt_update);
                } else {
                    $order_product_insert = "INSERT INTO order_request_product(order_request_id,product_id,quantity,created_date,created_by)"
                            . " VALUES('" . $order_id . "','" . $pvalue . "','" . $quantity[$pkey] . "','" . date('Y-m-d H:i:s') . "','" . $strAdminID . "')";
                    $DB->query($order_product_insert);
                }
            }
        }
    }
    $DB->close();
}
header('Location:manage_order_list.php');
?>