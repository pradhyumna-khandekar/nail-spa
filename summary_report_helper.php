<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php

function TimetoInt($val) {
    $time_array = explode(':', $val);
    $hours = (int) $time_array[0];
    $minutes = (int) $time_array[1];
    $seconds = (int) $time_array[2];
    $total_seconds = ($hours * 3600) + ($minutes * 60) + $seconds;
    return $total_seconds;
}

function Report_sale($strtoandfrom = '', $Store = '') {
    $final_array = array();
    $DB = Connect();

    $arraytofrom = explode("-", $strtoandfrom);

    $from = $arraytofrom[0];
    $datetime = new DateTime($from);
    $getfrom = $datetime->format('Y-m-d');


    $to = $arraytofrom[1];
    $datetime = new DateTime($to);
    $getto = $datetime->format('Y-m-d');

    if (!IsNull($from)) {
        $sqlTempfrom = " and Date(tblInvoiceDetails.OfferDiscountDateTime)>='" . $getfrom . "'";
        $sqlTempfrom1 = " and Date(tblAppointmentMembershipDiscount.DateTimeStamp)>='" . $getfrom . "'";
        $sqlTempfrom2 = " and Date(tblGiftVouchers.RedempedDateTime)>='" . $getfrom . "'";
        $sqlTempfrom3 = " and Date(tblPendingPayments.DateTimeStamp)>='" . $getfrom . "'";
        $sqlTempfrom4 = " and Date(tblGiftVouchers.Date)>='" . $getfrom . "'";
        $sqlTempfrom5 = " Date(tblCustomers.RegDate)>='" . $getfrom . "'";
        $sqlTempfrom6 = " and Date(tblCustomers.RegDate)<'" . $getfrom . "'";
        $sqlTempfrom7 = " and Date(tblAppointments.AppointmentDate)>='" . $getfrom . "'";
    }

    if (!IsNull($to)) {
        $sqlTempto = " and Date(tblInvoiceDetails.OfferDiscountDateTime)<='" . $getto . "'";
        $sqlTempto1 = " and Date(tblAppointmentMembershipDiscount.DateTimeStamp)<='" . $getto . "'";
        $sqlTempto2 = " and Date(tblGiftVouchers.RedempedDateTime)<='" . $getto . "'";
        $sqlTempto3 = " and Date(tblPendingPayments.DateTimeStamp)<='" . $getto . "'";
        $sqlTempto4 = " and Date(tblGiftVouchers.Date)<='" . $getto . "'";
        $sqlTempto5 = " and Date(tblCustomers.RegDate)<='" . $getto . "'";
        $sqlTempto7 = " and Date(tblAppointments.AppointmentDate)<='" . $getto . "'";
    }
    /*
     * get appointmetn id store wise
     */

    $append = "";

    $sqlservice = "SELECT AppointmentID,StoreID
		FROM `tblAppointments` 
		WHERE `Status` = 2 AND `IsDeleted` != 1 AND `FreeService` != 1
		AND AppointmentId IN
		(
			SELECT AppointmentId  
			FROM `tblInvoiceDetails` 
			WHERE `AppointmentDate` BETWEEN '" . $getfrom . " 00:00:00' AND '" . $getto . " 23:23:59' " . $append . "
                                                                                                )";


    $RSservice = $DB->query($sqlservice);
    if ($RSservice->num_rows > 0) {
        while ($rowservice = $RSservice->fetch_assoc()) {
            $queryresult[$rowservice['StoreID']][] = $rowservice['AppointmentID'];
            $apt_store_map[$rowservice['AppointmentID']] = $rowservice['StoreID'];
        }
    }

    $servicesale = array();
    $discountgiven = array();
    $customercount = array();
    $totalsale = array();


    if (isset($queryresult) && is_array($queryresult) && count($queryresult)) {
        foreach ($queryresult as $key => $value) {

            $gAmount_store = 0;
            $memamt = 0;
            $offamt = 0;
            $TotalRoundTotal = 0;


            /*
             * get new customer count
             */

            $sqlservicetyp = "select count(tblAppointments.CustomerID) as newcust, tblAppointments.StoreID from tblCustomers left join tblAppointments on tblAppointments.CustomerID = tblCustomers.CustomerID "
                    . "where " . $sqlTempfrom5 . $sqlTempto5 . " AND tblAppointments.AppointmentId IN (" . implode(',  ', $value) . ") AND tblAppointments.StoreID='" . $key . "'";
            $RSservice2 = $DB->query($sqlservicetyp);
            if ($RSservice2->num_rows > 0) {
                while ($rowservice2 = $RSservice2->fetch_assoc()) {
                    $newcntcust[$rowservice2["StoreID"]] = $rowservice2["newcust"];
                }
            }

            /*
             * Get New Customer Appointment
             */
            $sqlservicetyp = "select tblAppointments.AppointmentID,tblAppointments.StoreID from tblCustomers left join tblAppointments on tblAppointments.CustomerID = tblCustomers.CustomerID "
                    . "where " . $sqlTempfrom5 . $sqlTempto5 . " AND tblAppointments.AppointmentId IN (" . implode(',  ', $value) . ") AND tblAppointments.StoreID='" . $key . "'";
            $RSservice3 = $DB->query($sqlservicetyp);
            if ($RSservice3->num_rows > 0) {
                while ($rowservice3 = $RSservice3->fetch_assoc()) {
                    $newcntcust_appoint[$rowservice3["AppointmentID"]] = $rowservice3["AppointmentID"];
                }
            }

            /*
             * get service sale
             */
            $sqlservice = "SELECT sum(qty * ServiceAmount) as servicesale FROM `tblAppointmentsDetailsInvoice` WHERE AppointmentID IN(" . implode(',', $value) . ")";

            /* $RSservice = $DB->query($sqlservice);
              if ($RSservice->num_rows > 0) {
              $rowservice = $RSservice->fetch_assoc();
              $servicesale[$key] = $rowservice['servicesale'];
              } */
            $sqlservice = "SELECT inv.AppointmentId,inv.ServiceName,inv.Qty,inv.ServiceAmt,inv.DisAmt,inv.OfferAmt,inv.SubTotal,inv.RoundTotal FROM `tblInvoiceDetails` inv WHERE AppointmentID IN(" . implode(',', $value) . ")";

            $RSservice = $DB->query($sqlservice);
            if ($RSservice->num_rows > 0) {
                while ($rowservice = $RSservice->fetch_assoc()) {

                    $offer_amount = 0;
                    $amount_exculde_dis = 0;
                    if (!empty($rowservice['ServiceName'])) {
                        $ServiceName = explode(',', $rowservice['ServiceName']);
                        $Qty = explode(',', $rowservice['Qty']);
                        $ServiceAmt = explode(',', $rowservice['ServiceAmt']);
                        $DisAmt = explode(',', $rowservice['DisAmt']);
                        $count = count($ServiceName);
                        for ($i = 0; $i < $count; $i++) {

                            $offer_amount = str_replace(array('-', ' '), '', $rowservice['OfferAmt']);

                            if (isset($DisAmt[$i])) {
                                $amount_exculde_dis += ($ServiceAmt[$i] - $DisAmt[$i]);
                            } else {
                                $amount_exculde_dis += $ServiceAmt[$i];
                            }
                            //echo '<br>amount_exculde_dis=' . $amount_exculde_dis;
                        }


                        if ($offer_amount > 0) {
                            $amount_exculde_dis = $amount_exculde_dis - $offer_amount;
                        }

                        if ($amount_exculde_dis < 0) {
                            $amount_exculde_dis = 0;
                        }

                        $Store_id_apt = isset($apt_store_map[$rowservice['AppointmentId']]) ? $apt_store_map[$rowservice['AppointmentId']] : 0;
                        if ($Store_id_apt != 0) {
                            if (isset($servicesale[$Store_id_apt])) {
                                $servicesale[$Store_id_apt] += round($amount_exculde_dis, 2);
                            } else {
                                $servicesale[$Store_id_apt] = round($amount_exculde_dis, 2);
                            }
                        }

                        if (isset($newcntcust_appoint[$rowservice['AppointmentId']])) {
                            if (isset($new_cust_amount[$key])) {
                                $new_cust_amount[$key] += round($amount_exculde_dis, 2);
                            } else {
                                $new_cust_amount[$key] = round($amount_exculde_dis, 2);
                            }
                        } else {
                            if (isset($exist_cust_amount[$key])) {
                                $exist_cust_amount[$key] += round($amount_exculde_dis, 2);
                            } else {
                                $exist_cust_amount[$key] = round($amount_exculde_dis, 2);
                            }
                        }
                    }
                }
            }

            /*
             * get discount
             */
            $sqlservice = "SELECT sum(Amount) as amt FROM `tblGiftVouchers` WHERE Status = 1 and RedempedBy IN(" . implode(', ', $value) . ")" . $sqlTempfrom2 . $sqlTempto2;

            $RSservice = $DB->query($sqlservice);
            if ($RSservice->num_rows > 0) {
                $gAmount = $RSservice->fetch_assoc();

                if ($gAmount['amt'] == "") {
                    $gAmount = $gAmount_store + 0;
                } else {
                    $gAmount = $gAmount_store + $gAmount['amt'];
                }
            } else {
                $gAmount = $gAmount_store + 0;
            }



            $sqlservice = "SELECT DISTINCT AppointmentID,OfferID,OfferAmount as offamt,MembershipAmount as memamt,MembershipID FROM `tblAppointmentMembershipDiscount` WHERE AppointmentID IN(" . implode(', ', $value) . ")";

            $RSservice = $DB->query($sqlservice);
            if ($RSservice->num_rows > 0) {
                while ($result = $RSservice->fetch_assoc()) {
                    if ($result['offamt'] == '') {
                        $offamt = $offamt + 0;
                    } else {
                        $offamt = $offamt + $result['offamt'];
                    }
                    if ($result['memamt'] == '') {
                        $memamt = $memamt + 0;
                    } else {
                        $memamt = $memamt + $result['memamt'];
                    }
                    // echo '<br>offamt=' . $offamt . 'memamt=' . $memamt.'gAmount='.$gAmount;
                }
            }

            $discountgiven[$key] = $gAmount + $memamt + $offamt;



            /*
             * get customer count
             */
            $sqlservice = "SELECT count(CustomerID) as customercount FROM `tblAppointments` WHERE AppointmentID IN(" . implode(',', $value) . ")";
            $RSservice = $DB->query($sqlservice);
            if ($RSservice->num_rows > 0) {
                $rowservice = $RSservice->fetch_assoc();
                $customercount[$key] = $rowservice['customercount'];
            }
            /*
             * get total sale
             */
            $sqlservice1 = "SELECT tblInvoiceDetails.RoundTotal from tblInvoiceDetails where tblInvoiceDetails.AppointmentId IN (" . implode(',  ', $value) . ")";
            //echo $sqlservice1;exit;
            $RSservice1 = $DB->query($sqlservice1);
            if ($RSservice1->num_rows > 0) {
                while ($rowservice1 = $RSservice1->fetch_assoc()) {
                    $RoundTotal = $rowservice1["RoundTotal"];
                    if ($RoundTotal == "") {
                        $RoundTotal = "0.00";
                    } else {
                        $RoundTotal = $RoundTotal;
                    }
                    $TotalRoundTotal += $RoundTotal;
                }
            }
            $totalsale[$key] = $TotalRoundTotal;
        }
        /*
         * get service count
         */
        $SelectFreeService = "Select count(FreeService) as FreeService, StoreID from tblAppointments where FreeService = '1' " . $sqlTempfrom7 . $sqlTempto7 . " group by StoreID";

        $RSFreeSer = $DB->query($SelectFreeService);
        if ($RSFreeSer->num_rows > 0) {
            while ($rowFreeSer = $RSFreeSer->fetch_assoc()) {
                $FreeService[$rowFreeSer["StoreID"]] = $rowFreeSer["FreeService"];
            }
        }
    }

    $allstoreslist = select("* ", "tblStores ", " StoreID > 0");

    if (isset($allstoreslist) && is_array($allstoreslist) && count($allstoreslist)) {

        foreach ($allstoreslist as $skey => $svalue) {
            $store_name[$svalue['StoreID']] = $svalue;
            $sorting_service_amt[$svalue['StoreID']] = isset($servicesale[$svalue['StoreID']]) ? $servicesale[$svalue['StoreID']] : '0.00';
        }
        arsort($sorting_service_amt);
    }



    if (isset($sorting_service_amt) && is_array($sorting_service_amt) && count($sorting_service_amt)) {
        foreach ($sorting_service_amt as $skey => $svalue) {
            $total_cust = isset($customercount[$skey]) ? $customercount[$skey] : 0;
            $new_cust = isset($newcntcust[$skey]) ? $newcntcust[$skey] : 0;

            $final_array[$skey]['new_cust_amount'] = isset($new_cust_amount[$skey]) ? $new_cust_amount[$skey] : 0;
            $final_array[$skey]['exist_cust_amount'] = isset($exist_cust_amount[$skey]) ? $exist_cust_amount[$skey] : 0;

            $final_array[$skey]['store_name'] = isset($store_name[$skey]) ? $store_name[$skey]['StoreName'] : '';
            $final_array[$skey]['service_sale'] = isset($servicesale[$skey]) ? $servicesale[$skey] : '0.00';
            $final_array[$skey]['discount'] = isset($discountgiven[$skey]) ? $discountgiven[$skey] : 0;
            $final_array[$skey]['total_sale'] = isset($totalsale[$skey]) ? $totalsale[$skey] : 0;
            $final_array[$skey]['new_cust_cnt'] = isset($new_cust) ? $new_cust : 0;

            $final_array[$skey]['existing_cust_cnt'] = ($total_cust) - ($new_cust);
            if ($total_cust > 0) {
                $final_array[$skey]['arpu'] = $final_array[$skey]['total_sale'] / $total_cust;
            } else {
                $final_array[$skey]['arpu'] = 0;
            }
        }
    }

    $DB->close();
    return $final_array;
}

function Avg_time_spent($strtoandfrom = '') {
    $DB = Connect();
    $final_array = array();
    $arraytofrom = explode("-", $strtoandfrom);

    $from = $arraytofrom[0];
    $datetime = new DateTime($from);
    $getfrom = $datetime->format('Y-m-d');


    $to = $arraytofrom[1];
    $datetime = new DateTime($to);
    $getto = $datetime->format('Y-m-d');

    if (!IsNull($from)) {
        $sqlTempfrom = " and Date(tblAppointments.AppointmentDate)>=Date('" . $getfrom . "')";
    }

    if (!IsNull($to)) {
        $sqlTempto = " and Date(tblAppointments.AppointmentDate)<=Date('" . $getto . "')";
    }

    $sql = "select distinct(tblAppointments.CustomerID), tblAppointments.StoreID,timediff(tblAppointments.AppointmentCheckInTime,tblAppointments.AppointmentCheckOutTime) as spenttime,tblAppointments.AppointmentID
from tblAppointments where tblAppointments.IsDeleted!='1' and tblAppointments.AppointmentDate!='NULL' and tblAppointments.Status='2'  " .
            $sqlTempfrom . $sqlTempto . "";
    $RS = $DB->query($sql);
    if ($RS->num_rows > 0) {

        while ($row = $RS->fetch_assoc()) {
            $result_data[] = $row;
        }
    }

    if (isset($result_data) && is_array($result_data) && count($result_data) > 0) {
        foreach ($result_data as $key => $value) {
            $spenttime = $value["spenttime"];

            $spenttimet = str_replace("-", "", $spenttime);

            /* if (isset($timeRes[$value['StoreID']])) {
              $secs = strtotime($timeRes[$value['StoreID']]['time']) - strtotime("00:00:00");
              $timeRes[$value['StoreID']]['count'] += 1;
              } else {
              $start_time = '00:00:00';
              $secs = strtotime($start_time) - strtotime("00:00:00");
              $timeRes[$value['StoreID']]['count'] = 1;
              }
              $timeRes[$value['StoreID']]['time'] = date("H:i:s", strtotime($spenttimet) + $secs); */

            $timeRes[$value['StoreID']][] = $spenttimet;
        }
    }

    if (isset($timeRes) && is_array($timeRes) && count($timeRes) > 0) {
        foreach ($timeRes as $tkey => $tvalue) {
            if (count($tvalue) > 0) {
                /* $AverageTime = TimetoInt($tvalue['time']) / $tvalue['count'];
                  $final_array[$tkey]['avg_time'] = gmdate("H:i:s", $AverageTime); */

                $AverageTime = date('H:i:s', array_sum(array_map('strtotime', $tvalue)) / count($tvalue));
                $final_array[$tkey]['avg_time'] = $AverageTime;
            }
        }
    }

    $DB->close();
    return $final_array;
}

function Membership_data($strtoandfrom = '') {
    $DB = Connect();
    $final_array = array();
    $arraytofrom = explode("-", $strtoandfrom);

    $from = $arraytofrom[0];
    $datetime = new DateTime($from);
    $getfrom = $datetime->format('Y-m-d');


    $to = $arraytofrom[1];
    $datetime = new DateTime($to);
    $getto = $datetime->format('Y-m-d');


    if (!IsNull($from)) {
        $sqlTempfrom = " and Date(tblInvoiceDetails.OfferDiscountDateTime)>=Date('" . $getfrom . "')";
        $sqlTempfrom1 = " and Date(tblCustomerMemberShip.ExpireDate)>=Date('" . $getfrom . "')";
        $sqlTempfrom2 = " and tblAppointments.AppointmentDate>=Date('" . $getfrom . "')";
        $sqlTempfrom3 = " and tblCustomerMemberShip.StartDay>=Date('" . $getfrom . "')";
        $sqlTempfrom4 = " and Date(tblCustomers.RegDate)>=Date('" . $getfrom . "')";
    }

    if (!IsNull($to)) {
        $sqlTempto = " and Date(tblInvoiceDetails.OfferDiscountDateTime)<=Date('" . $getto . "')";
        $sqlTempto1 = " and Date(tblCustomerMemberShip.ExpireDate)<=Date('" . $getto . "')";
        $sqlTempto2 = " and tblAppointments.AppointmentDate<=Date('" . $getto . "')";
        $sqlTempto3 = " and tblCustomerMemberShip.StartDay<=Date('" . $getto . "')";
        $sqlTempto4 = " and Date(tblCustomers.RegDate)<=Date('" . $getto . "')";
    }
    $selp = select("*", "tblStores", "StoreID > '0'");

    foreach ($selp as $vat) {
        $newcust = 0;
        $Totalmemamtfirst = 0;
        $in_apt_ids = '';
        $appointment_ids = array();
        $sqlservice = "SELECT DISTINCT tblCustomers.`CustomerID`,tblAppointments.AppointmentID FROM  tblCustomers "
                . " LEFT JOIN tblAppointments ON tblCustomers.`CustomerID` = tblAppointments.CustomerID "
                . " LEFT JOIN tblCustomerMemberShip ON tblCustomerMemberShip.`CustomerID` = tblCustomers.CustomerID "
                . " WHERE  tblAppointments.StoreID =  '" . $vat['StoreID'] . "' AND tblAppointments.memberid !=  '0' AND tblCustomerMemberShip.Status =  '1' "
                . " AND tblCustomerMemberShip.RenewStatus =  '0' AND tblAppointments.Status=2"
                . " $sqlTempfrom3 $sqlTempto3  AND tblCustomerMemberShip.StartDay = tblAppointments.AppointmentDate";
        $RSservice = $DB->query($sqlservice);
        if ($RSservice->num_rows > 0) {
            while ($rowservice = $RSservice->fetch_assoc()) {
                $newcust = $newcust + 1;
                $appointment_ids[$rowservice['AppointmentID']] = $rowservice['AppointmentID'];
            }
        }
        if (isset($appointment_ids) && is_array($appointment_ids) && count($appointment_ids) > 0) {

            $in_apt_ids = implode(",", $appointment_ids);
        }
        if ($in_apt_ids != '') {

            $toatlseramt = "";

            $setmemamount123 = "SELECT tblInvoiceDetails.Membership_Amount FROM tblInvoiceDetails "
                    . " WHERE AppointmentId IN(" . $in_apt_ids . ")";

            $setmemamount = $DB->query($setmemamount123);

            if ($setmemamount->num_rows > 0) {

                while ($rowsetmemamount = $setmemamount->fetch_assoc()) {


                    $Membership_Amount = $rowsetmemamount["Membership_Amount"];
                    $memamtfirst = explode(",", $Membership_Amount);

                    $memamtfirst = str_replace("+", "", $Membership_Amount);
                    $memamtfirst2 = str_replace(".00", "", $memamtfirst);
                    $memamtfirst3 = str_replace("+", "", $memamtfirst2);
                    $memamtfirst4 = str_replace(",", "", $memamtfirst3);
                    $memamtfirst5 = str_replace(".00", "", $memamtfirst4);

                    // $memamtfirst=str_replace(",", "", $Membership_Amount);
                    if ($memamtfirst5 == '') {
                        $memamtfirst5 = "0.00";
                    }


                    $Totalmemamtfirst = $Totalmemamtfirst + $memamtfirst5;
                }
            }
        }
        $final_array[$vat['StoreID']]['new_member'] = isset($newcust) ? $newcust : 0;
        $final_array[$vat['StoreID']]['new_member_amount'] = isset($Totalmemamtfirst) ? $Totalmemamtfirst : 0;
    }

    $DB->close();
    return $final_array;
}

?>