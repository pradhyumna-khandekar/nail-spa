<?php require_once 'setting.fya'; ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
$test = "";
if (isset($_POST["id"])) {
    $test = $_POST["id"];
}


$DB = Connect();

$sql = "SELECT * FROM super_gift_voucher WHERE status=1 AND GVDateFrom <= '" . date('Y-m-d') . "'";
$RS = $DB->query($sql);
?>
<div class="form-group"><label class="col-sm-3 control-label">Available Super Gift Vouchers <span>*</span></label>
    <div class="col-sm-3 ">
        <select id="giftvoucher"  class="form-control giftvoucher required" name="giftvoucher">
            <option value="">Select Super GV</option>
            <?php
            if ($RS->num_rows > 0) {
                while ($row = $RS->fetch_assoc()) {
                    $GV_StoreID = $row["StoreID"];
                    $store_id_arr = explode(",", $GV_StoreID);
                    if ($row['GVAmount'] != "" && date('Y-m-d') <= $row['GVDateTo'] && in_array($test, $store_id_arr)) {
                        ?>
                        <option value="<?= $row['id'] ?>"><?= $row['GVcode'] ?></option>
                        <?php
                    }
                }
            }
            ?>
        </select>
    </div>
</div>