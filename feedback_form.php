<?php
require_once("setting.fya");
$mail_body = require_once("feedback_admin_rating_temp.php");

$strimage = "app-icon (2).png";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $DB = Connect();

    $post_data = isset($_POST['feedback']) ? $_POST['feedback'] : array();

    $post_data['created_date'] = date('Y-m-d H:i:s');
    $total_sale_amount = 0;
    if (isset($post_data) && is_array($post_data) && count($post_data) > 0) {
        foreach ($post_data as $key => $value) {
            $col_heading[] = $key;
            $col_val[] = "'" . trim($value) . "'";
        }
        $field_values = implode(',', $col_heading);
        $data_values = implode(',', $col_val);

        $insert_sql = "INSERT into customer_feedback (" . $field_values . ") VALUES(" . $data_values . ")";

        $DB->query($insert_sql);

        $send_emails = array('amynnats@gmail.com', 'experiencenailspa@gmail.com', 'abhijeet@nailspaexperience.com', 'abhijeetcshrivastav@gmail.com');
        /*
         * If Rating is less than 3 start send email to amynnats@gmail.com, experiencenailspa@gmail.com
         */
        if (isset($send_emails) && is_array($send_emails) && count($send_emails) > 0) {
            if (isset($post_data['customer_id']) && $post_data['customer_id'] != '') {
                $cusomer_data = select("*", "tblCustomers", "CustomerID = '" . $post_data['customer_id'] . "'");

                $appointment_data = select("*", "tblAppointments", "AppointmentID = '" . $post_data['appointment_id'] . "'");

                $customer_name = isset($cusomer_data[0]['CustomerFullName']) ? $cusomer_data[0]['CustomerFullName'] : '';
                $customer_mobile = isset($cusomer_data[0]['CustomerMobileNo']) ? $cusomer_data[0]['CustomerMobileNo'] : '';
                $rating = isset($post_data['rating']) ? $post_data['rating'] : '';
                $comment = isset($post_data['comment']) ? $post_data['comment'] : '';
                $appointment_date = isset($appointment_data[0]['AppointmentDate']) ? date('d/m/Y', strtotime($appointment_data[0]['AppointmentDate'])) : '';


                /*
                 * Get Store Name
                 */
                $store_name = '';
                if (isset($appointment_data[0]['StoreID']) && $appointment_data[0]['StoreID'] != '') {
                    $store_name_data = select("*", "tblStores", "StoreID = '" . $appointment_data[0]['StoreID'] . "'");
                    if (isset($store_name_data) && is_array($store_name_data) && count($store_name_data) > 0) {
                        $store_name = $store_name_data[0]['StoreName'];
                    }
                }

                /*
                 * Get Service Name
                 */
                $apt_service_name = '';
                $service_name_data = select("*", "tblInvoiceDetails", "AppointmentId = '" . $post_data['appointment_id'] . "'");

                if (isset($service_name_data) && is_array($service_name_data) && count($service_name_data) > 0) {
                    $service_name = isset($service_name_data[0]['ServiceName']) ? trim($service_name_data[0]['ServiceName'], ',') : '';
                    $total_sale_amount = isset($service_name_data[0]['RoundTotal']) ? number_format($service_name_data[0]['RoundTotal'], 2) : 0;
                    if ($service_name != '') {
                        $ser_qry = "SELECT * FROM tblServices WHERE ServiceID IN(" . $service_name . ")";
                        $ser_qry_exe = $DB->query($ser_qry);
                        if ($ser_qry_exe->num_rows > 0) {
                            while ($service_row = $ser_qry_exe->fetch_assoc()) {
                                $apt_service[$service_row['ServiceID']] = $service_row['ServiceName'];
                            }
                        }
                    }
                }

                if (isset($apt_service) && is_array($apt_service) && count($apt_service) > 0) {
                    $apt_service_name = implode(",", $apt_service);
                }

                /*
                 * Get Appointment Assign Employee
                 */
                $apt_employee_data = select("*", "tblAppointmentAssignEmployee", "AppointmentID = '" . $post_data['appointment_id'] . "'");
                if (isset($apt_employee_data) && is_array($apt_employee_data) && count($apt_employee_data) > 0) {
                    foreach ($apt_employee_data as $ekey => $evalue) {
                        $employee_ids[$evalue['MECID']] = $evalue['MECID'];
                    }
                }

                if (isset($employee_ids) && is_array($employee_ids) && count($employee_ids) > 0) {
                    $emp_in_ids = implode(",", $employee_ids);
                    if ($emp_in_ids != '') {
                        $emp_qry = "SELECT * FROM tblEmployees WHERE EID IN(" . $emp_in_ids . ")";
                        $emp_qry_exe = $DB->query($emp_qry);
                        if ($emp_qry_exe->num_rows > 0) {
                            while ($emp_row = $emp_qry_exe->fetch_assoc()) {
                                $apt_employee[$emp_row['EID']] = $emp_row['EmployeeName'];
                            }
                        }
                    }
                }

                $apt_employee_name = '';
                if (isset($apt_employee) && is_array($apt_employee) && count($apt_employee) > 0) {
                    $apt_employee_name = implode(",", $apt_employee);
                }

                $rating_names = array(
                    1 => 'Poor',
                    2 => 'Average',
                    3 => 'Good',
                    4 => 'Excellent'
                );
                $service_quality = isset($post_data['service_quality']) && isset($rating_names[$post_data['service_quality']]) ? $rating_names[$post_data['service_quality']] : '';
                $value_for_money = isset($post_data['value_for_money']) && isset($rating_names[$post_data['value_for_money']]) ? $rating_names[$post_data['value_for_money']] : '';
                $salon_atmosphere = isset($post_data['salon_atmosphere']) && isset($rating_names[$post_data['salon_atmosphere']]) ? $rating_names[$post_data['salon_atmosphere']] : '';
                $staff_presentation_attitude = isset($post_data['staff_presentation_attitude']) && isset($rating_names[$post_data['staff_presentation_attitude']]) ? $rating_names[$post_data['staff_presentation_attitude']] : '';
                $cleanliness = isset($post_data['cleanliness']) && isset($rating_names[$post_data['cleanliness']]) ? $rating_names[$post_data['cleanliness']] : '';

                $mail_body = isset($service_quality) ? str_replace('{service_quality}', ucwords($service_quality), $mail_body) : $mail_body;
                $mail_body = isset($value_for_money) ? str_replace('{value_for_money}', ucwords($value_for_money), $mail_body) : $mail_body;
                $mail_body = isset($salon_atmosphere) ? str_replace('{salon_atmosphere}', ucwords($salon_atmosphere), $mail_body) : $mail_body;
                $mail_body = isset($staff_presentation_attitude) ? str_replace('{staff_presentation_attitude}', ucwords($staff_presentation_attitude), $mail_body) : $mail_body;
                $mail_body = isset($cleanliness) ? str_replace('{cleanliness}', ucwords($cleanliness), $mail_body) : $mail_body;

                $mail_body = isset($customer_name) ? str_replace('{customer_name}', ucwords($customer_name), $mail_body) : $mail_body;
                $mail_body = isset($customer_mobile) ? str_replace('{customer_mobile}', $customer_mobile, $mail_body) : $mail_body;
                $mail_body = isset($appointment_date) ? str_replace('{appointment_date}', $appointment_date, $mail_body) : $mail_body;
                $mail_body = isset($store_name) ? str_replace('{appointment_store}', $store_name, $mail_body) : $mail_body;
                $mail_body = isset($apt_service_name) ? str_replace('{service_name}', $apt_service_name, $mail_body) : $mail_body;
                $mail_body = isset($apt_employee_name) ? str_replace('{employee_name}', $apt_employee_name, $mail_body) : $mail_body;
                $mail_body = isset($total_sale_amount) ? str_replace('{sale_amount}', $total_sale_amount, $mail_body) : $mail_body;

              
                $rating_data['service_quality'] = isset($post_data['service_quality']) && $post_data['service_quality'] != '' ? $post_data['service_quality'] : '0';
                $rating_data['value_for_money'] = isset($post_data['value_for_money']) && $post_data['value_for_money'] != '' ? $post_data['value_for_money'] : '0';
                $rating_data['salon_atmosphere'] = isset($post_data['salon_atmosphere']) && $post_data['salon_atmosphere'] != '' ? $post_data['salon_atmosphere'] : '0';
                $rating_data['staff_presentation_attitude'] = isset($post_data['staff_presentation_attitude']) && $post_data['staff_presentation_attitude'] != '' ? $post_data['staff_presentation_attitude'] : '0';
                $rating_data['cleanliness'] = isset($post_data['cleanliness']) && $post_data['cleanliness'] != '' ? $post_data['cleanliness'] : '0';

                if (isset($rating_data) && is_array($rating_data) && count($rating_data) > 0 && (in_array('1', $rating_data) || in_array('2', $rating_data))) {
                    foreach ($send_emails as $ekey => $evalue) {
                        
                        $strTo = $evalue;
                        $strFrom = "invoice@nailspaexperience.com";
                        $strSubject = "Customer Feedback";
                        /*
                         * Insert Into message table
                         */
                        $col_val = array('ToEmail', 'FromEmail', 'Subject', 'Body', 'DateTime', 'Status', 'created_date', 'created_by', 'description', 'CustomerID');
                        $user['ToEmail'] = "'" . $strTo . "'";
                        $user['FromEmail'] = "'" . $strFrom . "'";
                        $user['Subject'] = "'" . $strSubject . "'";
                        $user['Body'] = "'" . $mail_body . "'";

                        $user['DateTime'] = "'" . date('Y-m-d H:i:s') . "'";
                        $user['Status'] = "'0'";
                        $user['created_date'] = "'" . date('Y-m-d H:i:s') . "'";
                        $user['created_by'] = '0';
                        $user['description'] = "'Customer Feedback Rating'";
                        $user['CustomerID'] = "'0'";
                        $field_values = implode(',', $col_val);
                        $data_values = implode(',', $user);
                        $insert_sql = "INSERT into tblEmailMessages (" . $field_values . ") VALUES(" . $data_values . ")";
                        $DB->query($insert_sql);
                        $last_email_id = $DB->insert_id;
                        unset($user);

                        $strbody1 = $mail_body;
                        $headers = "From: $strFrom\r\n";
                        $headers .= "Content-type: text/html\r\n";
                        $strBodysa = AntiFilter1($strbody1);

                        // Mail sending 
                        $retval = mail($strTo, $strSubject, $strBodysa, $headers);

                        if ($retval == true) {
                            $update_qry = "UPDATE tblEmailMessages SET Status='1',DateOfSending='" . date('Y-m-d H:i:s') . "'"
                                    . " WHERE ID='" . $last_email_id . "'";
                            $DB->query($update_qry);
                        } else {
                            $update_qry = "UPDATE tblEmailMessages SET Status='2'"
                                    . " WHERE ID='" . $last_email_id . "'";
                            $DB->query($update_qry);
                        }
                    }
                }
            }
        }
        echo "<script> alert('Thank You For Submitting Your Response!');
                                              location.href = 'http://nailspaexperience.com/';
                                                 </script>";
    } else {
        echo "<script> alert('Fill Mandatory Fields!');
                                              location.href = 'feedback_form.php';
                                                 </script>";
    }
    $DB->close();
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <link href="feedback/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <body>

        <style type="text/css">
            .advt {z-index: 999999999999;
                   background-color: #ffffffa3;
                   width: 728px;
                   height: 100px;
                   color: black;
                   font-weight: bold;
                   font-size: 20px;
                   position: fixed;
                   left: 10px;
                   margin-top: 20px;
                   bottom: 1px;
            }
            .adsense_btn_close,.adsense_btn_info{font-size:12px;color:#fff;height:20px;width:20px;vertical-align:middle;text-align:center;background:#000;top:4px;left:4px;position:absolute;z-index:99999999;font-family:Georgia;cursor:pointer;line-height:18px}
        </style>

        <div class="content">
            <!---728x90--->
            <div class="main">
                <img src="feedback/images/nailspa.png" style="width:100px; display:block; margin:0 auto">
                <form action="feedback_form.php" method="post">
                    <h3>Dear <?php echo $name; ?> </h3>
                    <p>Help us to serve you better by giving your valuable suggestions and feedback.</p>

                    <h3>Please rate the following</h3>
                    <input type="hidden" name="feedback[customer_id]" value="<?php echo isset($_GET['customer_id']) ? $_GET['customer_id'] : '0'; ?>"/>
                    <input type="hidden" name="feedback[appointment_id]" value="<?php echo isset($_GET['apt_id']) ? $_GET['apt_id'] : '0'; ?>"/>
                    <input type="hidden" name="feedback[feedback_mode]" value="<?php echo isset($_GET['src']) ? $_GET['src'] : '0'; ?>"/>
                    <h5>Service Quality</h5>
                    <div class="radio-btns">
                        <div class="swit">								
                            <div class="check_box_one"> <div class="radio"> <label><input type="radio" class="feedback" name="feedback[service_quality]" value="4" required="required"><i></i>Excellent</label> </div></div>
                            <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="feedback[service_quality]" value="3" required="required"><i></i>Good</label> </div></div>
                            <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="feedback[service_quality]" value="2" required="required"><i></i>Average</label> </div></div>
                            <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="feedback[service_quality]" value="1" required="required"><i></i>Poor</label> </div></div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <h5>Value for money</h5>
                    <div class="radio-btns">
                        <div class="swit">								
                            <div class="check_box_one"> <div class="radio"> <label><input type="radio" class="feedback" name="feedback[value_for_money]" value="4"><i></i>Excellent</label> </div></div>
                            <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="feedback[value_for_money]" value="3"><i></i>Good</label> </div></div>
                            <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="feedback[value_for_money]" value="2"><i></i>Average</label> </div></div>
                            <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="feedback[value_for_money]" value="1"><i></i>Poor</label> </div></div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <h5>Salon Atmosphere</h5>
                    <div class="radio-btns">
                        <div class="swit">								
                            <div class="check_box_one"> <div class="radio"> <label><input type="radio" class="feedback" name="feedback[salon_atmosphere]" value="4"><i></i>Excellent</label> </div></div>
                            <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="feedback[salon_atmosphere]" value="3"><i></i>Good</label> </div></div>
                            <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="feedback[salon_atmosphere]" value="2"><i></i>Average</label> </div></div>
                            <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="feedback[salon_atmosphere]" value="1"><i></i>Poor</label> </div></div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <h5>Staff Presentation & Attitude</h5>
                    <div class="radio-btns">
                        <div class="swit">								
                            <div class="check_box_one"> <div class="radio"> <label><input type="radio" class="feedback" name="feedback[staff_presentation_attitude]" value="4"><i></i>Excellent</label> </div></div>
                            <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="feedback[staff_presentation_attitude]" value="3"><i></i>Good</label> </div></div>
                            <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="feedback[staff_presentation_attitude]" value="2"><i></i>Average</label> </div></div>
                            <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="feedback[staff_presentation_attitude]" value="1"><i></i>Poor</label> </div></div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <h5>Cleanliness</h5>
                    <div class="radio-btns">
                        <div class="swit">								
                            <div class="check_box_one"> <div class="radio"> <label><input type="radio" class="feedback" name="feedback[cleanliness]" value="4"><i></i>Excellent</label> </div></div>
                            <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="feedback[cleanliness]" value="3"><i></i>Good</label> </div></div>
                            <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="feedback[cleanliness]" value="2"><i></i>Average</label> </div></div>
                            <div class="check_box"> <div class="radio"> <label><input type="radio" class="feedback" name="feedback[cleanliness]" value="1"><i></i>Poor</label> </div></div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div id="div" style="display:none">
                        <h5>Whould You Recommend Your Friends and Family</h5>
                        <div class="radio-btns">
                            <div class="swit">								
                                <div class="check_box_one"> <div class="radio"> <label><input type="radio" name="recommend" value="Yes"><i></i>Yes</label> </div></div>
                                <div class="check_box"> <div class="radio"> <label><input type="radio" name="recommend" value="No"><i></i>No</label> </div></div>

                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                    <input type="submit" value="Send Feedback">
                </form>
            </div>

        </div>


    </body>
</html>