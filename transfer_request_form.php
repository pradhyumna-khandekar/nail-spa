<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
$strPageTitle = "Customer Details | NailSpa";
$strDisplayTitle = "Customer Details for NailSpa";
$strMenuID = "10";
$strMyTable = "tblAppointments";
$strMyTableID = "AppointmentID";
$strMyField = "AppointmentDate";
$strMyActionPage = "DisplayNonCustomerCount.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <?php require_once("incChart-SalonDashboard.fya"); ?>
        <?php /* <script type="text/javascript" src="assets/widgets/modal/modal.js"></script> */ ?>
        <link rel="stylesheet" type="text/css" href="assets/widgets/chosen/chosen.css">
        <script type="text/javascript" src="assets/widgets/chosen/chosen.js"></script>
        <script type="text/javascript" src="assets/widgets/chosen/chosen-demo.js"></script>
        <script>
            function LoadStoreEmp()
            {

                valuable = [];
                var store = $('#from_store').val();

                $.ajax({
                    type: 'POST',
                    url: "get_store_emp.php",
                    data: {
                        store_id: store
                    },
                    success: function (response) {
                        //alert(response)
                        $("#asmita1").html("");
                        $("#asmita1").html(response);
                        $(".chosen-select").chosen();
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $("#asmita1").html("<center><font color='red'><b>Please try again after some time</b></font></center>");
                        return false;
                        //alert (response);
                    }
                });
            }
        </script>
    </head>

    <body>

        <div id="sb-site">


            <?php // require_once("incOpenLayout.fya");   ?>
            <!----------commented by gandhali 1/9/18---------------->


            <?php require_once("incLoader.fya"); ?>


            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <script type="text/javascript" src="assets/widgets/datatable/date-euro.js"></script>
                <script type="text/javascript">
            /* Datatables responsive */
            $(document).ready(function () {
                $('#datatable-responsive-date').DataTable({
                    "order": [[0, "desc"]],
                    columnDefs: [{type: 'date-euro', targets: 0}]
                });
            });
            $(document).ready(function () {
                $('.dataTables_filter input').attr("placeholder", "Search...");
            });


            function proceed_delete(transfer_id) {
                if (confirm('Are you sure you want to Delete Request?')) {
                    $.ajax({
                        url: "insert_transfer_req.php",
                        type: 'post',
                        data: {"transfer_id": transfer_id, "type": '4'},
                        success: function (response) {
                            if (response == 1) {
                                $msg = "Request Deleted Successfully!";
                                alert($msg);
                                window.location.reload(true);
                            } else {
                                $msg = "Failed To Delete!";
                                alert($msg);
                                window.location.reload(true);
                            }
                        }
                    });
                }
            }

                </script>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>

                        <div class="modal fade" id="bulk_email_modal" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Employee Request</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" class="form-horizontal bordered-row" role="form" action="insert_transfer_req.php">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Store Name<span>*</span></label>
                                                <div class="col-sm-8">
                                                    <?php
                                                    if (isset($strStore) && $strStore != '') {
                                                        $all_store = select("*", "tblStores", "Status ='0' AND StoreID!='" . $strStore . "'");
                                                    } else {
                                                        $all_store = select("*", "tblStores", "Status ='0'");
                                                    }
                                                    ?>
                                                    <select class="form-control required"  name="from_store" id="from_store" onChange="LoadStoreEmp();">
                                                        <option value="">--Select Store--</option>
                                                        <?php
                                                        if (isset($all_store) && is_array($all_store) && count($all_store) > 0) {
                                                            foreach ($all_store as $rowk => $row2) {
                                                                $strStoreName = $row2["StoreName"];
                                                                $strStoreID = $row2["StoreID"];
                                                                ?>
                                                                <option value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <span id="asmita1"></span>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-6">
                                                    <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Submit</span></button>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="panel">

                            <div class="panel-body">
                                <div class="example-box-wrapper">

                                    <div id="normal-tabs-1">
                                        <span class="form_result">&nbsp; <br>
                                        </span>

                                        <h4 class="title-hero"><center><?php echo 'Employee Transfer : Sent Request to other store | NailSpa' ?></center></h4>

                                        <a class="btn btn-alt btn-hover btn-warning" data-toggle="modal" data-target="#bulk_email_modal" href="#" id="reconcillation_confirm">Add Request</a> &nbsp;&nbsp;&nbsp;

                                        <?php
                                        $request_data = select("*", "emp_transfer_request", "status=1 AND to_store='" . $strStore . "'");
                                        $store = select("*", "tblStores", "StoreID > 0");
                                        if (isset($store) && is_array($store) && count($store) > 0) {
                                            foreach ($store as $skey => $svalue) {
                                                $store_data[$svalue['StoreID']] = $svalue;
                                            }
                                        }

                                        $emp = select("*", "tblEmployees", "EID > 0");
                                        if (isset($emp) && is_array($emp) && count($emp) > 0) {
                                            foreach ($emp as $ekey => $evalue) {
                                                $emp_data[$evalue['EID']] = $evalue;
                                            }
                                        }

                                        $admin = select("*", "tblAdmin", "AdminID > 0");
                                        if (isset($admin) && is_array($admin) && count($admin) > 0) {
                                            foreach ($admin as $akey => $avalue) {
                                                $admin_data[$avalue['AdminID']] = $avalue;
                                            }
                                        }
                                        ?>

                                        <table id="datatable-responsive-date" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Request date</th>
                                                    <th>Request By</th>
                                                    <th>Store Name</th>
                                                    <th>Employee</th>
                                                    <th>Status</th>
                                                    <th>Approve date</th>
                                                    <th>Approve By</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Request date</th>
                                                    <th>Request By</th>
                                                    <th>Store Name</th>
                                                    <th>Employee</th>
                                                    <th>Status</th>
                                                    <th>Approve date</th>
                                                    <th>Approve By</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                <?php
                                                if (isset($request_data) && is_array($request_data) && count($request_data) > 0) {
                                                    foreach ($request_data as $rkey => $rvalue) {
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                <?php echo isset($rvalue['request_date']) && $rvalue['request_date'] != '0000-00-00 00:00:00' ? date('d/m/Y h:i a', strtotime($rvalue['request_date'])) : ''; ?> 

                                                            </td>
                                                            <td>
                                                                <?php echo isset($admin_data[$rvalue['request_by']]) ? $admin_data[$rvalue['request_by']]['AdminFullName'] : ''; ?>
                                                            </td>
                                                            <td><?php echo isset($store_data[$rvalue['from_store']]) ? $store_data[$rvalue['from_store']]['StoreName'] : ''; ?></td>
                                                            <td><?php echo isset($emp_data[$rvalue['emp_id']]) ? $emp_data[$rvalue['emp_id']]['EmployeeName'] : ''; ?></td>
                                                            <td>
                                                                <?php
                                                                if ($rvalue['request_status'] == '1') {
                                                                    echo "<span style='color:blue;'>Pending</span>";
                                                                } else if ($rvalue['request_status'] == '2') {
                                                                    echo "<span class='text-success'>Approved</span>";
                                                                } else if ($rvalue['request_status'] == '3') {
                                                                    echo "<span class='text-danger'>Rejected</span>";
                                                                }
                                                                ?>
                                                            </td>

                                                            <td>
                                                                <?php if ($rvalue['request_status'] != '1') { ?>
                                                                    <?php echo isset($rvalue['approved_date']) && $rvalue['approved_date'] != '0000-00-00 00:00:00' ? date('d/m/Y h:i a', strtotime($rvalue['approved_date'])) : ''; ?>
                                                                <?php } ?>
                                                            </td>
                                                            <td>
                                                                <?php if ($rvalue['request_status'] != '1') { ?>
                                                                    <?php echo isset($admin_data[$rvalue['approved_by']]) ? $admin_data[$rvalue['approved_by']]['AdminFullName'] : ''; ?>
                                                                <?php } ?>
                                                            </td>
                                                            <td>
                                                                <?php if ($rvalue['request_status'] == '1') { ?>
                                                                    <a class="btn btn-link font-red" href="#" id="delete_request" onclick="proceed_delete('<?php echo $rvalue['id']; ?>');">Delete</a> &nbsp;&nbsp;&nbsp;
                                                                <?php } ?>
                                                            </td>

                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php require_once 'incFooter.fya'; ?>
        </div>


    </body>
</html>
