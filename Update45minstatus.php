<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php

$DB = Connect();
$app = $_POST["app"];

/*
 * Get Appointment Service
 */
$send_approve_mail = 0;
$apt_service = select("ServiceID", "tblAppointmentsDetailsInvoice", "AppointmentID='" . $app . "'");
$apt_data = select("*", "tblAppointments", "AppointmentID='" . $app . "'");
if (isset($apt_data) && is_array($apt_data) && count($apt_data) > 0) {
    if (isset($apt_service) && is_array($apt_service) && count($apt_service) > 0) {
        foreach ($apt_service as $skey => $svalue) {
            $apt_service = select("ServiceID,is_concent", "tblServices", "ServiceID='" . $svalue['ServiceID'] . "'");
            if (isset($apt_service) && is_array($apt_service) && count($apt_service) > 0) {
                if ($apt_service[0]['is_concent'] == 1) {
                    $send_approve_mail = 1;
                }
            }
        }
    }

    // $send_approve_mail = 1;
    if ($send_approve_mail == 1) {

        $concent_data = select("*", "tblnotification_msg", "status = 1 AND message_type='service_concent_form'");

        $message = file_get_contents('ConcentapproveTemplate.php');
        $message = str_replace("[\]", '', $message);

        $approve_row = '<td width="20%" align="left" style="padding-left: 19px;padding-bottom: 15px;">';
        $approve_row .= '<a target="_blank" style="background-color: #3f7900;color: #fff;padding: 5px;" href="https://pos.nailspaexperience.com/og/approve_concent.php?apt_id=' . $app . '">I APPROVE</a>';
        $approve_row .= '</td>';


        $strCustomerID = $apt_data[0]['CustomerID'];
        $seldatac = select("CustomerEmailID,CustomerFullName,CustomerMobileNo,FirstName,CustomerLocation", "tblCustomers", "CustomerID='" . $strCustomerID . "'");
        $fullname = $seldatac[0]['CustomerFullName'];
        $CustomerLocation = $seldatac[0]['CustomerLocation'];
        $emailc = $seldatac[0]['CustomerEmailID'];
        $cust_mobile = $seldatac[0]['CustomerMobileNo'];
        if ($emailc != "") {
            $strStoreID = $apt_data[0]['StoreID'];
            $seldata = select("*", "tblStores", "StoreID='" . $strStoreID . "'");
            $storename = $seldata[0]['StoreName'];

            $path = "`http://nailspaexperience.com/images/test2.png`";
            $vars = array('{Name_Detail}', '{Branch}', '{apt_id}', '{path}', '{Cust_addr}', '{concent_text}', '{approve_row}'); //Replace varaibles
            $values = array($fullname, $storename, $app, $path, $CustomerLocation, $concent_data[0]['message'], $approve_row);

            //replace vars
            $message = str_replace($vars, $values, $message);

            $strFrom = "appnt@nailspaexperience.com";
            $headers = "From: $strFrom\r\n";
            $headers .= "Content-type: text/html\r\n";
            $strBodysa = AntiFilter1($message);

            $strSubject = "NS Client Consent";
            // Mail sending 
            $retval = mail($emailc, $strSubject, $strBodysa, $headers);
        }


        /*
         * Send SMS
         */

        if ($cust_mobile != '') {
            $feedback_link = "http://pos.nailspaexperience.com/og/show-consent-form.php?app=" . $app . "";
            $sms_content = 'Dear ' . $fullname . ', Click on link to approve consent form. ' . $feedback_link;

            $xmldata = '<?xml version="1.0" encoding="UTF-8"?>
                  <api>
                  <campaign>Service Reminder</campaign>
                  <unicode>0</unicode>
                  <flash>0</flash>
                  <sender>NAILSP</sender>
                  <message><![CDATA[' . $sms_content . ']]></message>
                  <sms>
                  <to>' . $cust_mobile . '</to>
                  </sms>
                  </api>';

            $api_url = 'http://api-alerts.solutionsinfini.com/v3/?method=sms.xml&api_key=Aebe2e79fd5ce8a3ffb0720d4bef17fe9&format=json&xml=' . urlencode($xmldata);
            $response = file_get_contents($api_url);
            $response = json_decode($response, true);
            $strSendingStatus = $response['status'];
        }
        $approve_update = "UPDATE tblAppointments SET is_concent_approve=2 WHERE AppointmentID='" . $app . "'";
        $DB->query($approve_update);
    }
}

$sqlUpdate2 = "UPDATE tblAppointments SET 45minstatus='1' WHERE AppointmentID='" . $app . "'";
// echo $sqlUpdate2."<br>";
if ($DB->query($sqlUpdate2) === TRUE) {
    echo 2;
}
?>