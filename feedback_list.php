<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
$strPageTitle = "Customer Feedback | NailSpa";
$strDisplayTitle = "Customer Feedback for NailSpa";
$strMenuID = "10";
$strMyTable = "tblAppointments";
$strMyTableID = "AppointmentID";
$strMyField = "AppointmentDate";
$strMyActionPage = "feedback_list.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <?php require_once("incChart-SalonDashboard.fya"); ?>

        <!-----------------included datatable.css & js files by gandhali 1/9/18----------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/datatable/datatable.css">
        <script type="text/javascript" src="assets/widgets/datatable/datatable.js"></script>
        <script type="text/javascript" src="assets/widgets/datatable/datatable-bootstrap.js"></script>
        <script type="text/javascript" src="assets/widgets/datatable/datatable-responsive.js"></script>
        <!-----------------included datatable.css by gandhali 1/9/18----------------->
        <style>
            .btn-danger:hover
            {
                border-color: #fc8213;
                background: #fc8213;
            }

        </style>
        <!-- Styles -->


    </head>

    <body>

        <div id="sb-site">


            <?php // require_once("incOpenLayout.fya");   ?>
            <!----------commented by gandhali 1/9/18---------------->


            <?php require_once("incLoader.fya"); ?>


            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>

                        <?php /* <script type="text/javascript" src="assets/widgets/skycons/skycons.js"></script>
                          <script type="text/javascript" src="assets/widgets/datatable/datatable.js"></script>
                          <script type="text/javascript" src="assets/widgets/datatable/datatable-bootstrap.js"></script>
                          <script type="text/javascript" src="assets/widgets/datatable/datatable-tabletools.js"></script> */ ?>

                        <link rel="stylesheet" type="text/css" href="assets/widgets/chosen/chosen.css">
                        <script type="text/javascript" src="assets/widgets/chosen/chosen.js"></script>

                        <script type="text/javascript" src="assets/widgets/datatable/date-euro.js"></script>
                        <script type="text/javascript">
                            /* Datatables responsive */
                            $(document).ready(function () {
                                $('#datatable-responsive-date').DataTable({
                                    "order": [[2, "desc"]],
                                    columnDefs: [{type: 'date-euro', targets: 2}]
                                });


                            });
                            $(document).ready(function () {
                                $('.dataTables_filter input').attr("placeholder", "Search...");
                                $(".chosen-select").chosen();
                            });
                        </script>
                        <?php
                        $sqlTempfrom = '';
                        $sqlTempto = '';

                        if (isset($_GET["toandfrom"])) {
                            $strtoandfrom = $_GET["toandfrom"];
                            $arraytofrom = explode("-", $strtoandfrom);

                            $from = $arraytofrom[0];
                            $datetime = new DateTime($from);
                            $getfrom = $datetime->format('Y-m-d');


                            $to = $arraytofrom[1];
                            $datetime = new DateTime($to);
                            $getto = $datetime->format('Y-m-d');

                            if (!IsNull($from)) {
                                $sqlTempfrom = " and Date(AppointmentDate)>=Date('" . $getfrom . "')";
                            }

                            if (!IsNull($to)) {
                                $sqlTempto = " and Date(AppointmentDate)<=Date('" . $getto . "')";
                            }

                            $append = '';
                            if (isset($_GET["Store"]) && $_GET["Store"] != '' && !empty($_GET["Store"])) {
                                $append .= " AND StoreID ='" . $_GET["Store"] . "'";
                            }
                            $DB = Connect();

                            $all_store = select("*", "tblStores", "StoreID > 0");
                            if (isset($all_store) && is_array($all_store) && count($all_store) > 0) {
                                foreach ($all_store as $stkey => $stvalue) {
                                    $store_name_data[$stvalue['StoreID']] = $stvalue['StoreName'];
                                }
                            }
                            $qry = "SELECT  AppointmentID,AppointmentDate,StoreID FROM tblAppointments WHERE Status= 2 " . $sqlTempfrom . $sqlTempto . $append;

                            $qry_exe = $DB->query($qry);
                            if ($qry_exe->num_rows > 0) {
                                while ($apt_res = $qry_exe->fetch_assoc()) {
                                    $Appointment_ids[] = $apt_res['AppointmentID'];
                                    $appoint_data[$apt_res['AppointmentID']] = $apt_res;
                                }
                            }
                            $feed_append = '';
                            $rating_filter = isset($_GET["Rating"]) ? $_GET["Rating"] : array();
                            if (isset($rating_filter) && is_array($rating_filter) && count($rating_filter) > 0) {
                                $rating_ids = implode(",", $rating_filter);
                                if ($rating_ids != '') {
                                    $feed_append = " AND (service_quality IN(" . $rating_ids . ") OR value_for_money IN(" . $rating_ids . ") OR salon_atmosphere IN(" . $rating_ids . ")"
                                            . " OR staff_presentation_attitude IN(" . $rating_ids . ") OR cleanliness IN(" . $rating_ids . "))";
                                }
                            }


                            if (isset($Appointment_ids) && is_array($Appointment_ids) && count($Appointment_ids) > 0) {
                                $apt_ids = implode(", ", $Appointment_ids);
                                if ($apt_ids != '') {
                                    $feed_qry = "SELECT * FROM customer_feedback WHERE status = 1 AND appointment_id IN(" . $apt_ids . ")" . $feed_append;
                                    $feed_qry_exe = $DB->query($feed_qry);
                                    if ($feed_qry_exe->num_rows > 0) {
                                        while ($feed_res = $feed_qry_exe->fetch_assoc()) {
                                            $final_res[] = $feed_res;
                                            $invoice_apt_ids[] = $feed_res['appointment_id'];
                                            $customer_ids[] = $feed_res['customer_id'];
                                        }
                                    }
                                }
                            }

                            /*
                             * Get Customer Data
                             */
                            if (isset($customer_ids) && is_array($customer_ids) && count($customer_ids) > 0) {
                                $in_cust_ids = implode(", ", $customer_ids);
                                if ($in_cust_ids != '') {
                                    $cust_qry = "SELECT * FROM tblCustomers WHERE CustomerID IN(" . $in_cust_ids . ")";
                                    $cust_qry_exe = $DB->query($cust_qry);
                                    if ($cust_qry_exe->num_rows > 0) {
                                        while ($cust_res = $cust_qry_exe->fetch_assoc()) {
                                            $customer_data[$cust_res['CustomerID']] = $cust_res;
                                        }
                                    }
                                }
                            }

                            /*
                             * Get Appointment Invoice
                             */
                            if (isset($invoice_apt_ids) && is_array($invoice_apt_ids) && count($invoice_apt_ids) > 0) {
                                $inv_apt_ids = implode(", ", $invoice_apt_ids);
                                if ($inv_apt_ids != '') {

                                    /*
                                     * Get All Service name
                                     */
                                    $ser_name_qry = "SELECT * FROM tblServices";
                                    $ser_name_exe = $DB->query($ser_name_qry);
                                    if ($ser_name_exe->num_rows > 0) {
                                        while ($ser_name_row = $ser_name_exe->fetch_assoc()) {
                                            $apt_service_name[$ser_name_row['ServiceID']] = $ser_name_row['ServiceName'];
                                        }
                                    }

                                    /*
                                     * Get All Employee name
                                     */
                                    $emp_qry = "SELECT * FROM tblEmployees";
                                    $emp_qry_exe = $DB->query($emp_qry);
                                    if ($emp_qry_exe->num_rows > 0) {
                                        while ($emp_row = $emp_qry_exe->fetch_assoc()) {
                                            $apt_employee_name[$emp_row['EID']] = $emp_row['EmployeeName'];
                                        }
                                    }


                                    /*
                                     * get Invoice number
                                     */
                                    $invoice_qry = "SELECT * FROM tblInvoiceDetails WHERE AppointmentId IN(" . $inv_apt_ids . ")";
                                    $invoice_qry_exe = $DB->query($invoice_qry);
                                    if ($invoice_qry_exe->num_rows > 0) {
                                        while ($invoice_res = $invoice_qry_exe->fetch_assoc()) {
                                            $invoice_data[$invoice_res['AppointmentId']] = $invoice_res['InvoiceId'];
                                        }
                                    }


                                    /*
                                     * Get Service name
                                     */
                                    $invoice_ser_qry = "SELECT * FROM tblInvoiceDetails WHERE AppointmentId IN(" . $inv_apt_ids . ")";
                                    $invoice_ser_qry_exe = $DB->query($invoice_ser_qry);
                                    if ($invoice_ser_qry_exe->num_rows > 0) {
                                        while ($invoice_ser_res = $invoice_ser_qry_exe->fetch_assoc()) {
                                            $service_ids = trim($invoice_ser_res['ServiceName']);
                                            $ser_arr = explode(",", $service_ids);
                                            if (isset($ser_arr) && is_array($ser_arr) && count($ser_arr) > 0) {
                                                foreach ($ser_arr as $snkey => $snvalue) {
                                                    if (isset($apt_service_name[$snvalue])) {
                                                        $invoice_ser_data[$invoice_ser_res['AppointmentId']][$snvalue] = $apt_service_name[$snvalue];
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    /*
                                     * Get Appointment Employee
                                     */
                                    $invoice_emp_qry = "SELECT * FROM tblAppointmentAssignEmployee WHERE AppointmentID IN(" . $inv_apt_ids . ")";
                                    $invoice_emp_qry_exe = $DB->query($invoice_emp_qry);
                                    if ($invoice_emp_qry_exe->num_rows > 0) {
                                        while ($invoice_emp_res = $invoice_emp_qry_exe->fetch_assoc()) {
                                            if (isset($apt_employee_name[$invoice_emp_res['MECID']])) {
                                                $invoice_emp_data[$invoice_emp_res['AppointmentID']][$invoice_emp_res['MECID']] = $apt_employee_name[$invoice_emp_res['MECID']];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        ?>

                        <div class="panel">
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="example-box-wrapper">
                                        <div id="normal-tabs-1">
                                            <span class="form_result">&nbsp; <br>
                                            </span>

                                            <div class="panel-body">
                                                <h4 class="title-hero"><center>List of 	Customer Feedback | NailSpa</center></h4>
                                                <br>


                                                <form method="get" class="form-horizontal bordered-row" role="form" action="feedback_list.php">
                                                    <div class="form-group"><label for="" class="col-sm-4 control-label">Select date</label>
                                                        <div class="col-sm-4">
                                                            <div class="input-prepend input-group">
                                                                <span class="add-on input-group-addon">
                                                                    <i class="glyph-icon icon-calendar"></i>
                                                                </span> 
                                                                <input type="text" name="toandfrom" id="daterangepicker-example" class="form-control" value="<?= $strtoandfrom
                        ?>">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">Select Store</label>
                                                        <div class="col-sm-4">
                                                            <select name="Store" class="form-control">
                                                                <option value="0">All</option>
                                                                <?php
                                                                $selp = select("*", "tblStores", "Status='0'");
                                                                foreach ($selp as $val) {
                                                                    $strStoreName = $val["StoreName"];
                                                                    $strStoreID = $val["StoreID"];
                                                                    $store = $_GET["Store"];
                                                                    if ($store == $strStoreID) {
                                                                        ?>
                                                                        <option  selected value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                        <?php
                                                                    } else {
                                                                        ?>
                                                                        <option value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">Select Rating</label>
                                                        <div class="col-sm-4">
                                                            <?php
                                                            $rating_names = array(
                                                                1 => 'Poor',
                                                                2 => 'Average',
                                                                3 => 'Good',
                                                                4 => 'Excellent'
                                                            );
                                                            $get_rating = isset($_GET['Rating']) ? $_GET['Rating'] : array();
                                                            ?>
                                                            <select name="Rating[]" class="form-control chosen-select" multiple>
                                                                <?php
                                                                if (isset($rating_names) && is_array($rating_names) && count($rating_names) > 0) {
                                                                    foreach ($rating_names as $key => $value) {
                                                                        ?>
                                                                        <option value="<?php echo $key; ?>"
                                                                        <?php
                                                                        if (in_array($key, $get_rating)) {
                                                                            echo 'selected';
                                                                        }
                                                                        ?>><?php echo $value; ?></option>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>                                                             
                                                            </select>
                                                        </div>
                                                    </div>


                                                    <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                        <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                                        &nbsp;&nbsp;&nbsp;
                                                        <a class="btn btn-link" href="feedback_list.php">Clear All Filter</a>
                                                        &nbsp;&nbsp;&nbsp;

                                                    </div>
                                                </form>

                                                <div class="example-box-wrapper">
                                                    <?php
                                                    $rating_names = array(
                                                        1 => 'Poor',
                                                        2 => 'Average',
                                                        3 => 'Good',
                                                        4 => 'Excellent'
                                                    );
                                                    if (isset($_GET["toandfrom"]) && $_GET["toandfrom"] != '') {
                                                        if (isset($final_res) && is_array($final_res) && count($final_res) > 0) {
                                                            ?>
                                                            <table id="datatable-responsive-date" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="padding: 5px !important;"><center>Sr. No.</center></th>
                                                                <th style="padding: 5px !important;"><center>Invoice No.</center></th>
                                                                <th style="padding: 5px !important;"><center>Appointment Date</center></th>
                                                                <th style="padding: 5px !important;"><center>Customer</center></th>
                                                                <th style="padding: 5px !important;"><center>Mobile No.</center></th>
                                                                <th style="padding: 5px !important;"><center>Store</center></th>
                                                                <th style="padding: 5px !important;"><center>Services</center></th>
                                                                <th style="padding: 5px !important;"><center>Employee</center></th>
                                                                <?php /* <th><center>Customer Location</center></th> */ ?>
                                                                <th style="padding: 5px !important;"><center>Service Quality</center></th>
                                                                <th style="padding: 5px !important;"><center>Value for money</center></th>
                                                                <th style="padding: 5px !important;"><center>Salon Atmosphere</center></th>
                                                                <th style="padding: 5px !important;"><center>Staff Presentation & Attitude</center></th>
                                                                <th style="padding: 5px !important;"><center>Cleanliness</center></th>
                                                                </tr>
                                                                </thead>


                                                                <tbody>
                                                                    <?php
                                                                    if (isset($final_res) && is_array($final_res) && count($final_res) > 0) {
                                                                        $cnt = 1;
                                                                        foreach ($final_res as $skey => $svalue) {
                                                                            $apt_store_name = '';
                                                                            $apt_store_id = isset($appoint_data[$svalue['appointment_id']]) ? $appoint_data[$svalue['appointment_id']]['StoreID'] : '';
                                                                            if ($apt_store_id != '') {
                                                                                $apt_store_name = isset($store_name_data[$apt_store_id]) ? $store_name_data[$apt_store_id] : '';
                                                                            }
                                                                            ?>
                                                                            <tr>
                                                                                <td><center><?php echo $cnt; ?></center></td>
                                                                        <td><center><?php echo isset($invoice_data[$svalue['appointment_id']]) ? $invoice_data[$svalue['appointment_id']] : ''; ?></center></td>
                                                                        <td><center><?php echo isset($appoint_data[$svalue['appointment_id']]) ? date('d/m/Y', strtotime($appoint_data[$svalue['appointment_id']]['AppointmentDate'])) : ''; ?></center></td>
                                                                        <td><center><?php echo isset($customer_data[$svalue['customer_id']]) ? $customer_data[$svalue['customer_id']]['CustomerFullName'] : ''; ?></center></td>
                                                                        <td><center><?php echo isset($customer_data[$svalue['customer_id']]) ? $customer_data[$svalue['customer_id']]['CustomerMobileNo'] : ''; ?></center></td>
                                                                        <td><center><?php echo ucwords($apt_store_name); ?></center></td>
                                                                        <?php /* <td><center><?php echo isset($customer_data[$svalue['customer_id']]) ? $customer_data[$svalue['customer_id']]['CustomerLocation'] : ''; ?></center></td> */ ?>
                                                                        <td>
                                                                            <?php
                                                                            $service_count = 1;
                                                                            if (isset($invoice_ser_data[$svalue['appointment_id']]) && is_array($invoice_ser_data[$svalue['appointment_id']]) && count($invoice_ser_data[$svalue['appointment_id']]) > 0) {
                                                                                foreach ($invoice_ser_data[$svalue['appointment_id']] as $sikey => $sivalue) {
                                                                                    echo "<p>" . $service_count . ". " . $sivalue . "</p>";
                                                                                    $service_count++;
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </td>

                                                                        <td>
                                                                            <?php
                                                                            $emp_count = 1;
                                                                            if (isset($invoice_emp_data[$svalue['appointment_id']]) && is_array($invoice_emp_data[$svalue['appointment_id']]) && count($invoice_emp_data[$svalue['appointment_id']]) > 0) {
                                                                                foreach ($invoice_emp_data[$svalue['appointment_id']] as $eikey => $eivalue) {
                                                                                    echo "<p>" . $emp_count . ". " . $eivalue . "</p>";
                                                                                    $emp_count++;
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                        <td><?php echo isset($svalue['service_quality']) && isset($rating_names[$svalue['service_quality']]) ? $rating_names[$svalue['service_quality']] : ''; ?></td>
                                                                        <td><?php echo isset($svalue['value_for_money']) && isset($rating_names[$svalue['value_for_money']]) ? $rating_names[$svalue['value_for_money']] : ''; ?></td>
                                                                        <td><?php echo isset($svalue['salon_atmosphere']) && isset($rating_names[$svalue['salon_atmosphere']]) ? $rating_names[$svalue['salon_atmosphere']] : ''; ?></td>
                                                                        <td><?php echo isset($svalue['staff_presentation_attitude']) && isset($rating_names[$svalue['staff_presentation_attitude']]) ? $rating_names[$svalue['staff_presentation_attitude']] : ''; ?></td>
                                                                        <td><?php echo isset($svalue['cleanliness']) && isset($rating_names[$svalue['cleanliness']]) ? $rating_names[$svalue['cleanliness']] : ''; ?></td>
                                                                        </tr>
                                                                        <?php
                                                                        $cnt++;
                                                                    }
                                                                } else {
                                                                    ?>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td colspan="6"><center>No Feedback Found...</center></td>
                                                                    <td></td>
                                                                    </tr>  
                                                                <?php }
                                                                ?>
                                                                </tbody>
                                                                <?php
                                                                $DB->close();
                                                                ?>
                                                            </table>
                                                            <?php
                                                        } else {
                                                            echo "<br><center><h3>No Feedback Found!</h3></center>";
                                                        }
                                                    } else {
                                                        echo "<br><center><h3>Please Select Month And Year!</h3></center>";
                                                    }
                                                    ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php require_once 'incFooter.fya'; ?>

                </div>
                </body>

                </html>		