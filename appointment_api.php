<?php require_once("setting.fya"); ?>
<?php require_once("new_appointment_helper.php"); ?>
<?php

$post = $_POST;


//file_put_contents('apt_post_data', serialize($_POST));
$all_services_str = isset($post['services']) ? $post['services'] : '';
$all_services_name_str = isset($post['services_name']) ? $post['services_name'] : '';

unset($post['services']);
$all_services = explode(",", $all_services_str);
$all_services_name = explode(",", $all_services_name_str);


if (isset($all_services) && is_array($all_services) && count($all_services) > 0) {
    foreach ($all_services as $key => $value) {
        if (trim($value) != '') {
            $post['services'][$key] = array(
                'service_code' => $value,
                'store_id' => $post['store_id'],
                'name' => isset($all_services_name[$key]) ? $all_services_name[$key] : '',
                'quantity' => 1
            );
        }
    }
}
/* $post['services'] = array(0 => array(
  'service_code' => $post['services'],
  'store_id' => $post['store_id'],
  'name' => $post['services_name'],
  'quantity' => 1
  )
  ); */
$result = add_appointment($post);
echo $result;
?>