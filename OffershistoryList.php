<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Manage Offers | Nailspa";
$strDisplayTitle = "Offers Logs for Nailspa";
$strMenuID = "3";
$strMyTable = "tblOffers";
$strMyTableID = "OfferID";
$strMyField = "OfferName";
$strMyActionPage = "OffersHistoryList.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <!-----------css & js files added for tabs by gandhali 3/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");     ?>
            <!----------commented by gandhali 3/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>

                        <script type="text/javascript">
                            /* Datatables responsive */
                            $(document).ready(function () {
                                $('#datatable-responsive-scroll').DataTable({
                                    "scrollX": true
                                });
                            });
                            $(document).ready(function () {
                                $('.dataTables_filter input').attr("placeholder", "Search...");
                            });
                        </script>

                        <?php
                        $DB = Connect();


                        /*
                         * get store history
                         */
                        if (isset($_GET['offer_id']) && $_GET['offer_id'] != '') {
                            $historyq = "SELECT * FROM tblOffers_history WHERE OfferID='" . $_GET['offer_id'] . "'";

                            $historyq_exe = $DB->query($historyq);
                            if ($historyq_exe->num_rows > 0) {
                                while ($history_row = $historyq_exe->fetch_assoc()) {
                                    $history_data[] = $history_row;
                                }
                            }
                            if (isset($history_data) && is_array($history_data) && count($history_data) > 0) {
                                /*
                                 * get all users
                                 */
                                $userq = "SELECT * FROM tblAdmin";
                                $userq_exe = $DB->query($userq);
                                if ($userq_exe->num_rows > 0) {
                                    while ($user_row = $userq_exe->fetch_assoc()) {
                                        $user_data[$user_row['AdminID']] = $user_row;
                                    }
                                }
                            }

                            /*
                             * Get All Store,Category,Service Ids
                             */
                            if (isset($history_data) && is_array($history_data) && count($history_data) > 0) {
                                foreach ($history_data as $namekey => $namevalue) {
                                    /*
                                     * Get Store Ids
                                     */
                                    $store_arr = explode(",", $namevalue['StoreID']);
                                    if (isset($store_arr) && is_array($store_arr) && count($store_arr) > 0) {
                                        foreach ($store_arr as $stkey => $stvalue) {
                                            if ($stvalue != '') {
                                                $all_store_id[$stvalue] = $stvalue;
                                            }
                                        }
                                    }

                                    /*
                                     * Get Category Ids
                                     */
                                    $category_arr = explode(",", $namevalue['OfferCategory']);
                                    if (isset($category_arr) && is_array($category_arr) && count($category_arr) > 0) {
                                        foreach ($category_arr as $catkey => $catvalue) {
                                            if ($catvalue != '') {
                                                $all_category_id[$catvalue] = $catvalue;
                                            }
                                        }
                                    }

                                    /*
                                     * Get Service Ids
                                     */
                                    $service_arr = explode(",", $namevalue['ServiceID']);
                                    if (isset($service_arr) && is_array($service_arr) && count($service_arr) > 0) {
                                        foreach ($service_arr as $serkey => $servalue) {
                                            if ($servalue != '') {
                                                $all_service_id[$servalue] = $servalue;
                                            }
                                        }
                                    }
                                }


                                /*
                                 * Get All Store,Category,Service name
                                 */
                                if (isset($all_store_id) && is_array($all_store_id) && count($all_store_id) > 0) {
                                    $in_store_ids = implode(",", $all_store_id);
                                    if ($in_store_ids != '') {
                                        $store_sql = "select * FROM tblStores WHERE StoreID IN(" . $in_store_ids . ")";
                                        $store_sql_exe = $DB->query($store_sql);
                                        if ($store_sql_exe->num_rows > 0) {
                                            while ($store_row = $store_sql_exe->fetch_assoc()) {
                                                $all_store_name[$store_row["StoreID"]] = $store_row['StoreName'];
                                            }
                                        }
                                    }
                                }

                                if (isset($all_category_id) && is_array($all_category_id) && count($all_category_id) > 0) {
                                    $in_cat_ids = implode(",", $all_category_id);
                                    if ($in_cat_ids != '') {
                                        $category_sql = "select * FROM tblCategories WHERE CategoryID IN(" . $in_cat_ids . ")";
                                        $category_sql_exe = $DB->query($category_sql);
                                        if ($category_sql_exe->num_rows > 0) {
                                            while ($category_row = $category_sql_exe->fetch_assoc()) {
                                                $all_category_name[$category_row["CategoryID"]] = $category_row['CategoryName'];
                                            }
                                        }
                                    }
                                }

                                if (isset($all_service_id) && is_array($all_service_id) && count($all_service_id) > 0) {
                                    $in_service_ids = implode(",", $all_service_id);
                                    if ($in_service_ids != '') {
                                        $service_sql = "select * FROM tblServices WHERE ServiceID IN(" . $in_service_ids . ")";
                                        $service_sql_exe = $DB->query($service_sql);
                                        if ($service_sql_exe->num_rows > 0) {
                                            while ($service_row = $service_sql_exe->fetch_assoc()) {
                                                $all_service_data[$service_row["ServiceID"]] = $service_row['ServiceName'];
                                            }
                                        }
                                    }
                                }
                            }

                            $DB->close();
                        }
                        $counter = 0;
                        ?>
                        <div id="page-title">
                            <h2>All Offers Log</h2>
                        </div>

                        <div class="panel">
                            <div class="panel">
                                <div class="panel-body">

                                    <div class="example-box-wrapper">
                                        <div class="tabs">
                                            <div id="normal-tabs-1">
                                                <span class="form_result">&nbsp; <br>
                                                </span>

                                                <div class="panel-body">
                                                    <h3 class="title-hero">List of Offers Logs for POS</h3>
                                                    <div class="example-box-wrapper">
                                                        <table id="datatable-responsive-scroll" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                                                            <thead>
                                                                <tr>
                                                                    <th>Action</th>
                                                                    <th>User</th>
                                                                    <th>Date/time</th>
                                                                    <th>Offer Name</th>
                                                                    <th>Offer Code</th>
                                                                    <th>Description</th>
                                                                    <th>Offer Date From</th>
                                                                    <th>Offer Date To</th>
                                                                    <th>Base Amount</th>
                                                                    <th>Type</th>
                                                                    <th>Type(Amount or %)</th>
                                                                    <th>Store Name</th>
                                                                    <th>Category</th>
                                                                    <th>Service</th>
                                                                    <th>Status</th>
                                                                </tr>
                                                            </thead>
                                                            <tfoot>
                                                                <tr>
                                                                    <th>Action</th>
                                                                    <th>User</th>
                                                                    <th>Date/time</th>
                                                                    <th>Offer Name</th>
                                                                    <th>Offer Code</th>
                                                                    <th>Description</th>
                                                                    <th>Offer Date From</th>
                                                                    <th>Offer Date To</th>
                                                                    <th>Base Amount</th>
                                                                    <th>Type</th>
                                                                    <th>Type(Amount or %)</th>
                                                                    <th>Store Name</th>
                                                                    <th>Category</th>
                                                                    <th>Service</th>
                                                                    <th>Status</th>
                                                                </tr>
                                                            </tfoot>
                                                            <tbody>
                                                                <?php
                                                                if (isset($history_data) && is_array($history_data) && count($history_data) > 0) {
                                                                    foreach ($history_data as $hkey => $hvalue) {
                                                                        if ($hvalue['history_notes'] == 'insert') {
                                                                            $action_name = "<p style='color:blue;'>Add</p>";
                                                                            $action_responsible = isset($user_data[$hvalue['created_by']]) ? $user_data[$hvalue['created_by']]['AdminFullName'] : '';
                                                                        } else if ($hvalue['history_notes'] == 'update') {
                                                                            $action_name = "<p class='text-success'>Edit</p>";
                                                                            $action_responsible = isset($user_data[$hvalue['modified_by']]) ? $user_data[$hvalue['modified_by']]['AdminFullName'] : '';
                                                                        } else if ($hvalue['history_notes'] == 'delete') {
                                                                            $action_name = "<p class='text-danger'>Delete</p>";
                                                                            $action_responsible = isset($user_data[$hvalue['modified_by']]) ? $user_data[$hvalue['modified_by']]['AdminFullName'] : '';
                                                                        }
                                                                        ?> 
                                                                        <tr id="my_data_tr_<?= $counter ?>">
                                                                            <td><?php echo isset($action_name) ? $action_name : '' ?></td>
                                                                            <td><?php echo isset($action_responsible) ? $action_responsible : '' ?></td>
                                                                            <td><?php echo isset($hvalue['history_timestamp']) ? date('d/m/Y h:i a', strtotime($hvalue['history_timestamp'])) : ''; ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && $history_data[$hkey - 1]['OfferName'] != $hvalue['OfferName']) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . $hvalue['OfferName'] . '</span>';
                                                                                ?></td>

                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && $history_data[$hkey - 1]['OfferCode'] != $hvalue['OfferCode']) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . $hvalue['OfferCode'] . '</span>';
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && $history_data[$hkey - 1]['OfferDescription'] != $hvalue['OfferDescription']) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                //echo '<span ' . $edit_class . '>' . $hvalue['OfferDescription'] . '</span>';
                                                                                echo $hvalue['OfferDescription'];
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && $history_data[$hkey - 1]['OfferDateFrom'] != $hvalue['OfferDateFrom']) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . date('d/m/Y', strtotime($hvalue['OfferDateFrom'])) . '</span>';
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && $history_data[$hkey - 1]['OfferDateTo'] != $hvalue['OfferDateTo']) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . date('d/m/Y', strtotime($hvalue['OfferDateTo'])) . '</span>';
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && $history_data[$hkey - 1]['BaseAmount'] != $hvalue['BaseAmount']) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . $hvalue['BaseAmount'] . '</span>';
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && $history_data[$hkey - 1]['Type'] != $hvalue['Type']) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . (isset($hvalue['Type']) && $hvalue['Type'] == 1 ? 'Amount' : '%') . '</span>';
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && $history_data[$hkey - 1]['TypeAmount'] != $hvalue['TypeAmount']) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . $hvalue['TypeAmount'] . '</span>';
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && trim($history_data[$hkey - 1]['StoreID'], ',') != trim($hvalue['StoreID'], ',')) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                $history_store = explode(",", $hvalue['StoreID']);
                                                                                $his_store_name = array();
                                                                                if (isset($history_store) && is_array($history_store) && count($history_store) > 0) {
                                                                                    foreach ($history_store as $hstkey => $hstvalue) {
                                                                                        if ($hstvalue != '' && isset($all_store_name[trim($hstvalue)])) {
                                                                                            $his_store_name[$hstvalue] = $all_store_name[trim($hstvalue)];
                                                                                        }
                                                                                    }
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . implode(",", $his_store_name) . '</span>';
                                                                                ?></td>

                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && trim($history_data[$hkey - 1]['OfferCategory'], ',') != trim($hvalue['OfferCategory'], ',')) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                $history_cat = explode(",", $hvalue['OfferCategory']);
                                                                                $his_cat_name = array();
                                                                                if (isset($history_cat) && is_array($history_cat) && count($history_cat) > 0) {
                                                                                    foreach ($history_cat as $hcakey => $hcavalue) {
                                                                                        if ($hcavalue != '' && isset($all_category_name[trim($hcavalue)])) {
                                                                                            $his_cat_name[$hcavalue] = $all_category_name[trim($hcavalue)];
                                                                                        }
                                                                                    }
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . implode(",", $his_cat_name) . '</span>';
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';

                                                                                $pre_services = isset($history_data[$hkey - 1]['ServiceID']) ? explode(",", trim($history_data[$hkey - 1]['ServiceID'], ',')) : array();

                                                                                $cur_services = explode(",", trim($hvalue['ServiceID'], ','));

                                                                                $pre_services_uni = array_unique($pre_services);
                                                                                $cur_services_uni = array_unique($cur_services);

                                                                                if (count($pre_services_uni) > 0) {
                                                                                    $pre_diff = array_diff($pre_services_uni, $cur_services_uni);
                                                                                    $cur_diff = array_diff($cur_services_uni, $pre_services_uni);

                                                                                    if (count($pre_diff) > 0 || count($cur_diff) > 0) {
                                                                                        $edit_class = 'class="text-danger"';
                                                                                    }
                                                                                }
                                                                                /* if (isset($history_data[$hkey - 1]) && trim($history_data[$hkey - 1]['ServiceID'], ',') != trim($hvalue['ServiceID'], ',')) {
                                                                                  $edit_class = 'class="text-danger"';
                                                                                  } */
                                                                                $history_service = $cur_services_uni;

                                                                                $his_service_name = array();
                                                                                if (isset($history_service) && is_array($history_service) && count($history_service) > 0) {
                                                                                    foreach ($history_service as $hserkey => $hseralue) {
                                                                                        if ($hseralue != '' && isset($all_service_data[trim($hseralue)])) {
                                                                                            $his_service_name[$all_service_data[trim($hseralue)]] = $all_service_data[trim($hseralue)];
                                                                                        }
                                                                                    }
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . implode(",", $his_service_name) . '</span>';
                                                                                ?></td>
                                                                            <td><?php
                                                                                $edit_class = '';
                                                                                if (isset($history_data[$hkey - 1]) && $history_data[$hkey - 1]['Status'] != $hvalue['Status']) {
                                                                                    $edit_class = 'class="text-danger"';
                                                                                }
                                                                                echo '<span ' . $edit_class . '>' . (isset($hvalue['Status']) && $hvalue['Status'] == 1 ? 'Offline' : 'Live') . '</span>';
                                                                                ?></td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    $counter++;
                                                                } else {
                                                                    ?>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>
                                                                <?php }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <?php require_once 'incFooter.fya'; ?>
        </div>
    </body>
</html>
