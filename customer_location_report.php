<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Manage Customers | Nailspa";
$strDisplayTitle = "Customer Location Report of Nailspa Experience";
$strMenuID = "10";
$strMyTable = "tblCustomers";
$strMyTableID = "CustomerID";
$strMyField = "CustomerMobileNo";
$strMyActionPage = "customer_location_report.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
?>
<!DOCTYPE html>
<html lang="en">

    <?php require_once("incMetaScript.fya"); ?>
</head>

<body>
    <div id="sb-site">

        <?php // require_once("incOpenLayout.fya");   ?>
        <!----------commented by gandhali 3/9/18---------------->


        <?php require_once("incLoader.fya"); ?>

        <div id="page-wrapper">
            <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

            <?php require_once("incLeftMenu.fya"); ?>

            <div id="page-content-wrapper">
                <div id="page-content">

                    <?php require_once("incHeader.fya"); ?>
                    <div id="page-title">
                        <h2><?= $strDisplayTitle ?></h2>
                    </div>
                    <script type="text/javascript">
                        /* Datatables responsive */
                        $(document).ready(function () {
                            $('#datatable-responsive-count').DataTable({
                                "order": [[3, "desc"]]
                            });
                        });

                    </script>

                    <div class="panel">
                        <div class="panel-body" style="overflow-x: scroll;">
                            <h3 class="title-hero">Customer Location</h3>
                            <form method="get" class="form-horizontal bordered-row" role="form">
                                <?php
                                $sql1 = "SELECT StoreID, StoreName FROM tblStores WHERE Status = 0";
                                $RS2 = $DB->query($sql1);
                                if ($RS2->num_rows > 0) {
                                    ?>
                                    <div class="form-group"><label class="col-sm-2 control-label">Store</label>
                                        <div class="col-sm-4">
                                            <select class="form-control required"  id="StoreID" name="StoreID" >
                                                <option value="" selected>All</option>
                                                <?php
                                                while ($row2 = $RS2->fetch_assoc()) {
                                                    $StoreID = $row2["StoreID"];
                                                    $StoreName = $row2["StoreName"];
                                                    ?>	
                                                    <option value="<?= $StoreID ?>" <?php echo isset($_GET['StoreID']) && $_GET['StoreID'] == $StoreID ? 'selected' : '' ?>><?= $StoreName ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>	

                                    <div class="form-group"><label class="col-sm-3 control-label"></label>
                                        <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                        &nbsp;&nbsp;&nbsp;
                                        <a class="btn btn-link" href="customer_location_report.php">Clear All Filter</a> &nbsp;&nbsp;&nbsp;
                                    </div>
                                    <?php
                                }
                                ?>


                            </form>

                            <?php
                            if (isset($_GET['StoreID']) && !empty($_GET['StoreID'])) {
                                $store_data = select("*", "tblStores", "StoreID='" . $_GET['StoreID'] . "'");
                                $store_name = $store_data[0]['StoreName'];
                            } else {
                                $store_name = "All";
                            }
                            ?>
                            <h3>Store : <?php echo $store_name; ?></h3>
                            <table id = "datatable-responsive-count" class = "table table-striped table-bordered responsive no-wrap" cellspacing = "0" width = "100%">
                                <thead>
                                    <tr>
                                        <th>Location</th>
                                        <th>Existing Customer</th>
                                        <th>New Customer</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Location</th>
                                        <th>Existing Customer</th>
                                        <th>New Customer</th>
                                        <th>Total</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php
                                    /*
                                     * Get Customer Count 
                                     */
                                    $old_total = 0;
                                    $new_total = 0;
                                    $customerq = "SELECT CustomerID,CustomerLocation FROM `tblCustomers`";
                                    $customerq_exe = $DB->query($customerq);
                                    if ($customerq_exe->num_rows > 0) {
                                        while ($cust_data = $customerq_exe->fetch_assoc()) {
                                            $customer_ids[$cust_data['CustomerID']] = $cust_data['CustomerID'];
                                            if ($cust_data['CustomerLocation'] == '') {
                                                $customer_loc[$cust_data['CustomerID']] = "Empty";
                                            } else {
                                                $customer_loc[$cust_data['CustomerID']] = $cust_data['CustomerLocation'];
                                            }
                                        }
                                    }

                                    /*
                                     * Get Customer Appointment Count to check new or exisitng client
                                     */
                                    $cust_count = isset($customer_ids) ? count($customer_ids) : 0;
                                    $limit = 1000;
                                    if ($cust_count > 0) {
                                        $total_iteration = $cust_count / $limit;
                                        $iter = ceil($total_iteration);
                                        for ($i = 0; $i < $iter; $i++) {
                                            $offset = $i * $limit;
                                            //echo "<br>Offset=" . $offset;
                                            $data = array_slice($customer_ids, $offset, $limit);
                                            if (isset($data) && is_array($data) && count($data) > 0) {
                                                $cust_in_ids = implode(",", $data);
                                                if ($cust_in_ids != '') {
                                                    $append = '';
                                                    if (isset($_GET['StoreID']) && !empty($_GET['StoreID'])) {
                                                        $append = " AND tblAppointments.StoreID='" . $_GET['StoreID'] . "'";
                                                    }
                                                    $apt_countQ = "SELECT COUNT(AppointmentId) as apt_count,CustomerID FROM tblAppointments "
                                                            . " WHERE AppointmentCheckInTime!='00:00:00' AND status=2 AND CustomerID IN(" . $cust_in_ids . ")" . $append
                                                            . " GROUP BY CustomerID";
                                                    $apt_count_exe = $DB->query($apt_countQ);
                                                    // echo "<br><br>Query=" . $apt_countQ;
                                                    //echo "<br>Count=" . $apt_count_exe->num_rows;

                                                    while ($count_data = $apt_count_exe->fetch_assoc()) {
                                                        $customer_apt_count[$count_data['CustomerID']] = $count_data['apt_count'];

                                                        /*
                                                         * Get Customer Location
                                                         */
                                                        $apt_cust_location = isset($customer_loc[$count_data['CustomerID']]) ? $customer_loc[$count_data['CustomerID']] : 'Empty';
                                                        if ($count_data['apt_count'] > 1) {
                                                            /*
                                                             * Old Customer
                                                             */
                                                            if (isset($final_res[$apt_cust_location]['old'])) {
                                                                $final_res[$apt_cust_location]['old'] +=1;
                                                            } else {
                                                                $final_res[$apt_cust_location]['old'] = 1;
                                                            }
                                                            $old_total += 1;
                                                        } else {
                                                            /*
                                                             * New Customer
                                                             */
                                                            if (isset($final_res[$apt_cust_location]['new'])) {
                                                                $final_res[$apt_cust_location]['new'] +=1;
                                                            } else {
                                                                $final_res[$apt_cust_location]['new'] = 1;
                                                            }
                                                            $new_total += 1;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    $all_location = select("*", "tblCutomerLocation", "id > 0");
                                    if (isset($all_location) && is_array($all_location) && count($all_location) > 0) {
                                        ?>
                                        <tr>
                                            <td>All</td>
                                            <td><?php echo $old_total; ?></td>
                                            <td><?php echo $new_total; ?></td>
                                            <td><?php echo $old_total + $new_total; ?></td>
                                        </tr>
                                        <?php
                                        foreach ($all_location as $lkey => $lvalue) {
                                            $old_client = isset($final_res[$lvalue['location_name']]['old']) ? $final_res[$lvalue['location_name']]['old'] : 0;
                                            $new_client = isset($final_res[$lvalue['location_name']]['new']) ? $final_res[$lvalue['location_name']]['new'] : 0;
                                            $total_client = $old_client + $new_client;
                                            ?>
                                            <tr>
                                                <td><?php echo ucwords($lvalue['location_name']); ?></td>
                                                <td><?php echo $old_client; ?></td>
                                                <td><?php echo $new_client; ?></td>
                                                <td><?php echo $total_client; ?></td>
                                            </tr>
                                            <?php
                                        }
                                        /*
                                         * Show Customer Count whose location is empty
                                         */
                                        if (isset($final_res['Empty']) && is_array($final_res['Empty']) && count($final_res['Empty']) > 0) {
                                            $old_client = isset($final_res['Empty']['old']) ? $final_res['Empty']['old'] : 0;
                                            $new_client = isset($final_res['Empty']['new']) ? $final_res['Empty']['new'] : 0;
                                            $total_client = $old_client + $new_client;
                                            ?>
                                            <tr>
                                                <td><?php echo "Empty"; ?></td>
                                                <td><?php echo $old_client; ?></td>
                                                <td><?php echo $new_client; ?></td>
                                                <td><?php echo $total_client; ?></td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr>
                                            <td></td>
                                            <td colspan="3">No Location Found...</td>
                                            <td></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
        <?php require_once 'incFooter.fya'; ?>
    </div>
</body>
</html>