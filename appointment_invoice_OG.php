<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php
$strPageTitle = "Invoice | Nailspa";
$strDisplayTitle = "Invoice for Nailspa";
$strMenuID = "10";
$strMyTable = "tblAppointmentsDetailsInvoice";
$strMyTableID = "AppointmentDetailsID";
$strMyActionPage = "appointment_invoice.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
//error_reporting(E_ALL);
// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights	
if (isset($_GET["toandfrom"])) {
    $strtoandfrom = $_GET["toandfrom"];
    $arraytofrom = explode("-", $strtoandfrom);
    $from = $arraytofrom[0];
    $datetime = new DateTime($from);
    $getfrom = $datetime->format('Y-m-d');

    $to = $arraytofrom[1];
    $datetime = new DateTime($to);
    $getto = $datetime->format('Y-m-d');
    if (!IsNull($from)) {
        $sqlTempfrom = " AND Date(AppointmentDate)>=Date('" . $getfrom . "')";
    }
    if (!IsNull($to)) {
        $sqlTempto = " and Date(AppointmentDate)<=Date('" . $getto . "')";
    }
}
if (isset($_GET["toandfrom"])) {
    $strtoandfrom = $_GET["toandfrom"];
    $arraytofrom = explode("-", $strtoandfrom);
    $from = $arraytofrom[0];
    $datetime = new DateTime($from);
    $getfrom = $datetime->format('Y-m-d');

    $to = $arraytofrom[1];
    $datetime = new DateTime($to);
    $getto = $datetime->format('Y-m-d');
    if (!IsNull($from)) {
        $sqlTempfrom = " AND Date(AppointmentDate)>=Date('" . $getfrom . "')";
    }
    if (!IsNull($to)) {
        $sqlTempto = " and Date(AppointmentDate)<=Date('" . $getto . "')";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once("incMetaScript.fya"); ?>

        <?php require_once("invoice_view/js_function.php"); ?>

    </head>
    <body onload="AddBalanceAmount();">
        <div id="sb-site">
            <?php // require_once("incOpenLayout.fya");   ?>
            <!----------commented by gandhali 3/9/18---------------->


            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>
                <?php require_once("incLeftMenu.fya"); ?>
                <div id="page-content-wrapper">
                    <div id="page-content">
                        <?php require_once("incHeader.fya"); ?>
                        <link rel="stylesheet" type="text/css" href="print.css" media="print" />
                        <div id="print_bill"></div>
                        <?php
                        if (isset($_GET['uid'])) {
                            $DB = Connect();
                            $uidd = DecodeQ($_GET['uid']);
                            $ud = EncodeQ($uidd);
                            $counter = 0;
                            $strID = DecodeQ($_GET['uid']);
                            ?>
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="fa-hover">
                                        <a href="javascript:window.location = document.referrer;" class="btn btn-primary btn-lg btn-block"><i class="fa fa-backward"></i> &nbsp; Go back to <?= $strPageTitle ?></a><br/>
                                        <center><b><span id="displayoffererror" style="text-align:center;text-weight:bold"></b></center>
                                        </span>
                                        <?php
                                        if (isset($data) != "") {
                                            ?>
                                            <h1><b><?php echo $data; ?></b></h1>
                                        <?php }
                                        ?>
                                    </div>

                                    <div class="panel-body col-md-4">
                                        <br/>
                                        <div class="example-box-wrapper">

                                            <?php
                                            /*
                                             * Get Appointment Data
                                             */
                                            $apt_all_data = select("*", "tblAppointments", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                            $apt_all_service_count = select("count(*)", "tblAppointmentsDetailsInvoice", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                            $apt_all_service = select("ServiceID", "tblAppointmentsDetailsInvoice", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                            $apt_all_customer = select("*", "tblCustomers", "CustomerID='" . $apt_all_data[0]['CustomerID'] . "'");
                                            $apt_all_invoice = select("InvoiceID", "tblInvoice", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                            $apt_all_store = select("*", "tblStores", "StoreID='" . $apt_all_data[0]['StoreID'] . "'");
                                            // Show Customer Available Package
                                            require_once("invoice_view/package_show.php");

                                            if ($invoiceflag != 'H') {
                                                // Show All Package List
                                                require_once("invoice_view/package_list.php");
                                                ?>

                                                <?php
                                                // Show All Service List
                                                require_once("invoice_view/service_list.php");
                                            }
                                            ?>

                                        </div>

                                        <!-----------------display membership dropdown----------------->
                                        <?php require_once("invoice_view/membership_show.php"); ?>
                                        <div class="example-box-wrapper" id="printbill" >
                                            <form id="printcontent" name="printcontent">
                                                <table border="0" cellspacing="0" cellpadding="0" width="100%" >
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center" >
                                                                    <tbody>
                                                                        <?php require_once("invoice_view/invoice_header.php"); ?>   

                                                                        <?php
                                                                        /*
                                                                         * GET Offer Discount Amount if Applied
                                                                         */
                                                                        $offerid = $apt_all_data[0]['offerid'];
                                                                        if ($offerid != "0") {
                                                                            ?>
                                                                            <tr>
                                                                                <td width="50%">&nbsp;</td>
                                                                                <?php
                                                                                $tax_offeramtt = 0;
                                                                                $offerid = $apt_all_data[0]['offerid'];
                                                                                $StoreIDd = $apt_all_data[0]['StoreID'];
                                                                                $memid = $apt_all_data[0]['memberid'];
                                                                                $seldofferp = select("*", "tblOffers", "OfferID='" . $offerid . "'");
                                                                                $services = $seldofferp[0]['ServiceID'];
                                                                                $offernamee = $seldofferp[0]['OfferName'];
                                                                                $baseamt = $seldofferp[0]['BaseAmount'];
                                                                                $Type = $seldofferp[0]['Type'];
                                                                                $TypeAmount = $seldofferp[0]['TypeAmount'];
                                                                                $StoreID = $seldofferp[0]['StoreID'];
                                                                                $stores = explode(",", $StoreID);
                                                                                $servicessf = explode(",", $services);
                                                                                $seldpdept = select("*", "tblAppointmentsDetailsInvoice", "AppointmentID='" . DecodeQ($_GET['uid']) . "' and PackageService='0'");
                                                                                if (in_array("$StoreIDd", $stores)) { /////////////////check service in specified store
                                                                                    $statuscheck = "No";
                                                                                    foreach ($seldpdept as $val) {
                                                                                        $totalammt = 0;
                                                                                        $serviceid = $val['ServiceID'];
                                                                                        $AppointmentDetailsID = $val['AppointmentDetailsID'];
                                                                                        $AppointmentID = $val['AppointmentID'];
                                                                                        if (in_array("$serviceid", $servicessf)) { ///////////////check which service in offer
                                                                                            $sqp = select("*", "tblAppointmentsDetailsInvoice", "ServiceID='" . $serviceid . "' and AppointmentID='" . $AppointmentID . "'");
                                                                                            $amtt = $sqp[0]['ServiceAmount'];
                                                                                            $qtyyy = $sqp[0]['qty'];
                                                                                            $totals = $qtyyy * $amtt;
                                                                                            $totalpp = $totalpp + $totals;
                                                                                            if ($baseamt != "") {
                                                                                                if ($totalpp >= $baseamt) {
                                                                                                    //echo 1;
                                                                                                    if ($Type == '1') { ////////////check type of amount % or amount
                                                                                                        if ($statuscheck == "No") {
                                                                                                            $servicefinal = $serviceid;
                                                                                                            //$offeramtt=$totalpp-$TypeAmount;
                                                                                                            $tax_offeramtt = $TypeAmount;
                                                                                                            $tax_amt_per = 0;
                                                                                                            $statuscheck = "Yes";
                                                                                                        } else {
                                                                                                            
                                                                                                        }
                                                                                                    } else {
                                                                                                        $tax_amt_per = $TypeAmount;
                                                                                                        $amt = $totals * $TypeAmount / 100;
                                                                                                        $tax_offeramtt += $amt;
                                                                                                    }
                                                                                                }
                                                                                            } else {
                                                                                                if ($Type == '1') {
                                                                                                    if ($statuscheck == "No") {
                                                                                                        $servicefinal = $serviceid;
                                                                                                        //$offeramtt=$totalpp-$TypeAmount;
                                                                                                        $tax_offeramtt = $TypeAmount;
                                                                                                        $tax_amt_per = 0;
                                                                                                        $statuscheck = "Yes";
                                                                                                    }
                                                                                                } else {
                                                                                                    $amt = $totals * $TypeAmount / 100;
                                                                                                    $tax_offeramtt += $amt;
                                                                                                    $tax_amt_per = $TypeAmount;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                            ?>
                                                                        <tr>
                                                                            <td>
                                                                                <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">

                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <th width="5%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">Sr</th>
                                                                                            <th width="40%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:left; padding-left:2%;">Item Description</th>
                                                                                            <th width="15%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">Qty / Valid Till</th>
                                                                                            <th width="15%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">Amount</th>
                                                                                            <th width="15%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">Action</th>
                                                                                        </tr>
                                                                                        <?php
                                                                                        $seldpdept = select("*", "tblAppointmentsDetailsInvoice", "AppointmentID='" . DecodeQ($_GET['uid']) . "' and PackageService='0'");
                                                                                        $sub_total = 0;
                                                                                        $countersaif = "";
                                                                                        $countsf = "0";
                                                                                        $counterusmani = "1";
                                                                                        foreach ($seldpdept as $val) {
//echo $val['ServiceID'];
                                                                                            $countersaif ++;
                                                                                            $countsf++;
                                                                                            $counterusmani = $counterusmani + 1;
                                                                                            $totalammt = 0;
                                                                                            $AppointmentDetailsID = $val['AppointmentDetailsID'];
                                                                                            $AppointmentDetailsServiceID = $val['ServiceID'];
                                                                                            $servicee = select("*", "tblServices", "ServiceID='" . $val['ServiceID'] . "'");
                                                                                            $qtyyy = $val['qty'];
                                                                                            $amtt = $val['ServiceAmount'];
                                                                                            $totalammt = $qtyyy * $amtt;
                                                                                            $total = 0;
                                                                                            ?>	
                                                                                            <!--display service detail-->										 
                                                                                            <tr>
                                                                                                <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;"><input type="hidden"  name="servicecode[]" value="<?php echo $servicee[0]['ServiceCode']; ?>" /><input type="hidden" name="membertype" id="membertype" class="membertype"/><?= $countsf ?></td>
                                                                                                <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; padding-left:2%;"><input type="hidden" name="serviceid[]" id="serviceid" value="<?php echo $val['ServiceID'] ?>" /><?php echo $servicee[0]['ServiceName']; ?></td>
                                                                                                <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; padding-left:2%;">
                                                                                                    <select id="<?= $countersaif ?>" class="quantity" onchange="test1(this)" name="qty[]">
                                                                                                        <?php
                                                                                                        /////////////qty////////////////////
                                                                                                        $count = 1;
                                                                                                        $max_count = isset($servicee[0]['max_quantity']) ? $servicee[0]['max_quantity'] : 0;
                                                                                                        while ($count <= $max_count) {
                                                                                                            if ($val['qty'] == $count) {
                                                                                                                ?>
                                                                                                                <option value=" <?php echo $count; ?>" selected='selected'  ><?php echo $count; ?></option>
                                                                                                                <?php
                                                                                                            } else {
                                                                                                                ?>

                                                                                                                <option value="<?= $count ?>"><?= $count ?></option>
                                                                                                                <?php
                                                                                                            }
                                                                                                            $count++;
                                                                                                        }
                                                                                                        ?>
                                                                                                    </select>
                                                                                                </td>
                                                                                                <td id="cost" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;"><input id="saif<?= $counterusmani ?>" name="serviceamt[]" type="text" readonly value="<?php echo $totalammt . ".00"; ?>" />
                                                                                                    <?php
                                                                                                    $sub_total = $sub_total + $totalammt;
                                                                                                    $total = $total + $sub_total;
                                                                                                    ?></td>
                                                                                                <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;" class="no-print">
                                                                                                    <input type="hidden" id="AppointmentDetailsID" value="<?= $AppointmentDetailsID ?>" />
                                                                                                    <?php if (isset($servicee[0]['is_concent']) && $servicee[0]['is_concent'] != 1) { ?>
                                                                                                        <a id="deleteservice" href="#" onClick="checkdelete(this)">Remove</a>
                                                                                                    <?php } ?>
                                                                                                </td>
                                                                                                <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:normal; text-align:center;" class="no-print">

                                                                                                </td>
                                                                                            </tr>

                                                                                            <?php
                                                                                            require_once("invoice_view/service_membership_discount.php");
                                                                                            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                            //////////////////////////////////display package name with validity-price//////////////////////////////////////		
                                                                                        }
                                                                                        require_once("invoice_view/invoice_package.php");
                                                                                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                        require_once("invoice_view/gift_voucher_redemp.php");
                                                                                        ///////////////////////////
                                                                                        ?>


                                                                                    </tbody>
                                                                                </table>
                                                                            </td>

                                                                        </tr>

                                                                        <tr>
                                                                            <td>
                                                                                <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                                                                    <tbody>
                                                                                        <!---------------------------------display sub totatl------------->
                                                                                        <?php
                                                                                        $seldember = $apt_all_data;
                                                                                        ?>
                                                                                        <tr>
                                                                                            <td width="50%">&nbsp;</td>
                                                                                            <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Sub Total</td>
                                                                                            <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="text" id="sub_total" name="sub_total" readonly value="<?php echo number_format($sub_total, 2); ?>" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <?php
                                                                                        $package_dis = 0;
                                                                                        if (isset($seldember[0]['PackageID']) && $seldember[0]['PackageID'] > 0) {
                                                                                            $package_dis_data = select("*", "tblCustomerPackageAmt", "appointment_id='" . DecodeQ($_GET['uid']) . "' AND package_id=" . $seldember[0]['PackageID'] . " AND status=1");
                                                                                            if (isset($package_dis_data[0]['package_amt_used']) && $package_dis_data[0]['package_amt_used'] > 0) {
                                                                                                $package_dis = $package_dis_data[0]['package_amt_used'];
                                                                                                ?>
                                                                                                <tr>
                                                                                                    <td width="50%">&nbsp;</td>
                                                                                                    <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Package Discount</td>
                                                                                                    <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;">
                                                                                                        <input type="text" id="sub_total" name="package_discount" readonly value="<?php echo " - " . number_format($package_dis, 2); ?>" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <?php
                                                                                            }
                                                                                        }
                                                                                        ?>

                                                                                        <?php
                                                                                        $VoucherID = $seldember[0]['VoucherID'];
                                                                                        $seldembert = select("Status", "tblGiftVouchers", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                                                        //print_r($seldembert);
                                                                                        $Status = $seldembert[0]['Status'];
                                                                                        if ($Status == '0') {
                                                                                            $selpt = select("*", "tblGiftVouchers", "AppointmentID='" . DecodeQ($_GET['uid']) . "' and Status='0'");
                                                                                            //print_r($selpt);
                                                                                            $amtt = $selpt[0]['Amount'];
                                                                                            $id = $selpt[0]['GiftVoucherID'];
                                                                                            $selptp = select("*", "tblGiftVouchers", "RedempedBy='" . DecodeQ($_GET['uid']) . "' and Status='1'");
                                                                                            if ($selptp != '0') {
                                                                                                $amttp = $selptp[0]['Amount'];
                                                                                                $id = $selptp[0]['GiftVoucherID'];
                                                                                                if ($amttp != "0") {
                                                                                                    if ($amtt > $sub_total) {
                                                                                                        $redamt = $sub_total;
                                                                                                        $sub_total = 0;
                                                                                                    } else {
                                                                                                        $totalamt = $amtt - $amttp;
                                                                                                        $sub_total = $sub_total + $totalamt;
                                                                                                    }
                                                                                                    ?>
                                                                                                    <tr>
                                                                                                        <td width="50%">&nbsp;</td>
                                                                                                        <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Redemption Gift Voucher Discount</td>
                                                                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="hidden" name="Redemptid" Value="<?= $id ?>" /><input type="text" name="vouchercost" readonly value="<?php echo " - " . number_format($amttp, 2); ?>" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <?php
                                                                                                } else {
                                                                                                    
                                                                                                }
                                                                                            }
                                                                                            ////////////////////////////////////////////////////////////////////////////////////////
                                                                                            ///////////////////////////////////////////////display gift voucher purchase cost/////////////////////////////
                                                                                            $sepcont = $apt_all_service_count;
                                                                                            $cntserp = $sepcont[0]['count(*)'];
                                                                                            if ($cntserp > 0) {
                                                                                                $sub_total = $sub_total + $amtt;
                                                                                                ?>
                                                                                                <tr>
                                                                                                    <td width="50%">&nbsp;</td>
                                                                                                    <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Gift Voucher Cost</td>
                                                                                                    <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="hidden" name="purchaseid" Value="<?= $id ?>" /><input type="text" name="vouchercost" readonly value="<?php echo " + " . number_format($amtt, 2); ?>" />
                                                                                                    </td>
                                                                                                    <?php
                                                                                                } else {
                                                                                                    
                                                                                                }
                                                                                                ?>
                                                                                            </tr>
                                                                                            <?php
                                                                                        } else {
////////////////////////////display gift voucher redemption amount//////////////
                                                                                            $selpt = select("*", "tblGiftVouchers", "RedempedBy='" . DecodeQ($_GET['uid']) . "' and Status='1'");
                                                                                            $amtt = $selpt[0]['Amount'];
                                                                                            $RedempedBy = $selpt[0]['RedempedBy'];
                                                                                            $id = $selpt[0]['GiftVoucherID'];
                                                                                            if ($amtt != '0') {
                                                                                                if ($RedempedBy == DecodeQ($_GET['uid'])) {

                                                                                                    if ($amtt > $sub_total) {
                                                                                                        $redamt = $sub_total;
                                                                                                        $sub_total = 0;
                                                                                                    } else {
                                                                                                        $sub_total = $sub_total - $amtt;
                                                                                                    }
                                                                                                    ?>
                                                                                                    <tr>
                                                                                                        <td width="50%">&nbsp;</td>
                                                                                                        <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Redemption Gift Voucher Discount</td>
                                                                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="hidden" name="Redemptid" Value="<?= $id ?>" /><input type="text" name="vouchercost" readonly value="<?php echo " - " . number_format($amtt, 2); ?>" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <?php
                                                                                                }
                                                                                            }
                                                                                            $selpt = select("*", "tblGiftVouchers", "AppointmentID='" . DecodeQ($_GET['uid']) . "' and Status='0'");
                                                                                            if ($selpt != '0') {
                                                                                                $sepcont = $apt_all_service_count;
                                                                                                $cntserp = $sepcont[0]['count(*)'];
                                                                                                if ($cntserp > 0) {
                                                                                                    $amttp = $selpt[0]['Amount'];
                                                                                                    $id = $selpt[0]['GiftVoucherID'];
                                                                                                    $sub_total = $sub_total + $amttp;
                                                                                                    ?>
                                                                                                    <tr>
                                                                                                        <td width="50%">&nbsp;</td>
                                                                                                        <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Gift Voucher Cost</td>
                                                                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="hidden" name="purchaseid" Value="<?= $id ?>" /><input type="text" name="vouchercost" readonly value="<?php echo " + " . number_format($amttp, 2); ?>" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <?php
                                                                                                } else {
                                                                                                    
                                                                                                }
                                                                                            } else {
                                                                                                
                                                                                            }
                                                                                        }

                                                                                        //////////////////////////////////////display first time membercost if flag is processing and hold/////////////////////////////				   
                                                                                        $seldember = $apt_all_data;
                                                                                        $memid = $seldember[0]['memberid'];
                                                                                        $custid = $seldember[0]['CustomerID'];
                                                                                        $seldemberg = select("*", "tblMembership", "MembershipID='" . $memid . "'");
                                                                                        $Cost = $seldemberg[0]['Cost'];
                                                                                        $selcust = $apt_all_customer;
                                                                                        $memberflag = $selcust[0]['memberflag'];
                                                                                        $cust_name = $selcust[0]['CustomerFullName'];
                                                                                        $memidd = $selcust[0]['memberid'];
                                                                                        if ($memberflag == '0') {
                                                                                            if ($memid != "0") {
                                                                                                $sub_total = $sub_total + $Cost;
                                                                                                $member_ship_tax_amount = number_format($Cost, 2);
                                                                                                ?>
                                                                                                <tr>
                                                                                                    <td width="50%">&nbsp;</td>
                                                                                                    <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">
                                                                                                        <?= $membershipname ?>&nbsp;Cost</td>
                                                                                                    <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="text" name="membercost" readonly value="<?php echo " + " . number_format($Cost, 2); ?>" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <?php
                                                                                            }
                                                                                        } elseif ($memberflag == '1') {
                                                                                            
                                                                                        } elseif ($memberflag == '2') {
                                                                                            
                                                                                        } else {
                                                                                            ////////////////check type of bill if hold with flag in memberflag of customer table///////////////////
                                                                                            $est = explode(",", $memberflag);
                                                                                            if ($est[0] == '3') {
                                                                                                $app_id = DecodeQ($_GET['uid']);
                                                                                                if ($app_id == $est[1]) {
                                                                                                    $selcustd = select("Membership_Amount", "tblInvoiceDetails", "CustomerFullName='" . $cust_name . "' and AppointmentId='" . DecodeQ($_GET['uid']) . "'");
                                                                                                    $Membership_Amount = $selcustd[0]['Membership_Amount'];
                                                                                                    if ($Membership_Amount != "") {
                                                                                                        $sub_total = $sub_total + $Cost;
                                                                                                        $member_ship_tax_amount = number_format($Cost, 2);
                                                                                                        ?>
                                                                                                        <tr>
                                                                                                            <td width="50%">&nbsp;</td>
                                                                                                            <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">
                                                                                                                <?= $membershipname ?>&nbsp;Cost</td>
                                                                                                            <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="text" name="membercost" readonly value="<?php echo " + " . number_format($Cost, 2); ?>" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <?php
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            ?>
                                                                                            <?php
                                                                                        }
                                                                                        /////////////////////////////////////////////////////
                                                                                        ////////////////////////////////////////display offer td/////////////////////////////////
                                                                                        require_once("invoice_view/invoice_offer.php");

                                                                                        ///////////////////////////////////////////////////////////////
                                                                                        ////////////////Member Discount///////////////////////
                                                                                        $seldember = select("*", "tblAppointments", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                                                        $memid = $seldember[0]['memberid'];
                                                                                        $offerid = $seldember[0]['offerid'];
                                                                                        $custid = $seldember[0]['CustomerID'];
                                                                                        $seldemberg = select("*", "tblMembership", "MembershipID='" . $memid . "'");
                                                                                        $Cost = $seldemberg[0]['Cost'];
                                                                                        $selcustd = select("Membership_Amount", "tblInvoiceDetails", "CustomerFullName='" . $cust_name . "' and AppointmentId='" . DecodeQ($_GET['uid']) . "'");
                                                                                        $Membership_Amount = $selcustd[0]['Membership_Amount'];
                                                                                        $selcust = select("*", "tblCustomers", "CustomerID='" . $custid . "'");
                                                                                        $memberflag = $selcust[0]['memberflag'];
                                                                                        $CustomerFullName = $selcust[0]['CustomerFullName'];
                                                                                        $seldofferp = select("*", "tblOffers", "OfferID='" . $offerid . "'");
                                                                                        $services = $seldofferp[0]['ServiceID'];
                                                                                        $baseamt = $seldofferp[0]['BaseAmount'];
                                                                                        $Type = $seldofferp[0]['Type'];
                                                                                        $TypeAmount = $seldofferp[0]['TypeAmount'];
                                                                                        $StoreID = $seldofferp[0]['StoreID'];
                                                                                        $stores = explode(",", $StoreID);
                                                                                        if ($memid != "0" && number_format($memberdis, 2) > 0) {
                                                                                            ?>
                                                                                            <tr>
                                                                                                <td width="50%">&nbsp;</td>
                                                                                                <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">
                                                                                                    <?= $membershipname ?>&nbsp; Discount</td>
                                                                                                <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="text" name="memberdiscost" readonly value="<?php echo " - " . number_format($memberdis, 2); ?>" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <?php
                                                                                        }
//////////////////////////////////////////////////////////////////////////////////////////////////													
                                                                                        //////////////////////////////////////display charges////////////////////////////////////////////////////////////////////////
                                                                                        $seldoffert = select("*", "tblAppointments", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                                                        $FreeService = $seldoffert[0]['FreeService'];
                                                                                        if ($FreeService != "0") {
                                                                                            
                                                                                        } else {
                                                                                            $sepcont = select("count(*)", "tblAppointmentsDetailsInvoice", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                                                            $cntserp = $sepcont[0]['count(*)'];
                                                                                            $sqlExtraCharges = select("DISTINCT (ChargeName), SUM( ChargeAmount ) AS Sumarize", "tblAppointmentsChargesInvoice", "AppointmentID ='" . DecodeQ($_GET['uid']) . "' AND AppointmentDetailsID !=0 GROUP BY ChargeName");

                                                                                            foreach ($sqlExtraCharges as $vaqq) {
                                                                                                $strChargeNameDetails = $vaqq["ChargeName"];
                                                                                                $strChargeAmountDetails = $vaqq["Sumarize"];
                                                                                                ?>

                                                                                                <?php if ($strChargeNameDetails == 'GST') { ?>
                                                                                                    <tr>
                                                                                                        <td width="50%">&nbsp;</td>
                                                                                                        <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;"><input type="text" name="chargename[]" readonly value="CGST @ 9%" /></td>
                                                                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="text" name="chargeamount[]" readonly value="<?= " + " . $strChargeAmountDetails / 2 ?>" /></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td width="50%">&nbsp;</td>
                                                                                                        <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;"><input type="text" name="chargename[]" readonly value="SGST @ 9%" /></td>
                                                                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="text" name="chargeamount[]" readonly value="<?= " + " . $strChargeAmountDetails / 2 ?>" /></td>
                                                                                                    </tr>
                                                                                                <?php } else { ?>
                                                                                                    <tr>
                                                                                                        <td width="50%">&nbsp;</td>
                                                                                                        <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;"><input type="text" name="chargename[]" readonly value="<?= $strChargeNameDetails ?>" /></td>
                                                                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="text" name="chargeamount[]" readonly value="<?= " + " . $strChargeAmountDetails ?>" /></td>
                                                                                                    </tr>
                                                                                                <?php } ?>

                                                                                                <?php
                                                                                                $amountdetail = $amountdetail + $strChargeAmountDetails;
                                                                                            }
                                                                                        }
                                                                                        $total = 0;
                                                                                        //echo 
                                                                                        if ($amtt > $sub_total) {
                                                                                            $totalred = $sub_total + $redamt;
                                                                                            $total = $total + $sub_total;
                                                                                            ?>
                                                                                        <input type="hidden" name="totalredamt" id="totalredamt" value="<?= $totalred ?>" />
                                                                                        <?php
                                                                                    } else {
                                                                                        $total = $total + $sub_total + $amountdetail - $offeramtt - $memberdis - $package_dis;
                                                                                    }
                                                                                    if ($total < 0) {
                                                                                        $total = 0;
                                                                                    }
                                                                                    ?>
                                                                        </tr>
                                                                        <?php
                                                                        ////////////////////////////if pending amount is remain then it display//////////////////
                                                                        $sept = select("PendingAmount,AppointmentID", "tblPendingPayments", "CustomerID='" . $CustomerFullName . "'AND Status=1 AND PendingStatus=2");

                                                                        foreach ($sept as $val) {
                                                                            $totalpendamt = $totalpendamt + $val['PendingAmount'];
                                                                            $total_appointment[0] = $val['AppointmentID'];
                                                                        }
                                                                        if (isset($total_appointment) && is_array($total_appointment) && count($total_appointment) > 0) {
                                                                            $DB = Connect();
                                                                            $apt_in_ids = implode(",", $total_appointment);
                                                                            $date_q = "SELECT AppointmentID,AppointmentDate FROM tblAppointments WHERE AppointmentID IN(" . $apt_in_ids . ")";
                                                                            $date_q_exe = $DB->query($date_q);
                                                                            while ($aptdetails = $date_q_exe->fetch_assoc()) {
                                                                                $all_apt_date[] = $aptdetails['AppointmentDate'];
                                                                            }
                                                                            $DB->close();
                                                                        }
                                                                        ?>
                                                                        <tr id="pendingpayment" style="display:none">
                                                                    <input type="hidden" name="pending_amt_apt_id" value="<?php echo $total_appointment[0]; ?>"/>
                                                                    <td>&nbsp;</td>
                                                                    <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;">Pending Amount
                                                                        <?php
                                                                        if (isset($all_apt_date) && is_array($all_apt_date) && count($all_apt_date) > 0) {
                                                                            echo "<br><b>(Invoice Date : " . implode(", ", $all_apt_date) . ")</b>";
                                                                        }
                                                                        ?>
                                                                    </td>
                                                                    <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="text" name="totalpend" id="totalpend" readonly /></td>
                                                        </tr>
                                                        <!-------display round total--------->
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Total</td>
                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;" id="totalvalue"><input type="text" name="total" id="totalcost" readonly value="<?= $total ?>" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Round Off</td>
                                                            <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input id="roundtotal" type="text" name="roundtotal" readonly value="<?php
                                                                echo round($total);
                                                                //  $total=0;
                                                                ?>" /></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                </td>
                                                </tr>
                                                <?php
                                                //////////////////////////check type of flag and display pending amount according //////////////////////////
                                                $seldppay = select("*", "tblInvoiceDetails", "AppointmentId='" . DecodeQ($_GET['uid']) . "'");
                                                $amount = $seldppay[0]['CashAmount'];
                                                $flag = $seldppay[0]['Flag'];
                                                if ($flag == 'CS') {
                                                    ?>
                                                    <tr>
                                                        <td id="payment">
                                                            <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">

                                                                <tbody>
                                                                    <tr>
                                                                        <td colspan="3" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:16px;FONT-WEIGHT:bold; text-align:center;">Payments</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                        <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Cash</td>
                                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;">
                                                                            <?php echo $amount ?>
                                                                        </td>
                                                                    </tr>

                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                } elseif ($flag == 'H') {
                                                    $seldppayt = select("*", "tblPendingPayments", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                    $pendamt = $seldppayt[0]['PendingAmount'];
                                                    ?>
                                                    <tr>
                                                        <td id="payment">
                                                            <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">

                                                                <tbody>
                                                                    <tr>
                                                                        <td colspan="3" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:16px;FONT-WEIGHT:bold; text-align:center;">Payments</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                        <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Pending Amount</td>
                                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;">
                                                                            <?php echo $pendamt ?>
                                                                        </td>
                                                                    </tr>

                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                } elseif ($flag == 'C') {
                                                    ?>
                                                    <tr>
                                                        <td id="payment">
                                                            <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">

                                                                <tbody>
                                                                    <tr>
                                                                        <td colspan="3" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:16px;FONT-WEIGHT:bold; text-align:center;">Payments</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                        <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Card</td>
                                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;">
                                                                            <?php echo $amount ?>
                                                                        </td>
                                                                    </tr>

                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <tr>
                                                        <td style="display:none" id="payment">
                                                            <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">

                                                                <tbody>
                                                                    <tr>
                                                                        <td colspan="3" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:16px;FONT-WEIGHT:bold; text-align:center;">Payments</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                        <td width="30%" id="displaytext" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Cash</td>
                                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="number" id="paymentid" name="cashamt" value="<?php echo $total; ?>" onkeyup="test()" /></td>

                                                                    </tr>
                                                                    <tr>
                                                                        <td>&nbsp;</td>
                                                                        <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Total Payment</td>
                                                                        <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input id="totalpayment" name="totalpayment" value="<?php
                                                                            echo round($total);
//  $total=0;
                                                                            ?>" /></td>
                                                                    </tr>

                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
/////////////////////////////////////////////////////////////////////
                                                ?>
                                                <tr>
                                                    <td id="paymenttype">
                                                        <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                                            <tbody>
                                                                <?php
                                                                $sep = select("count(PendingPaymentID)", "tblPendingPayments", "CustomerID='" . $CustomerFullName . "' and Status='1' and PendingStatus='2'");
                                                                $cnt = $sep[0]['count(PendingPaymentID)'];
                                                                $totalpendamt = 0;
                                                                if ($cnt > 0) {
                                                                    $sept = select("PendingAmount,AppointmentID", "tblPendingPayments", "CustomerID='" . $CustomerFullName . "' and Status='1' and PendingStatus='2'");
                                                                    foreach ($sept as $val) {
                                                                        $totalpendamt = $totalpendamt + $val['PendingAmount'];
                                                                        $total_appointment[0] = $val['AppointmentID'];
                                                                    }
                                                                    if ($flag == 'H') {
                                                                        
                                                                    } else {
                                                                        if (isset($total_appointment) && is_array($total_appointment) && count($total_appointment) > 0) {
                                                                            $DB = Connect();
                                                                            $apt_in_ids = implode(",", $total_appointment);
                                                                            $date_q = "SELECT AppointmentID,AppointmentDate FROM tblAppointments WHERE AppointmentID IN(" . $apt_in_ids . ")";
                                                                            $date_q_exe = $DB->query($date_q);
                                                                            while ($aptdetails = $date_q_exe->fetch_assoc()) {
                                                                                $all_apt_date[0] = $aptdetails['AppointmentDate'];
                                                                            }
                                                                            $DB->close();
                                                                        }
                                                                        ?>
                                                                        <tr id="showpend">
                                                                            <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                            <td width="15%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;">Till Date Pending Payments 
                                                                                <?php
                                                                                if (isset($all_apt_date) && is_array($all_apt_date) && count($all_apt_date) > 0) {
                                                                                    echo "<br>Invoice Date : " . implode(", ", $all_apt_date);
                                                                                }
                                                                                ?>
                                                                            </td>
                                                                    <input type="hidden" name="pending_amt_apt_id" value="<?php echo $total_appointment[0]; ?>"/>
                                                                    <td width="15%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"><input type="text" name="totalpendamt" id="totalpendamt" readonly value="<?= $totalpendamt ?>" /></td>
                                                                    <td width="15%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"><button type="button" id="addtobill" style="display:inline-block;" value="<?php echo $totalpendamt ?>" class="btn btn-success" data-toggle="button"><center>Add</center></button></td>

                                                        </tr>
                                                        <?php
                                                    }
                                                } else {
                                                    
                                                }
                                                ?>
                                                <!--decide which type of paymenyt--->
                                                <tr id="paymenttype1">
                                                    <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                    <td width="20%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-WEIGHT:bold;FONT-SIZE:14px;"><input type="radio" name="paytype" id="paytype" value="Partial" />Partial</td>
                                                    <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold;"><input type="radio" name="paytype" value="Complete" id="paytype" checked />Complete</td>
                                                </tr>
                                                <!--distribute amount complete partial and pending-->
                                                <?php
                                                $apt_paid_data = select("*", "tblAppointments", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                $prepaid_amount = isset($apt_paid_data[0]['prepaid_amount']) ? $apt_paid_data[0]['prepaid_amount'] : 0;
                                                if ($prepaid_amount > 0) {
                                                    $total = $total - $prepaid_amount;
                                                    ?>
                                                    <tr>
                                                        <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                        <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Online Paid Amount</td>
                                                        <?php //disabled by aman, 17th may 2019    ?>
                                                        <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="text" value="-<?= number_format($prepaid_amount, 2) ?>" disabled /></td>
                                                    </tr>
                                                <?php }
                                                ?>
                                                <tr>
                                                    <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                    <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Complete Amount</td>

                                                    <?php
                                                    /* if (isset($totalpendamt)) {
                                                      $total = $total + $totalpendamt;
                                                      } */
                                                    ?>
                                                    <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="number" id="completeamtt" name="completeamtt" readonly value="<?= round($total); ?>" /></td>

                                                </tr>

                                                <tr style="display:none" class="partial">
                                                    <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                    <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Partial Amount</td>
                                                    <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="number" id="completeamt" name="completeamt" onkeyup="calculatecomplete()" /></td>
                                                </tr>
                                                <tr style="display:none" class="partial">
                                                    <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                    <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Pending Amount</td>
                                                    <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="number" id="pendamt" name="pendamt" onkeyup="calculatepend()" readonly /></td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Total Payment</td>
                                                    <td style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input id="totalpaymentamt" name="totalpaymentamt" readonly value="<?= round($total); ?>" /></td>
                                                </tr>
                                                </tbody>
                                                </table>
                                                </td>
                                                </tr>
                                                <tr>
                                                    <td style="display:none" id="payment1">
                                                        <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">

                                                            <tbody>
                                                                <tr>
                                                                    <td colspan="3" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:16px;FONT-WEIGHT:bold; text-align:center;">Payments</td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                    <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Cash Amount</td>
                                                                    <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="number" id="cashboth" name="cashboth" onkeyup="calculatecashamount()" /></td>

                                                                </tr>
                                                                <tr>
                                                                    <td width="50%" style="LINE-HEIGHT:25px;PADDING-LEFT:5px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;"></td>
                                                                    <td width="30%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:14px;FONT-WEIGHT:bold; text-align:right;">Card Amount</td>
                                                                    <td width="20%" style="LINE-HEIGHT:25px;FONT-FAMILY:Verdana,Geneva,sans-serif;COLOR:#000;FONT-SIZE:12px;FONT-WEIGHT:bold; text-align:center;"><input type="number" id="cardboth" name="cardboth" onkeyup="calculatecardamt()" /></td>
                                                                </tr>


                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td height="8">
                                                        <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                                            <tbody>
                                                                <tr>
                                                                    <td bgcolor="#e4e4e4" height="4"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <!--<tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <td height="8"></td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </tr>-->
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <!--<tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             <style>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            .con  {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            height:200px;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            width:100%;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            border:1px solid #d0ad53;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            }												
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            </style>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       <div class="con">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <p align="center">Advertisement </p>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               </div>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               </td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </tr>-->
                                                <tr>
                                                    <td style="BACKGROUND:#d0ad53;">
                                                        <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
                                                            <tbody>


                                                            <td width="33%" style="FONT-FAMILY:Arial,Helvetica,sans-serif;BACKGROUND:#d0ad53;COLOR:#000;FONT-SIZE:12px; padding:1%;" height="32" align="center">
                                                                <span style="font-size:14px; font-weight:600;">KHAR | BREACH CANDY | ANDHERI | COLABA | LOKHANDWALA</span><br>
                                                            </td>
                                                            </tbody>
                                                        </table>
                                                    </td>

                                                </tr>
                                                </tbody>
                                                </table>
                                                </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                </tbody>
                                                </table>
                                                </td>
                                                </tr>
                                                </tbody>
                                                </table>
                                            </form>
                                        </div>
                                        <?php
//////////////////////////check any employee is assign or not/////////////////////////////////////
                                        $seldoffertqy = select("*", "tblAppointments", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                        $FreeService = $seldoffertqy[0]['FreeService'];
                                        $PackageID = $seldoffertqy[0]['PackageID'];
                                        $packbunch = explode(",", $PackageID);
                                        $sepcont = select("count(*)", "tblAppointmentsDetailsInvoice", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                        $cntserp = $sepcont[0]['count(*)'];
                                        if ($cntserp > 0) {
                                            if ($FreeService != "0") {
                                                foreach ($seldpdept as $val) {
                                                    $seldt = select("count(AppointmentAssignEmployeeID)", "tblAppointmentAssignEmployee", "AppointmentID='" . DecodeQ($_GET['uid']) . "' and ServiceID='" . $val['ServiceID'] . "'");
                                                    $cnt = $seldt[0]['count(AppointmentAssignEmployeeID)'];
                                                    if ($cnt > 0) {
                                                        $seldppay = select("*", "tblAppointmentAssignEmployee", "AppointmentID='" . DecodeQ($_GET['uid']) . "' and ServiceID='" . $val['ServiceID'] . "'");
                                                        foreach ($seldppay as $value) {
                                                            if ($value['ServiceID'] == '0') {
                                                                $daata = 1;
                                                            } elseif ($value['MECID'] == '0') {
                                                                $daata = 1;
                                                            } else {
                                                                $daata = 2;
                                                            }
                                                        }
                                                    } elseif ($cnt == '0') {
                                                        $daata = 3;
                                                    } else {
                                                        $daata = 2;
                                                    }
                                                }
                                                if ($daata == '1') {
                                                    ?>
                                                    <a href="appointment_invoice.php" class="btn btn-primary" style="float: left;">Go Back</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="updateemployee.php?uid=<?= $ud ?>" class="btn btn-primary">Assign Employee</a>

                                                    <?php
                                                } elseif ($daata == '3') {
                                                    ?>
                                                    <a href="appointment_invoice.php" class="btn btn-primary" style="float: left;">Go Back</a>&nbsp;&nbsp;&nbsp;<a href="updateemployee.php?uid=<?= $ud ?>" class="btn btn-primary">Assign Employee</a>
                                                    <?php
                                                } elseif ($daata == '2') {
                                                    ?>
                                                    <a href="appointment_invoice.php" class="btn btn-primary" style="float: left;">Go Back</a>&nbsp;&nbsp;&nbsp;<button type="button" id="CompleteAmt" value="Complete" class="btn btn-primary active" data-toggle="button" style="float:left;">Complete Free Bill</button>
                                                    <!-- Modal -->
                                                    <?php
                                                }
                                            } else {
                                                if ($PackageID != "") {

                                                    $seldtpt = select("count(AppointmentAssignEmployeeID)", "tblAppointmentAssignEmployee", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                    $cnty = $seldtpt[0]['count(AppointmentAssignEmployeeID)'];
                                                    if ($cnty > 0) {
                                                        $seldpdeptq = select("*", "tblAppointmentsDetailsInvoice", "AppointmentID='" . DecodeQ($_GET['uid']) . "'");
                                                        foreach ($seldpdeptq as $val) {
                                                            $seldpdas = select("count(*)", "tblAppointmentsDetailsInvoice", "AppointmentID='" . DecodeQ($_GET['uid']) . "' and ServiceID='" . $val['ServiceID'] . "' and PackageService!='0'");
                                                            $cntas = $seldpdas[0]['count(*)'];
                                                            if ($cntas > 0) {
                                                                $seldt = select("count(AppointmentAssignEmployeeID)", "tblAppointmentAssignEmployee", "AppointmentID='" . DecodeQ($_GET['uid']) . "' and ServiceID='" . $val['ServiceID'] . "'");
                                                                $cnt = $seldt[0]['count(AppointmentAssignEmployeeID)'];
                                                                if ($cnt > 0) {
                                                                    $seldppay = select("*", "tblAppointmentAssignEmployee", "AppointmentID='" . DecodeQ($_GET['uid']) . "' and ServiceID='" . $val['ServiceID'] . "'");
                                                                    foreach ($seldppay as $value) {
                                                                        if ($value['ServiceID'] == '0') {
                                                                            $daata = 1;
                                                                        } elseif ($value['MECID'] == '0') {
                                                                            $daata = 1;
                                                                        } else {
                                                                            $daata = 2;
                                                                        }
                                                                    }
                                                                } elseif ($cnt == '0') {
                                                                    $seldpdaspo = select("PackageService", "tblAppointmentsDetailsInvoice", "AppointmentID='" . DecodeQ($_GET['uid']) . "' and ServiceID='" . $val['ServiceID'] . "'");
                                                                    $PackageService = $seldpdaspo[0]['PackageService'];
                                                                    if ($PackageService != "0") {
                                                                        $daata = 2;
                                                                    } else {
                                                                        $daata = 3;
                                                                    }
                                                                } else {
                                                                    $daata = 2;
                                                                }
                                                            } else {
                                                                $seldt = select("count(AppointmentAssignEmployeeID)", "tblAppointmentAssignEmployee", "AppointmentID='" . DecodeQ($_GET['uid']) . "' and ServiceID='" . $val['ServiceID'] . "'");
                                                                $cnt = $seldt[0]['count(AppointmentAssignEmployeeID)'];
                                                                if ($cnt > 0) {
                                                                    $seldppay = select("*", "tblAppointmentAssignEmployee", "AppointmentID='" . DecodeQ($_GET['uid']) . "' and ServiceID='" . $val['ServiceID'] . "'");
                                                                    foreach ($seldppay as $value) {
                                                                        if ($value['ServiceID'] == '0') {
                                                                            $daata = 1;
                                                                        } elseif ($value['MECID'] == '0') {
                                                                            $daata = 1;
                                                                        } else {
                                                                            $daata = 2;
                                                                        }
                                                                    }
                                                                } elseif ($cnt == '0') {
                                                                    $daata = 3;
                                                                } else {
                                                                    $daata = 2;
                                                                }
                                                            }
                                                        }
                                                        if ($daata == '1') {
                                                            ?>
                                                            <a href="appointment_invoice.php" class="btn btn-primary" style="float: left;">Go Back</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="updateemployee.php?uid=<?= $ud ?>" class="btn btn-primary">Assign Employee</a>

                                                            <?php
                                                        } elseif ($daata == '3') {
                                                            ?>
                                                            <a href="appointment_invoice.php" class="btn btn-primary" style="float: left;">Go Back</a>&nbsp;&nbsp;&nbsp;<a href="updateemployee.php?uid=<?= $ud ?>" class="btn btn-primary">Assign Employee</a>
                                                            <?php
                                                        } elseif ($daata == '2') {
                                                            if ($invoiceflag == 'H') {
                                                                ?>
                                                                <button type="button" id="cash" value="cash" class="btn btn-success" data-toggle="button"><center>Cash</center></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <button type="button" id="card" value="card" class="btn btn-info" data-toggle="button" style="float:left;">Card</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" style="display:none" id="confirm" value="Confirm" class="btn btn-info" data-toggle="button" style="float:left;">Confirm</button>
                                                                <button type="button" id="both" value="both" class="btn btn-blue-alt" data-toggle="button" style="float:left;">Both</button>&nbsp;&nbsp;
                                                                <a href="appointment_invoice.php" class="btn btn-primary" style="float: left;">Go Back</a>
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <button type="button" id="cash" value="cash" class="btn btn-success" data-toggle="button"><center>Cash</center></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <button type="button" id="card" value="card" class="btn btn-info" data-toggle="button" style="float:left;">Card</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" style="display:none" id="confirm" value="Confirm" class="btn btn-info" data-toggle="button" style="float:left;">Confirm</button>
                                                                <button type="button" id="hold" value="hold" class="btn btn-primary active" data-toggle="button" style="float:left;">Balance Payable</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <button type="button" id="both" value="both" class="btn btn-blue-alt" data-toggle="button" style="float:left;">Both</button>&nbsp;&nbsp;
                                                                <a href="appointment_invoice.php" class="btn btn-primary" style="float: left;">Go Back</a>
                                                                <a href="updateemployee.php?uid=<?= $ud ?>" class="btn btn-primary">Assign Employee</a>
                                                                <?php
                                                            }
                                                        }
                                                    } else {
                                                        ?>
                                                        <button type="button" id="cash" value="cash" class="btn btn-success" data-toggle="button"><center>Cash</center></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <button type="button" id="card" value="card" class="btn btn-info" data-toggle="button" style="float:left;">Card</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" style="display:none" id="confirm" value="Confirm" class="btn btn-info" data-toggle="button" style="float:left;">Confirm</button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <button type="button" id="both" value="both" class="btn btn-blue-alt" data-toggle="button" style="float:left;">Both</button>&nbsp;&nbsp;
                                                        <a href="appointment_invoice.php" class="btn btn-primary" style="float: left;">Go Back</a>
                                                        <a href="updateemployee.php?uid=<?= $ud ?>" class="btn btn-primary">Assign Employee</a>
                                                        <?php
                                                    }
                                                } else {
                                                    foreach ($seldpdept as $val) {
                                                        $seldt = select("count(AppointmentAssignEmployeeID)", "tblAppointmentAssignEmployee", "AppointmentID='" . DecodeQ($_GET['uid']) . "' and ServiceID='" . $val['ServiceID'] . "'");
                                                        $cnt = $seldt[0]['count(AppointmentAssignEmployeeID)'];
                                                        if ($cnt > 0) {
                                                            $seldppay = select("*", "tblAppointmentAssignEmployee", "AppointmentID='" . DecodeQ($_GET['uid']) . "' and ServiceID='" . $val['ServiceID'] . "'");
                                                            foreach ($seldppay as $value) {
                                                                if ($value['ServiceID'] == '0') {
                                                                    $daata = 1;
                                                                } elseif ($value['MECID'] == '0') {
                                                                    $daata = 1;
                                                                } else {
                                                                    $daata = 2;
                                                                }
                                                            }
                                                        } elseif ($cnt == '0') {
                                                            $daata = 3;
                                                        } else {
                                                            $daata = 2;
                                                        }
                                                    }
                                                    //echo $daata;	
                                                    if ($daata == '1') {
                                                        ?>
                                                        <a href="appointment_invoice.php" class="btn btn-primary" style="float: left;">Go Back</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="updateemployee.php?uid=<?= $ud ?>" class="btn btn-primary">Assign Employee</a>

                                                        <?php
                                                    } elseif ($daata == '3') {
                                                        ?>
                                                        <a href="appointment_invoice.php" class="btn btn-primary" style="float: left;">Go Back</a>&nbsp;&nbsp;&nbsp;<a href="updateemployee.php?uid=<?= $ud ?>" class="btn btn-primary">Assign Employee</a>
                                                        <?php
                                                    } elseif ($daata == '2') {
                                                        if ($invoiceflag == 'H') {
                                                            ?>
                                                            <button type="button" id="cash" value="cash" class="btn btn-success" data-toggle="button"><center>Cash</center></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <button type="button" id="card" value="card" class="btn btn-info" data-toggle="button" style="float:left;">Card</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" style="display:none" id="confirm" value="Confirm" class="btn btn-info" data-toggle="button" style="float:left;">Confirm</button>
                                                            <button type="button" id="both" value="both" class="btn btn-blue-alt" data-toggle="button" style="float:left;">Both</button>&nbsp;&nbsp;
                                                            <a href="appointment_invoice.php" class="btn btn-primary" style="float: left;">Go Back</a>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <button type="button" id="cash" value="cash" class="btn btn-success" data-toggle="button"><center>Cash</center></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <button type="button" id="card" value="card" class="btn btn-info" data-toggle="button" style="float:left;">Card</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" style="display:none" id="confirm" value="Confirm" class="btn btn-info" data-toggle="button" style="float:left;">Confirm</button>
                                                            <button type="button" id="hold" value="hold" class="btn btn-primary active" data-toggle="button" style="float:left;">Balance Payable</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <button type="button" id="both" value="both" class="btn btn-blue-alt" data-toggle="button" style="float:left;">Both</button>&nbsp;&nbsp;
                                                            <a href="appointment_invoice.php" class="btn btn-primary" style="float: left;">Go Back</a>
                                                            <a href="updateemployee.php?uid=<?= $ud ?>" class="btn btn-primary">Assign Employee</a>
                                                            <?php
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            ?>
                                            <button type="button" id="cash" value="cash" class="btn btn-success" data-toggle="button"><center>Cash</center></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <button type="button" id="card" value="card" class="btn btn-info" data-toggle="button" style="float:left;">Card</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" style="display:none" id="confirm" value="Confirm" class="btn btn-info" data-toggle="button" style="float:left;">Confirm</button>
                                            <button type="button" id="hold" value="hold" class="btn btn-primary active" data-toggle="button" style="float:left;">Balance Payable</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <button type="button" id="both" value="both" class="btn btn-blue-alt" data-toggle="button" style="float:left;">Both</button>&nbsp;&nbsp;
                                            <a href="appointment_invoice.php" class="btn btn-primary" style="float: left;">Go Back</a>
                                            <?php
                                        }
/////////////////////////////////////////////////////////////////////////////
                                        ?>
                                        <?php $seldp = select("*", "tblAppointments", "AppointmentID='" . DecodeQ($_GET['uid']) . "'"); ?>
                                        <a target="_blank" class="btn btn-danger" href="ManageAppointments.php?source=1&bid=<?= EncodeQ($seldp[0]['CustomerID']) ?>&appid=<?= DecodeQ($_GET['uid']); ?>"  title="Book Appointment"><span>Book Future Appointment</span></a>
                                    </div>


                                </div>
                            </div>
                            <?php
                        } else {
                            if (isset($_GET["toandfrom"])) {
                                $strtoandfrom = $_GET["toandfrom"];
                                $arraytofrom = explode("-", $strtoandfrom);
                                $from = $arraytofrom[0];
                                $datetime = new DateTime($from);
                                $getfrom = $datetime->format('Y-m-d');

                                $to = $arraytofrom[1];
                                $datetime = new DateTime($to);
                                $getto = $datetime->format('Y-m-d');
                                if (!IsNull($from)) {
                                    $sqlTempfrom = " and Date(tblAppointments.AppointmentDate)>=Date('" . $getfrom . "')";
                                }
                                if (!IsNull($to)) {
                                    $sqlTempto = " and Date(tblAppointments.AppointmentDate)<=Date('" . $getto . "')";
                                }
                            }
                            if (isset($_GET["toandfrom"])) {
                                $strtoandfrom = $_GET["toandfrom"];
                                $arraytofrom = explode("-", $strtoandfrom);
                                $from = $arraytofrom[0];
                                $datetime = new DateTime($from);
                                $getfrom = $datetime->format('Y-m-d');

                                $to = $arraytofrom[1];
                                $datetime = new DateTime($to);
                                $getto = $datetime->format('Y-m-d');
                                if (!IsNull($from)) {
                                    $sqlTempfrom = " and Date(tblAppointments.AppointmentDate)>=Date('" . $getfrom . "')";
                                }
                                if (!IsNull($to)) {
                                    $sqlTempto = " and Date(tblAppointments.AppointmentDate)<=Date('" . $getto . "')";
                                }
                            }
                            ?>
                            <div class="panel">
                                <div class="panel">
                                    <div class="panel-body">
                                        <div class="example-box-wrapper">
                                            <span class="form_result">&nbsp; <br></span>
                                            <div class="panel-body">
                                                <h3 class="title-hero">List of Invoice | Nailspa</h3>
                                                <form method="get" class="form-horizontal bordered-row" role="form">
                                                    <div class="form-group"><label for="" class="col-sm-4 control-label">Select date</label>
                                                        <div class="col-sm-4">
                                                            <div class="input-prepend input-group">
                                                                <span class="add-on input-group-addon">
                                                                    <i class="glyph-icon icon-calendar"></i>
                                                                </span>
                                                                <input type="text" name="toandfrom" id="daterangepicker-example" class="form-control" value="<?= $strtoandfrom ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                        <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button> &nbsp;&nbsp;&nbsp;
                                                        <a class="btn btn-link" href="appointment_invoice.php">Clear All Filter</a> &nbsp;&nbsp;&nbsp;

                                                    </div>
                                                </form>
                                                <br/>
                                                <?php
                                                if ($_GET["toandfrom"] != "") {
                                                    ?>
                                                    <h3 class="title-hero">Date Range selected : FROM -
                                                        <?= $getfrom ?> / TO -
                                                        <?= $getto ?>
                                                    </h3>
                                                    <br>
                                                    <div class="example-box-wrapper">
                                                        <table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                                                            <thead>
                                                                <tr>
                                                                    <th>Sr.No</th>

                                                                    <th>Invoice Name</th>
                                                                    <th>Appointment Date</th>
                                                                    <th>Store Name</th>
                                                                    <th>Invoice Amount</th>
                                                                    <th>Status</th>
                                                                </tr>
                                                            </thead>
                                                            <tfoot>
                                                                <tr>
                                                                    <th>Sr.No</th>

                                                                    <th>Invoice Name</th>
                                                                    <th>Appointment Date</th>
                                                                    <th>Store Name</th>
                                                                    <th>Invoice Amount</th>
                                                                    <th>Status</th>
                                                                </tr>
                                                            </tfoot>
                                                            <tbody>
                                                                <?php
// Create connection And Write Values
                                                                $DB = Connect();
                                                                $date = date('Y-m-d');

                                                                if (isset($_GET["uids"])) {
                                                                    $asmitaabc = DecodeQ($_GET["uids"]);
                                                                    if ($strAdminRoleID == "36" || $strAdminRoleID == "38") { ////////superadmin
                                                                        $sql = "SELECT * FROM tblAppointments where IsDeleted!='1' and AppointmentID=$asmitaabc $sqlTempfrom $sqlTempto";
                                                                    } elseif ($strAdminRoleID == "39") { ///admin
                                                                        $sql = "SELECT * FROM tblAppointments where IsDeleted!='1' and AppointmentID=$asmitaabc $sqlTempfrom $sqlTempto";
                                                                    } else {
                                                                        $sql = "SELECT * FROM tblAppointments where StoreID='" . $strStore . "' and IsDeleted!='1' and AppointmentID=$asmitaabc $sqlTempfrom $sqlTempto";
                                                                    }
                                                                } else {
                                                                    if ($strAdminRoleID == "36" || $strAdminRoleID == "38") {
                                                                        $sql = "SELECT * FROM tblAppointments where IsDeleted!='1' $sqlTempfrom $sqlTempto order by AppointmentID desc ";
                                                                    } elseif ($strAdminRoleID == "39") {
                                                                        $sql = "SELECT * FROM tblAppointments where IsDeleted!='1' $sqlTempfrom $sqlTempto order by AppointmentID desc ";
                                                                    } else {
                                                                        $sql = "SELECT * FROM tblAppointments where StoreID='" . $strStore . "' and IsDeleted!='1' $sqlTempfrom $sqlTempto order by AppointmentID desc";
                                                                    }
                                                                }
// $sql = "SELECT * FROM tblAppointmentlog order by id desc";
// echo $sql."<br>";
                                                                $RS = $DB->query($sql);
                                                                if ($RS->num_rows > 0) {
                                                                    $counter = 0;
                                                                    while ($row = $RS->fetch_assoc()) {
                                                                        $counter ++;
                                                                        $appointment_id = $row["AppointmentID"];
                                                                        if ($strAdminRoleID == "36" || $strAdminRoleID == "38") {
                                                                            $selda = select("*", "tblAppointments", "AppointmentID='$appointment_id' and IsDeleted!='1' $sqlTempfrom $sqlTempto");
                                                                        } elseif ($strAdminRoleID == "39") {
                                                                            $selda = select("*", "tblAppointments", "AppointmentID='$appointment_id' and IsDeleted!='1' $sqlTempfrom $sqlTempto");
                                                                        } else {
                                                                            $selda = select("*", "tblAppointments", "AppointmentID='$appointment_id' and IsDeleted!='1' and StoreID='" . $strStore . "' $sqlTempfrom $sqlTempto");
                                                                        }
                                                                        $invoice_name = $selda[0]["CustomerID"];
                                                                        $FreeService = $selda[0]["FreeService"];
                                                                        $sada = select("CustomerFullName", "tblCustomers", "CustomerID='" . $invoice_name . "'");
                                                                        $customer = $sada[0]['CustomerFullName'];
                                                                        $getUID = EncodeQ($appointment_id);
                                                                        $appointment_date = $selda[0]["AppointmentDate"];
                                                                        $statusd = $selda[0]["Status"];
                                                                        $store = $selda[0]["StoreID"];
                                                                        $sadad = select("StoreName", "tblStores", "StoreID='" . $store . "'");
                                                                        $storename = $sadad[0]['StoreName'];
                                                                        $status = $selda[0]["Status"];
                                                                        if ($selda[0]["appointment_type"] == '2') {
                                                                            $retail_invoice = select("*", "retailinvoicedetails", "AppointmentId='" . $appointment_id . "'");
                                                                            $invoiceamt = $retail_invoice[0]['RoundTotal'];
                                                                            $amt = number_format($invoiceamt, 2);
                                                                        } else if ($FreeService == '1') {
                                                                            $seldappt = select("*", "tblFreeServices", "AppointmentId='$appointment_id'");
                                                                            $Flagt = $seldappt[0]['Flag'];
                                                                            $invoiceamt = $seldappt[0]['RoundTotal'];
                                                                            $amt = number_format($invoiceamt, 2);
                                                                        } else {
                                                                            $seldapp = select("*", "tblInvoiceDetails", "AppointmentId='$appointment_id'");
                                                                            $invoiceamt = $seldapp[0]['RoundTotal'];
                                                                            $amt = number_format($invoiceamt, 2);
                                                                            $flag = $seldapp[0]['Flag'];
                                                                            $PackageIDFlag = $seldapp[0]['PackageIDFlag'];
                                                                            $seldapptq = select("*", "tblAppointmentPackageValidity", "AppointmentID='$appointment_id'");
                                                                            $ValidTill = $seldapptq[0]['ValidTill'];
                                                                        }
                                                                        ?>
                                                                    <script>
                                                                        function checkstatus(ext, ep) {
                                                                            //alert(111)
                                                                            var uid = $("#uid").val();
                                                                            //alert(ext)
                                                                            //	alert(ep)
                                                                            var number = $("#status").text();
                                                                            //alert(number)
                                                                            if (ext == 'Hold') {
                                                                                //alert(1234)
                                                                                // window.location="ManageAppointments.php";
                                                                                window.location = "appointment_invoice.php?uid=" + ep;
                                                                            } else if (ext == 'Processing') {
                                                                                window.location = "appointment_invoice.php?uid=" + ep;
                                                                            } else if (ext == 'Completed') {
                                                                                //	window.location="appointment_invoice.php?uid=<?php echo $appointment_id ?>";

                                                                            } else {
                                                                            }
                                                                        }
                                                                    </script>
                                                                    <tr id="my_data_tr_<?= $counter ?>">
                                                                        <td style="text-align: center">
                                                                            <?= $counter ?>
                                                                            <input type="hidden" id="uid" value="<?php echo $getUID ?>" />
                                                                        </td>

                                                                        <td style="text-align: center">
                                                                            <?= $customer ?>
                                                                        </td>
                                                                        <td style="text-align: center">
                                                                            <?= $appointment_date ?>
                                                                        </td>
                                                                        <td style="text-align: center">
                                                                            <?= $storename ?>
                                                                        </td>
                                                                        <td style="text-align: center">
                                                                            <?= $amt ?>
                                                                        </td>
                                                                        <td style="text-align: center" id="statusp">
                                                                            <?php if ($row["appointment_type"] == 2) { ?>
                                                                                <a id="status" class="btn btn-link" href="retail_invoice.php?uid=<?= $getUID; ?>">
                                                                                    Retail Invoice
                                                                                </a>
                                                                                <?php
                                                                            } else if ($Flagt == 'Complete') {
                                                                                if ($FreeService == '1') {
                                                                                    $Status = "Completed";
                                                                                    //echo $Status;
                                                                                    ?>
                                                                                    <a id="status" class="btn btn-link" href="CheckInvoices.php?uid=<?= $getUID; ?>">
                                                                                        <?php echo $Status; ?>
                                                                                    </a>
                                                                                    <?php
                                                                                } else {
                                                                                    if ($flag == "H") {
                                                                                        if ($PackageIDFlag == 'P') {
                                                                                            if ($date > $ValidTill) {
                                                                                                //$Status = "Package Service Expired";
                                                                                                $Status = "Completed";
                                                                                                ?>
                                                                                                <?php /* <a id="status" href="#" class="btn btn-link disabled" onClick="checkstatus('Processing', '<?= $getUID; ?>')">
                                                                                                  <?php echo $Status; ?>
                                                                                                  </a> */ ?>
                                                                                                <a id="status" class="btn btn-link" href="CheckInvoices.php?uid=<?= $getUID; ?>">
                                                                                                    <?php echo $Status; ?>
                                                                                                </a>
                                                                                                <?php
                                                                                            } else {
                                                                                                // $Status = "Package Service Pending";
                                                                                                $Status = "Completed";
                                                                                                ?>
                                                                                                <a id="status" class="btn btn-link" href="CheckInvoices.php?uid=<?= $getUID; ?>">
                                                                                                    <?php echo $Status; ?>
                                                                                                </a>
                                                                                                <?php
                                                                                            }
                                                                                        } else {
                                                                                            $Status = "Balance Payable";
                                                                                            ?>
                                                                                            <a id="status" href="#" class="btn btn-link" onClick="checkstatus('Hold', '<?= $getUID; ?>')">
                                                                                                <?php echo $Status; ?>
                                                                                            </a>
                                                                                            <?php
                                                                                        }
                                                                                    } elseif ($flag == "CS") {
                                                                                        if ($PackageIDFlag == 'P') {
                                                                                            if ($date > $ValidTillx) {
                                                                                                //$Status = "Package Service Expired";
                                                                                                $Status = "Completed";
                                                                                                ?>
                                                                                                <a id="status" class="btn btn-link" href="CheckInvoices.php?uid=<?= $getUID; ?>">
                                                                                                    <?php echo $Status; ?>
                                                                                                </a>
                                                                                                <?php
                                                                                            } else {
                                                                                                //$Status = "Package Service Pending";
                                                                                                $Status = "Completed";
                                                                                                ?>
                                                                                                <a id="status" class="btn btn-link" href="CheckInvoices.php?uid=<?= $getUID; ?>">
                                                                                                    <?php echo $Status; ?>
                                                                                                </a>
                                                                                                <?php
                                                                                            }
                                                                                        } else {
                                                                                            $Status = "Completed";
                                                                                            //echo $Status;
                                                                                            ?>
                                                                                            <a id="status" class="btn btn-link" href="CheckInvoices.php?uid=<?= $getUID; ?>">
                                                                                                <?php echo $Status; ?>
                                                                                            </a>
                                                                                            <?php
                                                                                        }
                                                                                    } elseif ($flag == "C") {
                                                                                        if ($PackageIDFlag == 'P') {
                                                                                            if ($date > $ValidTill) {
                                                                                                //$Status = "Package Service Expired";
                                                                                                $Status = "Completed";
                                                                                                ?>
                                                                                                <a id="status" class="btn btn-link" href="CheckInvoices.php?uid=<?= $getUID; ?>">
                                                                                                    <?php echo $Status; ?>
                                                                                                </a>
                                                                                                <?php
                                                                                            } else {
                                                                                                //$Status = "Package Service Pending";
                                                                                                $Status = "Completed";
                                                                                                ?>
                                                                                                <a id="status" class="btn btn-link" href="CheckInvoices.php?uid=<?= $getUID; ?>">
                                                                                                    <?php echo $Status; ?>
                                                                                                </a>
                                                                                                <?php
                                                                                            }
                                                                                        } else {
                                                                                            $Status = "Completed";
                                                                                            //echo $Status;
                                                                                            ?>
                                                                                            <a id="status" class="btn btn-link" href="CheckInvoices.php?uid=<?= $getUID; ?>">
                                                                                                <?php echo $Status; ?>
                                                                                            </a>
                                                                                            <?php
                                                                                        }
                                                                                    } elseif ($flag == "BOTH") {
                                                                                        if ($PackageIDFlag == 'P') {
                                                                                            if ($date > $ValidTill) {
                                                                                                $Status = "Completed";
                                                                                                ?>
                                                                                                <a id="status" class="btn btn-link" href="CheckInvoices.php?uid=<?= $getUID; ?>">
                                                                                                    <?php echo $Status; ?>
                                                                                                </a
                                                                                                <?php
                                                                                            } else {
                                                                                                $Status = "Package Service Pending";
                                                                                                ?>
                                                                                                <a id="status" href="#" class="btn btn-link" onClick="checkstatus('Processing', '<?= $getUID; ?>')">
                                                                                                    <?php echo $Status; ?>
                                                                                                </a>
                                                                                                <?php
                                                                                            }
                                                                                        } else {
                                                                                            $Status = "Both";
                                                                                            //echo $Status;
                                                                                            ?>
                                                                                            <a id="status" class="btn btn-link" href="CheckInvoices.php?uid=<?= $getUID; ?>">
                                                                                                <?php echo $Status; ?>
                                                                                            </a>
                                                                                            <?php
                                                                                        }
                                                                                    } elseif ($flag == "") {
                                                                                        //echo $flag;
                                                                                        if ($statusd == '1') {
                                                                                            $Status = "Processing";
                                                                                            //echo $Status;
                                                                                            ?>
                                                                                            <a id="status" class="btn btn-link" href="#" onClick="checkstatus('Processing', '<?= $getUID; ?>')">
                                                                                                <?php echo $Status; ?>
                                                                                            </a>
                                                                                            <?php
                                                                                        } elseif ($statusd == '0') {
                                                                                            $Status = "Need To Checkin First";
                                                                                            //echo $Status;
                                                                                            ?>
                                                                                            <a id="status" class="btn btn-link disabled" href="#" onClick="checkstatus('Processing', '<?= $getUID; ?>')">
                                                                                                <?php echo $Status; ?>
                                                                                            </a>
                                                                                            <?php
                                                                                        } elseif ($statusd == '3') {
                                                                                            $Status = "Cancel";
                                                                                            //echo $Status;
                                                                                            ?>
                                                                                            <a id="status" class="btn btn-link disabled" href="#" onClick="checkstatus('Processing', '<?= $getUID; ?>')">
                                                                                                <?php echo $Status; ?>
                                                                                            </a>
                                                                                            <?php
                                                                                        } else {
                                                                                            $Status = "Immediate Checkout";
                                                                                            //echo $Status;
                                                                                            ?>
                                                                                            <a id="status" class="btn btn-link disabled" href="#" onClick="checkstatus('Processing', '<?= $getUID; ?>')">
                                                                                                <?php echo $Status; ?>
                                                                                            </a>
                                                                                            <?php
                                                                                        }
                                                                                    }
                                                                                }
                                                                            } else {

                                                                                if ($flag == "H") {
                                                                                    if ($PackageIDFlag == 'P') {
                                                                                        if ($date > $ValidTill) {
                                                                                            //$Status = "Package Service Expired";
                                                                                            $Status = "Completed";
                                                                                            ?>
                                                                                            <a id="status" class="btn btn-link" href="CheckInvoices.php?uid=<?= $getUID; ?>">
                                                                                                <?php echo $Status; ?>
                                                                                            </a>
                                                                                            <?php
                                                                                        } else {
                                                                                            //$Status = "Package Service Pending";
                                                                                            $Status = "Completed";
                                                                                            ?>
                                                                                            <a id="status" class="btn btn-link" href="CheckInvoices.php?uid=<?= $getUID; ?>">
                                                                                                <?php echo $Status; ?>
                                                                                            </a>
                                                                                            <?php
                                                                                        }
                                                                                    } else {
                                                                                        $Status = "Balance Payable";
                                                                                        ?>
                                                                                        <a id="status" href="#" class="btn btn-link" onClick="checkstatus('Hold', '<?= $getUID; ?>')">
                                                                                            <?php echo $Status; ?>
                                                                                        </a>
                                                                                        <?php
                                                                                    }
                                                                                } elseif ($flag == "CS") {
                                                                                    if ($PackageIDFlag == 'P') {
                                                                                        //echo $date;
                                                                                        //echo $ValidTill;
                                                                                        if ($date > $ValidTill) {
                                                                                            //$Status = "Package Service Expired";
                                                                                            $Status = "Completed";
                                                                                            ?>
                                                                                            <a id="status" class="btn btn-link" href="CheckInvoices.php?uid=<?= $getUID; ?>">
                                                                                                <?php echo $Status; ?>
                                                                                            </a>
                                                                                            <?php
                                                                                        } else {
                                                                                            $Status = "Completed";
                                                                                            ?>
                                                                                            <a id="status" class="btn btn-link" href="CheckInvoices.php?uid=<?= $getUID; ?>">
                                                                                                <?php echo $Status; ?>
                                                                                            </a>
                                                                                            <?php
                                                                                        }
                                                                                    } else {
                                                                                        $Status = "Completed";
                                                                                        //echo $Status;
                                                                                        ?>
                                                                                        <a id="status" class="btn btn-link" href="CheckInvoices.php?uid=<?= $getUID; ?>">
                                                                                            <?php echo $Status; ?>
                                                                                        </a>
                                                                                        <?php
                                                                                    }
                                                                                } elseif ($flag == "C") {
                                                                                    if ($PackageIDFlag == 'P') {
                                                                                        if ($date > $ValidTill) {
                                                                                            //$Status = "Package Service Expired";
                                                                                            $Status = "Completed";
                                                                                            ?>
                                                                                            <a id="status" class="btn btn-link" href="CheckInvoices.php?uid=<?= $getUID; ?>">
                                                                                                <?php echo $Status; ?>
                                                                                            </a>
                                                                                            <?php
                                                                                        } else {
                                                                                            // $Status = "Package Service Pending";
                                                                                            $Status = "Completed";
                                                                                            ?>
                                                                                            <a id="status" class="btn btn-link" href="CheckInvoices.php?uid=<?= $getUID; ?>">
                                                                                                <?php echo $Status; ?>
                                                                                            </a>
                                                                                            <?php
                                                                                        }
                                                                                    } else {
                                                                                        $Status = "Completed";
                                                                                        //echo $Status;
                                                                                        ?>
                                                                                        <a id="status" class="btn btn-link" href="CheckInvoices.php?uid=<?= $getUID; ?>">
                                                                                            <?php echo $Status; ?>
                                                                                        </a>
                                                                                        <?php
                                                                                    }
                                                                                } elseif ($flag == "BOTH") {
                                                                                    if ($PackageIDFlag == 'P') {
                                                                                        if ($date > $ValidTill) {
                                                                                            //$Status = "Package Service Expired";
                                                                                            $Status = "Completed";
                                                                                            ?>
                                                                                            <a id="status" class="btn btn-link" href="CheckInvoices.php?uid=<?= $getUID; ?>">
                                                                                                <?php echo $Status; ?>
                                                                                            </a>
                                                                                            <?php
                                                                                        } else {
                                                                                            //$Status = "Package Service Pending";
                                                                                            $Status = "Completed";
                                                                                            ?>
                                                                                            <a id="status" class="btn btn-link" href="CheckInvoices.php?uid=<?= $getUID; ?>">
                                                                                                <?php echo $Status; ?>
                                                                                            </a>
                                                                                            <?php
                                                                                        }
                                                                                    } else {
                                                                                        $Status = "Both";
                                                                                        //echo $Status;
                                                                                        ?>
                                                                                        <a id="status" class="btn btn-link" href="CheckInvoices.php?uid=<?= $getUID; ?>">
                                                                                            <?php echo $Status; ?>
                                                                                        </a>
                                                                                        <?php
                                                                                    }
                                                                                } elseif ($flag == "") {
                                                                                    if ($statusd == '1') {
                                                                                        $Status = "Processing";
                                                                                        //echo $Status;
                                                                                        ?>
                                                                                        <a id="status" class="btn btn-link" href="#" onClick="checkstatus('Processing', '<?= $getUID; ?>')">
                                                                                            <?php echo $Status; ?>
                                                                                        </a>
                                                                                        <?php
                                                                                    } elseif ($statusd == '0') {
                                                                                        $Status = "Need To Checkin First";
                                                                                        //echo $Status;
                                                                                        ?>
                                                                                        <a id="status" class="btn btn-link disabled" href="#" onClick="checkstatus('Processing', '<?= $getUID; ?>')">
                                                                                            <?php echo $Status; ?>
                                                                                        </a>
                                                                                        <?php
                                                                                    } elseif ($statusd == '3') {
                                                                                        $Status = "Cancel";
                                                                                        //echo $Status;
                                                                                        ?>
                                                                                        <a id="status" class="btn btn-link disabled" href="#" onClick="checkstatus('Processing', '<?= $getUID; ?>')">
                                                                                            <?php echo $Status; ?>
                                                                                        </a>
                                                                                        <?php
                                                                                    } else {
                                                                                        $Status = "Immediate Checkout";
                                                                                        //echo $Status;
                                                                                        ?>
                                                                                        <a id="status" class="btn btn-link disabled" href="#" onClick="checkstatus('Processing', '<?= $getUID; ?>')">
                                                                                            <?php echo $Status; ?>
                                                                                        </a>
                                                                                        <?php
                                                                                    }
                                                                                }
                                                                            }

                                                                            //echo $Status;
                                                                            ?>
                                                                        </td>

                                                                    </tr>
                                                                    <?php
                                                                }
                                                            } else {
                                                                ?>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td>No Records Found</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                            $DB->close();
                                                            ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <?php
                                                } else {
                                                    echo "<br><center><h3>Please select dates!</h3></center>";
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?php require_once 'incFooter.fya'; ?>
    </body>
</html>