<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Manage Appointment Source | Nailspa";
$strDisplayTitle = "Source for Nailspa";
$strMenuID = "3";
$strMyActionPage = "appointment_source.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>


<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <!-----------css & js files added for tabs by gandhali 3/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");      ?>
            <!----------commented by gandhali 3/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>
                        <?php
                        $app_source = select("*", "tblSource", "status = 1");
                        if (isset($app_source) && is_array($app_source) && count($app_source) > 0) {
                            ?>

                            <div class="panel">
                                <div class="panel">
                                    <div class="panel-body">

                                        <?php
                                        if ($_SERVER["REQUEST_METHOD"] == "POST") {
                                            $DB = Connect();

                                            $strStep = Filter($_POST["mode"]);

                                            if ($strStep == "add") {
                                                $source_name = isset($_POST['source_name']) ? trim($_POST['source_name']) : '';
                                                if ($source_name != '') {
                                                    /*
                                                     * Check if source already exist or not
                                                     */
                                                    $source_exist = select("*", "tblSource", "name = '" . trim($source_name) . "'");
                                                    if (isset($source_exist) && is_array($source_exist) && count($source_exist) > 0) {
                                                        echo('<div class="alert alert-close alert-danger">
					<div class="bg-red alert-icon"><i class="glyph-icon icon-times"></i></div>
					<div class="alert-content">
						<h4 class="alert-title">Record Add Failed</h4>
						<p>Source name already exists in our system. Please try again with a different source name.</p>
					</div>
				</div>');
                                                        echo("<script>location.href='appointment_source.php';</script>");
                                                    } else {
                                                        $InsertAdmin = "Insert into tblSource(name, created_by, created_date) VALUES ('" . $source_name . "','" . $strAdminID . "','" . date('Y-m-d H:i:s') . "')";
                                                        $DB->query($InsertAdmin);
                                                        echo('<div class="alert alert-success alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                            <strong>Source Added Successfully.</strong>
                            </div>');
                                                        echo("<script>location.href='appointment_source.php';</script>");
                                                    }
                                                }
                                            } else if ($strStep == "edit") {
                                                $source_id = isset($_POST['source_id']) ? $_POST['source_id'] : '';
                                                $source_name = isset($_POST['source_name']) ? $_POST['source_name'] : '';
                                                if ($source_name != '' && $source_id != '') {
                                                    $udpateq = "UPDATE tblSource SET name='" . $source_name . "',modified_date='" . date('Y-m-d H:i:s') . "',modified_by='" . $strAdminID . "'"
                                                            . " WHERE id='" . $source_id . "'";
                                                    $DB->query($udpateq);
                                                    echo('<div class="alert alert-success alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                            <strong>Source Updated Successfully.</strong>
                            </div>');
                                                    echo("<script>location.href='appointment_source.php';</script>");
                                                }
                                            }
                                            $DB->close();
                                        }
                                        ?>

                                        <div class="example-box-wrapper">
                                            <?php
                                            if (isset($_GET['source_id'])) {
                                                $source_edit_data = select("*", "tblSource", "Status=1 AND id='" . $_GET['source_id'] . "'");
                                                ?>

                                                <div class="fa-hover">	
                                                    <a class="btn btn-primary btn-lg btn-block" href="appointment_source.php"><i class="fa fa-backward"></i> &nbsp; Go back to Source management</a>
                                                </div>

                                                <br><br>
                                                <h3 class="title-hero">Edit Source Details</h3>

                                                <form role="form" enctype="multipart/form-data" class="form-horizontal bordered-row source_form" action="appointment_source.php" method="post">
                                                    <input type="hidden" value="<?php echo $_GET['source_id']; ?>" name="source_id"/>
                                                    <input type="hidden" value="edit" name="mode"/>
                                                    <div class="form-group"><label class="col-sm-3 control-label">Source Name <span>*</span></label>
                                                        <div class="col-sm-3"><input type="text" name="source_name" class="form-control admin_password required" placeholder="Source Name" required="required" value="<?php echo isset($source_edit_data[0]['name']) ? $source_edit_data[0]['name'] : ''; ?>"></div>
                                                    </div>

                                                    <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                        <input type="submit" class="btn ra-100 btn-primary" value="Update">
                                                        <div class="col-sm-1"><a class="btn ra-100 btn-black-opacity" href="javascript:;" onclick="ClearInfo('source_form');" title="Clear"><span>Clear</span></a></div>
                                                    </div>
                                                </form>

                                            <?php } else { ?>

                                                <div class="tabs">
                                                    <ul>
                                                        <li><a href="#normal-tabs-1" title="Tab 1">Manage</a></li>
                                                        <li><a href="#normal-tabs-2" title="Tab 2">Add</a></li>
                                                    </ul>
                                                    <div id="normal-tabs-1">

                                                        <span class="form_result">&nbsp; <br>
                                                        </span>

                                                        <div class="panel-body">
                                                            <div class="example-box-wrapper">
                                                                <table id="datatable-responsive-date" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Sr. No.</th>
                                                                            <th>Name</th>
                                                                            <th>Created date</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tfoot>
                                                                        <tr>
                                                                            <th>Sr. No.</th>
                                                                            <th>Name</th>
                                                                            <th>Created date</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                    </tfoot>
                                                                    <tbody>
                                                                        <?php
                                                                        $cnt = 1;
                                                                        foreach ($app_source as $skey => $svalue) {
                                                                            ?>
                                                                            <tr>
                                                                                <td><?php echo $cnt; ?></td>
                                                                                <td><?php echo $svalue['name']; ?></td>
                                                                                <td><?php echo date('d M,Y h:i a', strtotime($svalue['created_date'])); ?></td>
                                                                                <td>
                                                                                    <a class="btn btn-link" href="appointment_source.php?source_id=<?= $svalue['id']; ?>">Edit</a>
                                                                                    <a class="btn btn-link font-red" font-redhref="javascript:;" onclick="DeleteData('delete_source', '<?= $svalue['id'] ?>', 'Are you sure you want to delete this Source - <?= $svalue['name'] ?>?', 'my_data_tr_<?= $cnt ?>');">Delete</a>
                                                                                </td>
                                                                            </tr>
                                                                            <?php
                                                                            $cnt++;
                                                                        }
                                                                        ?>


                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="normal-tabs-2">
                                                        <div class="panel-body">
                                                            <form role="form" enctype="multipart/form-data" class="form-horizontal bordered-row source_form" action="appointment_source.php" method="post">


                                                                <input type="hidden" value="add" name="mode"/>
                                                                <div class="form-group"><label class="col-sm-3 control-label">Source Name <span>*</span></label>
                                                                    <div class="col-sm-3"><input type="text" name="source_name" class="form-control admin_password required" placeholder="Source Name" required="required"></div>
                                                                </div>

                                                                <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                                    <input type="submit" class="btn ra-100 btn-primary" value="Submit">

                                                                    <div class="col-sm-1"><a class="btn ra-100 btn-black-opacity" href="javascript:;" onclick="ClearInfo('source_form');" title="Clear"><span>Clear</span></a></div>
                                                                </div>

                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }
                        ?>

                    </div>
                </div>
            </div>
            <?php require_once 'incFooter.fya'; ?>
        </div>
    </body>
</html>