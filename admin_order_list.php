<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Manage Order Request | Nailspa";
$strDisplayTitle = "List Of Order Request";
$strMenuID = "3";
$strMyActionPage = "manage_order_list.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <!-----------css & js files added for tabs by gandhali 3/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>
        <?php /* <script type="text/javascript" src="assets/widgets/modal/modal.js"></script> */ ?>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");     ?>
            <!----------commented by gandhali 3/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>

                        <script type="text/javascript">
                            $(document).ready(function () {
                                $('#datatable-responsive-id').DataTable({
                                    responsive: true,
                                    "order": [[0, "desc"]],
                                });
                            });
                        </script>
                        <script>
                            function ApproveOrder(DataID, ParamMessage, ApproveStatus)
                            {
                                var validatenow = confirm(ParamMessage);
                                if (validatenow == true)
                                {
                                    $.ajax({
                                        type: 'POST',
                                        url: '<?= FindHostAdmin() ?>/ApproveOrder.php',
                                        data: {
                                            order_id: DataID,
                                            status: ApproveStatus
                                        },
                                        success: function (response)
                                        {
                                            if (response == 'success') {
                                                alert('Done Successfully.');
                                                window.location.reload(true);
                                            } else if (response == 'fail') {
                                                alert('Failed');
                                            } else {
                                                alert(response);
                                            }
                                        }
                                    });
                                }
                            }
                        </script>
                        <div class="panel">
                            <div class="panel-body">

                                <div class="panel-body">
                                    <h3 class="title-hero"><?php echo $strDisplayTitle; ?></h3>

                                    <span class="form_result">&nbsp; <br></span>

                                    <table id="datatable-responsive-id" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sr. No.</th>
                                                <th>Name</th>
                                                <th>Approve Status</th>
                                                <th>Created Date</th>
                                                <th>Created By</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Sr. No.</th>
                                                <th>Name</th>
                                                <th>Approve Status</th>
                                                <th>Created Date</th>
                                                <th>Created By</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <?php
                                            $DB = Connect();
                                            /*
                                             * get all users
                                             */
                                            $userq = "SELECT * FROM tblAdmin";
                                            $userq_exe = $DB->query($userq);
                                            if ($userq_exe->num_rows > 0) {
                                                while ($user_row = $userq_exe->fetch_assoc()) {
                                                    $user_data[$user_row['AdminID']] = $user_row;
                                                }
                                            }


                                            $order_data = select("*", "order_request", "status =1 ORDER BY id desc");
                                            $DB->close();

                                            if (isset($order_data) && is_array($order_data) && count($order_data) > 0) {
                                                foreach ($order_data as $key => $value) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $value['id']; ?></td>
                                                        <td><?php echo $value['order_name']; ?></td>
                                                        <td>
                                                            <?php if ($value['approve_status'] == 1) { ?>
                                                                <span style="color:#227dca;">In Queue</span>
                                                                <?php
                                                            } else if ($value['approve_status'] == 2) {
                                                                if ($value['is_verified'] == 1) {
                                                                    ?>
                                                                    <span class="text-success">Approved & Verified</span>
                                                                <?php } else { ?>
                                                                    <span class="text-success">Approved</span>
                                                                <?php } ?>
                                                            <?php } else { ?>
                                                                <span class="text-danger">Rejected</span>
                                                            <?php } ?>
                                                        </td>
                                                        <td><?php echo date('d/m/Y h:i a', strtotime($value['created_date'])); ?></td>
                                                        <td><?php echo isset($user_data[$value['created_by']]) ? $user_data[$value['created_by']]['AdminFullName'] : ''; ?></td>
                                                        <td>
                                                            <a class="btn btn-xs btn-primary" href="view_order_detail.php?id=<?php echo $value['id']; ?>">View Order</a>
                                                            <?php if ($value['approve_status'] == 1) { ?>
                                                                <a id="my_data_tr_<?= $counter ?>_button" class="btn btn-xs btn-warning" href="javascript:;" onclick="ApproveOrder('<?= $value['id']; ?>', 'Are you sure you want to Approve this Order?', '2');">Approve</a>
                                                                <a id="my_data_tr_<?= $counter ?>_button" class="btn btn-xs btn-danger" href="javascript:;" onclick="ApproveOrder('<?= $value['id']; ?>', 'Are you sure you want to Reject this Order?', '3');">Reject</a>
                                                            <?php } else if ($value['approve_status'] == 2) { ?>
                                                                <a class="btn btn-xs btn-info" href="create_order_pdf.php?id=<?php echo $value['id']; ?>">Download PDF</a>
                                                                <a class="btn btn-xs btn-info" href="order_export.php?id=<?php echo $value['id']; ?>">Download CSV</a>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <?php require_once 'incFooter.fya'; ?>
        </div>

    </body>
</html>