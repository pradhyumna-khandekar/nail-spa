<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
	$strPageTitle = "Manage Campaign Analysis | Nailspa";
	$strDisplayTitle = "Manage Campaign Analysis for Nailspa";
	$strMenuID = "10";
	$strMyTable = "tblMembership";
	$strMyTableID = "MembershipID";
	$strMyField = "MembershipName";
	$strMyActionPage = "ManageCampaignAnalysis.php";
	$strMessage = "";
	$sqlColumn = "";
	$sqlColumnValues = "";
	
// code for not allowing the normal admin to access the super admin rights	
	if($strAdminType!="0")
	{
		die("Sorry you are trying to enter Unauthorized access");
	}
// code for not allowing the normal admin to access the super admin rights	


	
	if ($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$strStep = Filter($_POST["step"]);
		if($strStep=="add")
		{
			foreach($_POST as $key => $val)
			{
				if($key!="step")
				{
					if(IsNull($sqlColumn))
					{
						$sqlColumn = $key;
						$sqlColumnValues = "'".$_POST[$key]."'";
					}
					else
					{
						$sqlColumn = $sqlColumn.",".$key;
						$sqlColumnValues = $sqlColumnValues.", '".$_POST[$key]."'";
					}
				}
	
			}
			
			// foreach($_POST as $key => $val)
			// {
				// echo $key ." = ".$_POST[$key];
			// }
			// die();
			
			$Name = Filter($_POST["Name"]);
			$Descriprtion = Filter($_POST["Descriprtion"]);
			$Status = Filter($_POST["Status"]);
			
			$DB = Connect();
			$sql = "SELECT CampaignAnalysisID FROM tblCampaignAnalysis WHERE Name='$Name'";
			$RS = $DB->query($sql);
			if ($RS->num_rows > 0) 
			{
				$DB->close();
				die('<div class="alert alert-close alert-danger">
					<div class="bg-red alert-icon"><i class="glyph-icon icon-times"></i></div>
					<div class="alert-content">
						<h4 class="alert-title">Record Add Failed</h4>
						<p>Campaign with the same Name already exists. Please try again with a different Name.</p>
					</div>
				</div>');
			}
			else
			{
				 $sqlInsert = "INSERT INTO tblCampaignAnalysis (Name, Descriprtion, Status) VALUES 
				('".$Name."', '".$Descriprtion."', '".$Status."')";
				ExecuteNQ($sqlInsert);
				//echo $sqlInsert;
				// die();
				
				$DB->close();
				die('<div class="alert alert-close alert-success">
					<div class="bg-green alert-icon"><i class="glyph-icon icon-check"></i></div>
					<div class="alert-content">
						<h4 class="alert-title">Record Added Successfully.</h4>
						
					</div>
				</div>');
			}
		}

		if($strStep=="edit")
		{
			$DB = Connect();
			
			$Name = Filter($_POST["Name"]);
			$Descriprtion = Filter($_POST["Descriprtion"]);
			$Status = Filter($_POST["Status"]);
			$strMyTableID=Filter($_POST["CampaignAnalysisID"]);
			////////////////////////////////////////////////////////////////////////////////
		
					$sqlUpdate = "UPDATE tblCampaignAnalysis SET Name='$Name',Descriprtion='$Descriprtion',Status='$Status' WHERE CampaignAnalysisID='".Decode($strMyTableID)."'";
					ExecuteNQ($sqlUpdate);
					//echo $sqlUpdate;
				
			$DB->close();
			die('<div class="alert alert-close alert-success">
					<div class="bg-green alert-icon"><i class="glyph-icon icon-check"></i></div>
					<div class="alert-content">
						<h4 class="alert-title">Record Updated Successfully</h4>
					</div>
				</div>');
		}
		die();
	}	
	
	if(isset($_GET['renew']))
	{
			$DB = Connect();
		$ID=$_GET['renew'];
		$date=date('Y-m-d');
		$new_date = strtotime(date("Y-m-d", strtotime($date)) . " +12 month");
		$new_dated = date("Y-m-d",$new_date);
        $sell=select("*","tblMembership","MembershipID='".$ID."'");
		$endate=$sell[0]['TimeForDiscountEnd'];
		$sql = "SELECT * FROM tblCustomers WHERE memberid='".$ID."'";
		$RS = $DB->query($sql);
		if ($RS->num_rows > 0) 
		{
			$counter = 0;

			while($row = $RS->fetch_assoc())
			{
				
				$CustomerEmailID=$row['CustomerEmailID'];
						$CustomerFullName=$row['CustomerFullName'];
						$CustomerMobileNo=$row['CustomerMobileNo'];
						
						
						$strTo = $CustomerEmailID;
				$strFrom = "order@fyatest.website";
				$strSubject = "Your Membership Renew Successfully of Nailspa services";
				$strBody = "";
				$strStatus = "0"; // Pending = 0 / Sent = 1 / Error = 2
				$Name = $CustomerFullName;
				$strDate = $endate;
					
				$seldata=select("*","tblMembership","MembershipID='".$ID."'");
				$MembershipName=$seldata[0]['MembershipName'];
				$Type=$seldata[0]['Type'];
				if($Type=='0')
				{
					$type='Elite Membership';
				}
				else
				{
					$type='Normal Membership';
				}
				$DiscountType=$seldata[0]['DiscountType'];
				$Discount=$seldata[0]['Discount'];
				$TimeForDiscountEnd=$seldata[0]['TimeForDiscountEnd'];
				$storeid=$seldata[0]['storeid'];
				$sep=select("*","tblStores","StoreID='".$storeid."'");
				$officeaddress=$sep[0]['StoreOfficialAddress'];
				$storename=$sep[0]['StoreName'];
			    $branche=explode(",",$storename);
				$branchname=$branche[1]; 
		
				$message = file_get_contents('EmailFormat/membership_Renew.html');
				$message = eregi_replace("[\]",'',$message);
				//setup vars to replace
				$vars = array('{membership_name}','{member_name}','{Discount}','{TimeForDiscountEnd}','{Address}','{Branch}','{Renewdate}'); //Replace varaibles
				$values = array($MembershipName,$Name,$Discount,$TimeForDiscountEnd,$officeaddress,$branchname,$new_dated);

				//replace vars
				$message = str_replace($vars,$values,$message);

				$strBody1 = $message;
				
				// exit();
				 
				$flag='CM';
				$id = $last_id2;
				
				sendmail($id,$strTo,$strFrom,$strSubject,$strBody1,$strDate,$flag,$strStatus);
			}
		}
	
					
		$sqlUpdate = "UPDATE $strMyTable SET TimeForDiscountEnd='".$new_dated."' WHERE $strMyTableID='".$ID."'";
					ExecuteNQ($sqlUpdate);
					
					$msg="Membership Renew Successfully";
					$DB->close();
					header('Location:ManageMemberships.php?msg='.$msg.'');
	}
?>


<!DOCTYPE html>
<html lang="en">

<head>
	<?php require_once("incMetaScript.fya"); ?>
	<script>
						$(function ()						
						{
							$("#StartDay").datepicker({ minDate: 0 });
							$("#EndDay").datepicker({ minDate: 0 });
							$("#StartDay1").datepicker({ minDate: 0 });
							$("#EndtDay1").datepicker({ minDate: 0 });
							
						});
					</script>
</head>

<body>
    <div id="sb-site">
        
		<?php require_once("incOpenLayout.fya"); ?>
        <?php require_once("incLoader.fya"); ?>
		
        <div id="page-wrapper">
            <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>
            
				<?php require_once("incLeftMenu.fya"); ?>
			
            <div id="page-content-wrapper">
                <div id="page-content">
                    
					<?php require_once("incHeader.fya"); ?>
					

                    <div id="page-title">
                        <h2><?=$strDisplayTitle?></h2>
                        <p>Add, Edit, Delete Memberships</p>
                    </div>
<?php

if(!isset($_GET["uid"]))
{

?>					
					
                    <div class="panel">
						<div class="panel">
							<div class="panel-body">
								
								<div class="example-box-wrapper">
									<div class="tabs">
										<ul>
											<li><a href="#normal-tabs-1" title="Tab 1">Manage</a></li>
											<li><a href="#normal-tabs-2" title="Tab 2">Add</a></li>
										</ul>
											<div id="normal-tabs-1">
												<span class="form_result">&nbsp; <br>
											</span>
											
											<div class="panel-body">
												<h3 class="title-hero">List of Campaign | Nailspa</h3>
												<div class="example-box-wrapper">
													<table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
														<thead>
															<tr>
																<th>Sr.No</th>
																<th>Campaign Analysis</th>
																<th>Description</th>
																<th>Action</th>
															</tr>
														</thead>
														<tfoot>
															<tr>
																<th>Sr.No</th>
																<th>Campaign Analysis</th>
																<th>Description</th>
																<th>Action</th>
															</tr>
														</tfoot>
														<tbody>

<?php
// Create connection And Write Values
$DB = Connect();
$sql = "SELECT * FROM tblCampaignAnalysis WHERE Status='0'";
// echo $sql."<br>";
$RS = $DB->query($sql);
if ($RS->num_rows > 0) 
{
	$counter = 0;

	while($row = $RS->fetch_assoc())
	{
		$counter ++;
		$CampaignAnalysisID = $row["CampaignAnalysisID"];
		$getUID = EncodeQ($CampaignAnalysisID);
		$getUIDDelete = Encode($CampaignAnalysisID);		
		$Name = $row["Name"];
	
		$Descriprtion = $row["Descriprtion"];
		
		$Status = $row["Status"];
		
		if($Status=="0")
		{
			$Status = "Live";
		}
		else
		{
			$Status = "Offline";
		}
?>	
															<tr id="my_data_tr_<?=$counter?>">
																<td><?=$counter?></td>
																<td><?=$Name?></td>
																
																<td>
																<?=$Descriprtion?>
																</td>
															
																
																<td style="text-align: center">
																	<a class="btn btn-link" href="<?=$strMyActionPage?>?uid=<?=$getUID?>">Edit</a>
																		<?php
																	if($strAdminRoleID=="36")
                                                                    {
																		
																		?>
																	<a class="btn btn-link font-red" font-redhref="javascript:;" onclick="DeleteData('Step36','<?=$getUIDDelete?>', 'Are you sure you want to delete - <?=$AdminFullName?>?','my_data_tr_<?=$counter?>');">Delete</a>
																	<?php 
																	}
																	?>
																	
																	<br>
																	
																</td>
															</tr>
<?php
	}
}
else
{
?>															
															<tr>
																<td></td>
																<td></td>
																<td></td>
																<td>No Records Found</td>
																

																
															</tr>
														
<?php
}
$DB->close();
?>
														
														</tbody>
													</table>
												</div>
											</div>
										</div>
										
										<div id="normal-tabs-2">
											<div class="panel-body">
												<form role="form" class="form-horizontal bordered-row enquiry_form" onSubmit="proceed_formsubmit('.enquiry_form', '<?=$strMyActionPage?>', '.result_message', '', '', ''); return false;">
											
											<span class="result_message">&nbsp; <br>
											</span>
											<input type="hidden" name="step" value="add">

											
												<h3 class="title-hero">Add Campaign</h3>
												<div class="example-box-wrapper">
													
<?php
// Create connection And Write Values
$DB = Connect();
$sql = "SHOW COLUMNS FROM tblCampaignAnalysis";
$RS = $DB->query($sql);
if ($RS->num_rows > 0) 
{

	while($row = $RS->fetch_assoc())
	{
		if($row["Field"]=="CampaignAnalysisID")
		{
		}
		else if ($row["Field"]=="Name")
		{
?>	
													<div class="form-group"><label class="col-sm-3 control-label"><?=str_replace("Name", "Name", $row["Field"])?> <span>*</span></label>
															<div class="col-sm-4"><input type="text" name="<?=$row["Field"]?>" id="<?=str_replace("Name", "Name", $row["Field"])?>" class="form-control required" placeholder="<?=str_replace("Name", "Name", $row["Field"])?>"></div>
													</div>
<?php
		}
	else if ($row["Field"]=="Status")
		{
?>
													<div class="form-group"><label class="col-sm-3 control-label"><?=str_replace("Admin", "Status", $row["Field"])?> <span>*</span></label>
														<div class="col-sm-4">
															<select name="<?=$row["Field"]?>" class="form-control required">
																<option value="0" Selected>Live</option>
																<option value="1">Offline</option>	
															</select>
														</div>
													</div>
<?php	
		}
		else if ($row["Field"]=="Descriprtion")
		{
?>
													<div class="form-group"><label class="col-sm-3 control-label"><?=str_replace("Descriprtion", "Descriprtion", $row["Field"])?> <span>*</span></label>
															<div class="col-sm-4"><textarea  rows="4" cols="150" name="<?=$row["Field"]?>" id="<?=str_replace("Descriprtion", "Descriprtion", $row["Field"])?>" class="form-control  wysiwyg" placeholder="<?=str_replace("Descriprtion", "Descriprtion", $row["Field"])?>"></textarea></div>
														</div>	
<?php	
		}
		else
		{
?>
														<div class="form-group"><label class="col-sm-3 control-label"><?=str_replace("Admin", " ", $row["Field"])?> <span>*</span></label>
															<div class="col-sm-4"><input type="text" name="<?=$row["Field"]?>" id="<?=str_replace("Admin", " ", $row["Field"])?>" class="form-control required" placeholder="<?=str_replace("Admin", " ", $row["Field"])?>"></div>
														</div>
<?php
		}
	}
?>
														<div class="form-group">
															<label class="col-sm-3 control-label"></label>
															<input type="submit" class="btn ra-100 btn-primary" value="Submit">
															<div class="col-sm-2">
																<a class="btn ra-100 btn-black-opacity" href="javascript:;" onclick="ClearInfo('enquiry_form');" title="Clear"><span>Clear</span></a>
															</div>
														</div>
<?php
}
$DB->close();
?>													
												</div>
												</form>
											</div>
											
											
											
										</div>
										
									</div>
								</div>
							</div>
						</div>
                    </div>
<?php
} // End null condition


//-----------------Normal Edit

else
{
?>						
					
					<div class="panel">
						<div class="panel-body">
							<div class="fa-hover">	
								<a class="btn btn-primary btn-lg btn-block" href="<?=$strMyActionPage?>"><i class="fa fa-backward"></i> &nbsp; Go back to <?=$strPageTitle?></a>
							</div>
						
							<div class="panel-body">
							<form role="form" class="form-horizontal bordered-row enquiry_form" onSubmit="proceed_formsubmit('.enquiry_form', '<?=$strMyActionPage?>', '.result_message', '', '.admin_email', '.admin_password'); return false;">
											
								<span class="result_message">&nbsp; <br>
								</span>
								<br>
								<input type="hidden" name="step" value="edit">

								
									<h3 class="title-hero">Edit Campaign</h3>
									<div class="example-box-wrapper">
										
<?php
$strID = DecodeQ(Filter($_GET["uid"]));
$DB = Connect();
$sql = "SELECT * FROM tblCampaignAnalysis WHERE CampaignAnalysisID = '$strID'";
$RS = $DB->query($sql);
if ($RS->num_rows > 0) 
{
	while($row = $RS->fetch_assoc())
	{
		foreach($row as $key => $val)
		{
			if($key=="CampaignAnalysisID")
			{
?>
											<input type="hidden" name="<?=$key?>" value="<?=Encode($strID)?>">	

<?php
			}
			elseif($key=="Name")
			{
?>	
											<div class="form-group"><label class="col-sm-3 control-label"><?=str_replace("Name", "Name", $key)?> <span>*</span></label>
												<div class="col-sm-4"><input type="text" name="<?=$key?>" class="form-control required" placeholder="<?=str_replace("Name", "Name", $key)?>" value="<?=$row[$key]?>"></div>
											</div>
<?php
			}
		 elseif($key=="Descriprtion")
		{
?>
														<div class="form-group"><label class="col-sm-3 control-label"><?=str_replace("Description", "Description", $key)?> <span>*</span></label>
															<div class="col-sm-4"><textarea  rows="4" cols="150" name="<?=$key?>" id="<?=str_replace("Description", "Description", $key)?>" class="form-control  wysiwyg" placeholder="<?=str_replace("Description", "Description", $key)?>"><?=$row[$key]?></textarea></div>
														</div>			
                                                        													
													
			
<?php
		}
			elseif($key=="Status")
			{
?>
											<div class="form-group"><label class="col-sm-3 control-label"><?=str_replace("Admin", " ", $key)?> <span>*</span></label>
												<div class="col-sm-4">
													<select name="<?=$key?>" class="form-control required">
														<?php
															if ($row[$key]=="0")
															{
														?>
																<option value="0" selected>Live</option>
																<option value="1">Offline</option>
														<?php
															}
															elseif ($row[$key]=="1")
															{
														?>
																<option value="0">Live</option>
																<option value="1" selected>Offline</option>
														<?php
															}
															else
															{
														?>
																<option value="" selected>--Choose option--</option>
																<option value="0">Live</option>
																<option value="1">Offline</option>
														<?php
															}
														?>	
													</select>
												</div>
											</div>
<?php	
			}
			
		}
	}
?>
											<div class="form-group"><label class="col-sm-3 control-label"></label>
												<input type="submit" class="btn ra-100 btn-primary" value="Update">
												
												<div class="col-sm-2"><a class="btn ra-100 btn-black-opacity" href="javascript:;" onclick="ClearInfo('enquiry_form');" title="Clear"><span>Clear</span></a></div>
											</div>
<?php
}
$DB->close();
?>													
										
									</div>
							</form>
							</div>
						</div>
                   </div>			
<?php
}
?>	                   
                </div>
            </div>
        </div>
		
        <?php require_once 'incFooter.fya'; ?>
		
    </div>
</body>

</html>