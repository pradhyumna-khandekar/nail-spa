<?php

require_once 'setting.fya';
require_once 'incFirewall.fya';
$order_id = Filter($_POST["order_id"]);
$status = Filter($_POST["status"]);
if ($order_id != '' && $status != '') {
    $DB = Connect();

    /*
     * Get Order Products
     */
    $order_pdt = select("*", "order_request_product", "status=1 AND order_request_id='" . $order_id . "'");

    $order_req = select("*", "order_request", "id='" . $order_id . "'");


    if (isset($order_pdt) && is_array($order_pdt) && count($order_pdt) > 0) {
        foreach ($order_pdt as $okey => $ovalue) {
            $pdt_id_arr[$ovalue['product_id']] = $ovalue['product_id'];
        }
    }


    if (isset($pdt_id_arr) && is_array($pdt_id_arr) && count($pdt_id_arr) > 0) {
        $pdt_in_ids = implode(",", $pdt_id_arr);
        if ($pdt_in_ids != '') {
            /*
             * Get Order Product Which are not approved
             */
            $product_data = select("ProductID,ProductName,ProductUniqueCode,barcode_present", "tblNewProducts", "ProductID IN(" . $pdt_in_ids . ")");
        }
    }


    if (isset($product_data) && is_array($product_data) && count($product_data) > 0) {
        foreach ($product_data as $pkey => $pvalue) {
            $pdt_name_data[$pvalue['ProductID']] = $pvalue;
            if ($status == 2 && $pvalue['is_approve'] == 2) {
                $unapp_pdt[$pvalue['ProductID']] = $pvalue['ProductName'] . ' (Code : ' . $pvalue['ProductUniqueCode'] . ')';
            }
        }
    }

    if (isset($unapp_pdt) && is_array($unapp_pdt) && count($unapp_pdt) > 0) {
        $pdt_name = implode(",", $unapp_pdt);
        $msg = "Product : " . $pdt_name . " not Approved. \nApprove Product First.";
    } else {
        /*
         * Check Approve By admin or audit
         */
        if ($strAdminRoleID == 38) {
            /*
             * Audit Manager
             */

            // if audit has approved request automatically approve admin manager also
            if ($status == 2) {
                $qry = "UPDATE order_request SET approve_status_audit='" . $status . "',modified_date='" . date('Y-m-d H:i:s') . "'"
                        . ",modified_by='" . $strAdminID . "',approve_by_audit='" . $strAdminID . "',approved_date_audit='" . date('Y-m-d H:i:s') . "'"
                        . ",approve_status_admin='" . $status . "',approve_by_admin='" . $strAdminID . "',approved_date_admin='" . date('Y-m-d H:i:s') . "' WHERE id='" . $order_id . "'";
                $DB->query($qry);
            } else {
                $qry = "UPDATE order_request SET approve_status_audit='" . $status . "',modified_date='" . date('Y-m-d H:i:s') . "'"
                        . ",modified_by='" . $strAdminID . "',approve_by_audit='" . $strAdminID . "',approved_date_audit='" . date('Y-m-d H:i:s') . "'"
                        . " WHERE id='" . $order_id . "'";
                $DB->query($qry);
            }
        } else {
            if ($strAdminRoleID == 39) {
                /*
                 * Audit Manager
                 */
                $qry = "UPDATE order_request SET approve_status_admin='" . $status . "',modified_date='" . date('Y-m-d H:i:s') . "'"
                        . ",modified_by='" . $strAdminID . "',approve_by_admin='" . $strAdminID . "',approved_date_admin='" . date('Y-m-d H:i:s') . "'"
                        . " WHERE id='" . $order_id . "'";
                $DB->query($qry);
            }
        }

        $approve_status = select("*", "order_request", "id='" . $order_id . "' AND approve_status_audit=2 AND approve_status_admin=2");
        if (isset($approve_status) && is_array($approve_status) && count($approve_status) > 0 && $status == 2) {

            /*
             * Generate Barcode if Both Audit and Admin Approve
             */

            $barcode_count = 0;
            if (isset($order_pdt) && is_array($order_pdt) && count($order_pdt) > 0) {
                foreach ($order_pdt as $pkey => $pvalue) {
                    if ($pvalue['product_id'] > 0 && $pvalue['quantity'] > 0) {
                        $product_update = "UPDATE order_request_product SET is_barcode_generated='3'"
                                . " WHERE order_request_id='" . $order_id . "' AND product_id='" . $pvalue['product_id'] . "'";
                        $DB->query($product_update);
                        /* if (isset($pdt_name_data[$pvalue['product_id']]['barcode_present']) && $pdt_name_data[$pvalue['product_id']]['barcode_present'] == 2) {
                          $pro_qty = $pvalue['quantity'];
                          for ($i = 0; $i < $pro_qty; $i++) {
                          $insert_query = '';
                          $product_barcode = array();
                          /*
                         * Get Max Barcode From product_barcode
                         */
                        /* $max_barcode = select("MAX(barcode) as barcode", "product_barcode", "status=1");


                          if (isset($max_barcode) && is_array($max_barcode) && count($max_barcode) > 0 && $max_barcode[0]['barcode'] != '') {
                          $product_barcode[$barcode_count]['barcode'] = $max_barcode[0]['barcode'] + 1;
                          } else {
                          $product_barcode[$barcode_count]['barcode'] = $i + 1;
                          }
                          $product_name = isset($pdt_name_data[$pvalue['product_id']]) ? trim($pdt_name_data[$pvalue['product_id']]['ProductName']) : '';
                          //$product_barcode[$barcode_count]['prefix'] = strtoupper(substr($product_name, 0, 2));
                          $product_barcode[$barcode_count]['prefix'] = "NS";
                          $product_barcode[$barcode_count]['full_barcode'] = "NS" . sprintf("%010d", $product_barcode[$barcode_count]['barcode']);
                          ;
                          $product_barcode[$barcode_count]['created_date'] = date('Y-m-d H:i:s');
                          $product_barcode[$barcode_count]['created_by'] = $strAdminID;
                          //$product_barcode[$barcode_count]['product_id'] = $pvalue['product_id'];
                          $product_barcode[$barcode_count]['order_request_id'] = $order_id;
                          $product_barcode[$barcode_count]['store_id'] = isset($order_req[0]['store_id']) ? $order_req[0]['store_id'] : 0;


                          if (isset($product_barcode) && is_array($product_barcode) && count($product_barcode) > 0) {
                          foreach ($product_barcode as $ikey => $ivalue) {
                          foreach ($ivalue as $ickey => $icvalue) {
                          $col[$ickey] = $ickey;
                          $row_value[$ikey][$ickey] = "'" . $icvalue . "'";
                          }
                          }


                          $insert_query = "INSERT INTO product_barcode (" . implode(",", $col) . ") VALUES";
                          if (isset($row_value) && is_array($row_value) && count($row_value) > 0) {
                          foreach ($row_value as $rkey => $rvalue) {
                          $insert_query .= " (" . implode(",", $rvalue) . ");";
                          /* if ((count($row_value) - 1) == $rkey) {
                          $insert_query .= " (" . implode(",", $rvalue) . ");";
                          } else {
                          $insert_query .= " (" . implode(",", $rvalue) . "),";
                          } */
                        /*  }
                          }
                          $DB->query($insert_query);
                          }

                          unset($product_barcode);
                          unset($col);
                          unset($row_value);
                          $barcode_count++;
                          }
                          $product_update = "UPDATE order_request_product SET is_barcode_generated='2'"
                          . " WHERE order_request_id='" . $order_id . "' AND product_id='" . $pvalue['product_id'] . "'";
                          $DB->query($product_update);
                          } else {
                          $product_update = "UPDATE order_request_product SET is_barcode_generated='3'"
                          . " WHERE order_request_id='" . $order_id . "' AND product_id='" . $pvalue['product_id'] . "'";
                          $DB->query($product_update);
                          } */
                    }
                }
            }
        }


        $msg = "success";
    }
    $DB->close();
} else {
    $msg = "fail";
}

echo $msg;
?>