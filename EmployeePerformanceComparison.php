<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php require_once 'comparison_report_helper.php'; ?>
<?php
if (isset($_GET) && is_array($_GET) && count($_GET) > 0) {
    $date1 = isset($_GET['date1']) ? $_GET['date1'] : '';
    $date2 = isset($_GET['date2']) ? $_GET['date2'] : '';
    $date3 = isset($_GET['date3']) ? $_GET['date3'] : '';
    $Store = isset($_GET['Store']) && !empty($_GET['Store']) ? $_GET['Store'] : '';

    if ($date1 != '') {
        $date1_data = Employee_sale($date1, $Store);
    }

    if ($date2 != '') {
        $date2_data = Employee_sale($date2, $Store);
    }

    if ($date3 != '') {
        $date3_data = Employee_sale($date3, $Store);
    }
}
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <!-----------css & js files added for tabs by gandhali 3/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>
    </head>

    <body>
        <div id="sb-site">
            <?php require_once("incLoader.fya"); ?>



            <div id="page-wrapper">


                <?php
                $year1_total = 0;
                $year2_total = 0;
                $year3_total = 0;
                if (isset($_GET) && is_array($_GET) && count($_GET) > 0) {
                    if (isset($_GET['emp_id']) && $_GET['emp_id'] != '') {
                        $emp_data = select("*", "tblEmployees", "EID = '" . $_GET['emp_id'] . "'");
                        if (isset($emp_data) && is_array($emp_data) && count($emp_data) > 0) {
                            $employee_name = $emp_data[0]['EmployeeName'];
                        }
                    }
                    ?>
                    <h3>Employee - <?php echo isset($employee_name) ? ucwords($employee_name) : ''; ?></h3>
                    <table class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Month</th>
                                <th><?php echo isset($_GET['date1']) && $_GET['date1'] != '' ? $_GET['date1'] : 'Date1' ?></th>
                                <th><?php echo isset($_GET['date2']) && $_GET['date2'] != '' ? $_GET['date2'] : 'Date2' ?></th>
                                <th><?php echo isset($_GET['date3']) && $_GET['date3'] != '' ? $_GET['date3'] : 'Date3' ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $year1 = isset($_GET['date1']) ? $_GET['date1'] : '';
                            $year2 = isset($_GET['date2']) ? $_GET['date2'] : '';
                            $year3 = isset($_GET['date3']) ? $_GET['date3'] : '';
                            for ($m = 01; $m <= 12; ++$m) {
                                ?>
                                <tr>
                                    <td><?php echo date('F', mktime(0, 0, 0, $m, 1)); ?></td>
                                    <?php
                                    if (strlen($m) == 1) {
                                        $mon = '0' . $m;
                                    } else {
                                        $mon = $m;
                                    }

                                    $chart_data[$year1]['name'] = $year1;
                                    $chart_data[$year2]['name'] = $year2;
                                    $chart_data[$year3]['name'] = $year3;

                                    $chart_data[$year1]['data'][$mon] = isset($date1_data[$year1 . '-' . $mon]) ? $date1_data[$year1 . '-' . $mon] : 0;
                                    $chart_data[$year2]['data'][$mon] = isset($date2_data[$year2 . '-' . $mon]) ? $date2_data[$year2 . '-' . $mon] : 0;
                                    $chart_data[$year3]['data'][$mon] = isset($date3_data[$year3 . '-' . $mon]) ? $date3_data[$year3 . '-' . $mon] : 0;

                                    $year1_data = isset($date1_data[$year1 . '-' . $mon]) ? round($date1_data[$year1 . '-' . $mon], 2) : 0;
                                    $year2_data = isset($date2_data[$year2 . '-' . $mon]) ? round($date2_data[$year2 . '-' . $mon], 2) : 0;
                                    $year3_data = isset($date3_data[$year3 . '-' . $mon]) ? round($date3_data[$year3 . '-' . $mon], 2) : 0;

                                    $year1_total += $year1_data;
                                    $year2_total += $year2_data;
                                    $year3_total += $year3_data;
                                    ?>
                                    <td><?php echo isset($year1_data) ? $year1_data : 0; ?></td>
                                    <td><?php echo isset($year2_data) ? $year2_data : 0; ?></td>
                                    <td><?php echo isset($year3_data) ? $year3_data : 0; ?></td>
                                </tr>
                            <?php } ?>

                            <tr>
                                <td><b>Total</b></td>
                                <td><b><?php echo isset($year1_total) ? round($year1_total, 2) : 0; ?></b></td>
                                <td><b><?php echo isset($year2_total) ? round($year2_total, 2) : 0; ?></b></td>
                                <td><b><?php echo isset($year3_total) ? round($year3_total, 2) : 0; ?></b></td>
                            </tr>
                        </tbody>
                    </table>

                <?php } else { ?>
                    <h3>No Date Selected.</h3>
                    <?php
                }

                $final_data = array();
                if (isset($chart_data) && is_array($chart_data) && count($chart_data) > 0) {
                    $col_count = 0;
                    foreach ($chart_data as $ykey => $yvalue) {
                        $final_data[$col_count]['name'] = $yvalue['name'];
                        $mon_count = 0;
                        foreach ($yvalue['data'] as $ykey => $yvalue) {
                            $final_data[$col_count]['data'][$mon_count] = $yvalue;
                            $mon_count++;
                        }
                        $col_count++;
                    }
                }
                ?>
            </div>

            <script src="assets/widgets/highcharts/highcharts.js"></script>
            <script src="assets/widgets/highcharts/exporting.js"></script>
            <script src="assets/widgets/highcharts/export-data.js"></script>

            <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            <script>
                Highcharts.chart('container', {
                    chart: {
                        type: 'line'
                    },
                    title: {
                        text: '<?php echo isset($employee_name) ? ucwords($employee_name) : ''; ?>'
                    },
                    xAxis: {
                        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                    },
                    yAxis: {
                        title: {
                            text: 'Service Sale(In Rs.)'
                        }
                    },
                    plotOptions: {
                        line: {
                            dataLabels: {
                                enabled: true
                            },
                            enableMouseTracking: true
                        }
                    },
                    series: <?php echo json_encode($final_data); ?>
                });
            </script>
            <?php require_once 'incFooter.fya'; ?>
        </div>
    </body>
</html>