<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>
<?php require_once 'comparison_report_helper.php'; ?>
<?php
if (isset($_GET) && is_array($_GET) && count($_GET) > 0) {
    $date1 = isset($_GET['date1']) ? $_GET['date1'] : '';
    $date2 = isset($_GET['date2']) ? $_GET['date2'] : '';
    $date3 = isset($_GET['date3']) ? $_GET['date3'] : '';
    $Store = isset($_GET['Store']) && !empty($_GET['Store']) ? $_GET['Store'] : '';
    if ($date1 != '') {
        $date1_data = Membership_sale($date1, $Store);
    }

    if ($date2 != '') {
        $date2_data = Membership_sale($date2, $Store);
    }

    if ($date3 != '') {
        $date3_data = Membership_sale($date3, $Store);
    }
}
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <!-----------css & js files added for tabs by gandhali 3/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>
    </head>

    <body>
        <div id="sb-site">
            <?php require_once("incLoader.fya"); ?>
            <div id="page-wrapper">
                <?php
                if (isset($_GET) && is_array($_GET) && count($_GET) > 0) {
                    ?>
                    <table class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Month</th>
                                <th><?php echo isset($_GET['date1']) && $_GET['date1'] != '' ? $_GET['date1'] : 'Date1' ?></th>
                                <th><?php echo isset($_GET['date2']) && $_GET['date2'] != '' ? $_GET['date2'] : 'Date2' ?></th>
                                <th><?php echo isset($_GET['date3']) && $_GET['date3'] != '' ? $_GET['date3'] : 'Date3' ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total_new_amount1 = 0;
                            $total_renew_amount1 = 0;
                            $total_new1 = 0;
                            $total_renew1 = 0;

                            $total_new_amount2 = 0;
                            $total_renew_amount2 = 0;
                            $total_new2 = 0;
                            $total_renew2 = 0;

                            $total_new_amount3 = 0;
                            $total_renew_amount3 = 0;
                            $total_new3 = 0;
                            $total_renew3 = 0;

                            $year1 = isset($_GET['date1']) ? $_GET['date1'] : '';
                            $year2 = isset($_GET['date2']) ? $_GET['date2'] : '';
                            $year3 = isset($_GET['date3']) ? $_GET['date3'] : '';
                            for ($m = 01; $m <= 12; ++$m) {
                                ?>
                                <tr>
                                    <td><?php echo date('F', mktime(0, 0, 0, $m, 1)); ?></td>
                                    <?php
                                    if (strlen($m) == 1) {
                                        $mon = '0' . $m;
                                    } else {
                                        $mon = $m;
                                    }

                                    $chart_data[$year1]['name'] = $year1;
                                    $chart_data[$year2]['name'] = $year2;
                                    $chart_data[$year3]['name'] = $year3;

                                    $new_cust1 = isset($date1_data[$year1 . '-' . $mon]['new_cust']) ? $date1_data[$year1 . '-' . $mon]['new_cust'] : 0;
                                    $new_cust_amount1 = isset($date1_data[$year1 . '-' . $mon]['new_cust_amount']) ? $date1_data[$year1 . '-' . $mon]['new_cust_amount'] : 0;
                                    // $renew_cust1 = isset($date1_data[$year1 . '-' . $mon]['renew_cust']) ? $date1_data[$year1 . '-' . $mon]['renew_cust'] : 0;
                                    // $renew_cust_amount1 = isset($date1_data[$year1 . '-' . $mon]['renew_cust_amount']) ? $date1_data[$year1 . '-' . $mon]['renew_cust_amount'] : 0;


                                    $new_cust2 = isset($date2_data[$year2 . '-' . $mon]['new_cust']) ? $date2_data[$year2 . '-' . $mon]['new_cust'] : 0;
                                    $new_cust_amount2 = isset($date2_data[$year2 . '-' . $mon]['new_cust_amount']) ? $date2_data[$year2 . '-' . $mon]['new_cust_amount'] : 0;
                                    // $renew_cust2 = isset($date2_data[$year2 . '-' . $mon]['renew_cust']) ? $date2_data[$year2 . '-' . $mon]['renew_cust'] : 0;
                                    // $renew_cust_amount2 = isset($date2_data[$year2 . '-' . $mon]['renew_cust_amount']) ? $date2_data[$year2 . '-' . $mon]['renew_cust_amount'] : 0;


                                    $new_cust3 = isset($date3_data[$year3 . '-' . $mon]['new_cust']) ? $date3_data[$year3 . '-' . $mon]['new_cust'] : 0;
                                    $new_cust_amount3 = isset($date3_data[$year3 . '-' . $mon]['new_cust_amount']) ? $date3_data[$year3 . '-' . $mon]['new_cust_amount'] : 0;
                                    //$renew_cust3 = isset($date3_data[$year3 . '-' . $mon]['renew_cust']) ? $date3_data[$year3 . '-' . $mon]['renew_cust'] : 0;
                                    //$renew_cust_amount3 = isset($date3_data[$year3 . '-' . $mon]['renew_cust_amount']) ? $date3_data[$year3 . '-' . $mon]['renew_cust_amount'] : 0;


                                    $total_new_amount1 += $new_cust_amount1;
                                    //$total_renew_amount1 += $renew_cust_amount1;
                                    $total_new1 += $new_cust1;
                                    //$total_renew1 += $renew_cust1;

                                    $total_new_amount2 += $new_cust_amount2;
                                    //$total_renew_amount2 += $renew_cust_amount2;
                                    $total_new2 += $new_cust2;
                                    // $total_renew2 += $renew_cust2;

                                    $total_new_amount3 += $new_cust_amount3;
                                    //$total_renew_amount3 += $renew_cust_amount3;
                                    $total_new3 += $new_cust3;
                                    //$total_renew3 += $renew_cust3;

                                    $chart_data[$year1]['data'][$mon] = isset($new_cust1) ? $new_cust1 : 0;
                                    $chart_data[$year2]['data'][$mon] = isset($new_cust2) ? $new_cust2 : 0;
                                    $chart_data[$year3]['data'][$mon] = isset($new_cust3) ? $new_cust3 : 0;
                                    ?>
                                    <td>
                                        <b>New Members :</b> <?php echo $new_cust1; ?><br>
                                        <b>New Members Amount :</b> <?php echo $new_cust_amount1; ?>
                                        <?php /* <br><b>Repeat Customer :</b> <?php echo $renew_cust1; ?> (<?php echo $renew_cust_amount1; ?> ) */ ?>
                                    </td>
                                    <td>
                                        <b>New Members :</b> <?php echo $new_cust2; ?> <br>
                                        <b>New Members Amount :</b> <?php echo $new_cust_amount2; ?>
                                        <?php /* <br><b>Repeat Customer :</b> <?php echo $renew_cust2; ?> (<?php echo $renew_cust_amount2; ?> ) */ ?>
                                    </td>
                                    <td>
                                        <b>New Members :</b> <?php echo $new_cust3; ?><br>
                                        <b>New Members Amount :</b> <?php echo $new_cust_amount3; ?>
                                        <?php /* <br><b>Repeat Customer :</b> <?php echo $renew_cust3; ?> (<?php echo $renew_cust_amount3; ?> ) */ ?>
                                    </td>
                                </tr>
                            <?php } ?>

                            <tr>
                                <td>Total</td>
                                <td>
                                    <b>New Members :</b> <?php echo $total_new1; ?> <br>
                                    <b>New Members Amount :</b> <?php echo $total_new_amount1; ?>
                                    <?php /*  <br><b>Repeat Customer :</b> <?php echo $total_renew1; ?> (<?php echo $total_renew_amount1; ?> ) */ ?> 
                                </td>
                                <td>
                                    <b>New Members :</b> <?php echo $total_new2; ?> <br>
                                    <b>New Members Amount :</b> <?php echo $total_new_amount2; ?>
                                    <?php /*  <br><b>Repeat Customer :</b> <?php echo $total_renew2; ?> (<?php echo $total_renew_amount2; ?> ) */ ?> 
                                </td>
                                <td>
                                    <b>New Members :</b> <?php echo $total_new3; ?> <br>
                                    <b>New Members Amount :</b> <?php echo $total_new_amount3; ?>
                                    <?php /*  <br><b>Repeat Customer :</b> <?php echo $total_renew3; ?> (<?php echo $total_renew_amount3; ?> ) */ ?> 
                                </td>
                            </tr> 
                        </tbody>
                    </table>

                <?php } else { ?>
                    <h3>No Date Selected.</h3>
                    <?php
                }

                $final_data = array();
                if (isset($chart_data) && is_array($chart_data) && count($chart_data) > 0) {
                    $col_count = 0;
                    foreach ($chart_data as $ykey => $yvalue) {
                        $final_data[$col_count]['name'] = $yvalue['name'];
                        $mon_count = 0;
                        foreach ($yvalue['data'] as $ykey => $yvalue) {
                            $final_data[$col_count]['data'][$mon_count] = $yvalue;
                            $mon_count++;
                        }
                        $col_count++;
                    }
                }
                ?>
            </div>

            <script src="assets/widgets/highcharts/highcharts.js"></script>
            <script src="assets/widgets/highcharts/exporting.js"></script>
            <script src="assets/widgets/highcharts/export-data.js"></script>

            <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            <script>
                Highcharts.chart('container', {
                    chart: {
                        type: 'line'
                    },
                    title: {
                        text: 'Customer Membership'
                    },
                    xAxis: {
                        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                    },
                    yAxis: {
                        title: {
                            text: 'Service Sale(In Rs.)'
                        }
                    },
                    plotOptions: {
                        line: {
                            dataLabels: {
                                enabled: true
                            },
                            enableMouseTracking: true
                        }
                    },
                    series: <?php echo json_encode($final_data); ?>
                });
            </script>
            <?php require_once 'incFooter.fya'; ?>
        </div>
    </body>
</html>