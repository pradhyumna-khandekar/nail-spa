<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Sales Report | Nailspa";
$strDisplayTitle = "Sales Report Nailspa";
$strMenuID = "2";
$strMyTable = "tblStoreStock";
$strMyTableID = "StoreStockID";
$strMyField = "";
$strMyActionPage = "ReportSale.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}
// code for not allowing the normal admin to access the super admin rights	



if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $strStep = Filter($_POST["step"]);

    if ($strStep == "add") {
        
    }

    if ($strStep == "edit") {
        
    }
}
?>


<?php
if (isset($_GET["toandfrom"])) {
    $strtoandfrom = $_GET["toandfrom"];
    $arraytofrom = explode("-", $strtoandfrom);

    $from = $arraytofrom[0];
    $datetime = new DateTime($from);
    $getfrom = $datetime->format('Y-m-d');


    $to = $arraytofrom[1];
    $datetime = new DateTime($to);
    $getto = $datetime->format('Y-m-d');

    if (!IsNull($from)) {
        $sqlTempfrom = " and Date(tblInvoiceDetails.OfferDiscountDateTime)>='" . $getfrom . "'";
        $sqlTempfrom1 = " and Date(tblAppointmentMembershipDiscount.DateTimeStamp)>='" . $getfrom . "'";
        $sqlTempfrom2 = " and Date(tblGiftVouchers.RedempedDateTime)>='" . $getfrom . "'";
        $sqlTempfrom3 = " and Date(tblPendingPayments.DateTimeStamp)>='" . $getfrom . "'";
        $sqlTempfrom4 = " and Date(tblGiftVouchers.Date)>='" . $getfrom . "'";
        $sqlTempfrom5 = " Date(tblCustomers.RegDate)>='" . $getfrom . "'";
        $sqlTempfrom6 = " and Date(tblCustomers.RegDate)<'" . $getfrom . "'";
        $sqlTempfrom7 = " and Date(tblAppointments.AppointmentDate)>='" . $getfrom . "'";
    }

    if (!IsNull($to)) {
        $sqlTempto = " and Date(tblInvoiceDetails.OfferDiscountDateTime)<='" . $getto . "'";
        $sqlTempto1 = " and Date(tblAppointmentMembershipDiscount.DateTimeStamp)<='" . $getto . "'";
        $sqlTempto2 = " and Date(tblGiftVouchers.RedempedDateTime)<='" . $getto . "'";
        $sqlTempto3 = " and Date(tblPendingPayments.DateTimeStamp)<='" . $getto . "'";
        $sqlTempto4 = " and Date(tblGiftVouchers.Date)<='" . $getto . "'";
        $sqlTempto5 = " and Date(tblCustomers.RegDate)<='" . $getto . "'";
        $sqlTempto7 = " and Date(tblAppointments.AppointmentDate)<='" . $getto . "'";
    }
}

if (!IsNull($_GET["Store"])) {
    $strStoreID = $_GET["Store"];

    $sqlTempStore = " StoreID='$strStoreID'";
}
?>	


<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>

        <script type="text/javascript" src="assets/widgets/datepicker/datepicker.js"></script>
        <script type="text/javascript">
            /* Datepicker bootstrap */

            $(function () {
                "use strict";
                $('.bootstrap-datepicker').bsdatepicker({
                    format: 'mm-dd-yyyy'
                });
            });
        </script>
        <script>
            function printDiv(divName)
            {

                var divToPrint = document.getElementById("printdata");
                var htmlToPrint = '' +
                        '<style type="text/css">' +
                        'table th, table td {' +
                        'border:1px solid #000;' +
                        'padding;0.5em;' +
                        '}' +
                        '</style>';
                htmlToPrint += divToPrint.outerHTML;
                newWin = window.open("");
                newWin.document.write(htmlToPrint);
                newWin.print();
                newWin.close();
                // var printContents = document.getElementById(divName);
                // var originalContents = document.body.innerHTML;

                // document.body.innerHTML = printContents;

                // window.print();

                // document.body.innerHTML = originalContents; 
            }

        </script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker-demo.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/moment.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");       ?>
            <!----------commented by gandhali 5/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>


                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                        </div>
                        <?php
                        if (!isset($_GET["uid"])) {
                            ?>					

                            <div class="panel">
                                <div class="panel">
                                    <div class="panel-body">


                                        <div class="example-box-wrapper">
                                            <div class="tabs">

                                                <div id="normal-tabs-1">

                                                    <span class="form_result">&nbsp; <br>
                                                    </span>

                                                    <div class="panel-body">

                                                        <form method="get" class="form-horizontal bordered-row" role="form">

                                                            <div class="form-group"><label for="" class="col-sm-4 control-label">Select Date Range</label>
                                                                <div class="col-sm-4">
                                                                    <div class="input-prepend input-group">
                                                                        <span class="add-on input-group-addon">
                                                                            <i class="glyph-icon icon-calendar"></i>
                                                                        </span> 
                                                                        <input type="text" name="toandfrom" id="daterangepicker-example" class="form-control" value="<?= $strtoandfrom ?>">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label">Select Store</label>
                                                                <div class="col-sm-4">
                                                                    <select name="Store" class="form-control">
                                                                        <option value="0">All</option>
                                                                        <?php
                                                                        $selp = select("*", "tblStores", "Status='0'");
                                                                        foreach ($selp as $val) {
                                                                            $active_store[$val["StoreID"]] = $val["StoreName"];
                                                                            $strStoreName = $val["StoreName"];
                                                                            $strStoreID = $val["StoreID"];
                                                                            $store = $_GET["Store"];
                                                                            if ($store == $strStoreID) {
                                                                                ?>
                                                                                <option  selected value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                                <?php
                                                                            } else {
                                                                                ?>
                                                                                <option value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label">Select Appointment Source</label>
                                                                <div class="col-sm-4">
                                                                    <select name="source" class="form-control">
                                                                        <option value="0">All</option>
                                                                        <?php
                                                                        $sel_source = select("*", "tblsource", "status = '1'");
                                                                        foreach ($sel_source as $val) {
                                                                            ?>
                                                                            <option value="<?= $val['id']; ?>" <?php echo isset($_GET['source']) && $_GET['source'] == $val['id'] ? 'selected' : ''; ?>><?= $val['name'] ?></option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label">Select Percentage</label>
                                                                <div class="col-sm-4">
                                                                    <?php
                                                                    $per = $_GET["per"];
                                                                    ?>
                                                                    <select name="per" class="form-control">
                                                                        <option value="0" <?php if ($per == '0') { ?> selected <?php } ?>>Without Percentage</option>
                                                                        <option value="1" <?php if ($per == '1') { ?> selected <?php } ?>>Percentage</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <?php /*  <div class="form-group">
                                                              <label class="col-sm-4 control-label">Select Percentage</label>
                                                              <div class="col-sm-4">
                                                              <?php
                                                              $per = $_GET["per"];
                                                              ?>
                                                              <select name="per" class="form-control">
                                                              <option value="0" <?php if ($per == '0') { ?> selected <?php } ?>>Without Percentage</option>
                                                              <option value="1" <?php if ($per == '1') { ?> selected <?php } ?>>Percentage</option>
                                                              </select>
                                                              </div>
                                                              </div> */ ?>



                                                            <div class="form-group"><label class="col-sm-3 control-label"></label>
                                                                <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                                                &nbsp;&nbsp;&nbsp;
                                                                <a class="btn btn-link" href="ReportSaleOG.php">Clear All Filter</a>
                                                                &nbsp;&nbsp;&nbsp;
                                                                <button onclick="printDiv('printarea')" class="btn btn-blue-alt">Print<div class="ripple-wrapper"></div></button>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <!--<a class="btn btn-border btn-alt border-primary font-primary" href="ExcelExportData.php?from=<?= $getfrom ?>&to=<?= $getto ?>" title="Excel format 2016"><span>Export To Excel</span><div class="ripple-wrapper"></div></a>
                                                                -->

                                                            </div>
                                                        </form>

                                                        <br>

                                                        <?php
                                                        if ($_GET["toandfrom"] != "") {
                                                            $storr = $_GET["Store"];
                                                            if ($storr == '0') {
                                                                $storrrp = 'All';
                                                            } else {
                                                                $stpp = select("StoreName", "tblStores", "StoreID='" . $storr . "'");
                                                                $StoreName = $stpp[0]['StoreName'];
                                                                $storrrp = $StoreName;
                                                            }
                                                            ?>
                                                            <div id="printdata">
                                                                <h3 class="title-hero">Date Range selected : FROM - <?= $getfrom ?> / TO - <?= $getto ?> / Store Filter selected : <?= $storrrp ?> </h3>
                                                                <?php
                                                                // echo $getfrom."<br>";
                                                                // echo $storrrp."<br>"
                                                                ?>
                                                                <br>
                                                                <?php
                                                                $DB = Connect();
                                                                $counter = 0;
                                                                $per = $_GET["per"];
                                                                ?>
                                                                <div class="panel">
                                                                    <div class="panel-body">

                                                                        <div class="example-box-wrapper">
                                                                            <div class="scroll-columns">



                                                                                <table class="table table-bordered table-striped table-condensed cf" width="100%">
                                                                                    <thead class="cf">

                                                                                        <tr>
                                                                                            <th><center>Sr</center></th>

                                                                                    <th class="numeric"><center>Day & Date</center></th>
                                                                                    <th><center>Store</center></th>
                                                                                    <th class="numeric"><center>Service Sale</center></th>
                                                                                    <th class="numeric"><center>Product Sale</center></th>
                                                                                    <th class="numeric"><center>Discount Given</center></th>
                                                                                    <?php
                                                                                    if ($per != '0') {
                                                                                        ?>
                                                                                        <th class="numeric" id="percol" ><center>Discount %</center></th>
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                    <th class="numeric"><center>Total Sale</center></th>

                                                                                    <th class="numeric"><center>Free Service Count</center></th>

                                                                                    <th class="numeric"><center>Customer Count</center></th>
                                                                                    <th class="numeric"><center>New Client Count</center></th>
                                                                                    <?php
                                                                                    if ($per != '0') {
                                                                                        ?>

                                                                                        <th class="numeric" id="percol" ><center>New Client %</center></th>
                                                                                        <?php
                                                                                    }
                                                                                    ?>

                                                                                    <th class="numeric"><center>Existing Client Count</center></th>
                                                                                    <?php
                                                                                    if ($per != '0') {
                                                                                        ?>

                                                                                        <th class="numeric" id="percoll" ><center>Existing Client %</center></th>
                                                                                        <?php
                                                                                    }
                                                                                    ?>

                                                                                    <th>ARPO</th>
                                                                                    </tr>   


                                                                                    </thead>

                                                                                    <tbody>

                                                                                        <?php
                                                                                        $strtoandfrom = $_GET["toandfrom"];
                                                                                        $arraytofrom = explode("-", $strtoandfrom);

                                                                                        $from = $arraytofrom[0];
                                                                                        $datetime = new DateTime($from);
                                                                                        $getfrom = $datetime->format('Y-m-d');


                                                                                        $to = $arraytofrom[1];
                                                                                        $datetime = new DateTime($to);
                                                                                        $getto = $datetime->format('Y-m-d');
                                                                                        $DateTime = FormatDateTime($getfrom);
                                                                                        $DateTime1 = FormatDateTime($getto);

                                                                                        $StartOfDay = date('D', strtotime($getfrom));
                                                                                        $EndOfDay = date('D', strtotime($getto));

                                                                                        $sqlTempfrom2 = " and Date(tblGiftVouchers.RedempedDateTime)>='" . $getfrom . "'";
                                                                                        $sqlTempto2 = " and Date(tblGiftVouchers.RedempedDateTime)<='" . $getto . "'";
                                                                                        /*
                                                                                         * get appointmetn id store wise
                                                                                         */

                                                                                        $append = "";
                                                                                        if (!empty($_GET["Store"])) {
                                                                                            $append = " AND tblAppointments.StoreID='" . $_GET["Store"] . "'";
                                                                                        }

                                                                                        $get_source = $_GET["source"];
                                                                                        if (!empty($get_source)) {
                                                                                            $append .= " AND tblAppointments.source='" . $get_source . "'";
                                                                                        }

                                                                                        $sqlservice = "SELECT AppointmentID,StoreID
		FROM `tblAppointments` 
		WHERE `Status` = 2 AND `IsDeleted` != 1 AND `FreeService` != 1
		AND AppointmentId IN
		(
			SELECT AppointmentId  
			FROM `tblInvoiceDetails` 
			WHERE `AppointmentDate` BETWEEN '" . $getfrom . " 00:00:00' AND '" . $getto . " 23:23:59' " . $append . " AND tblAppointments.IsDeleted=0 AND tblAppointments.Status=2
                                                                                                )";




                                                                                        $RSservice = $DB->query($sqlservice);
                                                                                        if ($RSservice->num_rows > 0) {
                                                                                            while ($rowservice = $RSservice->fetch_assoc()) {
                                                                                                $queryresult[$rowservice['StoreID']][] = $rowservice['AppointmentID'];
                                                                                                $apt_store_map[$rowservice['AppointmentID']] = $rowservice['StoreID'];
                                                                                            }
                                                                                        }

                                                                                        $servicesale = array();
                                                                                        $discountgiven = array();
                                                                                        $customercount = array();
                                                                                        $totalsale = array();



                                                                                        if (isset($queryresult) && is_array($queryresult) && count($queryresult)) {
                                                                                            foreach ($queryresult as $key => $value) {

                                                                                                $gAmount_store = 0;
                                                                                                $memamt = 0;
                                                                                                $offamt = 0;
                                                                                                $TotalRoundTotal = 0;


                                                                                                /*
                                                                                                 * get service sale
                                                                                                 */
                                                                                                $sqlservice = "SELECT sum(qty * ServiceAmount) as servicesale FROM `tblAppointmentsDetailsInvoice` WHERE AppointmentID IN(" . implode(',', $value) . ")";

                                                                                                /* $RSservice = $DB->query($sqlservice);
                                                                                                  if ($RSservice->num_rows > 0) {
                                                                                                  $rowservice = $RSservice->fetch_assoc();
                                                                                                  $servicesale[$key] = $rowservice['servicesale'];
                                                                                                  } */

                                                                                                $sqldata = " SELECT AppointmentID,ServiceID,service_amount_deduct_dis FROM tblAppointmentsDetailsInvoice WHERE ServiceID != 'NULL' AND ServiceID != '' AND AppointmentID IN (" . implode(',', $value) . ")";
                                                                                                $RSdata = $DB->query($sqldata);
                                                                                                if ($RSdata->num_rows > 0) {
                                                                                                    while ($rowdata = $RSdata->fetch_assoc()) {
                                                                                                        $serviceid[$rowdata["ServiceID"]] = $rowdata["ServiceID"];
                                                                                                        if (isset($service_deduct_disc[$rowdata["AppointmentID"]])) {
                                                                                                            $service_deduct_disc[$rowdata["AppointmentID"]] += $rowdata["service_amount_deduct_dis"];
                                                                                                            $service_package_discount[$rowdata["AppointmentID"]] += $rowdata["package_discount"];
                                                                                                        } else {
                                                                                                            $service_deduct_disc[$rowdata["AppointmentID"]] = $rowdata["service_amount_deduct_dis"];
                                                                                                            $service_package_discount[$rowdata["AppointmentID"]] = $rowdata["package_discount"];
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                
                                                                                                
                                                                                                $sqlservice = "SELECT inv.AppointmentId,inv.ServiceName,inv.Qty,inv.ServiceAmt,inv.DisAmt,inv.OfferAmt,inv.SubTotal,inv.RoundTotal FROM `tblInvoiceDetails` inv WHERE AppointmentID IN(" . implode(',', $value) . ")";

                                                                                                $RSservice = $DB->query($sqlservice);
                                                                                                if ($RSservice->num_rows > 0) {
                                                                                                    while ($rowservice = $RSservice->fetch_assoc()) {

                                                                                                        $offer_amount = 0;
                                                                                                        $amount_exculde_dis = 0;
                                                                                                        if (!empty($rowservice['ServiceName'])) {
                                                                                                            $ServiceName = explode(',', $rowservice['ServiceName']);
                                                                                                            $Qty = explode(',', $rowservice['Qty']);
                                                                                                            $ServiceAmt = explode(',', $rowservice['ServiceAmt']);
                                                                                                            $DisAmt = explode(',', $rowservice['DisAmt']);
                                                                                                            $count = count($ServiceName);
                                                                                                            for ($i = 0; $i < $count; $i++) {

                                                                                                                $offer_amount = str_replace(array('-', ' '), '', $rowservice['OfferAmt']);

                                                                                                                if (isset($DisAmt[$i])) {
                                                                                                                    $amount_exculde_dis += ($ServiceAmt[$i] - $DisAmt[$i]);
                                                                                                                } else {
                                                                                                                    $amount_exculde_dis += $ServiceAmt[$i];
                                                                                                                }
                                                                                                                //echo '<br>amount_exculde_dis=' . $amount_exculde_dis;
                                                                                                            }


                                                                                                            if ($offer_amount > 0) {
                                                                                                                $amount_exculde_dis = $amount_exculde_dis - $offer_amount;
                                                                                                            }

                                                                                                            if ($amount_exculde_dis < 0) {
                                                                                                                $amount_exculde_dis = 0;
                                                                                                            }

                                                                                                            $amount_exculde_dis = isset($service_deduct_disc[$rowservice['AppointmentId']]) ? $service_deduct_disc[$rowservice['AppointmentId']] : 0;
                                                                                                            $Store_id_apt = isset($apt_store_map[$rowservice['AppointmentId']]) ? $apt_store_map[$rowservice['AppointmentId']] : 0;
                                                                                                            if ($Store_id_apt != 0) {
                                                                                                                if (isset($servicesale[$Store_id_apt])) {
                                                                                                                    $servicesale[$Store_id_apt] += round($amount_exculde_dis, 2);
                                                                                                                } else {
                                                                                                                    $servicesale[$Store_id_apt] = round($amount_exculde_dis, 2);
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }

                                                                                                /*
                                                                                                 * get discount
                                                                                                 */
                                                                                                $sqlservice = "SELECT sum(Amount) as amt FROM `tblGiftVouchers` WHERE Status = 1 and RedempedBy IN(" . implode(', ', $value) . ")" . $sqlTempfrom2 . $sqlTempto2;

                                                                                                $RSservice = $DB->query($sqlservice);
                                                                                                if ($RSservice->num_rows > 0) {
                                                                                                    $gAmount = $RSservice->fetch_assoc();

                                                                                                    if ($gAmount['amt'] == "") {
                                                                                                        $gAmount = $gAmount_store + 0;
                                                                                                    } else {
                                                                                                        $gAmount = $gAmount_store + $gAmount['amt'];
                                                                                                    }
                                                                                                } else {
                                                                                                    $gAmount = $gAmount_store + 0;
                                                                                                }



                                                                                                $sqlservice = "SELECT DISTINCT AppointmentID,OfferID,OfferAmount as offamt,MembershipAmount as memamt,MembershipID FROM `tblAppointmentMembershipDiscount` WHERE AppointmentID IN(" . implode(', ', $value) . ")";

                                                                                                $RSservice = $DB->query($sqlservice);
                                                                                                if ($RSservice->num_rows > 0) {
                                                                                                    while ($result = $RSservice->fetch_assoc()) {
                                                                                                        if ($result['offamt'] == '') {
                                                                                                            $offamt = $offamt + 0;
                                                                                                        } else {
                                                                                                            $offamt = $offamt + $result['offamt'];
                                                                                                        }
                                                                                                        if ($result['memamt'] == '') {
                                                                                                            $memamt = $memamt + 0;
                                                                                                        } else {
                                                                                                            $memamt = $memamt + $result['memamt'];
                                                                                                        }
                                                                                                        // echo '<br>offamt=' . $offamt . 'memamt=' . $memamt.'gAmount='.$gAmount;
                                                                                                    }
                                                                                                }

                                                                                                $discountgiven[$key] = $gAmount + $memamt + $offamt;



                                                                                                /*
                                                                                                 * get customer count
                                                                                                 */
                                                                                                $sqlservice = "SELECT count(CustomerID) as customercount FROM `tblAppointments` WHERE AppointmentID IN(" . implode(',', $value) . ")";
                                                                                                $RSservice = $DB->query($sqlservice);
                                                                                                if ($RSservice->num_rows > 0) {
                                                                                                    $rowservice = $RSservice->fetch_assoc();
                                                                                                    $customercount[$key] = $rowservice['customercount'];
                                                                                                }
                                                                                                /*
                                                                                                 * get total sale
                                                                                                 */
                                                                                                $sqlservice1 = "SELECT tblInvoiceDetails.RoundTotal from tblInvoiceDetails where tblInvoiceDetails.AppointmentId IN (" . implode(',  ', $value) . ")";
                                                                                                //echo $sqlservice1;exit;
                                                                                                $RSservice1 = $DB->query($sqlservice1);
                                                                                                if ($RSservice1->num_rows > 0) {
                                                                                                    while ($rowservice1 = $RSservice1->fetch_assoc()) {
                                                                                                        $RoundTotal = $rowservice1["RoundTotal"];
                                                                                                        if ($RoundTotal == "") {
                                                                                                            $RoundTotal = "0.00";
                                                                                                        } else {
                                                                                                            $RoundTotal = $RoundTotal;
                                                                                                        }
                                                                                                        $TotalRoundTotal += $RoundTotal;
                                                                                                    }
                                                                                                }
                                                                                                $totalsale[$key] = $TotalRoundTotal;

                                                                                                /*
                                                                                                 * get new customer count
                                                                                                 */
                                                                                                $sqlservicetyp = "select count(tblAppointments.CustomerID) as newcust, tblAppointments.StoreID from tblCustomers left join tblAppointments on tblAppointments.CustomerID = tblCustomers.CustomerID "
                                                                                                        . "where " . $sqlTempfrom5 . $sqlTempto5 . " AND tblAppointments.AppointmentId IN (" . implode(',  ', $value) . ") AND tblAppointments.StoreID='" . $key . "'";

                                                                                                $RSservice2 = $DB->query($sqlservicetyp);
                                                                                                if ($RSservice2->num_rows > 0) {
                                                                                                    while ($rowservice2 = $RSservice2->fetch_assoc()) {
                                                                                                        $newcntcust[$rowservice2["StoreID"]] = $rowservice2["newcust"];
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            /*
                                                                                             * get service count
                                                                                             */
                                                                                            $SelectFreeService = "Select count(FreeService) as FreeService, StoreID from tblAppointments where FreeService = '1' " . $sqlTempfrom7 . $sqlTempto7 . " group by StoreID";

                                                                                            $RSFreeSer = $DB->query($SelectFreeService);
                                                                                            if ($RSFreeSer->num_rows > 0) {
                                                                                                while ($rowFreeSer = $RSFreeSer->fetch_assoc()) {
                                                                                                    $FreeService[$rowFreeSer["StoreID"]] = $rowFreeSer["FreeService"];
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        if (!empty($_GET['Store'])) {
                                                                                            $allstoreslist = select("* ", "tblStores ", " StoreID = " . $_GET['Store']);
                                                                                        } else {
                                                                                            $allstoreslist = select("* ", "tblStores ", " StoreID > 0");
                                                                                        }
                                                                                        ?>

                                                                                    <tbody>
                                                                                        <?php
                                                                                        if (isset($allstoreslist) && is_array($allstoreslist) && count($allstoreslist)) {

                                                                                            foreach ($allstoreslist as $skey => $svalue) {
                                                                                                $store_name[$svalue['StoreID']] = $svalue;
                                                                                                $sorting_service_amt[$svalue['StoreID']] = isset($servicesale[$svalue['StoreID']]) ? $servicesale[$svalue['StoreID']] : '0.00';
                                                                                            }
                                                                                            arsort($sorting_service_amt);
                                                                                        }
                                                                                        $counter = 1;
                                                                                        //if (isset($allstoreslist) && is_array($allstoreslist) && count($allstoreslist)) {
                                                                                        //foreach ($allstoreslist as $key => $value) {

                                                                                        if (empty($_GET['Store'])) {
                                                                                            if (isset($sorting_service_amt) && is_array($sorting_service_amt) && count($sorting_service_amt)) {
                                                                                                foreach ($sorting_service_amt as $skey => $svalue) {
                                                                                                    $service_sale = isset($servicesale[$skey]) ? $servicesale[$skey] : '0.00';
                                                                                                    $tservice_sale += $service_sale;

                                                                                                    $service_dis = isset($discountgiven[$skey]) ? $discountgiven[$skey] : '0.00';
                                                                                                    $tservice_dis += $service_dis;

                                                                                                    $sale_amt = isset($totalsale[$skey]) ? $totalsale[$skey] : '0.00';
                                                                                                    $tsale_amt += $sale_amt;

                                                                                                    $FreeService_cnt = isset($FreeService[$skey]) ? $FreeService[$skey] : '0.00';
                                                                                                    $tFreeService_cnt += $FreeService_cnt;

                                                                                                    $customercount_val = isset($customercount[$skey]) ? $customercount[$skey] : '0.00';
                                                                                                    $tcustomercount_val += $customercount_val;

                                                                                                    $newcntcust_val = isset($newcntcust[$skey]) ? $newcntcust[$skey] : '0.00';
                                                                                                    $tnewcntcust_val += $newcntcust_val;

                                                                                                    $texisting_cust = $tcustomercount_val - $tnewcntcust_val;
                                                                                                }
                                                                                            }
                                                                                            ?>

                                                                                            <tr>
                                                                                                <td><?php echo $counter; ?></td>
                                                                                                <td><center><?= $StartOfDay ?> <?= $DateTime ?> to<br> <?= $EndOfDay ?> <?= $DateTime1 ?></center></td>
                                                                                        <td><?php echo 'All'; ?></td>
                                                                                        <td><?php
                                                                                            $service_sale_amt = isset($tservice_sale) ? $tservice_sale : '0.00';
                                                                                            echo $service_sale_amt;
                                                                                            ?></td> 
                                                                                        <td class="numeric"><center>-</center></td>
                                                                                        <td><?php
                                                                                            $discount_amt = isset($tservice_dis) ? $tservice_dis : '0.00';
                                                                                            echo $discount_amt;
                                                                                            ?></td>
                                                                                        <?php
                                                                                        if ($per != '0') {
                                                                                            ?>
                                                                                            <td class="numeric" id="percol2" ><center>
                                                                                                <?php
                                                                                                $dis_amt_per = ($discount_amt / $tsale_amt) * 100;
                                                                                                echo round($dis_amt_per, 2)
                                                                                                ?>
                                                                                            </center></td>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                                        <td><?php echo isset($tsale_amt) ? $tsale_amt : '0.00'; ?></td>
                                                                                        <td><?php echo isset($tFreeService_cnt) ? $tFreeService_cnt : 0; ?></td>
                                                                                        <td><?php echo isset($tcustomercount_val) ? $tcustomercount_val : 0; ?></td>
                                                                                        <td><?php echo isset($tnewcntcust_val) ? $tnewcntcust_val : 0; ?></td>
                                                                                        <?php
                                                                                        if ($per != '0') {
                                                                                            ?>
                                                                                            <td class="numeric" id="percol2" ><center>
                                                                                                <?php
                                                                                                $existingclientper = ($tnewcntcust_val / $tcustomercount_val) * 100;
                                                                                                echo round($existingclientper, 2)
                                                                                                ?>
                                                                                            </center></td>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                                        <td><?php echo isset($texisting_cust) ? $texisting_cust : 0; ?></td>
                                                                                        <?php
                                                                                        if ($per != '0') {
                                                                                            ?>
                                                                                            <td class="numeric" id="percol2" ><center>
                                                                                                <?php
                                                                                                $existingclientper = ($texisting_cust / $tcustomercount_val) * 100;
                                                                                                echo round($existingclientper, 2)
                                                                                                ?>
                                                                                            </center></td>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                                        <td>
                                                                                            <?php
                                                                                            $arpo = $tsale_amt / $tcustomercount_val;
                                                                                            echo round($arpo, 2);
                                                                                            ?>
                                                                                        </td>
                                                                                        </tr>
                                                                                        <?php
                                                                                    }

                                                                                    if (isset($sorting_service_amt) && is_array($sorting_service_amt) && count($sorting_service_amt)) {


                                                                                        foreach ($sorting_service_amt as $skey => $svalue) {
                                                                                            if (isset($active_store[$skey])) {
                                                                                                ?>
                                                                                                <tr>
                                                                                                    <td><?php echo $counter; ?></td>
                                                                                                    <td><center><?= $StartOfDay ?> <?= $DateTime ?> to<br> <?= $EndOfDay ?> <?= $DateTime1 ?></center></td>
                                                                                                <td><?php echo isset($store_name[$skey]) ? $store_name[$skey]['StoreName'] : ''; ?></td>
                                                                                                <td><?php
                                                                                                    $service_sale_amt = isset($servicesale[$skey]) ? $servicesale[$skey] : '0.00';
                                                                                                    echo $service_sale_amt;
                                                                                                    ?></td> 

                                                                                                <td class="numeric"><center>-</center></td>
                                                                                                <td><?php echo $discountgiven[$skey]; ?></td>
                                                                                                <?php
                                                                                                if ($per != '0') {
                                                                                                    ?>
                                                                                                    <td class="numeric" id="percol2" ><center>
                                                                                                        <?php
                                                                                                        $dis_amt_per = ($discountgiven[$skey] / $totalsale[$skey]) * 100;
                                                                                                        echo round($dis_amt_per, 2)
                                                                                                        ?>
                                                                                                    </center></td>
                                                                                                    <?php
                                                                                                }
                                                                                                ?>
                                                                                                <td><?php echo $totalsale[$skey]; ?></td>
                                                                                                <td><?php echo isset($FreeService[$skey]) ? $FreeService[$skey] : 0; ?></td>
                                                                                                <td><?php echo $customercount[$skey]; ?></td>
                                                                                                <td><?php
                                                                                                    $t = isset($newcntcust[$skey]) ? $newcntcust[$skey] : 0;
                                                                                                    echo $t;
                                                                                                    ?></td>
                                                                                                <?php
                                                                                                if ($per != '0') {
                                                                                                    ?>
                                                                                                    <td class="numeric" id="percol2" ><center>
                                                                                                        <?php
                                                                                                        $existingclientper = ($t / $customercount[$skey]) * 100;
                                                                                                        echo round($existingclientper, 2)
                                                                                                        ?>
                                                                                                    </center></td>
                                                                                                    <?php
                                                                                                }
                                                                                                ?>
                                                                                                <td><?php echo ($customercount[$skey]) - ($newcntcust[$skey]); ?></td>
                                                                                                <?php
                                                                                                if ($per != '0') {
                                                                                                    ?>
                                                                                                    <td class="numeric" id="percol21" ><center>
                                                                                                        <?php
                                                                                                        $existing = ($customercount[$skey]) - ($newcntcust[$skey]);
                                                                                                        $existingclientper = ($existing / $customercount[$skey]) * 100;
                                                                                                        echo round($existingclientper, 2)
                                                                                                        ?></center></td>
                                                                                                    <?php
                                                                                                }
                                                                                                ?>
                                                                                                <td>
                                                                                                    <?php
                                                                                                    $arpo = $totalsale[$skey] / $customercount[$skey];
                                                                                                    echo round($arpo, 2);
                                                                                                    ?>
                                                                                                </td>
                                                                                                </tr>
                                                                                                <?php
                                                                                                $counter++;
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    ?>

                                                                                    </tbody>	
                                                                                </table>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <?php ?>


                                                            <?php
                                                        } else {
                                                            echo "<br><center><h3>Please Select Month And Year!</h3></center>    ";
                                                        }
                                                        ?>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        } // End null condition
                        else {
                            
                        }
                        ?>
                    </div>
                </div>

                <?php require_once 'incFooter.fya'; ?>

            </div>
    </body>

</html>