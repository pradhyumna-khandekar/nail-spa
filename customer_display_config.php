<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Lead Diversion Configuration | Nailspa";
$strDisplayTitle = "Lead Diversion Configuration";
$strMenuID = "3";
$strMyActionPage = "customer_display_config.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $DB = Connect();

    if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
        if (isset($_POST['type']) && $_POST['type'] != '') {
            $sql1 = "Update report_config set email_id='" . $_POST['type'] . "' "
                    . "where report_name ='website_customer_display' AND status='1'";
            $DB->query($sql1);
            die('<div class="alert alert-close alert-success">
					<div class="bg-green alert-icon"><i class="glyph-icon icon-check"></i></div>
					<div class="alert-content">
						<p>Configuration updated Successfully.</p>
					</div>
				</div>');
            exit;
        } else {
            die('<div class="alert alert-warning alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
								</button>
								<strong>Failed to update configuration.</strong>
							</div>');
        }
    }
    $DB->close();
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="refresh" content="240" />
        <?php require_once("incMetaScript.fya"); ?>


    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");      ?>
            <!----------commented by gandhali 3/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                        </div>
                        <?php require_once("incHeader.fya"); ?>


                        <div class="panel">
                            <div class="panel-body">

                                <div class="panel-body">
                                    <form role="form" class="form-horizontal bordered-row config_form" onSubmit="proceed_formsubmit('.config_form', '<?= $strMyActionPage ?>', '.result_message', '', '', '');
                                            return false;">
                                        <span class="result_message">&nbsp; <br></span>
                                        <br>


                                        <?php
                                        /*
                                         * Get Data
                                         */
                                        $config_data = select("*", 'report_config', 'status =1 AND report_name="website_customer_display" ');

                                        $counter = 1;
                                        ?>
                                        <div class="example-box-wrapper">

                                            <div class="form-group"><label class="col-sm-3 control-label">Divert lead to:</label>
                                                <div class="col-sm-4">
                                                    <input type = "radio" name = "type" value = "1" id = "type_1" <?php echo isset($config_data[0]['email_id']) && $config_data[0]['email_id'] == 1 ? 'checked' : 'checked'; ?>/><b><label for="type_1">Telecaller</label></b>&nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                    <input type = "radio" name = "type" value = "2" id = "type_2" <?php echo isset($config_data[0]['email_id']) && $config_data[0]['email_id'] == 2 ? 'checked' : ''; ?>/><b><label for="type_2">Branch Manager</label></b>&nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-6">
                                                    <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Submit</span></button>
                                                </div>
                                            </div>
                                        </div>


                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <script type="text/javascript">
                var DivCount = '<?php echo $counter; ?>';
                $(document).ready(function () {


                    $(".add_field_button").click(function (e) {
                        e.preventDefault();
                        // Max attachment allowed
                        var append_data = '<div class="form-group" id="div_count_' + DivCount + '"><label class="col-sm-3 control-label"></label>' +
                                '<div class="col-sm-4">' +
                                '<input autocomplete="off" type="email" name="email_ids[' + DivCount + ']" value="" class="form-control required">'
                                + '</div><div class="col-sm-4"><label class="col-sm-4 control-label" style="text-align:left;"><a href="#" class="btn btn-link remove_field" data-lang="' + DivCount + '">Remove</a></label></div></div>';
                        $(".config_div").append(append_data); //add attachment
                        DivCount = parseInt(DivCount) + 1;
                    });

                    $(".config_div").on("click", ".remove_field", function (e) { //user click on to remove attachment
                        var div_count = $(this).attr('data-lang');
                        e.preventDefault();
                        $('#div_count_' + div_count).remove();
                    });
                });
            </script>
            <?php require_once 'incFooter.fya'; ?>
        </div>

    </body>
</html>
