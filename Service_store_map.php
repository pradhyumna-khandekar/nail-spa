<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>

<?php
$strPageTitle = "Service - Add / Remove Store | Nailspa";
$strDisplayTitle = "Service - Add / Remove Store for Nailspa";
$strMenuID = "2";
$strMyTable = "services_pos";
$strMyTableID = "ServiceID";
$strMyField = "";
$strMyActionPage = "Service_store_map.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";

// code for not allowing the normal admin to access the super admin rights	
if ($strAdminType != "0") {
    die("Sorry you are trying to enter Unauthorized access");
}

// code for not allowing the normal admin to access the super admin rights	



if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $post_data = $_POST;
    $DB = Connect();
    $service_pos_id = isset($post_data['service_pos_id']) ? $post_data['service_pos_id'] : 0;
    $service_code = isset($post_data['service_code']) ? $post_data['service_code'] : '';
    if ($service_pos_id > 0) {
        $service_data = select("*", "services_pos", "id='" . $service_pos_id . "'");

        $strServiceName = $service_data[0]["ServiceName"];
        $strServiceCode = $service_data[0]["ServiceCode"];
        $strServiceCost = $service_data[0]["ServiceCost"];
        $strServiceCommission = $service_data[0]["ServiceCommission"];
        $strMRPLessTax = $service_data[0]["MRPLessTax"];
        $strDirectCost = $service_data[0]["DirectCost"];
        $strGMPercentage = $service_data[0]["GMPercentage"];
        $strGrossMargin = $service_data[0]["GrossMargin"];
        $strStatus = $service_data[0]["Status"];
        $strTime = $service_data[0]["Time"];
        $cost = $service_data[0]["ProductPrice"];
        $max_quantity = $service_data[0]["max_quantity"];
        $is_concent = $service_data[0]["is_concent"];
        $getfrom = date('Y-m-d');

        $inac_q = "UPDATE tblServices SET active_status=2 WHERE ServiceCode='" . $service_code . "'";
        $DB->query($inac_q);

        // $inacpos_q = "UPDATE services_pos SET active_status=2 WHERE ServiceCode='" . $service_code . "'";
        // $DB->query($inacpos_q);


        $service_code_data = select("*", "tblServices", "ServiceCode='" . $service_code . "'");
        $present_service_id = isset($service_code_data[0]['ServiceID']) ? $service_code_data[0]['ServiceID'] : 0;
        $strCharges = select("*", "tblServicesCharges", "ServiceID='" . $present_service_id . "'");
        $strTechnician = select("*", "tblEmployeesServices", "ServiceID='" . $present_service_id . "'");
        $strProducts = select("*", "tblProductsServices", "ServiceID='" . $present_service_id . "'");
        $strProductsCategory = select("*", "tblProductServiceCategory", "ServiceID='" . $present_service_id . "'");

        if (isset($post_data['store']) && is_array($post_data['store']) && count($post_data['store']) > 0) {
            foreach ($post_data['store'] as $skey => $svalue) {
                if ($svalue > 0) {
                    /*
                     * Check if service present in store or not
                     */
                    $service_store_exist = select("*", "tblServices", "ServiceCode='" . $service_code . "' AND StoreID='" . $svalue . "'");
                    if (isset($service_store_exist) && is_array($service_store_exist) && count($service_store_exist) > 0) {
                        $active_service_pos = "UPDATE tblServices SET active_status=1 WHERE ServiceCode='" . $service_code . "' AND StoreID='" . $svalue . "'";
                        $DB->query($active_service_pos);
                    } else {
                        /*
                         * Service Not Present add new Service
                         */
                        $sqlInsert = "INSERT INTO tblServices (is_concent,max_quantity,ServiceName, ServiceCode, ServiceCost, ServiceCommission, MRPLessTax, DirectCost, GMPercentage, GrossMargin,Status,StoreID,Time,ProductPrice) VALUES
						('" . $is_concent . "','" . $max_quantity . "','" . $strServiceName . "','" . $strServiceCode . "', '" . $strServiceCost . "', '" . $strServiceCommission . "', '" . $strMRPLessTax . "', '" . $strDirectCost . "', '" . $strGMPercentage . "', '" . $strGrossMargin . "' , '" . $strStatus . "' , '" . $svalue . "', '" . $strTime . "','" . $cost . "')";
                        $DB->query($sqlInsert);
                        $last_id = $DB->insert_id;

                        if (isset($strCharges) && is_array($strCharges) && count($strCharges) > 0) {
                            foreach ($strCharges as $chaKey => $Charges) {
                                $sqlInsert1 = "Insert into tblServicesCharges (ServiceID, ChargeNameID, Status) values
							('" . $last_id . "', '" . $Charges['ChargeNameID'] . "','0')";
                                $DB->query($sqlInsert1);
                                $ServicesCharges_id = $DB->insert_id;
                                insert_history('tblServicesCharges', $ServicesCharges_id, 'insert', 'SCID', array('service_history_id' => $last_service_his_id));
                            }
                        }

                        if (isset($strTechnician) && is_array($strTechnician) && count($strTechnician) > 0) {
                            foreach ($strTechnician as $tecjKey => $emp) {
                                $sqlInsert1 = "Insert into tblEmployeesServices (EID, ServiceID,CategoryID,Status) values
							('" . $emp['EID'] . "', '" . $last_id . "','" . $emp['CategoryID'] . "','0')";
                                $DB->query($sqlInsert1);
                                $EmployeeServiceID_id = $DB->insert_id;
                                insert_history('tblEmployeesServices', $EmployeeServiceID_id, 'insert', 'EmployeeServiceID', array('service_history_id' => $last_service_his_id));
                            }
                        }

                        if (isset($strProducts) && is_array($strProducts) && count($strProducts) > 0) {
                            foreach ($strProducts as $Products1) {

                                $sqlInsert2 = "Insert into tblProductsServices(ProductID,ProductStockID, ServiceID, Status,CategoryID,StoreID,DateTimeStamp) values
									('" . $Products1['ProductID'] . "','" . $Products1['ProductStockID'] . "','" . $last_id . "','1','" . $Products1['CategoryID'] . "','" . $svalue . "','" . $Products1['DateTimeStamp'] . "')";

                                $DB->query($sqlInsert2);
                                $ProductServiceID_id = $DB->insert_id;
                                insert_history('tblProductsServices', $ProductServiceID_id, 'insert', 'ProductServiceID', array('service_history_id' => $last_service_his_id));
                            }
                        }


                        if (isset($strProductsCategory) && is_array($strProductsCategory) && count($strProductsCategory) > 0) {
                            foreach ($strProductsCategory as $Products1) {
                                $sqlInsert2 = "Insert into tblProductServiceCategory(ProductID,ServiceID,StoredID,CategoryID) "
                                        . " values('" . $Products1['ProductID'] . "','" . $last_id . "','" . $svalue . "','" . $Products1['CategoryID'] . "')";
                                $DB->query($sqlInsert2);
                                $ProdServiceCatID_id = $DB->insert_id;
                                insert_history('tblProductServiceCategory', $ProdServiceCatID_id, 'insert', 'ProdServiceCatID', array('service_history_id' => $last_service_his_id));
                            }
                        }
                    }
                }
            }
        }
        die('<div class="alert alert-close alert-success">
					<div class="bg-green alert-icon"><i class="glyph-icon icon-check"></i></div>
					<div class="alert-content">
						<h4 class="alert-title">Store Mapped Successfully.</h4>
					</div>
				</div>');
    } else {
        die('<div class="alert alert-warning alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<strong>Service Not Present in any Store.</strong>
					</div>');
    }
    $DB->close();
}
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>

        <script type="text/javascript" src="assets/widgets/datepicker/datepicker.js"></script>
        <script type="text/javascript">
            /* Datepicker bootstrap */

            $(function () {
                "use strict";
                $('.bootstrap-datepicker').bsdatepicker({
                    format: 'mm-dd-yyyy'
                });
            });
        </script>
        <script type = "text/javascript" src = "assets/widgets/datepicker-ui/datepicker.js" ></script>
        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker-demo.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/moment.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript" src="assets/widgets/daterangepicker/daterangepicker-demo.js"></script>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");                        ?>
            <!----------commented by gandhali 5/9/18---------------->
            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>
                <?php require_once("incLeftMenu.fya"); ?>
                <div id="page-content-wrapper">
                    <div id="page-content">
                        <?php require_once("incHeader.fya"); ?>
                        <div id="page-title">
                            <h2><?= $strDisplayTitle ?></h2>
                        </div>


                        <?php
                        $DB = Connect();
                        $service_pos_id = isset($_GET['service_id']) ? $_GET['service_id'] : 0;
                        $all_stores = select("*", "tblStores", "Status='0' ORDER BY StoreName");

                        $service_data = select("*", "services_pos", "id='" . $service_pos_id . "'");

                        /*
                         * Get Service Present in which store
                         */
                        $service_stores = select("*", "tblServices", "ServiceCode='" . $service_data[0]['ServiceCode'] . "' AND active_status=1");

                        if (isset($service_stores) && is_array($service_stores) && count($service_stores) > 0) {
                            foreach ($service_stores as $skey => $svalue) {
                                $store_ids[$svalue['StoreID']] = $svalue['StoreID'];
                            }
                        }
                        $DB->close();
                        ?>
                        <div class="panel">
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="example-box-wrapper">
                                        <div class="tabs">
                                            <div id="normal-tabs-1">
                                                <span class="form_result">&nbsp; <br>
                                                </span>

                                                <div class="panel-body">
                                                    <h3 class="title-hero">List of all Stores ( <?php echo isset($service_data[0]['ServiceName']) ? $service_data[0]['ServiceName'] : ''; ?> )</h3>

                                                    <form role="form" class="form-horizontal bordered-row enquiry_form" onSubmit="proceed_formsubmit('.enquiry_form', 'Service_store_map.php', '.result_message', '', '', '', '.imageupload');
                return false;">
                                                        <span class="result_message">&nbsp; <br>
                                                        </span>
                                                        <input type="hidden" name="service_pos_id" value="<?php echo isset($service_pos_id) ? $service_pos_id : '0'; ?>"/>
                                                        <input type="hidden" name="service_code" value="<?php echo isset($service_data[0]['ServiceCode']) ? $service_data[0]['ServiceCode'] : ''; ?>"/>

                                                        <div class="col-md-12">
                                                            <?php
                                                            if (isset($all_stores) && is_array($all_stores) && count($all_stores) > 0) {
                                                                foreach ($all_stores as $skey => $svalue) {
                                                                    ?>
                                                                    <div class="col-md-4 col-sm-4">
                                                                        <input type="checkbox" name="store[]" value="<?php echo $svalue['StoreID']; ?>" id="store_<?php echo $svalue['StoreID']; ?>"
                                                                               <?php echo isset($store_ids) && in_array($svalue['StoreID'], $store_ids) ? 'checked' : '' ?>/><label for="store_<?php echo $svalue['StoreID']; ?>"><?php echo $svalue['StoreName']; ?></label><br>
                                                                    </div>                                                       
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </div>
                                                        <div class="col-md-4 col-md-offset-8"> 
                                                            <input type="submit" class="btn ra-100 btn-primary" value="Submit">
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php require_once 'incFooter.fya'; ?>
        </div>
    </body>
</html>

