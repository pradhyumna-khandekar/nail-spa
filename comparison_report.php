<?php require_once("setting.fya"); ?>
<?php require_once 'incFirewall.fya'; ?>


<?php
$strPageTitle = "Comparison Report | Nailspa";
$strDisplayTitle = "Comparison Report for Nailspa";
$strMenuID = "3";
$strMyTable = "tblOffers";
$strMyTableID = "OfferID";
$strMyField = "OfferName";
$strMyActionPage = "comparison_report.php";
$strMessage = "";
$sqlColumn = "";
$sqlColumnValues = "";
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php require_once("incMetaScript.fya"); ?>
        <!-----------css & js files added for tabs by gandhali 3/9/18-------------->
        <link rel="stylesheet" type="text/css" href="assets/widgets/tabs-ui/tabs.css">
        <script type="text/javascript" src="assets/js-core/jquery-ui-core.js"></script>
    </head>

    <body>
        <div id="sb-site">

            <?php // require_once("incOpenLayout.fya");     ?>
            <!----------commented by gandhali 3/9/18---------------->


            <?php require_once("incLoader.fya"); ?>

            <div id="page-wrapper">
                <div id="mobile-navigation"><button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button></div>

                <?php require_once("incLeftMenu.fya"); ?>

                <div id="page-content-wrapper">
                    <div id="page-content">

                        <?php require_once("incHeader.fya"); ?>

                        <script type="text/javascript" src="assets/widgets/datepicker/datepicker.js"></script>
                        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker.js"></script>
                        <script type="text/javascript" src="assets/widgets/datepicker-ui/datepicker-demo.js"></script>
                        <script type="text/javascript" src="assets/widgets/daterangepicker/moment.js"></script>

                        <script>
                            $(function ()
                            {
                                $(".datepicker-example").datepicker();
                            });
                        </script>

                        <?php
                        $DB = Connect();


                        $DB->close();
                        ?>
                        <div id="page-title">
                            <h2>View Comparison Report</h2>
                        </div>

                        <div class="panel">
                            <div class="panel-body">
                                <form method="get" class="form-horizontal bordered-row" role="form" action="comparison_report.php">
                                    <div class="form-group">
                                        <label for="" class="col-sm-1 control-label">Year1 :</label>
                                        <div class="col-sm-3">
                                            <div class="input-prepend input-group">
                                                <span class="add-on input-group-addon">
                                                    <i class="glyph-icon icon-calendar"></i>
                                                </span> 
                                                <?php /* <input type="text" name="date1" id="ReportDate1"  class="form-control datepicker-example" data-date-format="yy/mm/dd" value="<?php echo isset($_GET["date1"]) ? $_GET["date1"] : ''; ?>"> */ ?>

                                                <select name="date1" class="form-control">
                                                    <option value="">Select Year.</option>
                                                    <?php for ($i = 2010; $i <= date('Y'); $i++) { ?>
                                                        <option value="<?php echo $i; ?>" <?php echo isset($_GET["date1"]) && $_GET["date1"] == $i ? 'selected' : ''; ?>><?php echo $i; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <label for="" class="col-sm-1 control-label">Year2 :</label>
                                        <div class="col-sm-3">
                                            <div class="input-prepend input-group">
                                                <span class="add-on input-group-addon">
                                                    <i class="glyph-icon icon-calendar"></i>
                                                </span> 
                                                <?php /* <input type="text" name="date2" id="ReportDate2"  class="form-control datepicker-example" data-date-format="yy/mm/dd"  value="<?php echo isset($_GET["date2"]) ? $_GET["date2"] : ''; ?>"> */ ?>

                                                <select name="date2" class="form-control">
                                                    <option value="">Select Year.</option>
                                                    <?php for ($i = 2010; $i <= date('Y'); $i++) { ?>
                                                        <option value="<?php echo $i; ?>" <?php echo isset($_GET["date2"]) && $_GET["date2"] == $i ? 'selected' : ''; ?>><?php echo $i; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <label for="" class="col-sm-1 control-label">Year3 :</label>
                                        <div class="col-sm-3">
                                            <div class="input-prepend input-group">
                                                <span class="add-on input-group-addon">
                                                    <i class="glyph-icon icon-calendar"></i>
                                                </span> 
                                                <?php /* <input type="text" name="date3" id="ReportDate3"  class="form-control datepicker-example" data-date-format="yy/mm/dd"  value="<?php echo isset($_GET["date3"]) ? $_GET["date3"] : ''; ?>"> */ ?>

                                                <select name="date3" class="form-control">
                                                    <option value="">Select Year.</option>
                                                    <?php for ($i = 2010; $i <= date('Y'); $i++) { ?>
                                                        <option value="<?php echo $i; ?>" <?php echo isset($_GET["date3"]) && $_GET["date3"] == $i ? 'selected' : ''; ?>><?php echo $i; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <label class="col-sm-1 control-label">Select Store</label>
                                        <div class="col-sm-3">
                                            <select name="Store" class="form-control">
                                                <option value="0">All</option>
                                                <?php
                                                $selp = select("*", "tblStores", "StoreID > '0'");
                                                foreach ($selp as $val) {
                                                    $strStoreName = $val["StoreName"];
                                                    $strStoreID = $val["StoreID"];
                                                    $store = $_GET["Store"];
                                                    if ($store == $strStoreID) {
                                                        ?>
                                                        <option  selected value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option value="<?= $strStoreID ?>" ><?= $strStoreName ?></option>														
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>

                                        <label for="" class="col-sm-1 control-label">Select Report :</label>
                                        <div class="col-sm-3">
                                            <select name="report_type" class="form-control" id="report_type">
                                                <option value="">Select Report...</option>
                                                <option value="1" <?php echo isset($_GET['report_type']) && $_GET['report_type'] == 1 ? 'selected' : ''; ?>>Service Sale</option>
                                                <option value="2" <?php echo isset($_GET['report_type']) && $_GET['report_type'] == 2 ? 'selected' : ''; ?>>Service Sale Category</option>
                                                <?php /* <option value="3" <?php echo isset($_GET['report_type']) && $_GET['report_type'] == 3 ? 'selected' : ''; ?>>Product Consumption</option> */ ?>
                                                <option value="4" <?php echo isset($_GET['report_type']) && $_GET['report_type'] == 4 ? 'selected' : ''; ?>>Membership</option>
                                                <option value="5" <?php echo isset($_GET['report_type']) && $_GET['report_type'] == 5 ? 'selected' : ''; ?>>Employee Performance</option>
                                            </select>
                                        </div>

                                        <?php
                                        if (isset($_GET['report_type']) && $_GET['report_type'] == 5) {
                                            $display = "";
                                        } else {
                                            $display = "style='display:none;'";
                                        }
                                        ?>
                                        <div id="emp_div" <?php echo $display; ?>>
                                            <label for="" class="col-sm-1 control-label">Employee</label>
                                            <div class="col-sm-3">
                                                <select class="form-control required"  name="emp_id">
                                                    <?php
                                                    $selp = select("*", "tblEmployees", "Status='0'");
                                                    foreach ($selp as $val) {
                                                        $EIDD = $val["EID"];
                                                        $EmployeeName = $val["EmployeeName"];
                                                        ?>
                                                        <option  <?php echo isset($_GET["emp_id"]) && $_GET["emp_id"] == $val["EID"] ? 'selected' : '' ?> value="<?= $val["EID"] ?>" ><?= $EmployeeName ?></option>														
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div> </div>

                                    <div class="form-group">
                                        <label class="col-sm-1 control-label"></label>
                                        <button type="submit" class="btn btn-alt btn-hover btn-success"><span>Apply Filter</span> <i class="glyph-icon icon-arrow-right"></i><div class="ripple-wrapper"></div></button>
                                        &nbsp;&nbsp;&nbsp;
                                        <a class="btn btn-link" href="comparison_report.php">Clear All Filter</a>
                                        &nbsp;&nbsp;&nbsp;

                                    </div>
                                </form>
                                <div>
                                    <?php $get_param = $_SERVER['QUERY_STRING']; ?>
                                    <?php if (isset($_GET['report_type']) && $_GET['report_type'] == 1) { ?>
                                        <iframe src="ReportSaleComparison.php?<?php echo $get_param; ?>" style="min-width: 100%; height: 1000px; margin: 0 auto;border: 1px solid #eee;"></iframe>
                                    <?php } else if (isset($_GET['report_type']) && $_GET['report_type'] == 2) { ?>
                                        <iframe src="ReportSaleCategoryComparison.php?<?php echo $get_param; ?>" style="min-width: 100%; height: 1000px; margin: 0 auto;border: 1px solid #eee;"></iframe>
                                    <?php } else if (isset($_GET['report_type']) && $_GET['report_type'] == 4) { ?>
                                        <iframe src="MembershipComparison.php?<?php echo $get_param; ?>" style="min-width: 100%; height: 1000px; margin: 0 auto;border: 1px solid #eee;"></iframe>
                                    <?php } else if (isset($_GET['report_type']) && $_GET['report_type'] == 5) { ?>
                                        <iframe src="EmployeePerformanceComparison.php?<?php echo $get_param; ?>" style="min-width: 100%; height: 1000px; margin: 0 auto;border: 1px solid #eee;"></iframe>
                                    <?php } else { ?>
                                        <h3>Select Report And date...</h3>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <?php require_once 'incFooter.fya'; ?>

            <script>
                $(document).ready(function () {
                    $('#report_type').on('change', function () {
                        var value = $(this).val();
                        if (value == 5) {
                            $('#emp_div').show();
                        } else {
                            $('#emp_div').hide();
                        }
                    });
                });
            </script>
        </div>
    </body>
</html>
